"use strict";
const { Model } = require("sequelize");
require("pg").defaults.parseInt8 = true;
module.exports = (sequelize, DataTypes) => {
  class BrandfolderAttachment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  BrandfolderAttachment.init(
    {
      attachment_id: DataTypes.STRING,
      asin: DataTypes.STRING,
      filename: DataTypes.STRING,
      extension: DataTypes.STRING,
      sku: DataTypes.STRING,
      ean: DataTypes.STRING,
      size: DataTypes.STRING,
      us_shape: DataTypes.STRING,
      deleted: DataTypes.BOOLEAN,
      primary: DataTypes.BOOLEAN,
      packaging: DataTypes.BOOLEAN,
      url: DataTypes.STRING,
      position: DataTypes.INTEGER,
      brand_internal_product_code: DataTypes.STRING,
      au_salsify_digital_asset_id: DataTypes.STRING,
      au_salsify_linked_to_product: DataTypes.BOOLEAN,
      global_salsify_digital_asset_id: DataTypes.STRING,
      global_salsify_linked_to_product: DataTypes.BOOLEAN,
      au_office_additional_ordering: DataTypes.STRING,
      au_office_dimensions_ordering: DataTypes.INTEGER,
      au_office_packaging_ordering: DataTypes.INTEGER,
      bf_updated_date: DataTypes.DATE,
      asset_id: DataTypes.STRING,
      section: DataTypes.STRING,
      season: DataTypes.STRING,
      us_sku: DataTypes.STRING,
      categories: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("categories"));
        },
        set: function (value) {
          return this.setDataValue("categories", JSON.stringify(value));
        },
      },
      au_salsify_snapshot: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("au_salsify_snapshot"));
        },
        set: function (value) {
          return this.setDataValue(
            "au_salsify_snapshot",
            JSON.stringify(value)
          );
        },
      },
      global_salsify_snapshot: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("global_salsify_snapshot"));
        },
        set: function (value) {
          return this.setDataValue(
            "global_salsify_snapshot",
            JSON.stringify(value)
          );
        },
      },
      labels: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("labels"));
        },
        set: function (value) {
          return this.setDataValue("labels", JSON.stringify(value));
        },
      },
      collections: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("collections"));
        },
        set: function (value) {
          return this.setDataValue("collections", JSON.stringify(value));
        },
      },
      tags: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("tags"));
        },
        set: function (value) {
          return this.setDataValue("tags", JSON.stringify(value));
        },
      },
      custom_fields: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("custom_fields"));
        },
        set: function (value) {
          return this.setDataValue("custom_fields", JSON.stringify(value));
        },
      },
      name: DataTypes.STRING,
      description: DataTypes.TEXT,
      new_asset_id: DataTypes.STRING,
      new_attachment_id: DataTypes.STRING,
      new_attachment_url: DataTypes.STRING,
      snapshot: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("snapshot"));
        },
        set: function (value) {
          return this.setDataValue("snapshot", JSON.stringify(value));
        },
      },
    },
    {
      sequelize,
      modelName: "BrandfolderAttachment",
    }
  );
  return BrandfolderAttachment;
};
