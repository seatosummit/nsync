"use strict";
const { Model } = require("sequelize");
require("pg").defaults.parseInt8 = true;
module.exports = (sequelize, DataTypes) => {
  class BrandfolderDigitalAsset extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of DataTypes lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  BrandfolderDigitalAsset.init(
    {
      salsify_digital_asset_id: {
        type: DataTypes.STRING,
      },
      salsify_brand: {
        type: DataTypes.STRING,
      },
      base_product_name: {
        type: DataTypes.STRING,
      },
      short_description: {
        type: DataTypes.TEXT,
      },
      brandfolder_asset_id: {
        type: DataTypes.STRING,
      },
      brandfolder_asset_url: {
        type: DataTypes.STRING,
      },
      brandfolder_collection_id: {
        type: DataTypes.STRING,
      },
      salsify_url: {
        type: DataTypes.STRING,
      },
      filename: {
        type: DataTypes.STRING,
      },
      format: {
        type: DataTypes.STRING,
      },
      salsify_parent_skus: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("salsify_parent_skus"));
        },
        set: function (value) {
          return this.setDataValue(
            "salsify_parent_skus",
            JSON.stringify(value)
          );
        },
      },
      salsify_variant_skus: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("salsify_variant_skus"));
        },
        set: function (value) {
          return this.setDataValue(
            "salsify_variant_skus",
            JSON.stringify(value)
          );
        },
      },
      salsify_parent_eans: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("salsify_parent_eans"));
        },
        set: function (value) {
          return this.setDataValue(
            "salsify_parent_eans",
            JSON.stringify(value)
          );
        },
      },
      salsify_variant_eans: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("salsify_variant_eans"));
        },
        set: function (value) {
          return this.setDataValue(
            "salsify_variant_eans",
            JSON.stringify(value)
          );
        },
      },
      au_office_image_stack: {
        type: DataTypes.STRING,
      },
      us_office_image_stack: {
        type: DataTypes.STRING,
      },
      ger_office_image_stack: {
        type: DataTypes.STRING,
      },
      au_office_additional_ordering: {
        type: DataTypes.STRING,
      },
      gender: {
        type: DataTypes.STRING,
      },
      base_product_name: {
        type: DataTypes.STRING,
      },
      colour: {
        type: DataTypes.STRING,
      },
      size: {
        type: DataTypes.STRING,
      },
      layman_colour: {
        type: DataTypes.STRING,
      },
      file_size_bytes: {
        type: DataTypes.INTEGER,
      },
      original_width_px: {
        type: DataTypes.INTEGER,
      },
      original_height_px: {
        type: DataTypes.INTEGER,
      },
      tags: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("tags"));
        },
        set: function (value) {
          return this.setDataValue("tags", JSON.stringify(value));
        },
      },
      salsify_digital_asset_snapshot: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(
            this.getDataValue("salsify_digital_asset_snapshot")
          );
        },
        set: function (value) {
          return this.setDataValue(
            "salsify_digital_asset_snapshot",
            JSON.stringify(value)
          );
        },
      },
    },
    {
      sequelize,
      modelName: "BrandfolderDigitalAsset",
    }
  );
  return BrandfolderDigitalAsset;
};
