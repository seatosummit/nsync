"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class PriceList extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      PriceList.belongsTo(models.SalesChannel);
      PriceList.hasMany(models.PricingEnhancement);
    }
  }
  PriceList.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      SalesChannelId: DataTypes.UUID,
      name: DataTypes.STRING,
      description: DataTypes.STRING,
      erpFieldName: DataTypes.STRING,
      removeGSTFromPrice: DataTypes.BOOLEAN,
      timeSensitive: DataTypes.BOOLEAN,
      timeSensitiveStartField: DataTypes.STRING,
      timeSensitiveEndField: DataTypes.STRING,
      price_list_slug: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "PriceList",
    }
  );
  return PriceList;
};
