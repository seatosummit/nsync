"use strict";

const fs = require("fs");
const path = require("path");
const basename = path.basename(__filename);
const {
  SequelizeConnector,
} = require("../modules/sequelize/SequelizeConnector");
const db = {};
const sequelizeObject = SequelizeConnector.createConnection(
  "Sequelize module index"
);

fs.readdirSync(__dirname)
  .filter((file) => {
    return (
      file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
    );
  })
  .forEach((file) => {
    const model = require(path.join(__dirname, file))(
      sequelizeObject.sequelize,
      sequelizeObject.Sequelize.DataTypes
    );
    db[model.name] = model;
  });

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelizeObject.sequelize;
db.Sequelize = sequelizeObject.Sequelize;

module.exports = db;
