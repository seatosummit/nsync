"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class ProductVariantEnhancement extends Model {
    static associate(models) {
      ProductVariantEnhancement.belongsTo(models.SalesChannel);
      ProductVariantEnhancement.belongsTo(models.Product);
      ProductVariantEnhancement.belongsTo(models.ProductVariant);
      ProductVariantEnhancement.belongsTo(models.ProductEnhancement);
      ProductVariantEnhancement.hasMany(models.PricingEnhancement);
    }
  }
  ProductVariantEnhancement.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      ProductId: DataTypes.UUID,
      ProductVariantId: DataTypes.UUID,
      SalesChannelId: DataTypes.UUID,
      shopifyProductId: DataTypes.BIGINT,
      shopifyVariantId: DataTypes.BIGINT,
      GraphQLVariantId: DataTypes.STRING,
      GraphQLInventoryItemId: DataTypes.STRING,
      shopifyImageId: DataTypes.BIGINT,
      override_pricing: { type: DataTypes.BOOLEAN, defaultValue: false },
      price: DataTypes.DOUBLE,
      compareToPrice: DataTypes.DOUBLE,
      warehousePrice: DataTypes.DOUBLE,
      sku: DataTypes.STRING,
      barcode_ean: DataTypes.STRING,
      size: DataTypes.STRING,
      colour: DataTypes.STRING,
      colourLayman: DataTypes.STRING,
      metafields: DataTypes.JSON,
      shopifySnapshot: DataTypes.JSON,
      shopifyInventoryItemId: DataTypes.BIGINT,
      shopifyInventoryQuantity: DataTypes.INTEGER,
      enabled: DataTypes.BOOLEAN,
      disable_reason: DataTypes.STRING,
      minimumOrderQuantity: DataTypes.INTEGER,
      updatedToShopify: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "ProductVariantEnhancement",
    }
  );
  return ProductVariantEnhancement;
};
