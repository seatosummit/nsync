'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class STSBufferProduct extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  STSBufferProduct.init({
    sku: DataTypes.STRING,
    name: DataTypes.STRING,
    woocommerce_product_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'STSBufferProduct',
  });
  return STSBufferProduct;
};