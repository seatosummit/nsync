"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class ProductEnhancement extends Model {
    static associate(models) {
      ProductEnhancement.belongsTo(models.SalesChannel);
      ProductEnhancement.belongsTo(models.Product);
      ProductEnhancement.hasMany(models.ProductVariantEnhancement);
    }
  }
  ProductEnhancement.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      ProductId: DataTypes.UUID,
      SalesChannelId: DataTypes.UUID,
      shopifyProductId: DataTypes.BIGINT,
      GraphQLProductId: DataTypes.STRING,
      shopifyImages: DataTypes.JSON,
      shopifyOptions: DataTypes.JSON,
      shopifyVariants: DataTypes.JSON,
      metafields: DataTypes.JSON,
      status: DataTypes.STRING,
      shopifyPayload: {
        type: DataTypes.JSON,
        defaultValue: {},
      },
      validPayload: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      updatedToShopify: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "ProductEnhancement",
    }
  );
  return ProductEnhancement;
};
