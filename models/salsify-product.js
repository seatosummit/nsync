"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class SalsifyProduct extends Model {
    static associate(models) {
      SalsifyProduct.belongsTo(models.SalsifyProductParent, {
        foreignKey: "parent_sku",
        targetKey: "sku",
      });
      SalsifyProduct.hasOne(models.SalsifyGlobalProduct, {
        foreignKey: "ean",
        sourceKey: "barcode_ean",
      });
    }
  }
  SalsifyProduct.init(
    {
      sku: DataTypes.STRING,
      name: DataTypes.STRING,
      size: DataTypes.STRING,
      brand: DataTypes.STRING,
      colour: DataTypes.STRING,
      colour_layman: DataTypes.STRING,
      base_product_name: DataTypes.STRING,
      brand_internal_product_code: DataTypes.STRING,
      barcode_ean: DataTypes.STRING,
      usa_sku: DataTypes.STRING,
      asin_amazon: DataTypes.STRING,
      primary_image: DataTypes.STRING,
      additional_images: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("additional_images"));
        },
        set: function (value) {
          return this.setDataValue("additional_images", JSON.stringify(value));
        },
      },
      packaging_images: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("packaging_images"));
        },
        set: function (value) {
          return this.setDataValue("packaging_images", JSON.stringify(value));
        },
      },
      salsify_category_ids: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("salsify_category_ids"));
        },
        set: function (value) {
          return this.setDataValue(
            "salsify_category_ids",
            JSON.stringify(value)
          );
        },
      },
      erp_tier1_financial_category: DataTypes.STRING,
      erp_category: DataTypes.STRING,
      erp_sub_category: DataTypes.STRING,
      b2b_category: DataTypes.STRING,
      b2b_sub_category: DataTypes.STRING,
      parent_sku: DataTypes.STRING,
      simple_or_configurable: DataTypes.STRING,
      is_child: DataTypes.BOOLEAN,
      digital_assets: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("digital_assets"));
        },
        set: function (value) {
          return this.setDataValue("digital_assets", JSON.stringify(value));
        },
      },
      snapshot: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("snapshot"));
        },
        set: function (value) {
          return this.setDataValue("snapshot", JSON.stringify(value));
        },
      },
    },
    {
      sequelize,
      modelName: "SalsifyProduct",
    }
  );
  return SalsifyProduct;
};
