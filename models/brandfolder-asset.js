"use strict";
const { Model } = require("sequelize");
require("pg").defaults.parseInt8 = true;
module.exports = (sequelize, DataTypes) => {
  class BrandfolderAsset extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  BrandfolderAsset.init(
    {
      asset_id: {
        type: DataTypes.STRING,
      },
      name: {
        type: DataTypes.STRING,
      },
      type: {
        type: DataTypes.STRING,
      },
      attachments: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("attachments"));
        },
        set: function (value) {
          return this.setDataValue("attachments", JSON.stringify(value));
        },
      },
      section: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("section"));
        },
        set: function (value) {
          return this.setDataValue("section", JSON.stringify(value));
        },
      },
      collections: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("collections"));
        },
        set: function (value) {
          return this.setDataValue("collections", JSON.stringify(value));
        },
      },
      custom_fields: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("custom_fields"));
        },
        set: function (value) {
          return this.setDataValue("custom_fields", JSON.stringify(value));
        },
      },
      tags: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("tags"));
        },
        set: function (value) {
          return this.setDataValue("tags", JSON.stringify(value));
        },
      },
      labels: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("labels"));
        },
        set: function (value) {
          return this.setDataValue("labels", JSON.stringify(value));
        },
      },
      associated_skus: {
        type: DataTypes.TEXT("long"),
        get: function () {
          const currentValue = this.getDataValue("associated_skus");
          return currentValue != null ? currentValue.split(",") : [];
        },
        set: function (value) {
          const convertedValue = Array.isArray(value)
            ? value.toString()
            : value;
          return this.setDataValue("associated_skus", convertedValue);
        },
      },
      associated_eans: {
        type: DataTypes.TEXT("long"),
        get: function () {
          const currentValue = this.getDataValue("associated_eans");
          return currentValue != null ? currentValue.split(",") : [];
        },
        set: function (value) {
          const convertedValue = Array.isArray(value)
            ? value.toString()
            : value;
          return this.setDataValue("associated_eans", convertedValue);
        },
      },
      metadata: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("metadata"));
        },
        set: function (value) {
          return this.setDataValue("metadata", JSON.stringify(value));
        },
      },
      snapshot: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("snapshot"));
        },
        set: function (value) {
          return this.setDataValue("snapshot", JSON.stringify(value));
        },
      },
    },
    {
      sequelize,
      modelName: "BrandfolderAsset",
    }
  );
  return BrandfolderAsset;
};
