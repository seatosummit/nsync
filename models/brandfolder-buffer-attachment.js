"use strict";
const { Model } = require("sequelize");
require("pg").defaults.parseInt8 = true;
module.exports = (sequelize, DataTypes) => {
  class BrandfolderBufferAttachment extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of DataTypes lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      BrandfolderBufferAttachment.belongsTo(models.BrandfolderBufferAsset);
    }
  }
  BrandfolderBufferAttachment.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      BrandfolderBufferAssetId: DataTypes.UUID,
      bf_id: DataTypes.STRING,
      bf_attachment_id: DataTypes.STRING,
      bf_asset_id: DataTypes.STRING,
      filename: DataTypes.STRING,
      extension: DataTypes.STRING,
      section_id: DataTypes.STRING,
      url: DataTypes.STRING,
      bf_updated_date: DataTypes.DATE,
      metadata: DataTypes.JSON,
      snapshot: DataTypes.JSON,
    },
    {
      sequelize,
      modelName: "BrandfolderBufferAttachment",
    }
  );
  return BrandfolderBufferAttachment;
};
