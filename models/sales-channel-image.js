"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class SalesChannelImage extends Model {
    static associate(models) {
      SalesChannelImage.belongsTo(models.SalesChannel);
      SalesChannelImage.belongsTo(models.ProductEnhancement);
      SalesChannelImage.belongsTo(models.ProductVariantEnhancement);
      SalesChannelImage.belongsTo(models.BrandfolderBufferAttachment);
      SalesChannelImage.belongsTo(models.BrandfolderBufferAsset);
    }
  }
  SalesChannelImage.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      SalesChannelId: DataTypes.UUID,
      ProductEnhancementId: DataTypes.UUID,
      ProductVariantEnhancementId: DataTypes.UUID,
      BrandFolderBufferAttachmentId: DataTypes.UUID,
      BrandfolderBufferAssetId: DataTypes.UUID,
      brandfolderURL: DataTypes.STRING,
      format: DataTypes.STRING,
      name: DataTypes.STRING,
      associatedSKUs: DataTypes.JSON,
      imageGraphQLId: DataTypes.STRING,
      imageShopifyURL: DataTypes.STRING,
      imageStack: DataTypes.STRING,
      orderNumber: DataTypes.INTEGER,
      isPrimary: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "SalesChannelImage",
    }
  );
  return SalesChannelImage;
};
