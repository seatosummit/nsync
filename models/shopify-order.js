"use strict";
const { Model } = require("sequelize");
require("pg").defaults.parseInt8 = true;
module.exports = (sequelize, DataTypes) => {
  class ShopifyOrder extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      /*ShopifyOrder.belongsTo(models.SalesChannel, {
        onDelete: null,
      });*/
    }
  }
  ShopifyOrder.init(
    {
      salesChannelId: DataTypes.STRING,
      orderID: DataTypes.BIGINT,
      orderNumber: DataTypes.STRING,
      checkoutID: DataTypes.STRING,
      orderSnapshot: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("orderSnapshot"));
        },
        set: function (value) {
          return this.setDataValue("orderSnapshot", JSON.stringify(value));
        },
      },
      xmlExported: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      customerEmail: DataTypes.STRING,
      fulfilled: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
    },
    {
      sequelize,
      modelName: "ShopifyOrder",
    }
  );
  return ShopifyOrder;
};
