"use strict";
const { Model } = require("sequelize");
require("pg").defaults.parseInt8 = true;
module.exports = (sequelize, DataTypes) => {
  class BrandfolderBufferAsset extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of DataTypes lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      BrandfolderBufferAsset.hasMany(models.BrandfolderBufferAttachment);
      BrandfolderBufferAsset.hasMany(models.PepperiBufferImage);
    }
  }
  BrandfolderBufferAsset.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      bf_id: DataTypes.STRING,
      bf_asset_id: DataTypes.STRING,
      bf_processed: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      name: DataTypes.STRING,
      description: DataTypes.TEXT,
      brand: DataTypes.STRING,
      type: DataTypes.STRING,
      collections: DataTypes.JSON,
      custom_fields: DataTypes.JSON,
      tags: DataTypes.JSON,
      labels: DataTypes.JSON,
      au_skus: DataTypes.JSON,
      usa_skus: DataTypes.JSON,
      parent_skus: DataTypes.JSON,
      eans: DataTypes.JSON,
      au_office_image_stack: DataTypes.STRING,
      au_office_image_ordering: DataTypes.STRING,
      usa_office_image_stack: DataTypes.STRING,
      ger_office_image_stack: DataTypes.STRING,
      china_office_image_stack: DataTypes.STRING,
      brand_internal_product_code: DataTypes.STRING,
      au_salsify_digital_asset_id: DataTypes.STRING,
      au_salsify_linked_to_product: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      global_salsify_digital_asset_id: DataTypes.STRING,
      global_salsify_linked_to_product: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      section_id: DataTypes.STRING,
      snapshot: DataTypes.JSON,
    },
    {
      sequelize,
      modelName: "BrandfolderBufferAsset",
    }
  );
  return BrandfolderBufferAsset;
};
