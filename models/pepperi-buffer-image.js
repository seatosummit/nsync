"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class PepperiBufferImage extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PepperiBufferImage.belongsTo(models.BrandfolderBufferAsset);
    }
  }
  PepperiBufferImage.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      BrandfolderBufferAssetId: DataTypes.UUID,
      sku: DataTypes.STRING,
      filename: DataTypes.STRING,
      original_url: DataTypes.STRING,
      brand: DataTypes.STRING,
      uploadedToSFTP: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
    },
    {
      sequelize,
      modelName: "PepperiBufferImage",
    }
  );
  return PepperiBufferImage;
};
