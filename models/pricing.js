"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Pricing extends Model {
    static associate(models) {
      Pricing.belongsTo(models.SalesChannel);
    }
  }
  Pricing.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      SalesChannelId: DataTypes.UUID,
      variant_sku: DataTypes.STRING,
      variant_ean: DataTypes.STRING,
      price_retail: DataTypes.DOUBLE,
      price_warehouse: DataTypes.DOUBLE,
      price_special: DataTypes.DOUBLE,
      special_price_startdate: DataTypes.DATE,
      special_price_enddate: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "Pricing",
    }
  );
  return Pricing;
};
