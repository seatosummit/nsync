"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class SalsifyProductParent extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      SalsifyProductParent.hasMany(models.SalsifyProduct, {
        foreignKey: "parent_sku",
        sourceKey: "sku",
      });
    }
  }
  SalsifyProductParent.init(
    {
      sku: DataTypes.STRING,
      name: DataTypes.STRING,
      brand: DataTypes.STRING,
      brand_category: DataTypes.STRING,
      base_product_name: DataTypes.STRING,
      primary_image: DataTypes.STRING,
      additional_images: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("additional_images"));
        },
        set: function (value) {
          return this.setDataValue("additional_images", JSON.stringify(value));
        },
      },
      packaging_images: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("packaging_images"));
        },
        set: function (value) {
          return this.setDataValue("packaging_images", JSON.stringify(value));
        },
      },
      salsify_category_ids: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("salsify_category_ids"));
        },
        set: function (value) {
          return this.setDataValue(
            "salsify_category_ids",
            JSON.stringify(value)
          );
        },
      },
      brand_sub_category: DataTypes.STRING,
      erp_tier1_financial_category: DataTypes.STRING,
      erp_category: DataTypes.STRING,
      erp_sub_category: DataTypes.STRING,
      b2b_category: DataTypes.STRING,
      b2b_sub_category: DataTypes.STRING,
      colour_layman: DataTypes.STRING,
      child_skus: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return this.getDataValue("child_skus").split(",");
        },
        set: function (value) {
          const convertedValue = Array.isArray(value)
            ? value.toString()
            : value;
          return this.setDataValue("child_skus", convertedValue);
        },
      },
      snapshot: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("snapshot"));
        },
        set: function (value) {
          return this.setDataValue("snapshot", JSON.stringify(value));
        },
      },
    },
    {
      sequelize,
      modelName: "SalsifyProductParent",
    }
  );
  return SalsifyProductParent;
};
