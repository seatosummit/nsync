"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class SalesChannelProductEnable extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.other = models.SalesChannelProductEnable.belongsTo(models.WMSProduct, {foreignKey: 'sku'});
    }
  }
  SalesChannelProductEnable.init(
    {
      sku: DataTypes.STRING,
      channel_enum: DataTypes.STRING,
      enabled: DataTypes.BOOLEAN
    },
    {
      sequelize,
      modelName: "SalesChannelProductEnable",
    }
  );
  return SalesChannelProductEnable;
};
