'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class WMSCustomer extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define associations here
    }
  };
  WMSCustomer.init({
    myob_card_id: DataTypes.INTEGER,
    company_name: DataTypes.STRING,
    contact_name: DataTypes.STRING,
    contact_email: DataTypes.STRING,
    category: DataTypes.STRING,
    sts_territory_manager: DataTypes.STRING,
    street1: DataTypes.STRING,
    street2: DataTypes.STRING,
    street3: DataTypes.STRING,
    street4: DataTypes.STRING,
    suburb: DataTypes.STRING,
    state: DataTypes.STRING,
    postcode: DataTypes.STRING,
    phone: DataTypes.STRING,
    customerSnapshot: {
      type: DataTypes.TEXT('long'),
        get: function() {
          return JSON.parse(this.getDataValue("customerSnapshot"));
        },
        set: function(value) {
          return this.setDataValue("customerSnapshot", JSON.stringify(value));
        }
    }
  }, {
    sequelize,
    modelName: 'WMSCustomer',
  });
  return WMSCustomer;
};