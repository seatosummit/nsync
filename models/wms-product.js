"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class WMSProduct extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      WMSProduct.hasOne(models.SalsifyProduct, {
        foreignKey: "sku",
        sourceKey: "sku",
      });
    }
  }
  WMSProduct.init(
    {
      WMSProductID: DataTypes.INTEGER,
      sku: DataTypes.STRING,
      name: DataTypes.STRING,
      unitPrice: DataTypes.DOUBLE,
      category: DataTypes.STRING,
      brand: DataTypes.STRING,
      barcode: DataTypes.STRING,
      retailPrice: DataTypes.DOUBLE,
      perUnitQuantity: DataTypes.DOUBLE,
      stockQuantity: { type: DataTypes.INTEGER, defaultValue: 0 },
      unitOfMeasure: DataTypes.STRING,
      pepperiEnabled: DataTypes.BOOLEAN,
      b2b_enabled: DataTypes.BOOLEAN,
      b2c_enabled: DataTypes.BOOLEAN,
      stockETA: DataTypes.DATE,
      status: DataTypes.STRING,
      moq: { type: DataTypes.INTEGER, defaultValue: 1 },
      productSnapshot: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("productSnapshot"));
        },
        set: function (value) {
          return this.setDataValue("productSnapshot", JSON.stringify(value));
        },
      },
    },
    {
      sequelize,
      modelName: "WMSProduct",
    }
  );
  return WMSProduct;
};
