"use strict";
const { Model } = require("sequelize");
require("pg").defaults.parseInt8 = true;
module.exports = (sequelize, DataTypes) => {
  class BlackDiamondProductCategoryDefinition extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  BlackDiamondProductCategoryDefinition.init(
    {
      name: DataTypes.STRING,
      salsify_id: DataTypes.STRING,
      custom_name: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "BlackDiamondProductCategoryDefinition",
    }
  );
  return BlackDiamondProductCategoryDefinition;
};
