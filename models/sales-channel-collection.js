"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class SalesChannelCollection extends Model {
    static associate(models) {
      SalesChannelCollection.belongsTo(models.SalesChannel);
    }
  }
  SalesChannelCollection.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      SalesChannelId: DataTypes.STRING,
      shopifyCollectionId: DataTypes.STRING,
      title: DataTypes.STRING,
      handle: DataTypes.STRING,
      appliedDisjunctively: { type: DataTypes.BOOLEAN, defaultValue: false },
      descriptionHtml: DataTypes.TEXT,
      sortOrder: {
        type: DataTypes.STRING,
        defaultValue: "BEST_SELLING",
      },
      templateSuffix: {
        type: DataTypes.STRING,
      },
      ruleSet: {
        type: DataTypes.JSON,
      },
      payload: {
        type: DataTypes.JSON,
      },
    },
    {
      sequelize,
      modelName: "SalesChannelCollection",
    }
  );
  return SalesChannelCollection;
};
