"use strict";
const { Model } = require("sequelize");
require("pg").defaults.parseInt8 = true;
module.exports = (sequelize, DataTypes) => {
  class BlackDiamondBufferCollection extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  BlackDiamondBufferCollection.init(
    {
      shopify_collection_id: DataTypes.BIGINT,
      title: DataTypes.STRING,
      body_html: DataTypes.TEXT,
      template_suffix: DataTypes.STRING,
      handle: DataTypes.STRING,
      salsify_property_value_id: DataTypes.STRING,
      disjunctive: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      sort_order: DataTypes.STRING,
      rules: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("rules"));
        },
        set: function (value) {
          return this.setDataValue("rules", JSON.stringify(value));
        },
      },
      admin_graphql_api_id: DataTypes.STRING,
      snapshot: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("snapshot"));
        },
        set: function (value) {
          return this.setDataValue("snapshot", JSON.stringify(value));
        },
      },
    },
    {
      sequelize,
      modelName: "BlackDiamondBufferCollection",
    }
  );
  return BlackDiamondBufferCollection;
};
