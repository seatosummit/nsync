"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class SalesChannelIntegrationRuleset extends Model {
    static associate(models) {
      SalesChannelIntegrationRuleset.belongsTo(models.SalesChannel);
    }
  }
  SalesChannelIntegrationRuleset.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      SalesChannelId: DataTypes.UUID,
      productAdd: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      productRemove: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      productUpdate: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      productCategorise: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      productEnabledRules: {
        type: DataTypes.JSON,
        allowNull: false,
        defaultValue: {},
      },
      imagesAdd: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      imagesUpdate: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      ordersExport: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      orderExportFilter: DataTypes.STRING,
      ordersUpdateFulfilment: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      tagOrdersWithNoStock: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      salsifyCategoriesSync: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      priceUpdate: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      stockUpdate: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      continueSellingWhenStockZero: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      customOrderNotification: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      usePricingEnhancements: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      },
      defaultPriceFieldName: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: "",
      },
      customOrderNotificationEmail: DataTypes.STRING,
      erpProductDataView: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "SalesChannelIntegrationRuleset",
    }
  );
  return SalesChannelIntegrationRuleset;
};
