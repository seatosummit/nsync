"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class PricingUpdate extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      PricingUpdate.belongsTo(models.SalesChannel);
      PricingUpdate.hasMany(models.PricingEnhancement);
    }
  }
  PricingUpdate.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      SalesChannelId: DataTypes.UUID,
      payload: DataTypes.JSON,
      sku: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "PricingUpdate",
    }
  );
  return PricingUpdate;
};
