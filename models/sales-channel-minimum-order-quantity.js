"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class SalesChannelMinimumOrderQuantity extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  SalesChannelMinimumOrderQuantity.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      SalesChannelId: DataTypes.UUID,
      sku: DataTypes.STRING,
      minimumOrderQuantity: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "SalesChannelMinimumOrderQuantity",
    }
  );
  return SalesChannelMinimumOrderQuantity;
};
