'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PepperiOrderLineItem extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };

// Pepperi fields:
// TotalUnitsPriceAfterDiscount
// TotalUnitsPriceBeforeDiscount
// UnitPrice
// UnitPriceAfterDiscount
// UnitDiscountPercentage


  PepperiOrderLineItem.init({
    SAL_ID: DataTypes.STRING,
    itemID: DataTypes.INTEGER,
    sku: DataTypes.STRING,
    name: DataTypes.STRING,
    quantity: DataTypes.STRING,
    unitPriceExBeforeDiscount: DataTypes.DOUBLE,
    unitPriceExAfterDiscount: DataTypes.DOUBLE,
    lineTotalExBeforeDiscount: DataTypes.DOUBLE,
    lineTotalExAfterDiscount: DataTypes.DOUBLE,
    name: DataTypes.STRING,
    snapshot: {
      type: DataTypes.TEXT('long'),
        get: function() {
          return JSON.parse(this.getDataValue("orderSnapshot"));
        },
        set: function(value) {
          return this.setDataValue("orderSnapshot", JSON.stringify(value));
        }
    }
  }, {
    sequelize,
    modelName: 'PepperiOrderLineItem',
  });
  return PepperiOrderLineItem;
};