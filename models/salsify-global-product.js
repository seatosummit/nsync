"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class SalsifyGlobalProduct extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      SalsifyGlobalProduct.belongsTo(models.SalsifyProduct, {
        foreignKey: "ean",
        targetKey: "barcode_ean",
      });
    }
  }
  SalsifyGlobalProduct.init(
    {
      salsify_id: DataTypes.STRING,
      salsify_created_at: {
        type: DataTypes.DATE,
        get: function () {
          return this.getDataValue("salsify_created_at").toISOString();
        },
      },
      salsify_updated_at: {
        type: DataTypes.DATE,
        get: function () {
          return this.getDataValue("salsify_updated_at").toISOString();
        },
      },
      salsify_parent_id: DataTypes.STRING,
      salsify_system_id: DataTypes.STRING,
      ean: DataTypes.STRING,
      sku: DataTypes.STRING,
      us_sku: DataTypes.STRING,
      primary_name: DataTypes.STRING,
      us_product_name: DataTypes.STRING,
      size_name_longform: DataTypes.STRING,
      brand: DataTypes.STRING,
      category: DataTypes.STRING,
      sub_category: DataTypes.STRING,
      family: DataTypes.STRING,
      sub_family: DataTypes.STRING,
      colour_pantone: DataTypes.STRING,
      colour_family: DataTypes.STRING,
      gender: DataTypes.STRING,
      country_of_origin: DataTypes.STRING,
      detail1: {
        type: DataTypes.STRING,
        // get: function () {
        //   return JSON.parse(this.getDataValue("detail1"));
        // },
        // set: function (value) {
        //   return this.setDataValue("detail1", JSON.stringify(value));
        // },
      },
      detail2: {
        type: DataTypes.STRING,
        // get: function () {
        //   return JSON.parse(this.getDataValue("detail2"));
        // },
        // set: function (value) {
        //   return this.setDataValue("detail2", JSON.stringify(value));
        // },
      },
      detail3: {
        type: DataTypes.STRING,
        // get: function () {
        //   return JSON.parse(this.getDataValue("detail3"));
        // },
        // set: function (value) {
        //   return this.setDataValue("detail3", JSON.stringify(value));
        // },
      },
      detail4: {
        type: DataTypes.STRING,
        // get: function () {
        //   return JSON.parse(this.getDataValue("detail4"));
        // },
        // set: function (value) {
        //   return this.setDataValue("detail4", JSON.stringify(value));
        // },
      },
      detail5: {
        type: DataTypes.STRING,
        // get: function () {
        //   return JSON.parse(this.getDataValue("detail5"));
        // },
        // set: function (value) {
        //   return this.setDataValue("detail5", JSON.stringify(value));
        // },
      },
      packaged: {
        type: DataTypes.STRING,
        // get: function () {
        //   return JSON.parse(this.getDataValue("packaged"));
        // },
        // set: function (value) {
        //   return this.setDataValue("packaged", JSON.stringify(value));
        // },
      },
      digital_assets: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("digital_assets"));
        },
        set: function (value) {
          return this.setDataValue("digital_assets", JSON.stringify(value));
        },
      },
      snapshot: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("snapshot"));
        },
        set: function (value) {
          return this.setDataValue("snapshot", JSON.stringify(value));
        },
      },
    },
    {
      sequelize,
      modelName: "SalsifyGlobalProduct",
    }
  );
  return SalsifyGlobalProduct;
};
