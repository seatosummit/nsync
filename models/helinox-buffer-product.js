"use strict";
const { Model } = require("sequelize");
require("pg").defaults.parseInt8 = true;
module.exports = (sequelize, DataTypes) => {
  class HelinoxBufferProduct extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  HelinoxBufferProduct.init(
    {
      sku: DataTypes.STRING,
      name: DataTypes.STRING,
      shopify_product_id: DataTypes.BIGINT,
      shopify_variant_id: DataTypes.BIGINT,
    },
    {
      sequelize,
      modelName: "HelinoxBufferProduct",
    }
  );
  return HelinoxBufferProduct;
};
