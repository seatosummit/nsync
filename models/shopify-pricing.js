"use strict";
const { Model } = require("sequelize");
require("pg").defaults.parseInt8 = true;
module.exports = (sequelize, DataTypes) => {
  class ShopifyPricing extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  ShopifyPricing.init(
    {
      shopify_product_id: DataTypes.STRING,
      shopify_variant_id: DataTypes.STRING,
      parent_sku: DataTypes.STRING,
      variant_sku: DataTypes.STRING,
      variant_ean: DataTypes.STRING,
      price_retail: DataTypes.DOUBLE,
      price_warehouse: DataTypes.DOUBLE,
      price_special: DataTypes.DOUBLE,
      special_price_startdate: DataTypes.DATE,
      special_price_enddate: DataTypes.DATE,
    },
    {
      sequelize,
      modelName: "ShopifyPricing",
    }
  );
  return ShopifyPricing;
};
