'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class PepperiOrder extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  PepperiOrder.init({
    submittedAt: DataTypes.DATE,
    ownerEmail: DataTypes.STRING,
    salesPerson: DataTypes.STRING,
    pepperiCustomerID: DataTypes.INTEGER,
    wmsCardID: DataTypes.INTEGER,
    pepperiCustomerName: DataTypes.STRING,
    pepperiOrderInternalID: DataTypes.INTEGER,
    xmlExported: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
    },
    customerEmail: DataTypes.STRING,
    status: DataTypes.INTEGER,
    statusName: DataTypes.STRING,
    WMSStatus: DataTypes.STRING,
    PONumber: DataTypes.STRING,
    InvoiceNumber: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    SAL_ID: {
      type: DataTypes.INTEGER,
      defaultValue: 0,
    },
    orderLines: {
      type: DataTypes.TEXT('long'),
        get: function() {
          return JSON.parse(this.getDataValue("orderLines"));
        },
        set: function(value) {
          return this.setDataValue("orderLines", JSON.stringify(value));
        }
    },
    shipTo: {
      type: DataTypes.TEXT('long'),
        get: function() {
          return JSON.parse(this.getDataValue("shipTo"));
        },
        set: function(value) {
          return this.setDataValue("shipTo", JSON.stringify(value));
        }
    },
    orderSnapshot: {
      type: DataTypes.TEXT('long'),
        get: function() {
          return JSON.parse(this.getDataValue("orderSnapshot"));
        },
        set: function(value) {
          return this.setDataValue("orderSnapshot", JSON.stringify(value));
        }
    }
  }, {
    sequelize,
    modelName: 'PepperiOrder',
  });
  return PepperiOrder;
};