"use strict";
const { Model } = require("sequelize");
require("pg").defaults.parseInt8 = true;
module.exports = (sequelize, DataTypes) => {
  class BrandfolderBufferLabel extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  BrandfolderBufferLabel.init(
    {
      label_id: {
        type: DataTypes.STRING,
      },
      name: {
        type: DataTypes.STRING,
      },
      brandfolder_id: {
        type: DataTypes.STRING,
      },
      path: {
        type: DataTypes.JSON,
      },
      depth: {
        type: DataTypes.INTEGER,
      },
      snapshot: {
        type: DataTypes.JSON,
      },
    },
    {
      sequelize,
      modelName: "BrandfolderBufferLabel",
    }
  );
  return BrandfolderBufferLabel;
};
