"use strict";
const { Model } = require("sequelize");
require("pg").defaults.parseInt8 = true;
module.exports = (sequelize, DataTypes) => {
  class ShopifyBufferProductVariant extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  ShopifyBufferProductVariant.init(
    {
      shopify_id: DataTypes.BIGINT,
      shopify_product_id: DataTypes.BIGINT,
      shopify_title: DataTypes.STRING,
      shopify_price: {
        type: DataTypes.DOUBLE,
        get: function () {
          return this.getDataValue("shopify_price").toString();
        },
        set: function (value) {
          return this.setDataValue("shopify_price", parseFloat(value));
        },
      },
      shopify_compare_at_price: {
        type: DataTypes.DOUBLE,
        get: function () {
          return this.getDataValue("shopify_compare_at_price").toString();
        },
        set: function (value) {
          return this.setDataValue(
            "shopify_compare_at_price",
            parseFloat(value)
          );
        },
      },
      shopify_sku: DataTypes.STRING,
      shopify_position: DataTypes.INTEGER,
      shopify_inventory_policy: DataTypes.STRING,
      shopify_fulfillment_service: DataTypes.STRING,
      shopify_inventory_management: DataTypes.STRING,
      shopify_option1: DataTypes.STRING,
      shopify_option2: DataTypes.STRING,
      shopify_option3: DataTypes.STRING,
      shopify_taxable: DataTypes.STRING,
      shopify_barcode: DataTypes.STRING,
      shopify_grams: DataTypes.DOUBLE,
      shopify_weight: DataTypes.DOUBLE,
      shopify_weight_unit: DataTypes.STRING,
      shopify_image_id: DataTypes.BIGINT,
      shopify_inventory_item_id: DataTypes.BIGINT,
      shopify_inventory_quantity: DataTypes.INTEGER,
      shopify_old_inventory_quantity: DataTypes.INTEGER,
      shopify_requires_shipping: DataTypes.BOOLEAN,
      shopify_admin_graphql_api_id: DataTypes.STRING,
      salsify_system_id: DataTypes.STRING,
      salsify_parent_sku: DataTypes.STRING,
      salsify_size_variant: DataTypes.STRING,
      salsify_colour_variant: DataTypes.STRING,
      salsify_laymen_colour: DataTypes.STRING,
      erp_b2b_enabled: DataTypes.BOOLEAN,
      erp_b2c_enabled: DataTypes.BOOLEAN,
      shopify_snapshot: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("shopify_snapshot"));
        },
        set: function (value) {
          return this.setDataValue("shopify_snapshot", JSON.stringify(value));
        },
      },
      salsify_snapshot: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("shopify_snapshot"));
        },
        set: function (value) {
          return this.setDataValue("shopify_snapshot", JSON.stringify(value));
        },
      },
    },
    {
      sequelize,
      modelName: "ShopifyBufferProductVariant",
    }
  );
  return ShopifyBufferProductVariant;
};
