"use strict";
const { Model } = require("sequelize");
require("pg").defaults.parseInt8 = true;
module.exports = (sequelize, DataTypes) => {
  class BlackDiamondBufferImage extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  BlackDiamondBufferImage.init(
    {
      salsify_digital_asset_id: DataTypes.STRING,
      salsify_filename: DataTypes.STRING,
      salsify_format: DataTypes.STRING,
      salsify_url: DataTypes.STRING,
      salsify_parent_skus: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("salsify_parent_skus"));
        },
        set: function (value) {
          return this.setDataValue(
            "salsify_parent_skus",
            JSON.stringify(value)
          );
        },
      },
      salsify_variant_skus: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("salsify_variant_skus"));
        },
        set: function (value) {
          return this.setDataValue(
            "salsify_variant_skus",
            JSON.stringify(value)
          );
        },
      },
      salsify_primary_image_ids: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("salsify_primary_image_ids"));
        },
        set: function (value) {
          return this.setDataValue(
            "salsify_primary_image_ids",
            JSON.stringify(value)
          );
        },
      },
      salsify_additional_image_ids: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("salsify_additional_image_ids"));
        },
        set: function (value) {
          return this.setDataValue(
            "salsify_additional_image_ids",
            JSON.stringify(value)
          );
        },
      },
      salsify_packaging_image_ids: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("salsify_packaging_image_ids"));
        },
        set: function (value) {
          return this.setDataValue(
            "salsify_packaging_image_ids",
            JSON.stringify(value)
          );
        },
      },
      salsify_lifestyle_image_ids: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("salsify_lifestyle_image_ids"));
        },
        set: function (value) {
          return this.setDataValue(
            "salsify_lifestyle_image_ids",
            JSON.stringify(value)
          );
        },
      },
      shopify_id: DataTypes.BIGINT,
      shopify_product_id: DataTypes.BIGINT,
      shopify_variant_ids: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("shopify_variant_ids"));
        },
        set: function (value) {
          return this.setDataValue(
            "shopify_variant_ids",
            JSON.stringify(value)
          );
        },
      },
      shopify_src: DataTypes.STRING,
      position: DataTypes.INTEGER,
      main_image: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
      },
      encoded_version: {
        type: DataTypes.TEXT("long"),
      },
      snapshot: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("snapshot"));
        },
        set: function (value) {
          return this.setDataValue("snapshot", JSON.stringify(value));
        },
      },
    },
    {
      sequelize,
      modelName: "BlackDiamondBufferImage",
    }
  );
  return BlackDiamondBufferImage;
};
