"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class ProductVariant extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      ProductVariant.belongsTo(models.Product);
      ProductVariant.hasMany(models.ProductVariantEnhancement);
    }
  }
  ProductVariant.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      ProductId: DataTypes.UUID,
      name: DataTypes.STRING,
      sku: DataTypes.STRING,
      ean: DataTypes.STRING,
      asin: DataTypes.STRING,
      salsifyProductId: DataTypes.STRING,
      erpProductId: DataTypes.STRING,
      size: DataTypes.STRING,
      colour: DataTypes.STRING,
      modelVariant: DataTypes.STRING,
      colourLayman: DataTypes.STRING,
      description: DataTypes.TEXT,
      minOrderQty: DataTypes.INTEGER,
      erpSnapshot: DataTypes.JSON,
      stockRefillETA: DataTypes.DATE,
      brand: DataTypes.STRING,
      parent_sku: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "ProductVariant",
    }
  );
  return ProductVariant;
};
