"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Product extends Model {
    static associate(models) {
      Product.hasMany(models.ProductVariant);
      Product.hasMany(models.ProductEnhancement);
    }
  }
  Product.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      sku: DataTypes.STRING,
      salsifyProductId: DataTypes.STRING,
      name: DataTypes.STRING,
      description: DataTypes.TEXT,
      brand: DataTypes.STRING,
      gender: DataTypes.STRING,
      modelVariant: DataTypes.STRING,
      erp_category: DataTypes.STRING,
      erp_sub_category: DataTypes.STRING,
      tech_specs: DataTypes.JSON,
      usps: DataTypes.JSON,
    },
    {
      sequelize,
      modelName: "Product",
    }
  );
  return Product;
};
