"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class PricingEnhancement extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      PricingEnhancement.belongsTo(models.PriceList);
      PricingEnhancement.belongsTo(models.PricingUpdate);
      PricingEnhancement.belongsTo(models.SalesChannel);
      PricingEnhancement.belongsTo(models.Pricing);
      PricingEnhancement.belongsTo(models.ProductVariantEnhancement);
    }
  }
  PricingEnhancement.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      SalesChannelId: DataTypes.UUID,
      PricingId: DataTypes.UUID,
      PriceListId: DataTypes.UUID,
      PricingUpdateId: DataTypes.UUID,
      ProductVariantEnhancementId: DataTypes.UUID,
      amount: DataTypes.DOUBLE,
      quantity: DataTypes.INTEGER,
      sku: DataTypes.STRING,
      price_list_slug: DataTypes.STRING,
      valid: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "PricingEnhancement",
    }
  );
  return PricingEnhancement;
};
