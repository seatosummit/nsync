"use strict";
const { Model } = require("sequelize");
require("pg").defaults.parseInt8 = true;
module.exports = (sequelize, DataTypes) => {
  class BlackDiamondBufferProduct extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  BlackDiamondBufferProduct.init(
    {
      shopify_product_id: DataTypes.BIGINT,
      sku: DataTypes.STRING,
      salsify_system_id: DataTypes.STRING,
      title: DataTypes.STRING,
      body_html: DataTypes.TEXT("long"),
      vendor: DataTypes.STRING,
      gender: DataTypes.STRING,
      usps: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("usps"));
        },
        set: function (value) {
          return this.setDataValue("usps", JSON.stringify(value));
        },
      },
      metafields: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("metafields"));
        },
        set: function (value) {
          return this.setDataValue("metafields", JSON.stringify(value));
        },
      },
      tech_specs: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("tech_specs"));
        },
        set: function (value) {
          return this.setDataValue("tech_specs", JSON.stringify(value));
        },
      },
      tags: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("tags"));
        },
        set: function (value) {
          return this.setDataValue("tags", JSON.stringify(value));
        },
      },
      categories: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("categories"));
        },
        set: function (value) {
          return this.setDataValue("categories", JSON.stringify(value));
        },
      },
      status: DataTypes.ENUM("active", "archived", "draft"),
      admin_graphql_api_id: DataTypes.STRING,
      options: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("options"));
        },
        set: function (value) {
          return this.setDataValue("options", JSON.stringify(value));
        },
      },
      variants: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("variants"));
        },
        set: function (value) {
          return this.setDataValue("variants", JSON.stringify(value));
        },
      },
      snapshot: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("snapshot"));
        },
        set: function (value) {
          return this.setDataValue("snapshot", JSON.stringify(value));
        },
      },
    },
    {
      sequelize,
      modelName: "BlackDiamondBufferProduct",
    }
  );
  return BlackDiamondBufferProduct;
};
