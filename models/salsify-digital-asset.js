"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class SalsifyDigitalAsset extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  SalsifyDigitalAsset.init(
    {
      salsify_organization_id: DataTypes.STRING,
      salsify_id: DataTypes.STRING,
      salsify_system_id: DataTypes.STRING,
      salsify_url: DataTypes.STRING,
      salsify_format: DataTypes.STRING,
      salsify_brand: DataTypes.STRING,
      salsify_name: DataTypes.STRING,
      salsify_filename: DataTypes.STRING,
      salsify_status: DataTypes.STRING,
      au_office_additional_ordering: DataTypes.INTEGER,
      salsify_asset_height_pixels: DataTypes.INTEGER,
      salsify_asset_width_pixels: DataTypes.INTEGER,
      salsify_primary_skus: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("salsify_primary_skus"));
        },
        set: function (value) {
          return this.setDataValue(
            "salsify_primary_skus",
            JSON.stringify(value)
          );
        },
      },
      salsify_additional_skus: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("salsify_additional_skus"));
        },
        set: function (value) {
          return this.setDataValue(
            "salsify_additional_skus",
            JSON.stringify(value)
          );
        },
      },
      salsify_packaging_skus: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("salsify_packaging_skus"));
        },
        set: function (value) {
          return this.setDataValue(
            "salsify_packaging_skus",
            JSON.stringify(value)
          );
        },
      },
      salsify_au_ids: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("salsify_au_ids"));
        },
        set: function (value) {
          return this.setDataValue("salsify_au_ids", JSON.stringify(value));
        },
      },
      salsify_usa_ids: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("salsify_usa_ids"));
        },
        set: function (value) {
          return this.setDataValue("salsify_usa_ids", JSON.stringify(value));
        },
      },
      snapshot: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("snapshot"));
        },
        set: function (value) {
          return this.setDataValue("snapshot", JSON.stringify(value));
        },
      },
    },
    {
      sequelize,
      modelName: "SalsifyDigitalAsset",
    }
  );
  return SalsifyDigitalAsset;
};
