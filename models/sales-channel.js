"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class SalesChannel extends Model {
    static associate(models) {
      SalesChannel.hasOne(models.SalesChannelIntegrationRuleset);
      SalesChannel.hasMany(models.SalesChannelCollection);
      SalesChannel.hasMany(models.ProductVariantEnhancement);
      SalesChannel.hasMany(models.ProductEnhancement);
    }
  }
  SalesChannel.init(
    {
      id: {
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
        primaryKey: true,
      },
      name: DataTypes.STRING,
      machineName: DataTypes.STRING,
      description: DataTypes.STRING,
      shopify_config: {
        type: DataTypes.TEXT("long"),
        get: function () {
          return JSON.parse(this.getDataValue("shopify_config"));
        },
        set: function (value) {
          return this.setDataValue("shopify_config", JSON.stringify(value));
        },
      },
    },
    {
      sequelize,
      modelName: "SalesChannel",
    }
  );
  return SalesChannel;
};
