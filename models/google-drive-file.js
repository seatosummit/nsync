'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class GoogleDriveFile extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  GoogleDriveFile.init({
    drive_id: DataTypes.STRING,
    name: DataTypes.STRING,
    type: DataTypes.STRING,
    parent: DataTypes.STRING,
    list: {
        type: DataTypes.TEXT('long'),
        get: function() {
            return JSON.parse(this.getDataValue("list"));
        },
        set: function(value) {
            return this.setDataValue("list", JSON.stringify(value));
        }
    }
  }, {
    sequelize,
    modelName: 'GoogleDriveFile',
  });
  return GoogleDriveFile;
};