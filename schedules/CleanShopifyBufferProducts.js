const {
  CleanBufferProducts,
} = require("../modules/central-buffer/BufferOperations");
const { Logger } = require("../modules/helpers/Logger");
(async () => {
  Logger.info("Cleaning Shopify products buffer...");
  await CleanBufferProducts();
  Logger.info("...done.");
})();
