const {
  updatePricingEnhancementsBuffer,
} = require("../modules/central-buffer/CentralBufferSchedule");
const { Logger } = require("../modules/helpers/Logger");
const {
  updatePricingToSparkLayer,
} = require("../modules/sparklayer/SparkLayerSchedule");
(async () => {
  try {
    // TODO: Eventually we'll use pricingEnhancements for everything, so B2BDIST won't be hardcoded anymore
    await updatePricingToSparkLayer({
      machineNameOfSalesChannel: "B2BDIST",
    });
  } catch (error) {
    Logger.error(`Problem with SparkLayer pricing update: ${error}`);
  }
})();
