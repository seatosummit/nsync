const {
  BDProductBufferSync,
} = require("../modules/blackdiamond/schedules/BDProductSchedules");
(async () => {
  await BDProductBufferSync();
  process.exit();
})();
