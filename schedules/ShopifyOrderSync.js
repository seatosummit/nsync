const { exportShopifyOrders } = require("../modules/shopify/orders/ShopifyOrderSchedule");
(async () => {
    await exportShopifyOrders();
})();