const { updateAssetsBuffer } = require("../modules/brandfolder/assets/assets");
const { updateLabelsBuffer } = require("../modules/brandfolder/labels/labels");
const { Logger } = require("../modules/helpers/Logger");

(async () => {
  Logger.info("Syncing STS Brandfolder buffer...");
  await updateLabelsBuffer("q7cphz-8wbqqw-1dru9z");
  await updateAssetsBuffer(30, "q7cphz-8wbqqw-1dru9z");
  Logger.info("Syncing DIST Brandfolder buffer...");
  await updateLabelsBuffer("jgs64snkkfgvxkvm454vsmrb");
  await updateAssetsBuffer(2, "jgs64snkkfgvxkvm454vsmrb");
  Logger.info("...done.");
  process.exit();
})();
