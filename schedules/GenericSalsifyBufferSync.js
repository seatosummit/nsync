const {
  syncAustraliaSalsifyBuffer,
} = require("../modules/salsify/products/productbuffer");
(async () => {
  await syncAustraliaSalsifyBuffer();
})();
