const {
  convertNewlyAddedAssets,
} = require("../modules/brandfolder/transition");
(async () => {
  console.info("Convert newly added BF assets...");
  await convertNewlyAddedAssets();
  console.info("...done.");
})();
