const {
  syncSalsifyProductsToDB,
  syncSalsifyProductParentsToDB,
  syncGlobalSalsifyBuffer,
} = require("../modules/salsify/products/productbuffer");
(async () => {
  console.info("Syncing Salsify products...");
  await syncSalsifyProductsToDB();
  await syncSalsifyProductParentsToDB();
  console.info("Syncing Global Salsify products...");
  await syncGlobalSalsifyBuffer();
  console.info("...done.");
  process.exit();
})();
