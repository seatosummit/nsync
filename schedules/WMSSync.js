const {
  updateWMSProductBuffer,
} = require("../modules/wms/products/productbuffer");
(async () => {
  console.info("Syncing WMS products...");
  await updateWMSProductBuffer({ filterOnlyActiveProducts: false });
  console.info("...done.");
  process.exit();
})();
