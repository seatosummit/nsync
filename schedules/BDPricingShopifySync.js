const {
  BDShopifyPriceStoreSync,
} = require("../modules/blackdiamond/schedules/BDProductSchedules");
(async () => {
  await BDShopifyPriceStoreSync();
  process.exit();
})();
