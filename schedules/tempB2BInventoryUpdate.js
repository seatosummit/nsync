const {
  refreshAllVariants,
} = require("../modules/central-buffer/CentralBufferSchedule");
const {
  bulkUpdateInventoryToShopify,
} = require("../modules/shopify/store/ShopifyStoreSchedule");
(async () => {
  await refreshAllVariants({
    machineNameOfSalesChannel: "B2BDIST",
    refreshVariants: true,
    refreshProductEnhancements: false,
    refreshPricingBuffer: false,
    updateOnlyEnabled: false,
  });
  await bulkUpdateInventoryToShopify({
    machineNameOfSalesChannel: "B2BDIST",
    dryRun: false,
  });
})();
