const {
  CleanBufferProductVariants,
} = require("../modules/central-buffer/BufferOperations");
const { Logger } = require("../modules/helpers/Logger");
(async () => {
  Logger.info("Cleaning Shopify product variants buffer...");
  await CleanBufferProductVariants();
  Logger.info("...done.");
})();
