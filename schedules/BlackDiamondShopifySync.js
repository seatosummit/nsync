const {
  BDShopifyPriceStoreSync,
  BDInventorySync,
  BDShopifyProductSync,
} = require("../modules/blackdiamond/schedules/BDProductSchedules");
const { Logger } = require("../modules/helpers/Logger");
(async () => {
  Logger.info("Syncing BD products...");
  await BDShopifyProductSync({ newProductsOnly: true });
  Logger.info("Syncing BD pricing...");
  await BDShopifyPriceStoreSync();
  Logger.info("Syncing BD inventory...");
  await BDInventorySync();
  process.exit();
})();
