const {
  refreshAllVariants,
} = require("../modules/central-buffer/CentralBufferSchedule");
const {
  bulkUpdateInventoryToShopify,
  bulkUpdatePricingToShopify,
} = require("../modules/shopify/store/ShopifyStoreSchedule");
(async () => {
  await refreshAllVariants({
    refreshVariants: true,
    refreshProductEnhancements: true,
    refreshPricingBuffer: true,
    updateOnlyEnabled: false,
  });
  await bulkUpdateInventoryToShopify();
  await bulkUpdatePricingToShopify();
})();
