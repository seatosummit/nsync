const {
  updateWMSProductBuffer,
} = require("../modules/wms/products/productbuffer");
const {
  BDShopifyPriceBufferSync,
  BDShopifyPriceStoreSync,
  BDInventorySync,
  BDProductBufferSync,
  BDVariantBufferSync,
} = require("../modules/blackdiamond/schedules/BDProductSchedules");
(async () => {
  await updateWMSProductBuffer({ filterOnlyActiveProducts: false });
  await BDProductBufferSync();
  await BDVariantBufferSync();
  await BDShopifyPriceBufferSync();
  await BDInventorySync();
  await BDShopifyPriceStoreSync();
})();
