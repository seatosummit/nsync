const {
  tagOrdersWithNoStock,
} = require("../modules/shopify/orders/ShopifyOrderSchedule");
(async () => {
  await tagOrdersWithNoStock();
})();
