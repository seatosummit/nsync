const {
  syncProductsFromWMSUsingBuffer,
} = require("../modules/pepperi/products/product");
const {
  updateProductStockLevels,
} = require("../modules/pepperi/products/productstock");
const { syncCustomers } = require("../modules/pepperi/customers/customer");

(async () => {
  console.info("Syncing WMS customers to Pepperi...");
  await syncCustomers();
  console.info("Syncing WMS products to Pepperi...");
  await syncProductsFromWMSUsingBuffer(false);
  console.info("Syncing Pepperi product stock levels...");
  await updateProductStockLevels();
  console.info("...done.");
  process.exit();
})();
