const {
  bulkUpdateInventoryToShopify,
} = require("../modules/shopify/store/ShopifyStoreSchedule");
(async () => {
  await bulkUpdateInventoryToShopify();
})();
