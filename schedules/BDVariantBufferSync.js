const {
  BDVariantBufferSync,
} = require("../modules/blackdiamond/schedules/BDProductSchedules");
(async () => {
  await BDVariantBufferSync();
  process.exit();
})();
