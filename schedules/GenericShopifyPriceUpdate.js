const {
  bulkUpdatePricingToShopify,
} = require("../modules/shopify/store/ShopifyStoreSchedule");
(async () => {
  await bulkUpdatePricingToShopify();
})();
