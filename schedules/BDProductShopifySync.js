const {
  BDShopifyProductSync,
} = require("../modules/blackdiamond/schedules/BDProductSchedules");
(async () => {
  await BDShopifyProductSync();
  process.exit();
})();
