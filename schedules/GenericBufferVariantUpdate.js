const {
  refreshAllVariants,
  updatePricingEnhancementsBuffer,
} = require("../modules/central-buffer/CentralBufferSchedule");
(async () => {
  await refreshAllVariants({
    refreshVariants: true,
    refreshProductEnhancements: true,
    refreshPricingBuffer: true,
    updateOnlyEnabled: false,
  });
  // Make sure the SalesChannelIntegrationRuleset is set to usePricingEnhancements
  await updatePricingEnhancementsBuffer();
})();
