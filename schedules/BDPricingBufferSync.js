const {
  BDShopifyPriceBufferSync,
} = require("../modules/blackdiamond/schedules/BDProductSchedules");
(async () => {
  await BDShopifyPriceBufferSync();
  process.exit();
})();
