const {
  BDShopifyPriceBufferSync,
  BDBufferCollectionSync,
  BDProductBufferSync,
  BDBufferCategoryDefinitions,
  BDVariantBufferSync,
} = require("../modules/blackdiamond/schedules/BDProductSchedules");
(async () => {
  console.info("Syncing BD Product buffer...");
  await BDProductBufferSync();
  console.info("Syncing BD pricing buffer...");
  await BDShopifyPriceBufferSync();
  console.info("Syncing BD collection buffer...");
  await BDBufferCollectionSync();
  console.info("Syncing BD category definitions...");
  await BDBufferCategoryDefinitions();
  console.info("Syncing BD variants...");
  await BDVariantBufferSync();
  console.info("...done");
  process.exit();
})();
