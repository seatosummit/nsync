const {
  BDInventorySync,
} = require("../modules/blackdiamond/schedules/BDProductSchedules");
(async () => {
  await BDInventorySync();
  process.exit();
})();
