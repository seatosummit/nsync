const { Logger } = require("../modules/helpers/Logger");
const {
  importShopifyShipments,
} = require("../modules/shopify/shipments/ShopifyShipmentSchedule");
(async () => {
  try {
    await importShopifyShipments();
  } catch (error) {
    Logger.error(`Problem with Shopify shipments import: ${error}`);
  }
})();
