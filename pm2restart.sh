#!/bin/bash
pm2 stop all
pm2 delete all
pm2 start --only 'salsify-products-to-buffer, salsify-product-parents-to-buffer, update-wms-productbuffer'
pm2 start --only 'bd-variant-buffer, bd-pricing-buffer, bd-product-buffer'
sleep 5m
pm2 start --only 'bd-pricing-shopify, bd-orders-export-from-shopify'
sleep 30m
pm2 start --only 'bd-inventory-shopify'
sleep 30m
pm2 start --only 'bd-product-shopify'
sleep 6h
pm2 start --only 'pepperi-update-productimages'
pm2 save --force
