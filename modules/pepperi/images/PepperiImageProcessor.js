const fetch = require("node-fetch");
const { promises: fsp } = require("fs");
const fs = require("fs");
const sharp = require("sharp");
const pathTools = require("path");
const _ = require("lodash");
require("dotenv").config();
const {
  BrandfolderBufferAsset,
  BrandfolderBufferAttachment,
  PepperiBufferImage,
} = require("../../../models");
const { FTPTools } = require("../../helpers/ftpTools");
const { Logger } = require("../../helpers/Logger");
const { deleteFile, writeFile } = require("../../helpers/fileTools");
const { SequelizeOperations } = require("../../sequelize/SequelizeOperations");
const {
  getAllRelevantProductImages,
} = require("../../helpers/BrandfolderTools");

class PepperiImageProcessor {
  constructor() {
    this.productImages = [];
    this.pepperiImages = [];
    this.updatePromises = [];
    this.SFTP = new FTPTools({
      host: process.env.DATA_FTP_HOST,
      user: process.env.DATA_FTP_USER,
      password: process.env.DATA_FTP_PASSWORD,
    });
    this.SFTPRemotePath = process.env.PEPPERI_IMAGES_LOCATION_REMOTE;
  }

  async updateBuffer() {
    this.productImages = await getAllRelevantProductImages({});
    await this.getAllPepperiImages();
    this.updatePromises = [];

    try {
      for (const currentImageAsset of this.productImages) {
        if (currentImageAsset.au_skus.length === 0) continue;
        if (currentImageAsset.BrandfolderBufferAttachments.length == 0)
          continue;
        const pepperiImageInfo = this.extractInfo(currentImageAsset);
        for (const pepperiImageEntry of pepperiImageInfo) {
          this.updatePromises.push(
            SequelizeOperations.Upsert(
              pepperiImageEntry,
              "filename",
              PepperiBufferImage,
              this.pepperiImages
            )
          );
        }
      }
      const pepperiUpdateResults = await Promise.all(this.updatePromises);
      const actualUpdates = pepperiUpdateResults.filter(
        (updatePromise) => updatePromise
      );
      Logger.info(
        `Updated ${actualUpdates.length} out of ${pepperiUpdateResults.length} Pepperi images in buffer.`
      );
    } catch (error) {
      Logger.error(`Error updating Pepperi image buffer: ${error}`);
    }
  }

  extractInfo(imageAsset) {
    const imageInfo = [];
    if (["jpg", "png", "bmp", "tiff"].includes(imageAsset.type)) {
      const original_url = imageAsset.BrandfolderBufferAttachments[0].url;
      const skus = imageAsset.au_skus;
      for (const sku of skus) {
        let filename;
        const formattedSKU = sku.trim().replace(/[\\\/]+/gm, "-");
        if (imageAsset.au_office_image_ordering) {
          const imageOrdering =
            parseInt(imageAsset.au_office_image_ordering) + 1;
          filename = `${formattedSKU}_${imageOrdering}.jpg`;
        } else {
          filename = `${formattedSKU}_1.jpg`;
        }
        const pepperiBufferInfo = {
          BrandfolderBufferAssetId: imageAsset.id,
          sku: formattedSKU,
          filename,
          original_url,
          brand: imageAsset.brand,
          //uploadedToSFTP: true,
        };
        imageInfo.push(pepperiBufferInfo);
      }
    }
    return imageInfo;
  }

  async updatePepperiImagesFromBuffer() {
    await this.getAllPepperiImages({ filterNotUploaded: true });
    await this.SFTP.connect();
    for (const currentImage of this.pepperiBufferImages) {
      try {
        const processedImage = await this.processBufferImage(currentImage);
        await this.uploadAndDelete(processedImage);
        await currentImage.update({ uploadedToSFTP: true });
        Logger.info(`Processed image asset ${currentImage.id}`);
      } catch (error) {
        Logger.error(
          `Error processing image asset ${currentImage.id}: ${error}`
        );
      }
    }
    Logger.info(
      `Processed, uploaded and deleted ${this.pepperiBufferImages.length} images`
    );
  }

  async getAllPepperiImages({ filterNotUploaded = false }) {
    const query = {};
    if (filterNotUploaded)
      query.where = {
        uploadedToSFTP: false,
      };
    this.pepperiBufferImages = await PepperiBufferImage.findAll(query);
  }

  async uploadAndDelete(imageToProcess) {
    try {
      await this.SFTP.uploadOne(imageToProcess);
      Logger.info(`Uploaded file ${imageToProcess.sourcePath}`);
      const fileDeleteSuccess = deleteFile(imageToProcess.sourcePath);
      if (!fileDeleteSuccess) throw `Error deleting file`;
      Logger.info(`Deleted file ${imageToProcess.sourcePath}`);
    } catch (error) {
      throw `Problem with uploading and deleting ${imageToProcess.sourcePath}: ${error}`;
    }
  }

  async processBufferImage(bufferImage) {
    try {
      if (!bufferImage.original_url)
        throw `No URL provided for buffer image ${bufferImage.id}`;
      const response = await fetch(bufferImage.original_url);
      const buffer = await response.buffer();
      const localFilePath = `${process.env.PEPPERI_IMAGES_LOCATION}${bufferImage.filename}`;
      await sharp(buffer)
        .resize(800)
        .toFormat("jpg")
        .jpeg({ mozjpeg: true })
        .toFile(localFilePath);
      const remotePath = `${this.SFTPRemotePath}${bufferImage.filename}`;
      return { sourcePath: localFilePath, remotePath };
    } catch (error) {
      throw `Error saving image for asset ${bufferImage.id}: ${error}`;
    }
  }

  async processAllSkusRelatedToAsset(imageAsset) {
    if (["jpg", "png", "bmp", "tiff"].includes(imageAsset.type)) {
      try {
        const url = imageAsset.BrandfolderBufferAttachments[0].url;
        const skus = imageAsset.au_skus;
        const response = await fetch(url);
        const buffer = await response.buffer();
        const filePaths = [];
        const imageBuffer = await sharp(buffer)
          .resize(800)
          .toFormat("jpg")
          .jpeg({ mozjpeg: true })
          .toBuffer();
        for (const sku of skus) {
          let filename;
          if (imageAsset.au_office_image_ordering) {
            const imageOrdering = parseInt(imageAsset.au_office_image_ordering);
            filename = `${sku}_${imageOrdering}.jpg`;
          } else {
            filename = `${sku}.jpg`;
          }
          const remotePath = `${this.SFTPRemotePath}${filename
            .trim()
            .replace(/\\/gm, "-")}`;
          const filePath = `${process.env.PEPPERI_IMAGES_LOCATION}${filename
            .trim()
            .replace(/\\/gm, "-")}`;

          if (fs.existsSync(filePath)) {
            Logger.info(
              `Image ${pathTools.basename(filePath)} already exists, will skip`
            );
          } else {
            await writeFile(filePath, imageBuffer);
            filePaths.push({ source: filePath, remotePath });
          }
        }
        return filePaths;
      } catch (error) {
        throw `Error saving image for asset ${imageAsset.id}: ${error}`;
      }
    } else {
      throw `Image asset ID ${imageAsset.id} has incompatible image format (${imageAsset.type})`;
    }
  }
}

module.exports.PepperiImageProcessor = PepperiImageProcessor;
