const { Logger } = require("../../helpers/Logger");
const { PepperiImageProcessor } = require("./PepperiImageProcessor");

const updatePepperiImagesBuffer = async () => {
  try {
    Logger.info(`Updating Pepperi images buffer.`);
    const pepperiProcessor = new PepperiImageProcessor();
    await pepperiProcessor.updateBuffer();
    Logger.info(`Done updating Pepperi images from buffer.`);
    process.exit();
  } catch (error) {
    Logger.error(`Error updating Pepperi images buffer: ${error}`);
  }
};

const updateImagesToPepperiFromBuffer = async () => {
  try {
    Logger.info(`Updating images to Pepperi from buffer.`);
    const pepperiProcessor = new PepperiImageProcessor();
    await pepperiProcessor.updatePepperiImagesFromBuffer();
    Logger.info(`Done updating images to Pepperi from buffer.`);
    process.exit();
  } catch (error) {
    Logger.error(`Error updating Pepperi images: ${error}`);
  }
};

module.exports.updatePepperiImagesBuffer = updatePepperiImagesBuffer;
module.exports.updateImagesToPepperiFromBuffer =
  updateImagesToPepperiFromBuffer;
