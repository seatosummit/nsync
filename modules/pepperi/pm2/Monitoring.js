// TODO: this needs to be swapped out for
// some other monitoring solution. TBD
// PM2 is great, but it's not what we want.
// look at Heroku KPI dashboards
// const io = require("@pm2/io");

class Monitoring {
  constructor() {
    // this.shopifyAvailableConnections = io.metric({
    //   name: "Available Shopify API Quota",
    //   id: "app/realtime/available-shopify-connections",
    // });
    // this.shopifyAPICallAmounts = io.meter({
    //   name: "Shopify API Calls",
    //   id: "app/realtime/shopify-api-calls",
    // });
    // this.skusWithoutPricing = io.meter({
    //   name: "No pricing found for SKU",
    //   id: "app/realtime/skus-with-no-pricing",
    // });
    // this.orphanSalsifyProducts = io.meter({
    //   name: "Orphan Salsify products with no SKU match in ERP",
    //   id: "app/realtime/orphan-salsify-products",
    // });
    // this.shopifyProductNotFound = io.meter({
    //   name: "Tried to update a Shopify product that doesn't exist",
    //   id: "app/realtime/shopify-products-not-found",
    // });
    // this.tooManyShopifyAPIRequests = io.meter({
    //   name: "Shopify API responding with 429: Too Many Requests",
    //   id: "app/realtime/shopify-api-too-many-requests",
    // });
  }
  // sendShopifyAPICallStats(shopifyThrottleMessage) {
  //   // Shopify throttle format: { remaining: 14, current: 26, max: 40 }
  //   this.shopifyAvailableConnections.set(shopifyThrottleMessage.remaining);
  //   this.shopifyAPICallAmounts.mark();
  // }
  // sendSKUWithoutPricing() {
  //   this.skusWithoutPricing.mark();
  // }
  // sendOrphanSalsifyProduct() {
  //   this.orphanSalsifyProducts.mark();
  // }
  // sendShopifyProductNotFound() {
  //   this.shopifyProductNotFound.mark();
  // }
  // sendTooManyShopifyAPIRequests() {
  //   this.tooManyShopifyAPIRequests.mark();
  // }
}

module.exports.Monitoring = Monitoring;
