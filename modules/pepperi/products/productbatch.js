const fetch = require("node-fetch");
const _ = require("lodash");
const { prepareHeaders } = require("../../helpers/apitools");
const { Logger } = require("../../helpers/Logger");
const { deletePepperiItem } = require("../../helpers/pepperiAPI");
require("dotenv").config();
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

module.exports = {
  batchDeleteArrayOfSkus: async (arrayOfSkus) => {
    try {
      const headers = await prepareHeaders();

      for (const sku of arrayOfSkus) {
        deletePepperiItem(headers, sku);
      }
    } catch (error) {
      Logger.error(`Problem deleting: ${error}`);
    }
  },

  batchUpdatePepperiProducts: async (newProductObjectArray, headers) => {
    /**
        Headers: [
            "BrandExternalID", --> Brand
            "ExternalID", -->SKU
            "Price",
"            "SecondaryPrice",
"            "FutureAvailabilityDate",
            "Prop1",
            "Name",
            "Size",
            "Colour",
            "Prop2",
            "Prop3",
            "Prop4",
            "Prop5",
            "Prop6",
            "Dimension1Name",
            "Dimension2Name",
            ],
        */

    const payloadForBatchCreate = {
      Headers: [
        "BrandExternalID",
        "ExternalID",
        "Price",
        "SecondaryPrice",
        "FutureAvailabilityDate",
        "Prop1",
        "Prop7", // Formatted future availability date
        "TSAItemInStock",
        "Hidden",
        "UPC",
        "Name",
        "Dimension1Code",
        "Dimension2Code",
        "Prop2",
        "Prop3",
        "Prop4",
        "Prop5",
        "Prop6",
        "Dimension1Name",
        "Dimension2Name",
      ],
      Lines: [...newProductObjectArray],
    };

    const response = await fetch(
      `https://api.pepperi.com/v1.0/bulk/items/json`,
      {
        method: "POST",
        headers: headers.PepperiHeaders,
        body: JSON.stringify(payloadForBatchCreate),
      }
    );

    if (response.status === 400) {
      console.error(response.statusText);
      const badRequestBody = await response.json();
      console.error(badRequestBody.items.sku[0]);
    }
    if (response.status === 405) {
      console.error(response.statusText);
      const badRequestBody = await response.json();
      console.error(badRequestBody.items.sku[0]);
    }
    if (response.status === 401) {
      console.error(response.statusText);
      const unauthRequestBody = await response.json();
      console.error(unauthRequestBody);
    }

    if (response.status === 500) {
      console.error(response.statusText);
      const unauthRequestBody = await response.json();
      console.error(unauthRequestBody);
    }

    if (response.status === 200) {
      const requestBody = await response.json();
      const URI = requestBody.URI;
      // console.log(`Successfully set off a batch creation of Products in Pepperi. Here's the progress status: https://api.pepperi.com/v1.0${URI}`);
    }
    return response;
  },

  batchUpdatePepperiInventoryStatus: async (inventoryObjectArray, headers) => {
    try {
      const payloadForBatchInventoryUpdate = {
        Headers: ["ItemExternalID", "InStockQuantity"],
        Lines: [...inventoryObjectArray],
      };
      const response = await fetch(
        `https://api.pepperi.com/v1.0/bulk/inventory/json`,
        {
          method: "POST",
          headers: headers.PepperiHeaders,
          body: JSON.stringify(payloadForBatchInventoryUpdate),
        }
      );
      const requestBody = await response.json();
      if (response.status === 200) {
        const URI = requestBody.URI;
        // console.log(`Successfully set off a batch update of Inventory in Pepperi. Here's the progress status: https://api.pepperi.com/v1.0${URI}`);
        return response;
      } else {
        throw `Error with batch updates: ${requestBody}`;
      }
    } catch (error) {
      throw `Error with batch updates: ${error}`;
    }
  },

  batchUpdatePepperiStockETAInfoFromWMS: async (
    productObjectArray,
    headers
  ) => {
    // Date format example : 2017-04-03Z

    const payloadForBatchCreate = {
      Headers: ["BrandExternalID", "ExternalID", "FutureAvailabilityDate"],
      Lines: [...productObjectArray],
    };

    // console.log(payloadForBatchCreate.Headers);

    const response = await fetch(
      `https://api.pepperi.com/v1.0/bulk/items/json`,
      {
        method: "POST",
        headers: headers.PepperiHeaders,
        body: JSON.stringify(payloadForBatchCreate),
      }
    );

    if (response.status === 200) {
      const requestBody = await response.json();
      const URI = requestBody.URI;
      // console.log(`Successfully set off a batch update of Product stock ETAs in Pepperi. Here's the progress status: https://api.pepperi.com/v1.0${URI}`);
    } else {
      console.error(response.statusText);
      const unauthRequestBody = await response.json();
      console.error(unauthRequestBody);
    }
    return response;
  },
};
