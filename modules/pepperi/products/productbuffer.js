const fetch = require("node-fetch");
require("dotenv").config();
const { prepareHeaders } = require("../../helpers/apitools");
const { promisify } = require("util");
const sleep = promisify(setTimeout);
const { Product } = require("../../../models");
let APIHeaders = {};
const io = require("@pm2/io");
const amountOfPepperiProducts = io.metric({
  name: "Total amount of Products in Pepperi",
});
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

module.exports = {
  updateProductBuffer: async () => {
    try {
      APIHeaders = await prepareHeaders(process.env.PEPPERI_SECOND_API_KEY);
      console.log("API headers created");
    } catch (error) {
      console.error("Unable to create API headers", error);
    }
    try {
      await module.exports.handlePepperiProducts(APIHeaders);
      console.log("... product buffer sync done");
    } catch (error) {
      console.error(`Error syncing products: ${error}`);
    }
  },

  batchDeleteProductBufferEntries: async (arrayOfPepperiProducts) => {
    const arrayOfProductSKUs = arrayOfPepperiProducts.map(
      (product) => product.sku
    );
    await Product.destroy({
      where: {
        sku: arrayOfProductSKUs,
      },
    });
    console.log(
      `Removed these product SKUs from buffer DB: ${arrayOfProductSKUs.join()}`
    );
  },

  deleteProductBufferEntryWithSKU: async (pepperiProduct) => {
    await Product.destroy({
      where: {
        sku: pepperiProduct.sku,
      },
    });
  },

  saveProductBufferEntry: async (pepperiProduct) => {
    const oldOnesFound = await Product.findAll({
      where: {
        sku: pepperiProduct.sku,
      },
    });
    if (oldOnesFound.length === 0) {
      await Product.create(pepperiProduct);
      console.log(
        `Added new product buffer for SKU ${pepperiProduct.sku} - ${pepperiProduct.name}`
      );
    } else {
      const currentProductStockQty = pepperiProduct.stockqty
        ? pepperiProduct.stockqty
        : 0;
      const updateResultDirect = await oldOnesFound[0].update({
        stockqty: currentProductStockQty,
        ProductID: pepperiProduct.ProductID,
      });
      if (updateResultDirect instanceof Product) {
        console.log(
          `Updated Buffer for SKU ${pepperiProduct.sku} (${pepperiProduct.name})`
        );
      } else {
        console.log(
          `Problem Updating Buffer for SKU ${pepperiProduct.sku} (${pepperiProduct.name})`
        );
      }
    }
  },

  handlePepperiProducts: async (
    headers,
    url = "/api/latest/items?limit=100",
    products = []
  ) => {
    try {
      const waitBetweenUpdates = process.env.UPDATE_BATCH_WAIT_MS
        ? parseInt(process.env.UPDATE_BATCH_WAIT_MS)
        : 60000;
      console.log(`Querying Pepperi API: ${url}`);
      const response = await fetch(`https://app.pepperi.com${url}`, {
        headers: headers.PepperiHeaders,
      });
      if (response.status === 429) {
        console.error(
          `We're hitting Pepperi API limits on retrieving Products`
        );
        setTimeout(
          await module.exports.handlePepperiProducts(headers, url, products),
          60000
        );
      }
      if (response.status !== 200) {
        console.error(`${response.status}: ${response.statusText}`);
      }
      const productsJSON = await response.json();
      amountOfPepperiProducts.set(productsJSON.meta.total_count);
      products = productsJSON.objects;

      const productSavePromises = products.map((product) => {
        return module.exports.saveProductBufferEntry({
          objID: product.objID,
          name: product.name,
          sku: product.sku,
          stockqty: product.stockqty,
          ProductID: product.productid,
        });
      });

      await Promise.all(productSavePromises).catch((e) =>
        console.error(`Error syncing this batch of products: ${e}`)
      );

      if (productsJSON.meta.next) {
        await sleep(waitBetweenUpdates / 4);
        await module.exports.handlePepperiProducts(
          headers,
          productsJSON.meta.next
        );
      }
    } catch (error) {
      console.error(`Error syncing Pepperi Products: ${error}.`);
      await sleep(
        await module.exports.handlePepperiProducts(headers, url, products),
        60000
      );
    }
  },
};
