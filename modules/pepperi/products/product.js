const fetch = require("node-fetch");
const _ = require("lodash");
require("dotenv").config();
const { promisify } = require("util");
const sleep = promisify(setTimeout);
const { prepareHeaders } = require("../../helpers/apitools");
const { SalsifyProduct, WMSProduct } = require("../../../models");
const { updateWMSProductBuffer } = require("../../wms/products/productbuffer");
const {
  getProductsFromSalsifyWithPage,
} = require("../../salsify/products/product");
const {
  mergeSalsifyAndWMSProductsForPepperiBulk,
  WMSToPepperiBulkProduct,
  WMSBufferToPepperiBulkProduct,
} = require("./productdecorator");
const {
  batchUpdatePepperiProducts,
  batchDeletePepperiProducts,
  batchDeleteArrayOfSkus,
} = require("./productbatch");
const { getProductsFromWMS } = require("../../wms/products/products");
const { Logger } = require("../../helpers/Logger");
const { getPepperiItemsWithPage } = require("../../helpers/pepperiAPI");
const { skuIsZCoded } = require("../../helpers/stringTools");

// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

module.exports = {
  syncProductsFromSalsify: async () => {
    try {
      const headers = await prepareHeaders();
      APIHeaders = headers;
      const productUpdateBatchSize = parseInt(
        process.env.PRODUCT_UPDATE_BATCH_SIZE
      );
      let pepperiProductsToBeCreated = [];

      let salsifyProductResults = await getProductsFromSalsifyWithPage(
        1,
        headers
      );
      pepperiProductsToBeCreated.push(...salsifyProductResults.products);

      // while (salsifyProductResults.paging.nextPage < 1) {
      while (
        salsifyProductResults.paging.nextPage <
        salsifyProductResults.paging.totalPages + 1
      ) {
        salsifyProductResults = await getProductsFromSalsifyWithPage(
          salsifyProductResults.paging.nextPage,
          headers
        );
        pepperiProductsToBeCreated.push(...salsifyProductResults.products);
      }

      Logger.info(
        `There are ${pepperiProductsToBeCreated.length} new products to be created in Pepperi.`
      );

      await batchUpdatePepperiProducts(salsifiedProducts, headers);
    } catch (error) {
      Logger.error(`Error syncing products from Salsify to Pepperi, ${error}`);
      return;
    }
  },

  purgePepperiProductsUsingWMSBuffer: async () => {
    try {
      const headers = await prepareHeaders();
      const allWMSProducts = (await getProductsFromWMS(headers))
        .filter((product) => {
          const isNotZcoded = !skuIsZCoded(product.sku);
          const isActive = product.status === "ACTIVE";
          return isNotZcoded && isActive;
        })
        .map((product) => product.sku);
      const pepperiProductsToBeDeleted = [];
      let currentPage = 1;
      let pepperiProducts = await getPepperiItemsWithPage(headers, currentPage);
      while (pepperiProducts.length > 0) {
        for (const pepperiProduct of pepperiProducts) {
          const sku = pepperiProduct.ExternalID;
          const matchingSKUInWMS = _.find(allWMSProducts, { sku });
          if (matchingSKUInWMS) {
            Logger.info(`Found SKU ${sku} in WMS`);
          } else {
            Logger.info(
              `Did not find SKU ${sku} in WMS. This needs to be deleted.`
            );
            pepperiProductsToBeDeleted.push(sku);
          }
        }
        currentPage++;
        pepperiProducts = await getPepperiItemsWithPage(headers, currentPage);
      }

      // await batchDeleteArrayOfSkus(pepperiProductsToBeDeleted);
    } catch (error) {
      Logger.error(`Problem with cleanup: ${error}`);
    }
  },

  syncProductsFromWMS: async (filter = true) => {
    try {
      const headers = await prepareHeaders();
      const allWMSProducts = await getProductsFromWMS(headers, filter);
      if (allWMSProducts.status === 500) {
        throw `Error getting WMS products: ${allWMSProducts.message}`;
      }
      if (Array.isArray(allWMSProducts) && allWMSProducts.length === 0) {
        throw `Error getting WMS products: ${allWMSProducts.message}`;
      }
      if (!allWMSProducts) {
        throw `Error getting WMS products: ${allWMSProducts.message}`;
      }

      const allSalsifyBufferProducts = await SalsifyProduct.findAll({
        raw: true,
      });

      let WMSProductsToBeSynced = [];

      for (index in allWMSProducts) {
        const currentWMSProduct = allWMSProducts[index];
        let productToBeDecorated = currentWMSProduct;

        productToBeDecorated = WMSToPepperiBulkProduct(productToBeDecorated);

        const foundMatchingBufferProduct = _.find(allSalsifyBufferProducts, [
          "sku",
          currentWMSProduct.sku,
        ]);
        productToBeDecorated = mergeSalsifyAndWMSProductsForPepperiBulk(
          productToBeDecorated,
          foundMatchingBufferProduct,
          currentWMSProduct
        );

        WMSProductsToBeSynced.push(productToBeDecorated);
      }

      Logger.info(
        `Sending an update request to ${WMSProductsToBeSynced.length} products in Pepperi.`
      );

      await batchUpdatePepperiProducts(WMSProductsToBeSynced, headers);
    } catch (error) {
      Logger.error(`Error syncing products from WMS to Pepperi, ${error}`);
      return;
    }
  },

  batchDeleteProductsFromPepperi: async (arrayOfProducts = []) => {
    const arrayOfSkus = [
      "360SSB1000DM",
      "360SSB1000LI",
      "360SSB1000PM",
      "360SSB1000TQ",
      "360SSB550DM",
      "360SSB550LI",
      "360SSB550MTBK",
      "360SSB550PM",
      "360SSB550TQ",
      "360SSB750DM",
      "360SSB750DM",
      "360SSB750PM",
    ];

    await batchDeleteArrayOfSkus(arrayOfSkus);
  },

  syncProductsFromWMSUsingBuffer: async (filter = true) => {
    try {
      const headers = await prepareHeaders();
      const allWMSBufferProducts = await WMSProduct.findAll({
        raw: true,
      });

      const allSalsifyBufferProducts = await SalsifyProduct.findAll({
        raw: true,
      });

      let WMSProductsToBeSynced = [];
      let enabledProductsCount = 0;

      for (const currentWMSProduct of allWMSBufferProducts) {
        let productToBeDecorated = currentWMSProduct;

        if (currentWMSProduct.pepperiEnabled == 1) {
          enabledProductsCount++;
        }

        productToBeDecorated =
          WMSBufferToPepperiBulkProduct(productToBeDecorated);

        const foundMatchingBufferProduct = _.find(allSalsifyBufferProducts, [
          "sku",
          currentWMSProduct.sku,
        ]);
        productToBeDecorated = mergeSalsifyAndWMSProductsForPepperiBulk(
          productToBeDecorated,
          foundMatchingBufferProduct,
          currentWMSProduct
        );

        WMSProductsToBeSynced.push(productToBeDecorated);
      }

      Logger.info(
        `Sending an update request to ${WMSProductsToBeSynced.length} products in Pepperi.`
      );
      Logger.info(
        `Currently ${enabledProductsCount} products set to Pepperi Enabled.`
      );

      await batchUpdatePepperiProducts(WMSProductsToBeSynced, headers);
    } catch (error) {
      Logger.error(`Error syncing products from WMS to Pepperi, ${error}`);
      return;
    }
  },
};
