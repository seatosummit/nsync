const fetch = require("node-fetch");
require("dotenv").config();
const { prepareHeaders } = require("../../helpers/apitools");
const { Category } = require("../../../models");
let categoriesToSave = [];
const io = require("@pm2/io");
const amountOfCategories = io.metric({
  name: "Total amount of Project Categories",
});
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

module.exports = {
  syncCategories: async () => {
    const headers = await prepareHeaders(process.env.PEPPERI_MAIN_API_KEY);
    try {
      await module.exports.handlePepperiCategories(headers);
      amountOfCategories.set(categoriesToSave.length);
      for (index in categoriesToSave) {
        try {
          const category = categoriesToSave[index];
          const oldOnesFound = await Category.findAll({
            where: {
              uuid: category.uuid,
            },
          });
          if (oldOnesFound.length === 0) {
            const sequelizeCategory = await Category.create(category);
            // console.log(`Created new category ${sequelizeCategory.name}`);
          } else {
            console.log(
              `skipped new category ${category.name}, already exists`
            );
          }
        } catch (error) {
          console.error(`Problems: ${error}`);
        }
      }
    } catch (error) {
      console.error(`Error syncing categories: ${error}`);
    }
  },

  handlePepperiCategories: async (
    headers,
    url = "/api/latest/categories?limit=100",
    categories = []
  ) => {
    try {
      const response = await fetch(`https://app.pepperi.com${url}`, {
        headers: headers.PepperiHeaders,
      });
      if (response.status === 429) {
        console.error(
          "Categories fetch timed out. Check the frequency of your API calls."
        );
        setTimeout(
          await module.exports.handlePepperiCategories(
            headers,
            url,
            categories
          ),
          60000
        );
      }
      if (response.status !== 200) {
        throw `${response.status}: ${response.statusText}`;
      }
      const categoriesJSON = await response.json();
      categories = categories.concat(categoriesJSON.objects);
      if (categoriesJSON.meta.next) {
        await module.exports.handlePepperiCategories(
          headers,
          categoriesJSON.meta.next,
          categories
        );
      } else {
        categoriesToSave = categories.map((category) => {
          return {
            objID: category.objID,
            name: category.name,
            description: category.description,
            category_position: category.categoryPosition,
            pepperi_id: category.id,
            uuid: category.uuid,
          };
        });
        return true;
      }
    } catch (error) {
      console.error(`Error syncing Pepperi Categories: ${error}`);
    }
  },
};
