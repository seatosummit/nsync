const path = require("path");
const _ = require("lodash");
const { prepareHeaders } = require("../../helpers/apitools");
const { SalsifyProduct } = require("../../../models");
const { getProductsFromWMS } = require("../../wms/products/products");

const {
  saveImageWithSKUFilename,
} = require("../../salsify/products/productimages");
const { salsifyDigitalAssetToPepperiImage } = require("./productdecorator");

module.exports = {
  processDigitalAssets: async (digitalAssetsArray, sku) => {
    if (digitalAssetsArray && digitalAssetsArray.length > 0) {
      let decoratedDigitalAssetsArray = [];
      let filePathsForCurrentDigitalAssets = [];
      for (const [index, assetToBeConverted] of digitalAssetsArray.entries()) {
        decoratedDigitalAssetsArray.push(
          salsifyDigitalAssetToPepperiImage(assetToBeConverted, sku)
        );
      }
      for (const [
        index,
        decoratedDigitalAsset,
      ] of decoratedDigitalAssetsArray.entries()) {
        filePathsForCurrentDigitalAssets.push(
          await saveImageWithSKUFilename(decoratedDigitalAsset, index + 1)
        );
      }
      return filePathsForCurrentDigitalAssets;
    }
  },

  syncRelevantSalsifyImages: async (cursor) => {
    const headers = await prepareHeaders();
    const allWMSProducts = await getProductsFromWMS(headers);
    const allSalsifyBufferProducts = await SalsifyProduct.findAll({
      raw: true,
    });

    for (const [index, currentWMSProduct] of allWMSProducts.entries()) {
      const matchingBufferProduct = _.find(allSalsifyBufferProducts, [
        "sku",
        currentWMSProduct.sku,
      ]);
      if (matchingBufferProduct) {
        // console.log(`found matching buffer product ${currentWMSProduct.sku}`);
        const filePathsForCurrentProduct =
          await module.exports.processDigitalAssets(
            JSON.parse(matchingBufferProduct["digital_assets"]),
            currentWMSProduct.sku
          );
      }
    }
  },
};
