const fetch = require("node-fetch");
const _ = require("lodash");
const moment = require("moment");
require("dotenv").config();
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

module.exports = {
  salsifyDigitalAssetToPepperiImage: (asset, sku) => {
    const imageObject = {
      salsifyId: asset["salsify:id"],
      sku: sku.replace(/\//g, "-"),
      URL: asset["salsify:url"],
      fileExtension: asset["salsify:format"],
      updatedAt: asset["salsify:updated_at"],
    };
    return imageObject;
  },

  WMSBufferToPepperiBulkProduct: (productObject) => {
    // https://auspost.com.au/mypost/track/#/details/DGJ2052696
    // tracking ID test

    /**
    Headers: [
        "BrandExternalID",
        "ExternalID",
        "Price"
        "SecondaryPrice"
        "FutureAvailabilityDate"
        "Prop7" -> future avail date, formatted
        "TSAItemInStock"
        "Hidden" --> show/hide product in Pep,
        "UPC"
        ],
    */

    //Date input format example : 30/06/2021 12:00:00 AM
    //Date target format example : 2017-04-03Z
    //moment('24/12/2019 09:15:00', "DD MM YYYY hh:mm:ss");

    let stockETA;
    let formattedStockETA;

    if (productObject["stockETA"]) {
      stockETA = moment(productObject["stockETA"]).format("YYYY-MM-DD");
      formattedStockETA = moment(productObject["stockETA"]).format(
        "Do MMM YYYY"
      );
    } else {
      stockETA = null;
      formattedStockETA = "--";
    }

    const inStock = productObject["stockQuantity"]
      ? Number.parseFloat(productObject["stockQuantity"]) > 0
      : false;
    const inStockText = inStock
      ? `${productObject["stockQuantity"]} in stock`
      : "Out of stock";
    const inStockColour = inStock ? "#4ceb34" : "#ff0000";
    const TSAItemInStock = `<strong style="color: ${inStockColour}">${inStockText}</strong>`;

    const regex = /^\D*/gm;
    const wholesalePrice = productObject["unitPrice"];
    const retailPrice = productObject["retailPrice"];
    const sku = productObject["sku"].replace(/\//g, "-");
    const PepperiHidden = productObject["pepperiEnabled"] == 1 ? false : true;

    const pepperiProduct = [
      productObject["brand"],
      sku,
      retailPrice,
      wholesalePrice,
      stockETA,
      productObject["category"],
      formattedStockETA,
      TSAItemInStock,
      PepperiHidden,
      productObject["barcode"],
    ];
    return pepperiProduct;
  },

  WMSToPepperiBulkProduct: (productObject) => {
    // https://auspost.com.au/mypost/track/#/details/DGJ2052696
    // tracking ID test

    /**
        Headers: [
            "BrandExternalID",
            "ExternalID",
            "Price"
            "SecondaryPrice"
            "FutureAvailabilityDate"
            "Prop7" -> future avail date, formatted
            ],
        */

    //Date input format example : 30/06/2021 12:00:00 AM
    //Date target format example : 2017-04-03Z
    //moment('24/12/2019 09:15:00', "DD MM YYYY hh:mm:ss");

    let stockETA;
    let formattedStockETA;

    if (productObject["Promise Date"]) {
      stockETA = moment(
        productObject["Promise Date"],
        "DD MM YYYY hh:mm:ss"
      ).format("YYYY-MM-DD");
      formattedStockETA = moment(
        productObject["Promise Date"],
        "DD MM YYYY hh:mm:ss"
      ).format("Do MMM YYYY");
    } else {
      stockETA = null;
      formattedStockETA = "Not available";
    }

    const inStock = productObject["stockqty"]
      ? Number.parseFloat(productObject["stockqty"]) > 0
      : false;
    const inStockText = inStock
      ? `${productObject["stockqty"]} in stock`
      : "Out of stock";
    const inStockColour = inStock ? "#4ceb34" : "#ff0000";
    const TSAItemInStock = `<strong style="color: ${inStockColour}">${inStockText}</strong>`;

    const regex = /^\D*/gm;
    const wholesalePrice = productObject["customfield3"]
      ? Number.parseFloat(productObject["customfield3"].replace(regex, ""))
      : null;
    const retailPrice = productObject["unitprice"]
      ? Number.parseFloat(productObject["unitprice"])
      : null;
    const sku = productObject["sku"].replace(/\//g, "-");

    const pepperiProduct = [
      productObject["brand"],
      sku,
      wholesalePrice,
      retailPrice,
      stockETA,
      productObject["category"],
      formattedStockETA,
      TSAItemInStock,
    ];
    return pepperiProduct;
  },

  mergeSalsifyAndWMSProductsForPepperiBulk: (
    WMSProduct,
    salsifyBufferProduct,
    currentWMSProduct
  ) => {
    /*  Sequelize Model:
     *  sku: DataTypes.STRING,
        name: DataTypes.STRING,
        size: DataTypes.STRING,
        colour: DataTypes.STRING,
        erp_tier1_financial_category: DataTypes.STRING,
        erp_category: DataTypes.STRING,
        erp_sub_category: DataTypes.STRING,
        b2b_category: DataTypes.STRING,
        b2b_sub_category: DataTypes.STRING
     */

    /**
        Headers: [
            --- WMS BASED INFO
            "BrandExternalID", --> Brand
            "ExternalID", -->SKU
            "Price"
            "SecondaryPrice"
            "FutureAvailabilityDate",
            "Prop1",
            "Prop7", // formatted future avail date
            "TSAItemInStock" // custom in stock message
            "Hidden", // whether this product is active
            "UPC", // barcode
            --- SALSIFY BASED INFO
            "Name",
            "Dimension1Code",
            "Dimension2Code",
            "Prop2", -> tier 1 financial category
            "Prop3", -> erp category
            "Prop4", -> erp sub category
            "Prop5", -> b2b category
            "Prop6", -> b2b sub category
            "Dimension1Name",
            "Dimension2Name",
            ],
        */

    let combinedProduct = [];

    if (salsifyBufferProduct) {
      combinedProduct = [
        ...WMSProduct,
        salsifyBufferProduct.name,
        salsifyBufferProduct.colour,
        salsifyBufferProduct.size,
        salsifyBufferProduct.erp_tier1_financial_category,
        salsifyBufferProduct.erp_category,
        salsifyBufferProduct.erp_sub_category,
        salsifyBufferProduct.b2b_category,
        salsifyBufferProduct.b2b_sub_category,
        "Colour",
        "Size",
      ];
    } else {
      combinedProduct = [
        ...WMSProduct,
        currentWMSProduct.name,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        "Colour",
        "Size",
      ];
    }

    return combinedProduct;
  },

  colourDimensionOne: (WMSProduct, salsifyBufferProduct) => {
    /**
        Headers: [
            --- SALSIFY BASED INFO
            "Dimension1Code",
            "Dimension1Name",
            ],
        */

    let combinedProduct = [];

    if (salsifyBufferProduct.colour) {
      dimension1Entry = [salsifyBufferProduct.colour, "Colour"];
    } else {
      dimension1Entry = [];
    }

    return dimension1Entry;
  },

  sizeDimensionTwo: (salsifyBufferProduct) => {
    /**
        Headers: [
            --- SALSIFY BASED INFO
            "Dimension1Code",
            "Dimension1Name",
            ],
        */

    let combinedProduct = [];

    if (salsifyBufferProduct.size) {
      dimension2Entry = [salsifyBufferProduct.size, "Size"];
    } else {
      dimension2Entry = [];
    }
    return dimension2Entry;
  },

  WMSToPepperiBulkStockDetails: (productObject) => {
    /**
         * Headers: [
                "ExternalID",
                "Prop1",
                "CaseQuantity",
                "Price"
            ],
         */

    const pepperiProduct = [productObject["sku"], productObject["stockqty"]];
    return pepperiProduct;
  },

  WMSToPepperiSingle: (productObject) => {
    const pepperiProduct = {
      Name: productObject["name"],
      ExternalID: productObject["sku"],
      MainCategoryID: productObject["brand"],
      Prop8: productObject["category"],
      Prop9: productObject["brand"],
      CaseQuantity: Number.parseFloat(productObject["PerUnitQty"]),
      Price: Number.parseFloat(productObject["unitprice"]),
    };
    return pepperiProduct;
  },
};
