const { prepareHeaders } = require("../../helpers/apitools");
const { WMSToPepperiBulkStockDetails } = require("./productdecorator");
const { batchUpdatePepperiInventoryStatus } = require("./productbatch");
const { getProductsFromWMS } = require("../../wms/products/products");

// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

module.exports = {
  updateProductStockLevels: async () => {
    try {
      const headers = await prepareHeaders();
      const allWMSProducts = await getProductsFromWMS(headers);
      let inventoryUpdates = [];
      for (const productToBeDecorated of allWMSProducts) {
        inventoryUpdates.push(
          WMSToPepperiBulkStockDetails(productToBeDecorated)
        );
      }
      await batchUpdatePepperiInventoryStatus(inventoryUpdates, headers);
    } catch (error) {
      console.error(
        `Error updating stock level for products from WMS, ${error}`
      );
    }
  },
};
