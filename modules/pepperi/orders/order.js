const fetch = require("node-fetch");
require("dotenv").config();
const { prepareHeaders } = require("../../helpers/apitools");
const { promisify } = require("util");
const sleep = promisify(setTimeout);
const { PepperiOrder, WMSProduct } = require("../../../models");
const { saveXMLFile, createOrderXML } = require("./orderxmltools_unified");
const { FTPTools } = require("../../helpers/ftpTools");
const { writeFile } = require("../../helpers/fileTools");
const { pepperiToBufferOrder } = require("./orderdecorator");
const mockOrderData = require("./orderdata.json");
const mockLineItemsData = require("./lineitemsdata.json");
let APIHeaders = {};
const moment = require("moment");
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

module.exports = {
  updatePepperiOrdersFromWMS: async () => {
    try {
      await sequelize.authenticate();
      APIHeaders = await prepareHeaders(process.env.PEPPERI_MAIN_API_KEY);
      // console.log("Local Order DB connected");
      // console.log("API headers created");
    } catch (error) {
      console.error(
        "Unable to connect to Local Order DB or create API headers:",
        error
      );
    }
  },

  updateOrderBufferFromWMS: async () => {
    // TODO: Get all orders from WMS. The orders listing should have
    // order ID (this should be the order ID in WMS as well)
    // line items, each with shipping status
    try {
      //await sequelize.authenticate();
      // console.log("Local Order DB connected");
    } catch (error) {
      console.error("Unable to connect to Local Order DB:", error);
    }
    try {
      APIHeaders = await prepareHeaders();
      // console.log("API headers created");
    } catch (error) {
      console.error("Unable to create API headers", error);
    }
    try {
      const ordersFromWMS = await module.exports.getOrdersFromWMS(APIHeaders);
      const orderLineItemsFromWMS =
        await module.exports.getOrderLineItemsFromWMS(APIHeaders);
      // console.log("... order buffer sync done");
    } catch (error) {
      console.error(`Error syncing orders: ${error}`);
    }

    try {
      await module.exports.updateAllXMLExports();
      // console.log("... saving XML files done");
    } catch (error) {
      console.error(`Error saving XML files: ${error}`);
    }
  },

  updateOrderBufferAllProducts: async () => {
    try {
      await sequelize.authenticate();
      // console.log("Local Order DB connected");
    } catch (error) {
      console.error("Unable to connect to Local Order DB:", error);
    }
    try {
      APIHeaders = await prepareHeaders(process.env.PEPPERI_MAIN_API_KEY);
      // console.log("API headers created");
    } catch (error) {
      console.error("Unable to create API headers", error);
    }
    try {
      await module.exports.updatePepperiOrdersBuffer(
        APIHeaders,
        `/api/latest/orders?status=Confirmed`
      );
      // console.log("... order buffer sync done");
    } catch (error) {
      console.error(`Error syncing orders: ${error}`);
    }

    try {
      await module.exports.updateAllXMLExports();
      // console.log("... saving XML files done");
    } catch (error) {
      console.error(`Error saving XML files: ${error}`);
    }
  },

  exportOrdersToXML: async () => {
    try {
      const FTPClient = await new FTPTools({
        host: process.env.DATA_FTP_HOST,
        user: process.env.DATA_FTP_USER,
        password: process.env.DATA_FTP_PASSWORD,
      });
      APIHeaders = await prepareHeaders(process.env.PEPPERI_MAIN_API_KEY);
      const ordersFromPepperi = await module.exports.getOrdersFromPepperi(
        APIHeaders
      );
      const orderSavePromises = ordersFromPepperi.map((order) => {
        return module.exports.saveOrderBufferEntry(pepperiToBufferOrder(order));
      });

      await Promise.all(orderSavePromises).catch((e) =>
        console.error(`Error syncing this batch of orders: ${e}`)
      );

      const XMLOrders = await module.exports.updateAllXMLExports();
      await module.exports.sendXMLFiles(XMLOrders, FTPClient);
      console.log("... saving XML files done");
      console.log("... order buffer sync done");
    } catch (error) {
      console.error(`Error syncing orders: ${error}`);
    }
  },

  saveOrderBufferEntry: async (pepperiOrder) => {
    const oldOnesFound = await PepperiOrder.findAll({
      where: {
        pepperiOrderInternalID: pepperiOrder.pepperiOrderInternalID,
      },
    });
    if (oldOnesFound.length === 0) {
      try {
        await PepperiOrder.create(pepperiOrder);
        // console.log(
        //   `Added new buffer entry for order ID ${pepperiOrder.pepperiOrderInternalID}`
        // );
      } catch (error) {
        console.error(
          `Error creating new buffer entry for order ID ${pepperiOrder.pepperiOrderInternalID}`
        );
      }
    } else {
      try {
        await PepperiOrder.update(pepperiOrder, {
          where: {
            pepperiOrderInternalID: pepperiOrder.pepperiOrderInternalID,
          },
        });
        console.info(
          `Order ${pepperiOrder.pepperiOrderInternalID} already exists in the buffer, updated`
        );
      } catch (error) {
        console.error(
          `Error updating new buffer entry for order ID ${pepperiOrder.pepperiOrderInternalID}, error: ${error}`
        );
      }
    }
  },

  overwriteAllXMLExports: async () => {
    const allOrders = await Order.findAll();
    console.log(
      `Found ${allOrders.length} orders to force overwrite and save as XML`
    );
    allOrders.forEach(async (order) => {
      try {
        const orderAsXMLString = await createOrderXML(
          order,
          APIHeaders.WMSHeaders
        );
        saveXMLFile(orderAsXMLString, order.orderID);
      } catch (error) {
        throw `Error saving XML for order ${order.orderID}: ${error}`;
      }
    });
  },

  updateAllXMLExports: async () => {
    const ordersNotYetAsXML = await PepperiOrder.findAll({
      where: {
        xmlExported: false,
        statusName: ["Submitted"],
      },
    });
    const bufferProducts = await WMSProduct.findAll();
    let files = [];
    // console.log(`Found ${ordersNotYetAsXML.length} orders to save as XML`);
    for (const order of ordersNotYetAsXML) {
      try {
        let filePath = `${process.env.XML_SAVE_LOCATION}PEP-#${order.pepperiOrderInternalID}.xml`;
        let remotePath = `${process.env.XML_FTP_LOCATION}PEP-#${order.pepperiOrderInternalID}.xml`;
        let archivePath = `${process.env.XML_ARCHIVE_FTP_LOCATION}PEP-#${order.pepperiOrderInternalID}.xml`;

        const orderAsXMLString = await createOrderXML(
          order,
          APIHeaders.WMSHeaders,
          bufferProducts
        );
        const savedFile = await writeFile(filePath, orderAsXMLString);
        if (savedFile) {
          console.info(
            `Wrote XML file for order ID ${order.pepperiOrderInternalID}`
          );
          files.push({ source: filePath, remotePath: remotePath });
          files.push({ source: filePath, remotePath: archivePath });
          await order.update({ xmlExported: true });
        }
      } catch (error) {
        console.error(
          `Error saving XML for order ${order.pepperiOrderInternalID}: ${error}`
        );
      }
    }

    return files;
  },

  getOrdersFromPepperi: async (headers, orders = []) => {
    const now = moment();
    const twodaysago = now.subtract(20, "days");
    const twodaysagoISO = twodaysago.format("YYYY-MM-DDTHH:mm:ss");
    const twodaysagoEncoded = encodeURIComponent(twodaysagoISO);
    const url = `https://api.pepperi.com/v1.0/transactions?full_mode=true&include_nested=true&page_size=250&where=ActionDateTime%20%3E%3D'${twodaysagoEncoded}Z'`;

    try {
      const waitBetweenUpdates = process.env.UPDATE_BATCH_WAIT_MS
        ? parseInt(process.env.UPDATE_BATCH_WAIT_MS)
        : 60000;
      // console.log(`Querying Pepperi API: ${url}`);
      const response = await fetch(url, { headers: headers.PepperiHeaders });
      if (response.status === 429) {
        console.error(
          "Orders fetch timed out. Check the frequency of your API calls."
        );
        setTimeout(
          await module.exports.updatePepperiOrdersBuffer(headers, url, orders),
          60000
        );
      }
      if (response.status !== 200) {
        console.error(`${response.status}: ${response.statusText}`);
      }
      const ordersJSONArray = await response.json();
      return ordersJSONArray;
    } catch (error) {
      console.error(`Error syncing Pepperi Orders: ${error}. Retrying...`);
      await sleep(
        await module.exports.updatePepperiOrdersBuffer(headers),
        60000
      );
    }
  },

  getOrdersFromWMS: async (headers) => {
    try {
      //TODO: uncomment when this call works
      const response = await fetch(
        `http://${process.env.WMS_API_LOCATION}/json/sql?filter~SELECT%20*%20FROM%20vGTH_AllOrders_PEP`,
        { headers: headers.WMSHeaders, timeout: 0 }
      );
      if (response.status !== 200) {
        throw `response other than 200 - ${response.status}: ${response.statusText}`;
      }
      const responseJSON = await response.json();
      return responseJSON;
    } catch (error) {
      console.error(`Error fetching orders from WMS: ${error}`);
      return {
        status: 500,
        message: error,
      };
    }
  },

  getOrderLineItemsFromWMS: async (headers) => {
    try {
      //TODO: uncomment when this call works
      const response = await fetch(
        `http://${process.env.WMS_API_LOCATION}/json/sql?filter~SELECT%20*%20FROM%20vGTH_OrderLines_PEP`,
        { headers: headers.WMSHeaders, timeout: 0 }
      );
      if (response.status !== 200) {
        throw `response other than 200 - ${response.status}: ${response.statusText}`;
      }
      const responseJSON = await response.json();

      return responseJSON;
    } catch (error) {
      console.error(`Error fetching order line items from WMS: ${error}`);
      return {
        status: 500,
        message: error,
      };
    }
  },

  updatePepperiOrdersBuffer: async (headers, url, orders = []) => {
    try {
      const waitBetweenUpdates = process.env.UPDATE_BATCH_WAIT_MS
        ? parseInt(process.env.UPDATE_BATCH_WAIT_MS)
        : 60000;
      // console.log(`Querying Pepperi API: ${url}`);
      const response = await fetch(
        `https://api.pepperi.com/v1.0/transactions?full_mode=true&include_nested=true`,
        { headers: headers.PepperiHeaders }
      );
      if (response.status === 429) {
        console.error(
          "Orders fetch timed out. Check the frequency of your API calls."
        );
        setTimeout(
          await module.exports.updatePepperiOrdersBuffer(headers, url, orders),
          60000
        );
      }
      if (response.status !== 200) {
        console.error(`${response.status}: ${response.statusText}`);
      }
      const ordersJSON = await response.json();
      // TODO: replace with some other monitoring system
      // amountOfPepperiOrders.set(ordersJSON.meta.total_count);
      orders = ordersJSON.objects;

      const orderSavePromises = orders.map((order) => {
        return module.exports.saveOrderBufferEntry({
          orderID: order.id,
          submittedAt: order.lastEdited,
          ownerEmail: order.owner_email,
          salesPerson: order.ownerName,
          customerID: order.customer ? order.customer.id : "Not defined",
          customerEmail: order.customer
            ? order.customer.email
            : "No customer email",
          objID: order.objID,
          orderLines: order.lines,
          shipTo: order.shipTo,
          orderSnapshot: order,
        });
      });

      await Promise.all(orderSavePromises).catch((e) =>
        console.error(`Error syncing this batch of orders: ${e}`)
      );

      if (ordersJSON.meta.next) {
        await sleep(waitBetweenUpdates / 3);
        await module.exports.updatePepperiOrdersBuffer(
          headers,
          ordersJSON.meta.next
        );
      }
    } catch (error) {
      console.error(`Error syncing Pepperi Orders: ${error}. Retrying...`);
      await sleep(
        await module.exports.updatePepperiOrdersBuffer(headers, url, orders),
        60000
      );
    }
  },
  async sendXMLFiles(xmlFiles, FTPClient) {
    let batchFiles = xmlFiles.reduce(function (a, b) {
      return a.concat(b);
    }, []);
    try {
      return await FTPClient.batchUpload(batchFiles);
    } catch (error) {
      console.error(`Error with FTP upload: ${error}`);
    }
  },
};
