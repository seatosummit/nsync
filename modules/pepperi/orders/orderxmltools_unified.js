const { create } = require("xmlbuilder2");
require("dotenv").config();
const _ = require("lodash");
const { productNameFromWMS } = require("../../helpers/WMSBreezeAPI");
const { confirmSKUNameFromWMSBuffer } = require("../../helpers/WMSBuffer");
const moment = require("moment");
const fs = require("fs");
const order = require("./order");
require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

module.exports = {
  sanitiseOrderObject: (orderObject) => {
    orderObject = orderObject.toJSON();
    _.forIn(orderObject, (value, key) => {
      orderObject[key] = orderObject[key] ? value : "Not set";
    });
    if (orderObject.shipTo) {
      _.forIn(orderObject.shipTo, (value, key) => {
        orderObject.shipTo[key] = orderObject.shipTo[key] ? value : "Not set";
      });
    }
    return orderObject;
  },

  createXMLDirectories: () => {},

  saveXMLFile: (formattedXML, orderID) => {
    const filePath = process.env.XML_SAVE_LOCATION;
    const filePathArchive = process.env.XML_ARCHIVE_SAVE_LOCATION;
    fs.writeFile(
      `${filePath}PEP-#${orderID}.xml`,
      formattedXML,
      { flag: "w" },
      (error, data) => {
        if (error) {
          console.error(`problem saving XML file: ${error}`);
          return false;
        }
        console.info(`Wrote XML file for order ID ${orderID}`);
        return true;
      }
    );

    fs.writeFile(
      `${filePathArchive}PEP-${orderID}-archived.xml`,
      formattedXML,
      { flag: "w" },
      (error, data) => {
        if (error) {
          console.error(`problem saving archive XML file: ${error}`);
          return false;
        }
        console.info(`Wrote archive XML file for order ID ${orderID}`);
        return true;
      }
    );
  },
  createOrderXML: async (orderObject, WMSheaders, bufferProductsArray = []) => {
    const orderObjectWithoutNulls =
      module.exports.sanitiseOrderObject(orderObject);
    const submittedAtISOTimestamp = `${
      moment(orderObjectWithoutNulls.submittedAt).toISOString().split(".")[0]
    }Z`;
    const submittedUnifiedFormat = `${moment(
      orderObjectWithoutNulls.submittedAt
    ).format("YYYY-MM-DDThh:mm:ss")}+`;
    const thisWillBecomeThePurchaseOrderID = `#${orderObjectWithoutNulls.pepperiOrderInternalID}`;
    const orderTotalExGST = orderObjectWithoutNulls.orderSnapshot.GrandTotal;
    const orderTotalInclGST = orderTotalExGST * 1.1;
    const customerName = orderObjectWithoutNulls.orderSnapshot.Account
      ? orderObjectWithoutNulls.orderSnapshot.Account.Data.Prop2
      : "";
    const faxNumber =
      orderObjectWithoutNulls.shipTo.ShipToFax != "Not set"
        ? orderObjectWithoutNulls.shipTo.ShipToFax
        : "";

    let documentRoot = create({ version: "1.0" }).ele("NewDataSet");

    // Create order headers
    documentRoot
      .ele("tREMOTETransHeader")
      .ele("Company_ID")
      .txt("DATAPEL")
      .up()
      .ele("DTD")
      .txt("MAGENTO")
      .up()
      .ele("Identifier")
      .txt("PEPPERI")
      .up()
      .ele("StoreCode")
      .txt("WAREHOUSE")
      .up()
      .ele("PostingDate")
      .txt(submittedUnifiedFormat)
      .up()
      .ele("TransID")
      .txt("")
      .up()
      .ele("TransDate")
      .txt(submittedUnifiedFormat)
      .up()
      .ele("ClosedYN")
      .txt("")
      .up()
      .ele("ClosedBy")
      .txt("")
      .up()
      .ele("SaleType")
      .txt("")
      .up()
      .ele("Status")
      .txt("S")
      .up()
      .ele("Cashier")
      .txt("")
      .up()
      .ele("Salesperson")
      .txt("")
      .up()
      .ele("CustomerID")
      .txt("")
      .up()
      .ele("CardIdentification")
      .txt("")
      .up()
      .ele("MYOBCardName")
      .txt(orderObjectWithoutNulls.pepperiCustomerName)
      .up()
      .ele("Till")
      .txt("")
      .up()
      .ele("OriginalSaleType")
      .txt("")
      .up()
      .ele("TransactionTime")
      .txt("")
      .up()
      .ele("VoidYN")
      .txt("")
      .up()
      .ele("VoidedTransID")
      .att("xml:space", "preserve")
      .txt("")
      .up()
      .ele("DeletedTransaction")
      .txt("")
      .up()
      .ele("Period")
      .txt("")
      .up()
      .ele("WeekNo")
      .txt("")
      .up()
      .ele("WeekStartDate")
      .txt("")
      .up()
      .ele("OriginalTransID")
      .txt(thisWillBecomeThePurchaseOrderID)
      .up()
      .ele("Notes")
      .att("xml:space", "preserve")
      .txt("")
      .up()
      .ele("ShipNote")
      .txt(orderObjectWithoutNulls.orderSnapshot.Remark)
      .up()
      .ele("Change")
      .txt("")
      .up()
      .ele("Sent2Host")
      .txt("")
      .up()
      .ele("Sent2HostDateTime")
      .txt("")
      .up()
      .ele("DueDate")
      .txt("")
      .up()
      .ele("Special1")
      .txt("")
      .up()
      .ele("Special2")
      .att("xml:space", "preserve")
      .txt(thisWillBecomeThePurchaseOrderID)
      .up()
      .ele("Special3")
      .txt("")
      .up()
      .ele("ShippingSKU")
      .txt("FR")
      .up()
      .ele("ShippingMethod")
      .txt("B2Bstandard")
      .up()
      .ele("ShippingNote")
      .txt("Standard Freight")
      .up()
      .ele("ShippingCost")
      .txt("17.6")
      .up()
      .ele("Priority")
      .txt("NORMAL")
      .up()
      .ele("DiscountSKU")
      .txt("")
      .up()
      .ele("DiscountAmount")
      .txt("")
      .up()
      .ele("DiscountNote")
      .txt("")
      .up()
      .ele("SurchargeAmount")
      .txt("")
      .up()
      .ele("SurchargeNote")
      .txt("")
      .up()
      .up()
      .ele("tREMOTETransSaleTenders")
      .ele("Company_ID")
      .txt("")
      .up()
      .ele("StoreCode")
      .txt("WAREHOUSE")
      .up()
      .ele("PostingDate")
      .txt(submittedUnifiedFormat)
      .up()
      .ele("TransID")
      .txt("")
      .up()
      .ele("TenderLine")
      .txt("")
      .up()
      .ele("TenderType")
      .txt("checkmo")
      .up()
      .ele("TenderAmount")
      .txt(orderTotalInclGST)
      .up()
      .ele("TDets1")
      .att("xml:space", "preserve")
      .txt("")
      .up()
      .ele("TDets2")
      .txt("")
      .up()
      .ele("CurrencyCode")
      .txt("AUD")
      .up()
      .ele("ForeignTenderAmount")
      .txt("")
      .up()
      .ele("Search")
      .att("xml:space", "preserve")
      .txt("")
      .up()
      .ele("Sent2Host")
      .txt("")
      .up()
      .ele("Sent2HostDateTime")
      .txt("")
      .up()
      .up()
      .ele("tREMOTETransCustomer")
      .ele("Company_ID")
      .txt("")
      .up()
      .ele("StoreCode")
      .txt("WAREHOUSE")
      .up()
      .ele("PostingDate")
      .txt(submittedUnifiedFormat)
      .up()
      .ele("TransID")
      .txt("")
      .up()
      .ele("Name")
      .txt("")
      .up()
      .ele("Address1")
      .txt(orderObjectWithoutNulls.shipTo.ShipToStreet)
      .up()
      .ele("Address2")
      .att("xml:space", "preserve")
      .txt("")
      .up()
      .ele("Suburb")
      .txt(orderObjectWithoutNulls.shipTo.ShipToCity)
      .up()
      .ele("PostCode")
      .txt(orderObjectWithoutNulls.shipTo.ShipToZipCode)
      .up()
      .ele("FaxNo")
      .att("xml:space", "preserve")
      .txt(faxNumber)
      .up()
      .ele("PhoneNo1")
      .att("xml:space", "preserve")
      .att("xml:space", "preserve")
      .txt("")
      .up()
      .ele("PhoneNo2")
      .att("xml:space", "preserve")
      .txt("")
      .up()
      .ele("EmailAddress")
      .att("xml:space", "preserve")
      .txt(orderObjectWithoutNulls.customerEmail)
      .up()
      .ele("SalesPerson")
      .txt("")
      .up()
      .ele("ContactName1")
      .att("xml:space", "preserve")
      .txt("")
      .up()
      .ele("DateTimeLastChanged")
      .txt("")
      .up()
      .ele("AccountCode")
      .txt("")
      .up()
      .ele("ShipToName")
      .txt(customerName)
      .up()
      .ele("Sent2Host")
      .txt("")
      .up()
      .ele("Sent2HostDateTime")
      .txt("")
      .up();

    // arrange order lines in SKU alphabetical ascending order

    const sortedOrderLines = _.orderBy(
      orderObjectWithoutNulls.orderLines,
      ["ItemExternalID"],
      ["asc"]
    );

    const formatter = new Intl.NumberFormat("en-AU", {
      minimumFractionDigits: 4,
      maximumFractionDigits: 4,
    });

    // Loop through order lines
    for (const [index, orderline] of sortedOrderLines.entries()) {
      const roundedSaleUnitAmount = formatter.format(orderline.UnitPrice * 1.1);
      const roundedQuantity = formatter.format(orderline.UnitsQuantity);
      const confirmedSKUName = await confirmSKUNameFromWMSBuffer(
        orderline.ItemExternalID,
        bufferProductsArray
      );

      documentRoot
        .ele("tREMOTETransSaleLines")
        .ele("SaleLineNo")
        .txt(index + 1)
        .up()
        .ele("SKU")
        .txt(confirmedSKUName)
        .up()
        .ele("SaleUnitQty")
        .txt(roundedQuantity)
        .up()
        .ele("SaleUnitAmountIncTax")
        .txt(roundedSaleUnitAmount)
        .up()
        .ele("SaleTaxByHost")
        .txt("Y")
        .up()
        .ele("SalePriceByHost")
        .txt("Y")
        .up()
        .ele("LineNote")
        .txt("")
        .up();
    }
    const xmlToBeSaved = documentRoot.end({
      prettyPrint: true,
      allowEmptyTags: true,
    });
    return xmlToBeSaved;
  },
};
