const fetch = require('node-fetch');
const _ = require('lodash');
const moment = require('moment');
require('dotenv').config();
require('console-stamp')(console, '[yyyy-mm-dd HH:MM:ss]');

module.exports = {

    pepperiToBufferOrder: (order) => {
        const decoratedOrder = {
            submittedAt: order.CreationDateTime,
            ownerEmail: order.Agent ? order.Agent.Data.Email : 'Not set',
            salesPerson: order.Agent ? `${order.Agent.Data.FirstName} ${order.Agent.Data.LastName}`: 'Not set',
            pepperiCustomerID: order.Account.Data.InternalID,
            wmsCardID: order.Account ? Number.parseInt(order.Account.Data.ExternalID) : null,
            pepperiCustomerName: order.Account.Data.Name,
            customerEmail: order.Account.Data.Email,
            pepperiOrderInternalID: order.InternalID,
            orderLines: order.TransactionLines.Data,
            status: order.Status,
            statusName: order.StatusName,
            shipTo: {
              ShipToName: order.ShipToName,
              ShipToStreet: order.ShipToStreet,
              ShipToCity: order.ShipToCity,
              ShipToState: order.ShipToState,
              ShipToZipCode: order.ShipToZipCode,
              ShipToCountry: order.ShipToCountry,
              ShipToFax: order.ShipToFax,
              ShipToPhone: order.ShipToPhone,
            },
            orderSnapshot: order,
          };
        return decoratedOrder;
    }

}