const { create } = require("xmlbuilder2");
require("dotenv").config();
const _ = require("lodash");
const { productNameFromWMS } = require("../../helpers/WMSBreezeAPI");
const { Order } = require("../../../models");
const moment = require("moment");
const fs = require("fs");
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

module.exports = {
  sanitiseOrderObject: (orderObject) => {
    orderObject = orderObject.toJSON();
    _.forIn(orderObject, (value, key) => {
      orderObject[key] = orderObject[key] ? value : "Not set";
    });
    if (orderObject.shipTo) {
      _.forIn(orderObject.shipTo, (value, key) => {
        orderObject.shipTo[key] = orderObject.shipTo[key] ? value : "Not set";
      });
    }
    return orderObject;
  },

  createXMLDirectories: () => {},

  saveXMLFile: (formattedXML, orderID) => {
    const filePath = process.env.XML_SAVE_LOCATION;
    const filePathArchive = process.env.XML_ARCHIVE_SAVE_LOCATION;
    fs.writeFile(
      `${filePath}PEP-${orderID}.xml`,
      formattedXML,
      { flag: "w" },
      (error, data) => {
        if (error) {
          console.error(`problem saving XML file: ${error}`);
          return false;
        }
        console.info(`Wrote XML file for order ID ${orderID}`);
        return true;
      }
    );

    fs.writeFile(
      `${filePathArchive}PEP-${orderID}-archived.xml`,
      formattedXML,
      { flag: "w" },
      (error, data) => {
        if (error) {
          console.error(`problem saving archive XML file: ${error}`);
          return false;
        }
        console.info(`Wrote archive XML file for order ID ${orderID}`);
        return true;
      }
    );
  },
  createOrderXML: async (orderObject, WMSheaders) => {
    const orderObjectWithoutNulls =
      module.exports.sanitiseOrderObject(orderObject);
    const submittedAtISOTimestamp = `${
      moment(orderObjectWithoutNulls.submittedAt).toISOString().split(".")[0]
    }Z`;

    let documentRoot = create({ version: "1.0" }).ele("NewDataSet");

    documentRoot
      .ele("tREMOTETransHeader")
      .ele("Company_ID")
      .txt("DATAPEL")
      .up()
      .ele("StoreCode")
      .txt("PERTH")
      .up()
      .ele("PostingDate")
      .txt("")
      .up()
      .ele("TransID")
      .txt(orderObjectWithoutNulls.pepperiOrderInternalID)
      .up()
      .ele("TransDate")
      .txt(submittedAtISOTimestamp)
      .up()
      .ele("Salesperson")
      .txt(orderObjectWithoutNulls.ownerEmail)
      .up()
      .ele("CustomerID")
      .txt(orderObjectWithoutNulls.wmsCardID)
      .up()
      .ele("SaleType")
      .txt("S")
      .up()
      .ele("ShippingMethod")
      .txt("Customer Default")
      .up()
      .ele("ClosedYN")
      .txt("Y")
      .up()
      .ele("OriginalSaleType")
      .txt("O")
      .up()
      .up()
      .ele("tREMOTETransCustomer")
      .ele("ShipToAddress1")
      .txt(orderObjectWithoutNulls.shipTo.ShipToStreet)
      .up()
      .ele("ShipToSuburb")
      .txt(orderObjectWithoutNulls.shipTo.ShipToCity)
      .up()
      .ele("ShipToPostCode")
      .txt(orderObjectWithoutNulls.shipTo.ShipToZipCode)
      .up()
      .ele("ShipToState")
      .txt(orderObjectWithoutNulls.shipTo.ShipToState)
      .up()
      .ele("EmailAddress")
      .txt(orderObjectWithoutNulls.customerEmail)
      .up();

    for (const orderline of orderObjectWithoutNulls.orderLines) {
      const roundedSaleUnitAmount = (orderline.UnitPrice * 1.1).toFixed(4);
      let productDescription = "Name not found in WMS";
      const WMSProductNameResult = await productNameFromWMS(
        WMSheaders,
        orderline.ItemExternalID
      );
      if (WMSProductNameResult.status !== 500) {
        productDescription = WMSProductNameResult.Description;
      } else {
        throw `Problem getting product name from WMS - ${WMSProductNameResult.message}`;
      }
      documentRoot
        .ele("tREMOTETransSaleLines")
        .ele("SKU")
        .txt(orderline.ItemExternalID)
        .up()
        .ele("SaleQty")
        .txt(orderline.UnitsQuantity)
        .up()
        .ele("SaleUnitQty")
        .txt("")
        .up()
        .ele("SaleUnitAmountIncTax")
        .txt(roundedSaleUnitAmount)
        .up()
        .ele("SaleTaxByHost")
        .txt("Y")
        .up()
        .ele("SKUDescription")
        .txt(productDescription)
        .up()
        .ele("SalePriceByHost")
        .txt("Y")
        .up();
    }
    const xmlToBeSaved = documentRoot.end({ prettyPrint: true });
    return xmlToBeSaved;
  },
};
