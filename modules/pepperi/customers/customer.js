const fetch = require("node-fetch");
const _ = require("lodash");
require("dotenv").config();
const { promisify } = require("util");
const sleep = promisify(setTimeout);
const { prepareHeaders } = require("../../helpers/apitools");
const { customerObjectSetupBatch } = require("./customerdecorator");
const { batchUpdatePepperiCustomers } = require("./customerbatch");
let APIHeaders = {};
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

module.exports = {
  syncCustomers: async () => {
    try {
      const headers = await prepareHeaders(process.env.PEPPERI_MAIN_API_KEY);
      const allWMSCustomers = await module.exports.getCustomersFromWMS(headers);
      let decoratedCustomers = allWMSCustomers.map((WMSCustomer) => {
        return customerObjectSetupBatch(WMSCustomer);
      });
      console.log(
        `There are ${decoratedCustomers.length} customers to be created/updated in Pepperi.`
      );
      await batchUpdatePepperiCustomers(decoratedCustomers, headers);
    } catch (error) {
      console.error(`Error syncing customers from WMS, ${error}`);
    }
  },

  deleteGhostCustomers: async (headers) => {
    //TODO: get WMS customers and decorate them
    //TODO: create an array which has Pepperi customers that don't have a SKU in WMS (using Pepperi objIDs)
    //TODO: use that array to send delete requests to Pepperi for each customer
  },

  getCustomersFromWMS: async (headers) => {
    try {
      const response = await fetch(
        `http://${process.env.WMS_API_LOCATION}/json/sql?filter~SELECT%20*%20FROM%20vGTH_Customers_PEP`,
        { headers: headers.WMSHeaders, timeout: 0 }
      );
      if (response.status !== 200) {
        throw `WMS Customers API call didn't return 200 - ${response.status}: ${response.statusText}`;
      }
      const responseJSON = await response.json();
      return responseJSON;
    } catch (error) {
      console.error(`Error fetching customers from WMS: ${error}`);
    }
  },
};
