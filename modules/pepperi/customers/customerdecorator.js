const fetch = require('node-fetch');
const _ = require('lodash');
require('dotenv').config();
require('console-stamp')(console, '[yyyy-mm-dd HH:MM:ss]');

module.exports = {

    capitaliseFirstLetters: (theString) => {
        if(theString !== '' && theString !== undefined) {
            const lowerCapsString = theString.toLowerCase();
            const words = lowerCapsString.split(" ");
            return words.map((word) => {
                if(word.length > 0) {
                    return word[0].toUpperCase() + word.substring(1); 
                }
            }).join(" ");
        } else {
            return null;
        }
    },

    customerObjectSetupBatch: (WMSCustomerObject) => {

        /**
         headers: [
             "Name",
             "ExternalID",
             "Email",
             "Street",
             "City",
             "Country",
             "Fax",
             "Mobile",
             "Phone",
             "ZipCode",
             "State",
             "Prop1", // Category
             "Prop2", // Customer contact person
             "Prop3", // Responsible salesperson
           ],
         */

        const concatenatedAddress = [
            WMSCustomerObject.street1,
            WMSCustomerObject.street2,
            WMSCustomerObject.street3,
            WMSCustomerObject.street4,
        ].filter(street => { return street != ''}).join(", ");

        const customerToBeCreated = [
            WMSCustomerObject.CompanyName,
            WMSCustomerObject.CardID,
            WMSCustomerObject.email,
            concatenatedAddress,
            WMSCustomerObject.suburb,
            WMSCustomerObject.country,
            WMSCustomerObject.fax,
            WMSCustomerObject.phone,
            WMSCustomerObject.phone,
            WMSCustomerObject.postcode,
            WMSCustomerObject.state,
            module.exports.capitaliseFirstLetters(WMSCustomerObject.Category), // Category
            module.exports.capitaliseFirstLetters(WMSCustomerObject.contact), // Customer contact person
            module.exports.capitaliseFirstLetters(WMSCustomerObject.TerritoryManager) // Responsible salesperson
        ];
        return customerToBeCreated;
    },

    customerObjectSetupSingle: (WMSCustomerObject) => {

        const concatenatedAddress = [
            WMSCustomerObject.street1,
            WMSCustomerObject.street2,
            WMSCustomerObject.street3,
            WMSCustomerObject.street4,
        ].filter(street => { return street != ''}).split(", ");

        const customerToBeCreated = {
            Name: WMSCustomerObject.CompanyName,
            ExternalID: WMSCustomerObject.CardID,
            Email: WMSCustomerObject.email,
            Street: WMSCustomerObject.concatenatedAddress,
            City: WMSCustomerObject.city,
            Country: WMSCustomerObject.country,
            Fax: WMSCustomerObject.fax,
            Mobile: WMSCustomerObject.phone,
            Phone: WMSCustomerObject.phone,
            Postcode: WMSCustomerObject.postcode,
            State: WMSCustomerObject.state,
            Prop1: module.exports.capitaliseFirstLetters(WMSCustomerObject.Category), // Category
            Prop2: module.exports.capitaliseFirstLetters(WMSCustomerObject.contact), // Customer contact person
            Prop3: module.exports.capitaliseFirstLetters(WMSCustomerObject.TerritoryManager) // Responsible salesperson
        }
        return customerToBeCreated;
    },
    addPepperiID: (WMSCustomerObject, pepperiCustomerLookup) => {
        const correspondingPepperiCustomer = pepperiCustomerLookup[WMSCustomerObject['cardID']];
        if(!correspondingPepperiCustomer) {
            return WMSCustomerObject
        } else {
            return {
                ...WMSCustomerObject,
                objID: parseInt(correspondingPepperiCustomer.objID)
            }
        }
    },
    addPepperiResourceURI: (WMSCustomerObject, pepperiCustomerLookup) => {
        const correspondingPepperiCustomer = pepperiCustomerLookup[WMSCustomerObject['cardID']];
        if(!correspondingPepperiCustomer) {
            return WMSCustomerObject
        } else {
            return {
                ...WMSCustomerObject,
                resource_uri: `/api/latest/customers/${correspondingPepperiCustomer.objID}`
            }
        }
    },

}