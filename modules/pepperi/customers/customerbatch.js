const fetch = require("node-fetch");
const _ = require("lodash");
require("dotenv").config();
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

module.exports = {
  batchUpdatePepperiCustomers: async (customerObjectArray, headers) => {
    const payloadForBatchUpdate = {
      headers: [
        "Name",
        "ExternalID",
        "Email",
        "Street",
        "City",
        "Country",
        "Fax",
        "Mobile",
        "Phone",
        "ZipCode",
        "State",
        "Prop1", // Category
        "Prop2", // Customer contact person
        "Prop3", // Responsible salesperson
      ],
      lines: [...customerObjectArray],
    };

    const response = await fetch(
      `https://api.pepperi.com/v1.0/bulk/accounts/json`,
      {
        method: "POST",
        headers: headers.PepperiHeaders,
        body: JSON.stringify(payloadForBatchUpdate),
      }
    );
    if (response.status === 200) {
      const requestBody = await response.json();
      const URI = requestBody.URI;
      console.log(
        `Successfully set off a batch upsert of Customers in Pepperi. Here's the progress status: https://api.pepperi.com/v1.0${URI}`
      );
    } else {
      console.info(response.statusText);
      const errorRequestBody = await response.json();
      console.info(errorRequestBody);
    }

    return response;
  },
};
