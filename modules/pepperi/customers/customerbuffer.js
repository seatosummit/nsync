const fetch = require("node-fetch");
require("dotenv").config();
const { prepareHeaders } = require("../../helpers/apitools");
const { promisify } = require("util");
const sleep = promisify(setTimeout);
let APIHeaders = {};

module.exports = {
  updateCustomerBuffer: async () => {
    try {
      APIHeaders = await prepareHeaders(process.env.PEPPERI_SECOND_API_KEY);
      // console.log("API headers created");
    } catch (error) {
      console.error("Unable to create API headers", error);
    }
    try {
      await module.exports.handlePepperiCustomers(APIHeaders);
      // console.log("... customer buffer sync done");
    } catch (error) {
      console.error(`Error syncing customers: ${error}`);
    }
  },

  saveCustomerBufferEntry: async (pepperiCustomer) => {
    const oldOnesFound = await Customer.findAll({
      where: {
        cardID: pepperiCustomer.cardID,
      },
    });
    if (oldOnesFound.length === 0) {
      await Customer.create(pepperiCustomer);
      console.log(
        `Added new customer buffer for customerID ${pepperiCustomer.customerID}`
      );
    } else {
      const updateResultDirect = await oldOnesFound[0].update(pepperiCustomer);
      if (updateResultDirect instanceof Customer) {
        console.log(
          `Updated Customer buffer, CardID ${pepperiCustomer.cardID} (${pepperiCustomer.coLastName})`
        );
      } else {
        console.error(
          `Problem Updating Customer buffer, CardID ${pepperiCustomer.cardID} (${pepperiCustomer.coLastName})`
        );
      }
    }
  },

  handlePepperiCustomers: async (
    headers,
    url = "/api/latest/customers?limit=100",
    customers = []
  ) => {
    try {
      // console.log(`Getting Customers from Pepperi: ${url}`);
      const waitBetweenUpdates = process.env.UPDATE_BATCH_WAIT_MS
        ? parseInt(process.env.UPDATE_BATCH_WAIT_MS)
        : 60000;
      const response = await fetch(`https://app.pepperi.com${url}`, {
        headers: headers.PepperiHeaders,
      });
      if (response.status === 429) {
        console.error(
          `We're hitting API limits on retrieving Customers from Pepperi`
        );
        setTimeout(
          await module.exports.handlePepperiCustomers(headers, url, customers),
          60000
        );
      }
      if (response.status !== 200) {
        throw `${response.status}: ${response.statusText}`;
      }
      const customersJSON = await response.json();
      //TODO: replace pm2/io monitoring for something else
      customers = customersJSON.objects;

      const customerSavePromises = customers.map((customer) => {
        return module.exports.saveCustomerBufferEntry({
          contactName: customer.contact,
          customerID: customer.customerID ? parseInt(customer.customerID) : 0,
          cardID:
            customer.cardID != 0
              ? parseInt(customer.cardID)
              : parseInt(customer.id),
          objID: customer.objID,
          coLastName: customer.name,
          pepperiSnapshot: customer,
        });
      });

      await Promise.all(customerSavePromises).catch((e) =>
        console.error(`Error syncing this batch of customers: ${e}`)
      );

      if (customersJSON.meta.next) {
        await sleep(waitBetweenUpdates);
        await module.exports.handlePepperiCustomers(
          headers,
          customersJSON.meta.next
        );
      }
    } catch (error) {
      console.error(`Error syncing Pepperi Customers: ${error}. Retrying...`);
      await sleep(
        module.exports.handlePepperiCustomers(headers, url, customers),
        60000
      );
    }
  },
};
