const {
  updateCustomerBuffer,
} = require("../../modules/customers/customerbuffer");
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

const customerBuffer = async () => {
  try {
    // console.log("Updating customer buffer...");
    await updateCustomerBuffer();
    // console.log("... done updating customer buffer.");
  } catch (error) {
    console.error(`Error customer buffer: ${error}`);
  }
};

customerBuffer();
