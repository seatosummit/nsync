const { syncCustomers } = require("../../customers/customer");
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

const customersSchedule = async () => {
  try {
    // console.log("Updating Pepperi customers ...");
    await syncCustomers();
    // console.log("... done updating Pepperi customers.");
  } catch (error) {
    console.error(`Error customers update: ${error}`);
  }
};

customersSchedule();
