const { updateProductStockLevels } = require("../../products/productstock");
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

const productsStockLevelsSchedule = async () => {
  try {
    // console.log("Syncing products to Pepperi...");
    await updateProductStockLevels();
    // console.log("... done syncing products.");
  } catch (error) {
    console.error(`Error with syncing products: ${error}`);
  }
};

productsStockLevelsSchedule();
