const { syncCategories } = require("../../modules/products/category");
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

const productCategories = async () => {
  try {
    // console.log("Updating product categories...");
    await syncCategories();
    // console.log("... done updating product categories.");
  } catch (error) {
    console.error(`Error with product categories update: ${error}`);
  }
};

productCategories();
