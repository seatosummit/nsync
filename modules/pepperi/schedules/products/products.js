const {
  syncProductsFromWMS,
  syncProductsFromWMSUsingBuffer,
} = require("../../products/product");
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

const productsSchedule = async () => {
  try {
    // console.log("Syncing products to Pepperi...");
    // await syncProductsFromWMS();
    await syncProductsFromWMSUsingBuffer(false);
    // console.log("... done syncing products.");
  } catch (error) {
    console.error(`Error with syncing products: ${error}`);
  }
};

productsSchedule();
