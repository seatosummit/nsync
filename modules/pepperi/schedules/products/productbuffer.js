const {
  updateWMSProductBuffer,
} = require("../../../wms/products/productbuffer");
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

const productBuffer = async () => {
  try {
    // console.log("Updating product buffer...");
    await updateWMSProductBuffer({ filterOnlyActiveProducts: false });
    // console.log("... done updating product buffer.");
  } catch (error) {
    console.error(`Error product buffer: ${error}`);
  }
};

productBuffer();
