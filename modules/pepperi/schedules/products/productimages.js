const { syncRelevantSalsifyImages } = require("../../products/productimages");
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

const productImagesSchedule = async () => {
  try {
    // console.log("Updating product images into Pepperi FTP folder...");
    await syncRelevantSalsifyImages();
    // console.log("... done updating product images.");
  } catch (error) {
    console.error(`Error with updating product images: ${error}`);
  }
};

productImagesSchedule();
