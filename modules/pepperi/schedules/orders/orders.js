// const { updateOrderBuffer } = require('..//modules/pepperi/orders/order');
const { exportOrdersToXML } = require("../../orders/order");

// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

const orders = async () => {
  try {
    // console.log("Getting Pepperi orders into XML files ...");
    await exportOrdersToXML();
    process.exit();
    // console.log("... done getting Pepperi orders.");
  } catch (error) {
    console.error(`Error getting orders: ${error}`);
  }
};

orders();
