const JSONSchemaValidator = require("ajv");
const { Logger } = require("../helpers/Logger");
const productCreateSchema = require("../../JSONValidationSchemas/productCreateSchema.json");
const productUpdateSchema = require("../../JSONValidationSchemas/productUpdateSchema.json");
const privateMetafieldUpsertSchema = require("../../JSONValidationSchemas/privateMetafieldUpsertSchema.json");
class CentralBufferValidators {
  static validShopifyProductPayload(args) {
    const ProductEnhancementId = args?.ProductEnhancementId;
    const shopifyProductPayload = args?.shopifyProductPayload;
    const productSKU = args?.productSKU;
    if (!shopifyProductPayload)
      throw `No Shopify product payload given. Can't validate.`;
    if (!ProductEnhancementId) throw `No ProductId given. Can't validate.`;

    const JSONValidator = new JSONSchemaValidator({ allowUnionTypes: true });

    const validate = JSONValidator.compile(productCreateSchema);
    let validationResult = validate(shopifyProductPayload);

    if (!validationResult) {
      const errorsArray = validate.errors.map((error) => {
        if (error.instancePath === "/input/tags")
          return `ERP Categories ${error.message}`;
        if (error.instancePath.startsWith("/input/variants/"))
          return `Variant Size/Colour ${error.message}`;
        return `${error.instancePath} ${error.message}`;
      });
      errorsArray.unshift(`Product ${productSKU}`);
      return { pass: validationResult, errors: errorsArray.join(", ") };
    }

    const variants = shopifyProductPayload.input.variants;
    const uniques = [
      ...new Set(
        variants.map((item) => {
          return item.options.join();
        })
      ),
    ];

    validationResult = variants.length === uniques.length;
    if (!validationResult)
      return {
        pass: false,
        errors: `Product ${productSKU} variants not unique`,
      };

    return { pass: validationResult, errors: null };
  }

  // TODO: this can easily become a generic validator
  static validBufferProductIdUpdatePayload(args) {
    const payload = args?.payload;
    if (!payload) throw `No payload given. Can't validate.`;
    const id = args?.id ?? "No ID given";
    const JSONValidator = new JSONSchemaValidator({ allowUnionTypes: true });
    const validate = JSONValidator.compile(privateMetafieldUpsertSchema);
    let validationResult = validate(payload);

    if (!validationResult) {
      const errorsArray = validate.errors.map((error) => {
        return `${error.instancePath} ${error.message}`;
      });
      errorsArray.unshift(`Payload for ${id}`);
      return { pass: validationResult, errors: errorsArray.join(", ") };
    }
    return { pass: validationResult, errors: null };
  }

  static validShopifyProductUpdatePayload(args) {
    const shopifyProductPayload = args?.shopifyProductPayload;
    const ProductEnhancementId = args?.ProductEnhancementId;
    if (!shopifyProductPayload)
      throw `No Shopify product payload given. Can't validate.`;
    if (!ProductEnhancementId)
      throw `No ProductEnhancementId given. Can't validate.`;

    const JSONValidator = new JSONSchemaValidator({ allowUnionTypes: true });

    const validate = JSONValidator.compile(productUpdateSchema);
    let validationResult = validate(shopifyProductPayload);

    if (!validationResult) {
      const errorsArray = validate.errors.map((error) => {
        return `${error.instancePath} ${error.message}`;
      });
      errorsArray.unshift(`ProductEnhancement ${ProductEnhancementId}`);
      return { pass: validationResult, errors: errorsArray.join(", ") };
    }

    const variants = shopifyProductPayload.input.variants;
    const uniques = [
      ...new Set(
        variants.map((item) => {
          return item.options.join();
        })
      ),
    ];

    validationResult = variants.length === uniques.length;
    if (!validationResult)
      return {
        pass: false,
        errors: `ProductEnhancement ${ProductEnhancementId} variants not unique`,
      };

    return { pass: validationResult, errors: null };
  }
  // TODO: variant uniqueness validator into its own function
  // TODO: generic validation with schema file and item input
}

module.exports.CentralBufferValidators = CentralBufferValidators;
