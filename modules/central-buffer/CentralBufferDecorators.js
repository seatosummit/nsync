const { todayIsBetween, parseDateWithFormat } = require("../helpers/dateTools");
const { Logger } = require("../helpers/Logger");
const { NumberTools } = require("../helpers/NumberTools");
const { SalsifyTools } = require("../helpers/SalsifyTools");
const _ = require("lodash");
const {
  valueExistsAndIsNotEmpty,
  addSpacesAndCapitalise,
  createSlug,
  sanitiseTechSpec,
} = require("../helpers/stringTools");
const { BufferDecorationError } = require("./BufferErrors");
class BufferDecorators {
  constructor(products, productVariants, metafields, productImages) {
    this.products = products;
    this.productVariants = productVariants;
    this.metafields = metafields;
    this.productImages = productImages;
  }

  decorateProductEnhancement(product) {
    const variants = _.filter(this.productVariants, { __parentId: product.id });
    const images = _.filter(this.productImages, { __parentId: product.id });
    const metafields = _.filter(this.metafields, { __parentId: product.id });
    const decoratedProductEnhancement = {
      name: product.title,
      shopifyProductId: parseInt(product.legacyResourceId),
      SalesChannelId: this.salesChannelInfo.id,
      shopifyImages: images,
      shopifyOptions: product.options,
      shopifyVariants: variants,
      metafields,
      status: product.status,
      updatedToShopify: product.updatedAt,
    };
    return decoratedProductEnhancement;
  }

  decorateVariantEnhancement(productVariant) {
    const product = _.find(this.products, { id: productVariant["__parentId"] });
    const images = _.filter(this.productImages, {
      __parentId: productVariant.id,
    });
    const metafields = _.filter(this.metafields, {
      __parentId: productVariant.id,
    });
    const decoratedVariantEnhancement = {
      name: productVariant.title,
      shopifyProductId: parseInt(product.legacyResourceId),
      shopifyVariantId: parseInt(productVariant.legacyResourceId),
      SalesChannelId: this.salesChannelInfo.id,
      sku: productVariant.sku,
      shopifyImageId: productVariant.image
        ? getLegacyId(productVariant.image.id)
        : null,
      price: productVariant.price,
      // compareToPrice: productVariant.compareAtPrice,
      shopifyInventoryQuantity: productVariant.inventoryQuantity,
      shopifyInventoryItemId: getLegacyId(productVariant.inventoryItem.id),
      minimumOrderQuantity: 1, // TODO: add support
      updatedToShopify: productVariant.updatedAt,
    };
    return decoratedVariantEnhancement;
  }
}

class PricingDecorators {
  static erpToPricingData(erpObject) {
    if (!erpObject) throw `Can't decorate Pricing data without erpObject`;
    const snapshot = erpObject.productSnapshot;
    const decoratedPricingData = {
      erpProductId: snapshot.ProductID,
      unitprice: snapshot.unitprice,
      retailprice: snapshot.customfield3,
      somecrazydiscount: snapshot.customfield2,
      sku: snapshot.sku,
      b2b_special: snapshot.b2b_special,
      b2b_startdate: snapshot.b2b_startdate,
      b2b_enddate: snapshot.b2b_enddate,
      b2c_special: snapshot.b2c_special,
      b2c_startdate: snapshot.b2c_startdate,
      b2c_enddate: snapshot.b2c_enddate,
      b2p_special: snapshot.b2p_special,
      b2p_startdate: snapshot.b2p_startdate,
      b2p_enddate: snapshot.b2p_enddate,
      b2p_default: snapshot.b2p_default,
    };
    return decoratedPricingData;
  }
  static pricingUpdate(args) {
    const pricingEnhancements = args?.pricingEnhancements;
    const sku = args?.sku;
    const SalesChannelId = args?.SalesChannelId;
    if (!pricingEnhancements) throw `No pricingEnhancements given for update`;
    const pricingUpdatePayload = pricingEnhancements.map((enhancement) => {
      const payload = {
        price_list_slug: enhancement.price_list_slug,
        pricing: [],
      };
      const price = Number.parseFloat(enhancement.amount);
      if (price === 0) {
        Logger.error(
          `Price list ${enhancement.price_list_slug} price zero for SKU ${enhancement.sku}`
        );
        return payload;
      }
      if (!enhancement.valid) return payload;
      payload.pricing.push({
        quantity: enhancement.quantity,
        price: Number.parseFloat(enhancement.amount),
        tax_type: "net", // TODO: does this ever need to change?
      });
      return payload;
    });
    const decoratedPricingUpdate = {
      SalesChannelId,
      payload: pricingUpdatePayload,
      sku,
    };

    return decoratedPricingUpdate;
  }
  static pricingEnhancement(args) {
    const priceList = args?.priceList;
    const erpVariant = args?.erpVariant;
    const pricingEntry = args?.pricingEntry;
    const salesChannelId = args?.salesChannelId;
    const moqProductVariant = args?.moqProductVariant;
    if (!priceList) throw BufferDecorationError(`No priceList`);
    if (!erpVariant) throw BufferDecorationError(`No erpVariant`);
    // if (!pricingEntry)
    //   // TODO: this is here for the future when D2C sites jump onto this logic
    //   Logger.error(`Variant SKU ${erpVariant.sku} has no Pricing entry`);
    const currentFieldToLookFor = priceList.erpFieldName;
    const currentPriceValue = erpVariant[`${currentFieldToLookFor}`];
    const quantity = 1;
    const decoratedPricingEnhancement = {
      PricingId: pricingEntry?.id,
      PriceListId: priceList.id,
      SalesChannelId: salesChannelId,
      valid: false,
      price_list_slug: priceList.price_list_slug,
      sku: erpVariant.sku,
      quantity,
    };
    if (!valueExistsAndIsNotEmpty(currentPriceValue)) {
      // Logger.error(
      //   `No price value "${currentFieldToLookFor}" set for SKU ${erpVariant.sku} for PriceList ${priceList.name}`
      // );
      return decoratedPricingEnhancement;
    }
    if (priceList.removeGSTFromPrice) {
      const roundedGSTRemovedPrice = NumberTools.twoDecimalsRounded(
        currentPriceValue / 1.1
      );
      decoratedPricingEnhancement.amount = roundedGSTRemovedPrice;
    } else {
      decoratedPricingEnhancement.amount = currentPriceValue;
    }
    if (priceList.timeSensitive) {
      const startDateField = priceList.timeSensitiveStartField;
      const endDateField = priceList.timeSensitiveEndField;
      const startDate = parseDateWithFormat({
        expectedFormat: "d/MM/yyyy hh:mm:ss a",
        dateString: erpVariant[`${startDateField}`],
      });
      const endDate = parseDateWithFormat({
        expectedFormat: "d/MM/yyyy hh:mm:ss a",
        dateString: erpVariant[`${endDateField}`],
      });
      const validBasedOnDate = todayIsBetween(startDate, endDate);
      if (!validBasedOnDate) {
        Logger.info(
          `Price for SKU ${erpVariant.sku} for PriceList ${priceList.name}, is not within time bounds`
        );
        return decoratedPricingEnhancement;
      }
    }
    decoratedPricingEnhancement.valid = true;
    return decoratedPricingEnhancement;
  }
}

class SalsifyToBuffer {
  static salsifyIntoGeneralBufferProductVariant(productVariantObject) {
    const basicMappings = {
      salsify_parent_id: "salsify:parent_id",
      salsifyProductId: "salsify:system_id",
      ean: "BARCODE/EAN",
      sku: "salsify:id",
      asin: "ASIN",
      name: "FULL PRODUCT NAME (incl VARIANT)",
      brand: "BRAND",
      colourLayman: "COLOUR LAYMAN (Generic Colour Group)",
      colour: "COLOUR VARIANT",
      modelVariant: "MODEL VARIANT",
      gender: "Gender",
      size: "SIZE VARIANT",
      description: "SHORT DESCRIPTION",
      parent_sku: "salsify:parent_id",
    };

    let salsifyBufferProduct = {
      digital_assets: productVariantObject["salsify:digital_assets"],
      snapshot: productVariantObject,
    };
    for (const attributeKey in basicMappings) {
      if (Object.hasOwnProperty.call(basicMappings, attributeKey)) {
        const salsifyAttributeKey = basicMappings[attributeKey];
        salsifyBufferProduct[attributeKey] =
          productVariantObject[`${salsifyAttributeKey}`];
      }
    }

    // factoring in simple products, TODO: remove this after conversion finished
    if (!salsifyBufferProduct.parent_sku) {
      salsifyBufferProduct.parent_sku = productVariantObject["salsify:id"];
    }

    if (
      !productVariantObject["SIZE VARIANT"] &&
      !productVariantObject["COLOUR VARIANT"]
    )
      salsifyBufferProduct.size = "One size";

    return salsifyBufferProduct;
  }

  static salsifyIntoGeneralBufferProduct(productObject) {
    const basicMappings = {
      sku: "salsify:id",
      salsifyProductId: "salsify:system_id",
      name: "BASE PRODUCT NAME",
      description: "SHORT DESCRIPTION",
      brand: "BRAND",
      gender: "Gender",
      modelVariant: "MODEL VARIANT",
      erp_category: "ERP - Category",
      erp_sub_category: "ERP - Sub-Category",
    };

    const salsifyBufferProduct = {
      snapshot: productObject,
    };
    salsifyBufferProduct.tech_specs = SalsifyTools.extractTechSpecs({
      salsifyEntry: productObject,
    });
    salsifyBufferProduct.usps = SalsifyTools.extractUSPs({
      salsifyEntry: productObject,
    });

    for (const attributeKey in basicMappings) {
      if (Object.hasOwnProperty.call(basicMappings, attributeKey)) {
        const salsifyAttributeKey = basicMappings[attributeKey];
        salsifyBufferProduct[attributeKey] =
          productObject[`${salsifyAttributeKey}`];
      }
    }

    return salsifyBufferProduct;
  }
}

class CollectionDecorators {
  static SalesChannelCollectionEntry(args) {
    const entry = args?.entry;
    const SalesChannel = args?.SalesChannel;
    if (!entry) throw `No collection entry given for decoration`;
    if (!SalesChannel) throw `No SalesChannel given for decoration`;
    const handle = createSlug(entry);
    const title = entry;
    const descriptionHtml = entry;
    const SalesChannelId = SalesChannel.id;
    const ruleSet = {
      appliedDisjunctively: false,
      rules: [
        {
          column: "TAG",
          relation: "EQUALS",
          condition: entry,
        },
      ],
    };
    const payload = {
      handle,
      title,
      descriptionHtml,
      ruleSet,
    };
    const decoratedEntry = {
      SalesChannelId,
      appliedDisjunctively: false,
      ...payload,
      payload: { input: payload },
    };
    return decoratedEntry;
  }
}

class EnhancementDecorators {
  constructor(args) {
    this.SalesChannel = args?.SalesChannel;
    if (!this.SalesChannel) throw `Can't decorate without a SalesChannel`;
    this.Rules = this.SalesChannel?.SalesChannelIntegrationRuleset;
    if (!this.Rules)
      throw `Can't decorate without SalesChannelIntegrationRuleset`;
    this.variantOptionStatuses = {
      NO_VARIANTS: "NO_OPTIONS",
      COLOUR_ONLY: "COLOUR_ONLY",
      SIZE_ONLY: "SIZE_ONLY",
      COLOUR_AND_SIZE: "COLOUR_AND_SIZE",
    };

    this.shopifyProductStatuses = {
      ACTIVE: "active",
      ARCHIVED: "archived",
      DRAFT: "draft",
    };
  }

  productUpdatePayload(args) {
    const productEnhancement = args?.productEnhancement;
    const payload = productEnhancement.shopifyPayload.input;
    const decoratedPayload = {};
    const listOfFieldsToInclude = [
      "bodyHtml",
      "handle",
      "options",
      "status",
      "tags",
      "title",
      "variants",
      "privateMetafields",
    ];
    listOfFieldsToInclude.forEach((fieldName) => {
      decoratedPayload[`${fieldName}`] = payload[`${fieldName}`];
    });
    decoratedPayload.id = productEnhancement.GraphQLProductId;

    const decoratedVariants = decoratedPayload.variants.map((item) => {
      return {
        sku: item.sku,
        inventoryPolicy: item.inventoryPolicy,
        options: item.options,
      };
    });

    decoratedPayload.variants = _.uniqWith(decoratedVariants, _.isEqual);

    return {
      id: productEnhancement.id,
      payload: {
        input: decoratedPayload,
      },
    };
  }

  updateExistingShopifyPayload(args) {
    const payload = args?.payload;
    if (!payload)
      throw `No existing payload given for the Shopify payload decorator`;
    const namespace = args?.namespace;
    if (!namespace)
      throw `No namespace given for the Shopify payload decorator`;
    const variants = args?.variants;
    if (!variants) throw `No variants given for the Shopify payload decorator`;
    const Product = args?.Product;
    if (!Product) throw `No Product given, for the Shopify payload decorator`;
    const tags = this.consolidateCategoryTags({ Product });
    const deepUniqueVariants = _.uniqWith(variants, _.isEqual);
    const updatedPayload = {
      input: {
        ...payload,
        published: true,
        variants: deepUniqueVariants,
        tags,
      },
      namespace,
    };
    return updatedPayload;
  }

  newVariantEnhancement(args) {
    const ERPProduct = args?.ERPProduct;
    const Product = args?.Product;
    const ProductVariant = args?.ProductVariant;
    if (!ERPProduct)
      throw `No ERPProduct given, can not prepare ProductVariantEnhancement`;
    if (!Product)
      throw `No Product given, can not prepare ProductVariantEnhancement`;
    if (!ProductVariant)
      throw `No ProductVariant given, can not prepare ProductVariantEnhancement`;

    const metafields = [
      {
        key: "min_order_quantity",
        namespace: "sparklayer",
        ownerId: null,
        type: "integer",
        value: ProductVariant.minOrderQty,
      },
    ];

    const entry = {
      ...ERPProduct,
      SalesChannelId: this.SalesChannel.id,
      ProductId: Product.id,
      ProductVariantId: ProductVariant.id,
      size: ProductVariant.size,
      colour: ProductVariant.colour,
      colourLayman: ProductVariant.colourLayman,
      barcode_ean: ProductVariant.ean,
      // inventory and price updated through variantEnhancement routine
      shopifyInventoryQuantity: null,
      price: null,
      metafields,
      minimumOrderQuantity: ProductVariant.minOrderQty, // TODO: sort this out before decorating
      enabled: true,
    };
    return entry;
  }

  updatedProductEnhancement(args) {
    // TODO: use this when updating Product Enhancements
  }

  shopifyProductBufferIdPrivateMetafield(args) {
    const productEnhancement = args?.productEnhancement;
    if (!productEnhancement) throw `No ProductEnhancement given`;
    const SalesChannel = args?.SalesChannel;
    if (!SalesChannel) throw `No SalesChannel given`;
    const GraphQLProductId = productEnhancement.GraphQLProductId;
    const shopifyProductId = productEnhancement.shopifyProductId;
    if (!GraphQLProductId && !shopifyProductId)
      throw `Can't update private metafield without GraphQLProductId or ShopifyProductId`;

    const privateMetafield = {
      input: {
        owner: GraphQLProductId
          ? GraphQLProductId
          : `gid://shopify/Product/${shopifyProductId}`,
        key: "bufferProductEnhancementId",
        namespace: SalesChannel.shopify_config.metafield_namespace,
        valueInput: {
          value: productEnhancement.id,
          valueType: "STRING",
        },
      },
    };
    return privateMetafield;
  }

  newProductEnhancement(args) {
    const productEnhancement = args?.currentProductEnhancement;
    const variantsArray = args?.variantsArray;
    const Product = args?.Product ?? productEnhancement?.Product;
    if (!variantsArray)
      throw `No variantsArray given, can not prepare ProductEnhancement`;

    if (!Product)
      throw productEnhancement
        ? `No Product given, can not prepare ProductEnhancement id ${productEnhancement?.id}`
        : `No Product given, can not create new ProductEnhancement`;

    const namespace =
      this.SalesChannel.shopify_config?.metafield_namespace ?? "b2b_dist";

    const privateMetafields = [
      {
        key: "bufferProductEnhancementId",
        namespace,
        valueInput: {
          value: Product.id,
          valueType: "STRING",
        },
      },
    ];

    const formattedTechSpecs = Array.isArray(Product.tech_specs)
      ? Product.tech_specs.map((spec) =>
          sanitiseTechSpec({
            object: spec,
            tag: "<br />",
          })
        )
      : [];

    const metafields = [
      {
        key: "tech_specs",
        namespace:
          this.SalesChannel.shopify_config?.metafield_namespace ?? "b2b_dist",
        type: "json",
        value: JSON.stringify(formattedTechSpecs),
      },
      {
        key: "gender",
        namespace:
          this.SalesChannel.shopify_config?.metafield_namespace ?? "b2b_dist",
        type: "single_line_text_field",
        value: Product.gender,
      },
      {
        key: "usps",
        namespace:
          this.SalesChannel.shopify_config?.metafield_namespace ?? "b2b_dist",
        type: "json",
        value: JSON.stringify(Product.usps),
      },
    ];

    const title = Product.name;
    const optionsResults = this.checkProductOptions({ variantsArray, Product });
    const bodyHtml = Product.description;
    const vendor = Product.brand;

    const tags = this.consolidateCategoryTags({ Product });
    const seoTitle =
      tags.length > 1 ? `${Product.name} - ${tags[1]}` : Product.name;
    const seoDescription = Product.description;
    const handle = createSlug(`${Product.name} ${Product.sku}`);

    const seo = {
      title: seoTitle,
      description: seoDescription,
    };

    const variants = variantsArray.map((ProductVariant) => {
      const metafields = [
        {
          key: "min_order_quantity",
          namespace: "sparklayer",
          type: "number_integer",
          value: `${ProductVariant.minimumOrderQuantity}`,
          description: "SparkLayer MOQ custom field",
        },
      ];
      return {
        sku: ProductVariant.sku,
        metafields,
        options: this.variantOptionValues({
          optionStatus: optionsResults.status,
          ProductVariant,
        }),
      };
    });

    const shopifyPayload = {
      input: {
        title,
        handle,
        bodyHtml,
        tags,
        options: optionsResults.options,
        vendor,
        metafields,
        privateMetafields,
        // seo,
        variants,
        status: variants.length > 0 ? "ACTIVE" : "DRAFT",
        published: true,
        publications: [
          {
            channelId: "gid://shopify/Channel/69312741569",
          },
          {
            channelId: "gid://shopify/Channel/77421379777",
          },
        ],
      },
      namespace,
    };

    const bufferProductEnhancement = {
      SalesChannelId: this.SalesChannel.id,
      ProductId: Product.id,
      shopifyOptions: optionsResults.options,
      shopifyVariants: variants,
      status: variants.length > 0 ? "ACTIVE" : "DRAFT",
      metafields,
      shopifyPayload,
    };
    return bufferProductEnhancement;
  }

  updatedPayloadVariant(args) {
    const newVariantInfo = args?.newVariantInfo;
    const existingVariant = args?.existingVariant;
    const updatedVariant = {
      ...existingVariant,
      //price: newVariantInfo.price,
      inventoryItem: {
        tracked: true,
      },
      inventoryQuantities: {
        availableQuantity: newVariantInfo.shopifyInventoryQuantity ?? 0,
        locationId: `gid:\/\/shopify\/Location\/${this.SalesChannel?.shopify_config?.location_id}`,
      },
    };

    if (newVariantInfo) updatedVariant.price = newVariantInfo.price;

    updatedVariant.inventoryPolicy = this.Rules?.continueSellingWhenStockZero
      ? "CONTINUE"
      : "DENY";

    if (updatedVariant.enabled === false)
      updatedVariant.inventoryPolicy = "DENY"; // Always deny if variant has been disabled

    if (newVariantInfo.compareToPrice)
      updatedVariant.compareAtPrice = newVariantInfo.compareToPrice; //TODO: facepalm. fix the column name

    return updatedVariant;
  }

  consolidateCategoryTags(args) {
    const Product = args?.Product;
    if (!Product) return [];
    if (!Product.erp_category || !Product.erp_sub_category) {
      Logger.error(
        `Product id ${Product.id}, SKU ${Product.sku} doesn't have ERP categories`
      );
      return [];
    }
    const erpCategory = addSpacesAndCapitalise(Product.erp_category);
    const erpSubCategory = addSpacesAndCapitalise(Product.erp_sub_category);
    const tags = [erpCategory, `${erpCategory} ${erpSubCategory}`];
    return tags;
  }

  checkProductOptions(args) {
    const Product = args?.Product;
    if (!Product) throw `Please provide a product`;
    const variantsArray = args?.variantsArray;
    const options = [];
    if (!valueExistsAndIsNotEmpty(variantsArray)) {
      Logger.error(
        `No colour/size options for Product id ${Product.id}, sku ${Product.sku}`
      );
      return { options, status: this.variantOptionStatuses.NO_VARIANTS };
    }
    const variantsHaveDefinedColour =
      variantsArray.filter((variant) => variant.colour).length ===
      variantsArray.length;
    if (variantsHaveDefinedColour) options.push("Colour");
    const variantsHaveDefinedSize =
      variantsArray.filter((variant) => variant.size).length ===
      variantsArray.length;
    if (variantsHaveDefinedSize) options.push("Size");
    const status = this.optionStatus({
      colourOption: variantsHaveDefinedColour,
      sizeOption: variantsHaveDefinedSize,
    });
    return { options, status };
  }

  optionStatus({ colourOption, sizeOption }) {
    if (!colourOption && !sizeOption)
      return this.variantOptionStatuses.NO_VARIANTS;
    if (colourOption && !sizeOption)
      return this.variantOptionStatuses.COLOUR_ONLY;
    if (!colourOption && sizeOption)
      return this.variantOptionStatuses.SIZE_ONLY;
    if (colourOption && sizeOption)
      return this.variantOptionStatuses.COLOUR_AND_SIZE;
  }

  variantOptionValues({ optionStatus, ProductVariant }) {
    if (!optionStatus) throw `No option status given`;
    if (!ProductVariant) throw `No ProductVariant given`;
    const variantOptions = [];
    switch (optionStatus) {
      case this.variantOptionStatuses.COLOUR_AND_SIZE:
        variantOptions.push(ProductVariant.colour);
        variantOptions.push(ProductVariant.size);
        break;
      case this.variantOptionStatuses.COLOUR_ONLY:
        variantOptions.push(ProductVariant.colour);
        break;
      case this.variantOptionStatuses.SIZE_ONLY:
        variantOptions.push(ProductVariant.size);
        break;
      case this.variantOptionStatuses.NO_VARIANTS:
        break;
    }
    return variantOptions;
  }
}

module.exports.BufferDecorators = BufferDecorators;
module.exports.SalsifyToBuffer = SalsifyToBuffer;
module.exports.PricingDecorators = PricingDecorators;
module.exports.CollectionDecorators = CollectionDecorators;
module.exports.EnhancementDecorators = EnhancementDecorators;
