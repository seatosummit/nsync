const { ShopifyProductImport } = require("./ShopifyProductImport");
const { GenericProductImport } = require("./GenericProductImport");
const { Logger } = require("../helpers/Logger");
const { ShopifyConfig } = require("../helpers/ShopifyConfig");
const { CreateEnhancementRelationships } = require("./BufferOperations");

const ShopifyProductImportCommand = async ({ storeFilterMachineName }) => {
  try {
    const ShopifyStores = await ShopifyConfig.getConfigsFromDatabase({
      machineNameOfSalesChannel: storeFilterMachineName,
    });
    for (const ShopifyStore of ShopifyStores) {
      Logger.info(`Importing products from ${ShopifyStore.name}...`);
      const importer = new ShopifyProductImport({ SalesChannel: ShopifyStore });
      await importer.importProducts();
      await CreateEnhancementRelationships({ SalesChannelId: ShopifyStore.id });
      Logger.info(`... done.`);
    }
  } catch (error) {
    Logger.error(`Problems with product sync: ${error}`);
    process.exit(1);
  }
};

const GenericImport = async () => {
  try {
    const importer = new GenericProductImport();
    Logger.info(`Importing product variants from ERP...`);
    await importer.importVariantInfoFromERP();
    Logger.info(`Importing product info from Salsify...`);
    await importer.importSalsifyInfo();
    Logger.info(`Linking products and variants with Shopify enhancements...`);
    await importer.linkWithEnhancements();
    Logger.info(`... done.`);
  } catch (error) {
    Logger.error(`Problems with generic product and variant import: ${error}`);
    process.exit(1);
  }
};

const CompareBufferToWMSView = async () => {
  try {
    const importer = new GenericProductImport();
    Logger.info(`Comparing Helinox query with buffer...`);
    const comparisonResults = await importer.compareWMSBufferToWMSViews();
    Logger.info(
      `... done: ${comparisonResults.matching.length} / ${comparisonResults.total} match.`
    );
  } catch (error) {
    Logger.error(`Problems with BD product fetch: ${error}`);
  }
};

module.exports.ShopifyProductImportCommand = ShopifyProductImportCommand;
module.exports.GenericImport = GenericImport;
module.exports.CompareBufferToWMSView = CompareBufferToWMSView;
