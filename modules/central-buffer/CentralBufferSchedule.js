const { checkForRule } = require("../helpers/IntegrationRuleTools");
const { Logger } = require("../helpers/Logger");
const { currentMemoryUsage } = require("../helpers/memoryTools");
const { ShopifyConfig } = require("../helpers/ShopifyConfig");
const { updateWMSProductBuffer } = require("../wms/products/productbuffer");
const { BufferRefresher } = require("./BufferRefresher");

module.exports.refreshAllVariants = async (args) => {
  //TODO: give this a more descriptive name=, refreshERPDataForVariantEnhancements
  const machineNameOfSalesChannel = args?.machineNameOfSalesChannel;
  const updateOnlyEnabled = args?.updateOnlyEnabled ?? false;
  const refreshPricingBuffer = args?.refreshPricingBuffer ?? true;
  const refreshVariants = args?.refreshVariants ?? true;
  const refreshProductEnhancements = args?.refreshProductEnhancements ?? true;
  try {
    const ShopifyStores = await ShopifyConfig.getConfigsFromDatabase({
      machineNameOfSalesChannel,
    });
    for (const ShopifyStore of ShopifyStores) {
      Logger.info(
        `Updating Variant and Product Enhancements for ${ShopifyStore.name}...`
      );
      const bufferRefresher = new BufferRefresher({
        SalesChannel: ShopifyStore,
      });
      await bufferRefresher.getResources({ updateOnlyEnabled });
      if (refreshPricingBuffer) {
        Logger.info(`Refreshing pricing buffer...`);
        await bufferRefresher.refreshPricingBuffer();
      }
      if (refreshVariants) {
        Logger.info(`Refreshing variant pricing and inventory...`);
        await bufferRefresher.refreshAllVariantPricingAndInventory({
          updatePrices: true,
        });
      }
      if (refreshProductEnhancements) {
        Logger.info(`Refreshing Product Enhancements...`);
        await bufferRefresher.refreshAllProductEnhancements();
      }
      Logger.info(`...done.`);
    }
  } catch (error) {
    Logger.error(`Error refreshing all variants: ${error}`);
  }
};
module.exports.refreshAllCollections = async (args) => {
  const machineNameOfSalesChannel = args?.machineNameOfSalesChannel;
  try {
    const ShopifyStores = await ShopifyConfig.getConfigsFromDatabase({
      machineNameOfSalesChannel,
    });
    for (const ShopifyStore of ShopifyStores) {
      Logger.info(`Updating Collections buffer for ${ShopifyStore.name}...`);
      const bufferRefresher = new BufferRefresher({
        SalesChannel: ShopifyStore,
      });
      await bufferRefresher.getResourcesForCollection();
      Logger.info(`Refreshing collections buffer...`);
      await bufferRefresher.refreshAllCollections();
      Logger.info(`...done.`);
    }
  } catch (error) {
    Logger.error(`Error refreshing all collections: ${error}`);
  }
};

module.exports.updatePricingEnhancementsBuffer = async (args) => {
  const machineNameOfSalesChannel = args?.machineNameOfSalesChannel;
  try {
    const ShopifyStores = await ShopifyConfig.getConfigsFromDatabase({
      machineNameOfSalesChannel,
    });
    for (const ShopifyStore of ShopifyStores) {
      if (!checkForRule({ ShopifyStore, rule: "usePricingEnhancements" })) {
        continue;
      }
      const bufferRefresher = new BufferRefresher({
        SalesChannel: ShopifyStore,
      });
      Logger.info(`Getting resources to update PricingEnhancements...`);
      await bufferRefresher.getResources({ updateOnlyEnabled: false });
      currentMemoryUsage(`After getting resources for PricingEnhancements`);

      Logger.info(`Refreshing base pricing buffer...`);
      await bufferRefresher.refreshPricingBuffer();
      currentMemoryUsage(`After refreshing pricing buffer`);

      Logger.info(`Updating PricingEnhancements for ${ShopifyStore.name}...`);
      await bufferRefresher.refreshPricingEnhancements();
      Logger.info(
        `Linking PricingEnhancements with ProductVariantEnhancements...`
      );
      await bufferRefresher.linkPricingEnhancementsWithProductVariants();
      Logger.info(`Updated PricingEnhancements for ${ShopifyStore.name}`);
    }
  } catch (error) {
    Logger.error(`Error refreshing all PricingEnhancements: ${error}`);
  }
};

module.exports.updateProductStatusBuffer = async () => {
  try {
    const bufferRefresher = new BufferRefresher();
    // await bufferRefresher.getResources();
    Logger.info(`Updating enabled/disabled product states...`);
    await bufferRefresher.updateProductAndVariantStatuses();
    Logger.info(`...done.`);
  } catch (error) {
    Logger.error(`Error Updating enabled/disabled product states: ${error}`);
  }
};

module.exports.addNewProductsToBuffer = async (args) => {
  const machineNameOfSalesChannel = args?.machineNameOfSalesChannel;
  try {
    const ShopifyStores = await ShopifyConfig.getConfigsFromDatabase({
      machineNameOfSalesChannel,
    });
    for (const ShopifyStore of ShopifyStores) {
      Logger.info(`Updating Product Enhancements for ${ShopifyStore.name}...`);
      const bufferRefresher = new BufferRefresher({
        SalesChannel: ShopifyStore,
      });
      await bufferRefresher.getResourcesForEnhancementCreation();
      await bufferRefresher.createNewProductAndVariantEnhancements();
      Logger.info(
        `Done with ${ShopifyStore.shopify_config.name} new product creation.`
      );
    }
    // process.exit();
  } catch (error) {
    Logger.error(`Error creating new products: ${error}`);
  }
};
