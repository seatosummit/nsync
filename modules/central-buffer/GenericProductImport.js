const { ShopifyAPI } = require("../helpers/ShopifyAPI");
const _ = require("lodash");

require("dotenv").config();
const {
  Product,
  ProductVariant,
  ProductVariantEnhancement,
  ProductEnhancement,
  SalesChannel,
  WMSProduct,
  SalsifyProduct,
  SalsifyProductParent,
} = require("../../models");
const { Sequelize } = require("sequelize");
const Op = Sequelize.Op;
const moment = require("moment");
const { Logger } = require("../helpers/Logger");
const { SequelizeOperations } = require("../sequelize/SequelizeOperations");
const { getLegacyId, skuIsZCoded } = require("../helpers/stringTools");
const { selectFromView } = require("../helpers/WMSBreezeAPI");
const { currentMemoryUsage } = require("../helpers/memoryTools");

class GenericProductImport {
  constructor() {
    this.productPromises = [];
    this.variantPromises = [];
    this.products = [];
    this.productVariants = [];
    this.productVariantEnhancements = [];
    this.salsifyProducts = [];
    this.salsifyVariants = [];
    this.erpProductVariants = [];
    this.productImages = [];
  }

  async compareWMSBufferToWMSViews() {
    const comparisonresults = {
      total: 0,
      matching: [],
      errors: [],
    };
    try {
      this.erpProductVariants = await WMSProduct.findAll({});
      this.productVariantEnhancements = await ProductVariantEnhancement.findAll(
        {
          where: {
            enabled: true,
          },
        }
      );
      // const variantsFromWMSAPICall = _.sortBy(
      //   await selectFromView("vSTS_WebProductInfo_HX"),
      //   ["sku"]
      // ).filter((item) => item.customlist2 === "ACTIVE");
      const variantsFromWMSAPICall = _.sortBy(
        await selectFromView("vSTS_WebProductInfo_HX"),
        ["sku"]
      ).map((item) => {
        return {
          sku: item.sku,
          name: item.name,
          status: item.customlist2,
          qtyavailable: item.qtyavailable,
          b2b_enabled: item.b2b_enabled === "1" ? "Yes" : "No",
          b2c_enabled: item.b2c_enabled === "1" ? "Yes" : "No",
        };
      });
      comparisonresults.total = variantsFromWMSAPICall.length;
      console.log(`sku,name,status,qtyavailable,b2b_enabled,b2c_enabled`);
      for (const productVariant of variantsFromWMSAPICall) {
        console.log(
          `${productVariant.sku},${productVariant.name},${productVariant.status},${productVariant.qtyavailable},${productVariant.b2b_enabled},${productVariant.b2c_enabled}`
        );
        // const matchingBufferProduct = _.find(this.erpProductVariants, {
        //   sku: productVariant.sku,
        // });
        const matchingBufferProduct = _.find(this.productVariantEnhancements, {
          sku: productVariant.sku,
        });
        if (matchingBufferProduct) {
          if (
            parseInt(productVariant.qtyavailable) ===
            matchingBufferProduct.shopifyInventoryQuantity
          ) {
            comparisonresults.matching.push(productVariant);
          } else {
            // Logger.info(
            //   `Helinox Breeze API ${productVariant.sku} stock ${productVariant.qtyavailable}, buffer stock ${matchingBufferProduct.shopifyInventoryQuantity}`
            // );
            comparisonresults.errors.push(productVariant);
          }
        } else {
          // Logger.error(
          //   `Didn't find a matching SKU ${productVariant.sku} in the buffer: ${productVariant.name}`
          // );
        }
      }
      return comparisonresults;
    } catch (error) {
      Logger.error(`Issue with comparison: ${error}`);
      return comparisonresults;
    }
  }

  async importVariantInfoFromERP() {
    try {
      this.productVariants = await ProductVariant.findAll({
        attributes: ["id", "sku"],
      });
      this.erpProductVariants = await WMSProduct.findAll({
        attributes: [
          "id",
          "name",
          "sku",
          "barcode",
          "WMSProductID",
          "brand",
          "productSnapshot",
        ],
      });

      this.erpProductVariants = this.erpProductVariants.filter(
        (currentVariant) => !skuIsZCoded(currentVariant.sku)
      );
      currentMemoryUsage("Before ERP processVariants");
      await this.processVariants();
    } catch (error) {
      Logger.error(`Issue with import: ${error}`);
    }
  }

  async importSalsifyInfo() {
    try {
      this.products = await Product.findAll({ attributes: ["id", "sku"] });
      this.salsifyProducts = (
        await SalsifyProductParent.findAll({
          attributes: ["id", "name", "sku", "snapshot", "brand"],
        })
      ).map((product) => {
        product.snapshot = {
          description: product.snapshot["SHORT DESCRIPTION"],
          salsifyProductId: product.snapshot["salsify:system_id"],
          gender: product.snapshot["Gender"],
        };
        return product;
      });

      this.salsifyVariants = (
        await SalsifyProduct.findAll({
          attributes: [
            "id",
            "name",
            "sku",
            "barcode_ean",
            "asin_amazon",
            "size",
            "colour",
            "colour_layman",
            "brand",
            "snapshot",
          ],
        })
      ).map((product) => {
        product.snapshot = {
          description: product.snapshot["SHORT DESCRIPTION"],
          salsifyProductId: product.snapshot["salsify:system_id"],
        };
        return product;
      });
      currentMemoryUsage("Before processSalsifyProducts");
      await this.processSalsifyProducts();
      currentMemoryUsage("Before processSalsifyVariants");
      await this.processSalsifyVariants();
      currentMemoryUsage("After processSalsifyVariants");
    } catch (error) {
      Logger.error(`Issue with import: ${error}`);
    }
  }

  async linkWithEnhancements() {
    try {
      this.productVariantEnhancements = await ProductVariantEnhancement.findAll(
        {
          include: {
            model: ProductVariant,
            attributes: ["id", "ProductId"],
          },
          attributes: ["id", "shopifyProductId", "ProductId"],
        }
      );
      this.products = await Product.findAll({
        attributes: ["id", "sku"],
      });
      this.productVariants = await ProductVariant.findAll({
        attributes: ["id", "sku", "ProductId"],
      });
      this.productEnhancements = await ProductEnhancement.findAll({
        // include: {
        //   model: Product,
        //   attributes: ["id", "salsifyProductId"],
        // },
      });
      currentMemoryUsage("Before linkVariantEnhancements");
      await this.linkVariantEnhancements();
      currentMemoryUsage("Before linkProductEnhancements");
      await this.linkProductEnhancements();
    } catch (error) {
      Logger.error(`Issue with linking enhancements: ${error}`);
    }
  }

  async linkVariantEnhancements() {
    for (const enhancement of this.productVariantEnhancements) {
      // TODO: confirm that we're still using SKU to distinguish variants? not EAN?
      const variantWithMatchingSKU = _.find(this.productVariants, {
        sku: enhancement.sku,
      });
      if (!enhancement.productVariantId && variantWithMatchingSKU) {
        enhancement.ProductVariantId = variantWithMatchingSKU.id;
        enhancement.ProductId = variantWithMatchingSKU.ProductId;
        await enhancement.save();
      }
    }
  }

  async linkProductEnhancements() {
    for (const enhancement of this.productEnhancements) {
      const matchingVariantEnhancement = _.find(
        this.productVariantEnhancements,
        { shopifyProductId: enhancement.shopifyProductId }
      );
      if (!enhancement.ProductId && matchingVariantEnhancement) {
        enhancement.ProductId = matchingVariantEnhancement.ProductVariant
          ? matchingVariantEnhancement.ProductVariant.ProductId
          : null;
        await enhancement.save();
      }
    }
  }

  async processSalsifyProducts() {
    for (const product of this.salsifyProducts) {
      const decoratedProduct = this.decorateProduct(product);
      this.productPromises.push(
        SequelizeOperations.Upsert(decoratedProduct, "sku", Product)
      );
    }
    try {
      const results = await Promise.all(this.productPromises);
      const actualResults = results.filter((result) =>
        result ? result.changed().length > 0 : result
      );
      Logger.info(
        `Created/updated ${actualResults.length} out of ${this.salsifyProducts.length} Products`
      );
    } catch (error) {
      Logger.error(error);
    }
  }

  async processSalsifyVariants() {
    for (const variant of this.salsifyVariants) {
      const decoratedVariant = this.decorateVariantWithSalsifyInfo(variant);
      const foundParent = _.find(this.products, {
        sku: variant.parent_sku,
      });
      if (foundParent) {
        decoratedVariant.ProductId = foundParent.id;
      }
      this.variantPromises.push(
        SequelizeOperations.Upsert(
          decoratedVariant,
          "sku",
          ProductVariant,
          this.productVariants
        )
      );
    }
    try {
      const results = await Promise.all(this.variantPromises);
      const actualResults = results.filter((result) =>
        result ? result.changed().length > 0 : result
      );
      Logger.info(
        `Created/updated ${actualResults.length} out of ${this.salsifyVariants.length} Products`
      );
    } catch (error) {
      Logger.error(error);
    }
  }

  async processVariants() {
    for (const variant of this.erpProductVariants) {
      const decoratedVariant = this.decorateVariant(variant);
      this.variantPromises.push(
        SequelizeOperations.Upsert(
          decoratedVariant,
          "sku",
          ProductVariant,
          this.productVariants
        )
      );
    }
    try {
      const results = await Promise.all(this.variantPromises);
      const actualResults = results.filter(
        (result) => result.changed().length > 0
      );
      Logger.info(
        `Created/updated ${actualResults.length} out of ${this.erpProductVariants.length} ProductVariants`
      );
    } catch (error) {
      Logger.error(error);
    }
  }

  decorateVariant(productVariant) {
    const decoratedVariant = {
      name: productVariant.name,
      sku: productVariant.sku,
      ean: productVariant.barcode,
      erpProductId: productVariant.WMSProductID.toString(),
      minOrderQty: 1, // TODO: add support
      brand: productVariant.brand,
      erpSnapshot: productVariant.productSnapshot, // TODO: add support
    };
    return decoratedVariant;
  }

  decorateVariantWithSalsifyInfo(salsifyVariant) {
    const decoratedVariant = {
      name: salsifyVariant.name,
      sku: salsifyVariant.sku,
      ean: salsifyVariant.barcode_ean,
      asin: salsifyVariant.asin_amazon,
      size: salsifyVariant.size,
      colour: salsifyVariant.colour,
      colourLayman: salsifyVariant.colour_layman,
      brand: salsifyVariant.brand,
      salsifyProductId: salsifyVariant.salsifyProductId,
      description: salsifyVariant.description,
    };

    return decoratedVariant;
  }

  decorateProduct(product) {
    const decoratedVariant = {
      name: product.name,
      sku: product.sku,
      salsifyProductId: product.salsifyProductId,
      description: product.description,
      brand: product.brand,
      gender: product.gender,
      minimumOrderQuantity: 1, // TODO: add support
    };
    return decoratedVariant;
  }
}

module.exports.GenericProductImport = GenericProductImport;
