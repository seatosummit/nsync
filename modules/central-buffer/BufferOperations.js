const { ShopifyProductImport } = require("./ShopifyProductImport");
const { GenericProductImport } = require("./GenericProductImport");
const { Logger } = require("../helpers/Logger");
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
const _ = require("lodash");

const {
  ShopifyBufferProductVariant,
  BlackDiamondBufferProduct,
  Product,
  ProductVariant,
  ProductEnhancement,
  ProductVariantEnhancement,
} = require("../../models");
const { promises } = require("form-data");
const { currentMemoryUsage } = require("../helpers/memoryTools");

const CleanBufferProducts = async () => {
  try {
    Logger.info(`Getting products from BLACKDIAMOND...`);
    const importer1 = new ShopifyProductImport("BLACKDIAMOND"); //TODO: this is broken, fix it
    const productsFromShopify = await importer1.getAllProducts();
    if (!productsFromShopify) throw `problem with getting Shopify products`;
    Logger.info(`Getting products from buffer...`);
    const allBufferProducts = (
      await BlackDiamondBufferProduct.findAll({})
    ).filter((product) => product.shopify_product_id);

    for (const product of allBufferProducts) {
      const foundMatchingShopifyProduct = _.find(
        productsFromShopify,
        (shopifyProduct) => {
          const match =
            product.shopify_product_id.toString() ===
            shopifyProduct.legacyResourceId;
          return match;
        }
      );
      if (!foundMatchingShopifyProduct) {
        await product.destroy();
        Logger.info(
          `Removed product ${product.shopify_product_id} with SKU ${product.sku} from buffer, it wasn't found in Shopify export`
        );
      } else {
        // Logger.info(
        //   `Product ${product.shopify_product_id} with SKU ${product.sku} exists in Shopify, not removing`
        // );
      }
    }
    Logger.info(`...done cleaning.`);
  } catch (error) {
    Logger.error(`Problems with variant clean: ${error}`);
  }
};

const CleanBufferProductVariants = async () => {
  try {
    Logger.info(`Getting products from BLACKDIAMOND...`);
    const importer1 = new ShopifyProductImport("BLACKDIAMOND"); //TODO: this is broken, fix it
    const productVariantsFromShopify = await importer1.getAllProductVariants();
    if (!productVariantsFromShopify)
      throw `problem with getting Shopify variants`;
    Logger.info(`Getting variants from buffer...`);
    const allBufferVariants = await ShopifyBufferProductVariant.findAll({});

    for (const variant of allBufferVariants) {
      const foundMatchingShopifyVariant = _.find(
        productVariantsFromShopify,
        (shopifyVariant) => {
          const match =
            variant.shopify_id.toString() === shopifyVariant.legacyResourceId;
          return match;
        }
      );
      if (!foundMatchingShopifyVariant) {
        await variant.destroy();
        Logger.info(
          `Removed variant ${variant.shopify_id} from buffer, as it wasn't found match`
        );
      } else {
        // Logger.info(
        //   `Variant ${variant.shopify_id} with SKU ${variant.shopify_sku} exists in Shopify, not removing`
        // );
      }
    }
    Logger.info(`...done cleaning.`);
  } catch (error) {
    Logger.error(`Problems with variant clean: ${error}`);
  }
};

const CreateEnhancementRelationships = async (args) => {
  const SalesChannelId = args?.SalesChannelId;
  const chunkSize = args?.chunkSize ?? 500;
  if (!SalesChannelId)
    throw `Need SalesChannelId to create enhancement relationships`;

  try {
    const ProductVariantEnhancements = await ProductVariantEnhancement.findAll({
      attributes: ["id", "sku"],
      where: {
        SalesChannelId,
      },
      include: ProductEnhancement,
    });
    const ProductVariants = await ProductVariant.findAll({
      attributes: ["id", "sku", "ProductId"],
      include: Product,
    });

    const updatePromises = [];

    for (const productVariant of ProductVariants) {
      const currentMatchingEnhancement = _.find(ProductVariantEnhancements, {
        sku: productVariant.sku,
      });
      if (currentMatchingEnhancement) {
        updatePromises.push(
          currentMatchingEnhancement.setProductVariant(productVariant)
        );
        updatePromises.push(
          currentMatchingEnhancement.setProduct(productVariant.Product)
        );
      }
    }

    const chunks = _.chunk(updatePromises, chunkSize);

    for (const chunk of chunks) {
      await Promise.all(chunk);
      currentMemoryUsage(`Processing variant relationships...`);
    }
  } catch (error) {
    Logger.error(`Issue with updating enhancement relationships: ${error}`);
  }
};
const CreateProductVariantRelationships = async () => {
  try {
    Logger.info(`Linking Variants with Products...`);

    const products = await Product.findAll({
      attributes: ["id", "sku"],
    });
    const productVariants = await ProductVariant.findAll({
      attributes: ["id", "sku", "parent_sku"],
    });

    const updatePromises = [];

    for (const variant of productVariants) {
      const matchingProduct = _.find(products, {
        sku: variant.parent_sku,
      });
      if (!matchingProduct) {
        Logger.error(
          `Variant ${variant.sku} has a parent sku ${variant.parent_sku} and can't find a match`
        );
        continue;
      }
      variant.ProductId = matchingProduct.id;
      if (!variant.modelVariant)
        variant.modelVariant = matchingProduct.modelVariant;
      if (variant.changed().length > 0) {
        const saveResult = variant.save();
        updatePromises.push(saveResult);
      }
    }

    const relationSaveResults = await Promise.all(updatePromises);

    Logger.info(`...done linking.`);
  } catch (error) {
    Logger.error(`Problems with variant clean: ${error}`);
  }
};

module.exports.CleanBufferProducts = CleanBufferProducts;
module.exports.CleanBufferProductVariants = CleanBufferProductVariants;
module.exports.CreateEnhancementRelationships = CreateEnhancementRelationships;
module.exports.CreateProductVariantRelationships =
  CreateProductVariantRelationships;
