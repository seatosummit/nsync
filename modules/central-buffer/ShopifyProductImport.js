const { ShopifyAPI } = require("../helpers/ShopifyAPI");
const _ = require("lodash");
const { v4 } = require("uuid");
const fetch = require("node-fetch");
const readline = require("readline");
const request = require("request");
require("dotenv").config();
const {
  Product,
  ProductVariant,
  ProductVariantEnhancement,
  ProductEnhancement,
  SalesChannel,
} = require("../../models");
const { Sequelize } = require("sequelize");
const Op = Sequelize.Op;
const moment = require("moment");
const { Logger } = require("../helpers/Logger");
const { ShopifyConnectionError } = require("../shopify/errors/ProductErrors");
const { PromiseTools } = require("../helpers/promisetools");
const { SequelizeOperations } = require("../sequelize/SequelizeOperations");
const { getLegacyId } = require("../helpers/stringTools");
const { currentMemoryUsage } = require("../helpers/memoryTools");

class ShopifyProductImport {
  constructor(args) {
    this.SalesChannel = args?.SalesChannel;
    if (!this.SalesChannel) throw `Need a SalesChannel to import products from`;
    this.config = this.SalesChannel.shopify_config;
    if (!this.config) throw `Need a SalesChannel config to import products`;
    this.API = new ShopifyAPI(this.config);
    this.storeSKUPrefix = this.config.skuPrefix ?? null;
    this.salesChannelMachineName = this.config.salesChannelMachineName;
    this.metafieldNamespace = this.config.metafield_namespace;
    this.salesChannelInfo;
    this.exportOperationStatus = {
      currentBulkOperation: { status: "NONE" },
    };
    this.productEnhancementPromises = [];
    this.variantEnhancementPromises = [];
    this.products = [];
    this.productVariants = [];
    this.productImages = [];
    this.metafields = [];
    this.inventoryItems = [];
  }

  async importProducts() {
    try {
      await this.connectToShopify();
      await this.API.waitUntilBulkOperationComplete({
        bulkOperationType: "QUERY",
      });
      const queryResults = await this.getAllProductsAndVariants();
      this.productDataSetsFrom(queryResults);
      Logger.info(`Found ${this.products.length} Products`);
      Logger.info(`Found ${this.productVariants.length} ProductVariants`);
      Logger.info(`Found ${this.productImages.length} Images`);
      Logger.info(`Found ${this.metafields.length} Metafields`);
      // TODO: figure out what to do with this. GraphQL doesn't seem to return these.
      // Logger.info(`Found ${this.privateMetafields.length} PrivateMetafields`);
      await this.processProductsAndTheirVariants();
    } catch (error) {
      Logger.error(`Issue with import: ${error}`);
    }
  }

  async getAllInventoryLevels() {
    await this.connectToShopify();
    const queryResults = await this.getBulkExport({
      operation: "INVENTORY_ITEMS",
    });
    this.inventoryDataSetsFrom(queryResults);
    const inventoryLevels = this.inventoryItems.map((inventoryItem) => {
      return {
        sku: inventoryItem.variant?.sku,
        barcode: inventoryItem.variant?.barcode,
        variantID: inventoryItem.variant?.id,
        inventoryID: inventoryItem.id,
        inventoryLegacyID: inventoryItem.legacyResourceId,
        inventoryQuantity: inventoryItem.inventoryQuantity,
        inventoryLocationId: inventoryItem.inventoryLocationId,
      };
    });
    return inventoryLevels;
  }

  async getAllProductsAndVariants() {
    try {
      await this.connectToShopify();
      const queryResults = await this.getBulkExport({
        operation: "PRODUCTS",
      });
      return this.productsAndVariantsFromQueryResults(queryResults);
    } catch (error) {
      Logger.error(`Issue with getting products from Shopify: ${error}`);
    }
  }

  async getAllProducts() {
    try {
      await this.connectToShopify();
      const queryResults = await this.getBulkExport({
        operation: "PRODUCTS",
      });
      return this.productsFromQueryResults(queryResults);
    } catch (error) {
      Logger.error(`Issue with getting products from Shopify: ${error}`);
    }
  }

  async getAllProductIDs() {
    try {
      await this.connectToShopify();
      const queryResults = await this.getBulkExport({
        operation: "PRODUCT_IDS",
      });
      return this.productsFromQueryResults(queryResults);
    } catch (error) {
      Logger.error(`Issue with getting products from Shopify: ${error}`);
    }
  }

  async getAllProductVariants() {
    try {
      await this.connectToShopify();
      const queryResults = await this.getBulkExport({
        operation: "PRODUCT_VARIANTS",
      });
      return this.productVariantsFromQueryResults(queryResults);
    } catch (error) {
      Logger.error(`Issue with getting variants from Shopify: ${error}`);
    }
  }

  async getAllProductImageData(args) {
    const tryUsingLatestExistingResult = args?.tryUsingLatestExistingResult;
    try {
      await this.connectToShopify();
      const queryResults = await this.getBulkExport({
        operation: "PRODUCT_IMAGES",
        tryUsingLatestExistingResult,
      });
      this.productDataSetsFrom(queryResults);
    } catch (error) {
      Logger.error(`Issue with getting product images from Shopify: ${error}`);
    }
  }

  productsAndVariantsFromQueryResults(queryResults) {
    return queryResults.filter((resultItem) => {
      if (resultItem["__typename"] === "Product") return true;
      if (resultItem["__typename"] === "ProductVariant") return true;
      return false;
    });
  }

  productsFromQueryResults(queryResults) {
    return queryResults.filter((resultItem) => {
      return resultItem["__typename"] === "Product";
    });
  }

  productVariantsFromQueryResults(queryResults) {
    return queryResults.filter((resultItem) => {
      return resultItem["__typename"] === "ProductVariant";
    });
  }

  productDataSetsFrom(queryResults) {
    this.products = queryResults.filter((resultItem) => {
      return resultItem["__typename"] === "Product";
    });
    this.productVariants = queryResults.filter((resultItem) => {
      return resultItem["__typename"] === "ProductVariant";
    });
    this.productImages = queryResults.filter((resultItem) => {
      return resultItem["__typename"] === "Image";
    });
    this.metafields = queryResults.filter((resultItem) => {
      return resultItem["__typename"] === "Metafield";
    });
    this.privateMetafields = queryResults.filter((resultItem) => {
      return resultItem["__typename"] === "PrivateMetafield";
    });
  }

  inventoryDataSetsFrom(queryResults) {
    if (!queryResults) throw `No inventory data set from GraphQL query`;
    const inventoryItems = queryResults.filter((resultItem) => {
      return resultItem["__typename"] === "InventoryItem";
    });
    for (const inventoryItem of inventoryItems) {
      const matchingInventoryLevel = _.find(queryResults, {
        __parentId: inventoryItem.id,
      });
      if (matchingInventoryLevel) {
        const inventoryItemWithLevel = {
          ...inventoryItem,
          inventoryQuantity: matchingInventoryLevel.available,
          inventoryLocationId:
            matchingInventoryLevel.location?.legacyResourceId,
        };
        this.inventoryItems.push(inventoryItemWithLevel);
      }
    }
  }

  async productEnhancementUpsert(decoratedProductEnhancement) {
    try {
      const query = {
        where: {},
      };
      if (decoratedProductEnhancement.id) {
        query.where = {
          id: decoratedProductEnhancement.id,
          SalesChannelId: this.salesChannelInfo.id,
        };
      } else {
        Logger.error(
          `productEnhancement ${decoratedProductEnhancement.GraphQLProductId} has no private metafield value`
        );
        query.where = {
          GraphQLProductId: decoratedProductEnhancement.GraphQLProductId,
          SalesChannelId: this.salesChannelInfo.id,
        };
      }
      const matchingProductEnhancement = await ProductEnhancement.findOne(
        query
      );
      if (matchingProductEnhancement) {
        return matchingProductEnhancement.update(decoratedProductEnhancement);
      } else {
        Logger.error(
          `ProductEnhancement ${decoratedProductEnhancement.GraphQLProductId} didn't have a matching Buffer ID ${decoratedProductEnhancement.id}`
        );
        return ProductEnhancement.create(decoratedProductEnhancement);
      }
    } catch (error) {
      Logger.error(error);
    }
  }

  async variantEnhancementUpsert(decoratedVariantEnhancement) {
    try {
      const matchingVariantEnhancement =
        await ProductVariantEnhancement.findOne({
          where: {
            GraphQLVariantId: decoratedVariantEnhancement.GraphQLVariantId,
            SalesChannelId: this.salesChannelInfo.id,
          },
        });
      if (matchingVariantEnhancement) {
        const updateResult = await matchingVariantEnhancement.update(
          decoratedVariantEnhancement
        );
        return updateResult;
      } else {
        const createResult = await ProductVariantEnhancement.create(
          decoratedVariantEnhancement
        );
        return createResult;
      }
    } catch (error) {
      Logger.error(error);
    }
  }

  async processProductsAndTheirVariants() {
    for (const product of this.products) {
      try {
        const relatedProductVariants = _.filter(
          this.productVariants,
          (variant) => {
            return variant["__parentId"] === product.id;
          }
        );
        const decoratedProductEnhancement =
          this.decorateProductEnhancement(product);

        const productResult = await this.productEnhancementUpsert(
          decoratedProductEnhancement
        );
        const variantsToProcess = [];
        for (const productVariant of relatedProductVariants) {
          const variantPromise = this.processVariant({
            variant: productVariant,
            ProductEnhancementId: productResult.id,
          });
          variantsToProcess.push(variantPromise);
        }
        // currentMemoryUsage(`Promise.all for Variants of ${productResult.id}`);
        await Promise.all(variantsToProcess);
      } catch (error) {
        Logger.error(`Issue with importing product ${product.id}: ${error}`);
      }
    }
  }

  async processProducts() {
    for (const product of this.products) {
      const decoratedProductEnhancement =
        this.decorateProductEnhancement(product);
      this.productEnhancementPromises.push(
        this.productEnhancementUpsert(decoratedProductEnhancement)
      );
    }
    try {
      Promise.all(this.productEnhancementPromises);
    } catch (error) {
      Logger.error(error);
    }
  }

  async processVariant(args) {
    const variant = args?.variant;
    if (!variant) throw `No variant given to process.`;
    const ProductEnhancementId = args?.ProductEnhancementId;
    const decoratedVariantEnhancement = this.decorateVariantEnhancement(
      variant,
      ProductEnhancementId
    );
    return this.variantEnhancementUpsert(decoratedVariantEnhancement);
  }

  async processVariants() {
    for (const variant of this.productVariants) {
      const decoratedVariantEnhancement =
        this.decorateVariantEnhancement(variant);
      this.variantEnhancementPromises.push(
        this.variantEnhancementUpsert(decoratedVariantEnhancement)
      );
    }
    try {
      await Promise.all(this.variantEnhancementPromises);
    } catch (error) {
      Logger.error(error);
    }
  }

  extractMetafields(args) {
    const variantId = args?.variantId;
    const productId = args?.productId;
    if (!variantId && !productId)
      throw `Neither variantId nor productId provided when finding metafields`;
    const filter = {};
    if (variantId) filter["__parentid"] = variantId;
    if (productId) filter["__parentid"] = productId;
    return _.filter(this.metafields, filter).map((metafield) => {
      return {
        key: metafield.key,
        value: metafield.key,
        namespace: metafield.key,
      };
    });
  }

  extractVariants(product) {
    return _.filter(this.metafields, {
      __parentId: product.id,
    }).map((metafield) => {
      return {
        key: metafield.key,
        value: metafield.key,
        namespace: metafield.key,
      };
    });
  }

  decorateProductEnhancement(product) {
    const bufferProductEnhancementId =
      product.bufferProductEnhancementId?.value;

    const variants = _.filter(this.productVariants, {
      __parentId: product.id,
    });
    const images = _.filter(this.productImages ?? [], {
      __parentId: product.id,
    }).map((image) => {
      const imageID = image.id.replace("gid://shopify/ProductImage/", "");
      const imageURL = image.url;
      const parentProductID = image.__parentId.replace(
        "gid://shopify/Product/",
        ""
      );
      return {
        imageID,
        imageURL,
        parentProductID,
        isMainProductImage: false,
      };
    });
    if (product.featuredImage) {
      const imageURL = product.featuredImage.url;
      const parentProductID = product.id;
      const isMainProductImage = true;
      const imageID = product.featuredImage.id.replace(
        "gid://shopify/ProductImage/",
        ""
      );
      const featuredImage = {
        imageURL,
        parentProductID,
        isMainProductImage,
        imageID,
      };
      images.push(featuredImage);
    }
    const metafields = this.extractMetafields({ productId: product.id });
    const decoratedProductEnhancement = {
      name: product.title,
      shopifyProductId: parseInt(product.legacyResourceId),
      GraphQLProductId: product.id,
      SalesChannelId: this.salesChannelInfo.id,
      shopifyImages: images,
      shopifyOptions: product.options,
      shopifyVariants: variants,
      metafields,
      status: product.status,
      updatedToShopify: product.updatedAt,
    };
    if (bufferProductEnhancementId)
      decoratedProductEnhancement.id = bufferProductEnhancementId;
    return decoratedProductEnhancement;
  }

  // TODO: the below decorator belongs in CentralBufferDecorators.
  // https://app.clickup.com/t/2fkgty6
  decorateVariantEnhancement(productVariant, ProductEnhancementId) {
    const product = _.find(this.products, { id: productVariant["__parentId"] });
    const metafields = this.extractMetafields({ variantId: productVariant.id });
    const sku = this.storeSKUPrefix
      ? `${this.storeSKUPrefix + productVariant.sku}`
      : productVariant.sku;
    const barcode_ean = productVariant.barcode
      ? Number(productVariant.barcode).toPrecision()
      : null;
    const colourEntry = productVariant.selectedOptions?.filter(
      (option) => option.name === "Color"
    )[0];
    const sizeEntry = productVariant.selectedOptions?.filter(
      (option) => option.name === "Size"
    )[0];
    const decoratedVariantEnhancement = {
      name: productVariant.title,
      shopifyProductId: parseInt(product.legacyResourceId),
      shopifyVariantId: parseInt(productVariant.legacyResourceId),
      SalesChannelId: this.salesChannelInfo.id,
      metafields,
      sku,
      barcode_ean,
      shopifyImageId: productVariant.image
        ? getLegacyId(productVariant.image.id)
        : null,
      price: productVariant.price,
      size: sizeEntry?.value,
      colour: colourEntry?.value,
      // compareToPrice: productVariant.compareAtPrice,
      shopifyInventoryQuantity: productVariant.inventoryQuantity,
      shopifyInventoryItemId: getLegacyId(productVariant.inventoryItem.id),
      minimumOrderQuantity: 1, // TODO: add support
      updatedToShopify: productVariant.updatedAt,
      shopifySnapshot: productVariant,
      enabled: product.status === "ACTIVE",
      metafields: productVariant.metafields,
      ProductEnhancementId,
      GraphQLVariantId: productVariant.id,
      GraphQLInventoryItemId: productVariant.inventoryItem.id,
    };
    return decoratedVariantEnhancement;
  }

  async connectToShopify() {
    try {
      await this.API.connect();
      this.salesChannelInfo = await this.API.getSalesChannelInfo();
    } catch (error) {
      throw new ShopifyConnectionError(error);
    }
  }

  async getBulkExport(args) {
    const operation = args?.operation;
    const tryUsingLatestExistingResult = args?.tryUsingLatestExistingResult;
    if (tryUsingLatestExistingResult) {
      this.exportOperationStatus =
        await this.API.waitUntilBulkOperationComplete({
          bulkOperationType: "QUERY",
        });
      if (this.exportOperationStatus.status === "COMPLETED") {
        const fileURL = this.exportOperationStatus.url ?? null;
        if (!fileURL) throw `No fileURL given. No bulk query was found?`;
        const rows = await this.processBulkExportResultsFile(fileURL);
        return rows;
      }
    }
    try {
      let newQueryStatus;
      switch (operation) {
        case "PRODUCT_IDS":
          newQueryStatus = await this.API.startBulkProductIDExport();
          break;
        case "PRODUCTS":
          newQueryStatus = await this.API.startBulkProductExport();
          break;
        case "PRODUCT_VARIANTS":
          newQueryStatus = await this.API.startBulkVariantExport();
          break;
        case "INVENTORY_ITEMS":
          newQueryStatus = await this.API.startBulkInventoryItemExport();
          break;
        case "PRODUCT_IMAGES":
          newQueryStatus = await this.API.startBulkProductImageDataExport();
          break;
        default:
          throw `You need to provide a bulk operation`;
      }
      this.exportOperationStatus = await this.API.checkBulkOperationProgress({
        bulkOperationType: "QUERY",
      });
      while (
        this.exportOperationStatus.currentBulkOperation.status !== "COMPLETED"
      ) {
        await PromiseTools.wait(2000);
        this.exportOperationStatus = await this.API.checkBulkOperationProgress({
          bulkOperationType: "QUERY",
        });
        Logger.info(`Bulk export in progress...`);
      }

      const URL = this.exportOperationStatus.currentBulkOperation.url;
      const rows = await this.processBulkExportResultsFile(URL);
      return rows;
    } catch (error) {
      throw `Error with bulk export: ${error.message}`;
    }
  }

  async processBulkExportResultsFile(URL) {
    return new Promise((resolve, reject) => {
      if (!URL) reject(`No URL defined?`);
      const lineBuffer = [];
      const readInterface = readline.createInterface({
        input: request.get(URL),
      });
      readInterface.on("line", function (line) {
        lineBuffer.push(JSON.parse(line));
      });
      readInterface.on("close", () => resolve(lineBuffer));
      readInterface.on("error", (err) => reject(err));
    });
  }
}

module.exports.ShopifyProductImport = ShopifyProductImport;
