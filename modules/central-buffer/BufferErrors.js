class BufferDecorationError extends Error {
  constructor(payload) {
    const amendedMessage = `Problems decorating Buffer item: ${payload}`;
    super(amendedMessage);
    this.name = "BufferDecorationError";
  }
}

module.exports.BufferDecorationError = BufferDecorationError;
