const { ShopifyAPI } = require("../helpers/ShopifyAPI");
const _ = require("lodash");

require("dotenv").config();
const {
  Product,
  ProductVariant,
  ProductVariantEnhancement,
  SalesChannel,
  SalesChannelIntegrationRuleset,
  SalesChannelMinimumOrderQuantity,
  SalesChannelCollection,
  ProductEnhancement,
  WMSProduct,
  ShopifyPricing,
  Pricing,
  PricingEnhancement,
  PriceList,
  PricingUpdate,
  SalsifyProduct,
  SalsifyProductParent,
} = require("../../models");
const { Sequelize } = require("sequelize");
const Op = Sequelize.Op;
const { Logger } = require("../helpers/Logger");
const { SequelizeOperations } = require("../sequelize/SequelizeOperations");
const {
  getLegacyId,
  skuIsZCoded,
  valueExistsAndIsNotEmpty,
} = require("../helpers/stringTools");
const { ShopifyConfig } = require("../helpers/ShopifyConfig");
const {
  isTodayBetween,
  todayIsBetween,
  stringToISODate,
  parseDateWithFormat,
} = require("../helpers/dateTools");
const { getSpecialPricingFromERP } = require("../helpers/WMSBreezeAPI");
const {
  ShopifyBufferPricing,
} = require("../shopify/products/ShopifyBufferPricing");
const WMSBreezeAPI = require("../helpers/WMSBreezeAPI");
const { IntegrationRuleset } = require("../helpers/IntegrationRuleset");
const {
  checkForRule,
  checkForRuleInRuleset,
} = require("../helpers/IntegrationRuleTools");
const {
  getERPBufferProductsUsingFilters,
} = require("../wms/products/productbuffer");
const { NumberTools } = require("../helpers/NumberTools");
const {
  EnhancementDecorators,
  CollectionDecorators,
  PricingDecorators,
} = require("./CentralBufferDecorators");
const { CentralBufferValidators } = require("./CentralBufferValidators");
const { currentMemoryUsage } = require("../helpers/memoryTools");
const { ProgressIndicators } = require("../helpers/ProgressIndicators");
const { default: slugify } = require("slugify");
const { isLength } = require("lodash");
const { createBadShopifyPayloadsCSV } = require("../helpers/fileTools");

class BufferRefresher {
  constructor(args) {
    if (!args?.SalesChannel) throw `BufferRefresher needs a SalesChannel`;
    if (!args?.SalesChannel?.SalesChannelIntegrationRuleset)
      throw `SalesChannel needs an IntegrationRuleset`;
    if (!args?.SalesChannel?.shopify_config)
      throw `SalesChannel needs a shopify_config`;
    this.variantPromises = [];
    this.products = [];
    this.productEnhancements = [];
    this.productVariants = [];
    this.productVariantEnhancements = [];
    this.salsifyProducts = [];
    this.salsifyVariants = [];
    this.helinoxErpProductVariants = [];
    this.B2CErpProductVariants = [];
    this.productLookupFromWMSView = [];
    this.erpProductVariants = [];
    this.pricingErpVariants = [];
    this.pricingEnhancementEntries = [];
    this.pricingUpdates = [];
    this.pricingEntries = [];
    this.priceLists = [];
    this.wmsAlternativeProducts = [];
    this.shopifyPricingEntries = [];
    this.updatePromises = [];
    this.promiseDates = [];
    this.integrationRulesets = [];
    this.minimumOrderQuantities = [];
    this.SalesChannelCollections = [];
    this.productCoreCategories = [];
    this.usingWMSBufferDatabase;
    this.SalesChannel = args?.SalesChannel;
    this.IntegrationRuleset = new IntegrationRuleset({
      ruleset: this.SalesChannel.SalesChannelIntegrationRuleset,
    });
    this.SalesChannelId = this.SalesChannel.id;
    this.SalesChannelConfig = this.SalesChannel.shopify_config;
    this.SalesChannelConfigLocation =
      this.SalesChannel.shopify_config?.location_id;
    this.EnhancementDecorators = new EnhancementDecorators({
      SalesChannel: this.SalesChannel,
    });
    this.failedShopifyPayloads = [];
    this.acceptableERPViewNames = [
      "vSTS_WebProductInfoV2",
      "vSTS_WebProductInfo_AF",
      "vSTS_WebProductInfo_HX",
      "vSTS_WebProductInfo_BD",
    ];
  }

  async getErpProductVariantsForPricing() {
    // TODO: shall we get this from the live source and not the buffer?
    const results = (
      await getERPBufferProductsUsingFilters({
        includeFields: ["id", "productSnapshot"],
        onlyEnabled: true,
      })
    ).map((erpBufferProduct) => {
      return PricingDecorators.erpToPricingData(erpBufferProduct);
    });
    return results;
  }

  async getErpProductVariants() {
    const results = (await getERPBufferProductsUsingFilters()).map(
      (erpBufferProduct) => {
        return {
          sku: erpBufferProduct.sku,
          stockQuantity: NumberTools.convertToInt(
            erpBufferProduct.stockQuantity
          ),
          b2b_enabled: erpBufferProduct.b2b_enabled,
          b2c_enabled: erpBufferProduct.b2c_enabled,
          retailPrice: erpBufferProduct.retailPrice,
          unitPrice: erpBufferProduct.unitPrice,
          barcode: erpBufferProduct.barcode,
          status: erpBufferProduct.status,
          promiseDate: erpBufferProduct.stockETA,
        };
      }
    );
    return results;
  }

  async getResourcesForCollection() {
    this.SalesChannelCollections = await SalesChannelCollection.findAll({
      where: {
        SalesChannelId: this.SalesChannelId,
      },
    });
  }

  async getResources(args) {
    const updateOnlyEnabled = args?.updateOnlyEnabled ?? false;
    this.specialPricingRules = await getSpecialPricingFromERP();
    this.erpProductVariants = await this.getErpProductVariants();
    this.wmsAlternativeProducts = this.erpProductVariants;

    const query = {
      include: SalesChannel,
    };

    if (this.SalesChannelId)
      query.where = {
        SalesChannelId: this.SalesChannelId,
      };

    // TODO: figure out why for-of looping through this query
    // is not returning a handful of SKUs, even though they're clearly in the results
    // query.logging = console.log;
    query.attributes = {
      exclude: ["shopifySnapshot"],
    };

    if (updateOnlyEnabled)
      query.where = {
        enabled: true,
      };
    this.productVariantEnhancements = await ProductVariantEnhancement.findAll(
      query
    );
    this.salesChannelIntegrationRulesets =
      await SalesChannelIntegrationRuleset.findAll();
  }

  async refreshPricingBuffer() {
    this.pricingEntries = await Pricing.findAll();
    this.updatePromises = [];
    for (const erpProductVariant of this.erpProductVariants) {
      const sku = erpProductVariant.sku;
      const variant_ean = erpProductVariant.barcode;
      const matchingSpecialPricing = _.find(this.specialPricingRules, {
        ItemNumber: sku,
      });
      const pricingEntry = {
        price_retail: erpProductVariant.retailPrice,
        price_warehouse: erpProductVariant.unitPrice,
        variant_sku: sku,
        variant_ean,
      };
      if (matchingSpecialPricing) {
        pricingEntry.special_price_startdate = parseDateWithFormat({
          dateString: matchingSpecialPricing.SpecialFrom,
          expectedFormat: "d/MM/yyyy tt",
        });
        pricingEntry.special_price_enddate = parseDateWithFormat({
          dateString: matchingSpecialPricing.SpecialTo,
          expectedFormat: "d/MM/yyyy tt",
        });
        pricingEntry.price_special = parseFloat(
          matchingSpecialPricing.SpecialPrice
        );
      }
      this.updatePromises.push(
        await SequelizeOperations.Upsert(
          pricingEntry,
          "variant_sku",
          Pricing,
          this.pricingEntries
        )
      );
    }
    try {
      Logger.info(
        `Sending ${this.updatePromises.length} pricing entry updates to buffer`
      );
      const updateResults = await Promise.all(this.updatePromises);
      const actualResults = updateResults.filter((result) => result);
      Logger.error(`Updated pricing for ${actualResults.length} results`);
    } catch (error) {
      Logger.error(`Problems with updating pricing`);
    }
  }

  async refreshPricingEnhancements() {
    const productVariantMOQs = await ProductVariant.findAll({
      attributes: ["id", "sku", "minOrderQty"],
    });
    this.pricingEntries = await Pricing.findAll();
    this.priceLists = await PriceList.findAll({
      where: {
        SalesChannelId: this.SalesChannelId,
      },
    });
    this.productVariantEnhancements = await ProductVariantEnhancement.findAll({
      where: {
        SalesChannelId: this.SalesChannelId,
      },
    });
    this.pricingUpdates = await PricingUpdate.findAll({
      where: {
        SalesChannelId: this.SalesChannelId,
      },
    });
    this.pricingEnhancementEntries = await PricingEnhancement.findAll({
      where: {
        SalesChannelId: this.SalesChannelId,
      },
    });
    this.pricingErpVariants = await this.getErpProductVariantsForPricing();
    currentMemoryUsage("after all resources fetched");
    this.updatePromises = [];
    const stats = { current: 0 };
    stats.total = this.pricingErpVariants.length;
    ProgressIndicators.showProgress(stats, 0);
    for (const erpVariant of this.pricingErpVariants) {
      try {
        const pricingEnhancementsForUpdateEntry = [];
        const pricingEntry = _.find(this.pricingEntries, {
          variant_sku: erpVariant.sku,
        });

        const moqProductVariant = _.find(productVariantMOQs, {
          sku: erpVariant.sku,
        });

        for (const priceList of this.priceLists) {
          const decoratedPricingEntry = PricingDecorators.pricingEnhancement({
            priceList: priceList,
            erpVariant: erpVariant,
            pricingEntry,
            salesChannelId: this.SalesChannelId,
            moqProductVariant,
          });
          const updatedPricingEnhancement = await SequelizeOperations.Upsert(
            decoratedPricingEntry,
            "sku",
            PricingEnhancement,
            this.pricingEnhancementEntries,
            [],
            true,
            "PriceListId"
          );
          pricingEnhancementsForUpdateEntry.push(updatedPricingEnhancement);
        }

        const decoratedPricingUpdate = PricingDecorators.pricingUpdate({
          pricingEnhancements: pricingEnhancementsForUpdateEntry,
          SalesChannelId: this.SalesChannelId,
          sku: erpVariant.sku,
        });

        const pricingUpdateResult = await SequelizeOperations.Upsert(
          decoratedPricingUpdate,
          "sku",
          PricingUpdate,
          this.pricingUpdates,
          [],
          true,
          "SalesChannelId"
        );

        await pricingUpdateResult.addPricingEnhancements(
          pricingEnhancementsForUpdateEntry
        );

        ProgressIndicators.showProgress(stats, 1);
      } catch (error) {
        Logger.error(
          `Problem with updating PricingEnhancements for SKU ${erpVariant.sku}: ${error}`
        );
      }
    }
    try {
      currentMemoryUsage("after running all updates");
      Logger.info(`Updated PricingEnhancements for ${stats.total} results`);
    } catch (error) {
      Logger.error(`Problems with updating PricingEnhancements`);
    }
  }

  async linkPricingEnhancementsWithProductVariants() {
    this.productVariantEnhancements = await ProductVariantEnhancement.findAll({
      where: {
        SalesChannelId: this.SalesChannelId,
      },
    });
    this.pricingEnhancementEntries = await PricingEnhancement.findAll({
      include: ProductVariantEnhancement,
      where: {
        SalesChannelId: this.SalesChannelId,
      },
    });

    for (const pricingEnhancement of this.pricingEnhancementEntries) {
      try {
        if (!pricingEnhancement.ProductVariantEnhancement) {
          const matchingVariantEnhancement = _.find(
            this.productVariantEnhancements,
            {
              sku: pricingEnhancement.sku,
            }
          );
          if (matchingVariantEnhancement) {
            await pricingEnhancement.setProductVariantEnhancement(
              matchingVariantEnhancement
            );
          }
        }
      } catch (error) {
        Logger.error(
          `Problem with associating pricing and variant enhancement for PricingEnhancement ${pricingEnhancement.id}`
        );
      }
    }
  }

  rulesetForChannel(args) {
    const SalesChannelId = args?.SalesChannelId;
    const ruleSetForThisProductsChannel = _.find(
      this.salesChannelIntegrationRulesets,
      {
        SalesChannelId,
      }
    );

    if (!ruleSetForThisProductsChannel)
      throw `No ruleset found for this channel`;
    return ruleSetForThisProductsChannel;
  }

  isProductActive(args) {
    const ERPProduct = args?.ERPProduct;
    const ruleset = args?.ruleset;
    if (!valueExistsAndIsNotEmpty(ERPProduct)) {
      throw `No ERP Product given, can't check enabled status.`;
    }
    if (!valueExistsAndIsNotEmpty(ruleset)) {
      throw `No Sales Channel Ruleset given`;
    }

    const validator = new IntegrationRuleset({
      ruleset,
    });
    const productIsValid = validator.productPassesEnabledRules({
      productInstance: ERPProduct,
    });

    return productIsValid;
  }

  async refreshAllProductEnhancements() {
    let errorsCount = 0;
    const ProductEnhancements = await ProductEnhancement.findAll({
      include: [ProductVariantEnhancement, Product],
      where: {
        SalesChannelId: this.SalesChannelId,
      },
    });

    const ProductVariantEnhancementsBuffer =
      await ProductVariantEnhancement.findAll({
        where: {
          SalesChannelId: this.SalesChannelId,
        },
      });

    for (const productEnhancement of ProductEnhancements) {
      try {
        let payload = productEnhancement.shopifyPayload?.input;
        if (!payload) {
          Logger.info(
            `No Shopify payload on ProductEnhancement ${productEnhancement.id}, constructing one...`
          );
          const newProductEnhancement =
            this.EnhancementDecorators.newProductEnhancement({
              currentProductEnhancement: productEnhancement,
              Product: productEnhancement.Product,
              variantsArray: productEnhancement.ProductVariantEnhancements,
              SalesChannel: this.SalesChannel,
            });
          payload = newProductEnhancement.shopifyPayload?.input;
        }
        const updatedPayloadVariants = [];
        for (const variant of payload.variants) {
          const newVariantInfo = _.find(
            ProductVariantEnhancementsBuffer,
            (variantEnhancement) => {
              const match = variantEnhancement.sku == variant.sku;
              if (!match) {
                // Logger.error(
                //   `NOT FOUND: Enhancement: ${variantEnhancement.sku} vs variant: ${variant.sku}`
                // );
              } else {
                // Logger.info(
                //   `FOUND: Enhancement: ${variantEnhancement.sku} vs variant: ${variant.sku}`
                // );
              }
              return match;
            }
          );
          if (!newVariantInfo) {
            Logger.error(
              `No corresponding VariantEnhancement found for payload variant SKU ${variant.sku}, productEnhancement ID ${productEnhancement.id}`
            );
          }
          // TODO: figure out how to get these details for some variant enhancements
          // TODO: THIS IS STILL NOT WORKING. Wrong price coming through!
          const decoratedPayloadVariant =
            this.EnhancementDecorators.updatedPayloadVariant({
              existingVariant: variant,
              newVariantInfo,
            });
          updatedPayloadVariants.push(decoratedPayloadVariant);
        }

        const updatedPayload =
          this.EnhancementDecorators.updateExistingShopifyPayload({
            payload,
            Product: productEnhancement.Product,
            variants: updatedPayloadVariants,
            namespace:
              this.SalesChannel?.shopify_config?.metafield_namespace ??
              "b2b_dist",
          });

        const shopifyPayloadValidationResult =
          CentralBufferValidators.validShopifyProductPayload({
            shopifyProductPayload: updatedPayload,
            ProductEnhancementId: productEnhancement.id,
            productSKU: productEnhancement.Product.sku,
          });

        if (!shopifyPayloadValidationResult.pass) {
          errorsCount++;
          this.failedShopifyPayloads.push({
            productEnhancementId: productEnhancement.id,
            productSKU,
            error: shopifyPayloadValidationResult.errors,
          });
          productEnhancement.validPayload = false;
          await productEnhancement.save();
          throw `Product Enhancement ${productEnhancement.id} didn't pass validation: ${shopifyPayloadValidationResult.errors}`;
        }

        await createBadShopifyPayloadsCSV({
          arrayOfPayloads: this.failedShopifyPayloads,
        });

        productEnhancement.validPayload = true;
        productEnhancement.set({ shopifyPayload: null });
        productEnhancement.set({ shopifyPayload: updatedPayload });
        if (productEnhancement.changed().length > 0)
          await productEnhancement.save();
      } catch (error) {
        Logger.error(`Problem with updating Product Enhancement: ${error}`);
      }
    }
    Logger.info(
      `${errorsCount} errors out of ${ProductEnhancements.length} Product Enhancements`
    );
  }

  async refreshBufferInventory() {
    //
  }

  async refreshAllVariantPricingAndInventory(args) {
    const updatePrices = args?.updatePrices ?? false;
    try {
      // basic pricing for standard Shopify prices
      this.pricingEntries = await Pricing.findAll();
      const channelName = this.SalesChannel.machineName;

      if (["SEA TO SUMMIT", "CAMELBAK"].includes(channelName)) {
        Logger.info(`Getting product data for ${channelName}...`);
        this.productLookupFromWMSView =
          await this.getProductDataForSalesChannel({
            salesChannelViewName: "vSTS_WebProductInfo_AF",
          });
      }
      if (["BLACKDIAMOND"].includes(channelName)) {
        Logger.info(`Getting product data for ${channelName}...`);
        this.productLookupFromWMSView =
          await this.getProductDataForSalesChannel({
            salesChannelViewName: "vSTS_WebProductInfo_BD",
          });
      }
      if (channelName === "HELINOX") {
        Logger.info(`Getting product data for HELINOX...`);
        this.productLookupFromWMSView =
          await this.getProductDataForSalesChannel({
            salesChannelViewName: "vSTS_WebProductInfo_HX",
          });
      }
      if (channelName === "B2BDIST") {
        Logger.info(`Getting data for B2BDIST channel`);
        this.productLookupFromWMSView =
          await this.getProductDataForSalesChannel({
            salesChannelViewName: "vSTS_WebProductInfoV2",
          });
      }
      Logger.info(`Processing buffer...`);
      for (const productVariantEnhancement of this.productVariantEnhancements) {
        try {
          const sku = productVariantEnhancement.sku;
          if (!sku)
            throw `No SKU defined for variant enhancement ${productVariantEnhancement.id}`;
          const currentSalesChannel = productVariantEnhancement.SalesChannel;
          if (!currentSalesChannel) {
            throw `Variant enhancement ${productVariantEnhancement.id} is not associated with a Sales Channel`;
          }
          const currentRuleset = this.rulesetForChannel({
            SalesChannelId: currentSalesChannel.id,
          });
          const channelName = currentSalesChannel.machineName;

          let matchingERPProduct = _.find(this.productLookupFromWMSView, {
            sku,
          });
          this.usingWMSBufferDatabase = false;
          // TODO: decide if we're default into buffer DB or just error out.
          // This will become clearer as we move to new ERP.

          // if (!matchingERPProduct) {
          // TODO: look through WMS buffer results as well: hack for now
          // this.usingWMSBufferDatabase = true;
          // matchingERPProduct = _.find(this.wmsAlternativeProducts, { sku });
          // }

          if (!matchingERPProduct) {
            if (
              productVariantEnhancement.enabled ||
              productVariantEnhancement.shopifyInventoryQuantity > 0
            ) {
              Logger.error(
                `No matching ERP product found for ${channelName} buffer variant ${sku}. Disabling it.`
              );
              productVariantEnhancement.enabled = false;
              productVariantEnhancement.shopifyInventoryQuantity = 0;
              if (
                checkForRuleInRuleset({
                  ruleset: currentRuleset,
                  rule: "productRemove",
                  storeName: channelName,
                })
              ) {
                //TODO: actually delete product here
                Logger.info(
                  `Set stock to zero on disabled variant ${sku} on channel ${channelName}`
                );
              } else {
                Logger.error(
                  `Sales channel ${channelName} is not allowed to delete disabled products`
                );
              }
            }

            productVariantEnhancement.disable_reason =
              "No ERP product SKU match";
            if (productVariantEnhancement.changed().length > 0)
              await productVariantEnhancement.save();
            continue;
          }

          const updatedVariantStatus = this.updatedVariantEnhancementStatus({
            ERPProduct: matchingERPProduct,
            productVariantEnhancement,
            ruleset: currentRuleset,
          });
          productVariantEnhancement.set(updatedVariantStatus);
          if (updatedVariantStatus.enabled === false) {
            productVariantEnhancement.set({ shopifyInventoryQuantity: 0 });
            // TODO: add a remove flag. This is zero stock because this variant is disabled
            if (productVariantEnhancement.changed().length > 0) {
              Logger.info(
                `Disabled ${productVariantEnhancement.sku} on channel ${channelName}: ${productVariantEnhancement.disable_reason}`
              );
              await productVariantEnhancement.save();
            }
            continue;
          }

          const updatedVariantStock = this.updatedVariantEnhancementStock({
            ERPProduct: matchingERPProduct,
            productVariantEnhancement,
          });
          productVariantEnhancement.set(updatedVariantStock);

          if (updatePrices) {
            const updatedVariantPricing = this.updatedVariantEnhancementPricing(
              {
                productVariantEnhancement,
              }
            );
            productVariantEnhancement.set(updatedVariantPricing);
          }

          const changesArray = productVariantEnhancement.changed()
            ? productVariantEnhancement.changed()
            : [];

          if (changesArray.length > 0) {
            await productVariantEnhancement.save();
            const updates = changesArray.join(", ");
            Logger.info(
              `Updated variant enhancement ID ${productVariantEnhancement.id}, SKU ${productVariantEnhancement.sku}, update for these columns: ${updates}`
            );
          }
        } catch (error) {
          Logger.error(
            `Issue with variant enhancement id ${productVariantEnhancement.id}: ${error}`
          );
        }
      }
    } catch (error) {
      Logger.error(`Error with buffer update: ${error}`);
    }
  }

  async getProductDataForSalesChannel(args) {
    try {
      const salesChannelViewName = args?.salesChannelViewName;
      if (!salesChannelViewName)
        throw `Need a salesChannelViewName to process variant enhancements`;
      if (!this.acceptableERPViewNames.includes(salesChannelViewName))
        throw `The given salesChannelViewName is not valid, can't update variant enhancements`;

      const productVariantResults = (
        await WMSBreezeAPI.selectFromView(salesChannelViewName)
      ).map((productVariant) => {
        const promiseDateResult = _.find(this.erpProductVariants, {
          sku: productVariant.sku,
        });
        return {
          sku: productVariant.sku,
          stockQuantity: productVariant.qtyavailable
            ? NumberTools.convertToInt(productVariant.qtyavailable)
            : NumberTools.convertToInt(productVariant.stockqty),
          b2b_enabled: productVariant.b2b_enabled,
          b2c_enabled: productVariant.b2c_enabled,
          status: productVariant.customlist2,
          promiseDate: promiseDateResult?.promiseDate,
        };
      });
      if (!valueExistsAndIsNotEmpty(productVariantResults))
        throw `Got an empty resultset back from ERP for View ${salesChannelViewName}`;
      return productVariantResults;
    } catch (error) {
      throw `Problems getting product data from ERP: ${error}`;
    }
  }

  updatedVariantEnhancementStatus(args) {
    const ERPProduct = args?.ERPProduct;
    const productVariantEnhancement = args?.productVariantEnhancement;
    const ruleset = args?.ruleset;
    if (!productVariantEnhancement) throw `No Variant Enhancement provided`;
    if (!ruleset) throw `No SalesChannel ruleset provided`;

    const productVariantValidationResult = this.isProductActive({
      ERPProduct,
      ruleset,
    });
    if (productVariantValidationResult.passed === false) {
      productVariantEnhancement.enabled = false;
      productVariantEnhancement.disable_reason =
        productVariantValidationResult.disable_reason;
    } else {
      productVariantEnhancement.enabled = true;
      productVariantEnhancement.disable_reason = null;
    }
    return productVariantEnhancement.toJSON();
  }

  updatedVariantEnhancementStock(args) {
    const productVariantEnhancement = args?.productVariantEnhancement;
    const ERPProduct = args?.ERPProduct;
    if (!productVariantEnhancement) {
      throw `Didn't receive a ProductVariantEnhancement to work on`;
    }
    if (!ERPProduct) {
      throw `Didn't receive an ERP Product to work on`;
    }

    if (!valueExistsAndIsNotEmpty(ERPProduct.stockQuantity)) {
      Logger.error(
        `There's no buffer stock quantity set for SKU ${productVariantEnhancement.sku}.`
      );
    }

    const salesChannelName = productVariantEnhancement.SalesChannel.machineName;

    if (productVariantEnhancement.enabled === false) {
      if (
        checkForRule({
          ShopifyStore: productVariantEnhancement.SalesChannel,
          rule: "productRemove",
        })
      ) {
        productVariantEnhancement.shopifyInventoryQuantity = 0;
        Logger.info(
          `Set the stock to zero for ${productVariantEnhancement.sku} on channel ${salesChannelName}`
        );
      } else {
        Logger.error(
          `Sales channel ${salesChannelName} is not allowed to zero stock levels on disabled products`
        );
      }
    } else {
      productVariantEnhancement.shopifyInventoryQuantity = parseInt(
        ERPProduct.stockQuantity ?? 0
      );
    }
    return productVariantEnhancement.toJSON();
  }

  updatedVariantEnhancementPricing(args) {
    const productVariantEnhancement = args?.productVariantEnhancement;
    if (!productVariantEnhancement) {
      throw `Didn't receive a ProductVariantEnhancement to work on`;
    }
    const matchingPricingEntry = _.find(this.pricingEntries, {
      variant_sku: productVariantEnhancement.sku,
    });

    if (!matchingPricingEntry) {
      throw `Pricing entry for ${productVariantEnhancement.sku} not found`;
    }

    const usingNewPricingEnhancements =
      this.IntegrationRuleset.usePricingEnhancements;
    // TODO: this pricing setup is in a transitional state.
    // B2B is using a new system that supports pricing tiers, other sites the older one.
    // https://app.clickup.com/t/2f88871
    if (usingNewPricingEnhancements) {
      const defaultFieldToUse =
        this.IntegrationRuleset.defaultPriceFieldName ?? "price_retail";
      productVariantEnhancement.price =
        matchingPricingEntry[`${defaultFieldToUse}`];
      productVariantEnhancement.warehousePrice =
        matchingPricingEntry["price_warehouse"];
      productVariantEnhancement.compareToPrice = null;
      return productVariantEnhancement.toJSON();
    } else {
      // TODO: below is the old pricing system, that relies on the Pricing entries instead.
      // This is all D2C sites.
      const specialStart = matchingPricingEntry.special_price_startdate;
      const specialEnd = matchingPricingEntry.special_price_enddate;
      const hasSpecialPricing =
        specialStart && specialEnd
          ? todayIsBetween(specialStart, specialEnd)
          : false;
      const defaultFieldToUse =
        this.IntegrationRuleset.defaultPriceFieldName ?? "price_retail";
      if (hasSpecialPricing) {
        productVariantEnhancement.compareToPrice =
          matchingPricingEntry[`${defaultFieldToUse}`];
        productVariantEnhancement.price = matchingPricingEntry.price_special;
      } else {
        productVariantEnhancement.price =
          matchingPricingEntry[`${defaultFieldToUse}`];
      }
      productVariantEnhancement.warehousePrice =
        matchingPricingEntry.price_warehouse;
    }

    return productVariantEnhancement.toJSON();
  }

  async updateProductAndVariantStatuses() {
    this.integrationRulesets = (
      await SalesChannelIntegrationRuleset.findAll()
    ).map((ruleset) => {
      return new IntegrationRuleset({ ruleset });
    });

    this.erpProductVariants = WMSProduct.findAll({});

    this.productVariantEnhancements = await ProductVariantEnhancement.findAll({
      attributes: [
        "id",
        "SalesChannelId",
        "sku",
        "barcode_ean",
        "enabled",
        "price",
        "shopifyInventoryQuantity",
      ],
    });

    this.erpProductVariants = await WMSProduct.findAll({
      attributes: [
        "sku",
        "name",
        "barcode",
        "status",
        "b2c_enabled",
        "b2b_enabled",
        "stockETA",
        "retailPrice",
      ],
    });

    for (const variantEnhancement of this.productVariantEnhancements) {
      // need to find this in a diff way
      const ruleset = _.find(this.integrationRulesets, {
        SalesChannelId: variantEnhancement.SalesChannelId,
      });
      if (!ruleset) {
        Logger.error(
          `Couldn't find matching sales channel rules for variant enhancement id ${variantEnhancement.id}`
        );
        continue;
      }

      const matchingErpProduct = _.find(this.erpProductVariants, {
        sku: variantEnhancement.sku,
      });

      if (!matchingErpProduct) {
        if (variantEnhancement.enabled)
          await variantEnhancement.update({ enabled: false });
        // Logger.info(
        //   `variant enhancement ${variantEnhancement.id} wasn't found in the WMS buffer. Disabling.`
        // );
        continue;
      }

      const productEnabledRules = ruleset.productEnabledRules;

      const productForEnableCheck = {
        ...variantEnhancement.toJSON(),
        ...matchingErpProduct.toJSON(),
      };
      const shouldThisBeEnabled = productPassesEnabledRules({
        productForEnableCheck,
        productEnabledRules,
      });
      variantEnhancement.enabled = shouldThisBeEnabled;
      if (!shouldThisBeEnabled) variantEnhancement.shopifyInventoryQuantity = 0;
      if (variantEnhancement.changed().length > 0) {
        await variantEnhancement.save();
      }
    }

    // get all product enhancements (with variants)
    // loop through all product enhancements and check if there's more than one enabled variant. if not, product goes into draft mode.
  }

  async getResourcesForEnhancementCreation() {
    if (!this.SalesChannel)
      throw `Please provide a SalesChannel before creating new Product / Variant enhancements.`;
    this.products = await Product.findAll({
      include: ProductVariant,
    });
    this.erpProductVariants = await this.getErpProductVariants();
    this.productVariantEnhancements = await ProductVariantEnhancement.findAll({
      where: {
        SalesChannelId: this.SalesChannelId,
      },
    });
    this.productEnhancements = await ProductEnhancement.findAll({
      where: {
        SalesChannelId: this.SalesChannelId,
      },
    });
    this.minimumOrderQuantities =
      await SalesChannelMinimumOrderQuantity.findAll({
        where: {
          SalesChannelId: this.SalesChannelId,
        },
      });
  }

  async createNewProductAndVariantEnhancements() {
    if (!this.SalesChannel)
      throw `Please provide a SalesChannel before creating new Product / Variant enhancements.`;
    for (const Product of this.products) {
      try {
        const variantProcessingResults = await this.processRelatedVariants({
          Product, // TODO: just pass the Product
          variantsArray: Product.ProductVariants,
        });

        if (variantProcessingResults.length > 0) {
          const productEnhancementEntry =
            this.EnhancementDecorators.newProductEnhancement({
              Product,
              variantsArray: variantProcessingResults,
              SalesChannel: this.SalesChannel,
            });

          const productEnhancementResult = await SequelizeOperations.Upsert(
            productEnhancementEntry,
            "ProductId",
            ProductEnhancement,
            this.productEnhancements,
            [],
            true
          );
          const amendedPayload = {
            input: {
              ...productEnhancementResult.shopifyPayload.input,
              privateMetafields: [
                {
                  key: "bufferProductEnhancementId",
                  namespace: "b2b_dist",
                  valueInput: {
                    value: productEnhancementResult.id,
                    valueType: "STRING",
                  },
                },
              ],
            },
          };
          productEnhancementResult.set({ shopifyPayload: null });
          productEnhancementResult.set({
            shopifyPayload: amendedPayload,
          });
          if (productEnhancementResult.changed().length > 0)
            await productEnhancementResult.save();
          await this.linkVariantEnhancements({
            ProductEnhancementId: productEnhancementResult.id,
            VariantEnhancements: variantProcessingResults,
          });
        }
      } catch (error) {
        Logger.error(`Problem with processing Product ${Product.id}: ${error}`);
      }
    }
  }

  async linkVariantEnhancements(args) {
    const ProductEnhancementId = args?.ProductEnhancementId;
    const VariantEnhancements = args?.VariantEnhancements ?? [];
    if (!ProductEnhancementId) throw `No Product Enhancement Id given`;
    const variantEnhancementUpdates = VariantEnhancements.map((enhancement) => {
      return enhancement.update({ ProductEnhancementId });
    });
    try {
      await Promise.all(variantEnhancementUpdates);
    } catch (error) {
      Logger.error(`Problem with linking Variant and Product enhancements`);
    }
  }

  async processRelatedVariants(args) {
    const variantsArray = args?.variantsArray ?? [];
    const Product = args?.Product;
    if (!variantsArray)
      throw `No variantsArray for processing related variants`;
    if (!Product) throw `No Product for processing related variants`;
    const processedVariants = [];
    for (const ProductVariant of variantsArray) {
      const ERPProduct = _.find(this.erpProductVariants, {
        sku: ProductVariant.sku,
      });
      if (ERPProduct) {
        const productVariantTest =
          this.IntegrationRuleset.productPassesEnabledRules({
            productInstance: ERPProduct,
          });
        if (!productVariantTest.passed) continue;

        const matchingMoq = _.find(this.minimumOrderQuantities, {
          sku: ProductVariant.sku,
        }); // TODO: this can be a relation
        ProductVariant.minOrderQty = matchingMoq?.minimumOrderQuantity ?? 1;
        const productVariantEnhancementEntry =
          this.EnhancementDecorators.newVariantEnhancement({
            ERPProduct,
            Product,
            ProductVariant,
            SalesChannel: this.SalesChannel,
          });

        const result = await SequelizeOperations.Upsert(
          productVariantEnhancementEntry,
          "sku",
          ProductVariantEnhancement,
          this.productVariantEnhancements,
          [],
          true
        );
        processedVariants.push(result);
      }
    }
    return processedVariants;
  }

  async refreshAllCollections() {
    const productErpCategories = (
      await Product.findAll({
        raw: true,
        attributes: ["erp_category", "erp_sub_category"],
        group: ["erp_category", "erp_sub_category"],
      })
    ).filter((item) => {
      return (
        valueExistsAndIsNotEmpty(item.erp_category) &&
        valueExistsAndIsNotEmpty(item.erp_sub_category)
      );
    });

    const mainCategories = new Set();
    productErpCategories.forEach((categoryEntry) => {
      mainCategories.add(categoryEntry.erp_category);
    });
    const subCategories = new Set();
    productErpCategories.forEach((categoryEntry) => {
      subCategories.add(
        `${categoryEntry.erp_category} ${categoryEntry.erp_sub_category}`
      );
    });

    const allCategories = [
      ...Array.from(mainCategories),
      ...Array.from(subCategories),
    ];

    for (const categoryItem of allCategories) {
      try {
        const salesChannelCategoryEntry =
          CollectionDecorators.SalesChannelCollectionEntry({
            entry: categoryItem,
            SalesChannel: this.SalesChannel,
          });

        const salesChannelCategoryEntryResult =
          await SequelizeOperations.Upsert(
            salesChannelCategoryEntry,
            "handle",
            SalesChannelCollection,
            this.SalesChannelCollections
          );
        if (salesChannelCategoryEntryResult)
          Logger.info(
            `Created new buffer Collection ${salesChannelCategoryEntryResult.title}`
          );
      } catch (error) {
        Logger.error(`Problem with Collection ${categoryItem}: ${error}`);
      }
    }
  }
}

module.exports.BufferRefresher = BufferRefresher;
