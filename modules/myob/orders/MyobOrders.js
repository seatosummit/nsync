const { Sequelize } = require("sequelize");
const Op = Sequelize.Op;
const _ = require("lodash");
const { Logger } = require("../../helpers/Logger");
const { valueExistsAndIsNotEmpty } = require("../../helpers/stringTools");
const { MyobAPI } = require("../../helpers/MyobAPI");

class MyobOrders {
  constructor() {
    this.MYOB = new MyobAPI();
  }

  async getOrders() {
    try {
      await this.MYOB.connect();
      const orders = await this.MYOB.getSalesOrders();
      return orders;
    } catch (error) {
      throw error;
    }
  }
  async getShipments() {
    try {
      await this.MYOB.connect();
      const orders = await this.MYOB.getShipments();
      return orders;
    } catch (error) {
      throw error;
    }
  }
}

module.exports = MyobOrders;
