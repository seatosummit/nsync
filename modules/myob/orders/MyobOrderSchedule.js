const { Logger } = require("../../helpers/Logger");
const MyobOrders = require("./MyobOrders");

const exportMyobOrders = async () => {
  const myobOrders = new MyobOrders();
  const orders = await myobOrders.getOrders();
  const shipments = await myobOrders.getShipments();
  Logger.info(`got back ${orders.length} SalesOrder entities`);
  Logger.info(`got back ${shipments.length} Shipment updates`);
  process.exit();
};

module.exports.exportMyobOrders = exportMyobOrders;
