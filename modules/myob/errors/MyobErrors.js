class MyobConnectionError extends Error {
  constructor(payload) {
    const amendedMessage = `Problems connecting to MYOB: ${payload}`;
    super(amendedMessage);
    this.name = "MyobConnectionError";
  }
}
class MyobAPIError extends Error {
  constructor(payload) {
    const amendedMessage = `Can't make the MYOB API call: ${payload}`;
    super(amendedMessage);
    this.name = "MyobAPIError";
  }
}

module.exports.MyobConnectionError = MyobConnectionError;
module.exports.MyobAPIError = MyobAPIError;
