const { ShopifyBuffer } = require("../../shopify/products/ShopifyBuffer");
const {
  BlackDiamondProductCategoryDefinition,
  SalsifyDigitalAsset,
  BlackDiamondBufferProductVariant,
} = require("../../../models");
class BDBuffer extends ShopifyBuffer {
  constructor(
    ShopifyProduct,
    ShopifyProductVariant,
    ShopifyBufferCollection,
    BDBufferImage
  ) {
    super(
      ShopifyProduct,
      ShopifyProductVariant,
      ShopifyBufferCollection,
      BDBufferImage,
      BlackDiamondProductCategoryDefinition,
      BlackDiamondBufferProductVariant
    );
    this.collectionExceptions = [
      {
        original: "apparel_mens_jackets_shell_rain",
        converted: "Apparel Men's Jackets Shell & Rain",
      },
      {
        original: "apparel_mens_pants_shell_rain",
        converted: "Apparel Men's Pants Shell & Rain",
      },
      {
        original: "apparel_mens_tops_tees_tanks",
        converted: "Apparel Men's Tops Tees & Tanks",
      },
      {
        original: "apparel_womens_jackets_shell_rain",
        converted: "Apparel Women's Jackets Shell & Rain",
      },
      {
        original: "apparel_womens_pants_shell_rain",
        converted: "Apparel Women's Pants Shell & Rain",
      },
      {
        original: "apparel_womens_pants_tights_capris",
        converted: "Apparel Women's Pants Tights & Capris",
      },
      {
        original: "apparel_womens_tops_tees_tanks",
        converted: "Apparel Women's Tops Tees & Tanks",
      },
      {
        original: "equipment_climb_ice_alpine",
        converted: "Equipment Climb Ice & Alpine",
      },
      {
        original: "equipment_climb_packs_bags",
        converted: "Equipment Climb Packs & Bags",
      },
      {
        original: "equipment_climb_quickdraws_runners",
        converted: "Equipment Climb Quickdraws & Runners",
      },
      {
        original: "equipment_climb_quickdraws_runners",
        converted: "Equipment Climb Quickdraws & Runners",
      },

      {
        original: "equipment_climb_chalk_accessories",
        converted: "Equipment Climb Chalk & Accessories",
      },

      {
        original: "equipment_tents_shelters",
        converted: "Equipment Tents & Shelters",
      },
      {
        original: "equipment_ski_snowsafety",
        converted: "Equipment Ski Snow Safety",
      },
      {
        original: "equipment_ski_tractiondevice",
        converted: "Equipment Ski Traction Device",
      },
      {
        original: "equipment_climb_skincare",
        converted: "Equipment Climb Skin Care",
      },
      {
        original: "experience_activities_skiing_snowboarding",
        converted: "Experience Activities Skiing & Snowboarding",
      },
    ];
    this.replacementRules = [
      {
        original: "Mens",
        replacement: "Men's",
      },
      {
        original: "Womens",
        replacement: "Women's",
      },
      {
        original: "Baselayer",
        replacement: "Base Layer",
      },
      {
        original: "Tractiondevice",
        replacement: "Traction Device",
      },
      {
        original: "Skincare",
        replacement: "Skin Care",
      },
      {
        original: "Climbingskins",
        replacement: "Climbing Skins",
      },
      {
        original: "Boulderingpads",
        replacement: "Bouldering Pads",
      },
      {
        original: "Rockprotection",
        replacement: "Rock Protection",
      },
    ];
  }

  async getAssetsForBufferUpdate() {
    await super.getAssetsForBufferUpdate("Black Diamond", "BLACKDIAMOND");
  }

  async getAssetsForCollectionUpdate() {
    this.salsifyBufferProductParents =
      await this.SalsifyBufferProductParent.findAll({
        where: { brand: "Black Diamond" },
      });
    this.salsifyBufferProducts = await this.SalsifyBufferProduct.findAll({
      where: { brand: "Black Diamond" },
    });
    this.categoryDefinitions = await this.CategoryDefinitionModel.findAll({});
  }

  async getSalsifyParentsFromBuffer() {
    this.salsifyBufferProductParents =
      await this.SalsifyBufferProductParent.findAll({
        where: { brand: "Black Diamond" },
      });
  }

  async getAssetsForImageBufferUpdate() {
    this.salsifyBufferDigitalAssets = await SalsifyDigitalAsset.findAll({});

    this.salsifyBufferProductParents =
      await this.SalsifyBufferProductParent.findAll({
        where: { brand: "Black Diamond" },
      });
    this.salsifyBufferProducts = await this.SalsifyBufferProduct.findAll({
      where: { brand: "Black Diamond" },
    });
    this.brandfolderBufferAssets = await this.BrandfolderBufferAsset.findAll({
      where: {
        salsify_brand: "Black Diamond",
      },
    });
    await super.getAllShopifyBufferProducts();
  }

  async updateCategoryDefinitionsFromSalsify() {
    await super.updateCategoryDefinitionsFromSalsify(
      "Black Diamond Shopify Category"
    );
  }
}

module.exports.BDBuffer = BDBuffer;
