const _ = require("lodash");
const {
  ShopifyBufferProduct,
} = require("../../shopify/products/ShopifyBufferProduct");
const {
  createSlug,
  addSpacesAndCapitalise,
} = require("../../helpers/stringTools");

class BDBufferProduct extends ShopifyBufferProduct {
  constructor(
    SalsifyBufferParent,
    allSalsifyProductsForThisParent,
    allWMSKidsForThisParent,
    categoryDefinitionReference,
    pricingBufferEntries,
    ShopifyBufferProductDBModel
  ) {
    super(
      SalsifyBufferParent,
      allSalsifyProductsForThisParent,
      allWMSKidsForThisParent,
      categoryDefinitionReference,
      pricingBufferEntries,
      ShopifyBufferProductDBModel
    );

    this.metadata.gender = {
      key: "gender",
      value: "",
      value_type: "string",
      owner_resource: "product",
      namespace: "blackdiamond", // TODO: this from config
    };
    this.metadata.tech_specs = {
      key: "tech_specs",
      value: "",
      value_type: "json_string",
      owner_resource: "product",
      namespace: "blackdiamond", // TODO: this from config
    };
    this.metadata.usps = {
      key: "usps",
      value: "Men's",
      value_type: "json_string",
      owner_resource: "product",
      namespace: "blackdiamond", // TODO: this from config
    };

    this.attributes.gender = "";
    this.attributes.tech_specs = [];
    this.attributes.usps = [];
    this.attributes.categories = [];
  }

  decorateSimpleProduct() {
    super.decorateSimpleProduct();
    const allSalsifyAttributes = this.SalsifyBufferParent.snapshot;
    const gender = allSalsifyAttributes["Gender"];
    this.attributes.gender = gender;

    const blackDiamondSalsifyCategories = allSalsifyAttributes[
      "Black Diamond Shopify Category"
    ]
      ? [].concat(allSalsifyAttributes["Black Diamond Shopify Category"])
      : [];

    let formattedSalsifyCategories = [];
    for (const salsifyCategoryID of blackDiamondSalsifyCategories) {
      const matchingCategory = _.find(this.categoryDefinitionReference, {
        salsify_id: salsifyCategoryID,
      });
      if (matchingCategory) {
        formattedSalsifyCategories.push(matchingCategory.custom_name);
      }
    }

    this.attributes.tags.push(gender, ...formattedSalsifyCategories);
    this.attributes.categories.push(
      createSlug(gender),
      ...formattedSalsifyCategories
    );

    this.attributes.usps = _.values(
      _.pickBy(allSalsifyAttributes, function (value, key) {
        return /USP \d/.test(key);
      })
    );

    let setOfTechSpecs = new Set();
    const techSpecsRegex = /Tech spec/;
    const lengthOfTechSpecsSlice = 11;
    for (const key in allSalsifyAttributes) {
      if (techSpecsRegex.test(key)) {
        const element = allSalsifyAttributes[key];
        const keySliced = key.slice(lengthOfTechSpecsSlice);
        const keyWithFirstCap = keySliced[0].toUpperCase() + keySliced.slice(1);
        let newObject = {};
        newObject[keyWithFirstCap] = element;
        setOfTechSpecs.add(newObject);
      }
    }
    const arrayOfTechSpecs = Array.from(setOfTechSpecs);
    this.attributes.tech_specs = arrayOfTechSpecs;
  }

  decorate() {
    super.decorate();
    const allSalsifyAttributes = this.SalsifyBufferParent.snapshot;
    const blackDiamondSalsifyCategories = allSalsifyAttributes[
      "Black Diamond Shopify Category"
    ]
      ? [].concat(allSalsifyAttributes["Black Diamond Shopify Category"])
      : [];
    let formattedSalsifyCategories = [];
    for (const salsifyCategoryID of blackDiamondSalsifyCategories) {
      const matchingCategory = _.find(this.categoryDefinitionReference, {
        salsify_id: salsifyCategoryID,
      });
      if (matchingCategory) {
        formattedSalsifyCategories.push(matchingCategory.custom_name);
      }
    }
    this.attributes.gender = allSalsifyAttributes["Gender"];
    this.attributes.tags.push(
      this.attributes.gender,
      ...formattedSalsifyCategories
    );

    const tagifiedCategories = [
      createSlug(this.attributes.gender),
      ...formattedSalsifyCategories,
    ];
    this.attributes.categories.push(...tagifiedCategories);

    this.attributes.usps = _.values(
      _.pickBy(allSalsifyAttributes, function (value, key) {
        return /USP \d/.test(key);
      })
    );

    let setOfTechSpecs = new Set();
    const techSpecsRegex = /Tech spec/;
    const lengthOfTechSpecsSlice = 11;
    for (const key in allSalsifyAttributes) {
      if (techSpecsRegex.test(key)) {
        const element = allSalsifyAttributes[key];
        const keySliced = key.slice(lengthOfTechSpecsSlice);
        const keyWithFirstCap = keySliced[0].toUpperCase() + keySliced.slice(1);
        let newObject = {};
        newObject[keyWithFirstCap] = element;
        setOfTechSpecs.add(newObject);
      }
    }
    const arrayOfTechSpecs = Array.from(setOfTechSpecs);
    this.attributes.tech_specs = arrayOfTechSpecs;
  }

  constructMetafields() {
    // TODO: clean this up. loop through the initial definitions, DRY
    // make sure that updated metafield IDs from Shopify stay intact... only the values change.

    const existingGenderMetafield = _.find(this.metadata, { key: "gender" });
    if (existingGenderMetafield.id) {
      this.metadata.gender = existingGenderMetafield;
    }
    this.metadata.gender.value = this.attributes.gender;

    const existingTechSpecsMetafield = _.find(this.metadata, {
      key: "tech_specs",
    });
    if (existingTechSpecsMetafield.id) {
      this.metadata.tech_specs = existingTechSpecsMetafield;
    }
    this.metadata.tech_specs.value = JSON.stringify(this.attributes.tech_specs);

    const existingUSPSSpecsMetafield = _.find(this.metadata, { key: "usps" });
    if (existingUSPSSpecsMetafield.id) {
      this.metadata.usps = existingUSPSSpecsMetafield;
    }
    this.metadata.usps.value = JSON.stringify(this.attributes.usps);

    this.attributes.metafields.push(this.metadata.gender);
    this.attributes.metafields.push(this.metadata.tech_specs);
    this.attributes.metafields.push(this.metadata.usps);
  }
}

module.exports.BDBufferProduct = BDBufferProduct;
