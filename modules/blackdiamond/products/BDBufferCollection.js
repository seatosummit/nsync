const _ = require("lodash");
const {
  ShopifyBufferCollection,
} = require("../../shopify/products/ShopifyBufferCollection");
const { BlackDiamondBufferCollection } = require("../../../models");
const {
  createSlug,
  addSpacesAndCapitalise,
} = require("../../helpers/stringTools");

class BDBufferCollection extends ShopifyBufferCollection {
  constructor(ArrayOfCategoriesForRules) {
    super(ArrayOfCategoriesForRules, BlackDiamondBufferCollection);
  }

  decorate() {
    super.decorate();
    this.attributes.rules = [
      {
        column: "tag",
        relation: "equals",
        condition: this.SalsifyCategoryObject.custom_name,
      },
    ];
  }
}

module.exports.BDBufferCollection = BDBufferCollection;
