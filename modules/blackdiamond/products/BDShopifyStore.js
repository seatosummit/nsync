const { ShopifyStore } = require("../../shopify/products/ShopifyStore");
const { ShopifyConfig } = require("../../helpers/ShopifyConfig");
class BDShopifyStore extends ShopifyStore {
  constructor(
    StoreProduct,
    StoreProductVariant,
    BDBufferInstance,
    BDStoreCollection,
    BDStoreImage
  ) {
    super(
      StoreProduct,
      StoreProductVariant,
      BDBufferInstance,
      BDStoreCollection,
      BDStoreImage
    );
    this.salesChannelMachineName = "BLACKDIAMOND";
  }

  // any BD-specific store overrides
}

module.exports.BDShopifyStore = BDShopifyStore;
