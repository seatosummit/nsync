const _ = require("lodash");
const {
  ShopifyStoreProductVariant,
} = require("../../shopify/products/ShopifyStoreProductVariant");
class BDStoreProductVariant extends ShopifyStoreProductVariant {
  constructor(shopifyConnection, bufferProductVariant) {
    super(shopifyConnection, bufferProductVariant);

    // BlackDiamond Variant specific stuff
  }
}

module.exports.BDStoreProductVariant = BDStoreProductVariant;
