const _ = require("lodash");
const { ShopifyStoreImage } = require("../../shopify/products/ShopifyStoreImage");

// TODO: get metafield namespace from shopify config

class BDStoreImage extends ShopifyStoreImage {
  constructor(shopifyConnection, currentBufferImage) {
    super(shopifyConnection, currentBufferImage);

    // BlackDiamond Specific stuff

  }

}

module.exports.BDStoreImage = BDStoreImage
