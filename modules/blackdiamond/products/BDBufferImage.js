const _ = require("lodash");
const {
  ShopifyBufferImage,
} = require("../../shopify/products/ShopifyBufferImageV2");
const { BlackDiamondBufferImage } = require("../../../models");
class BDBufferImage extends ShopifyBufferImage {
  constructor(DigitalAsset, ShopifyIDs) {
    super(DigitalAsset, ShopifyIDs, BlackDiamondBufferImage);
  }
}

module.exports.BDBufferImage = BDBufferImage;
