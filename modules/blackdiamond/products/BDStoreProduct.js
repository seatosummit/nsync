const _ = require("lodash");
const {
  ShopifyStoreProduct,
} = require("../../shopify/products/ShopifyStoreProduct");

// TODO: get metafield namespace from shopify config

class BDStoreProduct extends ShopifyStoreProduct {
  constructor(shopifyConnection, currentBufferProduct, imagesBuffer) {
    super(shopifyConnection, currentBufferProduct, imagesBuffer);

    // BlackDiamond Specific stuff
    this.metafields.gender = {
      key: "gender",
      value: currentBufferProduct.gender,
      value_type: "string",
      namespace: "blackdiamond",
    };
    this.metafields.tech_specs = [];
    this.metafields.usps = [];
  }

  decorate() {
    super.decorate();
  }
}

module.exports.BDStoreProduct = BDStoreProduct;
