const {
  ShopifyBufferProductVariant,
} = require("../../shopify/products/ShopifyBufferProductVariant");
class BDBufferProductVariant extends ShopifyBufferProductVariant {
  constructor(shopifyProductVariantSnapshot, shopifyPricingRule) {
    super(shopifyProductVariantSnapshot, shopifyPricingRule);
    // BlackDiamond Specific stuff
  }

  decorate() {
    super.decorate();
  }
}

module.exports.BDBufferProductVariant = BDBufferProductVariant;
