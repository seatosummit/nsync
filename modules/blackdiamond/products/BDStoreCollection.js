const _ = require("lodash");
const { ShopifyStoreCollection } = require("../../shopify/products/ShopifyStoreCollection");
class BDStoreCollection extends ShopifyStoreCollection {
  constructor(shopifyConnection, currentBufferCollection) {
    super(shopifyConnection, currentBufferCollection);

    // BlackDiamond Specific attributes
    
  }

  // BlackDiamond Specific overrides

  decorate(){
    super.decorate();
  }

}

module.exports.BDStoreCollection = BDStoreCollection
