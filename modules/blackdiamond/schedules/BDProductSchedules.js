const { BDBuffer } = require("../products/BDBuffer");
const { BDShopifyStore } = require("../products/BDShopifyStore");
const { BDStoreCollection } = require("../products/BDStoreCollection");
const { BDBufferCollection } = require("../products/BDBufferCollection");
const { BDBufferImage } = require("../products/BDBufferImage");
const { BDBufferProduct } = require("../products/BDBufferProduct");
const {
  BDBufferProductVariant,
} = require("../products/BDBufferProductVariant");
const { BDStoreImage } = require("../products/BDStoreImage");
const { BDStoreProduct } = require("../products/BDStoreProduct");
const { BDStoreProductVariant } = require("../products/BDStoreProductVariant");
const productsToDeleteFromShopify = require("../products/productsToRemove");
const { Logger } = require("../../helpers/Logger");

const shopifyConnectionMachineName = "BLACKDIAMOND";

// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

const BDProductBufferSync = async () => {
  try {
    Logger.info("Updating Black Diamond product buffer...");
    const buffer = new BDBuffer(
      BDBufferProduct,
      BDBufferProductVariant,
      BDBufferCollection
    );
    await buffer.getAssetsForBufferUpdate();
    const updateResults = await buffer.syncProductsIntoBuffer();
    const actualUpdates = updateResults.filter((result) => {
      return result != false;
    });

    Logger.info(
      `... done updating ${actualUpdates.length} entries to Black Diamond product buffer.`
    );
  } catch (error) {
    Logger.error(`Error updating Black Diamond product buffer: ${error}`);
  }
};

const BDProductCacheSync = async () => {
  try {
    Logger.info("Updating Black Diamond Firestore product cache...");
    const buffer = new BDBuffer();
    const productUpdates = await buffer.syncProductsIntoRealtimeCache();
    const productVariantUpdates =
      await buffer.syncProductVariantsIntoRealtimeCache();
    // Logger.info(
    //   `... done updating ${productUpdates.length} products and ${productVariantUpdates.length} variants to Black Diamond Firestore product cache.`
    // );
  } catch (error) {
    Logger.error(
      `Error updating Black Diamond Firestore product cache: ${error}`
    );
  }
};

const BDVariantBufferSync = async () => {
  try {
    Logger.info("Updating Black Diamond variants buffer...");
    const buffer = new BDBuffer(new BDBufferProduct(), BDBufferProductVariant);
    await buffer.getAssetsForProductVariantBufferUpdate();
    const updateResults = await buffer.syncVariantsIntoBuffer();
    const actualUpdates = updateResults.filter((result) => {
      return result != false;
    });

    Logger.info(
      `... done updating ${actualUpdates.length} entries to Black Diamond variant buffer.`
    );
  } catch (error) {
    Logger.error(`Error updating Black Diamond variants buffer: ${error}`);
  }
};

const BDShopifyPriceBufferSync = async () => {
  try {
    Logger.info("Updating Black Diamond Shopify pricing...");
    const buffer = new BDBuffer(
      new BDBufferProduct(),
      new BDBufferProductVariant()
    );
    await buffer.getAssetsForPricingBufferUpdate();
    await buffer.syncPricingRulesIntoBuffer();

    Logger.info("... done updating Black Diamond pricing buffer.");
  } catch (error) {
    Logger.error(`Error updating Black Diamond pricing buffer: ${error}`);
  }
};

const BDInventorySync = async () => {
  try {
    Logger.info("Syncing Black Diamond inventory to Shopify...");
    const BDBufferInstance = new BDBuffer(
      new BDBufferProduct(),
      new BDBufferProductVariant()
    );
    const BDShopifyStoreInstance = new BDShopifyStore(
      BDStoreProduct,
      BDStoreProductVariant,
      BDBufferInstance
    );
    await BDShopifyStoreInstance.initShopifyConnection(
      shopifyConnectionMachineName
    );
    await BDShopifyStoreInstance.updateInventoryLevels(true);
    Logger.info("... done syncing Black Diamond inventory to Shopify.");
  } catch (error) {
    Logger.error(`Error syncing Black Diamond inventory to Shopify: ${error}`);
  }
};

const BDShopifyProductSync = async ({ newProductsOnly = true }) => {
  try {
    Logger.info("Syncing Black Diamond products to Shopify...");
    const BDBufferInstance = new BDBuffer(
      new BDBufferProduct(),
      new BDBufferProductVariant(),
      null,
      new BDBufferImage()
    );
    const BDShopifyStoreInstance = new BDShopifyStore(
      BDStoreProduct,
      BDStoreProductVariant,
      BDBufferInstance
    );
    await BDShopifyStoreInstance.initShopifyConnection(
      shopifyConnectionMachineName
    );
    await BDBufferInstance.getAllShopifyBufferImages();
    const shopifyIDFilters = [];
    await BDShopifyStoreInstance.syncProductsFromBufferToShopify({
      newProductsOnly,
      selectedShopifyProducts: shopifyIDFilters,
    });
    Logger.info("... done syncing Black Diamond products to Shopify.");
  } catch (error) {
    Logger.error(`Error syncing Black Diamond products to Shopify: ${error}`);
  }
};

const BDShopifyPriceStoreSync = async () => {
  try {
    Logger.info("Updating Black Diamond variants to Shopify...");
    const BDBufferInstance = new BDBuffer(
      new BDBufferProduct(),
      new BDBufferProductVariant(),
      null,
      new BDBufferImage()
    );
    const BDShopifyStoreInstance = new BDShopifyStore(
      BDStoreProduct,
      BDStoreProductVariant,
      BDBufferInstance
    );
    await BDShopifyStoreInstance.initShopifyConnection(
      shopifyConnectionMachineName
    );
    await BDBufferInstance.getAssetsForShopifyPricingUpdate();
    await BDShopifyStoreInstance.syncPricesFromBufferToShopify();

    Logger.info("... done updating Black Diamond pricing to Shopify.");
  } catch (error) {
    Logger.error(`Error updating Black Diamond pricing to Shopify: ${error}`);
  }
};

const BDShopifyMetafieldSync = async () => {
  try {
    Logger.info("Syncing Black Diamond metafields to Shopify...");
    const BDBufferInstance = new BDBuffer(
      new BDBufferProduct(),
      new BDBufferProductVariant()
    );
    const BDShopifyStoreInstance = new BDShopifyStore(
      BDStoreProduct,
      BDStoreProductVariant,
      BDBufferInstance
    );
    await BDShopifyStoreInstance.initShopifyConnection(
      shopifyConnectionMachineName
    );
    await BDShopifyStoreInstance.syncMetafieldsFromBufferToShopify();
    Logger.info("... done syncing Black Diamond metafields to Shopify.");
  } catch (error) {
    Logger.error(`Error syncing Black Diamond metafields to Shopify: ${error}`);
  }
};

const BDBufferCategoryDefinitions = async () => {
  try {
    Logger.info("Updating Black Diamond category definitions...");
    const buffer = new BDBuffer(
      BDBufferProduct,
      BDBufferProductVariant,
      BDBufferCollection
    );
    await buffer.updateCategoryDefinitionsFromSalsify();
    Logger.info("... done updating Black Diamond category definitions.");
  } catch (error) {
    Logger.error(`Error updating Black Diamond category definitions: ${error}`);
  }
};

const BDBufferCollectionSync = async () => {
  try {
    Logger.info("Updating Black Diamond collections buffer...");
    const buffer = new BDBuffer(
      BDBufferProduct,
      BDBufferProductVariant,
      BDBufferCollection
    );
    await buffer.getAssetsForCollectionUpdate();
    await buffer.syncCollectionsIntoBuffer();
    Logger.info("... done updating Black Diamond collections buffer");
  } catch (error) {
    Logger.error(
      `Error updating Black Diamond collections buffer buffer: ${error}`
    );
  }
};

const BDShopifyCollectionSync = async () => {
  try {
    Logger.info("Updating Black Diamond collections...");
    const BDBufferInstance = new BDBuffer(
      new BDBufferProduct(),
      new BDBufferProductVariant(),
      new BDBufferCollection()
    );
    await BDBufferInstance.getAssetsForCollectionUpdate();
    const BDShopifyStoreInstance = new BDShopifyStore(
      BDStoreProduct,
      BDStoreProductVariant,
      BDBufferInstance,
      BDStoreCollection
    );
    await BDShopifyStoreInstance.initShopifyConnection(
      shopifyConnectionMachineName
    );
    await BDShopifyStoreInstance.syncCollectionsFromBufferToShopify();
    Logger.info("... done updating Black Diamond collections.");
    return;
  } catch (error) {
    Logger.error(`Error updating Black Diamond collections: ${error}`);
  }
};

const BDBufferImagesSync = async () => {
  try {
    Logger.info("Updating Black Diamond image buffer...");
    const BDBufferInstance = new BDBuffer(
      new BDBufferProduct(),
      null,
      null,
      BDBufferImage
    );
    await BDBufferInstance.getAssetsForImageBufferUpdate();
    // await BDBufferInstance.syncImagesIntoBuffer();
    // await BDBufferInstance.updateImageBufferBasedOnBrandfolderData();
    await BDBufferInstance.syncImagesIntoBufferBasedOnSalsifyDigitalAssets();
    Logger.info("... done updating Black Diamond image buffer.");
  } catch (error) {
    Logger.error(`Error updating Black Diamond image buffer: ${error}`);
  }
};

const BDShopifyImagesSync = async () => {
  try {
    Logger.info("Updating Black Diamond images...");
    const BDBufferInstance = new BDBuffer(
      new BDBufferProduct(),
      new BDBufferProductVariant(),
      new BDBufferCollection(),
      new BDBufferImage()
    );
    const BDShopifyStoreInstance = new BDShopifyStore(
      BDStoreProduct,
      BDStoreProductVariant,
      BDBufferInstance,
      BDStoreCollection,
      BDStoreImage
    );
    await BDShopifyStoreInstance.initShopifyConnection(
      shopifyConnectionMachineName
    );
    // await BDShopifyStoreInstance.syncImagesFromBufferToShopify();
    const shopifyProductIDFilter = [];
    await BDShopifyStoreInstance.syncImagesFromBufferToShopify(false);
    Logger.info("... done updating Black Diamond images.");
  } catch (error) {
    Logger.error(`Error updating Black Diamond images: ${error}`);
  }
};

const BDCleanUpProducts = async () => {
  try {
    Logger.info("Cleaning Black Diamond products from Shopify...");
    const BDBufferInstance = new BDBuffer(
      new BDBufferProduct(),
      new BDBufferProductVariant()
    );
    const BDShopifyStoreInstance = new BDShopifyStore(
      BDStoreProduct,
      BDStoreProductVariant,
      BDBufferInstance
    );
    await BDShopifyStoreInstance.initShopifyConnection(
      shopifyConnectionMachineName
    );
    await BDShopifyStoreInstance.cleanUpSelectedProductsFromShopifyAndBuffer(
      productsToDeleteFromShopify
    );
    Logger.info("... done syncing Black Diamond products to Shopify.");
  } catch (error) {
    Logger.error(`Error syncing Black Diamond products to Shopify: ${error}`);
  }
};

module.exports.BDProductBufferSync = BDProductBufferSync;
module.exports.BDShopifyProductSync = BDShopifyProductSync;
module.exports.BDBufferCategoryDefinitions = BDBufferCategoryDefinitions;
module.exports.BDBufferCollectionSync = BDBufferCollectionSync;
module.exports.BDShopifyCollectionSync = BDShopifyCollectionSync;
module.exports.BDShopifyMetafieldSync = BDShopifyMetafieldSync;
module.exports.BDBufferImagesSync = BDBufferImagesSync;
module.exports.BDShopifyImagesSync = BDShopifyImagesSync;
module.exports.BDCleanUpProducts = BDCleanUpProducts;
module.exports.BDInventorySync = BDInventorySync;
module.exports.BDShopifyPriceBufferSync = BDShopifyPriceBufferSync;
module.exports.BDShopifyPriceStoreSync = BDShopifyPriceStoreSync;
module.exports.BDVariantBufferSync = BDVariantBufferSync;
module.exports.BDProductCacheSync = BDProductCacheSync;
