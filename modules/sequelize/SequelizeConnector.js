const Sequelize = require("sequelize");
require("dotenv").config();
const { DatabaseConnectionFailedError } = require("../helpers/CustomErrors");
const { Logger } = require("../helpers/Logger");

let dbConfig;
const pathToSearchForConfig = `${__dirname}/../../config`;
try {
  dbConfig = require(`${pathToSearchForConfig}/config.js`)[
    process.env.NODE_ENV ? process.env.NODE_ENV : "development"
  ];
} catch (error) {
  throw new Error(
    `dbConfig not found in ${pathToSearchForConfig} using mode ${process.env.NODE_ENV}`
  );
}

class SequelizeConnector {
  static createConnection(callerName = "Class not known") {
    Logger.info(`Creating Sequelize connection for ${callerName}.`);
    const sequelizeObject = {};
    if (!process.env.DATABASE_URL) throw new Error("DATABASE_URL is missing");
    sequelizeObject.sequelize = new Sequelize(
      process.env.DATABASE_URL,
      dbConfig
    );
    sequelizeObject.sequelize.addHook("afterInit", function (sequelize) {
      sequelize.connectionManager.pool.clear();
    });
    sequelizeObject.Sequelize = Sequelize;
    return sequelizeObject;
  }
  static async testDatabaseConnection(sequelizeConnectorInstance) {
    try {
      await sequelizeConnectorInstance.authenticate();
      Logger.info("DB connection test passed.");
    } catch (error) {
      throw new DatabaseConnectionFailedError(error, process.env);
    }
  }
}

module.exports.SequelizeConnector = SequelizeConnector;
