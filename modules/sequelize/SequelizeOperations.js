const _ = require("lodash");
const {
  valueExistsAndIsNotEmpty,
  valueIsNullOrUndefined,
} = require("../helpers/stringTools");
const { BufferUpsertError } = require("./SequelizeErrors");
class SequelizeOperations {
  static async Upsert(
    itemToSave,
    uniqueIdentifier,
    SequelizeModel,
    BufferOfModelEntries,
    arrayOfFieldsToIgnore = [],
    returnEntryIfNotUpdated = false,
    secondUniqueIdentifier
  ) {
    try {
      const where = {};
      let foundExisting;
      where[uniqueIdentifier] = itemToSave[`${uniqueIdentifier}`];
      if (secondUniqueIdentifier) {
        where[secondUniqueIdentifier] = itemToSave[`${secondUniqueIdentifier}`];
      }
      if (valueExistsAndIsNotEmpty(BufferOfModelEntries)) {
        foundExisting = _.find(BufferOfModelEntries, where);
      } else {
        foundExisting = await SequelizeModel.findOne({
          where,
        });
      }

      if (foundExisting) {
        for (const key in itemToSave) {
          if (arrayOfFieldsToIgnore.includes(key)) continue;
          if (valueIsNullOrUndefined(itemToSave[key])) continue;
          const modelAttribute = foundExisting.rawAttributes[key];
          if (valueExistsAndIsNotEmpty(modelAttribute)) {
            const newValue = itemToSave[key];
            foundExisting[key] = newValue;
          }
        }
        const thereAreChanges = foundExisting.changed().length > 0;
        if (thereAreChanges) {
          const updatedItem = foundExisting.save();
          return updatedItem;
        } else {
          if (returnEntryIfNotUpdated) {
            return foundExisting;
          } else {
            return false;
          }
        }
      } else {
        const createdItem = SequelizeModel.create(itemToSave);
        return createdItem;
      }
    } catch (error) {
      return new BufferUpsertError(error);
    }
  }
}

module.exports.SequelizeOperations = SequelizeOperations;
