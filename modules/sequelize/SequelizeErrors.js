class BufferUpsertError extends Error {
  constructor(payload) {
    const amendedMessage = `Problems upserting item: ${payload}`;
    super(amendedMessage);
    this.name = "BufferUpsertError";
  }
}

module.exports.BufferUpsertError = BufferUpsertError;
