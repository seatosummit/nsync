const createCsvWriter = require("csv-writer").createObjectCsvWriter;
const fetch = require("node-fetch");
const _ = require("lodash");
const Shopify = require("shopify-api-node");
require("dotenv").config();
const { promisify } = require("util");
const sleep = promisify(setTimeout);
const { prepareHeaders } = require("../../helpers/apitools");
const { SalsifyProduct, WMSProduct } = require("../../../models");

module.exports = {
  createSalsifySimpleProductsCSV: async (salsifyFilter, WMSFilter) => {
    // Get a list of products that are in Salsify, and find a corresponding product
    // in WMS. This product is either Z-coded or not.
    // it could be also a discontinued status product.
    // get all the corresponding products and put em in a CSV

    try {
      let allWMSBufferProducts;
      let allSalsifyBufferProducts;
      if (WMSFilter) {
        allWMSBufferProducts = await WMSProduct.findAll({
          where: {
            brand: WMSFilter,
          },
        });
      } else {
        allWMSBufferProducts = await WMSProduct.findAll();
      }

      if (salsifyFilter) {
        allSalsifyBufferProducts = await SalsifyProduct.findAll({
          where: {
            brand: salsifyFilter,
            simple_or_configurable: "simple",
            is_child: 0,
            child_skus: null,
            parent_sku: null,
          },
        });
      } else {
        allSalsifyBufferProducts = await SalsifyProduct.findAll();
      }

      let salsifyOrphans = [];

      for (const salsifyBufferProduct of allSalsifyBufferProducts) {
        // const foundWMSProduct = _.find(allWMSBufferProducts, function (currentProduct) {
        //   const regexString = /\b[Zz]*${salsifyBufferProduct.sku}/gm
        //   //const foundProduct = new RegExp(regexString).test(currentProduct.sku);
        //   const foundProduct = salsifyBufferProduct.sku == currentProduct.sku;
        //   return foundProduct !== null;
        // });
        const foundWMSProduct = _.find(allWMSBufferProducts, {
          sku: salsifyBufferProduct.sku,
        });

        const salsifyOrphan = {
          name: salsifyBufferProduct.name
            ? salsifyBufferProduct.name
            : "Salsify name undefined",
          sku: salsifyBufferProduct.sku
            ? salsifyBufferProduct.sku
            : "SKU not set",
          brand: salsifyBufferProduct.brand
            ? salsifyBufferProduct.brand
            : "Brand not set",
          wms_sku: foundWMSProduct ? foundWMSProduct.sku : "WMS SKU not found",
          status: foundWMSProduct
            ? foundWMSProduct.productSnapshot["status"]
            : "WMS SKU not found",
        };
        salsifyOrphans.push(salsifyOrphan);
      }

      const csvWriter = createCsvWriter({
        path: "./salsifySimpleProducts.csv",
        header: [
          { id: "name", title: "Name" },
          { id: "sku", title: "SKU" },
          { id: "brand", title: "Brand" },
          { id: "wms_sku", title: "WMS SKU" },
          { id: "status", title: "WMS Status" },
        ],
      });

      const records = [...salsifyOrphans];

      await csvWriter.writeRecords(records);
      // console.log("...done.");
    } catch (error) {
      console.error(`Error: ${error}`);
    }
  },
};
