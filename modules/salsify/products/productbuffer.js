const fetch = require("node-fetch");
const throttle = require("fetch-throttle");
const fetchThrottle = throttle(fetch, 5, 1000);
const _ = require("lodash");
require("dotenv").config();
const { promisify } = require("util");
const sleep = promisify(setTimeout);
const { prepareHeaders } = require("../../helpers/apitools");
const { Sequelize } = require("sequelize");
const Op = Sequelize.Op;
const {
  SalsifyProduct,
  SalsifyProductParent,
  SalsifyGlobalProduct,
  Product,
  ProductVariant,
} = require("../../../models");
const {
  salsifyIntoBufferProduct,
  salsifyIntoBufferParentProduct,
  salsifyIntoGlobalBufferProduct,
} = require("./productdecorator");
const { SalsifyAPI } = require("../helpers/SalsifyAPI");
const { SequelizeConnector } = require("../../sequelize/SequelizeConnector");
const { ProgressIndicators } = require("../../helpers/ProgressIndicators");
const { SequelizeOperations } = require("../../sequelize/SequelizeOperations");
const { Logger } = require("../../helpers/Logger");
const { valueExistsAndIsNotEmpty } = require("../../helpers/stringTools");
const { SalsifyTools } = require("../../helpers/SalsifyTools");
const {
  SalsifyToBuffer,
} = require("../../central-buffer/CentralBufferDecorators");
const {
  CreateProductVariantRelationships,
} = require("../../central-buffer/BufferOperations");

module.exports = {
  addProductsToGlobalSalsifyBuffer: async (productsArray) => {
    const productSavePromises = productsArray.map(
      function (product) {
        const decoratedProduct = this.salsifyIntoGlobalBufferProduct(product);
        return this.SequelizeOperations.Upsert(
          decoratedProduct,
          "ean",
          this.SalsifyGlobalProduct
        );
      },
      {
        SequelizeOperations,
        salsifyIntoGlobalBufferProduct,
        SalsifyGlobalProduct,
      }
    );
    try {
      const productResults = await Promise.all(productSavePromises);
      const actualProductResults = productResults.filter((result) => result);
      Logger.info(
        `Created/updated ${actualProductResults.length}/${actualProductResults.length} Products in this batch`
      );
    } catch (error) {
      console.error(`Error with Global Salsify buffer update: ${error}`);
    }
  },

  addProductsToAustraliaSalsifyBuffer: async (productsArray) => {
    const products = productsArray
      .filter((product) => {
        return SalsifyTools.isSalsifyProductParent({ salsifyEntry: product });
      })
      .map((product) =>
        SalsifyToBuffer.salsifyIntoGeneralBufferProduct(product)
      );
    const productVariants = productsArray
      .filter((product) => {
        const isParent = SalsifyTools.isSalsifyProductParent({
          salsifyEntry: product,
        });
        if (!isParent) return true;
        const isSimpleProduct = SalsifyTools.isSalsifySimpleProduct({
          salsifyEntry: product,
        });
        if (isSimpleProduct) return true;
        return false;
      })
      .map((productVariant) =>
        SalsifyToBuffer.salsifyIntoGeneralBufferProductVariant(productVariant)
      );

    const productSavePromises = products.map((decoratedProduct) => {
      return SequelizeOperations.Upsert(
        decoratedProduct,
        "salsifyProductId",
        Product
      );
    });
    const productVariantSavePromises = productVariants.map(
      (decoratedProductVariant) => {
        return SequelizeOperations.Upsert(
          decoratedProductVariant,
          "ean",
          ProductVariant
        );
      }
    );
    try {
      const productResults = await Promise.all(productSavePromises);
      const actualProductResults = productResults.filter((result) => result);
      Logger.info(
        `Created/updated ${actualProductResults.length}/${actualProductResults.length} Products in this batch`
      );
      const productVariantResults = await Promise.all(
        productVariantSavePromises
      );
      const actualVariantResults = productVariantResults.filter(
        (result) => result
      );
      Logger.info(
        `Created/updated ${actualVariantResults.length}/${actualVariantResults.length} ProductVariants in this batch`
      );
    } catch (error) {
      console.error(`Error with Global Salsify buffer update: ${error}`);
    }
  },

  syncGlobalSalsifyBuffer: async () => {
    // await SequelizeConnector.testDatabaseConnection(SalsifyProduct.sequelize);
    const API = new SalsifyAPI(process.env.SALSIFY_GLOBAL_ORG_ID);
    const stats = { current: 0 };
    let productsFetchResult = await API.getProductsWithCursor();
    stats.total = productsFetchResult.meta.total_entries;
    await module.exports.addProductsToGlobalSalsifyBuffer(
      productsFetchResult.data
    );
    ProgressIndicators.showProgress(stats, 100);
    let cursor = productsFetchResult.meta.cursor;
    while (cursor) {
      productsFetchResult = await API.getProductsWithCursor(cursor);
      await module.exports.addProductsToGlobalSalsifyBuffer(
        productsFetchResult.data
      );
      ProgressIndicators.showProgress(stats, 100);
      cursor = productsFetchResult.meta.cursor;
    }
  },

  syncAustraliaSalsifyBuffer: async () => {
    //TODO: consolidate Salsify syncs with org id as param

    try {
      const API = new SalsifyAPI(process.env.SALSIFY_ORG_ID);
      const stats = { current: 0 };
      let productsFetchResult = await API.getProductsWithCursor();
      stats.total = productsFetchResult.meta.total_entries;
      await module.exports.addProductsToAustraliaSalsifyBuffer(
        productsFetchResult.data
      );
      ProgressIndicators.showProgress(stats, 100);
      let cursor = productsFetchResult.meta.cursor;
      while (cursor) {
        productsFetchResult = await API.getProductsWithCursor(cursor);
        await module.exports.addProductsToAustraliaSalsifyBuffer(
          productsFetchResult.data
        );
        ProgressIndicators.showProgress(stats, 100);
        cursor = productsFetchResult.meta.cursor;
      }

      const allProductVariantSKUs = await ProductVariant.findAll({
        attributes: ["parent_sku"],
        group: ["parent_sku"],
      });

      const chunks = _.chunk(allProductVariantSKUs, 100);
      for (const chunk of chunks) {
        const skuArray = chunk.map((item) => item.parent_sku);
        const foundParentsFromSalsify = await API.getProductsBasedOnArray({
          skuArray,
        });
        await module.exports.addProductsToAustraliaSalsifyBuffer(
          foundParentsFromSalsify
        );
      }
    } catch (error) {
      Logger.error(
        `Problem with syncing Product and ProductVariant buffer: ${error}`
      );
    }
  },

  salsifyProductBufferUpsert: async (
    product,
    salsifyProductBuffer,
    salsifyParentBuffer
  ) => {
    if (product.sku) {
    }
    const foundExisting = _.find(salsifyProductBuffer, ["sku", product.sku]);

    if (foundExisting) {
      for (const key in product) {
        if (foundExisting[key] != undefined) {
          const newValue = product[key];
          foundExisting[key] = newValue;
        }
      }
      const thereAreChanges = foundExisting.changed().length > 0;
      if (thereAreChanges) {
        //console.info("actually updated");
        const updatedItem = await foundExisting.save();
        return updatedItem;
      } else {
        //console.info("didnt update");
        return false;
      }
    } else {
      return await SalsifyProduct.create(product);
    }
  },

  addToSalsifyProductBuffer: async (
    product,
    salsifyProductBuffer,
    salsifyParentBuffer
  ) => {
    try {
      if (product.sku) {
        let savePromise;
        const foundMatchingBufferProduct = _.find(salsifyProductBuffer, [
          "sku",
          product.sku,
        ]);
        const foundMatchingBufferProductParent = _.find(salsifyParentBuffer, [
          "sku",
          product.parent_sku,
        ]);
        if (!foundMatchingBufferProduct) {
          const sequelizeSalsifyProduct = await SalsifyProduct.create(product);
          // console.log(
          //   `Created new SalsifyProduct ${sequelizeSalsifyProduct.sku}`
          // );
          return savePromise;
        } else {
          savePromise = await foundMatchingBufferProduct.update(product);
          // console.log(
          //   `Updated SalsifyProduct ${product.sku}, it already existed`
          // );
          return savePromise;
        }
      } else {
        console.log(`Salsify product ${product} doesn't have an SKU.`);
      }
    } catch (error) {
      console.error(`Problems: ${error} for product ${product.sku}`);
    }
  },
  addToSalsifyParentBuffer: async (product, buffer) => {
    try {
      if (product.sku) {
        const foundMatchingBufferProduct = _.find(buffer, ["sku", product.sku]);

        if (!foundMatchingBufferProduct) {
          const sequelizeSalsifyProductParent =
            await SalsifyProductParent.create(product);
          console.log(
            `Created new SalsifyProductParent ${sequelizeSalsifyProductParent.sku}`
          );
          return;
        } else {
          await foundMatchingBufferProduct.update(product);
          // console.log(
          //   `Updated SalsifyProductParent ${product.sku}, it already existed`
          // );
          return;
        }
      } else {
        console.log(`Salsify product parent ${product} doesn't have an SKU.`);
      }
    } catch (error) {
      console.error(`Problems: ${error} for product parent ${product.sku}`);
    }
  },
  updateSelectedBufferProductParentsFromSalsify: async (
    arrayOfSalsifyProductParents,
    allSalsifyBufferProducts,
    allSalsifyBufferProductParents
  ) => {
    let savePromises = [];
    for (const product of arrayOfSalsifyProductParents) {
      savePromises.push(
        await module.exports.addToSalsifyParentBuffer(
          salsifyIntoBufferParentProduct(product),
          allSalsifyBufferProductParents
        )
      );
    }

    try {
      const results = await Promise.all(savePromises);
    } catch (error) {
      console.error(`Error saving SalsifyBufferProduct batch: ${error}`);
    }
  },

  addPrimaryImageToEachParent: async (
    allSalsifyBufferProducts,
    allSalsifyBufferProductParents
  ) => {
    let savePromises = [];
    for (const product of arrayOfSalsifyProductParents) {
      savePromises.push(
        await module.exports.addToSalsifyParentBuffer(
          salsifyIntoBufferParentProduct(product),
          allSalsifyBufferProductParents
        )
      );
    }

    try {
      const results = await Promise.all(savePromises);
    } catch (error) {
      console.error(`Error saving SalsifyBufferProduct batch: ${error}`);
    }
  },
  updateSelectedBufferProductsFromSalsify: async (
    arrayOfSalsifyProducts,
    allSalsifyBufferProducts,
    allSalsifyBufferProductParents
  ) => {
    let savePromises = [];
    for (const product of arrayOfSalsifyProducts) {
      savePromises.push(
        await module.exports.salsifyProductBufferUpsert(
          salsifyIntoBufferProduct(product),
          allSalsifyBufferProducts,
          allSalsifyBufferProductParents
        )
      );
    }

    try {
      const results = await Promise.all(savePromises);
    } catch (error) {
      console.error(`Error saving SalsifyBufferProduct batch: ${error}`);
    }
  },

  syncSalsifyProductsToDB: async () => {
    try {
      const headers = await prepareHeaders();
      const SalsifyAPIInstance = new SalsifyAPI();

      const allSalsifyBufferProducts = await SalsifyProduct.findAll({
        include: [SalsifyProductParent, SalsifyGlobalProduct],
      });
      let salsifyProductResults =
        await SalsifyAPIInstance.getProductsWithCursor();
      const stats = {
        current: 0,
        total: salsifyProductResults.meta.total_entries,
        increase: salsifyProductResults.meta.per_page,
      };
      let savePromises = [];

      for (const product of salsifyProductResults.data) {
        savePromises.push(
          await SequelizeOperations.Upsert(
            salsifyIntoBufferProduct(product),
            "sku",
            SalsifyProduct,
            allSalsifyBufferProducts
          )
        );
      }

      try {
        const results = await Promise.all(savePromises);
        ProgressIndicators.showProgress(stats);
      } catch (error) {
        console.error(`Error saving SalsifyBufferProduct batch: ${error}`);
      }

      while (salsifyProductResults.meta.cursor) {
        salsifyProductResults = await SalsifyAPIInstance.getProductsWithCursor(
          salsifyProductResults.meta.cursor
        );
        savePromises = [];
        for (const product of salsifyProductResults.data) {
          savePromises.push(
            await SequelizeOperations.Upsert(
              salsifyIntoBufferProduct(product),
              "sku",
              SalsifyProduct,
              allSalsifyBufferProducts
            )
          );
        }
        try {
          const results = await Promise.all(savePromises);
          ProgressIndicators.showProgress(stats, 100);
          const actualResults = results.filter((result) => result);
        } catch (error) {
          console.error(`Error saving SalsifyBufferProduct batch: ${error}`);
        }
      }
      console.log(`...finished syncing Salsify products buffer`);
    } catch (error) {
      console.error(`Error syncing Salsify products buffer, ${error}`);
      return;
    }
  },
  syncSalsifyProductParentsToDB: async () => {
    try {
      const headers = await prepareHeaders();
      const allSalsifyBufferParentProducts = await SalsifyProductParent.findAll(
        {
          //raw: true,
        }
      );
      const allSalsifyBufferProducts = await SalsifyProduct.findAll({
        raw: true,
        where: {
          name: {
            [Op.ne]: null,
          },
          parent_sku: {
            [Op.ne]: null,
          },
          //brand: "Black Diamond",
        },
      });

      const salsifyParents = _.keys(
        _.mapValues(_.groupBy(allSalsifyBufferProducts, "parent_sku"))
      );

      let i,
        j,
        batch = 100;
      for (i = 0, j = salsifyParents.length; i < j; i += batch) {
        let parents = [];
        parents.push({ ids: salsifyParents.slice(i, i + batch) });

        const salsifyProductResults =
          await module.exports.getProductsFromSalsifyWithIds(
            headers,
            "REPORT",
            parents[0]
          );

        for (const product of salsifyProductResults.products) {
          await module.exports.addToSalsifyParentBuffer(
            product,
            allSalsifyBufferParentProducts
          );
        }
      }

      console.log(`... finished syncing Salsify product parents`);
    } catch (error) {
      console.error(`Error syncing Salsify products buffer, ${error}`);
      return;
    }
  },

  getProductsFromSalsifyWithPageForBuffer: async (page = 0, headers) => {
    let returnPayload = {
      products: [],
      paging: {
        totalPages: 0,
        nextPage: 0,
      },
    };
    let url;
    try {
      url = `https://app.salsify.com/api/v1/orgs/${process.env.SALSIFY_ORG_ID}/products?access_token=${process.env.SALSIFY_TOKEN}&page=${page}`;
      const response = await fetchThrottle(url, {
        headers: headers.SalsifyHeaders,
        timeout: 0,
      });
      if (response.status !== 200) {
        throw `response other than 200 - ${response.status}: ${response.statusText}`;
      }
      let responseJSON = await response.json();
      console.log(responseJSON.meta);
      responseJSON.data.forEach((product) => {
        returnPayload.products.push(salsifyIntoBufferProduct(product));
      });
      returnPayload.paging.totalPages = responseJSON.meta.total_entries / 100;
      returnPayload.paging.nextPage = page + 1;
      return returnPayload;
    } catch (error) {
      Logger.error(`Error fetching products from Salsify: ${error}`);
    }
  },
  getProductsFromSalsifyWithIds: async (headers, method = "REPORT", ids) => {
    let returnPayload = {
      products: [],
    };
    let url;
    try {
      url = `https://app.salsify.com/api/v1/orgs/${process.env.SALSIFY_ORG_ID}/products?access_token=${process.env.SALSIFY_TOKEN}`;
      const response = await fetchThrottle(url, {
        headers: headers.SalsifyHeaders,
        method: method,
        body: JSON.stringify(ids),
        timeout: 0,
      });
      if (response.status !== 200) {
        throw `response other than 200 - ${response.status}: ${response.statusText}`;
      }
      let responseJSON = await response.json();
      responseJSON.forEach((product) => {
        if (product) {
          returnPayload.products.push(salsifyIntoBufferParentProduct(product));
        }
      });
      return returnPayload;
    } catch (error) {
      console.error(`Error fetching products from Salsify: ${error}`);
    }
  },

  extractColour: (products) => {
    let attributes = new Set();
    for (const product of products) {
      attributes.add(product.colour);
    }
    return attributes;
  },

  extractColourLayman: (products) => {
    let attributes = new Set();
    for (const product of products) {
      attributes.add(product.snapshot["COLOUR LAYMAN (Generic Colour Group)"]);
    }
    return attributes;
  },

  extractSize: (products) => {
    let attributes = new Set();
    for (const product of products) {
      if (product.size) {
        attributes.add(product.size);
      }
    }
    return attributes;
  },

  getSalsifyProductAttributes: async (extractorFunction) => {
    const allSalsifyBufferProducts = await SalsifyProduct.findAll({
      //raw: true,
    });

    const attributes = extractorFunction(allSalsifyBufferProducts);
    console.log(Array.from(attributes));
  },

  addSingleColourFromParentToProducts: async () => {
    const allSalsifyBufferProducts = await SalsifyProduct.findAll({
      where: {
        brand: "Black Diamond",
      },
    });

    const allSalsifyBufferProductParents = await SalsifyProductParent.findAll({
      where: {
        brand: "Black Diamond",
      },
    });

    const productsWithoutColour = allSalsifyBufferProducts.filter(
      (product) => product.colour == null || product.colour == ""
    );
    for (const product of productsWithoutColour) {
      const productParent = _.find(allSalsifyBufferProductParents, {
        sku: product.parent_sku,
      });
      if (productParent) {
        if (productParent.snapshot["COLOUR VARIANT"]) {
          product.colour = productParent.snapshot["COLOUR VARIANT"];
          await product.save();
          console.log(`Added colour to product ${product.sku}`);
        }
      }
    }
  },
};
