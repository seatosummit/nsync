const fetch = require("node-fetch");
const slugify = require("slugify");
const fieldsToIncludeInParent = require("./fieldsToIncludeInParent");
const _ = require("lodash");
require("dotenv").config();
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");
const { ProductTools } = require("../helpers/ProductTools");
const {
  valueIsNullOrUndefined,
  valueExistsAndIsNotEmpty,
} = require("../../helpers/stringTools");

module.exports = {
  salsifyIntoPepperiBulk: (productObject) => {
    /**
         * Headers: [
                "Name",
                "BrandExternalID",
                "ExternalID",
                "Prop1",
                "Prop2",
                "Prop3",
                "Prop4",
                "Dimension1Code"
                "Dimension1Name"
                "Dimension2Code"
                "Dimension2Name"
            ],
         */

    /**
         * "STS Catalogue Sheet Name": "Storage Sacks",
            "ERP - Tier 1 Financial Category": "Outdoor",
            "ERP - Category": "Storage",
            "ERP - Sub-Category": "Stuff Sacks",
            */

    const pepperiProduct = [
      productObject["FULL PRODUCT NAME (incl VARIANT)"],
      productObject["BRAND"] ? productObject["BRAND"] : "BRAND_NOT_SET",
      productObject["PRODUCT CODE"].replace(/\//g, "-"),
      productObject["STS Catalogue Sheet Name"]
        ? productObject["STS Catalogue Sheet Name"]
        : "Not set",
      productObject["ERP - Tier 1 Financial Category"]
        ? productObject["ERP - Tier 1 Financial Category"]
        : "Not set",
      productObject["ERP - Category"]
        ? productObject["ERP - Category"]
        : "Not set",
      productObject["ERP - Sub-Category"]
        ? productObject["ERP - Sub-Category"]
        : "Not set",
      productObject["SIZE VARIANT"] ? productObject["SIZE VARIANT"] : null,
      productObject["SIZE VARIANT"] ? "Size" : null,
      productObject["COLOUR VARIANT"] ? productObject["COLOUR VARIANT"] : null,
      productObject["COLOUR VARIANT"] ? "Colour" : null,
    ];
    return pepperiProduct;
  },

  salsifyIntoBufferProduct: (productObject) => {
    /**
 * 
 * sku: DataTypes.STRING,
    name: DataTypes.STRING,
    size: DataTypes.STRING,
    colour: DataTypes.STRING,
    erp_tier1_financial_category: DataTypes.STRING,
    erp_category: DataTypes.STRING,
    erp_sub_category: DataTypes.STRING,
    b2b_category: DataTypes.STRING,
    b2b_sub_category: DataTypes.STRING,
    parent_sku: DataTypes.STRING,
    simple_or_configurable: DataTypes.STRING,
    is_child: DataTypes.BOOLEAN,
    snapshot: DataTypes.TEXT('long'),
    child_skus: DataTypes.TEXT('long')
 */

    //TODO: may need to grab extra fields from parent product

    if (productObject["Simple or Configurable SKU Type"] != "simple") {
      // console.log("not a simple product");
    }

    const salsifyBufferProduct = {
      name: productObject["FULL PRODUCT NAME (incl VARIANT)"],
      sku: productObject["PRODUCT CODE"],
      usa_sku: "not implemented yet",
      asin_amazon: productObject["ASIN"],
      brand: productObject["BRAND"],
      brand_internal_product_code: productObject["Brand Internal Product Code"],
      // sku: productObject["PRODUCT CODE"].replace(/\//g, "-"), // this is due to 21 legacy products that had "/" in their skus
      erp_tier1_financial_category: productObject[
        "ERP - Tier 1 Financial Category"
      ]
        ? productObject["ERP - Tier 1 Financial Category"]
        : null,
      erp_category: productObject["ERP - Category"]
        ? productObject["ERP - Category"]
        : null,
      erp_sub_category: productObject["ERP - Sub-Category"]
        ? productObject["ERP - Sub-Category"]
        : null,
      b2b_category: productObject["B2B/B2C CATEGORY"]
        ? productObject["B2B/B2C CATEGORY"]
        : null,
      b2b_sub_category: productObject["B2B/B2C SUB CATEGORY"]
        ? productObject["B2B/B2C SUB CATEGORY"]
        : null,
      size: productObject["SIZE VARIANT"]
        ? productObject["SIZE VARIANT"]
        : null,
      colour: productObject["COLOUR VARIANT"]
        ? productObject["COLOUR VARIANT"]
        : null,
      colour_layman: productObject["COLOUR LAYMAN (Generic Colour Group)"],
      digital_assets: productObject["salsify:digital_assets"]
        ? productObject["salsify:digital_assets"]
        : [],
      barcode_ean: productObject["BARCODE/EAN"],
      base_product_name: productObject["BASE PRODUCT NAME"]
        ? productObject["BASE PRODUCT NAME"]
        : null,
      primary_image: productObject["Primary Image"],
      additional_images: productObject["Additional Image"]
        ? [].concat(productObject["Additional Image"])
        : [],
      packaging_images: productObject["Packaging Images"]
        ? [].concat(productObject["Packaging Images"])
        : [],
      parent_sku: productObject["Parent ID"],
      salsify_category_ids: productObject["Black Diamond Shopify Category"]
        ? productObject["Black Diamond Shopify Category"]
        : [],
      simple_or_configurable: productObject["Simple or Configurable SKU Type"],
      child_skus: _.isEmpty(
        productObject["B2B/B2C Child SKUs to be associated to Parent"]
      )
        ? null
        : productObject["B2B/B2C Child SKUs to be associated to Parent"].split(
            ","
          ),
      is_child: productObject["Is Child?"],
      snapshot: productObject,
    };
    return salsifyBufferProduct;
  },

  salsifyIntoGlobalBufferProduct: (productObject) => {
    const basicMappings = {
      salsifyProductId: "salsify:id",
      salsify_created_at: "salsify:created_at",
      salsify_updated_at: "salsify:updated_at",
      salsify_system_id: "salsify:system_id",
      sku: "SKU",
      name: "Primary Name",
      us_product_name: "US PRODUCT NAME",
      size_name_longform: "Size Name (Longform)",
      brand: "Brand",
      category: "Category",
      sub_category: "Sub Category",
      family: "Family",
      sub_family: "Sub Family",
      colour_pantone: "Colour (Pantone)",
      colour_family: "Colour (Family)",
      gender: "Gender",
      country_of_origin: "Country of Origin",
      detail1: "Detail1",
      detail2: "Detail2",
      detail3: "Detail3",
      detail4: "Detail4",
      detail5: "Detail5",
      detail6: "Detail6",
      detail7: "Packaging",
    };

    let salsifyBufferProduct = {
      digital_assets: productObject["salsify:digital_assets"],
      snapshot: productObject,
    };
    for (const attributeKey in basicMappings) {
      if (Object.hasOwnProperty.call(basicMappings, attributeKey)) {
        const salsifyAttributeKey = basicMappings[attributeKey];
        salsifyBufferProduct[attributeKey] =
          productObject[`${salsifyAttributeKey}`];
      }
    }

    //TODO: better version of this image detail adder

    for (let index = 0; index < 10; index++) {
      const firstAttribute = `detail${index}`;
      const secondAttribute = `Product Image: Detail${index}`;
      const value = productObject[firstAttribute];
      let secondValue = productObject[secondAttribute];
      if (valueIsNullOrUndefined(value) && valueIsNullOrUndefined(secondValue))
        continue;

      if (valueExistsAndIsNotEmpty(value)) {
        const parsedArray = [].concat(value);
        salsifyBufferProduct[firstAttribute] = parsedArray;
      }
      if (valueExistsAndIsNotEmpty(secondValue)) {
        const parsedArray = [].concat(secondValue);
        salsifyBufferProduct[secondAttribute] = parsedArray;
      }
    }

    return salsifyBufferProduct;
  },

  changeBufferSimpleIntoChild: (productObject) => {
    const updates = {
      is_child: productObject["Is Child?"],
      parent_sku: productObject["salsify:parent_id"],
      sku: productObject["PRODUCT CODE"],
    };
    return updates;
  },

  noBrandProduct: (productObject) => {
    /**
         * Headers: [
                "Name",
                "BrandExternalID",
                "ExternalID",
                "Prop1",
                "Prop2",
                "Prop3",
                "Prop4",
            ],
         */

    /**
         * "STS Catalogue Sheet Name": "Storage Sacks",
            "ERP - Tier 1 Financial Category": "Outdoor",
            "ERP - Category": "Storage",
            "ERP - Sub-Category": "Stuff Sacks",
            */

    const pepperiProduct = {
      Name: productObject["FULL PRODUCT NAME (incl VARIANT)"],
      SKU: productObject["PRODUCT CODE"],
    };
    return pepperiProduct;
  },

  salsifyIntoPepperiSingle: (productObject) => {
    const pepperiProduct = {
      Name: productObject["FULL PRODUCT NAME (incl VARIANT)"],
      ExternalID: productObject["PRODUCT CODE"].replace(/\//g, "-"),
      MainCategoryID: productObject["BRAND"],
      Prop1: productObject["STS Catalogue Sheet Name"],
      Prop2: productObject["ERP - Tier 1 Financial Category"],
      Prop3: productObject["ERP - Category"],
      Prop4: productObject["ERP - Sub-Category"],
    };
    return pepperiProduct;
  },
  salsifyIntoBufferParentProduct: (productObject) => {
    /**
 * 
 * sku: DataTypes.STRING,
    name: DataTypes.STRING,
    size: DataTypes.STRING,
    brand: DataTypes.STRING,
    colour: DataTypes.STRING,
    erp_tier1_financial_category: DataTypes.STRING,
    erp_category: DataTypes.STRING,
    erp_sub_category: DataTypes.STRING,
    b2b_category: DataTypes.STRING,
    b2b_sub_category: DataTypes.STRING
 */

    if (productObject["Simple or Configurable SKU Type"] == "simple") {
      // console.log("simple product");
    }

    const parentProduct = {
      name: productObject["BASE PRODUCT NAME"],
      sku: productObject["PRODUCT CODE"].replace(/\//g, "-"),
      child_skus: productObject["B2B/B2C Child SKUs to be associated to Parent"]
        ? productObject["B2B/B2C Child SKUs to be associated to Parent"]
        : "",
      brand: productObject["BRAND"],
      brand_category: productObject["BRAND CATEGORY"],
      brand_sub_category: productObject["BRAND SUB CATEGORY"],
      erp_tier1_financial_category: productObject[
        "ERP - Tier 1 Financial Category"
      ]
        ? productObject["ERP - Tier 1 Financial Category"]
        : null,
      erp_category: productObject["ERP - Category"]
        ? productObject["ERP - Category"]
        : null,
      erp_sub_category: productObject["ERP - Sub-Category"]
        ? productObject["ERP - Sub-Category"]
        : null,
      b2b_category: productObject["B2B/B2C CATEGORY"]
        ? productObject["B2B/B2C CATEGORY"]
        : null,
      b2b_sub_category: productObject["B2B/B2C SUB CATEGORY"]
        ? productObject["B2B/B2C SUB CATEGORY"]
        : null,
      base_product_name: productObject["BASE PRODUCT NAME"],
      primary_image: productObject["Primary Image"],
      additional_images: productObject["Additional Image"]
        ? [].concat(productObject["Additional Image"])
        : [],
      packaging_images: productObject["Packaging Images"]
        ? [].concat(productObject["Packaging Images"])
        : [],
      salsify_category_ids: productObject["Black Diamond Shopify Category"]
        ? [].concat(productObject["Black Diamond Shopify Category"])
        : [],
      colour_layman: productObject["COLOUR LAYMAN (Generic Colour Group)"],
      snapshot: productObject,
    };
    return parentProduct;
  },

  salsifySimpleIntoParentForCSV: (simpleProduct, fieldsToIncludeInParent) => {
    let salsifyParentObject = {};
    const snapshot = simpleProduct.snapshot;
    const generatedParentSKU = ProductTools.generateParentSKU(simpleProduct);
    const baseProductName = snapshot["BASE PRODUCT NAME"]
      ? snapshot["BASE PRODUCT NAME"]
      : snapshot["FULL PRODUCT NAME (incl VARIANT)"]
      ? snapshot["FULL PRODUCT NAME (incl VARIANT)"]
      : "No base product name or variant name";

    for (const currentAttribute of fieldsToIncludeInParent) {
      const slugifiedKey = ProductTools.convertToCSVHeaderKey(currentAttribute);
      salsifyParentObject[`${slugifiedKey}`] = snapshot[`${currentAttribute}`];
    }

    salsifyParentObject = {
      ...salsifyParentObject,
    };

    const additionalImages = [].concat(snapshot["Additional Image"]);

    for (const [index, additionalImageID] of additionalImages.entries()) {
      salsifyParentObject[`additionalImage_${index}`] = additionalImageID;
    }

    //const salsifyKey = ProductTools.convertToCSVHeaderKey("salsify:id");
    const productCodeKey = ProductTools.convertToCSVHeaderKey(
      "PROPOSED PARENT SKU"
    );
    const simpleConfKey = ProductTools.convertToCSVHeaderKey(
      "Simple or Configurable SKU Type"
    );
    const childKey = ProductTools.convertToCSVHeaderKey("Is Child?");
    const fullProductNameKey = ProductTools.convertToCSVHeaderKey(
      "FULL PRODUCT NAME (incl VARIANT)"
    );
    const baseProductNameKey =
      ProductTools.convertToCSVHeaderKey("BASE PRODUCT NAME");
    const childSKUKey = ProductTools.convertToCSVHeaderKey("CHILD SKU");

    //"SIMPLE PRODUCT SKU",
    // "NEW PARENT SKU",

    //salsifyParentObject[`${salsifyKey}`] = generatedParentSKU;
    salsifyParentObject[`${productCodeKey}`] = generatedParentSKU;
    salsifyParentObject[`${simpleConfKey}`] = "configurable";
    salsifyParentObject[`${childKey}`] = null;
    salsifyParentObject[`${fullProductNameKey}`] = baseProductName;
    salsifyParentObject[`${baseProductNameKey}`] = baseProductName;
    salsifyParentObject[`${childSKUKey}`] = simpleProduct.sku;

    return salsifyParentObject;
  },

  salsifySimpleIntoParent: (simpleProduct) => {
    let salsifyParentObject = {};
    const snapshot = simpleProduct.snapshot;
    const generatedParentSKU = ProductTools.generateParentSKU(simpleProduct);
    const baseProductName = snapshot["BASE PRODUCT NAME"]
      ? snapshot["BASE PRODUCT NAME"]
      : snapshot["FULL PRODUCT NAME (incl VARIANT)"]
      ? snapshot["FULL PRODUCT NAME (incl VARIANT)"]
      : "No base product name or variant name";
    const copyTheseAttributesToParent = [
      "BRAND",
      "SIZE VARIANT",
      "Product Volume (L)",
      "BRAND CATEGORY",
      "BRAND SUB CATEGORY",
      "SEASON RELEASED",
      "B2B/B2C CATEGORY",
      "B2B/B2C SUB CATEGORY",
      "B2B/B2C Category IDs",
      "ERP - Category",
      "ERP - Sub-Category",
      "ERP - Tier 1 Financial Category",
      "BLACK DIAMOND CATEGORIES & SUBCATEGORIES",
      "B2B/B2C Child SKUs to be associated to Parent",
      "B2B/B2C CONFIGURABLE ATTRIBUTES",
      "B2B/B2C Product ship rule",
      "B2B/B2C Tax class",
      "Black Diamond Shopify Category",
      "SHORT DESCRIPTION",
      "LONG PRODUCT DESCRIPTION",
      "COUNTRY OF ORIGIN",
      "SKU In-Use/Unpackaged Dimension Three - Length",
      "SKU In-Use/Unpackaged Dimension One - Height",
      "SKU In-Use/Unpackaged Dimension Two - Width",
      "SKU In-Use/Unpackaged Weight",
      "SKU Unit Of Measure For In-Use/Unpackaged Dimension",
      "SKU Unit Of Measure For In-Use/Unpacked Weight",
      "SKU Packaged Weight",
      "SKU Unit Of Measure For Packaged Dimension",
      "SKU Unit Of Measure For Packaged Weight",
      "SKU Packed Weight",
      "SKU Packed Dimension One - Height",
      "SKU Packed Dimension Three - Length",
      "SKU Packed Dimension Two - Width",
      "SKU Unit Of Measure For Packed Dimension",
      "SKU Unit Of Measure For Packed Weight",
      "(PRODUCT) UNITS PER OUTER CARTON",
      "OUTER CARTON GROSS WEIGHT (KG)",
      "OUTER CARTON HEIGHT (CM)",
      "OUTER CARTON LENGTH (CM)",
      "OUTER CARTON WIDTH (CM)",
      "COMPOSITION",
      "Gender",
      "Primary Image",
      "Additional Image",
      //"Combined In Use Dimensions",
      //"Combined In Use Weight",
      //"Combined Packaged Dimensions",
      //"Combined Packaged Weight",
      //"Combined Packed Dimensions",
      //"Combined Packed Weight",
      "Activity: Travel & Lifestyle",
      "Activity: Camp & Hike",
      "Activity: Fitness & Cycle",
      "Activity: Tactical & Industrial",
      "Activity: Watersports & Fishing",
      "salsify:digital_assets",
    ];

    for (const currentAttribute of fieldsToIncludeInParent) {
      //TODO: check this, it's coming from a central location
      if (snapshot[`${currentAttribute}`]) {
        salsifyParentObject[`${currentAttribute}`] =
          snapshot[`${currentAttribute}`];
      }
    }

    const techSpecsToAdd = ProductTools.extractTechSpecs(snapshot);
    const uspsToAdd = ProductTools.extractUSPs(snapshot);

    salsifyParentObject = {
      ...salsifyParentObject,
      ...techSpecsToAdd,
      ...uspsToAdd,
      "salsify:id": generatedParentSKU,
      "PRODUCT CODE": generatedParentSKU,
      "Simple or Configurable SKU Type": "configurable",
      "Is Child?": null,
      "FULL PRODUCT NAME (incl VARIANT)": baseProductName,
      "BASE PRODUCT NAME": baseProductName,
    };

    return salsifyParentObject;
  },

  salsifySimpleIntoChild: (simpleProduct, newParentSKU) => {
    let updatedChildProduct = {};

    updatedChildProduct = {
      "salsify:id": simpleProduct.sku,
      "salsify:parent_id": newParentSKU,
      "PRODUCT CODE": simpleProduct.sku,
      "Parent ID": newParentSKU,
      "Is Child?": true,
    };

    return updatedChildProduct; // TODO: remove once we can clean up the children

    const snapshot = simpleProduct.snapshot;
    const attributesToKeepForChildProduct = [
      "salsify:id",
      "salsify:version",
      "salsify:system_id",
      "SEASON RELEASED",
      "PRODUCT CODE",
      "BARCODE/EAN",
      "FULL PRODUCT NAME (incl VARIANT)",
      "SIZE VARIANT",
      "COLOUR VARIANT",
      "COLOUR LAYMAN (Generic Colour Group)",
      "BRAND",
      //"COUNTRY OF ORIGIN",
      // "SKU In-Use/Unpackaged Dimension Three - Length",
      // "SKU In-Use/Unpackaged Dimension One - Height",
      // "SKU In-Use/Unpackaged Dimension Two - Width",
      // "SKU In-Use/Unpackaged Weight",
      //"SKU Unit Of Measure For In-Use/Unpackaged Dimension",
      //"SKU Unit Of Measure For In-Use/Unpacked Weight",
      // "SKU Packaged Weight",
      //"SKU Unit Of Measure For Packaged Dimension",
      //"SKU Unit Of Measure For Packaged Weight",
      // "SKU Packed Weight",
      // "SKU Packed Dimension One - Height",
      // "SKU Packed Dimension Three - Length",
      // "SKU Packed Dimension Two - Width",
      //"SKU Unit Of Measure For Packed Dimension",
      //"SKU Unit Of Measure For Packed Weight",
      // "(PRODUCT) UNITS PER OUTER CARTON",
      // "OUTER CARTON GROSS WEIGHT (KG)",
      // "OUTER CARTON HEIGHT (CM)",
      // "OUTER CARTON LENGTH (CM)",
      // "OUTER CARTON WIDTH (CM)",
      "Primary Image",
      "Additional Image",
      "Packaging Images",
      // "B2B/B2C CATEGORY",
      // "B2B/B2C SUB CATEGORY",
      // "B2B/B2C Category IDs",
      // "ERP - Category",
      // "ERP - Sub-Category",
      // "ERP - Tier 1 Financial Category",
      // "B2B/B2C Child SKUs to be associated to Parent",
      // "B2B/B2C CONFIGURABLE ATTRIBUTES",
      // "B2B/B2C Product ship rule",
      // "B2B/B2C Tax class",
      // "SHORT DESCRIPTION",
      // "LONG PRODUCT DESCRIPTION",
      // "Combined In Use Dimensions",
      // "Combined In Use Weight",
      // "Combined Packaged Dimensions",
      // "Combined Packaged Weight",
      // "Combined Packed Dimensions",
      // "Combined Packed Weight",
      // "Black Diamond Shopify Category",
      // "COMPOSITION",
      // "Gender",
      // "Activity: Travel & Lifestyle",
      // "Activity: Camp & Hike",
      // "Activity: Fitness & Cycle",
    ];

    for (const key in snapshot) {
      if (Object.hasOwnProperty.call(snapshot, key)) {
        const salsifyRegex = /salsify:/g;
        const isSalsifyCoreField = salsifyRegex.test(key);
        const value = snapshot[`${key}`];
        if (
          attributesToKeepForChildProduct.includes(key) ||
          isSalsifyCoreField
        ) {
          updatedChildProduct[`${key}`] = value;
        } else {
          updatedChildProduct[`${key}`] = null;
        }
        updatedChildProduct[`${key}`] = value; // TODO: remove these fields once everything is set up
      }
    }

    const techSpecsToKeep = ProductTools.extractTechSpecs(snapshot);
    const uspsToKeep = ProductTools.extractUSPs(snapshot);

    updatedChildProduct = {
      //...updatedChildProduct,
      //...techSpecsToKeep,
      "salsify:parent_id": newParentSKU,
      "PRODUCT CODE": simpleProduct.sku,
      "Parent ID": newParentSKU,
      "Is Child?": true,
      // not keeping USPs on child level
      //...uspsToKeep,
    };

    return updatedChildProduct;
  },
};
