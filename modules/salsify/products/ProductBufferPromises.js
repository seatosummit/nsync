require("dotenv").config();
const {
  SalsifyProduct,
  SalsifyProductParent,
  SalsifyGlobalProduct,
} = require("../../../models");

class ProductBufferPromises {
  constructor() {
    this.SalsifyGlobalProduct = SalsifyGlobalProduct;
  }
  async getGlobalSalsifyProductBufferPromise(product) {
    try {
      const existingBufferProduct = await this.SalsifyGlobalProduct.findOne({
        where: {
          ean: product.ean,
        },
      });
      if (existingBufferProduct) {
        for (const key in product) {
          if (product[key] != undefined) {
            const newValue = product[key];
            existingBufferProduct[key] = newValue;
          }
        }
        const thereAreChanges = existingBufferProduct.changed().length > 0;
        if (thereAreChanges) {
          //console.info("actually updated");
          const updatedItem = await existingBufferProduct.save();
          return updatedItem;
        } else {
          //console.info("didnt update");
          return false;
        }
      } else {
        return await this.SalsifyGlobalProduct.create(product);
      }
    } catch (error) {
      console.error(
        `Problems with product ${product.ean}. Have you run all your migrations?`
      );
      console.error(error);
      return {};
    }
  }
}

module.exports.ProductBufferPromises = ProductBufferPromises;
