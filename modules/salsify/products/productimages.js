const fetch = require("node-fetch");
const { promises: fsp } = require("fs");
const fs = require("fs");
const sharp = require("sharp");
const pathTools = require("path");
const _ = require("lodash");
require("dotenv").config();
const { promisify } = require("util");
const sleep = promisify(setTimeout);
const { prepareHeaders } = require("../../helpers/apitools");

// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

module.exports = {
  getProductImagesFromSalsifyWithPage: async (page = 0, brand) => {
    const headers = await prepareHeaders();
    console.log(`current page: ${page}`);
    let url;
    try {
      url = `https://app.salsify.com/api/v1/orgs/${process.env.SALSIFY_ORG_ID}/products?access_token=${process.env.SALSIFY_TOKEN}&page=${page}`;

      const response = await fetch(url, {
        headers: headers.SalsifyHeaders,
        timeout: 0,
      });
      if (response.status !== 200) {
        throw `response other than 200 - ${response.status}: ${response.statusText}`;
      }
      let responseJSON = await response.json();
      console.log(responseJSON.meta);
      let arrayOfProducts = responseJSON.data;
      if (brand) {
        arrayOfProducts = arrayOfProducts.filter((product) => {
          const matchesFilteredBrand = product["BRAND"] === brand;
          return matchesFilteredBrand;
        });
      }
      let imageURLArray =
        module.exports.parseArrayOfProductURLs(arrayOfProducts);
      for (let index = 0; index < imageURLArray.length; index++) {
        const currentImage = imageURLArray[index];
        await module.exports.saveImageWithSKUFilename(currentImage);
      }
      const totalPages = responseJSON.meta.total_entries / 100;
      const currentPage = Number.parseInt(responseJSON.meta.current_page);
      const nextPage = currentPage + 1;
      console.log(`next page: ${nextPage}`);
      if (nextPage < totalPages) {
        //await sleep(10000);
        module.exports.getProductImagesFromSalsifyWithPage(nextPage, brand);
      } else {
        console.log("last page");
      }
    } catch (error) {
      console.error(`Error fetching products from Salsify: ${error}`);
    }
  },
  getProductImagesFromSalsifyWithCursor: async (cursor) => {
    const headers = await prepareHeaders();
    let url;
    try {
      if (cursor) {
        url = `https://app.salsify.com/api/v1/orgs/${process.env.SALSIFY_ORG_ID}/products?access_token=${process.env.SALSIFY_TOKEN}&cursor=${cursor}`;
      } else {
        url = `https://app.salsify.com/api/v1/orgs/${process.env.SALSIFY_ORG_ID}/products?access_token=${process.env.SALSIFY_TOKEN}`;
      }

      const response = await fetch(url, {
        headers: headers.SalsifyHeaders,
        timeout: 0,
      });
      if (response.status !== 200) {
        throw `response other than 200 - ${response.status}: ${response.statusText}`;
      }
      let responseJSON = await response.json();
      let imageURLArray = module.exports.parseArrayOfProductURLs(
        responseJSON.data
      );
      for (let index = 0; index < imageURLArray.length; index++) {
        const currentImage = imageURLArray[index];
        await module.exports.saveImageWithSKUFilename(currentImage);
      }
      if (responseJSON.meta.cursor) {
        module.exports.getProductImagesFromSalsifyWithCursor(
          responseJSON.meta.cursor
        );
        console.log(`cursor update: ${cursor}`);
      } else {
        console.log(`hit an empty cursor, stopping`);
      }
    } catch (error) {
      // console.error(`Error fetching products from Salsify: ${error}`);
    }
  },
  parseArrayOfProductURLs: (productArray) => {
    let imageURLArray = [];
    productArray.forEach((product) => {
      if (product["salsify:digital_assets"]) {
        const imageObject = {
          salsifyId: product["salsify:id"],
          sku: product["PRODUCT CODE"].replace(/\//g, "-"),
          URL: product["salsify:digital_assets"][0]["salsify:url"],
          fileExtension: product["salsify:digital_assets"][0]["salsify:format"],
          updatedAt: product["salsify:digital_assets"][0]["salsify:updated_at"],
        };
        imageURLArray.push(imageObject);
      } else {
        console.log(`No images for sku ${product["PRODUCT CODE"]}`);
      }
    });
    return imageURLArray;
  },
  saveImageWithSKUFilename: async (image, variantNumber) => {
    if (["jpg", "png", "bmp", "tiff"].includes(image.fileExtension)) {
      const url = image.URL;
      let path;
      if (variantNumber) {
        path = `${process.env.PEPPERI_IMAGES_LOCATION}/${image.sku}_${variantNumber}.jpg`;
      } else {
        path = `${process.env.PEPPERI_IMAGES_LOCATION}/${image.sku}.jpg`;
      }
      if (fs.existsSync(path)) {
        console.log(
          `Image ${pathTools.basename(path)} already exists, will update`
        );
      }
      try {
        const response = await fetch(url);
        const buffer = await response.buffer();
        await sharp(buffer)
          .resize(800)
          .toFormat("jpg")
          .jpeg({ mozjpeg: true })
          .toFile(path);
        console.log(
          `Saved image ${pathTools.basename(path)}, salsify id ${
            image.salsifyId
          }`
        );
        return path;
      } catch (error) {
        console.log(
          `Error saving image variant #${variantNumber} for sku ${image.sku}, salsify id ${image.salsifyId}, error ${error}`
        );
      }
    } else {
      console.log(
        `Error: image ${image.sku}.${image.fileExtension} is in wrong format (${image.fileExtension}), salsify id ${image.salsifyId}`
      );
    }
  },
};
