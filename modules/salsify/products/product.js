const fetch = require("node-fetch");
const createCsvWriter = require("csv-writer").createObjectCsvWriter;
const _ = require("lodash");
require("dotenv").config();
const { promisify } = require("util");
const sleep = promisify(setTimeout);
const { prepareHeaders } = require("../../helpers/apitools");
const fieldsToIncludeInParent = require("./fieldsToIncludeInParent");
const {
  SalsifyProduct,
  SalsifyProductParent,
  SalsifyGlobalProduct,
} = require("../../../models");
const { ProductTools } = require("../helpers/ProductTools");
const { SalsifyAPI } = require("../helpers/SalsifyAPI");
const {
  addToSalsifyProductBuffer,
  addToSalsifyParentBuffer,
  updateSelectedBufferProductsFromSalsify,
  updateSelectedBufferProductParentsFromSalsify,
} = require("./productbuffer");
const {
  salsifyIntoPepperiBulk,
  salsifyIntoBufferProduct,
  salsifyIntoBufferParentProduct,
  noBrandProduct,
  salsifySimpleIntoParent,
  salsifySimpleIntoChild,
  changeBufferSimpleIntoChild,
  salsifySimpleIntoParentForCSV,
} = require("./productdecorator");

module.exports = {
  // TODO: produce a CSV of all the simples, for BM's to fill in the parent SKU.
  // It's a list of parents, with all their attributes.
  // That CSV can then be imported directly.

  produceSimpleProductParentsCSV: async (brand) => {
    const query = brand ? { where: { brand } } : {};
    const salsifyProducts = await SalsifyProduct.findAll(query);

    let salsifySimpleProducts =
      ProductTools.filterSimpleProducts(salsifyProducts);

    const decoratedParents = salsifySimpleProducts.map(function (
      simpleProduct
    ) {
      return salsifySimpleIntoParentForCSV(simpleProduct, this);
    },
    fieldsToIncludeInParent);

    let header = fieldsToIncludeInParent.map((key) => {
      return {
        id: ProductTools.convertToCSVHeaderKey(key),
        title: key,
      };
    });
    for (let index = 0; index < 10; index++) {
      header.push({
        id: `additionalImage_${index}`,
        title: "Additional Image",
      });
    }
    const brandSlug = brand
      ? ProductTools.convertToCSVHeaderKey(brand)
      : "allbrands";

    const csvWriter = createCsvWriter({
      path: `./salsifySimpleProductsToParents_${brandSlug}.csv`,
      header,
    });

    const records = decoratedParents;
    await csvWriter.writeRecords(records);
    console.log(`Created Parents for ${decoratedParents.length} simple SKUs.`);
  },

  convertSimpleProductsToParents: async (brand = "Black Diamond") => {
    const API = new SalsifyAPI();

    const query = brand ? { where: { brand } } : {};
    const salsifyProducts = await SalsifyProduct.findAll(query);
    const salsifyProductParents = await SalsifyProductParent.findAll(query);

    const arrayOfChildProductsToProcess = [
      //   "BD0625940000ALLS",
      //   "BD0628050000ALLS",
      //   "BD1016050000ALL1",
      //   "BD1021400000ALL1",
      //   "BD1021740000ALL1",
      //   "BD1021820000ALL1",
      //   "BD102183FREDALL1",
      //   "BD1021840000ALL1",
      //   "SN6400010000ALL1",
    ];

    const arrayOfProductsToSyncToBuffer = [
      "BD0625940000ALLS",
      "BD0628050000ALLS",
      "BD1016050000ALL1",
      "BD1021360000ALL1",
      "BD1021400000ALL1",
      "BD1021740000ALL1",
      "BD1021820000ALL1",
      "BD102183FREDALL1",
      "BD1021840000ALL1",
      "BD1021860000ALL1",
      "BD1021870000ALL1",
      "BD1021880000ALL1",
      "BD109101FREDALL1",
      "BD1091020000ALL1",
      "BD1091030000ALL1",
      "BD1091040000ALL1",
      "BD1091050000ALL1",
      "BD110552BLAKALL1",
      "BD1105540000ALL1",
      "BD1105650000ALL1",
      "BD1105660000ALL1",
      "BD1105670000ALL1",
      "BD1115670000ALL1",
      "BD1115820000ALL1",
      "BD1115830000ALL1",
      "BD1115840000ALL1",
      "BD1120790000ALL1",
      "BD1120800000ALL1",
      "BD1120820000ALL1",
      "BD1121180000ALL1",
      "BD1121190000ALL1",
      "BD1122234002ALL1",
      "BD1122260000ALL1",
      "BD1125021007ALL1",
      "BD1125033000ALL1",
      "BD1125046016ALL1",
      "BD1125054020ALL1",
      "BD1125066006ALL1",
      "BD1125076006ALL1",
      "BD1125083000ALL1",
      "BD1125093007ALL1",
      "BD1125133000ALL1",
      "BD1125143010ALL1",
      "BD1125156017ALL1",
      "BD1125163012ALL1",
      "BD1125261007ALL1",
      "BD1125273000ALL1",
      "BD11510500001651",
      "BD1510010000ALL1",
      "BD1510020000ALL1",
      "BD1635150000ALL1",
      "BD1635190000ALL1",
      "BD1635290000ALL1",
      "BD1636290000ALL1",
      "BD1636310000ALL1",
      "BD1636320000ALL1",
      "BD2102770000ALL1",
      "BD2102780000ALL1",
      "BD2102910000ALL1",
      "BD2102920000ALL1",
      "BD2202350000ALL1",
      "BD2251220000ALL1",
      "BD2251230000ALL1",
      "BD2252150000ALL1",
      "BD2252350000ALL1",
      "BD32311480020601",
      "BD32311760090601",
      "BD3810830000ALL1",
      "BD3811221004ALL1",
      "BD3811270000ALL1",
      "BD3811290000ALL1",
      "BD4000690000ALL1",
      "BD4120850000ALL1",
      "BD4120860000ALL1",
      "BD4130000000ALL1",
      "BD5202000000ALL1",
      "BD5202200000ALL1",
      "BD5504930000ALL1",
      "BD5504980000ALL1",
      "BD5504990000ALL1",
      "BD5505040000ALL1",
      "BD5505170002ALL1",
      "BD5505190000ALL1",
      "BD5505210000ALL1",
      "BD5505220000ALL1",
      "BD5508030000ALL1",
      "BD5508080000ALL1",
      "BD5508180000ALL1",
      "BD5508580000ALL1",
      "BD6200050000ALL1",
      "BD6200200000ALL1",
      "BD6200600000ALL1",
      "BD620062BLAKALL1",
      "BD6200650000ALL1",
      "BD620078BLAKALL1",
      "BD620548BLAKALL1",
      "BD6206540004ALL1",
      "BD6206570000ALL1",
      "BD6206704031ALL1",
      "BD620713GRPHALL1",
      "BD620719BLAKALL1",
      "BD6301454026ALL1",
      "BD630148BLAKALL1",
      "BD6301510000ALL1",
      "BD650074BLAKALL1",
      "BD650075KFSHALL1",
      "BD6600120000ALL1",
      "BD6600500000M_L1",
      "BD681157BLAKM_L1",
      "BD681159BLAKALL1",
      "BD681160BLAKM_L1",
      "BD6812190000ALL1",
      "BD8001500000ALL1",
      "BD8019990000ALL1",
      "BD810020YELOALL1",
      "BD810030YELOALL1",
      "BD8101474028ALL1",
      "BD8101483013ALL1",
      "BD8101543012ALL1",
      "BD8101564029ALL1",
      "BD8101814029ALL1",
      "BD8102700000ALL1",
      "BD8102750000ALL1",
      "BD8102800000ALL1",
      "BD8104510000ALL1",
      "BD8104580000ALL1",
      "BD8104600000ALL1",
      "SN6400010000ALL1",
      "SN6400020000ALL1",
      "SN6400030000ALL1",
      "SN6400040000ALL1",
      "SN6400050000ALL1",
      "SN6400060000ALL1",
      "SN6400070000ALL1",
    ];

    for (
      let index = 0;
      index < arrayOfProductsToSyncToBuffer.length;
      index += 100
    ) {
      const currentElements = arrayOfProductsToSyncToBuffer.slice(
        index,
        index + 100
      );
      const arrayOfSyncProducts = await API.getInfoForArrayOfProductIDs(
        currentElements
      );
      await updateSelectedBufferProductsFromSalsify(
        arrayOfSyncProducts,
        salsifyProducts,
        salsifyProductParents
      );
    }

    return;

    let salsifySimpleProducts =
      ProductTools.filterSimpleProducts(salsifyProducts);
    //salsifySimpleProducts = salsifySimpleProducts.slice(0, 8);

    if (arrayOfChildProductsToProcess.length > 0) {
      salsifySimpleProducts = salsifySimpleProducts.filter(function (product) {
        return this.includes(product.sku);
      }, arrayOfChildProductsToProcess);
    }

    let newParentSKUs = [];

    for (const simpleProduct of salsifySimpleProducts) {
      const parentVersionOfSimpleProduct =
        salsifySimpleIntoParent(simpleProduct);
      const newlyCreatedParentSKU =
        parentVersionOfSimpleProduct["PRODUCT CODE"];
      newParentSKUs.push(newlyCreatedParentSKU);

      // Only updating the necessary parent/child attributes, not removing fields yet
      const updatedChildProduct = salsifySimpleIntoChild(
        simpleProduct,
        newlyCreatedParentSKU
      );
      // const existingParent = await API.getSalsifyProductInfo(
      //   newlyCreatedParentSKU
      // );
      // if (!existingParent) {
      //   const parentProductResult = await API.createOneSalsifyProduct(
      //     parentVersionOfSimpleProduct
      //   );
      //   const childProductResult = await API.updateOneSalsifyProduct(
      //     updatedChildProduct
      //   );
      // } else {
      //   console.log(
      //     `Parent ${newlyCreatedParentSKU} for ${simpleProduct.sku} has already been created`
      //   );
      // }
    }
    console.log("Updating buffer parents...");
    for (let index = 0; index < newParentSKUs.length; index += 100) {
      const currentElements = newParentSKUs.slice(index, index + 100);
      const arrayOfParentProducts = await API.getInfoForArrayOfProductIDs(
        currentElements
      );
      await updateSelectedBufferProductParentsFromSalsify(
        arrayOfParentProducts,
        salsifyProducts,
        salsifyProductParents
      );
    }
    console.log("...done.");

    console.log("Updating buffer products...");
    const arrayOfChildProductIDs = salsifySimpleProducts.map(
      (product) => product.sku
    );

    for (let index = 0; index < newParentSKUs.length; index += 100) {
      const arraySlice = arrayOfChildProductIDs.slice(index, index + 100);
      const arrayOfChildProducts = await API.getInfoForArrayOfProductIDs(
        arraySlice
      );
      await updateSelectedBufferProductsFromSalsify(
        arrayOfChildProducts,
        salsifyProducts,
        salsifyProductParents
      );
    }

    console.log("...done.");
  },
  getProductsWithNoBrand: async () => {
    try {
      const headers = await prepareHeaders();

      let pepperiProductsWithNoBrand = [];

      let salsifyProductResults =
        await module.exports.getProductsFromSalsifyWithNoBrand(0, headers);
      pepperiProductsWithNoBrand.push(...salsifyProductResults.products);

      while (
        salsifyProductResults.paging.nextPage <
        salsifyProductResults.paging.totalPages + 1
      ) {
        salsifyProductResults =
          await module.exports.getProductsFromSalsifyWithNoBrand(
            salsifyProductResults.paging.nextPage,
            headers
          );
        pepperiProductsWithNoBrand.push(...salsifyProductResults.products);
      }

      console.log(
        `There are ${pepperiProductsWithNoBrand.length} products with no brand in Pepperi.`
      );

      for (const product of pepperiProductsWithNoBrand) {
        console.log(`Name: ${product.Name}, SKU: ${product.SKU}`);
      }
    } catch (error) {
      console.error(`Error getting no-brand products from Salsify, ${error}`);
      return;
    }
  },

  getProductsFromSalsifyWithPage: async (page = 0, headers) => {
    let returnPayload = {
      products: [],
      paging: {
        totalPages: 0,
        nextPage: 0,
      },
    };
    let url;
    try {
      url = `https://app.salsify.com/api/v1/orgs/${process.env.SALSIFY_ORG_ID}/products?access_token=${process.env.SALSIFY_TOKEN}&page=${page}`;
      const response = await fetch(url, {
        headers: headers.SalsifyHeaders,
        timeout: 0,
      });
      if (response.status !== 200) {
        throw `response other than 200 - ${response.status}: ${response.statusText}`;
      }
      let responseJSON = await response.json();
      //console.log(responseJSON.meta);
      responseJSON.data.forEach((product) => {
        returnPayload.products.push(salsifyIntoPepperiBulk(product));
      });
      returnPayload.paging.totalPages = responseJSON.meta.total_entries / 100;
      returnPayload.paging.nextPage = page + 1;
      return returnPayload;
    } catch (error) {
      console.error(`Error fetching products from Salsify: ${error}`);
    }
  },

  getProductsFromSalsifyWithNoBrand: async (page = 0, headers) => {
    let returnPayload = {
      products: [],
      paging: {
        totalPages: 0,
        nextPage: 0,
      },
    };
    let url;
    try {
      url = `https://app.salsify.com/api/v1/orgs/${process.env.SALSIFY_ORG_ID}/products?access_token=${process.env.SALSIFY_TOKEN}&page=${page}`;
      const response = await fetch(url, {
        headers: headers.SalsifyHeaders,
        timeout: 0,
      });
      if (response.status !== 200) {
        throw `response other than 200 - ${response.status}: ${response.statusText}`;
      }
      let responseJSON = await response.json();
      console.log(responseJSON.meta);
      for (const product of responseJSON.data) {
        if (product["BRAND"] == undefined) {
          returnPayload["products"].push(noBrandProduct(product));
        }
      }
      returnPayload.paging.totalPages = responseJSON.meta.total_entries / 100;
      returnPayload.paging.nextPage = page + 1;
      return returnPayload;
    } catch (error) {
      console.error(`Error fetching products from Salsify: ${error}`);
    }
  },
  getParentProductPromises: async (parentProductsPromises) => {
    return Promise.all(parentProductsPromises).catch((e) =>
      console.error(`Error fetching parent products: ${e}`)
    );
  },
  getParentProductFromSalsify: async (product, headers) => {
    let url = `https://app.salsify.com/api/v1/orgs/${process.env.SALSIFY_ORG_ID}/products/${product.parent_sku}?access_token=${process.env.SALSIFY_TOKEN}`;
    console.log(url);

    const response = await fetch(url, {
      headers: headers.SalsifyHeaders,
      timeout: 0,
    });

    if (response.status !== 200) {
      return response.status;
    }
    const productData = await response.json();
    return salsifyIntoBufferParentProduct(productData);
  },
};
