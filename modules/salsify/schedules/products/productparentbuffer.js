const {
  syncSalsifyProductParentsToDB,
} = require("../../products/productbuffer");
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

const productParentBuffer = async () => {
  try {
    // console.log("Updating product parent buffer...");
    await syncSalsifyProductParentsToDB();
    // console.log("... done updating product parent buffer.");
  } catch (error) {
    console.error(`Error product parent buffer: ${error}`);
  }
};

productParentBuffer();
