const { syncSalsifyProductsToDB } = require("../../products/productbuffer");
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

// (async () => {
//   await syncSalsifyProductsToDB();
// })();

const productBuffer = async () => {
  try {
    console.log("Updating product buffer...");
    await syncSalsifyProductsToDB();
    console.log("... done updating product buffer.");
  } catch (error) {
    console.error(`Error product buffer: ${error}`);
  }
};

productBuffer();
