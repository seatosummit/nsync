const fetch = require("node-fetch");
const _ = require("lodash");
require("dotenv").config();
const { prepareHeaders } = require("../../helpers/apitools");

module.exports = {
  getAllValuesForProperty: async (
    propertyName = "Black Diamond Shopify Category"
  ) => {
    try {
      const headers = await prepareHeaders();
      let salsifyPropertyValues = [];
      let keepGettingProperties = true;
      let salsifyPropertyResults =
        await module.exports.getPropertyValuesFromSalsify(
          null,
          headers,
          propertyName
        );
      salsifyPropertyValues.push(...salsifyPropertyResults.propertyValues);

      for (const parentToFollow of salsifyPropertyResults.parentsToFollow) {
        salsifyPropertyResults =
          await module.exports.getPropertyValuesFromSalsify(
            parentToFollow,
            headers,
            propertyName
          );
        salsifyPropertyValues.push(...salsifyPropertyResults.propertyValues);
        for (const parentToFollow of salsifyPropertyResults.parentsToFollow) {
          salsifyPropertyResults =
            await module.exports.getPropertyValuesFromSalsify(
              parentToFollow,
              headers,
              propertyName
            );
          salsifyPropertyValues.push(...salsifyPropertyResults.propertyValues);
          for (const parentToFollow of salsifyPropertyResults.parentsToFollow) {
            salsifyPropertyResults =
              await module.exports.getPropertyValuesFromSalsify(
                parentToFollow,
                headers,
                propertyName
              );
            salsifyPropertyValues.push(
              ...salsifyPropertyResults.propertyValues
            );
          }
        }
      }

      console.log(
        `There are ${salsifyPropertyValues.length} properties for ${propertyName} in Salsify.`
      );

      const arrayOfSortedValues = salsifyPropertyValues.map(
        (propertyValueEntry) => {
          return {
            name: propertyValueEntry.name,
            salsify_id: propertyValueEntry.id,
          };
        }
      );

      return arrayOfSortedValues;
    } catch (error) {
      console.error(`Error getting properties from Salsify, ${error}`);
      return;
    }
  },

  getPropertyValuesFromSalsify: async (
    categoryParent,
    headers,
    salsifyPropertyName = "Black Diamond Shopify Category"
  ) => {
    const encodedSalsifyPropertyName = encodeURIComponent(salsifyPropertyName);
    let queryParameters = "page=1&per_page=50&serialize_system_ids=true";
    queryParameters += `&access_token=${process.env.SALSIFY_TOKEN}`;
    if (categoryParent) {
      queryParameters += `&within_value=${categoryParent}`;
    }
    let returnPayload = {
      propertyValues: [],
      parentsToFollow: [],
    };
    const queryURL = `https://app.salsify.com/api/orgs/s-3a66e755-76bc-4004-8166-b02b01ca7094/properties/${encodedSalsifyPropertyName}/enumerated_values?${queryParameters}`;
    try {
      const response = await fetch(queryURL, {
        type: "get",
        headers: headers.SalsifyHeaders,
        timeout: 0,
      });
      if (response.status !== 200) {
        throw `response other than 200 - ${response.status}: ${response.statusText}`;
      }
      let responseJSON = await response.json();
      responseJSON.data.forEach((property) => {
        returnPayload.propertyValues.push({
          name: property.name,
          id: property.external_id,
          salsify_id: property.id,
        });
      });
      responseJSON.data.forEach((property) => {
        if (property.has_children) {
          returnPayload.parentsToFollow.push(property.external_id);
        }
      });

      return returnPayload;
    } catch (error) {
      console.error(`Error fetching properties from Salsify: ${error}`);
    }
  },
};
