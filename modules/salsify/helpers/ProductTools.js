const _ = require("lodash");
const slugify = require("slugify");
class ProductTools {
  constructor() {}
  static isSimpleProduct(salsifyProduct) {
    if (
      _.isNil(salsifyProduct.child_skus) &&
      _.isNil(salsifyProduct.parent_sku) &&
      salsifyProduct.is_child == false
    ) {
      return true;
    } else {
      return false;
    }
  }
  static filterSimpleProducts(salsifyProductsArray) {
    return salsifyProductsArray.filter((product) => {
      return this.isSimpleProduct(product);
    });
  }

  static generateParentSKU(salsifyProduct) {
    const currentSKU = salsifyProduct.sku;
    const currentBrand = salsifyProduct.brand;
    switch (currentBrand) {
      case "Black Diamond":
        // TODO: create other brands, Black Diamond is the first 8 letters of the SKU
        const parentSKU = currentSKU.slice(0, 8);
        return parentSKU;
      case "Biolite":
        break;
      case "Evolv":
        return currentSKU;
      case "Croakies":
        return currentSKU.slice(0, 4);
      default:
        return currentSKU;
    }
  }

  static extractTechSpecs(salsifyProductAttributes) {
    let extractedTechSpecs = {};
    const techSpecsRegex = /Tech spec/;
    for (const key in salsifyProductAttributes) {
      if (techSpecsRegex.test(key)) {
        const newSpec = { [key]: salsifyProductAttributes[key] };
        extractedTechSpecs = { ...extractedTechSpecs, ...newSpec };
      }
    }
    return extractedTechSpecs;
  }
  static extractUSPs(salsifyProductAttributes) {
    let extractedUSPs = {};
    const uspArray = _.values(
      _.pickBy(salsifyProductAttributes, function (value, key) {
        return /USP \d/.test(key);
      })
    );
    for (const [index, currentUSP] of uspArray.entries()) {
      extractedUSPs[`USP ${index + 1}`] = currentUSP;
    }
    return extractedUSPs;
  }

  static convertToCSVHeaderKey(value) {
    const variousCharactersRegex = /[ '`~!@,-\/\"=.?&]+/g;
    const removeCharactersRegex = /[()]+/g;
    return value
      .replace(variousCharactersRegex, "_")
      .replace(removeCharactersRegex, "")
      .toLowerCase();
  }
}
module.exports.ProductTools = ProductTools;
