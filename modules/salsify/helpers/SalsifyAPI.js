const fetch = require("node-fetch");
require("dotenv").config();
const throttle = require("fetch-throttle");
const { valueExistsAndIsNotEmpty } = require("../../helpers/stringTools");
const fetchThrottle = throttle(fetch, 5, 1000);

class SalsifyAPI {
  constructor(org = process.env.SALSIFY_ORG_ID) {
    this.connection = {};
    this.currentResultSet = [];
    this.baseURL = `https://app.salsify.com/api/v1/orgs/${org}`;
    this.accessToken = process.env.SALSIFY_NEW_TOKEN;
    // this.accessToken = process.env.SALSIFY_DIGITAL_TOKEN;
    this.accessTokenString = `access_token=${this.accessToken}`;
  }

  async getSalsifyProductInfo(uniqueProductID) {
    try {
      if (!uniqueProductID) throw `No SKU provided. Check your payload.`;
      let url = `${this.baseURL}/products/${uniqueProductID}`;
      const apiCallResult = await this.SalsifyAPICall(url);
      return apiCallResult;
    } catch (error) {
      console.error(
        `Error getting info for Salsify product ${uniqueProductID}: ${error}`
      );
    }
  }

  async createOneSalsifyProduct(productCreatePayload) {
    const uniqueProductID = productCreatePayload["PRODUCT CODE"];
    if (!uniqueProductID) throw `No SKU provided. Check your payload.`;
    try {
      let url = `${this.baseURL}/products`;
      const apiCallResult = await this.SalsifyAPICall(
        url,
        "POST",
        productCreatePayload
      );
      // console.log(`Created ${uniqueProductID} in Salsify.`);
      return apiCallResult;
    } catch (error) {
      console.error(
        `Error creating new Salsify product ${uniqueProductID}: ${error}`
      );
    }
  }
  async updateOneSalsifyProduct(productUpdatePayload) {
    const uniqueProductID = productUpdatePayload["PRODUCT CODE"];
    if (!uniqueProductID) throw `No SKU provided. Check your payload.`;
    try {
      const url = `${this.baseURL}/products/${uniqueProductID}`;
      const apiCallResult = await this.SalsifyAPICall(
        url,
        "put",
        productUpdatePayload
      );
      // console.log(`Updated ${uniqueProductID} in Salsify.`);
      return apiCallResult;
    } catch (error) {
      console.error(
        `Error updating Salsify product ${uniqueProductID}: ${error}`
      );
    }
  }

  async updateBulkSalsifyProducts(productsUpdatePayloadArray) {
    const uniqueProductID = productUpdatePayload["PRODUCT CODE"];
    if (!uniqueProductID) throw `No SKU provided. Check your payload.`;
    try {
      const url = `${this.baseURL}/products/${uniqueProductID}`;
      const apiCallResult = await this.SalsifyAPICall(
        url,
        "put",
        productUpdatePayload
      );
      console.log(`Updated ${uniqueProductID} in Salsify.`);
      return apiCallResult;
    } catch (error) {
      console.log(
        `Error updating Salsify product ${uniqueProductID}: ${error}`
      );
    }
  }

  async getInfoForArrayOfProductIDs(arrayOfProductIDs = []) {
    if (arrayOfProductIDs.length > 0) {
      const url = `${this.baseURL}/products`;
      try {
        const productsArray = await this.SalsifyAPICall(url, "REPORT", {
          ids: arrayOfProductIDs,
        });
        return productsArray;
      } catch (error) {
        console.error(
          `Error getting info for products ${arrayOfProductIDs}: ${error}`
        );
      }
    }
  }

  async getDigitalAssets(page = 1) {
    const url = `${this.baseURL}/digital_assets?page=${page}`;
    try {
      const digitalAssetsResponse = await this.SalsifyAPICall(url, "GET");
      return digitalAssetsResponse;
    } catch (error) {
      console.error(`Error getting digital assets from Salsify: ${error}`);
    }
  }
  async getDigitalAssetsWithCursor(cursor = null) {
    const url = `${this.baseURL}/digital_assets?cursor=${cursor}`;
    try {
      const digitalAssetsResponse = await this.SalsifyAPICall(url, "GET");
      return digitalAssetsResponse;
    } catch (error) {
      console.error(`Error getting digital assets from Salsify: ${error}`);
    }
  }

  async createOneDigitalAsset(sourceUrl, name) {
    const url = `${this.baseURL}/digital_assets`;
    const payload = {
      "salsify:source_url": sourceUrl,
      "salsify:name": name,
    };

    try {
      const digitalAssetCreationResponse = await this.SalsifyAPICall(
        url,
        "POST",
        payload
      );
      return digitalAssetCreationResponse;
    } catch (error) {
      console.error(`Error getting digital assets from Salsify: ${error}`);
    }
  }
  async createManyDigitalAssets(arrayOfDigitalAssetURLs) {
    if (!valueExistsAndIsNotEmpty(arrayOfDigitalAssetURLs))
      throw `No array of digital asset URLs`;
    const url = `${this.baseURL}/digital_assets/_upsert`;
    const payload = arrayOfDigitalAssetURLs;
    try {
      const digitalAssetCreationResponse = await this.SalsifyAPICall(
        url,
        "PUT",
        payload
      );
      return digitalAssetCreationResponse;
    } catch (error) {
      console.error(`Error getting digital assets from Salsify: ${error}`);
    }
  }

  async associateDigitalAssetWithProduct(sku, type, digitalAssetId) {
    if (!digitalAssetId) throw `No digital asset id for ${sku}`;
    const url = `${this.baseURL}/products/${sku}`;
    let payload;
    switch (type) {
      case "Primary":
        payload = { "Primary Image": digitalAssetId };
        break;
      case "Packaging":
        payload = { "Packaging Images": digitalAssetId };
        break;
      case "Additional":
        if (digitalAssetId.length > 1) {
          payload = { "Additional Image": digitalAssetId };
        } else {
          payload = { "Additional Image": digitalAssetId[0] };
        }

        break;
      default:
        association = null;
        break;
    }
    try {
      const digitalAssetCreationResponse = await this.SalsifyAPICall(
        url,
        "PUT",
        payload
      );
      return digitalAssetCreationResponse;
    } catch (error) {
      console.error(`Error associating digital asset with product: ${error}`);
    }
  }

  async updateMultipleProducts(payloadArray) {
    if (!valueExistsAndIsNotEmpty(payloadArray))
      throw `Don't bother if you're not providing an array of SKUs and values`;
    const url = `${this.baseURL}/products`;
    try {
      const bulkProductUpdateResponse = await this.SalsifyAPICall(
        url,
        "PUT",
        payloadArray
      );
      return bulkProductUpdateResponse;
    } catch (error) {
      console.error(`Error bulk updating products: ${error}`);
    }
  }

  async associateDigitalAssetWithMultipleProducts(
    arrayOfSkus,
    type,
    digitalAssetId
  ) {
    if (!valueExistsAndIsNotEmpty(digitalAssetId))
      throw `No digital asset id given`;
    if (!valueExistsAndIsNotEmpty(arrayOfSkus)) throw `No skus array given`;
    const url = `${this.baseURL}/products`;
    const payload = arrayOfSkus.map((sku) => {
      const individualPayload = {
        "PRODUCT CODE": sku,
      };
      switch (type) {
        case "Primary":
          individualPayload["Primary Image"] = digitalAssetId;
          break;
        case "Packaging":
          individualPayload["Packaging Images"] = digitalAssetId;
          break;
        case "Additional":
          individualPayload["Additional Image"] = digitalAssetId;
          break;
        default:
          association = null;
          break;
      }
      return individualPayload;
    });
    try {
      const digitalAssetCreationResponse = await this.SalsifyAPICall(
        url,
        "PUT",
        payload
      );
      return digitalAssetCreationResponse;
    } catch (error) {
      console.error(
        `Error bulk associating digital asset with product: ${error}`
      );
    }
  }

  async getProductsWithCursor(cursor = null) {
    const url = cursor
      ? `${this.baseURL}/products?cursor=${cursor}`
      : `${this.baseURL}/products`;
    try {
      const digitalAssetsResponse = await this.SalsifyAPICall(url, "GET");
      return digitalAssetsResponse;
    } catch (error) {
      console.error(`Error getting digital assets from Salsify: ${error}`);
    }
  }

  async getProductsBasedOnArray(args) {
    const skuArray = args?.skuArray;
    const url = `${this.baseURL}/products`;
    const payload = { ids: skuArray };
    const salsifyProductResults = await this.SalsifyAPICall(
      url,
      "REPORT",
      payload
    );
    return salsifyProductResults;
  }

  async SalsifyAPICall(url, method = "GET", body = null) {
    const settings = {
      method,
      headers: {
        "Authorization": `Bearer ${this.accessToken}`,
        "Content-length": 0,
        "Content-Type": "application/json",
      },
      timeout: 0,
      body: body ? JSON.stringify(body) : null,
    };
    const response = await fetch(url, settings);

    switch (response.status) {
      case 204:
        //console.log(`Product updated successfully.`);
        return true;
      case 400:
        throw new Error(`Bad Request. Check what you're sending.`);
      case 401:
        throw new Error(`Unauthorized. Check your API credentials.`);
      case 403:
        throw new Error(`Forbidden. Check your API credentials.`);
      case 404:
        if (method == "put") {
          throw new Error(`SKU not found. Check your product ID`);
        } else {
          console.error(`SKU wasn't found.`);
          return false;
        }
      case 408:
        throw new Error(`Timeout. Issue with Salsify API`);
      case 500:
        throw new Error(`500 error with API`);
      default:
        // this was a normal 200
        break;
    }
    const responseData = await response.json();
    return responseData;
  }
}
module.exports.SalsifyAPI = SalsifyAPI;
