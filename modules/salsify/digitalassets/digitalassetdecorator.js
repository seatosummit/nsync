const _ = require("lodash");
require("dotenv").config();
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

module.exports = {
  salsifyIntoBufferDigitalAsset: (
    digitalAsset,
    salsifyProductsBuffer,
    salsifyProductParentsBuffer
  ) => {
    const salsifyDigitalAssetID = digitalAsset["salsify:id"];
    let salsify_au_ids = [];
    let salsify_usa_ids = [];
    let salsify_primary_skus = [];
    let salsify_additional_skus = [];
    let salsify_packaging_skus = [];
    let salsify_brand;
    const allBufferProducts = [
      ...salsifyProductsBuffer,
      ...salsifyProductParentsBuffer,
    ];
    for (const salsifyItem of allBufferProducts) {
      const primaryImage = [].concat(salsifyItem.primary_image);
      const additionalImages = [].concat(salsifyItem.additional_images);
      const packagingImages = [].concat(salsifyItem.packaging_images);
      if (primaryImage.includes(salsifyDigitalAssetID)) {
        const newAssociation = {
          role: "primary",
          sku: salsifyItem.sku,
        };
        salsify_au_ids.push(newAssociation);
        salsify_primary_skus.push(salsifyItem.sku);
        salsify_brand = salsifyItem.brand;
      }

      if (additionalImages.includes(salsifyDigitalAssetID)) {
        const newAssociation = {
          role: "additional",
          sku: salsifyItem.sku,
        };
        salsify_au_ids.push(newAssociation);
        salsify_additional_skus.push(salsifyItem.sku);
        salsify_brand = salsifyItem.brand;
      }

      if (packagingImages.includes(salsifyDigitalAssetID)) {
        const newAssociation = {
          role: "packaging",
          sku: salsifyItem.sku,
        };
        salsify_au_ids.push(newAssociation);
        salsify_packaging_skus.push(salsifyItem.sku);
        salsify_brand = salsifyItem.brand;
      }
    }

    const decoratedDigitalAsset = {
      salsify_id: digitalAsset["salsify:id"],
      salsify_url: digitalAsset["salsify:url"],
      salsify_name: digitalAsset["salsify:name"],
      salsify_status: digitalAsset["salsify:status"],
      salsify_asset_height_pixels: digitalAsset["salsify:asset_height"],
      salsify_asset_width_pixels: digitalAsset["salsify:asset_width"],
      salsify_brand,
      salsify_asset_resource_type: digitalAsset["salsify:asset_resource_type"],
      salsify_filename: digitalAsset["salsify:filename"],
      salsify_bytes: digitalAsset["salsify:bytes"],
      salsify_format: digitalAsset["salsify:format"],
      salsify_etag: digitalAsset["salsify:etag"],
      salsify_system_id: digitalAsset["salsify:system_id"],
      salsify_primary_skus,
      salsify_additional_skus,
      salsify_packaging_skus,
      salsify_au_ids,
      salsify_usa_ids,
      snapshot: digitalAsset,
    };
    return decoratedDigitalAsset;
  },
};
