const fetch = require("node-fetch");
const throttle = require("fetch-throttle");
const fetchThrottle = throttle(fetch, 5, 1000);
const _ = require("lodash");
require("dotenv").config();
const { promisify } = require("util");
const sleep = promisify(setTimeout);
const { prepareHeaders } = require("../../helpers/apitools");
const {
  SalsifyProduct,
  SalsifyProductParent,
  SalsifyDigitalAsset,
} = require("../../../models");
const { salsifyIntoBufferDigitalAsset } = require("./digitalassetdecorator");
const { SalsifyAPI } = require("../helpers/SalsifyAPI");
const cliProgress = require("cli-progress");
const { SequelizeOperations } = require("../../sequelize/SequelizeOperations");

module.exports = {
  updateDigitalAssetsBufferFromSalsify: async () => {
    const API = new SalsifyAPI();
    const progressbar = new cliProgress.SingleBar(
      {},
      cliProgress.Presets.shades_classic
    );
    let savePromises = [];
    const allSalsifyDigitalAssets = await SalsifyDigitalAsset.findAll({});
    const allSalsifyBufferProducts = await SalsifyProduct.findAll({});
    const allSalsifyBufferProductParents = await SalsifyProductParent.findAll(
      {}
    );
    let pageNumber = 1;
    let cursor;
    let response = await API.getDigitalAssets(pageNumber);
    const totalEntries = response.meta.total_entries;
    cursor = response.meta.cursor;
    const totalPages = Math.round(totalEntries / 100);
    progressbar.start(totalPages, 0);

    while (pageNumber < totalPages) {
      try {
        await module.exports.runBatchOfDigitalAssetBufferUpdates(
          response,
          allSalsifyBufferProducts,
          allSalsifyBufferProductParents,
          allSalsifyDigitalAssets,
          progressbar
        );
        pageNumber++;
        progressbar.increment();
        response = await API.getDigitalAssetsWithCursor(cursor);
        cursor = response.meta.cursor;
      } catch (error) {
        console.error(`Error: ${error}`);
      }
    }
    console.log(`Done.`);
  },

  runBatchOfDigitalAssetBufferUpdates: async (
    apiResponse,
    allSalsifyBufferProducts,
    allSalsifyBufferProductParents,
    allSalsifyDigitalAssets
  ) => {
    let savePromises = [];
    const digitalAssetsFromResponse = apiResponse.data;
    if (!digitalAssetsFromResponse) {
      throw apiResponse;
    }
    for (const digitalAsset of digitalAssetsFromResponse) {
      const decoratedDigitalAsset = salsifyIntoBufferDigitalAsset(
        digitalAsset,
        allSalsifyBufferProducts,
        allSalsifyBufferProductParents
      );
      savePromises.push(
        await SequelizeOperations.Upsert(
          decoratedDigitalAsset,
          "salsify_id",
          SalsifyDigitalAsset
        )
      );
      try {
        const results = await Promise.all(savePromises);
      } catch (error) {
        console.error(`Error saving SalsifyDigitalAsset batch: ${error}`);
      }
    }
  },

  addToSalsifyDigitalAssetsBuffer: async (decoratedDigitalAsset, buffer) => {
    try {
      if (decoratedDigitalAsset.salsify_id) {
        const foundMatchingBufferDigitalAsset = _.find(buffer, {
          salsify_id: decoratedDigitalAsset.salsify_id,
        });

        if (!foundMatchingBufferDigitalAsset) {
          const salsifyDigitalAssetResult = await SalsifyDigitalAsset.create(
            decoratedDigitalAsset
          );
          // console.log(
          //   `Created new SalsifyDigitalAsset ${salsifyDigitalAssetResult.salsify_id}`
          // );
          return;
        } else {
          await foundMatchingBufferDigitalAsset.update(decoratedDigitalAsset);
          // console.log(
          //   `Updated SalsifyDigitalAsset ${decoratedDigitalAsset.salsify_id}, it already existed`
          // );
          return;
        }
      } else {
        console.log(
          `SalsifyDigitalAsset ${decoratedDigitalAsset} doesn't have a salsify_id.`
        );
      }
    } catch (error) {
      console.error(
        `Problems: ${error} for SalsifyDigitalAsset ${decoratedDigitalAsset.salsify_id}`
      );
    }
  },
};
