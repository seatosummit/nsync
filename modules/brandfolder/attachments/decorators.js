const _ = require("lodash");
require("dotenv").config();
const {
  extractSKU,
  extractEAN,
  valueExistsAndIsNotEmpty,
  hasMetadata,
  getFormattedDate,
  getAUImageStackFromFilename,
  getAdditionalOrderFromFilename,
} = require(".././../helpers/stringTools");

module.exports = {
  decorateAttachment: (brandfolderAttachment, WMSProductReference) => {
    let formattedUpdatedDate = null;
    let au_office_additional_ordering = null;
    if (hasMetadata(brandfolderAttachment)) {
      if (
        valueExistsAndIsNotEmpty(
          brandfolderAttachment.attributes.metadata.file_modify_date
        )
      ) {
        formattedUpdatedDate = getFormattedDate(
          brandfolderAttachment.attributes.metadata.file_modify_date
        );
      }
    }

    const filename = brandfolderAttachment.attributes.filename;
    const asset_id = brandfolderAttachment.relationships.asset.data.id;
    const section = brandfolderAttachment.relationships.section.data.id;

    const sku = extractSKU(filename);
    const ean = extractEAN(WMSProductReference, sku, filename);

    const AUOfficeImageStack = getAUImageStackFromFilename(filename);

    if (AUOfficeImageStack == "Additional") {
      au_office_additional_ordering = getAdditionalOrderFromFilename(
        brandfolderAttachment.attributes.filename
      );

      au_office_additional_ordering =
        parseInt(au_office_additional_ordering) - 1;
    }

    const primary = AUOfficeImageStack == "Primary";
    const packaging = AUOfficeImageStack == "Packaging";

    const decoratedAttachment = {
      attachment_id: brandfolderAttachment.id,
      filename,
      sku,
      ean,
      primary,
      packaging,
      asset_id,
      extension: brandfolderAttachment.attributes?.extension,
      url: brandfolderAttachment.attributes.url,
      position: brandfolderAttachment.attributes.position,
      bf_updated_date: formattedUpdatedDate,
      snapshot: brandfolderAttachment,
      au_office_additional_ordering,
      section,
    };
    return decoratedAttachment;
  },
  decorateAttachmentFromSavedAsset(
    upsertedAsset,
    matchingDecoratedAsset,
    attachment
  ) {
    const attachmentToUpsert = {
      bf_id: matchingDecoratedAsset.bf_id,
      bf_asset_id: matchingDecoratedAsset.bf_asset_id,
      bf_attachment_id: attachment ? attachment.id : "not set",
      filename: attachment ? attachment.filename : "not set",
      extension: attachment ? attachment.extension : "not set",
      url: attachment ? attachment.url : "not set",
      BrandfolderBufferAssetId: upsertedAsset.id,
      section_id: matchingDecoratedAsset.section_id,
    };
    return attachmentToUpsert;
  },
};
