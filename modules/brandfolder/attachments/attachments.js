const fetch = require("node-fetch");
const { promises: fsp } = require("fs");
const fs = require("fs");
const sharp = require("sharp");
const pathTools = require("path");
const _ = require("lodash");
require("dotenv").config();
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

const { BrandfolderBufferAttachment, WMSProduct } = require("../../../models");
const { prepareHeaders } = require("../../helpers/apitools");
const { decorateAttachment } = require("./decorators");
const { BrandfolderAPI: API } = require("../../helpers/brandfolderAPI");
const { SequelizeOperations } = require("../../sequelize/SequelizeOperations");
const { Logger } = require("../../helpers/Logger");
const { valueIsNullOrUndefined } = require("../../helpers/stringTools");

module.exports = {
  updateSingleAttachmentInBuffer: async (attachmentId, WMSBuffer) => {
    const BrandfolderAPI = new API();
    const foundAttachment = await BrandfolderAPI.getAttachmentWithId(
      attachmentId
    );
    if (foundAttachment) {
      const attachmentInBuffer = await BrandfolderBufferAttachment.findAll({
        where: {
          attachment_id: attachmentId,
        },
      });

      const WMSProductsLookup = WMSBuffer
        ? WMSBuffer
        : await WMSProduct.findAll({});
      const decoratedAttachment = decorateAttachment(
        foundAttachment.data,
        WMSProductsLookup
      );

      try {
        await SequelizeOperations.Upsert(
          decoratedAttachment,
          "attachment_id",
          BrandfolderBufferAttachment,
          attachmentInBuffer
        );
        Logger.info(`Updated BF Attachment ID ${attachmentId}`);
      } catch (error) {
        Logger.error(
          `Problem updating BF attachment_id ${attachmentId}: ${error}`
        );
      }
    }
  },

  updateAttachmentsBufferWithSections: async () => {
    const attachmentsBuffer = await BrandfolderBufferAttachment.findAll({
      where: {
        section: { [Op.eq]: null },
        new_asset_id: { [Op.eq]: null },
      },
    });
    const WMSBuffer = await WMSProduct.findAll({});
    for (const attachment of attachmentsBuffer) {
      await module.exports.updateSingleAttachmentInBuffer(
        attachment.attachment_id,
        WMSBuffer
      );
    }
  },

  updateAttachmentsBuffer: async (
    goBackDays = 30,
    brandfolderId = "jgs64snkkfgvxkvm454vsmrb"
  ) => {
    Logger.info(`Syncing BF Attachments buffer, going back ${goBackDays} days`);
    const BrandfolderAPI = new API();
    let attachmentsBuffer = await BrandfolderBufferAttachment.findAll({
      //raw: true,
    });
    const WMSProducts = await WMSProduct.findAll({
      //raw: true,
    });
    const activeWMSProducts = WMSProducts.filter((product) => {
      const status = product.productSnapshot.status;
      const brandIsSeatosummit = ["SOLUTION", "SEA TO SUMMIT"].includes(
        product.brand
      );
      return brandIsSeatosummit && status === "ACTIVE";
    });
    const attachmentsResponse =
      await BrandfolderAPI.getLatestAttachmentsFromBrandfolderWithPage(
        1,
        goBackDays,
        brandfolderId
      );
    await module.exports.processAttachmentsBatch(
      attachmentsResponse,
      activeWMSProducts,
      attachmentsBuffer
    );
    let nextPage = attachmentsResponse.meta.next_page;
    while (nextPage != null) {
      attachmentsBuffer = await BrandfolderBufferAttachment.findAll({
        //raw: true,
      });
      try {
        const attachmentsResponse =
          await BrandfolderAPI.getLatestAttachmentsFromBrandfolderWithPage(
            nextPage,
            goBackDays,
            brandfolderId
          );
        await module.exports.processAttachmentsBatch(
          attachmentsResponse,
          activeWMSProducts,
          attachmentsBuffer
        );
        nextPage = attachmentsResponse.meta.next_page;
      } catch (error) {
        console.error(
          `Error saving this batch of Brandfolder attachments: ${error}`
        );
      }
    }
    console.log(`... done updating Brandfolder Attachments buffer.`);
  },

  processAttachmentsBatch: async (
    attachmentsResponse,
    productLookupArray,
    attachmentsBuffer
  ) => {
    const arrayOfAttachments = attachmentsResponse?.data;
    if (valueIsNullOrUndefined(arrayOfAttachments))
      throw new Error(`This batch of attachments is empty?`);
    const decoratedAttachments = arrayOfAttachments.map(
      function (attachment) {
        return decorateAttachment(attachment, this.productLookupArray);
      },
      { productLookupArray }
    );
    let attachmentSavePromises = [];
    for (const decoratedAttachment of decoratedAttachments) {
      attachmentSavePromises.push(
        SequelizeOperations.Upsert(
          decoratedAttachment,
          "attachment_id",
          BrandfolderBufferAttachment,
          attachmentsBuffer
        )
      );
    }
    try {
      await Promise.all(attachmentSavePromises);
      console.log(
        `Updated page ${attachmentsResponse.meta.current_page}/${attachmentsResponse.meta.total_pages} of Brandfolder attachments to buffer.`
      );
    } catch (error) {
      console.error(`Error saving this batch of attachments: ${error}`);
    }
  },
};
