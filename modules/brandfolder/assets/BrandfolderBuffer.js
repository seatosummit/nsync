const fetch = require("node-fetch");
const _ = require("lodash");
const { Sequelize } = require("sequelize");
const Op = Sequelize.Op;
const { prepareHeaders } = require("../../helpers/apitools");
const { BrandfolderAPI: API } = require("../../helpers/brandfolderAPI");

const {
  SalsifyProduct,
  SalsifyProductParent,
  WMSProduct,
  BrandfolderBufferAsset,
} = require("../../../models");
const {
  valueExistsAndIsNotEmpty,
  skuIsZCoded,
} = require("../../helpers/stringTools");
const { Logger } = require("../../helpers/Logger");
const buffer = require("../labels/buffer");
const { ProgressIndicators } = require("../../helpers/ProgressIndicators");

/**
 * This class represents a BrandfolderBuffer instance.
 * It needs a ShopifyProduct and a ShopifyProductVariant blueprint
 */

class BrandfolderBuffer {
  constructor() {
    this.SalsifyBufferProductParent = SalsifyProductParent;
    this.SalsifyBufferProduct = SalsifyProduct;
    this.ERPBufferProduct = WMSProduct;
    this.BrandfolderBufferAsset = BrandfolderBufferAsset;
    this.updatePromises = [];
    this.BrandfolderAPI = new API();
    this.currentBufferAsset = {
      salsify_digital_asset_id: "",
      base_product_name: "",
      short_description: "",
      brandfolder_asset_id: "",
      brandfolder_asset_url: "",
      format: "",
      salsify_url: "",
      salsify_parent_skus: [],
      salsify_variant_skus: [],
      salsify_parent_eans: [],
      salsify_variant_eans: [],
      au_office_image_stack: "",
      us_office_image_stack: "",
      ger_office_image_stack: "",
      gender: "",
      base_product_name: "",
      colour: "",
      layman_colour: "",
      file_size_bytes: 0,
      original_width_px: 0,
      original_height_px: 0,
      filename: "",
      tags: [],
      salsify_digital_asset_snapshot: {},
    };
    this.collectionLookup = {
      "Black Diamond": "wfrz8z5w79kbqk6hj4pf5b97",
      "360 Degrees": "bz3v74rfthth3fcfh3bbvh7z",
      "Croakies": "856nsnbr39mwgk8wbh4r9gvk",
      "Trangia": "gsv4s2pj7wg2w7k3k2r6mh7",
      "Jetboil": "gfjphkhc986xwv3swg29x7",
      "SOTO": "fvwsxqhtk7xg6gnhvx6r35qq",
      "Grangers": "2kmg3hnksj9ww3mm6hpv9t46",
      "Silva": "8j5wb95j3f4wvhf8vtbmq86",
      "Axis": "8tc3xhk6c4f77czc9tq6n4",
      "Biolite": "6w7t5p3hqshg6435ghfhn85m",
      "humangear": "4f9s4hhvwt2csfgpmf77q3",
      "SealSkinz": "4jtbc98g3qhmkjrnn98ps5p",
      "Gerber": "w47zc6m7cjgjszh58vfp9r",
      "Vasque": "8n2b46v9w7574c4npmg78nx",
      "Aqua Bound": "nq7bsh46jvfz9tn87vvh6nbh",
      "Evolv": "bwbst9xrnq2pq9rw94rgkgz",
      "Back Country Cuisine": "qr4tt9x889mzpqnmfzppcnj",
      "Beal": "m99nwv337f5r5g93wmq4gq2g",
      "Helinox": "krqf53w473bgt7h9k3m6kf8",
      "Bridgedale": "79szwkwwfz9478rv6s6838r",
      "Sunday Afternoons": "74k7ffgqghpnqsp4tg5rvtx",
      "Pacsafe": "nnptfrmsvfqn9rqwnc3z493c",
      "CamelBak": "vfqthx6hz88qbvxc9t34pq",
      "Black Diamond": "wfrz8z5w79kbqk6hj4pf5b97",
      "Sea to Summit": "mq789ncqb7c6jv2cvhrrhq",
      "The Outdoor Gourmet Company": "6zvtsg23qfqccjvmpmh55qq",
      "Bob Cooper": "2st77n27xfs99x8899fkqfs",
      "Bending Branches": "9shzf9s799jv9nthpzskmwsb",
    };

    this.brandfolderSections = {
      // all product images have been moved to the below section.
      "Product Images": "ht7s95whgp3b7txkgf987nt",
    };

    this.customFieldLookup = [
      {
        id: "xzvbrwj56s5rmgpj33kfxcn",
        name: "AU Office Image stack",
        lookup_field: "au_office_image_stack",
      },
      {
        id: "s95w6jwcvh5gwgkhvkr6cp8s",
        name: "SKU",
        lookup_field: "salsify_variant_skus",
      },
      {
        id: "vc33s3r68nxc65fcbvp7b5x",
        name: "EAN",
        lookup_field: "salsify_variant_eans",
      },
      {
        id: "t5j5vj5rmqmvv64txt7b39m",
        name: "Base Product Name",
        lookup_field: "base_product_name",
      },
      {
        id: "mpp9gqrk9fs58fbsx8qpx4m9",
        name: "Colour",
        lookup_field: "colour",
      },
      {
        id: "mb8xcbwt3686h4m5b8xr2vmz",
        name: "Colour Layman",
        lookup_field: "layman_colour",
      },
      {
        id: "q3rhtx83386mvhg5mw6prgf",
        name: "Size",
        lookup_field: "size",
      },
      {
        id: "thmf7fsjw4hvngvmp35br52",
        name: "Parent SKU",
        lookup_field: "salsify_parent_skus",
      },
      {
        id: "9rkg773sntv94tvm44mttgx",
        name: "Gender",
        lookup_field: "gender",
      },
      {
        id: "mthxc7r7p8rrth4m6h7krzsz",
        name: "Brand",
        lookup_field: "salsify_brand",
      },
      {
        id: "87x46xr4f7tbfmtpb2wwgbm",
        name: "Additional Product Ordering",
        lookup_field: "au_office_additional_ordering",
      },
    ];

    this.headers;
  }

  async getAssetsForBufferUpdate(salsifyBrandFilter, erpBrandFilter) {
    const query = {};
    const erpQuery = {};
    if (salsifyBrandFilter) query.where = { brand: salsifyBrandFilter };
    if (erpBrandFilter) erpQuery.where = { brand: erpBrandFilter };
    this.headers = await prepareHeaders();
    this.salsifyBufferProductParents =
      await this.SalsifyBufferProductParent.findAll(query);
    this.salsifyBufferProducts = await this.SalsifyBufferProduct.findAll(query);
    this.ERPBufferProducts = await this.ERPBufferProduct.findAll(erpQuery);
    this.ERPBufferProducts = this.ERPBufferProducts.filter(
      (product) => !skuIsZCoded(product)
    );
    // wfrz8z5w79kbqk6hj4pf5b97 BD collection
  }

  async getBufferAssetsForBrandfolderSync(salsifyBrandFilter) {
    const query = {
      where: {
        format: { [Op.not]: null },
      },
    };
    if (salsifyBrandFilter) query.where.salsify_brand = salsifyBrandFilter;
    this.headers = await prepareHeaders();
    this.brandfolderBufferAssets = await this.BrandfolderBufferAsset.findAll(
      query
    );
  }

  convertToArray(eitherArrayOrString) {
    return [].concat(eitherArrayOrString);
  }

  async updateBufferEntries() {
    //this.salsifyBufferProducts = this.salsifyBufferProducts.slice(0, 10);
    const brandFilterArray = [];
    let allBufferEntries = [
      ...this.salsifyBufferProducts,
      ...this.salsifyBufferProductParents,
    ];
    if (brandFilterArray.length > 0) {
      allBufferEntries = allBufferEntries.filter(
        function (entry) {
          return this.brandFilterArray.includes(entry.brand);
        },
        { brandFilterArray }
      );
    }

    for (const currentBufferEntry of allBufferEntries) {
      const snapshot = currentBufferEntry.snapshot;
      const digitalAssets = snapshot["salsify:digital_assets"];
      if (digitalAssets) {
        for (const currentDigitalAsset of digitalAssets) {
          if (
            currentDigitalAsset["salsify:asset_resource_type"] == "hosted_video"
          ) {
            Logger.info(
              `Digital asset ${currentDigitalAsset} is a youtube video. Skipping.`
            );
            continue;
          }

          const digitalAssetId = currentDigitalAsset["salsify:id"];
          let au_office_additional_ordering;

          let auOfficeImageStack = this.getAUOfficeImageStackValue(
            currentBufferEntry,
            digitalAssetId
          );

          if (auOfficeImageStack === "Additional") {
            au_office_additional_ordering = this.getAdditionalImageOrdering(
              currentBufferEntry,
              digitalAssetId
            );
          }

          this.currentBufferAsset = this.createNewBufferAssetPayload(
            currentDigitalAsset,
            currentBufferEntry,
            auOfficeImageStack,
            au_office_additional_ordering
          );
          try {
            this.updatePromises.push(await this.bufferUpsertBrandfolderAsset());
          } catch (error) {
            Logger.error(`Error: ${error}`);
          }
        }
      } else {
        Logger.error(
          `Salsify item ${currentBufferEntry.sku} doesn't have any digital assets. Skipping.`
        );
        continue;
      }
    }

    try {
      const bufferSaves = await Promise.all(this.updatePromises);
      const actualUpdates = bufferSaves.filter((result) => result);
      Logger.info(
        `Created/Updated ${actualUpdates.length} out of ${bufferSaves.length} into buffer. Total of ${allBufferEntries.length} products/parents.`
      );
    } catch (error) {
      Logger.error(`Error saving to buffer: ${error}`);
    }
  }

  async syncBufferEntriesToBrandfolder() {
    //this.brandfolderBufferAssets = this.brandfolderBufferAssets.slice(0, 5);
    const brandFilterArray = ["Sea to Summit", "360 Degrees"];
    this.brandfolderBufferAssets = this.brandfolderBufferAssets.filter(
      function (entry) {
        return !this.includes(entry.salsify_brand);
      },
      brandFilterArray
    );
    for (const bufferAsset of this.brandfolderBufferAssets) {
      try {
        if (!bufferAsset.brandfolder_asset_id) {
          const brandfolderResult = await this.createNewAssetinBrandfolder(
            bufferAsset
          );
          const brandfolderResultData = brandfolderResult.new.data[0];
          const brandfolderAssetURL = await this.fetchBrandfolderAssetURL(
            brandfolderResultData,
            bufferAsset
          );
          Logger.info(
            `Created salsify ID ${bufferAsset.salsify_digital_asset_id} as an asset with id ${brandfolderResultData.id} in BF`
          );
          const bufferUpdate = await this.updateBufferWithBrandfolderInfo(
            brandfolderResultData,
            bufferAsset,
            brandfolderAssetURL
          );
          const tagUpdate = await this.updateTags(
            brandfolderResultData.id,
            bufferAsset
          );
        } else {
          // continue;
          Logger.info(
            `buffer asset with brandfolder ID ${bufferAsset.brandfolder_asset_id} already exists. Just update if needed.`
          );
          const tagUpdate = await this.updateTags(
            bufferAsset.brandfolder_asset_id,
            bufferAsset
          );
          const assetUpdate = await this.updateBrandfolderAsset(
            bufferAsset.brandfolder_asset_id,
            bufferAsset
          );
        }
      } catch (error) {
        Logger.info(error);
      }
    }
  }

  async cleanUpStaleBufferAssets() {
    //this.brandfolderBufferAssets = this.brandfolderBufferAssets.slice(0, 5);
    const brandFilterArray = ["Sea to Summit", "360 Degrees"];
    this.brandfolderBufferAssets = this.brandfolderBufferAssets.filter(
      function (entry) {
        return !this.includes(entry.salsify_brand);
      },
      brandFilterArray
    );
    let counter = 0;
    for (const bufferAsset of this.brandfolderBufferAssets) {
      try {
        if (!bufferAsset.brandfolder_asset_id) continue;

        const foundAssetInBrandfolder =
          await this.BrandfolderAPI.getBrandfolderAssetInfo(
            bufferAsset.brandfolder_asset_id
          );
        if (!foundAssetInBrandfolder) {
          await bufferAsset.destroy();
          Logger.info(
            `Removed asset ${bufferAsset.brandfolder_asset_id} from buffer, BF said 404.`
          );
        }
      } catch (error) {
        Logger.info(error);
      }
    }
  }

  async updateBrandfolderCustomFields() {
    this.updatePromises = [];

    // this has changed. Now, you need this:
    /**
   * {
    "data": [
        {
          "attributes": {
            "value": "11111111113"
          },
          "relationships": {
            "asset": {
              "data": { "type": "assets", "id": "6k76wn3qkv47nn8f88h6p5pz" }
            }
          }
        }
      ]

    }
      */

    // https://brandfolder.com/api/v4/custom_field_keys/qdlecj-4sgrfk-b5oe3z/custom_field_values/

    for (const customField of this.customFieldLookup) {
      const customFieldAssociations =
        this.consolidateCustomFieldValues(customField);
      try {
        await this.BrandfolderAPI.associateAssetsWithCustomField(
          customField.id,
          customFieldAssociations
        );
        Logger.info(
          `Associated ${customFieldAssociations.length} assets with custom field ${customField.name}`
        );
      } catch (error) {
        Logger.error(`Problem with custom field association`);
        Logger.error(error);
      }
    }
  }

  parseFieldValues(field) {
    if (Array.isArray(field)) {
      return field.join(",");
    } else {
      return field;
    }
  }

  async consolidateAndSaveTags() {
    let tagAssociations = {};
    let uniqueTagList = new Set();
    const onlyAssetsThatAreInBrandfolderAndHaveTags =
      this.brandfolderBufferAssets.filter((asset) => {
        return (
          valueExistsAndIsNotEmpty(asset.brandfolder_asset_id) &&
          valueExistsAndIsNotEmpty(asset.tags)
        );
      });
    for (const asset of onlyAssetsThatAreInBrandfolderAndHaveTags) {
      for (const tag of asset.tags) {
        uniqueTagList.add(tag);
      }
    }
    const uniqueTagsArray = Array.from(uniqueTagList);

    for (const tag of uniqueTagsArray) {
      tagAssociations[`${tag}`] = [];
      for (const asset of onlyAssetsThatAreInBrandfolderAndHaveTags) {
        if (asset.tags.includes(tag)) {
          tagAssociations[`${tag}`].push(asset.brandfolder_asset_id);
        }
      }
      try {
        await this.BrandfolderAPI.associateAssetsWithTags(
          tagAssociations[`${tag}`],
          tag
        );
      } catch (error) {
        Logger.error(error);
      }
    }
  }

  consolidateCustomFieldValues(customFieldDetails) {
    let customFieldAssociations = [];
    for (const bufferAsset of this.brandfolderBufferAssets) {
      if (!valueExistsAndIsNotEmpty(bufferAsset.brandfolder_asset_id)) continue;
      const fieldValue = bufferAsset[`${customFieldDetails.lookup_field}`];
      if (!valueExistsAndIsNotEmpty(fieldValue)) continue;
      const value = this.parseFieldValues(fieldValue);
      const payload = {
        attributes: {
          value,
        },
        relationships: {
          asset: {
            data: { type: "assets", id: bufferAsset.brandfolder_asset_id },
          },
        },
      };
      customFieldAssociations.push(payload);
    }
    return customFieldAssociations;
  }

  getAdditionalImageOrdering(currentBufferEntry, currentDigitalAssetId) {
    const snapshot = currentBufferEntry.snapshot;
    const additionalIDs = snapshot["Additional Image"]
      ? [].concat(snapshot["Additional Image"])
      : [];
    const additionalImageOrdering = additionalIDs.indexOf(
      currentDigitalAssetId
    );
    if (additionalImageOrdering >= 0) {
      const orderString =
        additionalImageOrdering < 11
          ? `0${additionalImageOrdering + 1}`
          : `${additionalImageOrdering + 1}`;
      return orderString;
    }
    return null;
  }

  getAUOfficeImageStackValue(currentBufferEntry, currentDigitalAssetId) {
    const snapshot = currentBufferEntry.snapshot;
    let auOfficeImageStack = "Other";
    const primaryIDs = snapshot["Primary Image"]
      ? [].concat(snapshot["Primary Image"])
      : [];
    const additionalIDs = snapshot["Additional Image"]
      ? [].concat(snapshot["Additional Image"])
      : [];
    const packagingIDs = snapshot["Packaging Images"]
      ? [].concat(snapshot["Packaging Images"])
      : [];
    if (primaryIDs.includes(currentDigitalAssetId))
      auOfficeImageStack = "Primary";
    if (additionalIDs.includes(currentDigitalAssetId))
      auOfficeImageStack = "Additional";
    if (packagingIDs.includes(currentDigitalAssetId))
      auOfficeImageStack = "Packaging";
    return auOfficeImageStack;
  }

  async fetchBrandfolderAssetURL(brandfolderResultData, productInfo) {
    const assetID = brandfolderResultData.id;
    try {
      const assetInfo = await this.BrandfolderAPI.getBrandfolderAssetInfo(
        assetID,
        productInfo
      );
      const assetURL = assetInfo.included[0].attributes.url;
      return assetURL;
    } catch (error) {
      // Logger.info(error);
      return "URL not retrieved";
    }
  }

  createNewBufferAssetPayload(
    currentDigitalAsset,
    currentBufferEntry,
    auOfficeImageStack,
    au_office_additional_ordering
  ) {
    const snapshot = currentBufferEntry.snapshot;
    const isParent =
      currentBufferEntry instanceof SalsifyProductParent ||
      (currentBufferEntry instanceof SalsifyProduct &&
        !currentBufferEntry.parent_sku);
    let salsify_parent_skus = [];
    let salsify_variant_skus = [];
    let salsify_parent_eans = [];
    let salsify_variant_eans = [];
    if (isParent) {
      salsify_parent_skus = [currentBufferEntry.sku];
      salsify_parent_eans = snapshot["BARCODE/EAN"]
        ? [snapshot["BARCODE/EAN"]]
        : [];
    } else {
      salsify_parent_skus = currentBufferEntry.parent_sku
        ? [currentBufferEntry.parent_sku]
        : [];
      salsify_variant_skus = [currentBufferEntry.sku];
      salsify_variant_eans = snapshot["BARCODE/EAN"]
        ? [snapshot["BARCODE/EAN"]]
        : [];
    }

    let tags = [];
    tags.push(auOfficeImageStack);
    tags.push("Product Image");
    tags.push(currentBufferEntry.sku);
    tags.push(...salsify_variant_eans);

    const optionalTags = [
      "parent_sku",
      "brand_category",
      "brand_sub_category",
      "erp_tier1_financial_category",
      "erp_category",
      "erp_sub_category",
      "b2b_sub_category",
    ];

    for (const optionalTag of optionalTags) {
      if (currentBufferEntry[`${optionalTag}`])
        tags.push(currentBufferEntry[`${optionalTag}`]);
    }

    const brandfolder_collection_id =
      this.collectionLookup[`${snapshot["BRAND"]}`];

    //const base_product_name = snapshot["BASE PRODUCT NAME"] ? snapshot["BASE PRODUCT NAME"] : snapshot["FULL PRODUCT NAME (incl VARIANT)"];
    const base_product_name = currentBufferEntry.name;

    const short_description = snapshot["SHORT DESCRIPTION"]
      ? snapshot["SHORT DESCRIPTION"]
      : currentBufferEntry.snapshot["FULL PRODUCT NAME (incl VARIANT)"];

    let customFieldsUpdatePayload = {
      salsify_digital_asset_id: currentDigitalAsset["salsify:id"],
      salsify_brand: snapshot["BRAND"],
      base_product_name,
      short_description,
      brandfolder_asset_id: "",
      brandfolder_asset_url: "",
      brandfolder_collection_id,
      format: currentDigitalAsset["salsify:format"],
      salsify_url: currentDigitalAsset["salsify:url"],
      salsify_parent_skus,
      salsify_variant_skus,
      salsify_parent_eans,
      salsify_variant_eans,
      au_office_image_stack: auOfficeImageStack,
      au_office_additional_ordering,
      us_office_image_stack: "",
      ger_office_image_stack: "",
      gender: currentBufferEntry.snapshot["Gender"],
      colour: "",
      layman_colour: "",
      size: "",
      file_size_bytes: currentDigitalAsset["salsify:bytes"],
      original_width_px: currentDigitalAsset["salsify:asset_width"],
      original_height_px: currentDigitalAsset["salsify:asset_height"],
      filename: currentDigitalAsset["salsify:filename"],
      tags,
      salsify_digital_asset_snapshot: currentDigitalAsset,
    };

    if (currentBufferEntry.snapshot["SIZE VARIANT"]) {
      customFieldsUpdatePayload.size =
        currentBufferEntry.snapshot["SIZE VARIANT"];
      customFieldsUpdatePayload.tags.push(
        currentBufferEntry.snapshot["SIZE VARIANT"]
      );
    }

    if (currentBufferEntry.snapshot["COLOUR VARIANT"]) {
      customFieldsUpdatePayload.colour =
        currentBufferEntry.snapshot["COLOUR VARIANT"];
      customFieldsUpdatePayload.tags.push(
        currentBufferEntry.snapshot["COLOUR VARIANT"]
      );
    }
    if (currentBufferEntry.snapshot["COLOUR LAYMAN (Generic Colour Group)"]) {
      customFieldsUpdatePayload.layman_colour =
        currentBufferEntry.snapshot["COLOUR LAYMAN (Generic Colour Group)"];
      customFieldsUpdatePayload.tags.push(
        currentBufferEntry.snapshot["COLOUR LAYMAN (Generic Colour Group)"]
      );
    }

    return customFieldsUpdatePayload;
  }

  async bufferUpsertBrandfolderAsset() {
    if (!this.currentBufferAsset.filename) {
      throw `Digital asset with id ${this.currentBufferAsset.salsify_digital_asset_id} has no filename. How's that even possible?`;
    }
    const foundExisting = await this.BrandfolderBufferAsset.findOne({
      where: {
        [Op.or]: [
          {
            salsify_digital_asset_id:
              this.currentBufferAsset.salsify_digital_asset_id,
          },
          { filename: this.currentBufferAsset.filename },
        ],
      },
    });

    if (foundExisting) {
      this.currentBufferAsset.brandfolder_asset_id =
        foundExisting.brandfolder_asset_id;
      this.currentBufferAsset.brandfolder_asset_url =
        foundExisting.brandfolder_asset_url;
      const attributesToUnify = [
        "salsify_parent_skus",
        "salsify_variant_skus",
        "salsify_variant_eans",
        "tags",
        "au_office_additional_ordering",
      ];
      for (const attribute of attributesToUnify) {
        this.currentBufferAsset[attribute] = this.mergeOldAndNewValues(
          foundExisting,
          this.currentBufferAsset,
          attribute
        );
      }

      foundExisting.set(this.currentBufferAsset);
      if (foundExisting.changed().length > 0) {
        return await foundExisting.save();
      } else {
        return false;
      }
    } else {
      const result = await this.BrandfolderBufferAsset.create(
        this.currentBufferAsset
      );
      return result;
    }
  }

  async updateBufferWithBrandfolderInfo(
    brandfolderResultData,
    bufferEntry,
    brandfolderAssetURL
  ) {
    const foundExisting = await this.BrandfolderBufferAsset.findOne({
      where: {
        salsify_digital_asset_id: bufferEntry.salsify_digital_asset_id,
      },
    });

    if (foundExisting) {
      const result = await foundExisting.update({
        brandfolder_asset_id: brandfolderResultData.id,
        brandfolder_asset_url: brandfolderAssetURL,
      });
    } else {
      Logger.info("Did not find this buffer asset. Why?");
    }
  }

  mergeOldAndNewValues(brandfolderAssetBufferEntry, newBufferAsset, attribute) {
    const existingBufferAttribute = JSON.parse(
      JSON.stringify(brandfolderAssetBufferEntry[attribute])
    );
    const mergedAttributes = _.union(
      existingBufferAttribute,
      newBufferAsset[attribute]
    );
    return mergedAttributes;
  }

  async createNewAssetinBrandfolder(brandfolderBufferAsset) {
    // const section_key =
    //   this.brandfolderSections[
    //     `${brandfolderBufferAsset.au_office_image_stack}`
    //   ];
    const section_key = this.brandfolderSections["Product Images"];
    const collectionID = brandfolderBufferAsset.brandfolder_collection_id;
    const assetCreationPayload = {
      data: {
        attributes: [
          {
            name: brandfolderBufferAsset.base_product_name,
            description: brandfolderBufferAsset.short_description,
            attachments: [
              {
                url: brandfolderBufferAsset.salsify_url,
                filename: brandfolderBufferAsset.filename,
              },
            ],
          },
        ],
      },
      section_key,
    };

    const responseJSON =
      await this.BrandfolderAPI.createNewBrandfolderAssetInCollection(
        collectionID,
        assetCreationPayload
      );
    return responseJSON;
  }

  async updateTags(brandfolderAssetID, currentBufferEntry) {
    const result = await this.BrandfolderAPI.addTagsToOneAsset(
      brandfolderAssetID,
      currentBufferEntry.tags
    );
  }

  async updateBrandfolderAsset(brandfolderAssetID, currentBufferEntry) {
    const attributes = {
      name: currentBufferEntry.base_product_name
        ? currentBufferEntry.base_product_name
        : "No name set",
      description: currentBufferEntry.short_description
        ? currentBufferEntry.short_description
        : "",
    };
    const tagUpdatePayload = {
      data: {
        attributes,
      },
    };
    const result = await this.BrandfolderAPI.updateBrandfolderAsset(
      brandfolderAssetID,
      tagUpdatePayload,
      currentBufferEntry
    );
  }

  async getCustomFieldsAssociations() {
    await this.refreshAttachmentsBuffer({ onlyNew: true });
    const customFieldAssociations = {};
    const attachmentsWithCustomFields =
      this.brandfolderBufferAttachments.filter((attachment) => {
        return valueExistsAndIsNotEmpty(attachment.custom_fields);
      });
    for (const attachment of attachmentsWithCustomFields) {
      const customFields = attachment.custom_fields;

      for (const customField of customFields) {
        const associationEntry = {
          attributes: {
            value: customField.value,
          },
          relationships: {
            asset: {
              data: {
                type: "asset",
                id: attachment.new_asset_id,
              },
            },
          },
        };
        if (
          valueExistsAndIsNotEmpty(customFieldAssociations[`${customField.id}`])
        ) {
          customFieldAssociations[`${customField.id}`].data.push(
            associationEntry
          );
        } else {
          customFieldAssociations[`${customField.id}`] = {
            name: `${customField.name}`,
            data: [associationEntry],
          };
        }
      }
    }
    return customFieldAssociations;
  }

  async updateCustomFields(brandfolderAssetID, currentBufferEntry) {
    let customFieldsUpdatePayload = {
      data: {
        attributes: [
          {
            key: "SKU",
            value: currentBufferEntry.salsify_parent_skus.toString(),
          },
          {
            key: "Gender",
            value: currentBufferEntry.gender,
          },
          {
            key: "AU Office Image stack",
            value: currentBufferEntry.au_office_image_stack,
          },
        ],
      },
    };

    // if (currentBufferEntry.snapshot["SIZE VARIANT"]) {
    //   customFieldsUpdatePayload.data.attributes.push({ key: "Size", value: currentBufferEntry.snapshot["SIZE VARIANT"] });
    // }

    // if (currentBufferEntry.snapshot["BARCODE/EAN"]) {
    //   customFieldsUpdatePayload.data.attributes.push({ key: "Size", value: currentBufferEntry.snapshot["SIZE VARIANT"] });
    // }

    // if (currentBufferEntry.snapshot["COLOUR VARIANT"]) {
    //   customFieldsUpdatePayload.data.attributes.push({ key: "Colour", value: currentBufferEntry.snapshot["COLOUR VARIANT"] });
    // }

    if (currentBufferEntry.colour) {
      customFieldsUpdatePayload.data.attributes.push({
        key: "Colour",
        value: currentBufferEntry.colour,
      });
    }

    if (currentBufferEntry.salsify_parent_skus.length > 0) {
      customFieldsUpdatePayload.data.attributes.push({
        key: "Parent SKU",
        value: currentBufferEntry.salsify_parent_skus.toString(),
      });
    }

    if (currentBufferEntry.salsify_variant_skus.length > 0) {
      customFieldsUpdatePayload.data.attributes.push({
        key: "SKU",
        value: currentBufferEntry.salsify_variant_skus.toString(),
      });
    }

    if (currentBufferEntry.colour_layman) {
      customFieldsUpdatePayload.data.attributes.push({
        key: "Colour Layman",
        value: currentBufferEntry.colour_layman,
      });
    }

    if (currentBufferEntry.salsify_brand) {
      customFieldsUpdatePayload.data.attributes.push({
        key: "Brand",
        value: currentBufferEntry.salsify_brand,
      });
    }

    if (currentBufferEntry.base_product_name) {
      customFieldsUpdatePayload.data.attributes.push({
        key: "Base Product Name",
        value: base_product_name,
      });
    }

    try {
      const url = `https://brandfolder.com/api/v4/assets/${brandfolderAssetID}/custom_fields`;
      const response = await fetch(url, {
        method: "POST",
        body: JSON.stringify(customFieldsUpdatePayload),
        headers: this.headers.BrandfolderHeaders,
        timeout: 0,
      });
      if (response.status !== 200) {
        throw `response other than 200 - ${response.status}: ${response.statusText}`;
      }
      const responseJSON = await response.json();
      return responseJSON;
    } catch (error) {
      Logger.error(`Error fetching assets from Brandfolder: ${error}`);
    }
  }
}

module.exports.BrandfolderBuffer = BrandfolderBuffer;
