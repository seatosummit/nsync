const _ = require("lodash");
require("dotenv").config();
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");
const moment = require("moment");
const { stringToArray } = require("../../helpers/arrayTools");
const { Logger } = require("../../helpers/Logger");

module.exports = {
  decorateAsset: (brandfolderAsset, assetIncludesArray, brandfolderId) => {
    const attributes = brandfolderAsset.attributes;
    const description = attributes.description;
    const name = attributes?.name;
    const metadata = attributes.metadata[0];
    const bf_id = brandfolderId;
    const bf_asset_id = brandfolderAsset.id;
    const relationships = brandfolderAsset.relationships;
    const section_id = relationships.section.data.id;
    const attachments = module.exports
      .extractValues(relationships, assetIncludesArray, "attachments")
      .filter((attachment) => {
        const properFileType = ["jpg", "jpeg", "png", "tiff", "gif"].includes(
          attachment.extension
        );
        if (!properFileType) {
          Logger.error(
            `Brandfolder Attachment ID ${attachment.id} has the wrong format (.${attachment.extension})`
          );
          return false;
        }
        return true;
      })
      .filter(
        (attachment, index, array) => attachment.position === array.length - 1
      );
    if (attachments.length === 0) {
      Logger.error(
        `Brandfolder Asset ${name} ID ${bf_asset_id} had no attachments in the correct format`
      );
    }

    const collections = module.exports.extractValues(
      relationships,
      assetIncludesArray,
      "collections"
    );
    const labels = module.exports.extractValues(
      relationships,
      assetIncludesArray,
      "labels"
    );
    const tags = module.exports.extractValues(
      relationships,
      assetIncludesArray,
      "tags"
    );
    const custom_fields = module.exports.extractValues(
      relationships,
      assetIncludesArray,
      "custom_field_values"
    );

    const au_skus = stringToArray(
      module.exports.customFieldValue("SKU", custom_fields)
    );
    const parent_skus = stringToArray(
      module.exports.customFieldValue("Parent SKU", custom_fields)
    );
    const brand = module.exports.customFieldValue("Brand", custom_fields);
    const au_office_image_stack = module.exports.customFieldValue(
      "AU Office Image Stack",
      custom_fields
    );
    const gender = module.exports.customFieldValue("Gender", custom_fields);
    const size = module.exports.customFieldValue("Size", custom_fields);
    const eans = stringToArray(
      module.exports.customFieldValue("EAN", custom_fields)
    );
    const au_office_image_ordering = module.exports.customFieldValue(
      "AU Office Image Stack Additional ordering",
      custom_fields
    );
    const type = module.exports.getType(attachments);

    const snapshot = {
      bf_id,
      bf_asset_id,
      gender,
      attributes,
      attachments,
      collections,
      size,
      au_skus,
      au_office_image_stack,
      au_office_image_ordering,
      parent_skus,
      brand,
      custom_fields,
      tags,
      labels,
      metadata,
      section_id,
    };

    const decoratedAsset = {
      name,
      type,
      description,
      bf_asset_id,
      bf_id,
      brand,
      collections,
      custom_fields,
      tags,
      labels,
      au_skus,
      au_office_image_stack,
      au_office_image_ordering,
      eans,
      parent_skus,
      section_id,
      snapshot,
    };
    return decoratedAsset;
  },
  extractValues: (relationships, includesArray, type) => {
    const relevantIncludes = includesArray.filter(
      (include) => include.type === type
    );
    const relationshipsArray = relationships[`${type}`].data;
    let includeValues = [];
    for (const relationship of relationshipsArray) {
      const matchingInclude = _.find(relevantIncludes, ["id", relationship.id]);
      if (!matchingInclude) continue;
      const matchingIncludeEntry = {
        id: matchingInclude.id,
      };
      switch (type) {
        case "custom_field_values":
          matchingIncludeEntry.key = matchingInclude.attributes.key;
          matchingIncludeEntry.value = matchingInclude.attributes.value;
          break;
        case "tags":
          matchingIncludeEntry.name = matchingInclude.attributes.name;
          break;
        case "collections":
          matchingIncludeEntry.name = matchingInclude.attributes.name;
          matchingIncludeEntry.slug = matchingInclude.attributes.slug;
          break;
        case "attachments":
          matchingIncludeEntry.id = matchingInclude.id;
          matchingIncludeEntry.filename = matchingInclude.attributes?.filename;
          matchingIncludeEntry.extension =
            matchingInclude?.attributes?.extension;
          matchingIncludeEntry.url = matchingInclude.attributes?.url;
          matchingIncludeEntry.position = matchingInclude.attributes?.position;
          break;
        case "sections":
          matchingIncludeEntry.name = matchingInclude.attributes.name;
          break;
        default:
          break;
      }
      includeValues.push(matchingIncludeEntry);
    }
    return includeValues;
  },
  customFieldValue: (customFieldKey, entriesArray) => {
    const customFieldEntry = _.find(entriesArray, ["key", customFieldKey]);
    return customFieldEntry?.value ?? null;
  },
  getType: (attachments) => {
    const firstAttachment = attachments[0];
    return firstAttachment?.extension;
  },
};
