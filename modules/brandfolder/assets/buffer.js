const { BrandfolderBufferAsset } = require("../../../models");
module.exports = {
  saveToAssetsBuffer: async (asset, buffer) => {
    try {
      const foundMatchingBufferAsset = await BrandfolderBufferAsset.findOne({
        where: {
          asset_id: asset.asset_id,
        },
      });
      if (!foundMatchingBufferAsset) {
        return BrandfolderBufferAsset.create(asset);
      } else {
        return foundMatchingBufferAsset.update(asset);
      }
    } catch (error) {
      console.error(`Problems: ${error} for asset ${asset.asset_id}`);
    }
  },
};
