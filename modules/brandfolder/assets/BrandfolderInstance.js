const _ = require("lodash");
const slugify = require("slugify");

const {
  SalsifyProduct,
  SalsifyProductParent,
  WMSProduct,
} = require("../../../models");

class BrandfolderInstance {
  constructor(
    BrandfolderBuffer
  ) {
    this.BrandfolderBuffer = BrandfolderBuffer;
    this.SalsifyProduct = SalsifyProduct;
    this.SalsifyProductParent = SalsifyProductParent;
    this.WMSProduct = WMSProduct;
  }

  async getSalsifyProducts() {
    this.salsifyBufferProductParents =
      await this.SalsifyBufferProductParent.findAll({});
    this.salsifyBufferProducts = await this.SalsifyBufferProduct.findAll({});
    this.ERPBufferProducts = await this.ERPBufferProduct.findAll({});
  }

}

module.exports.BrandfolderInstance = BrandfolderInstance;
