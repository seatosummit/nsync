const fetch = require("node-fetch");
const { promises: fsp } = require("fs");
const fs = require("fs");
const sharp = require("sharp");
const pathTools = require("path");
const _ = require("lodash");
require("dotenv").config();
const {
  BrandfolderBufferAsset,
  BrandfolderBufferAttachment,
} = require("../../../models");
const { prepareHeaders } = require("../../helpers/apitools");
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");
const { decorateAsset } = require("./decorators");
const { saveToAssetsBuffer } = require("./buffer");
const { BrandfolderAPI } = require("../../helpers/brandfolderAPI");
const cliProgress = require("cli-progress");
const { SequelizeOperations } = require("../../sequelize/SequelizeOperations");
const { ProgressIndicators } = require("../../helpers/ProgressIndicators");
const { Logger } = require("../../helpers/Logger");
const {
  decorateAttachmentFromSavedAsset,
} = require("../attachments/decorators");
const progressbar = new cliProgress.SingleBar(
  {},
  cliProgress.Presets.shades_classic
);

module.exports = {
  updateAssetsBuffer: async (
    goBackDays = 30,
    brandfolderId = "q7cphz-8wbqqw-1dru9z"
  ) => {
    Logger.info(`Syncing BF Assets buffer, going back ${goBackDays} days`);
    try {
      const API = new BrandfolderAPI();
      let assetsBuffer = await BrandfolderBufferAsset.findAll({});
      const assetsResponse = await await API.getLatestAssetsWithPage(
        1,
        200,
        goBackDays,
        brandfolderId
      );
      const stats = {
        current: 0,
        total: assetsResponse.meta.total_pages,
        increase: 1,
      };
      await module.exports.processAssetsBatch(
        assetsResponse,
        assetsBuffer,
        stats,
        brandfolderId
      );
      let nextPage = assetsResponse.meta.next_page;
      while (nextPage != null) {
        assetsBuffer = await BrandfolderBufferAsset.findAll({});
        const assetsResponse = await API.getLatestAssetsWithPage(
          nextPage,
          200,
          goBackDays,
          brandfolderId
        );
        await module.exports.processAssetsBatch(
          assetsResponse,
          assetsBuffer,
          stats,
          brandfolderId
        );

        nextPage = assetsResponse.meta.next_page;
      }
    } catch (error) {
      Logger.error(`Error saving this batch of Brandfolder assets: ${error}`);
    }
    Logger.info("...done updating Assets buffer");
  },
  processAssetsBatch: async (assetsResponse, buffer, stats, brandfolderId) => {
    const arrayOfAssets = assetsResponse.data;
    const arrayOfAssetIncludes = assetsResponse.included;
    const sectionIdsArray = [
      "ht7s95whgp3b7txkgf987nt",
      "gxssj9rpsnn5gcbm39j7kn",
      "q7cphz-8wbqqw-c4dg2u",
      "tb6mwcswp9ppx3thgqpk336",
      "frxb996b64bb7prqgg38gns5",
      "f37sc7fbnsbh3x7578b9rgp",
    ];
    const decoratedAssets = arrayOfAssets
      .map((asset) => decorateAsset(asset, arrayOfAssetIncludes, brandfolderId))
      .filter((asset) => {
        return sectionIdsArray.includes(asset.section_id);
      });
    let savePromises = [];
    for (const decoratedAsset of decoratedAssets) {
      savePromises.push(
        SequelizeOperations.Upsert(
          decoratedAsset,
          "bf_asset_id",
          BrandfolderBufferAsset,
          buffer
        )
      );
    }
    try {
      const assetSaveResults = await Promise.all(savePromises);
      savePromises = [];
      const attachmentsBuffer = await BrandfolderBufferAttachment.findAll({});
      for (const upsertedAsset of assetSaveResults) {
        const matchingDecoratedAsset = _.find(decoratedAssets, {
          bf_asset_id: upsertedAsset.bf_asset_id,
        });
        if (!matchingDecoratedAsset) continue;
        for (const attachment of matchingDecoratedAsset.snapshot.attachments) {
          const attachmentToUpsert = decorateAttachmentFromSavedAsset(
            upsertedAsset,
            matchingDecoratedAsset,
            attachment
          );
          savePromises.push(
            SequelizeOperations.Upsert(
              attachmentToUpsert,
              "bf_attachment_id",
              BrandfolderBufferAttachment,
              attachmentsBuffer
            )
          );
        }
      }
      const attachmentSaveResults = await Promise.all(savePromises);
      ProgressIndicators.showProgress(stats);
    } catch (error) {
      console.error(
        `Error saving this batch of assets and attachments: ${error}`
      );
    }
  },
};
