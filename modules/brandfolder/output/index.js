const { Logger } = require("../../helpers/Logger");
const { BrandfolderOutput } = require("./BrandfolderOutput");

const BrandfolderAttachmentsToDigitalAssets = async (brand) => {
  try {
    Logger.info("Creating Salsify digital assets");
    const converter = new BrandfolderOutput();
    await converter.attachmentsIntoSalsifyDigitalAssets(brand);
    console.log("... done creating Salsify digital assets");
  } catch (error) {
    console.error(`Error creating Salsify digital assets: ${error}`);
  }
};
const BrandfolderAssetsToDigitalAssets = async (brand) => {
  try {
    Logger.info("Creating Salsify digital assets");
    const converter = new BrandfolderOutput();
    await converter.assetsIntoSalsifyDigitalAssets(brand);
    console.log("... done creating Salsify digital assets");
  } catch (error) {
    console.error(`Error creating Salsify digital assets: ${error}`);
  }
};

const BrandfolderAssetsSalsifyProductAssociations = async (brand) => {
  try {
    Logger.info("Associating Salsify digital assets with products");
    const converter = new BrandfolderOutput();
    await converter.associateAssetsWithProducts(brand);
    console.log("... done associating Salsify digital assets with products");
  } catch (error) {
    console.error(
      `Error Associating Salsify digital assets with products: ${error}`
    );
  }
};

const BrandfolderAttachmentsToSalsifyProductImages = async (brand) => {
  try {
    Logger.info("Associating BF attachments with Salsify products");
    const converter = new BrandfolderOutput();
    await converter.associateAttachmentsWithProducts();
    console.log("... done.");
  } catch (error) {
    console.error(
      `Error associating BF attachments with Salsify products: ${error}`
    );
  }
};

const BrandfolderSkuAndUrlList = async (brand) => {
  try {
    Logger.info("Creating CSV of skus and urls");
    const converter = new BrandfolderOutput();
    await converter.createSkuAndUrlList();
    console.log("... done.");
  } catch (error) {
    console.error(`Error producing CSV: ${error}`);
  }
};

module.exports.BrandfolderAttachmentsToDigitalAssets =
  BrandfolderAttachmentsToDigitalAssets;
module.exports.BrandfolderAttachmentsToSalsifyProductImages =
  BrandfolderAttachmentsToSalsifyProductImages;
module.exports.BrandfolderAssetsToDigitalAssets =
  BrandfolderAssetsToDigitalAssets;
module.exports.BrandfolderAssetsSalsifyProductAssociations =
  BrandfolderAssetsSalsifyProductAssociations;
module.exports.BrandfolderSkuAndUrlList = BrandfolderSkuAndUrlList;
