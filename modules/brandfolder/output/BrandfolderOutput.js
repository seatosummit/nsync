const _ = require("lodash");
const { Sequelize, QueryTypes } = require("sequelize");
const Op = Sequelize.Op;
const { v4: uuidV4 } = require("uuid");
const { BrandfolderAPI } = require("../../helpers/brandfolderAPI");
const {
  valueIsNullOrUndefined,
  valueExistsAndIsNotEmpty,
} = require("../../helpers/stringTools");

const cliProgress = require("cli-progress");

const {
  SalsifyProduct,
  SalsifyProductParent,
  SalsifyDigitalAsset,
  BrandfolderBufferAsset,
  BrandfolderBufferAttachment,
} = require("../../../models");
const { Logger } = require("../../helpers/Logger");
const { SalsifyAPI } = require("../../salsify/helpers/SalsifyAPI");
const { ShopifyAPI } = require("../../helpers/apitools");
const { SequelizeOperations } = require("../../sequelize/SequelizeOperations");
const { uniqueId, chunk } = require("lodash");
const { getCompatibleAttachment } = require("../../helpers/BrandfolderTools");
const { createSkuAndUrlList } = require("../../helpers/fileTools");

class BrandfolderOutput {
  constructor() {
    this.SalsifyBufferProductParent = SalsifyProductParent;
    this.SalsifyBufferProduct = SalsifyProduct;
    this.BrandfolderBufferAsset = BrandfolderBufferAsset;
    this.BrandfolderBufferAttachment = BrandfolderBufferAttachment;
    this.updatePromises = [];
    this.newAssets = [];
    this.SalsifyAPI = new SalsifyAPI();
    this.BrandfolderAPI = new BrandfolderAPI();
    this.eansNotFoundInGlobalSalsify = [];
    this.salsifyBufferProductParents = [];
    this.salsifyBufferProducts = [];
    this.salsifyGlobalBufferProducts = [];
    this.brandfolderBufferAttachmentsWithEan = [];
    this.brandfolderBufferAttachments = [];
    this.brandfolderBufferAssets = [];
    this.brandfolderPrimaryAssets = [];
    this.brandfolderAdditionalAssets = [];
    this.brandfolderPackagingAssets = [];
    this.progressbar = new cliProgress.SingleBar(
      {},
      cliProgress.Presets.shades_classic
    );

    this.brandfolderSectionIDs = Object.values({
      "Product Images": "q7cphz-8wbqqw-c4dg2u",
      "360 Product Images": "tb6mwcswp9ppx3thgqpk336",
      "Dist Product Images": "ht7s95whgp3b7txkgf987nt", // there only is one section for DIST now
    });

    this.headers;
    this.skusAndUrls = [];
  }

  async getAssetsForSalsifyUpdate({ brand, salsifyDAExists = false }) {
    const query = {
      where: {
        bf_processed: true,
      },
      include: BrandfolderBufferAttachment,
    };
    if (brand) query.where.brand = brand;
    this.brandfolderBufferAssets = await BrandfolderBufferAsset.findAll(query);
    this.brandfolderBufferAssets = this.brandfolderBufferAssets.filter(
      (asset) =>
        ["Primary", "Additional", "Packaging"].includes(
          asset.au_office_image_stack
        )
    );
    this.brandfolderBufferAssets = this.brandfolderBufferAssets.filter(
      (asset) => this.brandfolderSectionIDs.includes(asset.section_id)
    );
    this.brandfolderBufferAssets = this.brandfolderBufferAssets.filter(
      (asset) =>
        salsifyDAExists
          ? asset.au_salsify_digital_asset_id
          : !asset.au_salsify_digital_asset_id
    );
  }

  async assetsIntoSalsifyDigitalAssets(brand) {
    await this.getAssetsForSalsifyUpdate({ brand, salsifyDAExists: false });
    const howManyInOneGo = 100;
    let counter = 0;
    const assetChunks = _.chunk(this.brandfolderBufferAssets, howManyInOneGo);
    for (const chunkOfAssets of assetChunks) {
      const arrayOfDigitalAssets = [];
      const arrayOfBufferUpdatePromises = [];
      for (const currentAsset of chunkOfAssets) {
        const compatibleAttachment = getCompatibleAttachment(
          currentAsset.BrandfolderBufferAttachments
        );
        if (!compatibleAttachment) {
          Logger.error(
            `There's no matching attachment for asset ${currentAsset.bf_asset_id}. Check if it's a PSD?`
          );
          continue;
        }
        const uniqueId = uuidV4();
        arrayOfDigitalAssets.push({
          "salsify:id": uniqueId,
          "salsify:url": compatibleAttachment.url,
          "salsify:name": compatibleAttachment.filename,
        });
        currentAsset.au_salsify_digital_asset_id = uniqueId;
        arrayOfBufferUpdatePromises.push(currentAsset.save());
      }
      try {
        const salsifyCreationResult =
          await this.SalsifyAPI.createManyDigitalAssets(arrayOfDigitalAssets);
        const bufferUpdateResults = await Promise.all(
          arrayOfBufferUpdatePromises
        );
        counter += howManyInOneGo;
        Logger.info(
          `Saved batch ${counter} out of ${
            assetChunks.length * howManyInOneGo
          } digital assets.`
        );
      } catch (error) {
        Logger.error(`Problem with batch of digital assets: ${error}`);
      }
    }
  }

  createSKUGroupingForBulkUpdate() {
    const groupedSKUs = {};
    for (const asset of this.brandfolderBufferAssets) {
      for (const dirtySKU of asset.au_skus) {
        const sku = dirtySKU.trim(); // todo: other SKU cleanup actions
        const skuEntryExists = valueExistsAndIsNotEmpty(groupedSKUs[`${sku}`]);
        if (!skuEntryExists) {
          groupedSKUs[`${sku}`] = {
            primary: null,
            additional: [],
            packaging: null,
          };
        }
        switch (asset.au_office_image_stack) {
          case "Primary":
            if (groupedSKUs[`${sku}`].primary === null)
              groupedSKUs[`${sku}`].primary = asset.au_salsify_digital_asset_id;
            break;
          case "Additional":
            if (!asset.au_office_image_ordering) continue;
            groupedSKUs[`${sku}`].additional.push({
              id: asset.au_salsify_digital_asset_id,
              order: asset.au_office_image_ordering,
            });
            break;
          case "Packaging":
            if (groupedSKUs[`${sku}`].packaging === null)
              groupedSKUs[`${sku}`].packaging =
                asset.au_salsify_digital_asset_id;
            break;
          default:
            continue;
        }
      }
    }
    return groupedSKUs;
  }

  async associateAssetsWithProducts(brand) {
    await this.getAssetsForSalsifyUpdate({ brand, salsifyDAExists: true });
    const query = {};
    if (brand) query.where = { brand };
    this.salsifyBufferProducts = await this.SalsifyBufferProduct.findAll(query);
    const groupedSKUs = this.createSKUGroupingForBulkUpdate();
    const payloadsArray = [];
    for (const sku in groupedSKUs) {
      if (Object.hasOwnProperty.call(groupedSKUs, sku)) {
        const matchingSalsifyProduct = _.find(this.salsifyBufferProducts, {
          sku,
        });
        if (!matchingSalsifyProduct) continue;
        const skuValues = groupedSKUs[sku];
        const payload = {
          "PRODUCT CODE": sku,
        };
        if (skuValues.primary) payload["Primary Image"] = skuValues.primary;
        if (skuValues.additional.length > 1) {
          payload["Additional Image"] = _.orderBy(skuValues.additional, [
            "order",
          ]).map((value) => value.id);
        }
        if (skuValues.additional.length == 1) {
          payload["Additional Image"] = skuValues.additional[0].id;
        }
        payloadsArray.push(payload);
      }
    }

    const chunkedAssets = _.chunk(payloadsArray, 80);

    let counterForBulk = 0;
    for (const assetChunk of chunkedAssets) {
      try {
        const results = await this.SalsifyAPI.updateMultipleProducts(
          assetChunk
        );
        for (const asset of assetChunk) {
          const au_salsify_digital_asset_id = asset.au_salsify_digital_asset_id;
          const matchingAsset = _.find(this.brandfolderBufferAssetsDist, {
            au_salsify_digital_asset_id,
          });
          if (matchingAsset) {
            await matchingAsset.update({
              au_salsify_linked_to_product: true,
            });
          }
        }
      } catch (error) {
        Logger.error(
          `Problem with digital asset ${currentAsset.au_salsify_digital_asset_id}`
        );
      }
      counterForBulk++;
      Logger.info(
        `Updated Salsify associations: ${counterForBulk} out of ${chunkedAssets.length}`
      );
    }
  }

  async createSkuAndUrlList() {
    this.brandfolderBufferAssets = await this.BrandfolderBufferAsset.findAll({
      include: {
        model: BrandfolderBufferAttachment,
        attributes: ["id", "extension", "url"],
      },
      attributes: ["id", "au_skus"],
      where: {
        au_office_image_stack: "Primary",
      },
    });

    for (const asset of this.brandfolderBufferAssets) {
      try {
        if (!asset.BrandfolderBufferAttachments)
          throw `No attachment for Asset ${asset.id}`;
        const properAttachments = asset.BrandfolderBufferAttachments.filter(
          (attachment) => attachment.extension !== "psd"
        );
        if (properAttachments.length === 0)
          throw `Asset ${asset.id} just has a PSD`;
        const skusForThisAsset = [].concat(asset.au_skus);
        for (const sku of skusForThisAsset) {
          this.skusAndUrls.push({
            sku,
            url: properAttachments[0].url,
          });
        }
      } catch (error) {
        Logger.error(`Problem with asset ${asset.id}: ${error}`);
      }
    }

    await createSkuAndUrlList({
      arrayOfSKUs: this.skusAndUrls,
    });
  }

  async attachmentsIntoSalsifyDigitalAssets(brand) {
    this.brandfolderBufferAttachments =
      await this.BrandfolderBufferAttachment.findAll({
        where: {
          section: {
            [Op.in]: this.brandfolderSectionIDs,
          },
        },
      });

    const filterTheseNames = [
      // "Drylite Towel",
      // "Pocket Towel",
      // "Tek Towel",
      // "Airlite Towel",
    ];
    // Black Diamond collection
    this.brandfolderBufferAttachments =
      this.brandfolderBufferAttachments.filter((attachment) => {
        const foundCollection = _.find(attachment.collections, {
          id: "wfrz8z5w79kbqk6hj4pf5b97",
        });
        return valueExistsAndIsNotEmpty(foundCollection);
      });
    this.brandfolderBufferAttachments = this.filterAttachments(
      this.brandfolderBufferAttachments,
      filterTheseNames
    );

    for (const attachment of this.brandfolderBufferAttachments) {
      const filename = attachment.filename;
      const url = attachment.url;

      try {
        const salsifyCreationResult =
          await this.SalsifyAPI.createOneDigitalAsset(url, filename);
        const salsifyDigitalAssetId = salsifyCreationResult["salsify:id"];
        const updateAttachmentResult = await attachment.update({
          au_salsify_digital_asset_id: salsifyDigitalAssetId,
        });
        Logger.info(
          `Created new Salsify digital asset for BF Attachment ${attachment.attachment_id}`
        );
      } catch (error) {
        Logger.error(`Problem with attachment ${attachment.attachment_id}`);
      }
    }
  }

  async associateAttachmentsWithProducts() {
    this.salsifyBufferProducts = await this.SalsifyBufferProduct.findAll({
      where: {
        brand: "Sea to Summit",
      },
    });
    this.brandfolderBufferAttachments =
      await this.BrandfolderBufferAttachment.findAll({
        where: {
          section: {
            [Op.in]: this.brandfolderSectionIDs,
          },
          au_salsify_linked_to_product: false,
        },
      });
    const filterTheseNames = [
      "Drylite Towel",
      "Pocket Towel",
      "Tek Towel",
      "Airlite Towel",
    ];
    this.brandfolderBufferAttachments = this.filterAttachments(
      this.brandfolderBufferAttachments,
      filterTheseNames,
      true
    );

    for (const attachment of this.brandfolderBufferAttachments) {
      const matchingSalsifyProduct = _.find(this.salsifyBufferProducts, {
        barcode_ean: attachment.ean,
      });
      if (!matchingSalsifyProduct) {
        Logger.error(
          `No matching salsify product for BF Attachment ${attachment.attachment_id} with EAN ${attachment.ean}`
        );
        continue;
      }

      const sku = matchingSalsifyProduct.sku;
      const salsifyId = attachment.au_salsify_digital_asset_id;
      let type; // TODO: fix this sloppiness
      if (attachment.primary) type = "Primary";
      if (attachment.packaging) type = "Packaging";

      if (!type) {
        Logger.error(
          `Attachment ${attachment.attachment_id} is not Primary or Packaging`
        );
        continue;
      }
      try {
        const salsifyAssociationResult =
          await this.SalsifyAPI.associateDigitalAssetWithProduct(
            sku,
            type,
            salsifyId
          );
        const attachmentUpdateResult = await attachment.update({
          au_salsify_linked_to_product: true,
        });
        Logger.info(
          `Associated Salsify digital asset with SKU ${matchingSalsifyProduct.sku}`
        );
      } catch (error) {
        Logger.error(`Problem with attachment ${attachment.attachment_id}`);
      }
    }
  }

  filterAttachments(
    attachmentsArray,
    namesMatch = [],
    existingSalsifyDigitalAssets = false
  ) {
    const filteredArray = attachmentsArray.filter(
      function (attachment) {
        // const hasSKU = !valueIsNullOrUndefined(attachment.sku);
        const hasEAN = valueExistsAndIsNotEmpty(attachment.ean);
        if (attachment.ean === 0) return false;
        let hasKeyWords = false;
        const namesMatchArray = this.namesMatch;
        for (const value of namesMatchArray) {
          if (valueIsNullOrUndefined(attachment.name)) return false;
          if (attachment.name.search(value) !== -1) {
            hasKeyWords = true;
            break;
          }
        }

        if (this.existingSalsifyDigitalAssets) {
          if (valueIsNullOrUndefined(attachment.au_salsify_digital_asset_id)) {
            return false;
          }
        } else {
          if (
            valueExistsAndIsNotEmpty(attachment.au_salsify_digital_asset_id)
          ) {
            return false;
          }
        }

        const isPrimary = attachment.primary;
        const isPackaging = attachment.packaging;
        const isAdditional = valueExistsAndIsNotEmpty(
          attachment.au_office_additional_ordering
        );
        const hasJPGExtension = ["jpg", "png"].includes(attachment.extension);
        const allTrue =
          hasJPGExtension &&
          (isPrimary || isPackaging || isAdditional) &&
          hasEAN;
        return allTrue;
      },
      { namesMatch, existingSalsifyDigitalAssets }
    );
    return filteredArray;
  }
}

module.exports.BrandfolderOutput = BrandfolderOutput;
