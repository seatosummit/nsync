const { BrandfolderBuffer } = require("../assets/BrandfolderBuffer");
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

const salsifyImageTransfer = async (brand) => {
  try {
    console.log("Syncing Salsify assets to Brandfolder...");
    const buffer = new BrandfolderBuffer();
    // await buffer.getAssetsForBufferUpdate();
    await buffer.updateBufferEntries();
    await buffer.getBufferAssetsForBrandfolderSync();
    // await buffer.syncBufferEntriesToBrandfolder();
    // await buffer.updateBrandfolderCustomFields();
    await buffer.cleanUpStaleBufferAssets();
    // await buffer.consolidateAndSaveTags();
    console.log("... done syncing Salsify assets to Brandfolder.");
  } catch (error) {
    console.error(
      `Error syncing Black Diamond product images to Brandfolder: ${error}`
    );
  }
};

module.exports.salsifyImageTransfer = salsifyImageTransfer;
