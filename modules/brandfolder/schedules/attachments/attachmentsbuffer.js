const { updateAttachmentsBuffer } = require("../../attachments/attachments");
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

const attachmentsBuffer = async () => {
  try {
    // console.log("Updating attachments buffer...");
    await updateAttachmentsBuffer();
    // console.log("... done updating attachments buffer.");
  } catch (error) {
    console.error(`Error attachments buffer: ${error}`);
  }
};

attachmentsBuffer();
