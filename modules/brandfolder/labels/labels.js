const fetch = require("node-fetch");
const { promises: fsp } = require("fs");
const fs = require("fs");
const sharp = require("sharp");
const pathTools = require("path");
const _ = require("lodash");
require("dotenv").config();
const { BrandfolderBufferLabel } = require("../../../models");
const { prepareHeaders } = require("../../helpers/apitools");
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");
const { decorateLabel } = require("./decorators");
const { saveToLabelsBuffer } = require("./buffer");
const { BrandfolderAPI: API } = require("../../helpers/brandfolderAPI");

module.exports = {
  updateLabelsBuffer: async (brandfolder_id = "q7cphz-8wbqqw-1dru9z") => {
    const BrandfolderAPI = new API();
    const headers = await prepareHeaders();
    const labelsBuffer = await BrandfolderBufferLabel.findAll({
      raw: true,
    });
    try {
      const labelsResponse = await BrandfolderAPI.getLabelsWithPage(
        1,
        brandfolder_id
      );
      await module.exports.processLabelsBatch(
        labelsResponse,
        labelsBuffer,
        brandfolder_id
      );
      let nextPage = labelsResponse.meta.next_page;
      while (nextPage != null) {
        const labelsResponse = await BrandfolderAPI.getLabelsWithPage(
          nextPage,
          brandfolder_id
        );
        await module.exports.processLabelsBatch(
          labelsResponse,
          labelsBuffer,
          brandfolder_id
        );
        nextPage = labelsResponse.meta.next_page;
      }
    } catch (error) {
      console.error(`Error saving this batch of Brandfolder labels: ${error}`);
    }
  },
  processLabelsBatch: async (labelsResponse, buffer, brandfolder_id) => {
    const arrayOfLabels = labelsResponse.data;
    const decoratedLabels = arrayOfLabels.map((attachment) => {
      return decorateLabel(attachment, brandfolder_id);
    });
    let attachmentSavePromises = [];
    for (const decoratedLabel of decoratedLabels) {
      attachmentSavePromises.push(saveToLabelsBuffer(decoratedLabel, buffer));
    }
    try {
      await Promise.all(attachmentSavePromises);
      console.log(`Saved ${attachmentSavePromises.length} labels to buffer`);
    } catch (error) {
      console.error(`Error saving this batch of labels: ${error}`);
    }
  },
};
