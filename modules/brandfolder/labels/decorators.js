module.exports = {
  decorateLabel: (brandfolderLabel, brandfolder_id) => {
    const decoratedLabel = {
      label_id: brandfolderLabel.id,
      name: brandfolderLabel.attributes.name,
      path: brandfolderLabel.attributes.path,
      depth: brandfolderLabel.depth,
      brandfolder_id,
      snapshot: brandfolderLabel,
    };
    return decoratedLabel;
  },
};
