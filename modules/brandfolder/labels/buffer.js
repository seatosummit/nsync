const fetch = require("node-fetch");
const _ = require("lodash");
require("dotenv").config();
const { promisify } = require("util");
const sleep = promisify(setTimeout);
const { prepareHeaders } = require("../../helpers/apitools");

const { BrandfolderBufferLabel } = require("../../../models");

module.exports = {
  saveToLabelsBuffer: async (label, buffer) => {
    try {
      const foundMatchingBufferProduct = _.find(buffer, [
        "label_id",
        label.label_id,
      ]);
      if (!foundMatchingBufferProduct) {
        await BrandfolderBufferLabel.create(label);
        //console.log(
        //   `Created new BrandfolderBufferLabel ${label.label_id}`
        // );
      } else {
        await BrandfolderBufferLabel.update(label, {
          where: {
            label_id: label.label_id,
          },
        });
        //console.log(
        //   `Updated BrandfolderBufferLabel ${label.label_id}, it already existed`
        // );
      }
    } catch (error) {
      console.error(`Problems: ${error} for label ${label.label_id}`);
    }
  },
};
