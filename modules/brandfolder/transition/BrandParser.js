const {
  valueExistsAndIsNotEmpty,
  valueIsNullOrUndefined,
} = require("../../helpers/stringTools");
module.exports.BrandParser = class {
  static getUniqueIdentifier(filename, brand = "Black Diamond") {
    let identifierRegex = /^([a-zA-Z0-9]{4,}_[a-zA-Z0-9]{3,})_/;
    let salsifyIdentifierToUse = "brand_internal_product_code";
    switch (brand) {
      case "Black Diamond":
        identifierRegex = /^([a-zA-Z0-9]{4,}_[a-zA-Z0-9]{3,})_/;
        salsifyIdentifierToUse = "brand_internal_product_code";
        break;
      case "Biolite":
        identifierRegex = /^\d{4,}_\d{4}/;
        salsifyIdentifierToUse = "sku";
        break;
      case "Sea to Summit":
        identifierRegex = /^\d{4,}_\d{4}/;
        salsifyIdentifierToUse = "sku";
        break;
      case "Gerber":
        identifierRegex = /^(GE\d{7,})_/;
        salsifyIdentifierToUse = "sku";
        break;
      case "Sunday Afternoons":
        identifierRegex = /^(\w{7,})\.[jtp]/;
        salsifyIdentifierToUse = "sku";
        break;
      default:
        break;
    }
    const identifier = identifierRegex.exec(filename);
    if (identifier) {
      return { identifier: identifier[1], salsifyIdentifierToUse };
    } else {
      return false;
    }
  }

  static getImageOrderNumberFromFilename(filename) {
    const imageOrderingRegex = /[_-]*(\d+)_\.[gtpj]/;
    const imageOrderingNumber = imageOrderingRegex.exec(filename);
    if (!imageOrderingNumber) return false;
    const orderNumber = parseInt(imageOrderingNumber[1]);
    return orderNumber;
  }
};
