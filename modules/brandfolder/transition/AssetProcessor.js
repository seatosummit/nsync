const _ = require("lodash");
const { Logger } = require("../../helpers/Logger");
const {
  valueExistsAndIsNotEmpty,
  valueIsNullOrUndefined,
  getNumberAsZeroFilledString,
  getAUImageStackFromFilename,
} = require("../../helpers/stringTools");
const { BrandParser } = require("./BrandParser");

module.exports.AssetProcessor = (assetsDataSetFromBrandfolder) => {
  const data = assetsDataSetFromBrandfolder.data ?? null;
  const included = assetsDataSetFromBrandfolder.included ?? null;
  return {
    createDataset: () => {
      if (!valueExistsAndIsNotEmpty(data)) return null;
      if (!valueExistsAndIsNotEmpty(included)) return null;

      const simplifiedData = data.map((item) => {
        return {
          id: item.id,
          name: item.attributes.name,
          attachment_id: item.relationships.attachments?.data[0].id,
          brand_id: item.relationships.custom_field_values?.data[0].id,
        };
      });
      const amendedData = simplifiedData.map((item) => {
        try {
          const foundAttachment = _.find(included, {
            id: item.attachment_id,
          });
          const attachment_filename =
            foundAttachment?.attributes?.filename ?? null;
          const foundBrand = _.find(included, { id: item.brand_id });
          const brand_name = foundBrand?.attributes?.value;

          const AUOfficeImageStack =
            getAUImageStackFromFilename(attachment_filename);

          const imageOrderingNumber =
            BrandParser.getImageOrderNumberFromFilename(attachment_filename);
          const additionalImageOrdering =
            AUOfficeImageStack == "Additional"
              ? getNumberAsZeroFilledString(imageOrderingNumber - 1)
              : null;
          const uniqueIdentifier = BrandParser.getUniqueIdentifier(
            attachment_filename,
            brand_name
          );

          return {
            ...item,
            attachment_filename,
            brand_name,
            AUOfficeImageStack,
            imageOrderingNumber,
            uniqueIdentifier,
            additionalImageOrdering,
          };
        } catch (error) {
          Logger.error(`issue with attachment ${item.attachment_id}`);
        }
      });
      return amendedData;
    },
  };
};
