const _ = require("lodash");
const { Sequelize } = require("sequelize");
const Op = Sequelize.Op;
const { DateTime } = require("luxon");
const { promisify } = require("util");
const sleep = promisify(setTimeout);
const { BrandfolderAPI } = require("../../helpers/brandfolderAPI");
const {
  valueIsNullOrUndefined,
  valueExistsAndIsNotEmpty,
  getAUImageStackFromFilename,
  getNumberAsZeroFilledString,
} = require("../../helpers/stringTools");

const {
  BrandfolderBufferAttachment,
} = require("./BrandfolderBufferAttachment.js");
const cliProgress = require("cli-progress");
const collectionLookup = require("./collectionlookup.json").map(
  (collection) => {
    return { id: collection.id, name: collection.attributes.name };
  }
);
const transitionAssetLookup = require("./transitionAssetLookup.json");
const customFieldsLookup = require("./customFieldsLookup.json");
const customFieldsDist = customFieldsLookup.DIST.map((field) => {
  return {
    id: field.id,
    key: field.attributes.name,
  };
});
const customFieldsSts = customFieldsLookup.STS.map((field) => {
  return {
    id: field.id,
    key: field.attributes.name,
  };
});
const { BrandParser } = require("./BrandParser");

const {
  SalsifyProduct,
  SalsifyProductParent,
  WMSProduct,
  BrandfolderBufferLabel,
  BrandfolderBufferAsset: BrandfolderBufferAssetDBModel,
  BrandfolderBufferAttachment: BrandfolderBufferAttachmentDBModel,
  SalsifyGlobalProduct,
} = require("../../../models");
const { Logger } = require("../../helpers/Logger");
const { CSVWriter } = require("../../helpers/CSVWriter");
const { AssetProcessor } = require("./AssetProcessor");
const { BrandfolderProductAsset } = require("./BrandfolderProductAsset");

class BrandfolderConverter {
  constructor() {
    this.SalsifyBufferProductParent = SalsifyProductParent;
    this.SalsifyBufferProduct = SalsifyProduct;
    this.SalsifyGlobalBufferProduct = SalsifyGlobalProduct;
    this.BrandfolderBufferAssetDBModel = BrandfolderBufferAssetDBModel;
    this.BrandfolderBufferAttachment = BrandfolderBufferAttachment;
    this.BrandfolderBufferAttachmentDBModel =
      BrandfolderBufferAttachmentDBModel;
    this.updatePromises = [];
    this.newAssets = [];
    this.BrandfolderAPI = new BrandfolderAPI();
    this.eansNotFoundInGlobalSalsify = [];
    this.salsifyBufferProductParents = [];
    this.salsifyBufferProducts = [];
    this.salsifyGlobalBufferProducts = [];
    this.brandfolderBufferAttachmentsWithEan = [];
    this.brandfolderBufferAttachments = [];
    this.brandfolderBufferAssets = [];
    this.progressbar = new cliProgress.SingleBar(
      {},
      cliProgress.Presets.shades_classic
    );
    this.collectionLookup = collectionLookup;
    this.transitionAssetLookup = transitionAssetLookup;
    this.customFieldsDist = customFieldsDist;
    this.customFieldsSts = customFieldsSts;

    this.brandfolderSectionIDs = Object.values({
      "STS Product Images": "q7cphz-8wbqqw-c4dg2u",
      // "360 Product Images": "tb6mwcswp9ppx3thgqpk336",
      // "Product Images Transition": "f37sc7fbnsbh3x7578b9rgp",
      // "Dist Product Images": "ht7s95whgp3b7txkgf987nt",
      // "Dist Product Images Packaging": "twz7tcfkmb96hpb6mgrmnqq",
      // "Dist Product Images Additional": "x9cgtxbbhkp3t87c54pmrc5",
    });

    this.headers;
    this.CSV;
    this.BrandParser = BrandParser;
  }

  async getAssetsForConversion() {
    this.brandfolderBufferAttachments =
      await this.BrandfolderBufferAttachmentDBModel.findAll({
        where: {
          section: {
            [Op.in]: this.brandfolderSectionIDs,
          },
        },
      });
  }

  async updateBufferWithProductSections(brand, goBackDays) {
    const query = {
      where: {
        section: {
          [Op.in]: this.brandfolderSectionIDs,
        },
      },
    };
    const salsifyQuery = {};
    if (brand) {
      //query.where.brand = brand;
      salsifyQuery.where = {
        brand,
      };
    }
    if (goBackDays) {
      const goBackDate = DateTime.now().minus({ days: goBackDays }).toISODate();
      query.where.updatedAt = { [Op.gt]: goBackDate };
    }
    this.brandfolderBufferAttachments =
      await this.BrandfolderBufferAttachmentDBModel.findAll(query);
    this.brandfolderAssets = await this.BrandfolderBufferAssetDBModel.findAll(
      {}
    );
    this.salsifyBufferProductParents =
      await this.SalsifyBufferProductParent.findAll(salsifyQuery);
    this.salsifyBufferProducts = await this.SalsifyBufferProduct.findAll(
      salsifyQuery
    );
    this.salsifyGlobalBufferProducts =
      await this.SalsifyGlobalBufferProduct.findAll({});
  }

  async refreshAssetsBuffer(settings = {}) {
    let query = settings;
    query.order = [["createdAt", "ASC"]];
    if (settings.onlyCreate) {
      query = {
        where: {
          new_asset_id: { [Op.eq]: null },
          section: {
            [Op.in]: this.brandfolderSectionIDs,
          },
        },
      };
    }
    if (settings.onlyNew) {
      query = {
        where: {
          new_asset_id: { [Op.not]: null },
          section: {
            [Op.in]: this.brandfolderSectionIDs,
          },
        },
      };
    }
    this.brandfolderBufferAssets =
      await this.BrandfolderBufferAssetDBModel.findAll(query);
  }

  async refreshAttachmentsBuffer(settings = {}) {
    let query = settings;
    query.order = [["createdAt", "ASC"]];
    if (settings.onlyCreate) {
      query = {
        where: {
          new_asset_id: { [Op.eq]: null },
          section: {
            [Op.in]: this.brandfolderSectionIDs,
          },
        },
      };
    }
    if (settings.onlyNew) {
      query = {
        where: {
          new_asset_id: { [Op.not]: null },
          section: {
            [Op.in]: this.brandfolderSectionIDs,
          },
        },
      };
    }
    this.brandfolderBufferAttachments =
      await this.BrandfolderBufferAttachmentDBModel.findAll(query);
    this.brandfolderBufferAttachments =
      this.brandfolderBufferAttachments.filter((attachment) => {
        const foundCollection = _.find(attachment.collections, {
          id: "wfrz8z5w79kbqk6hj4pf5b97",
        });
        return valueExistsAndIsNotEmpty(foundCollection);
      });
  }

  async getAssetsForAttachmentAdds() {
    this.salsifyBufferProductParents =
      await this.SalsifyBufferProductParent.findAll({
        where: {
          [Op.or]: [{ brand: "Sea To Summit" }, { brand: "360 Degrees" }],
        },
      });
    this.salsifyBufferProducts = await this.SalsifyBufferProduct.findAll({
      where: {
        [Op.or]: [{ brand: "Sea To Summit" }, { brand: "360 Degrees" }],
      },
    });
    this.salsifyGlobalBufferProducts =
      await this.SalsifyGlobalBufferProduct.findAll({});

    this.brandfolderBufferAttachments =
      await this.BrandfolderBufferAttachmentDBModel.findAll({});
    this.brandfolderAssets = await this.BrandfolderBufferAssetDBModel.findAll(
      {}
    );
  }

  async addDetailsToAttachments() {
    this.progressbar.start(this.brandfolderBufferAttachments.length, 0);
    for (const attachment of this.brandfolderBufferAttachments) {
      let matchingBufferGlobalSalsifyProduct = _.find(
        this.salsifyGlobalBufferProducts,
        {
          salsify_id: attachment.ean,
        }
      );
      if (!matchingBufferGlobalSalsifyProduct) {
        matchingBufferGlobalSalsifyProduct = _.find(
          this.salsifyGlobalBufferProducts,
          {
            sku: attachment.sku,
          }
        );
      }
      const matchingBufferAUSalsifyProductParent = _.find(
        this.salsifyBufferProductParents,
        {
          sku: attachment.sku,
        }
      );

      let matchingBufferAUSalsifyProduct;
      if (valueExistsAndIsNotEmpty(attachment.ean)) {
        matchingBufferAUSalsifyProduct = _.find(this.salsifyBufferProducts, {
          barcode_ean: attachment.ean,
        });
      } else {
        matchingBufferAUSalsifyProduct = _.find(this.salsifyBufferProducts, {
          sku: attachment.sku,
        });
      }

      if (!matchingBufferGlobalSalsifyProduct) {
        const name = this.matchingBufferAUSalsifyProduct
          ? this.matchingBufferAUSalsifyProduct.name
          : attachment.filename;
        this.eansNotFoundInGlobalSalsify.push({
          ean: attachment.ean,
          sku: attachment.sku,
          name,
          attachment_id: attachment.attachment_id,
        });
      }
      const matchingBufferAsset = _.find(this.brandfolderAssets, {
        asset_id: attachment.asset_id,
      });
      if (!matchingBufferAsset) {
        continue;
      }
      const currentAttachment = new BrandfolderBufferAttachment(
        attachment,
        matchingBufferAsset,
        matchingBufferGlobalSalsifyProduct,
        matchingBufferAUSalsifyProduct,
        matchingBufferAUSalsifyProductParent,
        this.progressbar
      );
      currentAttachment.decorate();
      this.updatePromises.push(currentAttachment.upsertPromise());
    }

    try {
      const results = await Promise.all(this.updatePromises);
      this.progressbar.stop();
      Logger.info(`${results.length} attachments updated with details.`);
      Logger.info(
        `No matching Global salsify product for ${this.eansNotFoundInGlobalSalsify.length} EANs.`
      );
      const CSVHeaders = [
        { id: "name", title: "Name" },
        { id: "sku", title: "SKU" },
        { id: "ean", title: "EAN" },
      ];
      const globalSalsifyOrphans = this.eansNotFoundInGlobalSalsify.map(
        (item) => {
          return {
            name: item.name,
            sku: item.sku,
            ean: item.ean,
          };
        }
      );
      this.CSV = new CSVWriter({
        path: "./productsNotFoundInGlobal.csv",
        header: CSVHeaders,
        records: globalSalsifyOrphans,
      });
      await this.CSV.create();
    } catch (error) {
      Logger.error(error);
    }
  }

  convertToArray(eitherArrayOrString) {
    return [].concat(eitherArrayOrString);
  }

  async createNewAssetsFromAttachments() {
    this.brandfolderBufferAttachments =
      this.brandfolderBufferAttachments.filter(
        (attachment) => attachment.section === "q7cphz-8wbqqw-c4dg2u" // focus only on the Product Images section
        // (attachment) => attachment.section === "tb6mwcswp9ppx3thgqpk336" // focus only on the 360 Degrees Product Images section
      );
    // this.brandfolderBufferAttachments =
    //   this.brandfolderBufferAttachments.filter(
    //     // (attachment) => attachment.section === "q7cphz-8wbqqw-c4dg2u" // focus only on the Product Images section
    //     (attachment) =>
    //       attachment.filename === "TekTowel-Desert-XSmall-01_9327868149162.jpg" // focus only on the 360 Degrees Product Images section
    //   );
    this.brandfolderBufferAttachments = this.brandfolderBufferAttachments.slice(
      0,
      2000
    ); // TODO: remove soon

    for (const attachment of this.brandfolderBufferAttachments) {
      const newAsset = {
        data: {
          attributes: [
            {
              name: attachment.name,
              description: attachment.description,
              attachments: [
                {
                  url: attachment.url,
                  filename: attachment.filename,
                },
              ],
            },
          ],
        },
        section_key: attachment.section,
      };
      this.updatePromises.push(
        this.createNewAssetAndCleanUp(
          "q7cphz-8wbqqw-1dru9z",
          newAsset,
          attachment,
          this.BrandfolderAPI
        )
      );
      try {
        const results = await Promise.all(this.updatePromises);
        Logger.info(`Added ${results.length} new Assets to BF.`);
      } catch (error) {
        Logger.info(error);
      }
    }
  }

  async createNewAssetAndCleanUp(
    brandfolderID = "q7cphz-8wbqqw-1dru9z",
    newAsset,
    attachment,
    BrandfolderAPIInstance
  ) {
    const createdNewAsset =
      await BrandfolderAPIInstance.createNewBrandfolderAssetInBrandfolder(
        brandfolderID,
        newAsset,
        attachment
      );

    const attachmentToUpdate =
      await this.BrandfolderBufferAttachmentDBModel.findOne({
        where: {
          attachment_id: attachment.attachment_id,
        },
      });

    const new_asset_id = createdNewAsset.new.data[0].id;
    const new_attachment_id = createdNewAsset.new.included[0].id;
    const new_attachment_url = createdNewAsset.new.included[0].attributes.url;

    try {
      await BrandfolderAPIInstance.deleteAttachment(attachment.attachment_id);
      // TODO: remove old assets safely.
      return await attachmentToUpdate.update({
        new_asset_id,
        new_attachment_id,
        new_attachment_url,
        deleted: true,
      });
    } catch (error) {
      Logger.error(`Couldn't delete attachment ${attachment.attachment_id}`);
      Logger.error(error);
    }
  }

  async addAssetsToLabels() {
    let labelAssociations = {};
    await this.refreshAttachmentsBuffer({ onlyNew: true });
    const attachmentsWithLabels = this.brandfolderBufferAttachments.filter(
      (attachment) => {
        return valueExistsAndIsNotEmpty(attachment.labels);
      }
    );
    for (const attachment of attachmentsWithLabels) {
      for (const label of attachment.labels) {
        if (valueExistsAndIsNotEmpty(labelAssociations[`${label.id}`])) {
          labelAssociations[`${label.id}`].labels.push(attachment.new_asset_id);
        } else {
          labelAssociations[`${label.id}`] = {
            name: label.name,
            labels: [attachment.new_asset_id],
          };
        }
      }
    }
    try {
      for (const label_key in labelAssociations) {
        if (Object.hasOwnProperty.call(labelAssociations, label_key)) {
          const labelName = labelAssociations[label_key].name;
          const asset_keys = labelAssociations[label_key].labels;
          if (asset_keys.length === 0) continue;
          await this.BrandfolderAPI.associateAssetsWithLabels(
            asset_keys,
            label_key
          );
          Logger.info(
            `Added ${asset_keys.length} assets to label ${labelName}`
          );
        }
      }
    } catch (error) {
      throw new Error(`Problems with label association: ${error}`);
    }
  }

  async getCollectionsGrouping() {
    let collectionAssociations = {};
    await this.refreshAttachmentsBuffer({ onlyNew: true });
    const attachmentsWithCollections = this.brandfolderBufferAttachments.filter(
      (attachment) => {
        return valueExistsAndIsNotEmpty(attachment.collections);
      }
    );
    for (const attachment of attachmentsWithCollections) {
      for (const collection of attachment.collections) {
        if (
          valueExistsAndIsNotEmpty(collectionAssociations[`${collection.id}`])
        ) {
          collectionAssociations[`${collection.id}`].assets.push(
            attachment.new_asset_id
          );
        } else {
          collectionAssociations[`${collection.id}`] = {
            name: `${collection.name}`,
            assets: [attachment.new_asset_id],
          };
        }
      }
    }
    return collectionAssociations;
  }

  async addAssetsToCollections() {
    const collectionAssociations = await this.getCollectionsGrouping();
    try {
      for (const collection_key in collectionAssociations) {
        if (
          Object.hasOwnProperty.call(collectionAssociations, collection_key)
        ) {
          const collectionName = collectionAssociations[collection_key].name;
          const asset_keys = collectionAssociations[collection_key].assets;
          if (asset_keys.length === 0) continue;
          const collectionKeyArray = [collection_key];
          await this.BrandfolderAPI.associateAssetsWithCollections(
            asset_keys,
            collectionKeyArray
          );
          Logger.info(
            `Associated ${asset_keys.length} assets with collection ${collectionName}`
          );
        }
      }
    } catch (error) {
      throw new Error(`Problems with collection association: ${error}`);
    }
  }

  async consolidateTags() {
    await this.refreshAttachmentsBuffer({ onlyNew: true });
    const tagAssociations = {};
    for (const attachment of this.brandfolderBufferAttachments) {
      if (!valueExistsAndIsNotEmpty(attachment.tags)) continue;
      const tags = attachment.tags;
      const new_asset_id = attachment.new_asset_id;
      for (const tag of tags) {
        if (valueExistsAndIsNotEmpty(tagAssociations[`${tag}`])) {
          tagAssociations[`${tag}`].push(new_asset_id);
        } else {
          tagAssociations[`${tag}`] = [new_asset_id];
        }
      }
    }

    return tagAssociations;
  }

  async associateAssetsWithTags() {
    const tagAssociations = await this.consolidateTags();
    for (const tag in tagAssociations) {
      if (Object.hasOwnProperty.call(tagAssociations, tag)) {
        const tagAssociationIds = tagAssociations[tag];
        try {
          const tagAssociationResults =
            await this.BrandfolderAPI.associateAssetsWithTag(
              tagAssociationIds,
              tag
            );
          Logger.error(
            `Associated ${tagAssociationIds.length} assets with tag ${tag}`
          );
        } catch (error) {
          Logger.error(`Problem with associations for tag ${tag}: ${error}`);
        }
      }
    }
  }
  async addTagsToAssets() {
    const assetsAtATime = 100;
    let assetsCounter = 0;
    await this.refreshAttachmentsBuffer({ onlyNew: true });
    const attachmentsWithTags = this.brandfolderBufferAttachments.filter(
      (attachment) => valueExistsAndIsNotEmpty(attachment.tags)
    );
    const arrayChunks = _.chunk(attachmentsWithTags, assetsAtATime);
    for (const arrayWithAttachments of arrayChunks) {
      this.updatePromises = [];
      // TODO: bulk assign tags
      for (const attachment of arrayWithAttachments) {
        this.updatePromises.push(
          this.BrandfolderAPI.addTagsToOneAsset(
            attachment.new_asset_id,
            attachment.tags
          )
        );
      }
      try {
        const results = await Promise.all(this.updatePromises);
        const actualResults = results.filter((result) => result);

        assetsCounter += assetsAtATime;
        await sleep(5000);
        Logger.info(
          `Successfully added tags to ${assetsCounter}/${attachmentsWithTags.length} assets`
        );
      } catch (error) {
        Logger.error(error);
      }
    }
  }

  async getCustomFieldsAssociations() {
    await this.refreshAttachmentsBuffer({ onlyNew: true });
    const customFieldAssociations = {};
    for (const attachment of this.brandfolderBufferAttachments) {
      if (!valueExistsAndIsNotEmpty(attachment.custom_fields)) continue;
      const customFields = attachment.custom_fields;

      for (const customField of customFields) {
        if (!valueExistsAndIsNotEmpty(customField.value)) continue;
        const associationEntry = {
          attributes: {
            value: customField.value,
          },
          relationships: {
            asset: {
              data: {
                type: "asset",
                id: attachment.new_asset_id,
              },
            },
          },
        };
        if (
          valueExistsAndIsNotEmpty(customFieldAssociations[`${customField.id}`])
        ) {
          customFieldAssociations[`${customField.id}`].data.push(
            associationEntry
          );
        } else {
          customFieldAssociations[`${customField.id}`] = {
            name: `${customField.name}`,
            data: [associationEntry],
          };
        }
      }
    }
    return customFieldAssociations;
  }

  async addCustomFieldAssociations() {
    const customFieldAssociations = await this.getCustomFieldsAssociations();
    try {
      for (const customFieldKey in customFieldAssociations) {
        if (
          Object.hasOwnProperty.call(customFieldAssociations, customFieldKey)
        ) {
          const customFieldName = customFieldAssociations[customFieldKey].name;
          const asset_keys = customFieldAssociations[customFieldKey].data;
          if (valueIsNullOrUndefined(asset_keys)) continue;
          if (!valueExistsAndIsNotEmpty(asset_keys)) continue;
          await this.BrandfolderAPI.associateAssetsWithCustomField(
            customFieldKey,
            asset_keys
          );
          Logger.info(
            `Associated ${asset_keys.length} assets with custom field ${customFieldName}`
          );
        }
      }
    } catch (error) {
      throw new Error(`Problems with custom field association: ${error}`);
    }
  }

  async pruneBrandfolderBuffers() {
    await this.refreshAttachmentsBuffer();
    await this.refreshAssetsBuffer();

    this.progressbar.start(this.brandfolderBufferAssets.length, 0);
    for (const asset of this.brandfolderBufferAssets) {
      await this.checkForAssetAndUpdateBuffer(asset);
      this.progressbar.increment();
    }
  }

  async checkForAssetAndUpdateBuffer(asset) {
    try {
      const foundAsset = await this.BrandfolderAPI.getAssetWithId(
        asset.asset_id
      );
      if (foundAsset.name === "NotFoundError") {
        await asset.destroy();
        // Logger.info(
        //   `Asset ${asset.asset_id} was not found. It was deleted from buffer.`
        // );
      } else {
        // Logger.info(`Asset ${asset.asset_id} was found.`);
      }
    } catch (error) {
      Logger.error(`Issue with pruning Asset ${asset.asset_id}: {error}`);
    }
  }

  async checkForAttachmentsAssetAndUpdateBuffer(asset) {
    try {
      const foundAsset = await this.BrandfolderAPI.getAssetWithId(
        asset.asset_id
      );
      if (!foundAsset) {
        await asset.destroy();
        Logger.info(
          `Asset ${asset.asset_id} was not found. It was deleted from buffer.`
        );
      } else {
        Logger.info(`Asset ${asset.asset_id} was found.`);
      }
    } catch (error) {
      await asset.destroy();
      Logger.info(
        `Asset ${asset.asset_id} was not found. It was deleted from buffer.`
      );
    }
  }

  async processLabels() {
    this.labelsBuffer = await BrandfolderBufferLabel.findAll({
      where: {
        brandfolder_id: "jgs64snkkfgvxkvm454vsmrb",
      },
    });
    this.brandfolderBufferAssetsDist =
      await BrandfolderBufferAssetDBModel.findAll({
        where: {
          // section_id: "frxb996b64bb7prqgg38gns5",
          // bf_processed: false,
        },
      });
    for (const label of this.labelsBuffer) {
      if (["Accessories", "Spare Parts"].includes(label.name)) continue;
      const asset_keys = [];
      for (const asset of this.brandfolderBufferAssetsDist) {
        const tags = asset.tags.map((tag) => tag.name);
        if (tags.includes(label.name)) {
          asset_keys.push(asset.bf_asset_id);
        }
      }
      try {
        if (asset_keys.length === 0) continue;
        await this.BrandfolderAPI.associateAssetsWithLabels(
          asset_keys,
          label.label_id
        );
        Logger.info(
          `Associated ${asset_keys.length} assets with label ${label.name}`
        );
      } catch (error) {
        Logger.error(`Problem with association for label ${label.name}`);
      }
    }
  }

  async processNewAssets() {
    this.salsifyBufferProducts = await SalsifyProduct.findAll({});
    this.salsifyBufferProductParents = await SalsifyProductParent.findAll({});
    this.salsifyGlobalBufferProducts = await SalsifyGlobalProduct.findAll({});
    this.brandfolderBufferAssetsDist =
      await BrandfolderBufferAssetDBModel.findAll({
        where: {
          section_id: "gxssj9rpsnn5gcbm39j7kn",
          bf_processed: false,
        },
        include: BrandfolderBufferAttachmentDBModel,
      });

    this.brandfolderBufferAssetsSTS =
      await BrandfolderBufferAssetDBModel.findAll({
        where: {
          section_id: "frxb996b64bb7prqgg38gns5", // todo: handle STS
          bf_processed: false,
        },
        include: BrandfolderBufferAttachmentDBModel,
      });

    for (const assetToProcess of this.brandfolderBufferAssetsDist) {
      try {
        const filename =
          assetToProcess.BrandfolderBufferAttachments[0].filename;
        const AUOfficeImageStack = getAUImageStackFromFilename(filename);

        const imageOrderingNumber =
          BrandParser.getImageOrderNumberFromFilename(filename);
        const additionalImageOrdering =
          AUOfficeImageStack == "Additional"
            ? getNumberAsZeroFilledString(imageOrderingNumber - 1)
            : null;
        const uniqueConnection = BrandParser.getUniqueIdentifier(
          filename,
          assetToProcess.brand
        );

        //TODO: when there's no filename, use the SKU custom field values instead

        const filter = {};
        filter[`${uniqueConnection.salsifyIdentifierToUse}`] =
          uniqueConnection.identifier;

        const matchingSalsifyProducts = _.filter(
          this.salsifyBufferProducts,
          filter
        );

        if (!valueExistsAndIsNotEmpty(matchingSalsifyProducts)) {
          Logger.info(
            `no Salsify match for BF filename ${filename}, id ${assetToProcess.bf_asset_id}`
          );
          continue;
        }

        assetToProcess.au_office_image_stack = AUOfficeImageStack;
        assetToProcess.au_office_image_ordering = additionalImageOrdering;
        assetToProcess.brand_internal_product_code =
          uniqueConnection.identifier;

        await this.processOneAsset(
          assetToProcess,
          matchingSalsifyProducts,
          "gxssj9rpsnn5gcbm39j7kn" //Product images section_id in DIST TODO: make this work for both DIST and STS
        );
      } catch (error) {
        Logger.error(`Problem with processing new assets: ${error}`);
      }
    }
  }

  async processOneAsset(asset, matchingSalsifyProducts, section_id) {
    let matchingSalsifyParents = [];
    for (const product of matchingSalsifyProducts) {
      const matchingParents = _.filter(this.salsifyBufferProductParents, {
        sku: product.parent_sku,
      });
      if (matchingParents)
        matchingSalsifyParents = [
          ...matchingSalsifyParents,
          ...matchingParents,
        ];
    }
    const currentAsset = new BrandfolderProductAsset(
      this.BrandfolderAPI,
      asset,
      matchingSalsifyProducts,
      matchingSalsifyParents
    );

    currentAsset.decorate();
    await currentAsset.updateAssetBasicDetails();
    await currentAsset.updateTags();
    await currentAsset.updateCustomFields();

    if (currentAsset.hasBeenProcessed()) {
      const au_skus = currentAsset.skus;
      const tags = currentAsset.tags;
      const name = currentAsset.name;
      const eans = currentAsset.eams;
      const description = currentAsset.description;

      const currentCollection = _.find(this.collectionLookup, {
        name: asset.brand,
      });
      if (currentCollection) {
        await currentAsset.moveOutOfProcessing(currentCollection.id);
        const updatedBufferAsset = await asset.update({
          description,
          name,
          tags,
          au_skus,
          eans,
          section_id,
          bf_processed: true,
        });
        Logger.info(`Updated buffer asset details for ${currentAsset.name}`);
      } else {
        Logger.error(
          `Couldn't find a collection to move asset ${currentAsset.name}`
        );
      }
    }
  }

  async processNewAssetsFromBF() {
    let assetsResponse = await this.BrandfolderAPI.getAssetsFromSectionWithPage(
      1,
      400
    );
    await this.processBatchOfAssets(assetsResponse);
    let nextPage = assetsResponse.meta?.next_page;
    while (nextPage != null) {
      assetsResponse = await this.BrandfolderAPI.getAssetsFromSectionWithPage(
        nextPage,
        50
      );
      await this.processBatchOfAssets(assetsResponse);
      nextPage = assetsResponse.meta.next_page;
    }
  }

  async processBatchOfAssets(assetsResponse) {
    const assetProcessor = AssetProcessor(assetsResponse);
    const processedAssets = assetProcessor.createDataset();
    const customFieldAssociations = {};

    for (const asset of processedAssets) {
      const matchingSalsifyProducts = _.filter(this.salsifyBufferProducts, {
        brand_internal_product_code: asset.uniqueIdentifier,
      });

      if (!valueExistsAndIsNotEmpty(matchingSalsifyProducts)) {
        Logger.info(
          `no Salsify product found for BF asset ${asset.attachment_filename}, id ${asset.id}`
        );
        continue;
      }

      let matchingSalsifyParents = [];
      for (const product of matchingSalsifyProducts) {
        const matchingParents = _.filter(this.salsifyBufferProductParents, {
          sku: product.parent_sku,
        });
        if (matchingParents)
          matchingSalsifyParents = [
            ...matchingSalsifyParents,
            ...matchingParents,
          ];
      }

      const currentAsset = new BrandfolderProductAsset(
        this.BrandfolderAPI,
        asset,
        matchingSalsifyProducts,
        matchingSalsifyParents
      );

      currentAsset.decorate();
      await currentAsset.updateAssetBasicDetails();
      await currentAsset.updateTags();
      await currentAsset.updateCustomFields();

      if (currentAsset.hasBeenProcessed()) {
        const currentCollection = this.collectionLookup[asset.brand_name];
        await currentAsset.moveOutOfProcessing(currentCollection.id);
      }

      // TODO: if asset is ready, move it to product section and collection
    }
  }
  async cleanUpAssetDuplicates(sectionId = "q7cphz-8wbqqw-c4dg2u") {
    //
    this.brandfolderBufferAttachments =
      await this.BrandfolderBufferAttachmentDBModel.findAll({
        where: {
          section: "q7cphz-8wbqqw-c4dg2u",
        },
      });
    this.brandfolderBufferAssets =
      await this.BrandfolderBufferAssetDBModel.findAll({
        where: {
          section: "q7cphz-8wbqqw-c4dg2u",
        },
      });
    const grouping = this.brandfolderBufferAttachments.reduce(function (rv, x) {
      (rv[x["filename"]] = rv[x["filename"]] || []).push(x);
      return rv;
    }, {});
    let duplicatesContainer = [];
    for (const attachment in grouping) {
      if (Object.hasOwnProperty.call(grouping, attachment)) {
        const duplicateAttachments = grouping[attachment].filter(
          (attachment) => attachment.new_asset_id
        );

        if (duplicateAttachments.length < 2) {
          Logger.info(`Attachment id ${attachment} doesn't have duplicates.`);
          continue;
        }

        duplicatesContainer = [];

        const metadataToCompare =
          duplicateAttachments[0].snapshot.attributes.metadata
            ?.derived_from_original_document_id;

        for (const attachment of duplicateAttachments) {
          const attachmentMeta =
            attachment.snapshot.attributes.metadata
              .derived_from_original_document_id;
          if (attachmentMeta === metadataToCompare) {
            //Logger.info(`Metadata match`);
          } else {
            Logger.info(`Metadata didn't match with the first duplicate`);
            continue;
          }
          const matchingAssetInBrandfolder =
            await this.BrandfolderAPI.getBrandfolderAssetInfo(
              attachment.new_asset_id
            );
          if (!matchingAssetInBrandfolder) {
            Logger.info(`${attachment.new_asset_id} wasn't found in BF`);
            continue;
          } else {
            if (matchingAssetInBrandfolder.included.length > 3) {
              duplicatesContainer.push(attachment);
            }
            Logger.info(
              `${attachment.new_asset_id} was found in BF, it has ${
                matchingAssetInBrandfolder.included.length - 1
              } custom fields.`
            );
          }
        }

        const duplicatesToBeRemoved = duplicatesContainer.slice(1);
        const assetIdsToRemoveFromBF = [
          ...duplicatesToBeRemoved.map((item) => item.new_asset_id),
        ];
        Logger.info(
          `Attachment ${attachment} has ${assetIdsToRemoveFromBF.length} duplicates that will be removed from BF and the buffer`
        );
        try {
          await this.BrandfolderAPI.bulkDestroyAssets(assetIdsToRemoveFromBF);
          for (const deleteThis of duplicatesToBeRemoved) {
            const matchingBufferAssets = _.filter(
              this.brandfolderBufferAssets,
              {
                asset_id: deleteThis.new_asset_id,
              }
            );
            if (matchingBufferAssets) {
              for (const asset of matchingBufferAssets) {
                await asset.destroy();
              }
            }
            await deleteThis.destroy();
          }
          Logger.info(`Cleaned up attachment ${attachment}`);
        } catch (error) {
          Logger.error(
            `Problem with attachment ${attachment} cleanup: ${error}`
          );
        }
      }
    }
  }
}

module.exports.BrandfolderConverter = BrandfolderConverter;
