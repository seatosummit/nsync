const { Logger } = require("../../helpers/Logger");
const {
  valueExistsAndIsNotEmpty,
  getNumberAsZeroFilledString,
} = require("../../helpers/stringTools");
const { BrandParser } = require("./BrandParser");

class BrandfolderProductAsset {
  constructor(
    BrandfolderAPI,
    BrandfolderAsset,
    SalsifyBufferProducts,
    SalsifyBufferParents,
    SalsifyGlobalBufferProducts
  ) {
    this.BrandfolderAPI = BrandfolderAPI;
    this.BrandfolderAsset = BrandfolderAsset;
    this.SalsifyBufferProducts = SalsifyBufferProducts ?? [];
    this.SalsifyBufferParents = SalsifyBufferParents ?? [];
    this.SalsifyGlobalBufferProducts = SalsifyGlobalBufferProducts ?? [];
    this.customFields = [];
    this.tags = [];
    this.attachments = [];
    this.eans = [];
    this.skus = [];
    this.parentSkus = [];
    this.sizes = [];
    this.colours = [];
    this.laymanColours = [];
    this.collections = [];
    this.section = {};
    this.auOfficeImageStack = BrandfolderAsset.au_office_image_stack;
    this.auOfficeAdditionalOrdering;
    this.brandfolderUpdated;
    this.brandfolderCreated;
    this.name;
    this.description;

    this.tagAttributesToAdd = [
      "barcode_ean",
      "sub_category",
      "family",
      "colour",
      "colour_layman",
      "size_name_longform",
      "country_of_origin",
      "gender",
    ];

    this.salsifySnapshotFields = [
      "COLOUR VARIANT",
      "SIZE VARIANT",
      "COUNTRY OF ORIGIN",
      "PRODUCT CODE",
      "ERP - Tier 1 Financial Category",
      "ERP - Category",
      "ERP - Sub-Category",
      "Brand Internal Product Code",
      "Parent ID",
      "SEASON RELEASED",
      "COLOUR LAYMAN (Generic Colour Group)",
    ];
  }

  decorate() {
    this.getNameAndDescription();
    this.tags = this.getTagsFromSalsifyBufferProducts(
      this.SalsifyBufferProducts,
      this.tagAttributesToAdd,
      this.salsifySnapshotFields
    );
    this.consolidateFields();
    this.customFields = this.compileCustomFieldsForDIST();
  }

  getNameAndDescription() {
    const snapshot = this.SalsifyBufferParents[0]
      ? this.SalsifyBufferParents[0].snapshot
      : this.SalsifyBufferProducts[0].snapshot;
    this.name = snapshot["BASE PRODUCT NAME"];
    this.description = snapshot["SHORT DESCRIPTION"];
  }

  consolidateFields() {
    const eans = new Set();
    const skus = new Set();
    const parentSkus = new Set();
    const sizes = new Set();
    const colours = new Set();
    const laymanColours = new Set();
    for (const SalsifyBufferProduct of this.SalsifyBufferProducts) {
      eans.add(SalsifyBufferProduct.barcode_ean);
      skus.add(SalsifyBufferProduct.sku);
      parentSkus.add(SalsifyBufferProduct.parent_sku);
      sizes.add(SalsifyBufferProduct.size);
      colours.add(SalsifyBufferProduct.colour);
      laymanColours.add(SalsifyBufferProduct.colour_layman);
    }
    this.eans = [...eans];
    this.skus = [...skus];
    this.parentSkus = [...parentSkus];
    this.sizes = [...sizes];
    this.colours = [...colours];
    this.laymanColours = [...laymanColours];
  }

  compileCustomFieldsForDIST() {
    let customFieldsArray = [];

    customFieldsArray.push({
      //id: "xzvbrwj56s5rmgpj33kfxcn",
      key: "AU Office Image Stack",
      value: this.auOfficeImageStack,
    });

    if (this.auOfficeImageStack === "Additional") {
      customFieldsArray.push({
        //id: "87x46xr4f7tbfmtpb2wwgbm",
        key: "AU Office Image Stack Additional ordering",
        value: this.BrandfolderAsset.additionalImageOrdering,
      });
    }

    customFieldsArray.push({
      //id: "q3rhtx83386mvhg5mw6prgf",
      key: "Size",
      value: this.sizes.join(),
    });

    customFieldsArray.push({
      //id: "mpp9gqrk9fs58fbsx8qpx4m9",
      key: "Colour",
      value: this.colours.join(),
    });

    customFieldsArray.push({
      //id: "mb8xcbwt3686h4m5b8xr2vmz",
      key: "Colour Layman",
      value: this.laymanColours.join(),
    });

    customFieldsArray.push({
      //id: "vc33s3r68nxc65fcbvp7b5x",
      key: "EAN",
      value: this.eans.join(),
    });
    customFieldsArray.push({
      //id: "s95w6jwcvh5gwgkhvkr6cp8s",
      key: "SKU",
      value: this.skus.join(),
    });
    customFieldsArray.push({
      //id: "thmf7fsjw4hvngvmp35br52",
      key: "Parent SKU",
      value: this.parentSkus.join(),
    });

    return customFieldsArray;
  }

  compileCustomFieldsForSTS() {
    let customFieldsArray = [];

    // TODO: set up STS BF separately
    // customFieldsArray.push({
    //   id: "sqh2kwbhb2xj7bxjr9bjgmk",
    //   key: "AU Office Image Stack",
    //   value: this.auOfficeImageStack,
    // });

    if (this.GlobalSalsifyProduct) {
      // this means there was a match in Global
      if (this.GlobalSalsifyProduct.us_sku) {
        customFieldsArray.push({
          id: "79r672chp35t774p49x6pw",
          key: "US SKU",
          value: this.GlobalSalsifyProduct.us_sku,
        });
      }
      if (this.GlobalSalsifyProduct.season) {
        customFieldsArray.push({
          id: "xzb3jw8926mj2k7kmn853mk",
          key: "US Season",
          value: this.GlobalSalsifyProduct.season,
        });
      }
      if (this.GlobalSalsifyProduct.country_of_origin) {
        customFieldsArray.push({
          id: "qb2b64hmjk6w362jrq8nhpj",
          key: "Country Of Origin",
          value: this.GlobalSalsifyProduct.country_of_origin,
        });
      }
      if (this.GlobalSalsifyProduct.snapshot["Shape"]) {
        customFieldsArray.push({
          id: "j6b33hfzhsnwmv8q95wkn",
          key: "Shape",
          value: this.GlobalSalsifyProduct.snapshot["Shape"],
        });
      }
      if (this.GlobalSalsifyProduct.snapshot["Size Name (Longform)"]) {
        customFieldsArray.push({
          id: "c9jtpqxmw463665s89ssn9m",
          key: "Size",
          value: this.GlobalSalsifyProduct.snapshot["Size Name (Longform)"],
        });
      }
    }
    if (attributes.au_salsify_snapshot) {
      if (attributes.au_salsify_snapshot.size) {
        if (_.find(customFieldsArray, { key: "Size" })) {
          // do nothing
        } else {
          customFieldsArray.push({
            id: "c9jtpqxmw463665s89ssn9m",
            key: "Size",
            value: attributes.au_salsify_snapshot.size,
          });
        }
      }
      if (attributes.au_salsify_snapshot.colour) {
        if (_.find(customFieldsArray, { key: "Colour" })) {
          // do nothing
        } else {
          customFieldsArray.push({
            id: "rbxwqvhk8j273z5gkccqxzr9",
            key: "Colour",
            value: attributes.au_salsify_snapshot.colour,
          });
        }
      }
      if (attributes.au_salsify_snapshot.colour_layman) {
        customFieldsArray.push({
          id: "vz2fpqxcqjkcjhkcn6cpbq5",
          key: "Colour Layman",
          value: attributes.au_salsify_snapshot
            ? attributes.au_salsify_snapshot.colour_layman
            : null,
        });
      }
    }

    // this means it's a generic product family image,
    // such as sleeping mat valves
    if (valueIsNullOrUndefined(currentAttachment.ean) && currentParentAsset) {
      const filteredCustomFields = currentParentAsset.custom_fields.filter(
        (customFieldEntry) => {
          const exists = valueExistsAndIsNotEmpty(customFieldEntry.attributes);
          return exists;
        }
      );
      const mappedFields = filteredCustomFields.map((customFieldEntry) => {
        let value = customFieldEntry.attributes.value;
        let id;
        switch (customFieldEntry.attributes.key) {
          case "EAN":
            id = "qdlecj-4sgrfk-b5oe3z";
            break;
          case "SKU":
            id = "q7dl8d-28iaqg-bsfu6x";
            break;
          case "Brand":
            id = "xbpmmz5skfjbbgv863qhjnbf";
            break;
          default:
            id = customFieldEntry.id;
            break;
        }
        if (customFieldEntry.attributes.key === "EAN") {
          const arrayOfValues = customFieldEntry.attributes.value.split(",");
          value = arrayOfValues
            .map((value) => {
              return _.toSafeInteger(value);
            })
            .join(",");
        }
        return {
          id,
          key: customFieldEntry.attributes.key,
          value,
        };
      });
      customFieldsArray.push(...mappedFields);
    } else {
      const EAN = this.getEAN();
      customFieldsArray.push({
        // key: "qdlecj-4sgrfk-b5oe3z",
        key: "EAN",
        value: EAN,
      });
      customFieldsArray.push({
        //id: "q7dl8d-28iaqg-bsfu6x",
        key: "SKU",
        value: currentAttachment.sku,
      });
    }

    return customFieldsArray;
  }

  getTagsFromSalsifyBufferProducts(
    salsifyProducts = [],
    arrayOfAttributes = [],
    arrayOfSalsifySnapshotStuff = []
  ) {
    const allTags = new Set();
    for (const product of salsifyProducts) {
      arrayOfAttributes.forEach((attribute) => {
        const value = product[`${attribute}`];
        if (valueExistsAndIsNotEmpty(value)) allTags.add(value);
      });
      const snapshot = product.snapshot;
      arrayOfSalsifySnapshotStuff.forEach((attribute) => {
        const value = snapshot[`${attribute}`];
        if (valueExistsAndIsNotEmpty(value)) allTags.add(value);
      });
    }
    return [...allTags];
  }

  async updateTags() {
    try {
      const results = await this.BrandfolderAPI.addTagsToOneAsset(
        this.BrandfolderAsset.bf_asset_id,
        this.tags
      );
      Logger.info(
        `Updated tags for BF asset ${this.BrandfolderAsset.bf_asset_id}`
      );
    } catch (error) {
      Logger.error(
        `Error with BF asset ${this.name}, id ${this.BrandfolderAsset.bf_asset_id} tag addition: ${error}`
      );
      throw error;
    }
  }

  async updateCustomFields() {
    try {
      const results = await this.BrandfolderAPI.updateCustomFields(
        this.BrandfolderAsset.bf_asset_id,
        this.customFields
      );
      Logger.info(
        `Updated custom fields for BF asset ${this.BrandfolderAsset.bf_asset_id}`
      );
    } catch (error) {
      Logger.error(
        `Error with BF asset ${this.name}, id ${this.BrandfolderAsset.bf_asset_id} custom field: ${error}`
      );
    }
  }

  async updateAssetBasicDetails() {
    const payload = {
      name: this.name,
      description: this.description,
    };

    try {
      const results = await this.BrandfolderAPI.updateBrandfolderAssetDetails(
        this.BrandfolderAsset.bf_asset_id,
        payload
      );
      Logger.info(
        `Updated name and description for BF asset ${this.name}, id ${this.BrandfolderAsset.bf_asset_id}`
      );
    } catch (error) {
      Logger.error(
        `Error with BF asset ${this.name} ${this.BrandfolderAsset.bf_asset_id} details: ${error}`
      );
      throw error;
    }
  }

  hasBeenProcessed() {
    const fieldsToConfirm = [
      "skus",
      "eans",
      "name",
      "description",
      "auOfficeImageStack",
    ];

    for (const field of fieldsToConfirm) {
      if (!valueExistsAndIsNotEmpty(this[`${field}`])) return false;
    }
    return true;
  }

  async moveOutOfProcessing(collectionToMoveTo) {
    // wfrz8z5w79kbqk6hj4pf5b97 is Black Diamond
    // ht7s95whgp3b7txkgf987nt is Product Images section
    const payload = {
      collections: {
        data: [
          {
            id: collectionToMoveTo,
          },
        ],
      },
      section: "ht7s95whgp3b7txkgf987nt",
    };
    await this.BrandfolderAPI.updateBrandfolderAssetDetails(
      this.BrandfolderAsset.bf_asset_id,
      payload
    );
  }
}

module.exports.BrandfolderProductAsset = BrandfolderProductAsset;
