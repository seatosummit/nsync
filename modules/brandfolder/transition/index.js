//
// THIS ENTIRE FOLDER IS RELATED TO THE BRANDFOLDER TRANSITION
// THAT TOOK PLACE DECEMBER 2021 TO FEBRUARY 2022.
// ATTACHMENTS INSIDE ASSETS WERE SEPARATED TO THEIR OWN ASSETS.
// THIS FOLDER IS NO LONGER NEEDED.
// TODO: CLEAN UP THIS FOLDER NEXT TIME THERE'S CLEANUP GOING ON.
//

const { Logger } = require("../../helpers/Logger");
const { BrandfolderConverter } = require("./BrandfolderConverter");
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

const BrandfolderUpdateAttachmentDetails = async (brand, goBackDays) => {
  try {
    const converter = new BrandfolderConverter();
    await converter.updateBufferWithProductSections(brand, goBackDays);
    await converter.addDetailsToAttachments();
    console.log("... done syncing product images to Brandfolder.");
  } catch (error) {
    console.error(
      `Error syncing Black Diamond product images to Brandfolder: ${error}`
    );
  }
};

const BrandfolderTransition = async (brand) => {
  try {
    const converter = new BrandfolderConverter();
    // await converter.refreshAttachmentsBuffer({ onlyCreate: true });
    // await converter.createNewAssetsFromAttachments();
    await converter.addAssetsToLabels();
    // await converter.addAssetsToCollections();
    // await converter.associateAssetsWithTags();
    // await converter.addTagsToAssets(); // TODO: to be deleted
    // await converter.addCustomFieldAssociations();
    console.log("... done creating new assets in Brandfolder.");
  } catch (error) {
    console.error(
      `Error syncing Black Diamond product images to Brandfolder: ${error}`
    );
  }
};

const BrandfolderBufferPrune = async (brand) => {
  try {
    Logger.info(`Pruning BF buffers...`);
    const converter = new BrandfolderConverter();
    await converter.pruneBrandfolderBuffers();
    console.log("... done pruning BF buffers.");
  } catch (error) {
    console.error(`Error pruning BF buffers: ${error}`);
  }
};

const convertNewlyAddedAssets = async (brand) => {
  try {
    Logger.info(`Converting newly added assets...`);
    const converter = new BrandfolderConverter();
    await converter.processNewAssets();
    console.log("... done converting newly added assets.");
  } catch (error) {
    console.error(`Error converting newly added assets: ${error}`);
  }
};

const addLabelsToAssets = async (brand) => {
  try {
    Logger.info(`Adding labels to assets...`);
    const converter = new BrandfolderConverter();
    await converter.processLabels();
    console.log("... done adding labels to assets.");
  } catch (error) {
    console.error(`Error adding labels to assets: ${error}`);
  }
};

const cleanUpSTSDuplicates = async (brand) => {
  try {
    Logger.info(`Cleaning up STS asset duplicates...`);
    const converter = new BrandfolderConverter();
    await converter.cleanUpAssetDuplicates("q7cphz-8wbqqw-1dru9z");
    console.log("... done cleaning up asset duplicates.");
  } catch (error) {
    console.error(`Error cleaning up asset duplicates: ${error}`);
  }
};

module.exports.BrandfolderTransition = BrandfolderTransition;
module.exports.BrandfolderUpdateAttachmentDetails =
  BrandfolderUpdateAttachmentDetails;
module.exports.BrandfolderBufferPrune = BrandfolderBufferPrune;
module.exports.convertNewlyAddedAssets = convertNewlyAddedAssets;
module.exports.cleanUpSTSDuplicates = cleanUpSTSDuplicates;
module.exports.addLabelsToAssets = addLabelsToAssets;
