const _ = require("lodash");
const {
  valueIsNullOrUndefined,
  valueExistsAndIsNotEmpty,
  getAUImageStackFromFilename,
  getAdditionalOrderFromFilename,
} = require("../../helpers/stringTools");
const {
  BrandfolderAsset: BrandfolderBufferAssetDBModel,
  BrandfolderAttachment: BrandfolderBufferAttachmentDBModel,
} = require("../../../models");
const { indexOf } = require("lodash");

class BrandfolderBufferAttachment {
  constructor(
    BrandFolderAttachment,
    BrandfolderParentAsset,
    GlobalSalsifyProduct,
    AUSalsifyProduct,
    AUSalsifyProductParent,
    progressbar
  ) {
    //this.BrandfolderAttachmentDBModel = BrandfolderAttachment;
    this.BrandfolderAttachment = BrandFolderAttachment;
    this.BrandfolderParentAsset = BrandfolderParentAsset;
    this.GlobalSalsifyProduct = GlobalSalsifyProduct;
    this.AUSalsifyProduct = AUSalsifyProduct;
    this.AUSalsifyProductParent = AUSalsifyProductParent;
    this.BrandfolderBufferAssetDBModel = BrandfolderBufferAssetDBModel;
    this.BrandfolderBufferAttachmentDBModel =
      BrandfolderBufferAttachmentDBModel;
    this.brandfolderID = BrandFolderAttachment.attachment_id;
    this.brandfolderParentAssetID = BrandfolderParentAsset.asset_id;
    this.progressbar = progressbar;
    this.attributes = {
      au_salsify_snapshot: {},
      global_salsify_snapshot: {},
      asin: null,
      tags: [], // TODO: generic image attachments need tags from parent
      collections: [],
      categories: [],
      custom_fields: [],
      ean: null,
      shape: null,
      size: null,
      season: null,
      section: "f37sc7fbnsbh3x7578b9rgp", // product transition section as default
    };
    // q7cphz-8wbqqw-c4dg2u = product images
    // tb6mwcswp9ppx3thgqpk336 = 360 product images
  }
  decorate() {
    try {
      if (this.AUSalsifyProduct) {
        this.attributes.asin = this.AUSalsifyProduct.asin_amazon;
        this.attributes.au_salsify_snapshot = this.AUSalsifyProduct;
      }

      if (this.AUSalsifyProductParent) {
        this.attributes.au_salsify_snapshot = this.AUSalsifyProductParent;
      }

      if (this.GlobalSalsifyProduct) {
        this.attributes.season =
          this.GlobalSalsifyProduct.snapshot["Season Released"];
        this.attributes.us_sku = this.GlobalSalsifyProduct.us_sku;
      }
      const stackValue = getAUImageStackFromFilename(
        this.BrandfolderAttachment.filename
      );
      if (stackValue == "Additional") {
        this.attributes.au_office_additional_ordering =
          this.getAdditionalImageOrdering();
      }
      this.attributes.ean = this.getEAN();
      if (this.attributes.ean == 9327868149100) {
        const stophere = true;
      }

      const skuIfThereIsOne = this.getSKU();
      if (
        valueExistsAndIsNotEmpty(skuIfThereIsOne) &&
        this.BrandfolderAttachment.sku !== skuIfThereIsOne
      ) {
        this.attributes.sku = skuIfThereIsOne;
      }
      this.attributes.collections = this.getCollections();
      this.attributes.tags = this.getTags();
      if (stackValue) this.attributes.tags.push(stackValue);
      this.attributes.labels = this.getLabels();
      this.attributes.name = this.getName();
      this.attributes.description = this.getDescription();
      this.attributes.section = this.getSection();
      this.attributes.custom_fields = this.compileCustomFields(
        this.attributes,
        this.BrandfolderAttachment,
        this.BrandfolderParentAsset
      );
    } catch (error) {
      console.log(error);
      // TODO: figure out US image stack variable --> this may just have to be manual work.
    }
  }

  additionalOrderingAsString(number) {
    if (number < 10) {
      return `0${number}`;
    }
    return `${number}`;
  }

  compileCustomFields(attributes, currentAttachment, currentParentAsset) {
    let customFieldsArray = [];

    const AUImageStack = getAUImageStackFromFilename(
      currentAttachment.filename
    );
    if (
      AUImageStack === "Additional" &&
      currentAttachment.au_office_additional_ordering
    ) {
      customFieldsArray.push({
        id: "fr28kt9qh5764j5tqxn844q",
        name: "AU Office Image Stack Additional ordering",
        value: this.additionalOrderingAsString(
          currentAttachment.au_office_additional_ordering
        ),
      });
    }

    customFieldsArray.push({
      id: "sqh2kwbhb2xj7bxjr9bjgmk",
      name: "AU Office Image Stack",
      value: AUImageStack,
    });

    if (this.GlobalSalsifyProduct) {
      // this means there was a match in Global
      if (this.GlobalSalsifyProduct.us_sku) {
        customFieldsArray.push({
          id: "79r672chp35t774p49x6pw",
          name: "US SKU",
          value: this.GlobalSalsifyProduct.us_sku,
        });
      }
      if (this.GlobalSalsifyProduct.season) {
        customFieldsArray.push({
          id: "xzb3jw8926mj2k7kmn853mk",
          name: "US Season",
          value: this.GlobalSalsifyProduct.season,
        });
      }
      if (this.GlobalSalsifyProduct.country_of_origin) {
        customFieldsArray.push({
          id: "qb2b64hmjk6w362jrq8nhpj",
          name: "Country Of Origin",
          value: this.GlobalSalsifyProduct.country_of_origin,
        });
      }
      if (this.GlobalSalsifyProduct.snapshot["Shape"]) {
        customFieldsArray.push({
          id: "j6b33hfzhsnwmv8q95wkn",
          name: "Shape",
          value: this.GlobalSalsifyProduct.snapshot["Shape"],
        });
      }
      if (this.GlobalSalsifyProduct.snapshot["Size Name (Longform)"]) {
        customFieldsArray.push({
          id: "c9jtpqxmw463665s89ssn9m",
          name: "Size",
          value: this.GlobalSalsifyProduct.snapshot["Size Name (Longform)"],
        });
      }
    }
    if (attributes.au_salsify_snapshot) {
      if (attributes.au_salsify_snapshot.size) {
        if (_.find(customFieldsArray, { name: "Size" })) {
          // do nothing
        } else {
          customFieldsArray.push({
            id: "c9jtpqxmw463665s89ssn9m",
            name: "Size",
            value: attributes.au_salsify_snapshot.size,
          });
        }
      }
      if (attributes.au_salsify_snapshot.colour) {
        if (_.find(customFieldsArray, { name: "Colour" })) {
          // do nothing
        } else {
          customFieldsArray.push({
            id: "rbxwqvhk8j273z5gkccqxzr9",
            name: "Colour",
            value: attributes.au_salsify_snapshot.colour,
          });
        }
      }
      if (attributes.au_salsify_snapshot.colour_layman) {
        customFieldsArray.push({
          id: "vz2fpqxcqjkcjhkcn6cpbq5",
          name: "Colour Layman",
          value: attributes.au_salsify_snapshot
            ? attributes.au_salsify_snapshot.colour_layman
            : null,
        });
      }
    }

    // this means it's a generic product family image,
    // such as sleeping mat valves
    if (valueIsNullOrUndefined(currentAttachment.ean) && currentParentAsset) {
      const filteredCustomFields = currentParentAsset.custom_fields.filter(
        (customFieldEntry) => {
          const exists = valueExistsAndIsNotEmpty(customFieldEntry.attributes);
          return exists;
        }
      );
      const mappedFields = filteredCustomFields.map((customFieldEntry) => {
        let value = customFieldEntry.attributes.value;
        let id;
        switch (customFieldEntry.attributes.key) {
          case "EAN":
            id = "qdlecj-4sgrfk-b5oe3z";
            break;
          case "SKU":
            id = "q7dl8d-28iaqg-bsfu6x";
            break;
          case "Brand":
            id = "xbpmmz5skfjbbgv863qhjnbf";
            break;
          default:
            id = customFieldEntry.id;
            break;
        }
        if (customFieldEntry.attributes.key === "EAN") {
          const arrayOfValues = customFieldEntry.attributes.value.split(",");
          value = arrayOfValues
            .map((value) => {
              return _.toSafeInteger(value);
            })
            .join(",");
        }
        return {
          id,
          name: customFieldEntry.attributes.key,
          value,
        };
      });
      customFieldsArray.push(...mappedFields);
    } else {
      const EAN = this.getEAN();
      customFieldsArray.push({
        id: "qdlecj-4sgrfk-b5oe3z",
        name: "EAN",
        value: EAN,
      });
      customFieldsArray.push({
        id: "q7dl8d-28iaqg-bsfu6x",
        name: "SKU",
        value: currentAttachment.sku,
      });
    }

    return customFieldsArray;
  }

  getEAN() {
    let ean = null;
    if (this.AUSalsifyProduct) {
      if (valueExistsAndIsNotEmpty(this.AUSalsifyProduct.barcode_ean))
        return _.toSafeInteger(this.AUSalsifyProduct.barcode_ean).toString();
    }
    if (this.GlobalSalsifyProduct) {
      if (valueExistsAndIsNotEmpty(this.GlobalSalsifyProduct.ean))
        return _.toSafeInteger(this.GlobalSalsifyProduct.ean).toString();
    }
    return ean;
  }

  getSKU() {
    const sku = this.AUSalsifyProduct?.sku ?? null;
    return sku;
  }

  checkFilenameForAdditionalImage(filename) {
    const additionalCheck = /[-_](0[23456789])[_-]*\d*\.[jp]/g;
    const additionalCheckResults = additionalCheck.exec(filename);
    return additionalCheckResults;
  }

  getAdditionalImageOrdering() {
    let orderNumber = getAdditionalOrderFromFilename(
      this.BrandfolderAttachment.filename
    );

    if (orderNumber) return orderNumber;

    if (
      valueExistsAndIsNotEmpty(this.BrandfolderAttachment.au_salsify_snapshot)
    ) {
      let foundSalsifyID;
      const digitalAssetsArray =
        this.BrandfolderAttachment.au_salsify_snapshot.digital_assets;
      if (!valueExistsAndIsNotEmpty(digitalAssetsArray)) return orderNumber;

      for (const digitalAsset of digitalAssetsArray) {
        if (
          digitalAsset["salsify:filename"].includes(
            this.BrandfolderAttachment.filename.slice(0, -4)
          )
        ) {
          foundSalsifyID = digitalAsset["salsify:id"];
        }
      }

      if (!valueExistsAndIsNotEmpty(foundSalsifyID)) return orderNumber;

      const foundItemIndex = indexOf(
        this.BrandfolderAttachment.au_salsify_snapshot.additional_images,
        foundSalsifyID
      );
      if (foundItemIndex > -1) {
        if (foundItemIndex < 11) {
          return `${foundItemIndex + 1}`;
        } else {
          return `0${foundItemIndex + 1}`;
        }
      }
    }
    return orderNumber;
  }

  getName() {
    let name = "No name set";
    if (this.AUSalsifyProduct) {
      if (valueExistsAndIsNotEmpty(this.AUSalsifyProduct.name))
        return this.AUSalsifyProduct.name;
    }
    if (this.AUSalsifyProductParent) {
      if (valueExistsAndIsNotEmpty(this.AUSalsifyProductParent.name))
        return this.AUSalsifyProductParent.name;
    }
    if (this.GlobalSalsifyProduct) {
      if (valueExistsAndIsNotEmpty(this.GlobalSalsifyProduct.primary_name))
        return this.GlobalSalsifyProduct.primary_name;
    }
    if (this.BrandfolderParentAsset) {
      if (valueExistsAndIsNotEmpty(this.BrandfolderParentAsset.name))
        return this.BrandfolderParentAsset.name;
    }
    return name;
  }

  getCollections() {
    let collections = [];
    if (this.BrandfolderParentAsset) {
      collections = valueExistsAndIsNotEmpty(
        this.BrandfolderParentAsset.collections
      )
        ? this.BrandfolderParentAsset.collections.map((collectionEntry) => {
            return {
              id: collectionEntry.id,
              name: collectionEntry.attributes
                ? collectionEntry.attributes.name
                : "No name",
            };
          })
        : [];
    }
    return collections;
  }

  getTags() {
    let tags = new Set();
    if (this.BrandfolderParentAsset) {
      const parentTags = valueExistsAndIsNotEmpty(
        this.BrandfolderParentAsset.tags
      )
        ? this.BrandfolderParentAsset.tags
            .filter((tagEntry) => {
              if (valueExistsAndIsNotEmpty(tagEntry.attributes)) return true;
              if (valueExistsAndIsNotEmpty(tagEntry.name)) return true;
              return false;
            })
            .map((tagEntry) => {
              if (tagEntry.name) return tagEntry.name;
              if (tagEntry.attributes) return tagEntry.attributes.name;
            })
        : [];
      parentTags.forEach((item) => tags.add(item));
    }
    if (this.GlobalSalsifyProduct) {
      const tagAttributesToAdd = [
        "barcode_ean",
        "sub_category",
        "family",
        "colour",
        "colour_layman",
        "size_name_longform",
        "country_of_origin",
        "gender",
      ];

      for (const tagAttribute of tagAttributesToAdd) {
        const globalSalsifyValue = this.GlobalSalsifyProduct[`${tagAttribute}`];
        if (valueIsNullOrUndefined(globalSalsifyValue)) continue;
        tags.add(globalSalsifyValue);
      }
    }

    if (this.AUSalsifyProduct) {
      const tagAttributesToAdd = [
        "erp_tier1_financial_category",
        "erp_category",
        "erp_sub_category",
        "b2b_category",
        "b2b_sub_category",
        "colour",
        "colour_layman",
        "gender",
        "parent_sku",
        "sku",
        "size",
      ];

      for (const tagAttribute of tagAttributesToAdd) {
        const AUSalsifyValue = this.AUSalsifyProduct[`${tagAttribute}`];
        if (valueIsNullOrUndefined(AUSalsifyValue)) continue;
        tags.add(AUSalsifyValue);
      }
    }
    return Array.from(tags);
  }

  getDescription() {
    let description = "No description set";
    if (this.AUSalsifyProduct) {
      if (
        valueExistsAndIsNotEmpty(
          this.AUSalsifyProduct.snapshot["SHORT DESCRIPTION"]
        )
      )
        return this.AUSalsifyProduct.snapshot["SHORT DESCRIPTION"];
    }
    if (this.AUSalsifyProductParent) {
      if (
        valueExistsAndIsNotEmpty(
          this.AUSalsifyProductParent.snapshot["SHORT DESCRIPTION"]
        )
      )
        return this.AUSalsifyProductParent.snapshot["SHORT DESCRIPTION"];
    }
    if (this.GlobalSalsifyProduct) {
      if (
        valueExistsAndIsNotEmpty(
          this.GlobalSalsifyProduct.snapshot["Short/Lead Marketing Description"]
        )
      )
        return;
      this.GlobalSalsifyProduct.snapshot["Short/Lead Marketing Description"];
    }
    return description;
  }

  getSection() {
    let section = "f37sc7fbnsbh3x7578b9rgp"; // product transition section for now
    if (this.BrandfolderParentAsset) {
      if (valueExistsAndIsNotEmpty(this.BrandfolderParentAsset.section)) {
        section = this.BrandfolderParentAsset.section.id;
      }
    }

    return section;
  }

  getLabels() {
    let labels = [];
    if (this.BrandfolderParentAsset) {
      labels = valueExistsAndIsNotEmpty(this.BrandfolderParentAsset.labels)
        ? this.BrandfolderParentAsset.labels
            .filter((labelEntry) =>
              valueExistsAndIsNotEmpty(labelEntry.attributes)
            )
            .map((labelEntry) => {
              return {
                id: labelEntry.id,
                name: labelEntry.attributes.name,
              };
            })
        : [];
    }
    return labels;
  }

  async upsertPromise() {
    try {
      const existingAttachment =
        await this.BrandfolderBufferAttachmentDBModel.findOne({
          where: {
            attachment_id: this.brandfolderID,
          },
        });

      if (existingAttachment) {
        // console.log(`updating Attachment ${this.brandfolderID}`);
        this.progressbar.increment();
        return existingAttachment.update(this.attributes);
      } else {
        throw new Error(
          `Attachment ${this.brandfolderID} not found. This should not happen.`
        );
      }
    } catch (error) {
      console.log(`Error updating BF attachment ${this.brandfolderID}`);
      console.log(error);
    }
  }
}

module.exports.BrandfolderBufferAttachment = BrandfolderBufferAttachment;
