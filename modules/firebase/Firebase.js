// Import the functions you need from the SDKs you need
const {
  initializeApp,
  applicationDefault,
  cert,
} = require("firebase-admin/app");
const { getDatabase, ref, set, onValue } = require("firebase/database");
const {
  getFirestore,
  Timestamp,
  FieldValue,
} = require("firebase-admin/firestore");
const firebaseConfig = require("../../config/firebase/config.json");
const firebaseAdminConfig = require("../../config/firebase/adminConfig.json");
const {
  getAuth,
  signInWithCustomToken,
  signInWithEmailAndPassword,
} = require("firebase/auth");

// SDKs for Firebase products that you want to use are at
// https://firebase.google.com/docs/web/setup#available-libraries

class Firebase {
  constructor() {
    this.app = initializeApp({
      credential: cert(firebaseAdminConfig),
    });
    //this.database = getDatabase();
    //this.auth = getAuth();
    this.firestore = getFirestore();
  }

  async login() {
    return Promise.resolve(true);
    // this.user = await signInWithCustomToken(
    //   this.auth,
    //   "e5VIZ8CDTONSyQTwPNWqSiAb6pB2"
    // );
    // this.user = await signInWithEmailAndPassword(
    //   this.auth,
    //   firebaseConfig.username,
    //   firebaseConfig.password
    // );
  }

  async firestoreProductWritePromise(
    payload = { sku: "empty" },
    collection = "shopify-products"
  ) {
    const documentReference = this.firestore
      .collection(collection)
      .doc(payload.sku);
    return await documentReference.set(payload);
  }

  async firestoreProductVariantWritePromise(
    payload = { sku: "empty" },
    collection = "shopify-product-variants"
  ) {
    const documentReference = this.firestore
      .collection(collection)
      .doc(payload.sku);
    return await documentReference.set(payload);
  }

  async getAllVariants(collection = "shopify-product-variants") {
    const snapshot = this.firestore.collection(collection).get();
    return snapshot;
  }

  async shopifyProductWritePromise(
    payload = { sku: "empty" },
    collection = "shopify-products"
  ) {
    return set(ref(this.database, `${collection}/${payload.sku}`), payload);
  }
}

module.exports.Firebase = Firebase;
