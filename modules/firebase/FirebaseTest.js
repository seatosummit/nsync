const { Firebase } = require("./Firebase");

const FirebaseTest = async () => {
  try {
    console.log("Testing Firebase...");
    //const firebase = new Firebase();
    let addThese = [];
    const skusToUpdate = [
      { sku: "BDABC1", title: "First" },
      { sku: "BDDEF2", title: "Second" },
    ];
    for (const iterator of skusToUpdate) {
      addThese.push(firebase.shopifyProductWritePromise(iterator));
    }
    await Promise.all(addThese);
    console.log("... done testing Firebase.");
  } catch (error) {
    console.error(`Error testing Firebase: ${error}`);
  }
};

module.exports.FirebaseTest = FirebaseTest;
