const fetch = require("node-fetch");
const { prepareHeaders } = require("../../helpers/apitools");

// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

module.exports = {
  getProductsFromWMS: async (headers, filter = true, view) => {
    try {
      const fetchHeaders = headers ? headers : await prepareHeaders();
      let SQLView = filter
        ? "vSTS_WebProductInfo_PEP"
        : "vSTS_WebProductInfo_PEP_V2";

      if (view) SQLView = view;
      console.log(`Getting products from WMS using view ${SQLView}`);

      const response = await fetch(
        `http://${process.env.WMS_API_LOCATION}/json/sql?filter~SELECT%20*%20FROM%20${SQLView}`,
        { headers: fetchHeaders.WMSHeaders, timeout: 0 }
      );
      if (response.status !== 200) {
        throw `response other than 200 - ${response.status}: ${response.statusText}`;
      }

      const responseJSON = await response.json();

      if (Array.isArray(responseJSON) && responseJSON.length === 0) {
        throw `Empty resultset from WMS query: ${SQLView}`;
      }

      if (!responseJSON) {
        throw `No results WMS query: ${SQLView}`;
      }

      return responseJSON;
    } catch (error) {
      console.error(`Error fetching products from WMS: ${error}`);
      return {
        status: 500,
        message: error,
      };
    }
  },
};
