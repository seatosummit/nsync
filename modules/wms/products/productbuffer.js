const _ = require("lodash");
require("dotenv").config();
const {
  WMSProduct,
  SalesChannelProductEnable,
  SalsifyProduct,
} = require("../../../models");
const { SequelizeOperations } = require("../../sequelize/SequelizeOperations");
const { getProductsFromWMS } = require("../../wms/products/products");
const { WMSToWMSBuffer } = require("./productdecorator");
const { Logger } = require("../../helpers/Logger");
const Sequelize = require("sequelize");
const { currentMemoryUsage } = require("../../helpers/memoryTools");
const Op = Sequelize.Op;

module.exports = {
  updateWMSProductBuffer: async (args) => {
    const filterOnlyActiveProducts = args?.filterOnlyActiveProducts ?? false;
    try {
      const WMSProductBuffer = await WMSProduct.findAll({
        attributes: ["id", "sku"],
        include: SalsifyProduct,
      });
      const startMemory = Math.round(
        process.memoryUsage().heapUsed / 1024 / 1024
      );
      Logger.info(`Memory used after getting stuff from DB: ${startMemory} MB`);
      const allWMSProducts = await getProductsFromWMS(
        null,
        filterOnlyActiveProducts
      );
      if (allWMSProducts.status === 500) {
        throw `Error getting WMS products: ${allWMSProducts.message}`;
      }
      if (Array.isArray(allWMSProducts) && allWMSProducts.length === 0) {
        throw `Empty resultset from WMS query: ${allWMSProducts.message}`;
      }
      if (!allWMSProducts) {
        throw `No WMS products returned: ${allWMSProducts.message}`;
      }

      Logger.info(`Found ${allWMSProducts.length} products from WMS`);

      const filteredBrandWMSProducts = allWMSProducts.filter((product) => {
        return true;
      });

      const decoratedWMSBufferProducts = filteredBrandWMSProducts.map(
        (product) => {
          return WMSToWMSBuffer(product);
        }
      );

      const productSavePromises = decoratedWMSBufferProducts.map(
        function (decoratedProduct) {
          return SequelizeOperations.Upsert(
            decoratedProduct,
            "sku",
            WMSProduct,
            this.WMSProductBuffer
          );
        },
        { WMSProductBuffer }
      );

      const upsertResults = await Promise.all(productSavePromises);
      const actuallyUpdated = upsertResults.filter(
        (result) => result !== false
      );
      Logger.info(
        `... finished updating WMS buffer for ${actuallyUpdated.length} products.`
      );

      if (filterOnlyActiveProducts == false) {
        await module.exports.removeRedundantProducts(allWMSProducts);
      }
      const usedMemory = Math.round(
        process.memoryUsage().heapUsed / 1024 / 1024
      );
      Logger.info(`Memory used: ${usedMemory}`);
    } catch (error) {
      Logger.error(`Error syncing products from WMS to buffer, ${error}`);
      return;
    }
  },

  updateWMSProductBufferUsingChunks: async (args) => {
    const filterOnlyActiveProducts = args?.filterOnlyActiveProducts ?? false;
    const chunkSize = args?.chunkSize ?? 30;
    try {
      const WMSProductBuffer = await WMSProduct.findAll({
        attributes: ["id", "sku"],
      });
      const startMemory = Math.round(
        process.memoryUsage().heapUsed / 1024 / 1024
      );
      Logger.info(`Memory used after getting stuff from DB: ${startMemory} MB`);
      const allWMSProducts = await getProductsFromWMS(
        null,
        filterOnlyActiveProducts
      );
      if (allWMSProducts.status === 500) {
        throw `Error getting WMS products: ${allWMSProducts.message}`;
      }
      if (Array.isArray(allWMSProducts) && allWMSProducts.length === 0) {
        throw `Empty resultset from WMS query: ${allWMSProducts.message}`;
      }
      if (!allWMSProducts) {
        throw `No WMS products returned: ${allWMSProducts.message}`;
      }

      Logger.info(`Found ${allWMSProducts.length} products from WMS`);

      const filteredBrandWMSProducts = allWMSProducts.filter((product) => {
        return true;
      });
      const chunks = _.chunk(filteredBrandWMSProducts, chunkSize);

      for (const chunk of chunks) {
        try {
          const promisesArray = [];
          for (const product of chunk) {
            const decoratedProduct = WMSToWMSBuffer(product);
            promisesArray.push(
              SequelizeOperations.Upsert(
                decoratedProduct,
                "sku",
                WMSProduct,
                WMSProductBuffer
              )
            );
          }
          await Promise.all(promisesArray);
          currentMemoryUsage(`While running Promise.all`);
        } catch (error) {
          Logger.error(`Problem with saving WMS products: ${error}`);
        }
      }

      if (filterOnlyActiveProducts == false) {
        await module.exports.removeRedundantProducts(allWMSProducts);
      }
      currentMemoryUsage(`After running all operations`);
    } catch (error) {
      Logger.error(`Error syncing products from WMS to buffer, ${error}`);
      return;
    }
  },

  removeRedundantProducts: async (allWMSProducts) => {
    try {
      if (!Array.isArray(allWMSProducts)) throw `allWMSProducts not an array`;
      if (allWMSProducts.length === 0) throw `allWMSProducts is an empty array`;
      if (
        !allWMSProducts ||
        allWMSProducts === undefined ||
        allWMSProducts == null
      )
        throw `allWMSProducts is broken`;

      const currentWMSProductBuffer = await WMSProduct.findAll({ raw: true });
      const redundantBufferProductsToRemove = currentWMSProductBuffer.filter(
        function (currentItem) {
          const foundMatchingSKU = _.find(this, { sku: currentItem.sku });
          if (foundMatchingSKU) return false;
          return true;
        },
        allWMSProducts
      );
      const productDeletePromises = redundantBufferProductsToRemove.map(
        (product) => {
          return module.exports.removeWMSProductBufferEntry(product);
        }
      );
      const deleteResults = await Promise.all(productDeletePromises);
      Logger.info(
        `... finished removing ${deleteResults.length} redundant entries from WMS buffer.`
      );
    } catch (error) {
      throw error;
    }
  },

  saveProductEnableEntries: async (
    productObject,
    channel_enum = "PEPPERI_APP"
  ) => {
    const foundEnableEntries = await SalesChannelProductEnable.findAll({
      where: {
        sku: productObject.sku,
        channel_enum: channel_enum,
      },
    });

    if (foundEnableEntries.length === 0) {
      await SalesChannelProductEnable.create(productEnable);
    } else {
      const updateResultDirect = await foundEnableEntries[0].update(
        productEnable
      );
      if (updateResultDirect instanceof SalesChannelProductEnable) {
        //
      } else {
        Logger.error(
          `Problem ProductEnable buffer entry for ${productObject.sku} (${productObject.name})`
        );
      }
    }
  },

  removeWMSProductBufferEntry: async (productObject) => {
    try {
      const foundProductToDelete = await WMSProduct.findAll({
        where: {
          sku: productObject.sku,
        },
      });
      if (foundProductToDelete.length === 0) {
        throw `Product ${productObject.sku} wasn't found when trying to delete.`;
      } else {
        const updateResultDirect = await foundProductToDelete[0].destroy();
        Logger.info(`Deleted product ${productObject.sku}.`);
        return updateResultDirect;
      }
    } catch (error) {
      Logger.error(`Error deleting ${productObject.sku}: ${error}`);
    }
  },

  getERPBufferProductsUsingFilters: async (args) => {
    const excludeZCoded = args?.excludeZCoded ?? true;
    const onlyEnabled = args?.onlyEnabled ?? false;
    const excludeSnapshot = args?.excludeSnapshot ?? true;
    const includeFields = args?.includeFields;
    const queryForWMSProducts = {
      where: {},
    };

    if (includeFields) {
      queryForWMSProducts.attributes = { include: includeFields };
    }

    if (excludeSnapshot && !includeFields) {
      queryForWMSProducts.attributes = { exclude: ["productSnapshot"] };
    }

    if (onlyEnabled) {
      queryForWMSProducts.where = {
        [Op.or]: [{ b2b_enabled: true }, { b2c_enabled: true }],
      };
    }

    if (excludeZCoded) {
      queryForWMSProducts.where.sku = {
        [Op.notRegexp]: "^[Zz]+",
      };
    }

    try {
      const erpProductVariants = await WMSProduct.findAll(queryForWMSProducts);
      return erpProductVariants;
    } catch (error) {
      Logger.error(`Problem fetching products: ${error}`);
    }
  },
};
