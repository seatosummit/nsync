const fetch = require("node-fetch");
const _ = require("lodash");
const moment = require("moment");
require("dotenv").config();
const SalesChannels = require("../../helpers/salesChannelEnums");
const { NumberTools } = require("../../helpers/NumberTools");

module.exports = {
  WMSToWMSBuffer: (productObject) => {
    const stockETA =
      productObject["Promise Date"] != ""
        ? moment(productObject["Promise Date"], "DD/MM/YYYY hh:mm:ss").format(
            "YYYY-MM-DD hh:mm:ss"
          )
        : null;
    const retailPrice = NumberTools.priceToShopifyFormat(
      productObject["customfield3"]
    );
    const WMSBufferProduct = {
      WMSProductID: Number.parseInt(productObject["ProductID"]),
      sku: productObject["sku"],
      name: productObject["name"],
      unitPrice: isNaN(productObject["unitprice"])
        ? null
        : NumberTools.priceToShopifyFormat(productObject["unitprice"]),
      category: productObject["category"],
      brand: productObject["brand"],
      barcode: productObject["customfield1"],
      retailPrice,
      perUnitQuantity: Number.parseFloat(productObject["PerUnitQty"]),
      stockQuantity:
        productObject["stockqty"] == ""
          ? null
          : Number.parseFloat(productObject["stockqty"]),
      unitOfMeasure: productObject["UOM"],
      pepperiEnabled: productObject["PepperiEnabled"] === "Y",
      b2b_enabled: productObject["b2b_enabled"] === "1",
      b2c_enabled: productObject["b2c_enabled"] === "1",
      status: productObject["status"],
      stockETA,
      productSnapshot: productObject,
    };
    return WMSBufferProduct;
  },

  WMSToProductVariant: (productObject) => {
    const stockETA =
      productObject["Promise Date"] != ""
        ? moment(productObject["Promise Date"], "DD/MM/YYYY hh:mm:ss").format(
            "YYYY-MM-DD hh:mm:ss"
          )
        : null;
    const retailPrice = NumberTools.priceToShopifyFormat(
      productObject["customfield3"]
    );
    const WMSBufferProduct = {
      erpProductId: productObject["ProductID"],
      sku: productObject["sku"],
      name: productObject["name"],
      unitPrice: isNaN(productObject["unitprice"])
        ? null
        : NumberTools.priceToShopifyFormat(productObject["unitprice"]),
      category: productObject["category"],
      ean: productObject["customfield1"],
      perUnitQuantity: Number.parseFloat(productObject["PerUnitQty"]),
      stockQuantity:
        productObject["stockqty"] == ""
          ? null
          : Number.parseFloat(productObject["stockqty"]),
      unitOfMeasure: productObject["UOM"],
      pepperiEnabled: productObject["PepperiEnabled"] == "Y" ? true : false,
      stockETA,
      productSnapshot: productObject,
    };
    return WMSBufferProduct;
  },

  WMSToSalesChannelProductEnable: (productObject, channel_enum) => {
    let enabledForChannel;

    switch (channel_enum) {
      case "PEPPERI_APP":
        enabledForChannel =
          productObject["PepperiEnabled"] == "Y" ? true : false;
        break;
      case "B2B_DIST":
        enabledForChannel = productObject["b2b_enabled"] == "1" ? true : false;
        break;
      case "SEA_TO_SUMMIT":
        enabledForChannel = productObject["b2c_enabled"] == "1" ? true : false;
        break;
      case "HELINOX":
        enabledForChannel = productObject["b2c_enabled"] == "1" ? true : false;
        break;
      case "B2C_DIST":
        enabledForChannel = productObject["b2c_enabled"] == "1" ? true : false;
        break;
      default:
        enabledForChannel = false;
        break;
    }

    const SalesChannelProductEnable = {
      sku: productObject.sku,
      channel_enum: channel_enum,
      enabled: enabledForChannel,
    };

    return SalesChannelProductEnable;
  },
};
