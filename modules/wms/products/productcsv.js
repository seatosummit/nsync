const createCsvWriter = require("csv-writer").createObjectCsvWriter;
const fetch = require("node-fetch");
const _ = require("lodash");
const Shopify = require("shopify-api-node");
require("dotenv").config();
const { promisify } = require("util");
const { DateTime } = require("luxon");
const sleep = promisify(setTimeout);
const { prepareHeaders } = require("../../helpers/apitools");
const {
  SalsifyProduct,
  WMSProduct,
  SalsifyProductParent,
  HelinoxBufferProduct,
} = require("../../../models");
const {
  mergeSalsifyAndWMSProductsForShopify,
  shopifyBufferProductFromVariant,
  shopifyBufferParentProductFromSalsify,
} = require("./productdecorator");
const { skuIsZCoded } = require("../../helpers/stringTools");
const { selectFromView } = require("../../helpers/WMSBreezeAPI");
const { Logger } = require("../../helpers/Logger");

module.exports = {
  createSalsifyOrphansCSV: async (salsifyFilter, WMSFilter) => {
    try {
      let allWMSBufferProducts;
      let allSalsifyBufferProducts;
      if (WMSFilter) {
        allWMSBufferProducts = await WMSProduct.findAll({
          where: {
            brand: WMSFilter,
          },
        });
      } else {
        allWMSBufferProducts = await WMSProduct.findAll();
      }

      if (salsifyFilter) {
        allSalsifyBufferProducts = await SalsifyProduct.findAll({
          where: {
            brand: salsifyFilter,
          },
        });
      } else {
        allSalsifyBufferProducts = await SalsifyProduct.findAll();
      }

      const zCodeRegex = /\b[Zz]+/gm;

      const allWMSBufferProductsExceptZCoded = allWMSBufferProducts.filter(
        (product) => {
          return product.sku.match(zCodeRegex) === null;
        }
      );

      let salsifyOrphans = [];

      for (const salsifyBufferProduct of allSalsifyBufferProducts) {
        const foundProduct = _.find(allWMSBufferProductsExceptZCoded, {
          sku: salsifyBufferProduct.sku,
        });

        if (!foundProduct) {
          const salsifyOrphan = {
            name: salsifyBufferProduct.name
              ? salsifyBufferProduct.name
              : "Salsify name undefined",
            sku: salsifyBufferProduct.sku
              ? salsifyBufferProduct.sku
              : "SKU not set",
            brand: salsifyBufferProduct.brand
              ? salsifyBufferProduct.brand
              : "Brand not set",
            parent_sku: salsifyBufferProduct.parent_sku
              ? salsifyBufferProduct.parent_sku
              : "Parent SKU undefined",
          };
          salsifyOrphans.push(salsifyOrphan);
        }
      }

      const csvWriter = createCsvWriter({
        path: "./salsifyOrphans.csv",
        header: [
          { id: "name", title: "Name" },
          { id: "sku", title: "SKU" },
          { id: "brand", title: "Brand" },
          { id: "parent_sku", title: "Parent SKU" },
        ],
      });

      const records = [...salsifyOrphans];

      await csvWriter.writeRecords(records);
      // console.log("...done.");
    } catch (error) {
      console.error(`Error: ${error}`);
    }
  },

  reportOfDiscontinuedProducts: async () => {
    const allWMSProducts = await selectFromView("vSTS_WebProductInfo_PEP_V2");
    const nonZCodedProducts = allWMSProducts.filter((product) => {
      return !skuIsZCoded(product.sku);
    });

    const discontinuedProducts = nonZCodedProducts.filter((product) => {
      return product.status === "DISCONTINUED";
    });

    const inventoryZero = discontinuedProducts.filter((product) => {
      const hasZeroInventory = parseInt(product.stockqty) === 0;
      const noFutureDeliveryDate = product["Promise Date"] === "";
      return hasZeroInventory && noFutureDeliveryDate;
    });

    const pepperiEnabled = inventoryZero.filter((product) => {
      return product["PepperiEnabled"] === "Y";
    });

    const CSVRows = pepperiEnabled.map((product) => {
      return {
        sku: product.sku,
        name: product.name,
        brand: product.brand,
        category: product.category,
        b2b_enabled: product.b2b_enabled === "1" ? "Yes" : "No",
        b2c_enabled: product.b2c_enabled === "1" ? "Yes" : "No",
      };
    });

    const today = DateTime.now().toFormat("yyyy_LLL_dd");

    const csvWriter = createCsvWriter({
      path: `./csv/Discontinued_WMS_products_with_zero_stock_pepperi_enabled_${today}.csv`,
      header: [
        { id: "name", title: "Name" },
        { id: "sku", title: "SKU" },
        { id: "brand", title: "WMS Brand" },
        { id: "category", title: "WMS Category" },
        { id: "b2b_enabled", title: "WMS B2B Enabled" },
        { id: "b2c_enabled", title: "WMS B2C Enabled" },
      ],
    });

    const records = [...CSVRows];
    await csvWriter.writeRecords(records);
    Logger.info(`Wrote CSV with ${records.length} lines.`);
  },

  createWMSProductListCSV: async () => {
    try {
      const allWMSBufferProducts = await WMSProduct.findAll();
      const allSalsifyBufferProducts = await SalsifyProduct.findAll();

      const allWMSBufferProductsExceptZCoded = allWMSBufferProducts.filter(
        (product) => {
          return !skuIsZCoded(product);
        }
      );

      const formattedWMSProducts = allWMSBufferProductsExceptZCoded.map(
        function (product) {
          const foundProduct = _.find(this, { sku: product.sku }) !== undefined;

          const b2b_enabled =
            product.productSnapshot.b2b_enabled === "1" ? "Yes" : "No";
          const b2c_enabled =
            product.productSnapshot.b2c_enabled === "1" ? "Yes" : "No";

          const formattedProduct = {
            name: product.name,
            sku: product.sku,
            brand: product.brand,
            category: product.category,
            status: product.productSnapshot.status,
            b2b_enabled,
            b2c_enabled,
            barcode: product.productSnapshot.customfield1,
            found_in_salsify: foundProduct,
          };
          return formattedProduct;
        },
        allSalsifyBufferProducts
      );

      const csvWriter = createCsvWriter({
        path: "./file.csv",
        header: [
          { id: "name", title: "Name" },
          { id: "sku", title: "SKU" },
          { id: "brand", title: "WMS Brand" },
          { id: "category", title: "WMS Category" },
          { id: "status", title: "WMS Status" },
          { id: "b2b_enabled", title: "WMS B2B Enabled" },
          { id: "b2c_enabled", title: "WMS B2C Enabled" },
          { id: "barcode", title: "WMS Barcode" },
          { id: "found_in_salsify", title: "Found In Salsify?" },
        ],
      });

      const records = [...formattedWMSProducts];

      await csvWriter.writeRecords(records);
      // console.log("...done.");
    } catch (error) {
      console.error(`Error: ${error}`);
    }
  },
};
