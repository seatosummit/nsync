const _ = require("lodash");
require("dotenv").config();
const { SalesChannelProductEnable } = require("../../../models");
const { getProductsFromWMS } = require("../../wms/products/products");
const { WMSToSalesChannelProductEnable } = require("./productdecorator");
const SalesChannels = require("../../helpers/salesChannelEnums");

// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

module.exports = {
  updateSalesChannelProductEnables: async () => {
    // This function needs an "acquire": 120000 pool setting in config to avoid timeouts.

    try {
      // TODO: would be easier to get enables/disables
      // for each sku for each channel, from just one view

      const pepperiProducts = await getProductsFromWMS(
        null,
        false,
        SalesChannels.PEPPERI_APP.SQLView
      );
      const distProducts = await getProductsFromWMS(
        null,
        false,
        SalesChannels.B2B_DIST.SQLView
      );
      const helinoxProducts = await getProductsFromWMS(
        null,
        false,
        SalesChannels.HELINOX.SQLView
      );

      const stsProductsResults = await getProductsFromWMS(
        null,
        false,
        SalesChannels.SEA_TO_SUMMIT.SQLView
      );

      const stsProducts = stsProductsResults.filter((product) => {
        return product.customlist3 == "SEA TO SUMMIT";
      });

      const promisesForPepperi =
        module.exports.createProductEnablePromisesForChannel(
          SalesChannels.PEPPERI_APP.name,
          pepperiProducts
        );
      const promisesForSeaToSummit =
        module.exports.createProductEnablePromisesForChannel(
          SalesChannels.SEA_TO_SUMMIT.name,
          stsProducts
        );
      const promisesForHelinox =
        module.exports.createProductEnablePromisesForChannel(
          SalesChannels.HELINOX.name,
          helinoxProducts
        );
      const promisesForDistB2C =
        module.exports.createProductEnablePromisesForChannel(
          SalesChannels.B2C_DIST.name,
          distProducts
        );
      const promisesForDistB2B =
        module.exports.createProductEnablePromisesForChannel(
          SalesChannels.B2B_DIST.name,
          distProducts
        );

      const salesChannelEnablePromises = [
        {
          channel: SalesChannels.PEPPERI_APP.name,
          dataset: promisesForPepperi,
        },
        {
          channel: SalesChannels.SEA_TO_SUMMIT.name,
          dataset: promisesForSeaToSummit,
        },
        {
          channel: SalesChannels.HELINOX.name,
          dataset: promisesForHelinox,
        },
        {
          channel: SalesChannels.B2B_DIST.name,
          dataset: promisesForDistB2B,
        },
        {
          channel: SalesChannels.B2C_DIST.name,
          dataset: promisesForDistB2C,
        },
      ];

      for (const salesChannelPromiseObject of salesChannelEnablePromises) {
        await Promise.all(salesChannelPromiseObject.dataset).catch((e) => {
          throw `Error creating/updating these WMS product buffer entries: ${e}, channel ${salesChannelPromiseObject.channel}`;
        });
        console.log(
          `updated the enables/disables for sales channel ${salesChannelPromiseObject.channel}`
        );
      }
    } catch (error) {
      throw `Error syncing SalesChannelProductEnables from WMS to buffer, ${error}`;
    }
  },

  createProductEnablePromisesForChannel: (channelEnum, arrayOfProducts) => {
    const channelEnableDecorations = arrayOfProducts.map((product) => {
      return WMSToSalesChannelProductEnable(product, channelEnum);
    });

    const salesChannelEnablePromises = channelEnableDecorations.map(
      (decoratedProduct) => {
        // return module.exports.saveProductEnableEntriesWithFindAll(
        //   decoratedProduct
        // );
        return module.exports.saveProductEnableEntries(decoratedProduct);
      }
    );
    return salesChannelEnablePromises;
  },

  saveProductEnableEntries: async (productObject) => {
    if (!productObject.channel_enum)
      throw `No channel set for ${productObject.sku}`;

    const transaction = await sequelize.transaction();

    try {
      const foundEnableEntries = await SalesChannelProductEnable.findOrCreate({
        where: {
          sku: productObject.sku,
          channel_enum: productObject.channel_enum,
        },
        defaults: {
          ...productObject,
        },
        transaction,
      });
      const productWasFound = foundEnableEntries[1] === false;
      if (productWasFound) {
        const updateResultDirect = await foundEnableEntries[0].update(
          productObject,
          { transaction }
        );
      }

      const transactionResult = await transaction.commit();
    } catch (error) {
      await transaction.rollback();
      console.log(
        `Problem ProductEnable buffer entry for ${productObject.sku} (${productObject.channel_enum}): ${error}`
      );
    }
  },

  saveProductEnableEntriesWithFindAll: async (productObject) => {
    if (!productObject.channel_enum)
      throw `No channel set for ${productObject.sku}`;
    try {
      const foundEnableEntries = await SalesChannelProductEnable.findAll({
        where: {
          sku: productObject.sku,
          channel_enum: productObject.channel_enum,
        },
      });

      if (foundEnableEntries.length === 0) {
        await SalesChannelProductEnable.create(productObject);
      } else {
        await foundEnableEntries[0].update(productObject);
      }
    } catch (error) {
      console.log(
        `Problem ProductEnable buffer entry for ${productObject.sku} (${productObject.channel_enum}): ${error}`
      );
    }
  },
};
