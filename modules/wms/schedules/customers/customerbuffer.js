const { updateCustomerBuffer } = require("../../customers/customerbuffer");
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

const WMSProductBuffer = async () => {
  try {
    // console.log("Updating WMS customer buffer...");
    await updateCustomerBuffer();
    // console.log("... done updating WMS customer buffer.");
  } catch (error) {
    console.error(`Error updating WMS customer buffer: ${error}`);
  }
};

WMSProductBuffer();
