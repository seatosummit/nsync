const {
  updateWMSProupdateSalesChannelProductEnablesductBuffer,
} = require("../../products/productenable");
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

const WMSProductEnable = async () => {
  try {
    // console.log("Updating WMS product buffer...");
    await updateSalesChannelProductEnables();
    // console.log("... done updating WMS product buffer.");
  } catch (error) {
    console.error(`Error updating WMS product buffer: ${error}`);
  }
};

WMSProductEnable();
