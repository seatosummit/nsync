require("dotenv").config();
const { updateWMSProductBuffer } = require("../../products/productbuffer");
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

const WMSProductBuffer = async () => {
  try {
    console.log(`Current NODE_ENV: ${process.env.NODE_ENV}`);
    console.log("Updating WMS product buffer...");
    await updateWMSProductBuffer({ filterOnlyActiveProducts: false });
    console.log("... done updating WMS product buffer.");
  } catch (error) {
    console.error(`Error updating WMS product buffer: ${error}`);
  }
};

WMSProductBuffer();
