require("dotenv").config();
const { prepareHeaders } = require("../../helpers/apitools");
const { WMSCustomer } = require("../../../models");
const { customerBufferEntry } = require("./customerdecorator");
const { getCustomersFromWMS } = require("./customers");
let APIHeaders = {};
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

module.exports = {
  updateCustomerBuffer: async () => {
    try {
      APIHeaders = await prepareHeaders(process.env.PEPPERI_SECOND_API_KEY);
      console.log("API headers created");
    } catch (error) {
      console.error("Unable to create API headers", error);
    }
    try {
      const WMSCustomers = await getCustomersFromWMS(APIHeaders);
      const customerSavePromises = WMSCustomers.map((customer) => {
        return module.exports.saveCustomerBufferEntry(
          customerBufferEntry(customer)
        );
      });

      await Promise.all(customerSavePromises).catch((e) =>
        console.log(
          `Error syncing this batch of WMS customers into the buffer DB: ${e}`
        )
      );

      console.log("... customer buffer sync done");
    } catch (error) {
      console.error(`Error syncing customers: ${error}`);
    }
  },

  saveCustomerBufferEntry: async (decoratedWMSCustomer) => {
    const oldOnesFound = await WMSCustomer.findAll({
      where: {
        myob_card_id: pepperiCustomer.myob_card_id,
      },
    });
    if (oldOnesFound.length === 0) {
      await WMSCustomer.create(decoratedWMSCustomer);
      console.log(
        `Added new customer buffer for customerID ${decoratedWMSCustomer.myob_card_id}`
      );
    } else {
      const updateResultDirect = await oldOnesFound[0].update(
        decoratedWMSCustomer
      );
      if (updateResultDirect instanceof Customer) {
        console.log(
          `Updated Customer buffer, CardID ${pepperiCustomer.myob_card_id} (${decoratedWMSCustomer.contact})`
        );
      } else {
        console.log(
          `Problem Updating Customer buffer, CardID ${pepperiCustomer.myob_card_id} (${decoratedWMSCustomer.contact})`
        );
      }
    }
  },
};
