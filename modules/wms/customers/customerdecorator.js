require('console-stamp')(console, '[yyyy-mm-dd HH:MM:ss]');
const {capitaliseFirstLetters} = require('../../helpers/stringTools');

module.exports = {
    customerBufferEntry: (WMSCustomerObject) => {
        const customerToBeCreated = {
            company_name: WMSCustomerObject.CompanyName,
            contact_name: WMSCustomerObject.contact,
            contact_email: WMSCustomerObject.email,
            category: capitaliseFirstLetters(WMSCustomerObject.Category),
            myob_card_id: WMSCustomerObject.CardID,
            sts_territory_manager: capitaliseFirstLetters(WMSCustomerObject.TerritoryManager),
            street1: WMSCustomerObject.street1,
            street2: WMSCustomerObject.street2,
            street3: WMSCustomerObject.street3,
            street4: WMSCustomerObject.street4,
            suburb: WMSCustomerObject.suburb,
            state: WMSCustomerObject.state,
            phone: WMSCustomerObject.phone,
            postcode: WMSCustomerObject.postcode,
            customerSnapshot: WMSCustomerObject,
        }
        return customerToBeCreated;
    },

}