const fetch = require("node-fetch");
require("dotenv").config();
const { prepareHeaders } = require("../../helpers/apitools");
let APIHeaders = {};
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

module.exports = {
  getCustomersFromWMS: async (headers) => {
    if (!headers) {
      try {
        APIHeaders = await prepareHeaders(process.env.PEPPERI_SECOND_API_KEY);
        // console.log("API headers created");
      } catch (error) {
        console.error("Unable to create API headers", error);
      }
    }
    try {
      const response = await fetch(
        `http://mail.seatosummit.com.au:8080/json/sql?filter~SELECT%20*%20FROM%20vGTH_Customers_PEP`,
        { headers: headers.WMSHeaders, timeout: 0 }
      );
      if (response.status !== 200) {
        throw `WMS Customers API call didn't return 200 - ${response.status}: ${response.statusText}`;
      }
      const responseJSON = await response.json();
      return responseJSON;
    } catch (error) {
      console.error(`Error fetching customers from WMS: ${error}`);
    }
  },
};
