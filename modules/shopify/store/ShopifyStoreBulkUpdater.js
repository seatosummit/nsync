const { ShopifyAPI } = require("../../helpers/ShopifyAPI");
const { Sequelize } = require("sequelize");
const Op = Sequelize.Op;
const {
  ProductVariantEnhancement,
  ProductEnhancement,
  BrandfolderBufferAsset,
  BrandfolderBufferAttachment,
} = require("../../../models");
const {
  ShopifyProductImport,
} = require("../../central-buffer/ShopifyProductImport");

const _ = require("lodash");
const { daysSince } = require("../../helpers/dateTools");
const { DateTime } = require("luxon");
const { CSVWriter } = require("../../helpers/CSVWriter");
const { Logger } = require("../../helpers/Logger");
const { valueExistsAndIsNotEmpty } = require("../../helpers/stringTools");
const {
  writeJsonlFile,
  createPriceDifferenceCSV,
  parseJSONLFileFromURL,
} = require("../../helpers/fileTools");
const brandfolderBufferAsset = require("../../../models/brandfolder-buffer-asset");
const {
  CentralBufferValidators,
} = require("../../central-buffer/CentralBufferValidators");
const {
  EnhancementDecorators,
} = require("../../central-buffer/CentralBufferDecorators");
const { currentMemoryUsage } = require("../../helpers/memoryTools");
require("dotenv").config();

class ShopifyStoreBulkUpdater {
  constructor(args) {
    this.SalesChannel = args?.SalesChannel;
    if (!this.SalesChannel)
      throw `No SalesChannel given for ShopifyStoreBulkUpdater`;
    this.shopifyConfig = this.SalesChannel.shopify_config;
    if (!this.shopifyConfig)
      throw `No SalesChannel.shopify_config found for SalesChannel ID ${this.SalesChannel.id}`;
    this.API = new ShopifyAPI(this.shopifyConfig);
    this.productVariantEnhancements = [];
    this.shopifyInventoryLevels = [];
    this.shopifyProductVariants = [];
    this.productEnhancementCreationResults = [];
    this.EnhancementDecorators = new EnhancementDecorators({
      SalesChannel: this.SalesChannel,
    });
  }

  async getLatestShopifyInventoryLevels() {
    const importer = new ShopifyProductImport({
      SalesChannel: this.SalesChannel,
    });
    try {
      const inventoryLevels = await importer.getAllInventoryLevels();
      return inventoryLevels;
    } catch (error) {
      Logger.info(`Problem with getting inventory levels in bulk: ${error}`);
    }
  }

  async getLatestShopifyProductVariants() {
    const importer = new ShopifyProductImport({
      SalesChannel: this.SalesChannel,
    });
    try {
      const productVariants = await importer.getAllProductVariants();
      return productVariants;
    } catch (error) {
      Logger.info(`Problem with getting product variants in bulk: ${error}`);
    }
  }

  async getLatestShopifyProductIDs() {
    const importer = new ShopifyProductImport({
      SalesChannel: this.SalesChannel,
    });
    try {
      const productVariants = await importer.getAllProductIDs();
      return productVariants;
    } catch (error) {
      Logger.info(`Problem with getting product IDs in bulk: ${error}`);
    }
  }

  async bulkAppendProductImages(args) {
    const stackInclude = args?.stackInclude;
    const imagesToAppend = [];
    const imagesQuery = {
      include: {
        model: BrandfolderBufferAttachment,
        attributes: ["id", "url", "extension"],
      },
      attributes: [
        "id",
        "au_skus",
        "name",
        "au_office_image_stack",
        "au_office_image_ordering",
      ],
      where: {},
    };
    imagesQuery.where.section_id = {
      [Op.in]: ["ht7s95whgp3b7txkgf987nt", "q7cphz-8wbqqw-c4dg2u"],
    };
    if (valueExistsAndIsNotEmpty(stackInclude)) {
      imagesQuery.where.au_office_image_stack = { [Op.or]: stackInclude };
      imagesQuery.where.au_office_image_ordering = {
        [Op.or]: {
          [Op.eq]: null,
          [Op.in]: ["1", "2", "3", "4"],
        },
      };
    }

    try {
      this.bufferImageAssets = (
        await BrandfolderBufferAsset.findAll(imagesQuery)
      )
        .filter((image) => {
          if (!image.BrandfolderBufferAttachments) return false;
          const filetype = image.BrandfolderBufferAttachments[0].extension;
          return ["png", "tiff", "tif", "jpg", "jpeg", "gif"].includes(
            filetype
          );
        })
        .map((image) => {
          return {
            id: image.id,
            au_skus: image.au_skus,
            name: image.name,
            url: image.BrandfolderBufferAttachments[0].url,
            stack: image.au_office_image_stack,
            ordering: image.au_office_image_ordering ?? 0,
          };
        });
    } catch (error) {
      Logger.error(`Problem with buffer images query: ${error}`);
    }

    const productVariantEnhancements = (
      await ProductVariantEnhancement.findAll({
        where: {
          SalesChannelId: this.SalesChannel.id,
        },
        include: {
          model: ProductEnhancement,
          attributes: ["id", "GraphQLProductId"],
        },
        attributes: ["id", "sku"],
      })
    )
      .filter((item) => item.ProductEnhancement)
      .map((item) => {
        return {
          sku: item.sku,
          GraphQLProductId: item.ProductEnhancement.GraphQLProductId,
        };
      })
      .filter((item) => item.GraphQLProductId);
    for (const imageToAppend of this.bufferImageAssets) {
      const relatedSKUsOfImage = [...imageToAppend.au_skus];
      if (!valueExistsAndIsNotEmpty(relatedSKUsOfImage)) {
        Logger.error(
          `Brandfolder Asset id ${imageToAppend.id} has no SKU relations`
        );
        continue;
      }
      const matchingVariantEnhancement = _.find(
        productVariantEnhancements,
        (variantEnhancement) => {
          return relatedSKUsOfImage.includes(variantEnhancement.sku);
        }
      );
      if (!matchingVariantEnhancement) {
        Logger.info(
          `Brandfolder Asset id ${imageToAppend.id} has no matching variant enhancement for SalesChannel ${this.SalesChannel.name}`
        );
        continue;
      }

      const imageToAppendPayload = {
        altText: `${imageToAppend.name}-[${matchingVariantEnhancement.sku}==${imageToAppend.stack}||${imageToAppend.ordering}]`,
        src: imageToAppend.url,
      };

      const payload = {
        input: {
          id: matchingVariantEnhancement.GraphQLProductId,
          images: [imageToAppendPayload],
        },
        namespace: this.shopifyConfig.metafield_namespace,
      };
      imagesToAppend.push(payload);
    }
    this.result;
    try {
      await this.API.connect();
      this.result = await this.runBulkOperation({
        operation: this.API.productAppendImagesBulkMutation,
        items: imagesToAppend.slice(100, 3000), //TODO: remove this after testing
        shopName: this.shopifyConfig.name,
      });
    } catch (error) {
      Logger.error(`Problem with bulk product images operation`);
    }
    try {
      const processedBufferResults = await this.processBulkOperationResults({
        resultLogURL: this.result.url,
        parseImages: true,
        parseProducts: false,
      });
    } catch (error) {
      Logger.error(`Problem with processing bulk product images add results`);
    }
  }

  async bulkCreateNewProducts(args) {
    const saveToShopify = args?.saveToShopify ?? false;

    // TODO: get ProductEnhancements for this channel with no shopify ID
    // TODO: use the shopifyPayload of the ProductEnhancement to create JSONL file
    // TODO: do the JSONL bulk update with a productCreate mutation
    try {
      await this.API.connect();

      const productEnhancementsToBeCreated = (
        await ProductEnhancement.findAll({
          where: {
            SalesChannelId: this.shopifyConfig.id,
            shopifyProductId: null,
            GraphQLProductId: null,
            validPayload: true,
          },
          attributes: {
            include: ["id", "shopifyPayload"],
          },
        })
      )
        .filter((item) => {
          return valueExistsAndIsNotEmpty(item.shopifyPayload?.input);
        })
        .map((item) => {
          return item.shopifyPayload;
        });
      //.slice(0, 20); // TODO: remove after

      const result = await this.runBulkOperation({
        operation: this.API.productCreateBulkMutation,
        items: productEnhancementsToBeCreated,
        shopName: this.shopifyConfig.name,
      });
      const processedBufferResults = await this.processBulkOperationResults({
        resultLogURL: result.url,
      });
    } catch (error) {
      throw `Problem with bulk create: ${error}`;
    }
  }

  async bulkUpdateExistingProductsBufferIDs(args) {
    try {
      const saveToShopify = args?.saveToShopify ?? false;
      const processOnlyTheFirst = args?.processOnlyTheFirst;
      const productEnhancements = (
        await ProductEnhancement.findAll({
          where: {
            SalesChannelId: this.shopifyConfig.id,
          },
          attributes: {
            include: ["id", "GraphQLProductId", "shopifyProductId"],
          },
        })
      ).filter((productEnhancement) => {
        return (
          valueExistsAndIsNotEmpty(productEnhancement.GraphQLProductId) ||
          valueExistsAndIsNotEmpty(productEnhancement.shopifyProductId)
        );
      });
      Logger.info(`Decorating and validating metafield payloads...`);
      let metafieldPayloads = productEnhancements
        .map((productEnhancement) => {
          return {
            payload:
              this.EnhancementDecorators.shopifyProductBufferIdPrivateMetafield(
                {
                  productEnhancement,
                  SalesChannel: this.SalesChannel,
                }
              ),
            productEnhancement,
          };
        })
        .filter((metafieldContainer) => {
          const validationResult =
            CentralBufferValidators.validBufferProductIdUpdatePayload({
              payload: metafieldContainer.payload,
              id: metafieldContainer.productEnhancement.id,
            });
          return validationResult.pass;
        })
        .map((metafieldContainer) => metafieldContainer.payload);

      if (processOnlyTheFirst)
        metafieldPayloads = metafieldPayloads.slice(0, processOnlyTheFirst);

      currentMemoryUsage(`After creating dataset`);
      const result = await this.runBulkOperation({
        operation: this.API.productBufferIdBulkMutation,
        items: metafieldPayloads,
        shopName: this.shopifyConfig.name,
        filenamePrefix: "private_metafield_bulk",
      });
    } catch (error) {
      Logger.error(
        `Problem with updating Product private metafields: ${error}`
      );
    }

    // TODO: launch off this bulk update already
  }
  async bulkUpdateExistingProducts(args) {
    const saveToShopify = args?.saveToShopify ?? false;
    const processOnlyTheFirst = args?.processOnlyTheFirst;

    // TODO: get ProductEnhancements for this channel with no shopify ID
    // TODO: use the shopifyPayload of the ProductEnhancement to create JSONL file
    // TODO: do the JSONL bulk update with a productCreate mutation
    try {
      await this.API.connect();
      let productEnhancementsToBeUpdated = (
        await ProductEnhancement.findAll({
          where: {
            SalesChannelId: this.shopifyConfig.id,
            GraphQLProductId: {
              [Op.ne]: null,
            },
          },
          attributes: {
            include: ["id", "shopifyPayload"],
          },
        })
      )
        .filter((item) => {
          return valueExistsAndIsNotEmpty(item.shopifyPayload?.input);
        })
        .map((item) => {
          return this.EnhancementDecorators.productUpdatePayload({
            productEnhancement: item,
          });
        })
        .filter((item) => {
          const validUpdatePayload =
            CentralBufferValidators.validShopifyProductUpdatePayload({
              shopifyProductPayload: item.payload,
              ProductEnhancementId: item.id,
            });
          return validUpdatePayload;
        })
        .map((item) => {
          return item.payload;
        });

      if (processOnlyTheFirst)
        productEnhancementsToBeUpdated = productEnhancementsToBeUpdated.slice(
          0,
          processOnlyTheFirst
        );

      currentMemoryUsage(`After creating dataset`);
      const result = await this.runBulkOperation({
        operation: this.API.productUpdateBulkMutation,
        items: productEnhancementsToBeUpdated,
        shopName: this.shopifyConfig.name,
      });
      // const processedBufferResults = await this.processBulkOperationResults({
      //   resultLogURL: result.url,
      // });
    } catch (error) {
      throw `Problem with bulk create: ${error}`;
    }
  }

  async bulkUpdateShopifyInventory(args) {
    const dryRun = args?.dryRun ?? false;
    if (dryRun) {
      Logger.info(`================`);
      Logger.info(`= DRY RUN ONLY =`);
      Logger.info(`================`);
    }
    try {
      await this.API.connect();
      const bufferInventoryLevels = (
        await ProductVariantEnhancement.findAll({
          where: {
            SalesChannelId: this.shopifyConfig.id,
          },
          attributes: {
            exclude: ["shopifySnapshot"],
          },
        })
      ).map((variantEnhancement) => {
        return {
          variantID: `gid://shopify/ProductVariant/${variantEnhancement.shopifyVariantId}`,
          inventoryID: variantEnhancement.shopifyInventoryItemId,
          inventoryQuantity: variantEnhancement.shopifyInventoryQuantity,
        };
      });

      this.shopifyInventoryLevels =
        await this.getLatestShopifyInventoryLevels();

      if (!valueExistsAndIsNotEmpty(this.shopifyInventoryLevels))
        throw `Couldn't get inventory export from Shopify for ${this.shopifyConfig.name}`;

      const inventoryUpdates = [];

      for (const bufferInventoryLevel of bufferInventoryLevels) {
        const foundMatchingShopifyVariantID = _.find(
          this.shopifyInventoryLevels,
          {
            variantID: bufferInventoryLevel.variantID,
          }
        );

        if (foundMatchingShopifyVariantID) {
          const delta =
            bufferInventoryLevel.inventoryQuantity -
            foundMatchingShopifyVariantID.inventoryQuantity;
          if (delta !== 0) {
            const inventoryUpdatePacket = {
              availableDelta: delta,
              inventoryItemId: foundMatchingShopifyVariantID.inventoryID,
            };
            Logger.info(
              `Found delta for SKU ${foundMatchingShopifyVariantID.sku}: ${delta}`
            );
            if (!dryRun) {
              inventoryUpdates.push(inventoryUpdatePacket);
            }
          }
        }
      }

      const updateChunks = _.chunk(inventoryUpdates, 100);

      for (const updates of updateChunks) {
        try {
          const inventoryBulkUpdateResult =
            await this.API.bulkInventoryAdjustment({
              locationId: this.shopifyConfig.location_id,
              adjustmentsArray: updates,
            });
        } catch (error) {
          Logger.error(`Error bulk updating inventory levels: ${error}`);
        }
      }
      Logger.info(`Bulk updated ${inventoryUpdates.length} inventory levels`);
    } catch (error) {
      Logger.error(`Issue with bulk adjustment: ${error}`);
    }
  }

  async bulkUpdateShopifyProductVariants() {
    try {
      await this.API.connect();
      const bufferVariants = (
        await ProductVariantEnhancement.findAll({
          attributes: [
            "id",
            "price",
            "sku",
            "shopifyVariantId",
            "compareToPrice",
            "override_pricing",
            "shopifyInventoryQuantity",
          ],
          where: {
            SalesChannelId: this.shopifyConfig.id,
            override_pricing: {
              [Op.or]: [false, null],
            },
            price: {
              [Op.gt]: 0,
              [Op.not]: null,
            },
          },
        })
      ).map((variantEnhancement) => {
        return {
          id: `gid://shopify/ProductVariant/${variantEnhancement.shopifyVariantId}`,
          price: variantEnhancement.price,
          compareAtPrice: variantEnhancement.compareToPrice,
          sku: variantEnhancement.sku,
          ean: variantEnhancement.barcode_ean,
          stock: variantEnhancement.shopifyInventoryQuantity,
        };
      });

      this.shopifyProductVariants =
        await this.getLatestShopifyProductVariants();

      if (!valueExistsAndIsNotEmpty(this.shopifyProductVariants))
        throw `Couldn't get product variant export from Shopify for ${this.shopifyConfig.name}`;

      const variantUpdates = [];
      const shopName = this.shopifyConfig.shopName;

      for (const bufferProductVariant of bufferVariants) {
        const matchingShopifyVariant = _.find(this.shopifyProductVariants, {
          id: bufferProductVariant.id,
        });

        if (matchingShopifyVariant) {
          const input = {
            id: matchingShopifyVariant.id,
            sku: bufferProductVariant.sku,
          };
          variantUpdates.push({ input });
        }
      }

      try {
        const timestring = new Date().getTime();
        const fileURL = `./variantUpdates_${shopName}_${timestring}.jsonl`;
        await writeJsonlFile({
          path: fileURL,
          linesArray: variantUpdates,
        });

        const stagedUploadParams = await this.API.createStagedUpload();
        const stagedUploadPath = _.find(stagedUploadParams, { name: "key" })[
          "value"
        ];
        await this.API.uploadBulkUpdateFile({
          stagedUploadParams,
          fileURL,
        });

        await this.API.waitUntilBulkOperationComplete();
        const bulkVariantUpdateResult =
          await this.API.productVariantBulkMutation({
            stagedUploadPath,
          });
        await this.API.waitUntilBulkOperationComplete();
        Logger.info(`Updated ${variantUpdates.length} variants`);
      } catch (error) {
        throw `Error bulk updating variants: ${error}`;
      }
    } catch (error) {
      Logger.error(`Issue with bulk adjustment: ${error}`);
    }
  }

  async bulkUpdateShopifyPricing(args) {
    const dryRun = args?.dryRun ?? false;
    const outputCSV = args?.outputCSV;
    const saveToShopify = args?.saveToShopify;
    try {
      await this.API.connect();
      const bufferVariants = (
        await ProductVariantEnhancement.findAll({
          attributes: [
            "id",
            "price",
            "warehousePrice",
            "sku",
            "shopifyVariantId",
            "compareToPrice",
            "override_pricing",
            "shopifyInventoryQuantity",
          ],
          where: {
            SalesChannelId: this.shopifyConfig.id,
            override_pricing: {
              [Op.or]: [false, null],
            },
            price: {
              [Op.gt]: 0,
              [Op.not]: null,
            },
          },
        })
      ).map((variantEnhancement) => {
        return {
          id: `gid://shopify/ProductVariant/${variantEnhancement.shopifyVariantId}`,
          price: variantEnhancement.price,
          warehousePrice: variantEnhancement.warehousePrice,
          compareAtPrice: variantEnhancement.compareToPrice,
          sku: variantEnhancement.sku,
          ean: variantEnhancement.barcode_ean,
          stock: variantEnhancement.shopifyInventoryQuantity,
        };
      });

      this.shopifyProductVariants =
        await this.getLatestShopifyProductVariants();

      if (!valueExistsAndIsNotEmpty(this.shopifyProductVariants))
        throw `Couldn't get product variant export from Shopify for ${this.shopifyConfig.name}`;

      const pricingUpdates = [];
      const priceDifferentials = [];
      const shopName = this.shopifyConfig.shopName;

      for (const bufferProductVariant of bufferVariants) {
        const matchingShopifyVariant = _.find(this.shopifyProductVariants, {
          id: bufferProductVariant.id,
        });

        if (matchingShopifyVariant) {
          const shopifyCompareAtPrice = matchingShopifyVariant.compareAtPrice
            ? parseFloat(matchingShopifyVariant.compareAtPrice).toFixed(2)
            : null;
          const bufferCompareAtPrice = bufferProductVariant.compareAtPrice
            ? parseFloat(bufferProductVariant.compareAtPrice).toFixed(2)
            : null;
          const shopifyPrice = matchingShopifyVariant.price
            ? parseFloat(matchingShopifyVariant.price).toFixed(2)
            : null;
          if (!matchingShopifyVariant.price)
            Logger.error(
              `shopify variant id ${matchingShopifyVariant.id} has no price set??`
            );
          const priceFieldToUse =
            this.shopifyConfig.nativePriceField ?? "price";
          const bufferPrice = parseFloat(
            bufferProductVariant[`${priceFieldToUse}`]
          ).toFixed(2);
          const priceDifference = shopifyPrice - bufferPrice;
          const comparisonPriceDifference =
            shopifyCompareAtPrice - bufferCompareAtPrice;
          if (priceDifference !== 0 || comparisonPriceDifference !== 0) {
            const input = {
              id: matchingShopifyVariant.id,
              price: bufferPrice,
              compareAtPrice: bufferCompareAtPrice,
            };
            if (!dryRun) pricingUpdates.push({ input });

            if (outputCSV) {
              const differential = {
                sales_channel: shopName,
                sku: bufferProductVariant.sku,
                shopify_price: shopifyPrice,
                wms_price: bufferPrice,
                shopify_compare_price: shopifyCompareAtPrice,
                buffer_compare_price: bufferCompareAtPrice,
                price_to_be_updated: input.price,
                comparison_price_to_be_updated: input.compareAtPrice,
                product_status: matchingShopifyVariant.product.status,
                product_id: matchingShopifyVariant.product.legacyResourceId,
                stock: matchingShopifyVariant.inventoryQuantity,
                variant_id: matchingShopifyVariant.legacyResourceId,
              };
              priceDifferentials.push(differential);
            }
          }
        }
      }

      try {
        if (outputCSV) {
          try {
            await createPriceDifferenceCSV({
              shopName,
              differentialsArray: priceDifferentials,
            });
            Logger.info(`Saved CSV for ${priceDifferentials.length} diffs`);
          } catch (error) {
            Logger.error(`Problem saving price difference CSV: ${error}`);
          }
        }
        if (saveToShopify && pricingUpdates.length > 0 && !dryRun) {
          const timestring = new Date().getTime();
          const fileURL = `./pricing_${shopName}_${timestring}.jsonl`;
          await writeJsonlFile({
            path: fileURL,
            linesArray: pricingUpdates,
          });

          const stagedUploadParams = await this.API.createStagedUpload();
          const stagedUploadPath = _.find(stagedUploadParams, { name: "key" })[
            "value"
          ];
          const uploadedFileResult = await this.API.uploadBulkUpdateFile({
            stagedUploadParams,
            fileURL,
          });

          const bulkOperationResult =
            await this.API.waitUntilBulkOperationComplete({
              bulkOperationType: "MUTATION",
            });
          const bulkVariantUpdateResult =
            await this.API.productVariantBulkMutation({
              stagedUploadPath,
            });
          await this.API.waitUntilBulkOperationComplete({
            bulkOperationType: "MUTATION",
          });
          Logger.info(`Updated prices for ${pricingUpdates.length} variants`);
        } else {
          if (dryRun) {
            Logger.info(
              `No updates to Shopify. This is a dry run. Check the CSV in the project root.`
            );
          }
          Logger.info(`No pricing updates needed`);
        }
      } catch (error) {
        throw `Error bulk updating prices: ${error}`;
      }
    } catch (error) {
      Logger.error(`Issue with bulk adjustment: ${error}`);
    }
  }

  async runBulkOperation(args) {
    const operation = args?.operation;
    const items = args?.items;
    const shopName = args?.shopName ?? "shopify";
    if (!operation) throw `Please provide a bulk operation to use`;
    if (!items) throw `Please provide some items for bulk operation`;
    const filenamePrefix = args?.filenamePrefix ?? "bulk_update";

    const timestring = new Date().getTime();

    try {
      const fileURL = `./${filenamePrefix}_${shopName}_${timestring}.jsonl`;
      await writeJsonlFile({
        path: fileURL,
        linesArray: items,
      });

      const stagedUploadParams = await this.API.createStagedUpload();
      const stagedUploadPath = _.find(stagedUploadParams, { name: "key" })[
        "value"
      ];
      const uploadedFileResult = await this.API.uploadBulkUpdateFile({
        stagedUploadParams,
        fileURL,
      });
      if (!uploadedFileResult?.statusMessage === "Created")
        throw `Uploading bulk update file failed`;
      await this.API.waitUntilBulkOperationComplete({
        bulkOperationType: "MUTATION",
      });
      const operationToRun = this.API[operation.name]({
        stagedUploadPath,
      });
      await operationToRun;
      const bulkOperationResult = await this.API.waitUntilBulkOperationComplete(
        { bulkOperationType: "MUTATION" }
      );
      Logger.info(
        `Current bulk operation results URL: ${bulkOperationResult.url}`
      );
      return bulkOperationResult;
    } catch (error) {
      throw `Problems with bulk update: ${error}`;
    }
  }

  async reattemptIdSync() {
    await this.API.connect();
    const bulkOperationResult = await this.API.waitUntilBulkOperationComplete({
      bulkOperationType: "MUTATION",
    });
    const fileURL = bulkOperationResult.url ?? null;
    if (!fileURL) throw `No fileURL given. No bulk mutation was found?`;
    await this.processBulkOperationResults({
      resultLogURL: fileURL,
    });
  }

  sanitisedImageResults(resultItem) {
    const imageData = resultItem.data.productAppendImages?.product;
    const shopifyImages = imageData.images.edges.map((edge) => {
      const node = edge.node;
      return {
        id: node.id,
        url: node.url,
        altText: node.altText,
      };
    });
    return shopifyImages;
  }

  async processBulkOperationResults(args) {
    const resultLogURL = args?.resultLogURL;
    if (!resultLogURL) throw `Can't parse bulk results without a URL`;
    const processedResults = await parseJSONLFileFromURL({
      fileURL: resultLogURL,
    });
    const parseProducts = args?.parseProducts ?? true;
    const parseImages = args?.parseImages ?? false;
    const productEnhancementsForShopifyIds = await ProductEnhancement.findAll({
      include: {
        model: ProductVariantEnhancement,
        attributes: [
          "id",
          "sku",
          "GraphQLVariantId",
          "GraphQLInventoryItemId",
          "updatedToShopify",
        ],
      },
      where: {
        SalesChannelId: this.shopifyConfig.id,
      },
      attributes: ["id", "shopifyImages"],
    });

    const sanitisedCreationResults = processedResults.map((resultItem) => {
      //TODO: make this parsing universal for whatever result
      if (!resultItem.data) return { errors: "No result data in this row" };
      const data = resultItem.data;

      if (parseImages) {
        if (!data.productAppendImages?.product)
          return { errors: "No product data in this row" };
        if (data.productAppendImages.userErrors.length > 0)
          return { errors: data.productAppendImages.userErrors };
        const productData = data.productAppendImages.product;
        if (!productData.bufferProductEnhancementId)
          throw `No bufferProductEnhancementId in this row`;
        const productId = productData.id;
        const bufferProductEnhancementId =
          productData.bufferProductEnhancementId.value;
        const shopifyImages = this.sanitisedImageResults(resultItem);
        return {
          productId,
          bufferProductEnhancementId,
          shopifyImages,
          variants: [],
        };
      }

      if (parseProducts) {
        if (!data.productCreate?.product)
          return { errors: data?.productCreate.userErrors };
        if (data.productCreate.userErrors.length > 0)
          return { errors: data.productCreate.userErrors };
        const productData = data.productCreate.product;
        const productId = productData.id;
        if (!productData.bufferProductEnhancementId)
          throw `No bufferProductEnhancementId in this row`;
        const bufferProductEnhancementId =
          productData.bufferProductEnhancementId.value;
        const variants = this.productData.variants.edges.map((edge) => {
          const node = edge.node;
          return {
            id: node.id,
            legacyId: node.legacyResourceId,
            sku: node.sku,
            inventoryItemId: node.inventoryItem.id,
          };
        });

        return {
          productId,
          bufferProductEnhancementId,
          variants,
        };
      }
    });

    for (const result of sanitisedCreationResults) {
      if (result.errors) {
        Logger.error(
          `This product entry had the following problem: ${result.errors[0].message}`
        );
        continue;
      }
      try {
        const matchingProductEnhancement = _.find(
          productEnhancementsForShopifyIds,
          {
            id: result.bufferProductEnhancementId,
          }
        );
        if (!matchingProductEnhancement) {
          Logger.error(
            `No matching enhancement for result ID ${result.bufferProductEnhancementId}`
          );
          continue;
        }

        matchingProductEnhancement.set({
          GraphQLProductId: result.productId,
          shopifyImages: result.shopifyImages ?? [],
        });
        if (matchingProductEnhancement.changed().length > 0)
          await matchingProductEnhancement.save();
        for (const variant of result.variants) {
          const matchingVariantEnhancement = _.find(
            matchingProductEnhancement.ProductVariantEnhancements,
            { sku: variant.sku }
          );
          if (matchingVariantEnhancement) {
            matchingVariantEnhancement.set({
              GraphQLVariantId: variant.id,
              GraphQLInventoryItemId: variant.inventoryItemId,
              updatedToShopify: DateTime.now().toISO(),
            });

            if (matchingVariantEnhancement.changed().length > 0)
              await matchingVariantEnhancement.save();
          }
        }
      } catch (error) {
        Logger.error(`Problem with result entry buffer sync: ${error}`);
      }
    }
  }
}

module.exports = ShopifyStoreBulkUpdater;
