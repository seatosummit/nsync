const { ShopifyAPI } = require("../../helpers/ShopifyAPI");
const { Sequelize } = require("sequelize");
const Op = Sequelize.Op;
const {
  ProductVariantEnhancement,
  ProductEnhancement,
  BrandfolderBufferAsset,
  BrandfolderBufferAttachment,
} = require("../../../models");
const {
  ShopifyProductImport,
} = require("../../central-buffer/ShopifyProductImport");

const _ = require("lodash");
const { daysSince } = require("../../helpers/dateTools");
const { DateTime } = require("luxon");
const { CSVWriter } = require("../../helpers/CSVWriter");
const { Logger } = require("../../helpers/Logger");
const { valueExistsAndIsNotEmpty } = require("../../helpers/stringTools");
const {
  writeJsonlFile,
  createPriceDifferenceCSV,
  parseJSONLFileFromURL,
  createMissingImagesCSV,
} = require("../../helpers/fileTools");
const brandfolderBufferAsset = require("../../../models/brandfolder-buffer-asset");
require("dotenv").config();

class ShopifyStoreBulkRetriever {
  constructor(args) {
    this.SalesChannel = args?.SalesChannel;
    if (!this.SalesChannel)
      throw `No SalesChannel given for ShopifyStoreBulkRetriever`;
    this.shopifyConfig = this.SalesChannel.shopify_config;
    if (!this.shopifyConfig)
      throw `No SalesChannel.shopify_config found for SalesChannel ID ${this.SalesChannel.id}`;
    this.API = new ShopifyAPI(this.shopifyConfig);
    this.productVariantEnhancements = [];
    this.shopifyInventoryLevels = [];
    this.shopifyProductVariants = [];
    this.productEnhancementCreationResults = [];
    this.tryUsingLatestExistingResult =
      args?.tryUsingLatestExistingResult ?? false;
  }

  async getShopifyProductsAndImages() {
    const importer = new ShopifyProductImport({
      SalesChannel: this.SalesChannel,
    });
    try {
      await importer.getAllProductImageData({
        tryUsingLatestExistingResult: this.tryUsingLatestExistingResult,
      });
      return {
        products: importer.products,
        privateMetafields: importer.privateMetafields,
        variants: importer.productVariants,
        images: importer.productImages,
      };
    } catch (error) {
      Logger.info(`Problem with getting product variants in bulk: ${error}`);
    }
  }

  async reportOnMissingImages(args) {
    const shopifyImageData = args?.shopifyImageData;
    if (!shopifyImageData) throw `No shopifyImageData provided`;
    const productsCount = shopifyImageData.products.length;
    const productsWithNoImages = shopifyImageData.products
      .filter((product) => !product.featuredImage)
      .map((product) => {
        const variants = shopifyImageData.variants
          .filter((variant) => {
            const childOfCurrentProduct = variant["__parentId"] === product.id;
            const hasnoImage = !valueExistsAndIsNotEmpty(variant.image);
            return childOfCurrentProduct && hasnoImage;
          })
          .map((variant) => {
            return {
              type: "VARIANT",
              product_name: product.title,
              variant_name: variant.title,
              product_id: product.id,
              variant_id: variant.id,
              sku: variant.sku,
              vendor: product.vendor,
              status: product.status,
            };
          });
        return {
          type: "PRODUCT",
          product_name: product.title,
          variant_name: null,
          product_id: product.id,
          variant_id: null,
          sku: "PARENT",
          vendor: product.vendor,
          status: product.status,
          variants,
        };
      });

    await createMissingImagesCSV({
      productsArray: productsWithNoImages,
    });

    Logger.info(
      `${productsWithNoImages.length} out of ${productsCount} (${Math.ceil(
        (productsWithNoImages.length / productsCount) * 100
      )}%) have no images.`
    );
  }

  async getShopifyProductImageData() {
    try {
      await this.API.connect();
      await this.API.waitUntilBulkOperationComplete({
        bulkOperationType: "QUERY",
      });
      const shopifyImageData = await this.getShopifyProductsAndImages();
      await this.reportOnMissingImages({
        shopifyImageData,
      });
      Logger.info(`Missing images CSV created`);
    } catch (error) {
      throw `Error getting product image info: ${error}`;
    }
  }
}

module.exports = ShopifyStoreBulkRetriever;
