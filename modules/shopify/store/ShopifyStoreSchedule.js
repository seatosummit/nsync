const ShopifyStoreUpdater = require("./ShopifyStoreUpdater");
const { ShopifyConfig } = require("../../helpers/ShopifyConfig");
const { Logger } = require("../../helpers/Logger");
const { BufferRefresher } = require("../../central-buffer/BufferRefresher");
const buffer = require("../../brandfolder/labels/buffer");
const ShopifyStoreBulkUpdater = require("./ShopifyStoreBulkUpdater");
const { checkForRule } = require("../../helpers/IntegrationRuleTools");
const ShopifyStoreBulkRetriever = require("./ShopifyStoreBulkRetriever");

const updatePricingAndInventoryToShopify = async ({
  machineNameOfSalesChannel,
}) => {
  try {
    const bufferRefresher = new BufferRefresher();
    //await bufferRefresher.refreshAllVariants();
    const ShopifyStores = await ShopifyConfig.getConfigsFromDatabase({
      machineNameOfSalesChannel,
    });
    for (const ShopifyStore of ShopifyStores) {
      let storeUpdater = new ShopifyStoreUpdater(ShopifyStore.shopify_config);
      Logger.info(`Updating ${ShopifyStore.name} pricing...`);
      const shopifyPricingUpdates =
        await storeUpdater.updatePricingAndInventory();
      Logger.info(
        `Done with ${ShopifyStore.shopify_config.name} pricing updates.`
      );
    }
    process.exit();
  } catch (error) {
    Logger.error(`Error updating Shopify pricing: ${error}`);
  }
};

const updatePricingToShopify = async (args) => {
  const machineNameOfSalesChannel = args?.machineNameOfSalesChannel;
  const specialPricingOnly = args?.specialPricingOnly ?? false;
  const ShopifyStores = await ShopifyConfig.getConfigsFromDatabase({
    machineNameOfSalesChannel,
  });
  for (const ShopifyStore of ShopifyStores) {
    try {
      if (!checkForRule({ ShopifyStore, rule: "priceUpdate" })) {
        continue;
      }
      let storeBulkUpdater = new ShopifyStoreBulkUpdater({
        SalesChannel: ShopifyStore,
      });
      //TODO: move over to bulk variant updates for pricing and enabling
      //currently only the bulk export is used for comparison
      const shopifyVariantsArray =
        await storeBulkUpdater.getLatestShopifyProductVariants();
      let storeUpdater = new ShopifyStoreUpdater(ShopifyStore.shopify_config);
      Logger.info(`Updating ${ShopifyStore.name} prices...`);
      const shopifyPricingUpdates =
        await storeUpdater.updatePricingForSelectedVariants({
          shopifyVariantsArray,
          specialPricingOnly,
        });
      Logger.info(
        `Done with ${ShopifyStore.shopify_config.name} pricing updates.`
      );
    } catch (error) {
      Logger.error(`Error updating Shopify pricing: ${error}`);
    }
  }
  process.exit();
};

const bulkUpdateInventoryToShopify = async (args) => {
  const dryRun = args?.dryRun ?? false;
  const machineNameOfSalesChannel = args?.machineNameOfSalesChannel;
  try {
    const ShopifyStores = await ShopifyConfig.getConfigsFromDatabase({
      machineNameOfSalesChannel,
    });
    for (const ShopifyStore of ShopifyStores) {
      if (!checkForRule({ ShopifyStore, rule: "stockUpdate" })) {
        continue;
      }
      let storeBulkUpdater = new ShopifyStoreBulkUpdater({
        SalesChannel: ShopifyStore,
      });
      Logger.info(`Updating ${ShopifyStore.name} inventory...`);
      const shopifyInventoryUpdates =
        await storeBulkUpdater.bulkUpdateShopifyInventory({
          dryRun,
        });
      Logger.info(
        `Done with ${ShopifyStore.shopify_config.name} inventory updates.`
      );
    }
    process.exit();
  } catch (error) {
    Logger.error(`Error updating Shopify inventory: ${error}`);
  }
};

const bulkUpdateProductStatusToShopify = async ({
  machineNameOfSalesChannel,
}) => {
  try {
    const ShopifyStores = await ShopifyConfig.getConfigsFromDatabase({
      machineNameOfSalesChannel,
    });
    for (const ShopifyStore of ShopifyStores) {
      let storeBulkUpdater = new ShopifyStoreBulkUpdater({
        SalesChannel: ShopifyStore,
      });
      Logger.info(`Updating ${ShopifyStore.name} inventory...`);
      const shopifyInventoryUpdates =
        await storeBulkUpdater.bulkUpdateShopifyPricing({
          outputCSV: true,
          saveToShopify: false,
        });
      Logger.info(
        `Done with ${ShopifyStore.shopify_config.name} inventory updates.`
      );
    }
    process.exit();
  } catch (error) {
    Logger.error(`Error updating Shopify pricing: ${error}`);
  }
};

const bulkUpdateVariantsToShopify = async (args) => {
  const machineNameOfSalesChannel = args?.machineNameOfSalesChannel;
  try {
    const ShopifyStores = await ShopifyConfig.getConfigsFromDatabase({
      machineNameOfSalesChannel,
    });
    for (const ShopifyStore of ShopifyStores) {
      let storeBulkUpdater = new ShopifyStoreBulkUpdater({
        SalesChannel: ShopifyStore,
      });
      Logger.info(`Bulk updating ${ShopifyStore.name} variants...`);
      await storeBulkUpdater.bulkUpdateShopifyProductVariants();
      Logger.info(
        `Done with ${ShopifyStore.shopify_config.name} variant updates.`
      );
    }
    process.exit();
  } catch (error) {
    Logger.error(`Error updating Shopify variants: ${error}`);
  }
};

const bulkUpdatePrimaryImagesToShopify = async (args) => {
  const machineNameOfSalesChannel = args?.machineNameOfSalesChannel;
  const ShopifyStores = await ShopifyConfig.getConfigsFromDatabase({
    machineNameOfSalesChannel,
  });
  for (const ShopifyStore of ShopifyStores) {
    let storeBulkUpdater = new ShopifyStoreBulkUpdater({
      SalesChannel: ShopifyStore,
    });
    try {
      const stackIncludeValues = {
        PRIMARY: "Primary",
        ADDITIONAL: "Additional",
        PACKAGING: "Packaging",
      };
      const shopifyProductImageUpdates =
        await storeBulkUpdater.bulkAppendProductImages({
          stackInclude: [
            stackIncludeValues.PRIMARY,
            //stackIncludeValues.ADDITIONAL,
          ],
        });
    } catch (error) {
      Logger.error(`Problem with bulk append images: ${error}`);
    }
  }
};

const bulkUpdatePricingToShopify = async (args) => {
  const dryRun = args?.dryRun ?? true;
  const machineNameOfSalesChannel = args?.machineNameOfSalesChannel;
  try {
    const ShopifyStores = await ShopifyConfig.getConfigsFromDatabase({
      machineNameOfSalesChannel,
    });
    for (const ShopifyStore of ShopifyStores) {
      if (!checkForRule({ ShopifyStore, rule: "priceUpdate" })) {
        continue;
      }
      let storeBulkUpdater = new ShopifyStoreBulkUpdater({
        SalesChannel: ShopifyStore,
      });
      Logger.info(`Bulk updating ${ShopifyStore.name} pricing...`);
      const shopifyPricingUpdates =
        await storeBulkUpdater.bulkUpdateShopifyPricing({
          outputCSV: true,
          saveToShopify: true,
          dryRun,
        });
      Logger.info(
        `Done with ${ShopifyStore.shopify_config.name} pricing updates.`
      );
    }
    process.exit();
  } catch (error) {
    Logger.error(`Error updating Shopify pricing: ${error}`);
  }
};

const updateCollectionsToShopify = async (args) => {
  const machineNameOfSalesChannel = args?.machineNameOfSalesChannel;
  try {
    const ShopifyStores = await ShopifyConfig.getConfigsFromDatabase({
      machineNameOfSalesChannel,
    });
    for (const ShopifyStore of ShopifyStores) {
      if (!checkForRule({ ShopifyStore, rule: "productCategorise" })) {
        continue;
      }
      let storeUpdater = new ShopifyStoreUpdater(ShopifyStore.shopify_config);
      Logger.info(`Updating ${ShopifyStore.name} collections...`);
      const shopifyPricingUpdates =
        await storeUpdater.updateCollectionsToShopify();
      Logger.info(
        `Done with ${ShopifyStore.shopify_config.name} collection updates.`
      );
    }
    process.exit();
  } catch (error) {
    Logger.error(`Error creating new products: ${error}`);
  }
};

const getImagesFromSalesChannel = async (args) => {
  const machineNameOfSalesChannel = args?.machineNameOfSalesChannel;
  try {
    const ShopifyStores = await ShopifyConfig.getConfigsFromDatabase({
      machineNameOfSalesChannel,
    });
    for (const ShopifyStore of ShopifyStores) {
      let storeRetriever = new ShopifyStoreBulkRetriever({
        SalesChannel: ShopifyStore,
        tryUsingLatestExistingResult: false,
      });
      Logger.info(`Getting ${ShopifyStore.name} product image data...`);
      const shopifyProductImageData =
        await storeRetriever.getShopifyProductImageData();
      Logger.info(
        `Done with getting ${ShopifyStore.shopify_config.name} product image data.`
      );
    }
    process.exit();
  } catch (error) {
    Logger.error(`Error creating new products: ${error}`);
  }
};
const bulkCreateNewProducts = async (args) => {
  const machineNameOfSalesChannel = args?.machineNameOfSalesChannel;
  try {
    const ShopifyStores = await ShopifyConfig.getConfigsFromDatabase({
      machineNameOfSalesChannel,
    });
    for (const ShopifyStore of ShopifyStores) {
      if (!checkForRule({ ShopifyStore, rule: "productAdd" })) {
        continue;
      }
      const storeBulkUpdater = new ShopifyStoreBulkUpdater({
        SalesChannel: ShopifyStore,
      });
      Logger.info(`Bulk creating new products for ${ShopifyStore.name}...`);
      const shopifyProductCreationResults =
        await storeBulkUpdater.bulkCreateNewProducts({
          saveToShopify: true,
        });
      Logger.info(
        `Done with ${ShopifyStore.shopify_config.name} new product creation.`
      );
    }
    process.exit();
  } catch (error) {
    Logger.error(`Error creating new products: ${error}`);
  }
};

const bulkUpdateExistingProductBufferIds = async (args) => {
  const machineNameOfSalesChannel = args?.machineNameOfSalesChannel;
  const processOnlyTheFirst = args?.processOnlyTheFirst;
  try {
    const ShopifyStores = await ShopifyConfig.getConfigsFromDatabase({
      machineNameOfSalesChannel,
    });
    for (const ShopifyStore of ShopifyStores) {
      const storeBulkUpdater = new ShopifyStoreBulkUpdater({
        SalesChannel: ShopifyStore,
      });
      await storeBulkUpdater.API.connect();
      Logger.info(
        `Bulk updating existing products for ${ShopifyStore.name}...`
      );
      const shopifyProductCreationResults =
        await storeBulkUpdater.bulkUpdateExistingProductsBufferIDs({
          saveToShopify: true,
          processOnlyTheFirst,
        });
      Logger.info(
        `Done with ${ShopifyStore.shopify_config.name} existing product updates.`
      );
    }
    process.exit();
  } catch (error) {
    Logger.error(
      `Error adding buffer IDs to Shopify for ${machineNameOfSalesChannel}: ${error}`
    );
  }
};

const bulkUpdateExistingProducts = async (args) => {
  const machineNameOfSalesChannel = args?.machineNameOfSalesChannel;
  const processOnlyTheFirst = args?.processOnlyTheFirst;
  try {
    const ShopifyStores = await ShopifyConfig.getConfigsFromDatabase({
      machineNameOfSalesChannel,
    });
    for (const ShopifyStore of ShopifyStores) {
      if (!checkForRule({ ShopifyStore, rule: "productAdd" })) {
        continue;
      }
      const storeBulkUpdater = new ShopifyStoreBulkUpdater({
        SalesChannel: ShopifyStore,
      });
      Logger.info(
        `Bulk updating existing products for ${ShopifyStore.name}...`
      );
      const shopifyProductCreationResults =
        await storeBulkUpdater.bulkUpdateExistingProducts({
          saveToShopify: true,
          processOnlyTheFirst,
        });
      Logger.info(
        `Done with ${ShopifyStore.shopify_config.name} existing product updates.`
      );
    }
    process.exit();
  } catch (error) {
    Logger.error(`Error creating new products: ${error}`);
  }
};

const reattemptBulkImportSync = async (args) => {
  const machineNameOfSalesChannel = args?.machineNameOfSalesChannel;
  try {
    const ShopifyStores = await ShopifyConfig.getConfigsFromDatabase({
      machineNameOfSalesChannel,
    });
    for (const ShopifyStore of ShopifyStores) {
      const storeBulkUpdater = new ShopifyStoreBulkUpdater({
        SalesChannel: ShopifyStore,
      });
      Logger.info(`Reattempting ID sync for for ${ShopifyStore.name}...`);
      const syncResults = await storeBulkUpdater.reattemptIdSync();
      Logger.info(
        `Done with ${ShopifyStore.shopify_config.name} new product creation.`
      );
    }
    process.exit();
  } catch (error) {
    Logger.error(`Error creating new products: ${error}`);
  }
};

module.exports.updatePricingAndInventoryToShopify =
  updatePricingAndInventoryToShopify;
module.exports.updatePricingToShopify = updatePricingToShopify;
module.exports.bulkUpdateVariantsToShopify = bulkUpdateVariantsToShopify;
module.exports.bulkUpdateInventoryToShopify = bulkUpdateInventoryToShopify;
module.exports.bulkUpdateProductStatusToShopify =
  bulkUpdateProductStatusToShopify;
module.exports.bulkUpdatePricingToShopify = bulkUpdatePricingToShopify;
module.exports.bulkCreateNewProducts = bulkCreateNewProducts;
module.exports.bulkUpdateExistingProducts = bulkUpdateExistingProducts;
module.exports.bulkUpdateExistingProductBufferIds =
  bulkUpdateExistingProductBufferIds;
module.exports.bulkUpdatePrimaryImagesToShopify =
  bulkUpdatePrimaryImagesToShopify;
module.exports.reattemptBulkImportSync = reattemptBulkImportSync;
module.exports.updateCollectionsToShopify = updateCollectionsToShopify;
module.exports.getImagesFromSalesChannel = getImagesFromSalesChannel;
