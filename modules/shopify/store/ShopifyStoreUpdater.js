const { ShopifyAPI } = require("../../helpers/ShopifyAPI");
const { Sequelize } = require("sequelize");
const Op = Sequelize.Op;
const {
  ProductVariantEnhancement,
  SalesChannelCollection,
} = require("../../../models");

const _ = require("lodash");
const { daysSince } = require("../../helpers/dateTools");
const { DateTime } = require("luxon");
const { Logger } = require("../../helpers/Logger");
const { valueExistsAndIsNotEmpty } = require("../../helpers/stringTools");
require("dotenv").config();

class ShopifyStoreUpdater {
  constructor(storeConfig) {
    this.API = new ShopifyAPI(storeConfig);
    this.shopifyConfig = storeConfig;
    this.productVariantEnhancements = [];
  }

  async updatePricingAndInventory() {
    await this.API.connect();
    this.productVariantEnhancements = await ProductVariantEnhancement.findAll({
      where: {
        SalesChannelId: this.shopifyConfig.id,
      },
    });
    const needToBeUpdated = this.productVariantEnhancements.filter(
      (enhancement) => {
        const daysSinceLastUpdate = daysSince(enhancement.updatedToShopify);
        const hasntBeenUpdatedForAWhile = daysSinceLastUpdate > 1;
        return hasntBeenUpdatedForAWhile;
      }
    );
    for (const enhancement of needToBeUpdated) {
      try {
        const doesNotHaveAPriceOverride = !enhancement.override_pricing;
        if (doesNotHaveAPriceOverride) {
          const priceUpdateResult = await this.API.updateVariantPricing(
            enhancement
          );
        }
        if (!this.shopifyConfig?.location_id)
          throw `No location ID in Shopify Config, can't set inventory level`;
        enhancement.location_id = this.shopifyConfig.location_id;
        const inventoryUpdateResult =
          await this.API.updateShopifyInventoryLevel(enhancement);
        await enhancement.update({
          updatedToShopify: DateTime.now().toISO(),
        });
        Logger.info(`Updated price and inventory for SKU ${enhancement.sku}`);
      } catch (error) {
        Logger.error(
          `Problem updating price and inventory for SKU ${enhancement.sku}, id ${enhancement.id}: ${error}`
        );
      }
    }
  }

  async updatePricingForSelectedVariants(args) {
    const freshShopifyVariantsArray = args?.shopifyVariantsArray;
    const specialPricingOnly = args?.specialPricingOnly;
    if (!freshShopifyVariantsArray) throw `Missing Shopify variants array`;
    const query = {
      where: {
        SalesChannelId: this.shopifyConfig.id,
      },
    };
    if (specialPricingOnly) query.where.compareToPrice = { [Op.not]: null };
    await this.API.connect();
    this.productVariantEnhancements = await ProductVariantEnhancement.findAll(
      query
    );
    const enhancementsMatchingShopifyExport =
      this.productVariantEnhancements.filter((enhancement) => {
        const foundMatchingShopifyProductVariant = _.find(
          freshShopifyVariantsArray,
          (item) => {
            return (
              parseInt(item.legacyResourceId) === enhancement.shopifyVariantId
            );
          }
        );
        if (!foundMatchingShopifyProductVariant) {
          Logger.error(
            `Product variant SKU ${enhancement.sku}, variant ID ${enhancement.shopifyVariantId} was not found in Shopify. Delete this entry from the buffer.`
          );
          return false;
        }
        return true;
      });
    const needToBeUpdated = enhancementsMatchingShopifyExport.filter(
      (enhancement) => {
        const daysSinceLastUpdate = daysSince(enhancement.updatedToShopify);
        const hasntBeenUpdatedForAWhile = daysSinceLastUpdate > 1;
        return hasntBeenUpdatedForAWhile;
      }
    );
    Logger.info(
      `${needToBeUpdated.length} variants haven't been updated in over 24h. They need to be updated`
    );
    for (const enhancement of needToBeUpdated) {
      try {
        const doesNotHaveAPriceOverride = !enhancement.override_pricing;
        if (doesNotHaveAPriceOverride) {
          const priceUpdateResult = await this.API.updateVariantPricing(
            enhancement
          );
        }
        await enhancement.update({
          updatedToShopify: DateTime.now().toISO(),
        });
        Logger.info(
          `Updated price for SKU ${enhancement.sku}: price ${enhancement.price}, comparison price ${enhancement.compareToPrice}`
        );
      } catch (error) {
        Logger.error(
          `Problem updating price for SKU ${enhancement.sku}, id ${enhancement.id}: ${error}`
        );
      }
    }
  }

  async updateCollectionsToShopify() {
    const collections = await SalesChannelCollection.findAll({
      attributes: ["id", "payload", "shopifyCollectionId"],
      where: {
        SalesChannelId: this.shopifyConfig.id,
      },
    });

    if (collections.length > 0) await this.API.connect();

    for (const collection of collections) {
      try {
        if (valueExistsAndIsNotEmpty(collection.shopifyCollectionId)) {
          // Shopify ID exists, so update
          collection.payload.input.id = collection.shopifyCollectionId;
          const collectionUpdateResult = await this.API.upsertCollection({
            collection: collection.payload,
            operation: "UPDATE",
          });
          if (collectionUpdateResult.userErrors.length > 0)
            throw `Error updating collection ${collection.payload?.input?.title}: ${collectionUpdateResult.userErrors[0].message}`;
        } else {
          // Otherwise, create
          const collectionCreationResult = await this.API.upsertCollection({
            collection: collection.payload,
            operation: "CREATE",
          });
          if (collectionCreationResult.userErrors.length > 0)
            throw `Error creating collection ${collection.payload?.input?.title}: ${collectionCreationResult.userErrors[0].message}`;
          await collection.set({
            shopifyCollectionId: collectionCreationResult.collection.id,
          });
          await collection.save();
        }
      } catch (error) {
        Logger.error(`Error creating collection: ${error}`);
      }
    }
  }
}

module.exports = ShopifyStoreUpdater;
