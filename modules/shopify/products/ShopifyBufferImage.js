const _ = require("lodash");
const sharp = require("sharp");
const fetch = require("node-fetch");

// TODO: attach a decorator to this thing. Decorator should be injected into this, and have a decorate() implementation
class ShopifyBufferImage {
  constructor(
    DigitalAsset,
    ShopifyBufferProduct,
    SalsifyBufferParent,
    allSalsifyProductsForThisParent,
    BufferDBModel
  ) {
    this.ShopifyBufferProduct = ShopifyBufferProduct;
    this.BufferDBModel = BufferDBModel;
    this.DigitalAsset = DigitalAsset;
    this.SalsifyBufferParent = SalsifyBufferParent;
    this.allSalsifyProductsForThisParent = allSalsifyProductsForThisParent;

    // Shopify ID for updates
    this.shopifyID = null;

    this.widthInPixels = 800; //TODO: figure this out

    // Basic Salsify/Shopify attributes
    this.attributes = {
      salsify_digital_asset_id: null,
      salsify_filename: "",
      salsify_format: "",
      salsify_url: null,
      salsify_parent_skus: [],
      salsify_variant_skus: [],
      salsify_primary_image_ids: [],
      salsify_additional_image_ids: [],
      salsify_packaging_image_ids: [],
      salsify_lifestyle_image_ids: [],
      shopify_product_id: null,
      shopify_variant_ids: [],
      main_image: false,
      position: 0,
      encoded_version: "",
    };
  }

  decorate() {
    this.attributes.salsify_digital_asset_id = this.DigitalAsset["salsify:id"];
    if (
      this.attributes.salsify_digital_asset_id ==
      "bf11ede2ff817b149ec164e714615b5180f6a668"
    ) {
      // console.log("brrt");
    }

    this.attributes.salsify_url = this.DigitalAsset["salsify:url"];
    this.attributes.salsify_filename = this.DigitalAsset["salsify:filename"];
    this.attributes.salsify_format = this.DigitalAsset["salsify:format"];
    this.attributes.shopify_product_id =
      this.ShopifyBufferProduct.shopify_product_id;
    const filteredVariants = this.ShopifyBufferProduct.variants.filter(
      (variant) => {
        return variant.id != null && variant.id != null;
      }
    );
    this.shopifyVariants = filteredVariants.map((variant) => {
      return {
        variant_id: variant.id,
        variant_sku: variant.sku,
      };
    });
    this.createImageRelations();
  }

  createImageRelations() {
    // investigate image buffer entry with shopify_product_id = 7340413845667

    let variantIDs = new Set();
    let primaryImageIDSet = new Set();
    let packagingImagesIDSet = new Set();
    let additionalImageIDSet = new Set();
    let lifestyleImageIDSet = new Set(); // TODO: not supported for Black Diamond, but keep here for future Bandy migration

    const typesOfImages = [
      { snapshotAttribute: "Primary Image", referenceSet: primaryImageIDSet },
      {
        snapshotAttribute: "Packaging Images",
        referenceSet: packagingImagesIDSet,
      },
      {
        snapshotAttribute: "Additional Image",
        referenceSet: additionalImageIDSet,
      },
      {
        snapshotAttribute: "Lifestyle Image",
        referenceSet: lifestyleImageIDSet,
      },
    ];

    for (const typeOfImage of typesOfImages) {
      if (this.SalsifyBufferParent.snapshot[typeOfImage.snapshotAttribute]) {
        const resultingArray = Array.isArray(
          this.SalsifyBufferParent.snapshot[typeOfImage.snapshotAttribute]
        )
          ? this.SalsifyBufferParent.snapshot[typeOfImage.snapshotAttribute]
          : [this.SalsifyBufferParent.snapshot[typeOfImage.snapshotAttribute]];
        for (const currentImageID of resultingArray) {
          typeOfImage.referenceSet.add(currentImageID);

          if (typeOfImage.snapshotAttribute == "Primary Image") {
            this.attributes.main_image = true;
            this.attributes.position = 1;
          }
        }
      }
    }

    for (const salsifyProduct of this.allSalsifyProductsForThisParent) {
      const matchingShopifyVariantID = _.find(this.shopifyVariants, {
        variant_sku: salsifyProduct.sku,
      });

      for (const typeOfImage of typesOfImages) {
        if (salsifyProduct.snapshot[typeOfImage.snapshotAttribute]) {
          const resultingArray = Array.isArray(
            salsifyProduct.snapshot[typeOfImage.snapshotAttribute]
          )
            ? salsifyProduct.snapshot[typeOfImage.snapshotAttribute]
            : [salsifyProduct.snapshot[typeOfImage.snapshotAttribute]];
          for (const currentImageID of resultingArray) {
            typeOfImage.referenceSet.add(currentImageID);
            if (
              matchingShopifyVariantID &&
              matchingShopifyVariantID.variant_id != null
            ) {
              variantIDs.add(matchingShopifyVariantID.variant_id);
            }
            //if(typeOfImage.snapshotAttribute == "Primary Image" && currentImageID == this.attributes.salsify_digital_asset_id) this.attributes.main_image = true;
          }
        }
      }
    }

    this.attributes.shopify_variant_ids = [...Array.from(variantIDs)];
    if (this.attributes.shopify_variant_ids.length > 0) {
      // this.attributes.main_image = true;
      // this.attributes.position = 1;
    }
  }

  getVariantProductsForThisImage() {
    // look through variants and grab the id's
    this.salsify_variant_ids = [];
  }

  async getEncodedImage() {
    const imageFetch = await fetch(this.attributes.salsify_url);
    const imageBuffer = await imageFetch.buffer();
    const sharpedBuffer = await sharp(imageBuffer)
      .resize(this.widthInPixels)
      .toBuffer();
    return sharpedBuffer.toString("base64");
  }

  async bufferUpsert() {
    const foundExisting = await this.BufferDBModel.findOne({
      where: {
        salsify_digital_asset_id: this.attributes.salsify_digital_asset_id,
      },
    });

    if (foundExisting) {
      let variantsSet = new Set(foundExisting.shopify_variant_ids);
      for (const anotherVariantID of this.attributes.shopify_variant_ids) {
        variantsSet.add(anotherVariantID);
      }

      this.attributes.shopify_variant_ids = Array.from(variantsSet);

      const result = await foundExisting.update(this.attributes);
      console.log(
        `Updated buffer image with id ${this.attributes.salsify_digital_asset_id}`
      );
      return result;
    } else {
      const result = await this.BufferDBModel.create(this.attributes);
      console.log(
        `Created buffer image with id ${this.attributes.salsify_digital_asset_id}`
      );
      return result;
    }
  }
}

module.exports.ShopifyBufferImage = ShopifyBufferImage;
