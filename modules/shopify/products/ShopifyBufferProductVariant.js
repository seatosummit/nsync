const _ = require("lodash");
const moment = require("moment");
const {
  ShopifyBufferProductVariant: BufferDBModel,
} = require("../../../models");

class ShopifyBufferProductVariant {
  constructor(
    shopifyProductVariant,
    shopifyPricingRule,
    ERPProduct,
    SalsifyProduct
  ) {
    this.BufferDBModel = BufferDBModel;
    this.pricingRule = shopifyPricingRule;
    this.shopifyProductVariant = shopifyProductVariant;
    this.ERPProduct = ERPProduct;
    this.SalsifyProduct = SalsifyProduct;

    // Basic Shopify Variant attributes
    this.attributes = {
      shopify_id: "",
      shopify_sku: "",
      shopify_title: 0,
      shopify_price: 0,
    };
  }

  decoratePrice() {
    if (this.pricingRule) {
      if (this.hasSpecialPricing(this.pricingRule)) {
        this.attributes.shopify_price = this.pricingRule.price_special;
        this.attributes.shopify_compare_at_price =
          this.pricingRule.price_retail;
      } else {
        this.attributes.shopify_price = this.pricingRule.price_retail;
        this.attributes.shopify_compare_at_price = 0; // handling this on the StoreProductVariant
      }
    } else {
      throw `No pricing rule found for SKU ${this.shopifyProductVariant.sku}`;
    }
  }

  decorate() {
    for (const key in this.shopifyProductVariant) {
      if (Object.hasOwnProperty.call(this.shopifyProductVariant, key)) {
        this.attributes[`shopify_${key}`] = this.shopifyProductVariant[key];
      }
    }
    this.attributes.salsify_system_id = "";
    this.attributes.salsify_parent_sku = "";
    this.attributes.salsify_size_variant = "";
    this.attributes.salsify_colour_variant = "";
    this.attributes.salsify_laymen_colour = "";
    this.attributes.erp_b2b_enabled = false;
    this.attributes.erp_b2c_enabled = false;
    this.attributes.shopify_taxable = true;
    this.attributes.shopify_requires_shipping = true;
    this.attributes.shopify_snapshot = this.shopifyProductVariant;
    this.attributes.salsify_snapshot = {};
  }

  async bufferUpsert() {
    const foundExisting = await this.BufferDBModel.findOne({
      where: {
        shopify_id: this.attributes.shopify_id,
      },
    });

    if (foundExisting) {
      // console.log(`Updating variant ${this.attributes.shopify_sku}`);
      for (const key in this.attributes) {
        if (key == "shopify_taxable") continue;
        if (key == "shopify_snapshot") continue;
        if (foundExisting[key] != undefined) {
          const newValue = this.attributes[key];
          foundExisting[key] = newValue;
        }
      }
      const changes = foundExisting.changed();
      const thereAreChanges = changes ? changes.length > 0 : false;
      if (thereAreChanges) {
        const updatedItem = await foundExisting.save();
        return updatedItem;
      } else {
        return false;
      }
    } else {
      return await this.BufferDBModel.create(this.attributes);
    }
  }

  hasSpecialPricing(pricingRule) {
    if (
      !pricingRule.special_price_startdate ||
      !pricingRule.special_price_enddate
    ) {
      return false;
    }
    const today = moment();
    const start = moment(pricingRule.special_price_startdate);
    const end = moment(pricingRule.special_price_enddate);
    const todayIsBetweenStartAndEnd = today.isBetween(start, end);
    return todayIsBetweenStartAndEnd;
  }
}

module.exports.ShopifyBufferProductVariant = ShopifyBufferProductVariant;
