const _ = require("lodash");
const Op = require("sequelize").Op;
const {
  getAllValuesForProperty,
} = require("../../salsify/properties/properties");
const {
  createSlug,
  addSpacesAndCapitalise,
} = require("../../helpers/stringTools");
const { getSpecialPricingFromERP } = require("../../helpers/WMSBreezeAPI");
// TODO: replace Monitoring with other KPI monitoring system
const { ShopifyBufferPricing } = require("./ShopifyBufferPricing");
const {
  PricingError,
  OrphanSalsifySKUError,
} = require("../errors/ProductErrors");

const {
  SalsifyProduct,
  SalsifyProductParent,
  WMSProduct,
  BrandfolderBufferAsset,
  SalsifyDigitalAsset,
  ShopifyPricing,
  ShopifyBufferProductVariant: ShopifyBufferProductVariantDBModel,
  BlackDiamondBufferProduct: ShopifyBufferProductDBModel,
} = require("../../../models");

//const { Firebase } = require("../../firebase/Firebase");

const { Logger } = require("../../helpers/Logger");
const { SequelizeOperations } = require("../../sequelize/SequelizeOperations");

class ShopifyBuffer {
  constructor(
    ShopifyBufferProduct,
    ShopifyBufferProductVariant,
    ShopifyBufferCollection,
    ShopifyBufferImage,
    CategoryDefinitionModel
  ) {
    this.ShopifyBufferCollection = ShopifyBufferCollection;
    this.ShopifyBufferImage = ShopifyBufferImage;
    this.ShopifyBufferProduct = ShopifyBufferProduct;
    this.ShopifyBufferProductVariant = ShopifyBufferProductVariant;
    this.SalsifyBufferProductParent = SalsifyProductParent;
    this.SalsifyBufferProduct = SalsifyProduct;
    this.SalsifyDigitalAsset = SalsifyDigitalAsset;
    this.BrandfolderBufferAsset = BrandfolderBufferAsset;
    this.CategoryDefinitionModel = CategoryDefinitionModel;
    this.ShopifyBufferProductDBModel = ShopifyBufferProductDBModel;
    this.ShopifyBufferProductVariantDBModel =
      ShopifyBufferProductVariantDBModel;
    this.ShopifyPricingModel = ShopifyPricing;
    this.shopifyPricingEntries = ShopifyPricing;
    this.ERPBufferProduct = WMSProduct;
    this.collectionExceptions = [
      {
        original: "example_converted_special",
        converted: "Example Converted & Special",
      },
    ];
    this.replacementRules = [
      {
        original: "Mens",
        replacement: "Men's",
      },
    ];
    this.product = [];
    this.categoryDefinitions = [];
    this.shopifyBufferImages = [];
    this.salsifyBufferProducts = [];
    this.brandFolderBufferAssets = [];
    this.ERPBufferProducts = [];
    this.ERPPricingSpecialRules = [];
    this.shopifyPricingBufferEntries = [];
    this.ShopifyProducts = [];
    this.updatePromises = [];
    //this.firebase = new Firebase();
    this.firebase = {};
    this.cacheUpdatePromises = [];
    this.categoryDefinitionPromises = [];
    this.salsifyBufferProductParents = [];
    this.salsifyBufferSimpleProducts = [];
    this.BrandfolderBufferAsset;
    // TODO: replace with other KPI monitoring system
  }

  async closeAllDatabaseConnections() {
    const listOfDBModels = [
      "ShopifyBufferProductDBModel",
      "ShopifyBufferProductVariantDBModel",
      "ShopifyPricingModel",
      "ShopifyBufferCollection",
      "SalsifyBufferProduct",
      "SalsifyBufferProductParent",
      "SalsifyDigitalAsset",
      "CategoryDefinitionModel",
      "ERPBufferProduct",
    ];

    for (const model of listOfDBModels) {
      try {
        await this.ShopifyBufferProductDBModel.sequelize.close();
      } catch (error) {
        Logger.info(`${model} didn't have a sequelize connection to close.`);
      }
    }
  }

  async getAssetsForShopifyPricingUpdate() {
    this.shopifyBufferProductVariants =
      await this.ShopifyBufferProductVariant.BufferDBModel.findAll({});
  }

  async getAssetsForBufferUpdate(brandFilter = null, ERPBrandFilter = null) {
    const where = brandFilter ? { where: { brand: brandFilter } } : {};
    const ERPWhere = ERPBrandFilter ? { where: { brand: ERPBrandFilter } } : {};
    this.categoryDefinitions = this.CategoryDefinitionModel
      ? await this.CategoryDefinitionModel.findAll({})
      : null;
    this.ERPPricingSpecialRules = await getSpecialPricingFromERP();
    this.salsifyBufferProductParents =
      await this.SalsifyBufferProductParent.findAll(where);
    this.salsifyBufferProducts = await this.SalsifyBufferProduct.findAll(where);
    this.ERPBufferProducts = await this.ERPBufferProduct.findAll(ERPWhere);
    this.salsifyBufferSimpleProducts = await this.SalsifyBufferProduct.findAll({
      where: {
        simple_or_configurable: "simple",
        is_child: false,
        child_skus: null,
        parent_sku: null,
      },
    });
  }

  async getAssetsForProductVariantBufferUpdate() {
    this.shopifyPricingBufferEntries = await this.ShopifyPricingModel.findAll(
      {}
    );
    this.ERPBufferProducts = await this.ERPBufferProduct.findAll({});
    this.shopifyBufferProducts = await this.ShopifyBufferProductDBModel.findAll(
      {}
    );
  }

  async getAssetsForPricingBufferUpdate() {
    this.salsifyBufferProductParents =
      await this.SalsifyBufferProductParent.findAll({});
    this.salsifyBufferProducts = await this.SalsifyBufferProduct.findAll({});
    this.ERPBufferProducts = await this.ERPBufferProduct.findAll({});
    this.shopifyBufferProducts = await this.ShopifyBufferProductDBModel.findAll(
      {}
    );
    this.shopifyPricingBufferEntries = await this.ShopifyPricingModel.findAll(
      {}
    );
    this.ERPPricingSpecialRules = await getSpecialPricingFromERP();
  }

  async getAssetsForCollectionUpdate() {
    this.salsifyBufferProductParents =
      await this.SalsifyBufferProductParent.findAll({});
    this.categoryDefinitions = await this.CategoryDefinitionModel.findAll({});
  }

  async getAssetsForImageBufferUpdate() {
    this.salsifyBufferDigitalAssets = await SalsifyDigitalAsset.findAll({});
    this.salsifyBufferProductParents =
      await this.SalsifyBufferProductParent.findAll({});
    this.salsifyBufferProducts = await this.SalsifyBufferProduct.findAll({});
    this.shopifyBufferProducts = await this.ShopifyBufferProductDBModel.findAll(
      {}
    );
    this.brandFolderBufferAssets = await this.BranFolderBufferAsset.findAll({});
  }

  async getAllShopifyBufferImages() {
    this.shopifyBufferImages =
      await this.ShopifyBufferImage.BufferDBModel.findAll({});
    return this.shopifyBufferImages;
  }

  async getAllShopifyBufferProducts() {
    this.shopifyBufferProducts = await this.ShopifyBufferProductDBModel.findAll(
      {}
    );
    this.salsifyBufferProductParents =
      await this.SalsifyBufferProductParent.findAll({});
    this.categoryDefinitions = await this.CategoryDefinitionModel.findAll({});
    return this.shopifyBufferProducts;
  }

  async getBufferEntriesFromDB(onlyNewShopifyProducts = true) {
    const query = {};
    if (onlyNewShopifyProducts) {
      query.where = {
        shopify_product_id: {
          [Op.eq]: null,
        },
      };
    }
    const products = await this.ShopifyBufferProductDBModel.findAll(query);
    //const variants = await this.ShopifyBufferProductVariant.BufferDBModel.findAll({});
    const variants = ["TODO: implement these"];
    return { products, variants };
  }

  async getRawBufferEntriesFromDB(onlyNewShopifyProducts = false) {
    let products;
    if (onlyNewShopifyProducts) {
      products = await this.ShopifyBufferProductDBModel.findAll({
        where: {
          shopify_product_id: {
            [Op.eq]: null,
          },
        },
        raw: true,
      });
    } else {
      products = await this.ShopifyBufferProductDBModel.findAll({
        raw: true,
      });
    }
    //const variants = await this.ShopifyBufferProductVariant.BufferDBModel.findAll({});
    const variants = ["TODO: implement these"];
    return { products, variants };
  }

  async getBufferCollectionEntriesFromDB() {
    const collections =
      await this.ShopifyBufferCollection.BufferDBModel.findAll({});
    return { collections };
  }

  async getSalsifyCategoryDefinitionsFromDB() {
    const categoryDefinitions = await this.CategoryDefinitionModel.findAll({});
    return categoryDefinitions;
  }

  async syncProductsIntoBuffer() {
    //await this.firebase.login();
    let parentsWithNoMatches = [];
    let disabledProducts = [];
    for (const salsifyBufferParentProduct of this.salsifyBufferProductParents) {
      try {
        const allSalsifyProductsForThisParent = _.filter(
          this.salsifyBufferProducts,
          { parent_sku: salsifyBufferParentProduct.sku }
        );

        let matchingSalsifyProducts = [];
        let matchingERPProducts = [];

        for (const currentSalsifyProduct of allSalsifyProductsForThisParent) {
          const foundMatchingERPProduct = _.find(this.ERPBufferProducts, {
            sku: currentSalsifyProduct.sku,
          });
          if (foundMatchingERPProduct) {
            const productIsEnabled =
              foundMatchingERPProduct.productSnapshot.b2c_enabled == "1";
            if (!productIsEnabled) {
              // Logger.error(
              //   `product ${foundMatchingERPProduct.sku} is disabled.`
              // );
              disabledProducts.push(foundMatchingERPProduct);
            }
            matchingSalsifyProducts.push(currentSalsifyProduct);
            matchingERPProducts.push(foundMatchingERPProduct);
          } else {
            throw new OrphanSalsifySKUError(currentSalsifyProduct);
          }
        }

        if (matchingERPProducts.length === 0) {
          Logger.error(
            `No ERP matches found for Salsify parent SKU ${salsifyBufferParentProduct.sku}`
          );
          parentsWithNoMatches.push(salsifyBufferParentProduct);
          continue;
        }

        matchingSalsifyProducts = _.orderBy(
          matchingSalsifyProducts,
          ["sku"],
          ["asc"]
        );
        matchingERPProducts = _.orderBy(matchingERPProducts, ["sku"], ["asc"]);

        //// create a new ShopifyProduct instance
        const currentShopifyBufferProduct = new this.ShopifyBufferProduct(
          salsifyBufferParentProduct,
          matchingSalsifyProducts,
          matchingERPProducts,
          this.categoryDefinitions,
          this.ERPPricingSpecialRules,
          this.ShopifyBufferProductDBModel
        );

        currentShopifyBufferProduct.decorate();
        currentShopifyBufferProduct.constructMetafields();
        currentShopifyBufferProduct.updateVariants();
        this.updatePromises.push(currentShopifyBufferProduct.bufferUpsert());
      } catch (error) {
        if (error instanceof PricingError) {
          // TODO: replace with other KPI monitoring system
          // this.Monitoring.sendSKUWithoutPricing();
        }
        if (error instanceof OrphanSalsifySKUError) {
          // TODO: replace with other KPI monitoring system
          // this.Monitoring.sendOrphanSalsifyProduct();
        }
        Logger.error(
          `Error constructing Shopify buffer product ${salsifyBufferParentProduct.sku}: ${error.message}`
        );
      }
    }
    try {
      const results = await Promise.all(this.updatePromises);
      return results;
    } catch (error) {
      Logger.error(`Error syncing Buffer Products`);
      Logger.error(error);
    }

    // Logger.info("updated " + results.length + " products to buffer.");
  }

  async syncPricingRulesIntoBuffer() {
    let allPricingUpdates = [];

    const allVariants = this.getAllActiveShopifyProductVariants();

    for (const variant of allVariants) {
      const currentPricingEntry = new ShopifyBufferPricing(
        variant,
        this.salsifyBufferProducts,
        this.ERPBufferProducts,
        this.ERPPricingSpecialRules
      );

      currentPricingEntry.decorate();
      allPricingUpdates.push(currentPricingEntry.bufferUpsert());
    }
    try {
      const results = await Promise.all(allPricingUpdates);
      const actualResults = results.filter((result) => result);
      Logger.info(
        `Updated ${actualResults.length} out of ${results.length} pricing entries`
      );
    } catch (error) {
      Logger.error(error);
    }
  }

  async syncVariantsIntoBuffer() {
    this.updatePromises = [];
    const allVariants = this.getAllActiveShopifyProductVariants();
    const allVariantsWithID = allVariants.filter((variant) => {
      return variant.id !== undefined && variant.id !== null;
    });

    for (const variant of allVariantsWithID) {
      const matchingPricingRule = this.findPricingByShopifyVariantID(
        variant.id
      );
      if (!matchingPricingRule) {
        Logger.error(
          `No pricing information for variant ${variant.sku} found.`
        );
        continue; // TODO: revisit if these need to be skipped
      }
      const currentVariant = new this.ShopifyBufferProductVariant(
        variant,
        matchingPricingRule
      );
      currentVariant.decorate();
      currentVariant.decoratePrice();
      this.updatePromises.push(currentVariant.bufferUpsert());
    }
    const updateResult = await Promise.all(this.updatePromises);
    return updateResult;
  }
  async syncSimpleProductsIntoBuffer() {
    for (const salsifySimpleProduct of this.salsifyBufferSimpleProducts) {
      let matchingERPProducts = [];

      // ERP is the source of product truth, so using related ERP products as the reference
      const foundMatchingERPProduct = _.find(this.ERPBufferProducts, {
        sku: salsifySimpleProduct.sku,
      });
      if (foundMatchingERPProduct) {
        const productIsEnabled =
          foundMatchingERPProduct.productSnapshot.b2c_enabled === "1";
        if (!productIsEnabled) {
          // Logger.info(`product ${foundMatchingERPProduct.sku} is disabled.`);
        }
        matchingERPProducts.push(foundMatchingERPProduct);
      } else {
        Logger.error(
          `Salsify product ${currentSalsifyProduct.sku} doesn't have a match in ERP.`
        );
      }

      matchingERPProducts = _.orderBy(matchingERPProducts, ["sku"], ["asc"]);

      // This ShopifyBufferProduct behaves slightly differently
      // as it has no child products, and instead needs all its details from the parent.
      const currentShopifyBufferProduct = new this.ShopifyBufferProduct(
        salsifySimpleProduct,
        [],
        matchingERPProducts
      );

      currentShopifyBufferProduct.decorateSimpleProduct();
      currentShopifyBufferProduct.constructMetafields();
      currentShopifyBufferProduct.updateSimpleVariant();
      this.updatePromises.push(currentShopifyBufferProduct.bufferUpsert());
    }

    const results = await Promise.all(this.updatePromises);
    // Logger.info("updated " + results.length + " products to buffer.");
  }

  extractParent(shopifyBufferProduct) {
    let currentParentProduct = _.find(this.salsifyBufferProductParents, {
      sku: shopifyBufferProduct.sku,
    });

    if (!currentParentProduct) {
      // if this product is a simple product
      currentParentProduct = _.find(this.salsifyBufferProducts, {
        sku: shopifyBufferProduct.sku,
      });
      if (!currentParentProduct) {
        // something went wrong with finding this product.
        Logger.error(
          `Matching Salsify parent product for ${shopifyBufferProduct.sku} was not found. Why?`
        );
      }
    }
    return currentParentProduct;
  }

  createUniqueDigitalAssetsArray(SalsifyElement) {
    let uniqueDigitalAssetsArray = [];
    const digitalAssets = SalsifyElement.snapshot["salsify:digital_assets"];
    if (!digitalAssets) {
      Logger.error(
        `Salsify element ${SalsifyElement.sku} has no digital assets.`
      );
      return uniqueDigitalAssetsArray;
    }
    const primaryImageID = currentParentProduct.snapshot["Primary Image"];
  }

  async updateImageBufferBasedOnBrandfolderData() {
    this.updatePromises = [];

    // TODO: figure out the best way to get BF assets over to Shopify Image Buffer

    // todo: loop through this.brandFolderBufferAssets
    // for each asset
    //  for each primary sku
    //    look up the shopify product id
    //    if primary
    //      set the asset order to 1
    //    else
    //      set the order to 0
    //  for each variant in the variants field
    //    look up the variant shopify ids using the skus
    //    check primary
    //    add only primary variant

    for (const brandfolderBufferAsset of this.brandfolderBufferAssets) {
      const shopifyIDs = this.findShopifyIDs(brandfolderBufferAsset);
      const bufferImageToAdd = new this.ShopifyBufferImage(
        brandfolderBufferAsset,
        shopifyIDs
      );
      bufferImageToAdd.decorate();
      this.updatePromises.push(await bufferImageToAdd.bufferUpsert());
    }

    try {
      const updateResult = await Promise.all(this.updatePromises);
    } catch (error) {
      Logger.error(`Error with image buffer update: ${error}`);
    }
  }

  async syncProductsIntoRealtimeCache() {
    this.shopifyBufferProducts = await this.ShopifyBufferProductDBModel.findAll(
      {}
    );

    for (const product of this.shopifyBufferProducts) {
      const productWithGoodies = {
        ...product.dataValues,
        sku: product.sku,
        body_html: product.body_html,
        categories: product.categories,
        variants: product.variants,
        metafields: product.metafields,
        options: product.options,
        snapshot: product.snapshot,
        tags: product.tags,
        //tech_specs: product.tech_specs,
        usps: product.usps,
      };
      this.cacheUpdatePromises.push(
        this.firebase.firestoreProductWritePromise(productWithGoodies)
      );
    }
    const results = await Promise.all(this.cacheUpdatePromises);
    Logger.info(`Updated ${results.length} products into product cache`);
  }

  async syncProductVariantsIntoRealtimeCache() {
    this.cacheUpdatePromises = [];
    this.shopifyBufferProductVariants =
      await this.ShopifyBufferProductVariantDBModel.findAll({});

    for (const productVariant of this.shopifyBufferProductVariants) {
      const productVariantDecorated = {
        ...productVariant.dataValues,
        sku: productVariant.shopify_sku,
      };
      this.cacheUpdatePromises.push(
        this.firebase.firestoreProductVariantWritePromise(
          productVariantDecorated
        )
      );
    }
    const results = await Promise.all(this.cacheUpdatePromises);
    Logger.info(`Updated ${results.length} product variants into cache`);

    const allVariants = await this.firebase.getAllVariants();
    let variants = [];
    allVariants.forEach((doc) => {
      variants.push(doc.data());
    });
    Logger.info(variants.length);
  }

  async syncImagesIntoBuffer() {
    // TODO: this is deprecated. Also, moving to Brandfolder images next.
    // Get rid of this once digital assets sync finalised.

    const productsAlreadyInShopify = this.shopifyBufferProducts.filter(
      (product) => product.shopify_product_id != null
    );
    for (const currentShopifyProduct of productsAlreadyInShopify) {
      const currentParentProduct = this.extractParent(currentShopifyProduct);
      if (!currentParentProduct) continue;

      let uniqueDigitalAssetsForThisParent = [];
      let uniqueDigitalAssetsForTheseKids = [];
      let matchingSalsifyChildProducts = [];

      // PARENTS
      const parentDigitalAssets =
        currentParentProduct.snapshot["salsify:digital_assets"];
      if (parentDigitalAssets) {
        const parentPrimaryImageID =
          currentParentProduct.snapshot["Primary Image"];
        const imagesOnly = parentDigitalAssets.filter(
          (asset) => asset["salsify:asset_resource_type"] === "image"
        );
        for (const currentImage of imagesOnly) {
          if (
            uniqueDigitalAssetsForThisParent.some(
              (asset) =>
                asset.salsify_digital_asset_id == currentImage["salsify:id"]
            )
          )
            continue;
          const is_primary =
            currentImage["salsify:id"] === parentPrimaryImageID;
          const detailsToAdd = {
            salsify_digital_asset_id: currentImage["salsify:id"],
            name: currentImage["salsify:name"],
            url: currentImage["salsify:url"],
            is_primary,
          };
          uniqueDigitalAssetsForThisParent.push(detailsToAdd);
        }
      } else {
        Logger.error(
          `Parent ${currentParentProduct.sku} has no digital assets.`
        );
      }

      // VARIANTS
      if (!currentShopifyProduct.variants) {
        Logger.error(
          `Parent product with sku (${currentShopifyProduct.sku}) doesn't have any variants. Something's wrong.`
        );
      }

      if (currentShopifyProduct.variants[0].sku == currentShopifyProduct.sku) {
        Logger.info(
          `This is a simple product, the parent and the variant have the same sku (${currentShopifyProduct.sku}). Skipping the child digital assets gathering`
        );
        // todo: jump over child gathering
      }

      for (const currentShopifyProductVariant of currentShopifyProduct.variants) {
        const foundMatchingVariant = _.find(this.salsifyBufferProducts, {
          sku: currentShopifyProductVariant.sku,
        });
        if (foundMatchingVariant) {
          matchingSalsifyChildProducts.push(foundMatchingVariant);
        } else {
          Logger.error(
            `did not find a matching salsify for this variant. This should not happen.`
          );
        }
      }

      for (const currentSalsifyChildProduct of matchingSalsifyChildProducts) {
        const childDigitalAssets =
          currentSalsifyChildProduct.snapshot["salsify:digital_assets"];
        if (!childDigitalAssets) {
          Logger.info(
            `No digital assets found for ${currentSalsifyChildProduct.sku} `
          );
          continue;
        }
        const currentChildPrimaryImageID =
          currentSalsifyChildProduct.snapshot["Primary Image"];
        const imagesOnly = childDigitalAssets.filter(
          (asset) => asset["salsify:asset_resource_type"] === "image"
        );
        for (const currentImage of imagesOnly) {
          if (
            uniqueDigitalAssetsForTheseKids.some(
              (asset) =>
                asset.salsify_digital_asset_id == currentImage["salsify:id"]
            )
          )
            continue;
          const is_primary =
            currentImage["salsify:id"] === currentChildPrimaryImageID;
          const detailsToAdd = {
            salsify_digital_asset_id: currentImage["salsify:id"],
            salsify_format: currentImage["salsify:id"],
            salsify_filename: currentImage["salsify:name"],
            url: currentImage["salsify:url"],
            is_primary,
          };
          uniqueDigitalAssetsForTheseKids.push(detailsToAdd);
        }
      }
      // Logger.info(`Parent digital assets for ${currentParentProduct.sku}`);
      // Logger.info(uniqueDigitalAssetsForThisParent);
      // Logger.info(`Child digital assets or ${currentParentProduct.sku}`);
      // Logger.info(uniqueDigitalAssetsForTheseKids);

      // get the primary image of parent product
      if (uniqueDigitalAssetsForThisParent.length == 0) {
        // if the parent digital assets setup is empty, pick a primary image from the variants
        if (uniqueDigitalAssetsForTheseKids.length > 0) {
          const primaryImageReplacement = _.find(
            uniqueDigitalAssetsForTheseKids,
            { is_primary: true }
          );
          if (primaryImageReplacement)
            uniqueDigitalAssetsForThisParent.push(primaryImageReplacement);
        }
      }
    }
  }

  findShopifyIDs(brandfolderBufferAsset) {
    const variantsArray = brandfolderBufferAsset.salsify_variant_skus;
    let variantShopifyIDs = new Set();
    let productShopifyID = "";
    for (const currentVariant of variantsArray) {
      for (const currentShopifyBufferProduct of this.shopifyBufferProducts) {
        const found = _.find(currentShopifyBufferProduct.variants, {
          sku: currentVariant,
        });
        if (found) {
          variantShopifyIDs.add(found.id);
          productShopifyID = productShopifyID
            ? productShopifyID
            : found.product_id;
        }
      }
    }
    return {
      productShopifyID,
      variantShopifyIDsArray: Array.from(variantShopifyIDs),
    };
  }

  async syncImagesIntoBufferBasedOnSalsifyDigitalAssets() {
    const filteredDigitalAssets = this.salsifyBufferDigitalAssets.filter(
      (asset) => {
        return (
          (asset.salsify_primary_skus.length > 0 ||
            asset.salsify_additional_skus.length > 0 ||
            asset.salsify_packaging_skus.length > 0) &&
          asset.salsify_brand == "Black Diamond"
        );
      }
    );
    for (const salsifyDigitalAsset of filteredDigitalAssets) {
      const primarySKUs = salsifyDigitalAsset.salsify_primary_skus;
      const additionalSKUs = salsifyDigitalAsset.salsify_additional_skus;
      const packagingSKUs = salsifyDigitalAsset.salsify_packaging_skus;

      // find relevant Shopify IDs
      let shopifyProductsWithVariants = [];
      let productShopifyID = false;
      let foundShopifyVariant = false;
      let shopifyVariantIDs = [];
      let main_image = false;
      let position = 0;

      // for each primarySKU
      //// find out whether the SKU is a shopify product or a variant
      //// if a parent is found, look for an existing parent in
      //// for a variant, look for an existing parent in

      for (const currentSKU of primarySKUs) {
        productShopifyID = this.findProductIdUsingSKU(currentSKU);
        if (
          productShopifyID &&
          !shopifyProductsWithVariants.includes(productShopifyID)
        ) {
          const foundInShopifyProducts = _.find(shopifyProductsWithVariants, {
            product_id: productShopifyID,
          });
          if (!foundInShopifyProducts) {
            shopifyProductsWithVariants.push({
              product_id: productShopifyID,
              variant_ids: [],
            });
          }
        }

        foundShopifyVariant = this.findVariantUsingSKU(currentSKU);
        if (foundShopifyVariant) {
          const foundInShopifyProducts = _.find(shopifyProductsWithVariants, {
            product_id: foundShopifyVariant.product_id,
          });
          if (foundInShopifyProducts) {
            foundInShopifyProducts.variant_ids.push(foundShopifyVariant.id);
          } else {
            shopifyProductsWithVariants.push({
              product_id: foundShopifyVariant.product_id,
              variant_ids: [foundShopifyVariant.id],
            });
          }
        }
      }

      if (shopifyProductsWithVariants.length == 0) {
        Logger.error(
          `No matching Shopify product found for salsifyDigitalAsset ${salsifyDigitalAsset.salsify_id}`
        );
        continue;
      }

      if (shopifyProductsWithVariants.length > 1) {
        const variable = true;
      }

      for (const shopifyProductWithVariant of shopifyProductsWithVariants) {
        if (shopifyProductWithVariant.product_id == undefined) continue;
        main_image = true;
        position = 1;

        const shopifyIDs = {
          productShopifyID: shopifyProductWithVariant.product_id,
          variantShopifyIDsArray: shopifyProductWithVariant.variant_ids,
        };

        const DigitalAsset = {
          ...salsifyDigitalAsset.dataValues,
          salsify_digital_asset_id: salsifyDigitalAsset.dataValues.salsify_id,
          filename: salsifyDigitalAsset.dataValues.salsify_filename,
          main_image,
          position,
        };

        const bufferImageToAdd = new this.ShopifyBufferImage(
          DigitalAsset,
          shopifyIDs
        );
        bufferImageToAdd.decorate();

        this.updatePromises.push(await bufferImageToAdd.bufferUpsert());
      }
    }

    try {
      const results = await Promise.all(this.updatePromises);
      // Logger.info("updated " + results.length + " ShopifyImages to buffer.");
    } catch (error) {
      // Logger.info("error");
      // Logger.info(error);
    }
  }

  findVariantUsingSKU(variantSKU) {
    for (const currentShopifyBufferProduct of this.shopifyBufferProducts) {
      const foundVariant = _.find(currentShopifyBufferProduct.variants, {
        sku: variantSKU,
      });
      if (foundVariant) {
        return foundVariant;
      }
    }
    return false;
  }

  findProductIdUsingSKU(productSKU) {
    const foundProduct = _.find(this.shopifyBufferProducts, {
      sku: productSKU,
    });
    if (foundProduct) {
      return foundProduct.shopify_product_id;
    } else {
      return false;
    }
  }

  async syncCollectionsIntoBuffer() {
    for (const categoryDefinition of this.categoryDefinitions) {
      const currentShopifyBufferCollection = new this.ShopifyBufferCollection(
        categoryDefinition
      );
      currentShopifyBufferCollection.decorate();
      this.updatePromises.push(currentShopifyBufferCollection.bufferUpsert());
    }
    const results = await Promise.all(this.updatePromises);
    Logger.info("updated " + results.length + " collections to buffer.");
  }

  async updateCategoryDefinitions() {
    let uniqueCategoriesSet = new Set();
    const salsifyBufferCombined = [
      ...this.salsifyBufferProductParents,
      ...this.salsifyBufferProducts,
    ];

    for (const salsifyItem of salsifyBufferCombined) {
      const snapshot = salsifyItem.snapshot;
      const salsify_ids = snapshot["Black Diamond Shopify Category"]
        ? [].concat(snapshot["Black Diamond Shopify Category"])
        : [];

      for (const categoryID of salsify_ids) {
        uniqueCategoriesSet.add(categoryID);
      }
    }

    const categoriesArray = Array.from(uniqueCategoriesSet);

    for (const currentCategoryItem of categoriesArray) {
      this.categoryDefinitionPromises.push(
        this.categoryDefinitionUpsert(currentCategoryItem)
      );
    }

    const results = await Promise.all(this.categoryDefinitionPromises);
    // Logger.info(
    //   "updated " + results.length + " category definitions to buffer."
    // );
  }

  async categoryDefinitionUpsert(categoryEntry) {
    const foundExisting = await this.CategoryDefinitionModel.findOne({
      where: {
        salsify_id: categoryEntry.salsify_id,
      },
    });

    if (!foundExisting) {
      return await this.CategoryDefinitionModel.create(categoryEntry);
    } else {
      return await foundExisting.update(categoryEntry);
    }
  }

  async updateCategoryDefinitionsFromSalsify(categoryName) {
    const categoryDefinitions = await getAllValuesForProperty(categoryName);
    for (const currentDefinition of categoryDefinitions) {
      const custom_name = addSpacesAndCapitalise(
        currentDefinition.salsify_id,
        this.collectionExceptions,
        this.replacementRules
      );
      const updatedDefinition = {
        ...currentDefinition,
        custom_name,
      };
      this.categoryDefinitionPromises.push(
        this.categoryDefinitionUpsert(updatedDefinition)
      );
    }

    const results = await Promise.all(this.categoryDefinitionPromises);
    // Logger.info(
    //   "updated " + results.length + " category definitions to buffer."
    // );
  }

  getAllActiveShopifyProductVariants() {
    const activeShopifyProducts = this.shopifyBufferProducts.filter(
      (product) => {
        return (
          product.status == "active" &&
          product.shopify_product_id !== null &&
          product.variants.length > 0
        );
      }
    );
    let variantsArray = [];
    for (const product of activeShopifyProducts) {
      const variantsForThisProduct = product.variants;
      variantsArray.push(...variantsForThisProduct);
    }
    return variantsArray;
  }

  findPricingByShopifyVariantID(shopifyVariantID) {
    const foundPricingEntry = _.find(
      this.shopifyPricingBufferEntries,
      function (obj) {
        //shopify_variant_id: shopifyVariantID,
        return parseInt(obj.shopify_variant_id) == shopifyVariantID;
      }
    );
    return foundPricingEntry ? foundPricingEntry : false;
  }

  handleErrors(error) {}
}

module.exports.ShopifyBuffer = ShopifyBuffer;
