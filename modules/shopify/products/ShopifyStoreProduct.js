const _ = require("lodash");
const slugify = require("slugify");
const { NumberTools } = require("../../helpers/NumberTools");
const {
  ShopifyProductNotFoundError,
  GeneralShopifyAPIError,
  TooManyAPIRequestsError,
} = require("../errors/ShopifyErrors");
const { NoVariantsError } = require("../errors/ProductErrors");
const { Logger } = require("../../helpers/Logger");

// TODO: attach a decorator to this thing. Decorator should be injected into this, and have a decorate() implementation
class ShopifyStoreProduct {
  constructor(
    ShopifyStoreConnection,
    currentBufferProduct,
    shopifyBufferImages
  ) {
    this.ShopifyStoreConnection = ShopifyStoreConnection;
    this.currentBufferProduct = currentBufferProduct;
    this.shopifyBufferImages = shopifyBufferImages;

    // Shopify ID for updates
    this.shopifyID = currentBufferProduct.shopify_product_id;

    // Creating
    this.creating = currentBufferProduct.shopify_product_id ? false : true;

    // Updating
    this.updating = !this.creating;

    // Shopify custom product level metafields
    this.metafields = {};

    // Basic Shopify attributes
    this.shopifyPayload = {
      vendor: "",
      body_html: "",
      handle: "",
      title: "",
      sku: this.currentBufferProduct.sku,
      status: "draft",
    };
  }

  decorate() {
    const product = this.currentBufferProduct;
    this.shopifyPayload.status = product.status;
    if (product.title) {
      this.shopifyPayload.handle = slugify(
        `${product.title.toLowerCase()}-${product.sku}`
      );
      this.shopifyPayload.title = product.title;
    } else {
      this.shopifyPayload.title = "No Title";
      this.shopifyPayload.handle = slugify(`no-title-${product.sku}`);
      Logger.error(
        `Product title for sku ${product.sku} is missing. Check product buffer.`
      );
      this.shopifyPayload.status = "draft";
    }

    this.shopifyPayload.body_html = product.body_html;
    this.shopifyPayload.vendor = product.vendor;
    this.shopifyPayload.sku = product.sku;
    if (product.tags.length > 0) {
      this.shopifyPayload.tags = product.tags;
      // TODO: the below will possibly change, depending on categories structure
      if (product.categories.length > 0) {
        this.shopifyPayload.tags.push(...product.categories);
      }
    }
    if (product.options.length > 0) {
      this.shopifyPayload.options = product.options;
    }
    if (product.variants.length > 0) {
      this.shopifyPayload.variants = product.variants;
      if (this.updating) {
        this.shopifyPayload.variants = this.shopifyPayload.variants.map(
          (variant) => {
            let trimmedVariant = variant;
            trimmedVariant.price = NumberTools.priceToShopifyFormat(
              variant.price
            );
            trimmedVariant.compare_at_price = NumberTools.priceToShopifyFormat(
              variant.compare_at_price
            );
            delete trimmedVariant.inventory_quantity; // inventory deprecated from API /products call, using separate service
            delete trimmedVariant.old_inventory_quantity; // inventory deprecated from API /products call, using separate service
            delete trimmedVariant.image_id; // dealing with variant images in a separate service
            return trimmedVariant;
          }
        );
        for (const currentVariant of this.shopifyPayload.variants) {
          currentVariant.image_id = this.lookForImageIDInBuffer(
            currentVariant.id
          );
          if (currentVariant.image_id == null) {
            delete currentVariant.image_id;
          }
        }
      }
    }

    if (this.updating) {
      this.shopifyPayload.id = product.shopify_product_id;
      // if (!this.productAndItsVariantsHaveImages(product))
      //   this.shopifyPayload.status = "draft";
    }
    if (this.creating) {
      if (product.metafields.length > 0) {
        this.shopifyPayload.metafields = product.metafields; // Shopify API only supports adding metafields when adding new products
      }
      this.shopifyPayload.status = "draft"; // TODO: change this after image auto-add is sorted
    }
  }

  // TODO: revisit this
  async productAndItsVariantsHaveImages(product) {
    if (product.variants.length > 0) {
      imagesFound =
        product.variants.filter((variant) => variant.image_id).length > 0;
    }
    return imagesFound;
  }

  lookForImageIDInBuffer(variantID) {
    for (const currentImage of this.shopifyBufferImages) {
      if (currentImage.shopify_variant_ids.includes(variantID)) {
        return currentImage.shopify_id;
      }
    }
    return null;
  }

  async getShopifyMetafieldUpsertPromise() {
    const listOfShopifyMetafields =
      await this.getMetafieldsFromShopifyForThisProduct();
    const listOfBufferMetafields = this.currentBufferProduct.metafields;
    let postUpdateMetafields = [];
    for (const metafield of listOfBufferMetafields) {
      const metafieldToUpdate = _.find(listOfShopifyMetafields, {
        key: metafield.key,
      });
      if (metafieldToUpdate) {
        metafieldToUpdate.value = metafield.value;
        const metafieldUpdateResult =
          await this.ShopifyStoreConnection.metafield.update(
            metafieldToUpdate.id,
            metafieldToUpdate
          );
        postUpdateMetafields.push(metafieldUpdateResult);
      } else {
        let metafieldToCreate = metafield;
        metafieldToCreate.owner_id = this.shopifyID;
        metafieldToCreate.owner_resource = "product";
        const metafieldCreationResult =
          await this.ShopifyStoreConnection.metafield.create(metafieldToCreate);
        postUpdateMetafields.push(metafieldCreationResult);
      }
    }
    this.currentBufferProduct.metafields = postUpdateMetafields;
    return this.currentBufferProduct.save();
  }

  async getShopifyPromise() {
    let shopifyResult;
    try {
      if (this.creating) {
        shopifyResult = await this.ShopifyStoreConnection.product.create(
          this.shopifyPayload
        );
        console.log(`Created product ${this.shopifyPayload.title}`);
      }

      if (this.updating) {
        this.shopifyPayload = this.optimisePayloadForOptionsUpdate(
          this.shopifyPayload
        );
        shopifyResult = await this.ShopifyStoreConnection.product.update(
          this.shopifyID,
          this.shopifyPayload
        );
        // console.log(
        //   `Updated product ${this.shopifyPayload.title}, ${this.shopifyID}`
        // );
      }

      this.shopifyID = shopifyResult.id;
      this.currentBufferProduct.shopify_product_id = shopifyResult.id;
      this.currentBufferProduct.snapshot = shopifyResult;
      this.currentBufferProduct.title = shopifyResult.title;
      this.currentBufferProduct.options = shopifyResult.options;
      this.currentBufferProduct.variants = shopifyResult.variants;
      await this.currentBufferProduct.save();
      return shopifyResult;
    } catch (error) {
      if (error.response) {
        if (error.response.statusCode == 404) {
          // Product was manually deleted from the Shopify admin but is still in the buffer.
          throw new ShopifyProductNotFoundError(this.shopifyPayload);
        }
        if (error.response.statusCode == 429) {
          // Shopify API is getting too many requests.
          throw new TooManyAPIRequestsError(
            this.shopifyPayload,
            this.ShopifyStoreConnection
          );
        }
      }
      throw new GeneralShopifyAPIError(this.shopifyPayload);
    }
  }

  optimisePayloadForOptionsUpdate(payload) {
    if (payload.options == undefined) {
      return payload;
    }
    if (payload.options.length > 1) {
      //return payload;
    }
    if (payload.options[0].values == undefined) {
      return payload;
    }
    if (payload.options[0].values.length > 1) {
      //return payload;
    }
    let optimisedVariants = [];
    if (!payload.variants) {
      throw new NoVariantsError(payload);
    }
    for (const variant of payload.variants) {
      let optimisedVariant = {
        // title: variant.title,
        // deal with prices separately
        // deal with image_id separately
        sku: variant.sku,
      };
      if (variant.option1) {
        optimisedVariant.option1 = variant.option1;
      } else {
        optimisedVariant.option1 = payload.options[0].values[0];
      }
      if (variant.option2) {
        optimisedVariant.option2 = variant.option2;
      }
      optimisedVariants.push(optimisedVariant);
    }

    const payloadToReturn = {
      id: payload.id,
      title: payload.title,
      status: payload.status,
      options: payload.options,
      variants: optimisedVariants,
      tags: payload.tags,
    };
    return payloadToReturn;
  }

  async getShopifyDeletePromise() {
    try {
      if (!this.shopifyID)
        throw `No Shopify product ID defined for product with SKU ${this.shopifyPayload.sku}`;
      const deleteResult = await this.ShopifyStoreConnection.product.delete(
        this.shopifyID
      );
      console.log(`Deleted product ${this.shopifyPayload.sku}`);
      if (this.currentBufferProduct.salsify_system_id) {
        await this.currentBufferProduct.delete();
      }
      return deleteResult;
    } catch (error) {
      console.error(`Error with removing Shopify product ${this.shopifyID}`);
      console.log(error.response.body);
      return error;
    }
  }

  async getMetafieldsFromShopifyForThisProduct() {
    const listOfMetafields = await this.ShopifyStoreConnection.metafield.list({
      metafield: { owner_resource: "product", owner_id: this.shopifyID },
    });
    return listOfMetafields;
  }
}

module.exports.ShopifyStoreProduct = ShopifyStoreProduct;
