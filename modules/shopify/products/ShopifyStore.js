const _ = require("lodash");
const Shopify = require("shopify-api-node");
const { getProductsFromWMS } = require("../../wms/products/products");
const { WMSProduct } = require("../../../models");
const {
  ShopifyProductNotFoundError,
  GeneralShopifyAPIError,
  TooManyAPIRequestsError,
} = require("../errors/ShopifyErrors");
const {
  ShopifyConfigError,
  ShopifyConnectionError,
} = require("../errors/ProductErrors");
const { Logger } = require("../../helpers/Logger");
const { ShopifyConfig } = require("../../helpers/ShopifyConfig");

class ShopifyStore {
  constructor(
    ShopifyStoreProduct,
    ShopifyStoreProductVariant,
    BufferInstance,
    ShopifyStoreCollection,
    ShopifyStoreImage
  ) {
    this.ShopifyStoreImage = ShopifyStoreImage;
    this.ShopifyStoreCollection = ShopifyStoreCollection;
    this.ShopifyStoreProduct = ShopifyStoreProduct;
    this.ShopifyStoreProductVariant = ShopifyStoreProductVariant; // TODO: implement this
    this.BufferInstance = BufferInstance;
    this.ERPBufferProduct = WMSProduct;
    this.ShopifyProductBufferEntries = [];
    this.ShopifyProductVariantBufferEntries = [];
    this.collectionPromises = [];
    this.metafieldPromises = [];
    this.updatingPromises = [];
    this.creationPromises = [];
    this.shopifyPromises = [];
    this.salesChannelMachineName = false;
    this.shopifyConnection;
    // TODO: replace with other KPI monitoring system
    this.tooManyAPIRequestsCounter = 0;
  }

  async initShopifyConnection(salesChannelMachineName) {
    if (salesChannelMachineName) {
      try {
        this.shopifyConfigObject = await ShopifyConfig.getConfigForSalesChannel(
          salesChannelMachineName
        );
        this.shopifyConnection = new Shopify({
          shopName: this.shopifyConfigObject.shopName,
          apiKey: this.shopifyConfigObject.apiKey,
          password: this.shopifyConfigObject.password,
          apiVersion: this.shopifyConfigObject.apiversion,
          autoLimit: this.shopifyConfigObject.autoLimit,
        });
        this.shopifyConnection.on("callLimits", function (limits) {
          if (limits.remaining < 3) {
            Logger.info(
              `Shopify API limits nearly reached: ${limits.remaining}`
            );
          }
        });
      } catch (error) {
        throw new ShopifyConnectionError(error);
      }
    } else {
      throw new ShopifyConfigError("param salesChannelMachineName is required");
    }
  }

  async syncPricesFromBufferToShopify() {
    for (const bufferProductVariant of this.BufferInstance
      .shopifyBufferProductVariants) {
      // create a new ShopifyStoreProductVariant
      const currentStoreProductVariant = new this.ShopifyStoreProductVariant(
        this.shopifyConnection,
        bufferProductVariant
      );
      currentStoreProductVariant.decoratePrice();
      this.shopifyPromises.push(currentStoreProductVariant.getShopifyPromise());
    }
    const results = await Promise.all(this.shopifyPromises);
    // Logger.info(`Updated prices for ${results.length} variants in Shopify`);
  }

  async syncProductsFromBufferToShopify({
    newProductsOnly = true,
    selectedShopifyProducts = [],
  }) {
    this.ShopifyProductBufferEntries =
      await this.BufferInstance.getBufferEntriesFromDB(newProductsOnly);

    if (selectedShopifyProducts.length > 0) {
      this.ShopifyProductBufferEntries.products =
        this.ShopifyProductBufferEntries.products.filter(function (product) {
          return this.includes(product.shopify_product_id);
        }, selectedShopifyProducts);
    }

    // TODO: remove this when done. it limits the number of entries to process
    if (this.entriesToLimit) {
      this.ShopifyProductBufferEntries.products =
        this.ShopifyProductBufferEntries.products.slice(0, this.entriesToLimit);
    }

    //TODO: deal with deleting draft products later
    this.ShopifyProductBufferEntries.products =
      this.ShopifyProductBufferEntries.products.filter((product) => {
        const isDraft = product.status === "draft";
        const isNew = product.shopify_product_id === null;
        //if (isDraft && isNew) return false;
        if (isDraft && !isNew) return true;
        if ((!isDraft && isNew) || (!isDraft && !isNew)) return true;
      });

    try {
      for (const currentBufferProduct of this.ShopifyProductBufferEntries
        .products) {
        if (currentBufferProduct.variants.length == 0) {
          Logger.info(
            `Product with sku ${currentBufferProduct.sku} has no variants. Shopify needs at least one variant to update.`
          );
        }
        const shopifyProductInstance = new this.ShopifyStoreProduct(
          this.shopifyConnection,
          currentBufferProduct,
          this.BufferInstance.shopifyBufferImages
        );
        shopifyProductInstance.decorate();
        this.shopifyPromises.push(shopifyProductInstance.getShopifyPromise());
      }

      const createResults = await Promise.all(this.shopifyPromises);
      Logger.info(
        `Created/updated ${createResults.length} products to Shopify.`
      );
    } catch (error) {
      if (error instanceof ShopifyProductNotFoundError) {
        // TODO: replace with other KPI monitoring system
        // this.Monitoring.sendShopifyProductNotFound();
      }
      if (error instanceof TooManyAPIRequestsError) {
        // TODO: replace with other KPI monitoring system
        // this.Monitoring.sendTooManyShopifyAPIRequests();
        this.TooManyAPIRequestsError++;
        if (this.TooManyAPIRequestsError > 20) {
          process.exit();
        }
      }
    }
  }

  async syncMetafieldsFromBufferToShopify() {
    this.ShopifyProductBufferEntries =
      await this.BufferInstance.getBufferEntriesFromDB();

    if (this.entriesToLimit) {
      this.ShopifyProductBufferEntries.products =
        this.ShopifyProductBufferEntries.products.slice(0, this.entriesToLimit);
    }

    for (const currentBufferProduct of this.ShopifyProductBufferEntries
      .products) {
      const shopifyProductInstance = new this.ShopifyStoreProduct(
        this.shopifyConnection,
        currentBufferProduct
      );
      if (shopifyProductInstance.updating) {
        this.metafieldPromises.push(
          shopifyProductInstance.getShopifyMetafieldUpsertPromise()
        );
      }
    }
    const metafieldResults = await Promise.all(this.metafieldPromises);
    Logger.info(
      `Created/updated ${metafieldResults.length} metafields to Shopify.`
    );
  }

  async syncCollectionsFromBufferToShopify() {
    this.ShopifyCollectionBufferEntries =
      await this.BufferInstance.getBufferCollectionEntriesFromDB();
    this.ShopifyCategoryDefinitionEntries =
      await this.BufferInstance.getSalsifyCategoryDefinitionsFromDB();
    const bufferCollections = this.ShopifyCollectionBufferEntries.collections;
    for (const bufferCollection of bufferCollections) {
      const collectionInstance = new this.ShopifyStoreCollection(
        this.shopifyConnection,
        bufferCollection
      );
      collectionInstance.decorate();
      if (collectionInstance.updating) {
        this.collectionPromises.push(
          collectionInstance.getShopifyUpdatePromise()
        );
      }
      if (collectionInstance.creating) {
        this.collectionPromises.push(
          collectionInstance.getShopifyCreatePromise()
        );
      }
    }
    try {
      const collectionResults = await Promise.all(this.collectionPromises);
      // consosle.log(
      //   `Created/updated ${collectionResults.length} collections to Shopify.`
      // );
      return;
    } catch (error) {
      Logger.error(`Error: ${error}`);
      throw error;
    }
  }

  async syncImagesFromBufferToShopify(createOnly = true, filter = []) {
    this.ShopifyImageBufferEntries =
      await this.BufferInstance.getAllShopifyBufferImages();

    if (filter.length > 0) {
      this.ShopifyImageBufferEntries = this.ShopifyImageBufferEntries.filter(
        function (imageEntry) {
          return this.includes(imageEntry.shopify_product_id);
        },
        filter
      );
    }

    this.ShopifyProductBufferEntries =
      await this.BufferInstance.getAllShopifyBufferProducts();
    if (filter.length > 0) {
      this.ShopifyProductBufferEntries =
        this.ShopifyProductBufferEntries.filter(function (product) {
          return this.includes(product.shopify_product_id);
        }, filter);
    }

    let onlyActiveProductImages = [];

    for (const currentImage of this.ShopifyImageBufferEntries) {
      const matchingProduct = _.find(this.ShopifyProductBufferEntries, {
        shopify_product_id: currentImage.shopify_product_id,
      });
      if (matchingProduct) {
        if (matchingProduct.status != "draft") {
          onlyActiveProductImages.push(currentImage);
        }
      } else {
        Logger.error(
          `No matching product for image entry with shopify id ${currentImage.shopify_product_id} and salsify asset id ${currentImage.salsify_digital_asset_id}`
        );
      }
    }

    if (createOnly) {
      onlyActiveProductImages = onlyActiveProductImages.filter((image) => {
        return image.shopify_id == null;
      });
    }

    if (this.entriesToLimit) {
      this.ShopifyImageBufferEntries = this.ShopifyImageBufferEntries.slice(
        0,
        this.entriesToLimit
      );
    }

    for (const bufferImage of onlyActiveProductImages) {
      const imageInstance = new this.ShopifyStoreImage(
        this.shopifyConnection,
        bufferImage
      );
      await imageInstance.decorate();
      this.shopifyPromises.push(imageInstance.getShopifyPromise());
    }
    const imageResults = await Promise.all(this.shopifyPromises);
    // Logger.info(`Synced ${imageResults.length} images to Shopify`);
  }

  async syncMetafieldsFromBufferToShopify() {
    this.ShopifyProductBufferEntries =
      await this.BufferInstance.getBufferEntriesFromDB();

    if (this.entriesToLimit) {
      this.ShopifyProductBufferEntries.products =
        this.ShopifyProductBufferEntries.products.slice(0, this.entriesToLimit);
    }

    for (const currentBufferProduct of this.ShopifyProductBufferEntries
      .products) {
      const shopifyProductInstance = new this.ShopifyStoreProduct(
        this.shopifyConnection,
        currentBufferProduct
      );
      if (shopifyProductInstance.updating) {
        this.metafieldPromises.push(
          shopifyProductInstance.getShopifyMetafieldUpsertPromise()
        );
      }
    }
    const metafieldResults = await Promise.all(this.metafieldPromises);
    // Logger.info(
    //   `Created/updated ${metafieldResults.length} metafields to Shopify.`
    // );
  }

  async cleanUpDeadProductIDsFromBuffer() {
    // get all buffer products with Shopify Product ID
    // get product with ID from Shopify
    // if getting 404 back, delete product entry from buffer
    this.ShopifyProductBufferEntries =
      await this.BufferInstance.getRawBufferEntriesFromDB(false);
  }

  async cleanUpSelectedProductsFromShopifyAndBuffer(
    shopifyProductIDsToDelete = []
  ) {
    this.ShopifyProductBufferEntries =
      await this.BufferInstance.getRawBufferEntriesFromDB(false);

    for (const currentShopifyProductIDToRemove of shopifyProductIDsToDelete) {
      const matchingBufferProductToDelete = _.find(
        this.ShopifyProductBufferEntries.products,
        {
          shopify_product_id: currentShopifyProductIDToRemove,
        }
      );

      const bareMinimumProductInfo = {
        shopify_product_id: currentShopifyProductIDToRemove,
        sku: "SKU not set",
      };

      const shopifyBufferProduct = matchingBufferProductToDelete
        ? matchingBufferProductToDelete
        : bareMinimumProductInfo;

      const shopifyProductInstance = new this.ShopifyStoreProduct(
        this.shopifyConnection,
        shopifyBufferProduct
      );

      this.shopifyPromises.push(
        shopifyProductInstance.getShopifyDeletePromise()
      );
    }

    const deleteResults = await Promise.all(this.shopifyPromises);
    // Logger.info(`Deleted ${deleteResults.length} products from Shopify.`);
  }

  async updateInventoryLevels(useBufferDB = false, brandFilter = null) {
    // TODO: get WMS products and their SKU plus inventory level
    // TODO: get all product variants by looping through buffer products
    // TODO: get all product variants by looping through product variants once supported

    let inventoryLevelPromises = [];

    this.ShopifyProductBufferEntries =
      await this.BufferInstance.getBufferEntriesFromDB(false);

    this.ShopifyProductBufferEntries.products =
      this.ShopifyProductBufferEntries.products.filter((product) => {
        return product.status != "draft";
      });

    let erpProducts = [];
    if (useBufferDB) {
      const query = brandFilter ? { where: { brand: brandFilter } } : {};

      erpProducts = await this.ERPBufferProduct.findAll(query);
      erpProducts = erpProducts.filter((product) => {
        return product.productSnapshot["b2c_enabled"] == "1";
      });
    } else {
      erpProducts = await getProductsFromWMS(null, false);
      if (brandFilter) {
        erpProducts = erpProducts.filter(function (product) {
          return product.brand == this;
        }, brandFilter);
      }
    }

    for (const currentERPProduct of erpProducts) {
      let available = 0;
      if (currentERPProduct.stockQuantity) {
        available = currentERPProduct.stockQuantity;
      }
      if (currentERPProduct.stockqty) {
        available = currentERPProduct.stockqty;
      }
      const foundMatchingVariant = this.findVariantInventoryItemUsingSKU(
        currentERPProduct.sku
      );
      if (foundMatchingVariant) {
        if (!foundMatchingVariant.inventory_item_id) {
          // Logger.error(
          //   `No inventory item for Shopify product SKU ${foundMatchingVariant.sku}`
          // );
          continue;
        }
        const inventory_item_id = foundMatchingVariant.inventory_item_id;
        const location_id = 65269727395; // TODO: add multi location support
        const inventoryLevelsPayload = {
          inventory_item_id,
          available,
          location_id,
        };
        inventoryLevelPromises.push(
          this.saveInventoryLevelToShopify(
            inventoryLevelsPayload,
            foundMatchingVariant.product_id
          )
        );
      } else {
        Logger.error(
          `Error: couldn't fin matching Shopify product variant for SKU ${currentERPProduct.sku}`
        );
      }
    }

    try {
      await Promise.all(inventoryLevelPromises);
    } catch (error) {
      Logger.info(`Problems updating inventory levels: ${error}`);
      // Logger.info(error);
    }
  }

  findVariantInventoryItemUsingSKU(sku) {
    for (const currentShopifyBufferProduct of this.ShopifyProductBufferEntries
      .products) {
      const foundVariant = _.find(currentShopifyBufferProduct.variants, {
        sku: sku,
      });
      if (foundVariant) {
        return foundVariant;
      }
    }
    return false;
  }

  async saveInventoryLevelToShopify(payload, parentSKU) {
    try {
      const result = await this.shopifyConnection.inventoryLevel.set(payload);
      // Logger.info(
      //   `Updated inventory level for parent SKU ${parentSKU}, inventory id ${payload.inventory_item_id}`
      // );
      return result;
    } catch (error) {
      Logger.error(
        `Error with saving inventory level for inventory item ${payload.inventory_item_id}`
      );
      Logger.error(error);
    }
  }
}

module.exports.ShopifyStore = ShopifyStore;
