const {
  SalsifyProduct,
  SalsifyProductParent,
  WMSProduct,
  BrandfolderBufferAsset,
  SalsifyDigitalAsset,
  ShopifyPricing,
  ShopifyBufferProductVariant,
  BlackDiamondBufferProduct,
} = require("../../../models");
const { Sequelize } = require("sequelize");
const { valueExistsAndIsNotEmpty } = require("../../helpers/stringTools");
const { ShopifyStore: Store } = require("./ShopifyStore");
const { Logger } = require("../../helpers/Logger");
const Op = Sequelize.Op;

class ShopifyProductOperations {
  static async cleanOutRedundantBufferProducts() {
    const ShopifyStore = new Store();
    await ShopifyStore.initShopifyConnection("BLACKDIAMOND");
    const allBufferProducts = await BlackDiamondBufferProduct.findAll({
      where: {
        shopify_product_id: { [Op.ne]: null },
        //status: "active",
      },
    });
    for (const bufferProduct of allBufferProducts) {
      if (valueExistsAndIsNotEmpty(bufferProduct.shopify_product_id)) {
        try {
          //check if in Shopify
          const foundProduct = await ShopifyStore.shopifyConnection.product.get(
            bufferProduct.shopify_product_id
          );
          // TODO: decide what to do if found
          // if (foundProduct) {
          //   await ShopifyStore.shopifyConnection.product.delete(
          //     bufferProduct.shopify_product_id
          //   );
          // }
        } catch (error) {
          if (error.response.statusCode === 404) {
            // await bufferProduct.destroy();
            // Logger.info(
            //   `Removed ${bufferProduct.sku} from buffer, not found in Shopify`
            // );
          }
        }
      }
    }
    Logger.info("... done");
  }

  static async cleanOutRedundantBufferVariants() {
    const ShopifyStore = new Store();
    await ShopifyStore.initShopifyConnection("BLACKDIAMOND");
    const allBufferVariants = await ShopifyBufferProductVariant.findAll({});
    for (const variant of allBufferVariants) {
      if (valueExistsAndIsNotEmpty(variant.shopify_id)) {
        try {
          //check if in Shopify
          const foundProduct =
            await ShopifyStore.shopifyConnection.productVariant.get(
              variant.shopify_id
            );
          // TODO: decide what to do if found
          // if (foundProduct) {
          //   await ShopifyStore.shopifyConnection.product.delete(
          //     bufferProduct.shopify_product_id
          //   );
          // }
          Logger.info(
            `Keeping ${variant.shopify_sku} in buffer, it was found in Shopify`
          );
        } catch (error) {
          if (error.response.statusCode === 404) {
            await variant.destroy();
            Logger.info(
              `Removed ${variant.shopify_sku} from buffer, it was not found in Shopify`
            );
          }
        }
      }
    }
    Logger.info("... done");
  }
}

module.exports.ShopifyProductOperations = ShopifyProductOperations;
