const _ = require("lodash");
const slugify = require("slugify");

const { ShopifyPricing } = require("../../../models");
const { Logger } = require("../../helpers/Logger");
const { valueExistsAndIsNotEmpty } = require("../../helpers/stringTools");
const { SequelizeOperations } = require("../../sequelize/SequelizeOperations");

class ShopifyBufferPricing {
  constructor(
    ShopifyProductVariant,
    SalsifyBufferProducts,
    ERPBufferProducts,
    ERPPricingSpecialRules
  ) {
    this.ShopifyProductVariant = ShopifyProductVariant;
    this.SalsifyBufferProducts = SalsifyBufferProducts;
    this.ERPBufferProducts = ERPBufferProducts;
    this.ERPPricingSpecialRules = ERPPricingSpecialRules;
    this.ShopifyPricing = ShopifyPricing;

    // Shopify ID for updates
    this.shopifyID = null;

    // Basic Shopify attributes
    this.attributes = {
      shopify_product_id: null,
      shopify_variant_id: null,
      parent_sku: null,
      variant_sku: null,
      variant_ean: null,
      price_retail: null,
      price_warehouse: null,
      price_special: null,
    };
  }

  decorate() {
    const variantSKU = this.ShopifyProductVariant.sku;
    const relevantERPProduct = _.find(this.ERPBufferProducts, {
      sku: variantSKU,
    });
    const relevantSalsifyVariant = _.find(this.salsifyBufferProducts, {
      sku: variantSKU,
    });
    const relevantPricingSpecialRule = _.find(this.ERPPricingSpecialRules, {
      ItemNumber: variantSKU,
    });

    this.attributes.shopify_product_id = this.ShopifyProductVariant.product_id
      ? this.ShopifyProductVariant.product_id.toString()
      : null;
    this.attributes.shopify_variant_id = this.ShopifyProductVariant.id
      ? this.ShopifyProductVariant.id.toString()
      : null;
    this.attributes.parent_sku = relevantSalsifyVariant
      ? relevantSalsifyVariant.parent_sku
      : null;
    this.attributes.variant_sku = variantSKU;
    this.attributes.variant_ean = relevantERPProduct
      ? relevantERPProduct.barcode
      : null;
    this.attributes.price_retail = relevantERPProduct
      ? relevantERPProduct.retailPrice
      : null;
    this.attributes.price_warehouse = relevantERPProduct
      ? relevantERPProduct.unitPrice
      : null;
    this.attributes.price_special = valueExistsAndIsNotEmpty(
      relevantPricingSpecialRule
    )
      ? parseFloat(relevantPricingSpecialRule.SpecialPrice)
      : null;
    this.attributes.special_price_startdate = relevantPricingSpecialRule
      ? relevantPricingSpecialRule.SpecialFrom
      : null;
    this.attributes.special_price_enddate = relevantPricingSpecialRule
      ? relevantPricingSpecialRule.SpecialTo
      : null;
  }

  async findAll() {
    return this.ShopifyPricing.findAll({});
  }

  async bufferUpsert() {
    try {
      return await SequelizeOperations.Upsert(
        this.attributes,
        "variant_sku",
        this.ShopifyPricing
      );
    } catch (error) {
      Logger.error(error);
    }
  }
}

module.exports.ShopifyBufferPricing = ShopifyBufferPricing;
