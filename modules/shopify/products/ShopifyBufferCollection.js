const _ = require("lodash");
const {
  createSlug,
  addSpacesAndCapitalise,
} = require("../../helpers/stringTools");

// TODO: attach a decorator to this thing. Decorator should be injected into this, and have a decorate() implementation
class ShopifyBufferCollection {
  constructor(SalsifyCategoryObject, BufferDBModel) {
    this.BufferDBModel = BufferDBModel; // Brand-specific ORM model for buffer entries
    this.SalsifyCategoryObject = SalsifyCategoryObject; // these are the categories that will be added to each rule

    // Shopify ID for updates
    this.shopifyID = null;

    // the handle is used for the URL slug of the collection
    this.shopifyHandle = SalsifyCategoryObject
      ? this.SalsifyCategoryObject.salsify_id
        ? createSlug(this.SalsifyCategoryObject.salsify_id)
        : null
      : null;

    this.salsify_property_value_id = SalsifyCategoryObject
      ? SalsifyCategoryObject.salsify_id
      : null;
    this.custom_name = SalsifyCategoryObject
      ? SalsifyCategoryObject.custom_name
      : null;
    // Basic Shopify attributes
    this.attributes = {
      title: "",
      handle: "",
      salsify_property_value_id: "",
      rules: [],
      body_html: "",
      snapshot: {},
      template_suffix: null,
      sort_order: "best-selling",
      disjunctive: false,
    };
  }

  decorate() {
    this.attributes.title = this.SalsifyCategoryObject.name;
    this.attributes.salsify_property_value_id = this.salsify_property_value_id;
    this.attributes.handle = this.shopifyHandle;
    this.attributes.rules = [
      {
        column: "tag",
        relation: "equals",
        condition: this.custom_name,
      },
    ];
  }

  async bufferUpsert() {
    const foundExisting = await this.BufferDBModel.findOne({
      where: {
        salsify_property_value_id: this.attributes.salsify_property_value_id,
      },
    });

    if (foundExisting) {
      const result = await foundExisting.update(this.attributes);
      return result;
    } else {
      const result = await this.BufferDBModel.create(this.attributes);
      return result;
    }
  }
}

module.exports.ShopifyBufferCollection = ShopifyBufferCollection;
