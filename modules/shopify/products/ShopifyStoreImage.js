const _ = require("lodash");
const sharp = require("sharp");
const fetch = require("node-fetch");
const { Logger } = require("../../helpers/Logger");

// TODO: attach a decorator to this thing. Decorator should be injected into this, and have a decorate() implementation
class ShopifyStoreImage {
  constructor(ShopifyStoreConnection, currentBufferImage) {
    this.ShopifyStoreConnection = ShopifyStoreConnection;
    this.currentBufferImage = currentBufferImage;

    // Shopify ID for updates
    this.shopifyID = currentBufferImage.shopify_id;

    // Creating
    this.creating = currentBufferImage.shopify_id ? false : true;

    // Updating
    this.updating = !this.creating;

    // resize width
    this.widthInPixels = 800;

    // Shopify custom product level metafields
    this.metafields = {};

    // Basic Shopify attributes
    this.shopifyPayload = {
      salsify_digital_asset_id: null,
      filename: "",
      id: null,
      product_id: null,
      variant_ids: [],
      // src: "",  // using base64 encoding instead of src
      position: null, // figure this out
      attachment: "", // base64 encoded image
    };
  }

  async decorate() {
    if (this.updating) this.shopifyPayload.id = this.shopifyID;
    this.shopifyPayload.salsify_digital_asset_id =
      this.currentBufferImage.salsify_digital_asset_id;
    this.shopifyPayload.filename = this.currentBufferImage.salsify_filename;
    this.shopifyPayload.product_id = this.currentBufferImage.shopify_product_id;

    // cleaning up these variant id's. Get Brandfolder.
    this.shopifyPayload.variant_ids = this.shopifyPayload.variant_ids.filter(
      (variantID) => {
        return variantID != "null" && variantID != null;
      }
    );

    this.shopifyPayload.variant_ids =
      this.currentBufferImage.shopify_variant_ids;
    if (
      this.currentBufferImage.main_image ||
      this.currentBufferImage.position == 1
    )
      this.shopifyPayload.position = 1;
    this.shopifyPayload.attachment = await this.getEncodedImage();
  }

  async getShopifyPromise() {
    let shopifyResult;
    try {
      const deleteResult = await this.ShopifyStoreConnection.product.update(
        this.shopifyPayload.product_id,
        {
          id: this.shopifyPayload.product_id,
          images: [],
        }
      );
      //if (this.creating) {
      shopifyResult = await this.ShopifyStoreConnection.productImage.create(
        this.shopifyPayload.product_id,
        this.shopifyPayload
      );
      Logger.info(`Created image ${this.shopifyPayload.filename}`);
      //}

      // if (this.updating) {
      //   // wiping old images
      //   const deleteResult = await this.ShopifyStoreConnection.product.update(
      //     this.shopifyPayload.product_id,
      //     {
      //       id: this.shopifyPayload.product_id,
      //       images: [],
      //     }
      //   );
      //   const productImageUpdatePayload = {
      //     product: {
      //       id: this.shopifyPayload.product_id,
      //       images: [
      //         {
      //           id: this.shopifyPayload.id,
      //           position: this.shopifyPayload.position
      //             ? this.shopifyPayload.position
      //             : 0,
      //           product_id: this.shopifyPayload.product_id,
      //           variant_ids: this.shopifyPayload.variant_ids,
      //         },
      //       ],
      //     },
      //   };
      //   // shopifyResult = await this.ShopifyStoreConnection.productImage.update(this.shopifyPayload.product_id, this.shopifyPayload.id, this.shopifyPayload);

      //   shopifyResult = await this.ShopifyStoreConnection.productImage.update(
      //     this.shopifyPayload.product_id,
      //     this.shopifyPayload.id,
      //     productImageUpdatePayload
      //   );
      //   Logger.info(
      //     `Updated image ${this.shopifyPayload.filename} to be added to product ID ${this.shopifyPayload.product_id}`
      //   );
      // }

      this.shopifyID = shopifyResult.id;
      this.currentBufferImage.shopify_id = shopifyResult.id;
      this.currentBufferImage.snapshot = shopifyResult;
      this.currentBufferImage.shopify_src = shopifyResult.src;
      this.currentBufferImage.variants = shopifyResult.variants;
      await this.currentBufferImage.save();
      return shopifyResult;
    } catch (error) {
      Logger.error(
        `Error with image ${this.shopifyPayload.filename}, shopify product id ${this.shopifyPayload.product_id}, image id ${this.shopifyPayload.id}`
      );
      Logger.info(error);
      // if (error.response.statusCode == 404) {
      //   // this happens only if a product was manually deleted using the Shopify admin but is in the buffer.
      //   Logger.info(`Image ${this.shopifyPayload.filename} was not found in Shopify even though it should be.`);
      //   // this.shopifyID = null;
      //   // this.creating = true;
      //   // this.editing = false;
      //   // return this.getShopifyPromise();
      //   this.currentBufferImage.shopify_id = null;
      //   await this.currentBufferImage.save();
      // }
      return error;
    }
  }

  async getShopifyPromiseForProductImageUpdate() {
    let shopifyResult;
    try {
      if (this.creating) {
        shopifyResult = await this.ShopifyStoreConnection.product.update(
          this.shopifyPayload.product_id,
          this.shopifyPayload
        );
        Logger.info(`Created image ${this.shopifyPayload.filename}`);
      }

      if (this.updating) {
        const productImageUpdatePayload = {
          product: {
            id: this.shopifyPayload.product_id,
            images: [
              {
                id: this.shopifyPayload.id,
                position: this.shopifyPayload.position,
                product_id: this.shopifyPayload.product_id,
                variant_ids: this.shopifyPayload.variant_ids,
              },
            ],
          },
        };
        // shopifyResult = await this.ShopifyStoreConnection.productImage.update(this.shopifyPayload.product_id, this.shopifyPayload.id, this.shopifyPayload);

        shopifyResult = await this.ShopifyStoreConnection.productImage.update(
          this.shopifyPayload.product_id,
          this.shopifyPayload.id,
          productImageUpdatePayload
        );
        Logger.info(
          `Updated image ${this.shopifyPayload.filename} to be added to product ID ${this.shopifyPayload.product_id}`
        );
      }

      this.shopifyID = shopifyResult.id;
      this.currentBufferImage.shopify_id = shopifyResult.id;
      this.currentBufferImage.snapshot = shopifyResult;
      this.currentBufferImage.shopify_src = shopifyResult.src;
      this.currentBufferImage.variants = shopifyResult.variants;
      await this.currentBufferImage.save();
      return shopifyResult;
    } catch (error) {
      Logger.error(
        `Error with image ${this.shopifyPayload.filename}, shopify product id ${this.shopifyPayload.product_id}, image id ${this.shopifyPayload.id}`
      );
      Logger.info(error);
      // if (error.response.statusCode == 404) {
      //   // this happens only if a product was manually deleted using the Shopify admin but is in the buffer.
      //   Logger.info(`Image ${this.shopifyPayload.filename} was not found in Shopify even though it should be.`);
      //   // this.shopifyID = null;
      //   // this.creating = true;
      //   // this.editing = false;
      //   // return this.getShopifyPromise();
      //   this.currentBufferImage.shopify_id = null;
      //   await this.currentBufferImage.save();
      // }
      return error;
    }
  }

  async getMetafieldsFromShopifyForThisImage() {
    const listOfMetafields = await this.ShopifyStoreConnection.metafield.list({
      metafield: { owner_resource: "image", owner_id: this.shopifyID },
    });
    return listOfMetafields;
  }

  async getEncodedImage() {
    try {
      const imageURL = this.currentBufferImage.salsify_url;
      const imageFetch = await fetch(imageURL);
      const imageBuffer = await imageFetch.buffer();
      const sharpedBuffer = await sharp(imageBuffer)
        .resize(this.widthInPixels)
        .toBuffer();
      return sharpedBuffer.toString("base64");
    } catch (error) {
      Logger.info(error);
    }
  }
}

module.exports.ShopifyStoreImage = ShopifyStoreImage;
