const _ = require("lodash");
const { Logger } = require("../../helpers/Logger");

class ShopifyStoreCollection {
  constructor(ShopifyStoreConnection, currentBufferCollection) {
    this.ShopifyStoreConnection = ShopifyStoreConnection;
    this.currentBufferCollection = currentBufferCollection;

    // Shopify ID for updates
    this.shopifyID = currentBufferCollection.shopify_collection_id;

    // Creating
    this.creating = this.shopifyID ? false : true;

    // Updating
    this.updating = !this.creating;

    // Basic Shopify attributes
    this.shopifyPayload = {
      title: currentBufferCollection.title,
      handle: currentBufferCollection.handle,
      rules: [],
      sort_order: currentBufferCollection.sort_order,
      template_suffix: currentBufferCollection.template_suffix,
      disjunctive: currentBufferCollection.disjunctive, // this means all collection rules need to be met
    };
  }

  decorate() {
    const collection = this.currentBufferCollection;
    if (collection.rules.length > 0) {
      this.shopifyPayload.rules = collection.rules;
    }
  }

  async getShopifyUpdatePromise() {
    try {
      Logger.info(`Updating collection ${this.shopifyPayload.title}`);
      const updateResult =
        await this.ShopifyStoreConnection.smartCollection.update(
          this.shopifyID,
          this.shopifyPayload
        );
      this.currentBufferCollection.snapshot = updateResult;
      await this.currentBufferCollection.save();
      return updateResult;
    } catch (error) {
      if (error.response.statusCode == 404) {
        // this happens only if a collection was manually deleted using the Shopify admin but still exists in the buffer.
        Logger.info(
          `Collection ${this.shopifyID} has been deleted from Shopify. Re-adding it.`
        );
        return this.getShopifyCreatePromise();
      }
      return error;
    }
  }
  async getShopifyCreatePromise() {
    try {
      Logger.info(`creating collection ${this.shopifyPayload.title}`);
      const createResult =
        await this.ShopifyStoreConnection.smartCollection.create(
          this.shopifyPayload
        );
      this.shopifyID = createResult.id;
      this.currentBufferCollection.shopify_collection_id = createResult.id;
      this.currentBufferCollection.snapshot = createResult;
      await this.currentBufferCollection.save();
      return createResult;
    } catch (error) {
      if (error.response.statusCode == 422) {
        // this happens if the handle matches an existing one on creation.
        Logger.error(
          `Collection ${this.shopifyPayload.title} already exists. Skipping this one.`
        );
      }
      Logger.info(error.response.body);
      return error;
    }
  }
}

module.exports.ShopifyStoreCollection = ShopifyStoreCollection;
