const _ = require("lodash");
const sharp = require("sharp");
const fetch = require("node-fetch");
const { takeRightWhile } = require("lodash");

// This class uses the Salsify buffer product as the basis instead.
// This is because the buffer product, once created, has all the IDs needed.
class ShopifyBufferImage {
  constructor(DigitalAsset, ShopifyIDs, BufferDBModel) {
    this.BufferDBModel = BufferDBModel;
    this.ShopifyIDs = ShopifyIDs;
    this.DigitalAsset = DigitalAsset;

    // Shopify ID for updates
    this.shopifyProductID = this.ShopifyIDs
      ? this.ShopifyIDs.productShopifyID
        ? this.ShopifyIDs.productShopifyID
        : null
      : null;

    // Variant IDs for updates
    this.shopifyVariantIDs = this.ShopifyIDs
      ? this.ShopifyIDs.variantShopifyIDsArray
        ? this.ShopifyIDs.variantShopifyIDsArray
        : []
      : [];

    this.salsify_digital_asset_id = this.DigitalAsset
      ? this.DigitalAsset.salsify_digital_asset_id
        ? this.DigitalAsset.salsify_digital_asset_id
        : null
      : null;

    // updating or adding?
    this.updating = this.shopifyProductID != null;
    this.creating = !this.updating;

    this.widthInPixels = 800; //TODO: figure this out

    // primary image?

    this.auOfficeImageStack = this.DigitalAsset
      ? this.DigitalAsset.au_office_image_stack
        ? this.DigitalAsset.au_office_image_stack
        : null
      : null;

    const main_image = this.DigitalAsset ? this.DigitalAsset.main_image : false;
    const position = this.DigitalAsset ? this.DigitalAsset.position : 0;

    // Basic Salsify/Shopify attributes
    this.attributes = {
      salsify_digital_asset_id: this.salsify_digital_asset_id,
      shopify_product_id: this.shopifyProductID ? this.shopifyProductID : null,
      shopify_variant_ids: this.shopifyVariantIDs ? this.shopifyVariantIDs : [],
      main_image,
      position,
      salsify_url: "",
      salsify_filename: "",
      salsify_format: "",
      shopify_product_id: "",
    };
    //console.log(this.attributes);
  }

  decorate() {
    this.attributes.salsify_digital_asset_id =
      this.DigitalAsset.salsify_digital_asset_id;
    this.attributes.salsify_url = this.DigitalAsset.salsify_url;
    this.attributes.salsify_filename = this.DigitalAsset.filename;
    this.attributes.salsify_format = this.DigitalAsset.format;
    this.attributes.shopify_product_id = this.shopifyProductID;
    //const filteredVariants = this.shopifyVariantIDs;
    // this.shopifyVariants = filteredVariants.map((variant) => {
    //   return {
    //     variant_id: variant.id,
    //     variant_sku: variant.sku,
    //   };
    // });
    this.createImageRelations();
  }
  // decorate() {
  //   this.attributes.salsify_digital_asset_id = this.DigitalAsset["salsify:id"];
  //   this.attributes.salsify_url = this.DigitalAsset["salsify:url"];
  //   this.attributes.salsify_filename = this.DigitalAsset["salsify:filename"];
  //   this.attributes.salsify_format = this.DigitalAsset["salsify:format"];
  //   this.attributes.shopify_product_id =
  //     this.ShopifyBufferProduct.shopify_product_id;
  //   const filteredVariants = this.ShopifyBufferProduct.variants.filter(
  //     (variant) => {
  //       return variant.id != null && variant.id != null;
  //     }
  //   );
  //   this.shopifyVariants = filteredVariants.map((variant) => {
  //     return {
  //       variant_id: variant.id,
  //       variant_sku: variant.sku,
  //     };
  //   });
  //   this.createImageRelations();
  // }

  createImageRelations() {
    // set up the data set for one image.
  }

  async getEncodedImage() {
    const imageFetch = await fetch(this.attributes.salsify_url);
    const imageBuffer = await imageFetch.buffer();
    const sharpedBuffer = await sharp(imageBuffer)
      .resize(this.widthInPixels)
      .toBuffer();
    return sharpedBuffer.toString("base64");
  }

  async bufferUpsert() {
    const foundExisting = await this.BufferDBModel.findOne({
      where: {
        salsify_digital_asset_id: this.attributes.salsify_digital_asset_id,
        shopify_product_id: this.attributes.shopify_product_id,
      },
    });

    if (foundExisting) {
      // let variantsSet = new Set(foundExisting.shopify_variant_ids);
      // for (const anotherVariantID of this.attributes.shopify_variant_ids) {
      //   variantsSet.add(anotherVariantID);
      // }
      // this.attributes.shopify_variant_ids = Array.from(variantsSet);

      this.attributes.shopify_variant_ids = this.attributes.shopify_variant_ids;

      const result = await foundExisting.update(this.attributes);
      console.log(
        `Updated buffer image with id ${this.attributes.salsify_digital_asset_id}`
      );
      return result;
    } else {
      const result = await this.BufferDBModel.create(this.attributes);
      console.log(
        `Created buffer image with id ${this.attributes.salsify_digital_asset_id}`
      );
      return result;
    }
  }
}

module.exports.ShopifyBufferImage = ShopifyBufferImage;
