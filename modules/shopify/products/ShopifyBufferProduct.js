const _ = require("lodash");
const slugify = require("slugify");
const moment = require("moment");
const { NumberTools } = require("../../helpers/NumberTools");
const { PricingError } = require("../errors/ProductErrors");
class ShopifyBufferProduct {
  constructor(
    SalsifyBufferParent,
    allSalsifyProductsForThisParent,
    allERPProductsForThisParent,
    categoryDefinitionReference,
    pricingReference,
    BufferDBModel
  ) {
    this.BufferDBModel = BufferDBModel; // Brand-specific ORM model for buffer entries
    this.SalsifyBufferParent = SalsifyBufferParent; // Salsify Parent ORM model
    this.allSalsifyProductsForThisParent = allSalsifyProductsForThisParent;
    this.allERPProductsForThisParent = allERPProductsForThisParent;
    this.categoryDefinitionReference = categoryDefinitionReference;
    this.pricingReference = pricingReference;

    this.variantOptionStatuses = {
      NO_VARIANTS: "NO_OPTIONS",
      COLOUR_ONLY: "COLOUR_ONLY",
      SIZE_ONLY: "SIZE_ONLY",
      COLOUR_AND_SIZE: "COLOUR_AND_SIZE",
    };

    this.shopifyProductStatuses = {
      ACTIVE: "active",
      ARCHIVED: "archived",
      DRAFT: "draft",
    };

    // Shopify ID for updates
    this.shopifyID = null;

    // Shopify product level metadata definitions
    this.metadata = {};

    // Presence of size/colour options
    this.variantOptionStatus = "";

    // Simple or complex product?
    this.isSimpleProduct;
    if (this.SalsifyBufferParent) {
      this.isSimpleProduct =
        this.SalsifyBufferParent.simple_or_configurable == "simple";
    }

    // More accurate size and colour options consolidation
    this.allColourOptions = [];
    this.allSizeOptions = [];

    // Basic Shopify attributes
    this.attributes = {
      salsify_system_id: "",
      vendor: "",
      body_html: "",
      title: "",
      sku: "",
      status: this.shopifyProductStatuses.DRAFT,
      product_type: "",
      tags: [],
      variants: [],
      images: [],
      metafields: [],
      options: [],
      snapshot: {},
    };
  }

  consolidateColourAndSizeOptions() {
    this.allColourOptions = this.allSalsifyProductsForThisParent
      .filter((product) => product.colour)
      .map((product) => product.colour)
      .filter((value, index, self) => {
        return self.indexOf(value) === index;
      });
    if (this.SalsifyBufferParent.colour) {
      this.allColourOptions.push(this.SalsifyBufferParent.colour);
    }
    if (this.SalsifyBufferParent.snapshot["COLOUR VARIANT"]) {
      this.allColourOptions.push(
        this.SalsifyBufferParent.snapshot["COLOUR VARIANT"]
      );
    }
    this.allSizeOptions = this.allSalsifyProductsForThisParent
      .filter((product) => product.size)
      .map((product) => product.size)
      .filter((value, index, self) => {
        return self.indexOf(value) === index;
      });
    if (this.SalsifyBufferParent.size) {
      this.allSizeOptions.push(this.SalsifyBufferParent.size);
    }
  }

  optionStatus() {
    let productHasColourValue = this.allColourOptions.length > 0;
    if (this.isSimpleProduct) {
      productHasColourValue = this.SalsifyBufferParent.colour != null;
    }
    let productHasSizeValue = this.allSizeOptions.length > 0;
    if (this.isSimpleProduct) {
      productHasSizeValue = this.SalsifyBufferParent.size != null;
    }
    if (!productHasColourValue && !productHasSizeValue)
      return this.variantOptionStatuses.NO_VARIANTS;
    if (productHasColourValue && !productHasSizeValue)
      return this.variantOptionStatuses.COLOUR_ONLY;
    if (!productHasColourValue && productHasSizeValue)
      return this.variantOptionStatuses.SIZE_ONLY;
    if (productHasColourValue && productHasSizeValue)
      return this.variantOptionStatuses.COLOUR_AND_SIZE;
  }

  updateSimpleVariant() {
    for (
      let index = 0;
      index < this.allERPProductsForThisParent.length;
      index++
    ) {
      const ERPProduct = this.allERPProductsForThisParent[index];
      const salsifyProduct = this.SalsifyBufferParent;
      let variant = {
        sku: ERPProduct.sku,
        inventory_quantity: ERPProduct.stockQuantity
          ? ERPProduct.stockQuantity
          : 0,
        inventory_management: "shopify",
        price: ERPProduct.retailPrice ? ERPProduct.retailPrice : 0,
      };
      variant = this.addOptionsAttributeToVariant(variant, salsifyProduct);
      this.attributes.variants.push(variant);
    }
  }

  updateVariants() {
    for (
      let index = 0;
      index < this.allERPProductsForThisParent.length;
      index++
    ) {
      const ERPProduct = this.allERPProductsForThisParent[index];
      const salsifyProduct = this.allSalsifyProductsForThisParent[index];
      const prices = this.constructVariantPricing(ERPProduct);
      if (!prices) {
        throw new PricingError(ERPProduct);
      }

      const price = prices.price;
      const compare_at_price = prices.compare_at_price;

      let variant = {
        sku: ERPProduct.sku,
        barcode: ERPProduct.barcode,
        inventory_quantity: ERPProduct.stockQuantity
          ? ERPProduct.stockQuantity
          : 0,
        inventory_management: "shopify",
        price,
        compare_at_price,
      };
      variant = this.addOptionsAttributeToVariant(variant, salsifyProduct);
      this.attributes.variants.push(variant);
    }
  }

  productStatusBasedOnVariants() {
    const statuses = this.shopifyProductStatuses;
    const anyEnabledProducts = this.allERPProductsForThisParent.filter(
      (product) => {
        return product.productSnapshot.b2c_enabled === "1";
      }
    );
    // TODO: use Promise Date from ERPProduct
    return anyEnabledProducts.length > 0 ? statuses.ACTIVE : statuses.DRAFT;
  }

  decorate() {
    const bufferProductAttributes = this.attributes;
    const salsifyParent = this.SalsifyBufferParent;
    const allSalsifyAttributes = this.SalsifyBufferParent.snapshot;

    bufferProductAttributes.sku = salsifyParent.sku;
    bufferProductAttributes.salsify_system_id =
      allSalsifyAttributes["salsify:system_id"];
    bufferProductAttributes.title = salsifyParent.name;
    bufferProductAttributes.body_html =
      allSalsifyAttributes["LONG PRODUCT DESCRIPTION"];
    bufferProductAttributes.vendor = salsifyParent.brand;
    bufferProductAttributes.status = this.productStatusBasedOnVariants();
    bufferProductAttributes.options = this.addOptionsAttribute();
  }

  decorateSimpleProduct() {
    const bufferProductAttributes = this.attributes;
    const salsifyParent = this.SalsifyBufferParent;
    const allSalsifyAttributes = this.SalsifyBufferParent.snapshot;
    bufferProductAttributes.sku = salsifyParent.sku;
    bufferProductAttributes.salsify_system_id =
      allSalsifyAttributes["salsify:system_id"];
    bufferProductAttributes.title = salsifyParent.name;
    bufferProductAttributes.body_html =
      allSalsifyAttributes["LONG PRODUCT DESCRIPTION"];
    bufferProductAttributes.vendor = salsifyParent.brand;
    bufferProductAttributes.status = this.productStatusBasedOnVariants();
    bufferProductAttributes.options = this.addOptionsAttribute();
  }

  addOptionsAttribute() {
    this.consolidateColourAndSizeOptions();
    this.variantOptionStatus = this.optionStatus();
    switch (this.variantOptionStatus) {
      case this.variantOptionStatuses.NO_VARIANTS:
        return [];
      case this.variantOptionStatuses.SIZE_ONLY:
        return [{ name: "Size", position: 1, values: this.allSizeOptions }];
      case this.variantOptionStatuses.COLOUR_ONLY:
        return [{ name: "Color", position: 1, values: this.allColourOptions }];
      case this.variantOptionStatuses.COLOUR_AND_SIZE:
        return [
          { name: "Color", position: 1, values: this.allColourOptions },
          {
            name: "Size",
            position: 2,
            values: this.allSizeOptions,
          },
        ];
      default:
        return [];
    }
  }

  addOptionsAttributeToVariant(variant, currentSalsifyProduct) {
    switch (this.variantOptionStatus) {
      case this.variantOptionStatuses.NO_VARIANTS:
        break;
      case this.variantOptionStatuses.SIZE_ONLY:
        variant.option1 = currentSalsifyProduct.size;
        break;
      case this.variantOptionStatuses.COLOUR_ONLY:
        variant.option1 = currentSalsifyProduct.colour;
        break;
      case this.variantOptionStatuses.COLOUR_AND_SIZE:
        variant.option1 = currentSalsifyProduct.colour;
        variant.option2 = currentSalsifyProduct.size;
        break;
    }
    return variant;
  }

  async findAll() {
    return this.BufferDBModel.findAll({});
  }

  async updateShopifyID(shopifyID) {
    const foundExisting = await this.BufferDBModel.findOne({
      where: {
        salsify_system_id: this.attributes.salsify_system_id,
      },
    });

    if (foundExisting) {
      await foundExisting.update({
        shopify_product_id: shopifyID,
      });
    }
  }

  async bufferUpsert() {
    const foundExisting = await this.BufferDBModel.findOne({
      where: {
        salsify_system_id: this.attributes.salsify_system_id,
      },
    });

    if (foundExisting) {
      this.attributes.variants = this.mergeOldAndNewVariants(foundExisting);
      for (const key in this.attributes) {
        if (foundExisting[key] != undefined) {
          const newValue = this.attributes[key];
          foundExisting[key] = newValue;
        }
      }
      const thereAreChanges = foundExisting.changed().length > 0;
      if (thereAreChanges) {
        const updatedItem = await foundExisting.save();
        return updatedItem;
      } else {
        return false;
      }
    } else {
      return await this.BufferDBModel.create(this.attributes);
    }
  }

  mergeOldAndNewVariants(shopifyBufferEntry) {
    const variantsFromBuffer = JSON.parse(
      JSON.stringify(shopifyBufferEntry.variants)
    );
    let updatedVariants = [];
    for (let currentNewVariant of this.attributes.variants) {
      let matchingVariant = _.find(variantsFromBuffer, {
        sku: currentNewVariant.sku,
      });
      if (matchingVariant) {
        let mergedVariant = _.merge(matchingVariant, currentNewVariant);
        if (mergedVariant.hasOwnProperty("image_id")) {
          if (mergedVariant.image_id == null) {
            delete mergedVariant.image_id;
          }
        }
        updatedVariants.push(mergedVariant);
      } else {
        updatedVariants.push(currentNewVariant);
      }
    }
    return updatedVariants;
  }

  //TODO: centralise this when ProductVariant support is 100% done
  constructVariantPricing(ERPProduct) {
    const foundPricingRule = _.find(this.pricingReference, {
      ItemNumber: ERPProduct.sku,
    });

    const defaultPrice = {
      price: NumberTools.priceToShopifyFormat(ERPProduct.retailPrice),
      compare_at_price: null,
    };

    if (!foundPricingRule) {
      return defaultPrice;
    }
    if (!foundPricingRule.SpecialFrom || !foundPricingRule.SpecialTo) {
      return defaultPrice;
    }
    const today = moment();
    const dateFormat = "DD/MM/YYYY";
    const SpecialFrom = moment(foundPricingRule.SpecialFrom, dateFormat);
    const SpecialTo = moment(foundPricingRule.SpecialTo, dateFormat);
    if (today.isBetween(SpecialFrom, SpecialTo)) {
      const specialPrice = {
        price: NumberTools.priceToShopifyFormat(foundPricingRule.SpecialPrice),
        compare_at_price: NumberTools.priceToShopifyFormat(
          ERPProduct.retailPrice
        ),
      };
      return specialPrice;
    }
    return defaultPrice;
  }
}

module.exports.ShopifyBufferProduct = ShopifyBufferProduct;
