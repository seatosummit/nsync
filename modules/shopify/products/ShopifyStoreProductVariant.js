const _ = require("lodash");
const {
  ShopifyBufferProductVariant: BufferDBModel,
} = require("../../../models");
class ShopifyStoreProductVariant {
  constructor(shopifyConnection, bufferVariant) {
    this.BufferDBModel = BufferDBModel;
    this.bufferVariant = bufferVariant;
    this.ShopifyStoreConnection = shopifyConnection;

    this.id = this.bufferVariant
      ? this.bufferVariant.shopify_id
        ? this.bufferVariant.shopify_id
        : null
      : null;
    this.creating = this.id === null;
    this.updating = !this.creating;
    this.title = this.bufferVariant ? this.bufferVariant.shopify_title : null;

    // Basic Shopify Variant attributes
    this.shopifyPayload = {
      id: 0,
      product_id: 0,
      sku: "",
      price: 0,
      compare_at_price: 0,
    };
  }

  decoratePrice() {
    this.shopifyPayload.id = this.bufferVariant.shopify_id;
    this.shopifyPayload.barcode = this.bufferVariant.shopify_barcode;
    this.shopifyPayload.product_id = this.bufferVariant.shopify_product_id;
    this.shopifyPayload.sku = this.bufferVariant.shopify_sku;
    this.shopifyPayload.price = parseFloat(this.bufferVariant.shopify_price);
    this.shopifyPayload.compare_at_price =
      this.bufferVariant.shopify_compare_at_price &&
      this.bufferVariant.shopify_compare_at_price > 0
        ? parseFloat(this.bufferVariant.shopify_compare_at_price)
        : null;
  }

  async getShopifyPromise() {
    let shopifyResult;
    try {
      if (this.creating) {
        // shopifyResult = await this.ShopifyStoreConnection.productVariant.create(
        //   this.shopifyPayload
        // );
        // console.log(`Created variant ${this.shopifyPayload.title}`);
      }

      if (this.updating) {
        shopifyResult = await this.ShopifyStoreConnection.productVariant.update(
          this.id,
          this.shopifyPayload
        );
        //console.log(`Updated variant ${this.shopifyPayload.sku}`);
      }
      // TODO: implement this once product variants is finished
      // await this.currentBufferProductVariant.save();
      return shopifyResult;
    } catch (error) {
      console.error(`Error with sku ${this.shopifyPayload.sku}`);
      console.log(error.response.body);
      if (error.response.statusCode == 404) {
        // this happens only if a variant product was manually deleted or updated using the Shopify admin but is still in the buffer.
        console.log(
          `Variant ${this.shopifyPayload.sku} doesn't exist in Shopify.`
        );
        //return this.getShopifyCreatePromise();
      }
      return error;
    }
  }
}

module.exports.ShopifyStoreProductVariant = ShopifyStoreProductVariant;
