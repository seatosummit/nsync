const ShopifyShipmentImport = require("./ShopifyShipmentImport");
const { ShopifyConfig } = require("../../helpers/ShopifyConfig");
const { IntegrationRuleset } = require("../../helpers/IntegrationRuleset");
const { Logger } = require("../../helpers/Logger");
const { checkForRule } = require("../../helpers/IntegrationRuleTools");
const rules = {
  ORDERS_UPDATEFULFILMENT: "ordersUpdateFulfilment",
};

const importShopifyShipments = async () => {
  const ShopifyStores = await ShopifyConfig.getConfigsFromDatabase();
  for (const ShopifyStore of ShopifyStores) {
    try {
      if (
        !checkForRule({ ShopifyStore, rule: rules.ORDERS_UPDATEFULFILMENT })
      ) {
        continue;
      }

      const shipmentImporter = new ShopifyShipmentImport(
        ShopifyStore.shopify_config,
        ShopifyStore.shopify_config.shipping
      );
      console.log("Get Shopify Shipments");
      const shipmentFiles = await shipmentImporter.getShopifyShipments();
      console.log("Parse Shipments CSVs");
      const shipmentFilesData = await shipmentImporter.parseShipmentsCSVs(
        shipmentFiles
      );
      console.log("Import Shipments");
      await shipmentImporter.importShipments(shipmentFilesData);
    } catch (error) {
      console.error(`Error importing shipments: ${error}`);
    }
  }
  process.exit();
};

module.exports.importShopifyShipments = importShopifyShipments;
