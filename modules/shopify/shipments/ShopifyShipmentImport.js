const { ShopifyAPI } = require("../../helpers/apitools");
const { moveFile, getFiles, parseCSV, createDir } = require("../../helpers/fileTools");
const { FTPTools } = require("../../helpers/ftpTools");
const { ShopifyOrder } = require("../../../models");
const _ = require("lodash");
require("dotenv").config();

class ShopifyShipmentImport {
  constructor(storeConfig, shippingConfig) {
    this.shopify = ShopifyAPI(storeConfig);
    this.channelId = storeConfig.id;
    this.channelName = storeConfig.name.toLowerCase();
    this.filePath = process.env.SHOPIFY_SHIPMENTS;
    this.ftpPath = `${
      process.env.SHOPIFY_SHIPMENTS_FTP_LOCATION
    }${this.channelName}/`;
    this.channelPath = this.filePath + this.channelName + "/";
    this.location_id = storeConfig.location_id;
    this.suffix = storeConfig.suffix;
    this.shippingMethods = shippingConfig;
    this.ftpTools = new FTPTools({
      host: process.env.DATA_FTP_HOST,
      user: process.env.DATA_FTP_USER,
      password: process.env.DATA_FTP_PASSWORD,
      port: process.env.DATA_FTP_PORT,
    });
  }

  decorate(fulfillment) {
    const decoratedFulfillment = {
      tracking_number: fulfillment.ConsignmentNumber,
      tracking_company: this.shippingMethods[fulfillment.ShippingMethod],
      location_id: this.location_id,
      notify_customer: true,
    };
    return decoratedFulfillment;
  }

  async getShopifyShipments(channelPath = this.channelPath) {
    try {
      await createDir(channelPath);
      await this.ftpTools.getFiles(`${this.ftpPath}`,channelPath);

      let shipmentFiles = await getFiles(channelPath, "csv");
      if (shipmentFiles.length == 0) {
        console.log(`No shipments to process for ${this.channelName}`);
      } else {
        return shipmentFiles;
      }
    } catch (error) {
      console.error(error);
    }
  }

  async parseShipmentsCSVs(shipmentFiles) {
    if (shipmentFiles) {
      const shipmentImportFilePromises = await shipmentFiles.map(
        (shipmentFile) => {
          let currentFilePath = `${this.channelPath}${shipmentFile}`;

          console.log(
            `Parsing ${this.channelName} shipment file: ${currentFilePath}`
          );
          const shipmentData = parseCSV(currentFilePath);
          this.ftpTools.remove(`${this.ftpPath}${shipmentFile}`);
          moveFile(
            shipmentFile,
            this.channelPath,
            `${this.channelPath}archive/`
          );

          return shipmentData;
        }
      );

      const shipmentFilesData = await Promise.all(shipmentImportFilePromises);
      return shipmentFilesData;
    }
  }

  async importShipments(shipmentFilesData, channelId = this.channelId) {
    console.log(shipmentFilesData);
    if (shipmentFilesData) {
      const bufferOrders = await this.getBufferOrders(channelId);
      try {
        for (const shipmentData of shipmentFilesData) {
          const shipmentImportPromises = await shipmentData.map((shipment) => {
            console.log(shipment.WebID);
            let bufferOrder = _.find(bufferOrders, {
              orderNumber: shipment.WebID,
            });
            if (bufferOrder) {
              return this.createFulfillment(bufferOrder, shipment);
            }
            else{
              return false;
            }
          });

          const fulfillments = await Promise.all(shipmentImportPromises).catch((e) =>
            console.error(`Error importing this batch of shipments: ${e}`)
          );
          return fulfillments;
        }
      } catch (error) {
        console.error(`Error importing shipments: ${error}`);
      }
    }
    else{
      return false;
    }
  }

  async createFulfillment(bufferOrder, shipment) {
    try {
      await this.shopify.fulfillment.create(bufferOrder.orderID, this.decorate(shipment));
      bufferOrder.fulfilled = true;
      console.log('Updated Order Buffer');
      await bufferOrder.update({fulfilled: true});
      console.log(
        `Created fulfillment for ${this.channelName} invoice: ${shipment.WebID}`
      );
    } catch (error) {
      console.error(
        `Error importing order #${shipment.WebID} shipment: ${error}`
      );
    }
  }

  async getBufferOrders(channelId) {
    return ShopifyOrder.findAll({
      where: {
        salesChannelId: channelId,
        xmlExported: true,
        fulfilled: false,
      },
    });
  }
}

module.exports = ShopifyShipmentImport;