class OrphanSalsifySKUError extends Error {
  constructor(salsifyProduct) {
    const amendedMessage = `Salsify product ${salsifyProduct.sku} doesn't have a match in ERP`;
    super(amendedMessage);
    this.name = "OrphanSalsifySKUError";
  }
}
class PricingError extends Error {
  constructor(ERPProduct) {
    const amendedMessage = `There's no pricing available for ERP product ${ERPProduct.sku}`;
    super(amendedMessage);
    this.name = "PricingError";
  }
}
class ShopifyConnectionError extends Error {
  constructor(error) {
    const amendedMessage = `Problem initialising a Shopify connector: ${error}`;
    super(amendedMessage);
    this.name = "ShopifyConnectionError";
  }
}
class ShopifyConfigError extends Error {
  constructor(error) {
    const amendedMessage = `Problem getting Shopify configs: ${error}`;
    super(amendedMessage);
    this.name = "ShopifyConfigError";
  }
}
class ShopifyGraphQLError extends Error {
  constructor(userErrors) {
    const concatenatedErrorMessages = []
      .concat(userErrors)
      .map((error) => error.message ?? error)
      .join(", ");
    const amendedMessage = `Problem running Shopify GraphQL query: ${concatenatedErrorMessages}`;
    super(amendedMessage);
    this.name = "ShopifyGraphQLError";
  }
}

class NoVariantsError extends Error {
  constructor(payload) {
    const amendedMessage = `Product with sku ${payload.sku} has no variants. It doesn't qualify as a Shopify product.`;
    super(amendedMessage);
    this.response = { statusCode: 500 };
    this.name = "NoVariantsError";
  }
}

module.exports.OrphanSalsifySKUError = OrphanSalsifySKUError;
module.exports.PricingError = PricingError;
module.exports.NoVariantsError = NoVariantsError;
module.exports.ShopifyConnectionError = ShopifyConnectionError;
module.exports.ShopifyConfigError = ShopifyConfigError;
module.exports.ShopifyGraphQLError = ShopifyGraphQLError;
