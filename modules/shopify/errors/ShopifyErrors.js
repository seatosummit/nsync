class ShopifyProductNotFoundError extends Error {
  constructor(shopifyPayload) {
    const amendedMessage = `Shopify product ${shopifyPayload.title} with id ${shopifyPayload.id} wasn't found in Shopify.`;
    super(amendedMessage);
    this.name = "ShopifyProductNotFoundError";
  }
}

class GeneralShopifyAPIError extends Error {
  constructor(shopifyPayload) {
    const amendedMessage = `General Shopify API error with product ${shopifyPayload.title} having id ${shopifyPayload.id}.`;
    super(amendedMessage);
    this.name = "GeneralShopifyAPIError";
  }
}

class TooManyAPIRequestsError extends Error {
  constructor(shopifyPayload, shopifyConnectionSettings) {
    const amendedMessage = `Shopify API is getting too many requests. You need to check your API call throttling. Current throttle: ${shopifyConnectionSettings}`;
    super(amendedMessage);
    this.name = "TooManyAPIRequestsError";
  }
}

module.exports.ShopifyProductNotFoundError = ShopifyProductNotFoundError;
module.exports.GeneralShopifyAPIError = GeneralShopifyAPIError;
module.exports.TooManyAPIRequestsError = TooManyAPIRequestsError;
