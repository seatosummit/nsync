const ShopifyOrderExport = require("./ShopifyOrderExport");
const { ShopifyConfig } = require("../../helpers/ShopifyConfig");
const {
  checkForRule,
  getOrdersFilter,
  getCustomOrderNotificationEmail,
} = require("../../helpers/IntegrationRuleTools");
const EmailService = require("../../emails/EmailService");
const { Logger } = require("../../helpers/Logger");
const ShopifyOrderStockAudit = require("./ShopifyOrderStockAudit");
const WMSBreezeAPI = require("../../helpers/WMSBreezeAPI");
const { ShopifyAPI } = require("../../helpers/ShopifyAPI");

const exportShopifyOrders = async (args) => {
  const machineNameFilter = args?.machineNameFilter;
  const uploadXML = args?.uploadXML ?? true;
  const sendEmailNotifications = args?.sendEmail ?? true;
  const ShopifyStores = await ShopifyConfig.getConfigsFromDatabase({
    machineNameOfSalesChannel: machineNameFilter,
  });
  for (const ShopifyStore of ShopifyStores) {
    try {
      if (!checkForRule({ ShopifyStore, rule: "ordersExport" })) {
        continue;
      }
      let orderExporter = new ShopifyOrderExport(ShopifyStore.shopify_config);
      Logger.info(`Get ${ShopifyStore.shopify_config.name} Shopify Orders...`);
      const shopifyOrders = await orderExporter.getShopifyOrders({
        orderExportFilter: getOrdersFilter({ ShopifyStore }),
      });
      const filteredShopifyOrders = await orderExporter.filterBufferedOrders(
        shopifyOrders
      );
      if (filteredShopifyOrders.length === 0) {
        Logger.info(
          `No new orders found for ${ShopifyStore.shopify_config.name}. Moving on`
        );
        continue;
      }
      Logger.info(`Export Orders To XML...`);
      const savedOrders = await orderExporter.exportOrdersToXML({
        ordersFromShopify: filteredShopifyOrders,
      });
      if (uploadXML) {
        Logger.info(`Send XML Files...`);
        await orderExporter.sendXMLFiles(savedOrders);
        Logger.info(`XML Export Complete.`);
      }

      if (sendEmailNotifications) {
        if (checkForRule({ ShopifyStore, rule: "customOrderNotification" })) {
          const emailer = new EmailService();
          const emailRecipient = getCustomOrderNotificationEmail({
            ShopifyStore,
          });
          Logger.info(
            `Sending notification to ${emailRecipient} for ${
              filteredShopifyOrders.length
            } order${filteredShopifyOrders.length > 0 ? "s" : ""}`
          );
          const results = await emailer.sendNotificationForOrders({
            emailRecipient,
            orders: filteredShopifyOrders,
            ShopifyStore,
          });
        }
      }
    } catch (error) {
      Logger.error(`Error importing orders: ${error}`);
    }
  }
  process.exit();
};

const tagOrdersWithNoStock = async (args) => {
  const machineNameFilter = args?.machineNameFilter;
  const ShopifyStores = await ShopifyConfig.getConfigsFromDatabase({
    machineNameOfSalesChannel: machineNameFilter,
  });
  for (const ShopifyStore of ShopifyStores) {
    try {
      if (!checkForRule({ ShopifyStore, rule: "tagOrdersWithNoStock" })) {
        continue;
      }
      let orderAuditor = new ShopifyOrderStockAudit(
        ShopifyStore.shopify_config
      );
      Logger.info(
        `Auditing ${ShopifyStore.shopify_config.name} Shopify Orders...`
      );
      await orderAuditor.shopifyGraphQL.connect();
      await orderAuditor.getLatestUnfulfilledOrders();
      if (orderAuditor.shopifyUnfulfilledOrders.length === 0) {
        Logger.info(
          `No unfulfilled orders found for ${ShopifyStore.shopify_config.name}. Moving on`
        );
        continue;
      }

      await orderAuditor.getErpProducts({
        viewToUse: "vSTS_WebProductInfo_AF",
      });
      const stockAuditResult = orderAuditor.auditLineItems();

      for (const order of stockAuditResult.ordersNotPassingAudit) {
        if (order.tags.includes("rivervale-can-not-fulfill")) continue;
        await orderAuditor.shopifyGraphQL.addTagsToOrder({
          GraphQLOrderID: order.id,
          tagsToAdd: ["rivervale-can-not-fulfill"],
        });
      }
      for (const order of stockAuditResult.ordersPassingAudit) {
        if (!order.tags.includes("rivervale-can-not-fulfill")) continue;
        await orderAuditor.shopifyGraphQL.removeTagsFromOrder({
          GraphQLOrderID: order.id,
          tagsToRemove: ["rivervale-can-not-fulfill"],
        });
      }
      Logger.info(`... done auditing and tagging orders`);
    } catch (error) {
      Logger.error(`Error auditing orders: ${error}`);
    }
  }
  process.exit();
};

const exportOneShopifyOrder = async (args) => {
  const salesChannelMachineName = args?.salesChannelMachineName;
  const orderNumber = args?.orderNumber;
  if (!salesChannelMachineName || !orderNumber)
    throw `salesChannelMachineName and orderNumber needed.`;
  const ShopifyStore = (
    await ShopifyConfig.getConfigsFromDatabase({
      machineNameOfSalesChannel: salesChannelMachineName,
    })
  )[0];
  try {
    let orderExporter = new ShopifyOrderExport(ShopifyStore.shopify_config);
    Logger.info(`Get ${ShopifyStore.shopify_config.name} Shopify Orders...`);
    const shopifyOrders = await orderExporter.getOneShopifyOrder({
      orderNumber,
    });
    Logger.info(`Export order #${orderNumber} To XML...`);
    const savedOrders = await orderExporter.exportOrdersToXML({
      ordersFromShopify: shopifyOrders,
      bypassBufferCheck: true,
    });
    Logger.info(`Send XML File to SFTP...`);
    await orderExporter.sendXMLFiles(savedOrders);
    Logger.info(`XML Export of order #${orderNumber} Complete.`);
  } catch (error) {
    Logger.error(`Error exporting order: ${error}`);
  }
};

module.exports.exportShopifyOrders = exportShopifyOrders;
module.exports.exportOneShopifyOrder = exportOneShopifyOrder;
module.exports.tagOrdersWithNoStock = tagOrdersWithNoStock;
