const { ShopifyAPI } = require("../../helpers/apitools");
const { ShopifyAPI: ShopifyGraphQLAPI } = require("../../helpers/ShopifyAPI");
const { ShopifyOrder } = require("../../../models");
const { Sequelize } = require("sequelize");
const Op = Sequelize.Op;
const { FTPTools } = require("../../helpers/ftpTools");
const { writeFile } = require("../../helpers/fileTools");
const { create } = require("xmlbuilder2");
const moment = require("moment");
const _ = require("lodash");
const { Logger } = require("../../helpers/Logger");
const { valueExistsAndIsNotEmpty } = require("../../helpers/stringTools");
const WMSBreezeAPI = require("../../helpers/WMSBreezeAPI");
require("dotenv").config();

class ShopifyOrderStockAuditor {
  constructor(storeConfig) {
    this.shopify = ShopifyAPI(storeConfig);
    this.shopifyGraphQL = new ShopifyGraphQLAPI(storeConfig);
    this.channelId = storeConfig.id;
    this.channelName = storeConfig.name;
    this.fromDate = moment()
      .subtract(storeConfig.orderAuditDays ?? 30, "days")
      .toISOString();
    this.shopifyUnfulfilledOrders = [];
    this.erpProducts = [];
  }

  async getLatestUnfulfilledOrders() {
    try {
      const unfulfilledOrders = await this.shopifyGraphQL.getUnfulfilledOrders({
        noTag: "sts-rivervale",
        financialStatus: "paid",
        fromDate: this.fromDate,
      });
      this.shopifyUnfulfilledOrders = unfulfilledOrders;
    } catch (error) {
      throw error;
    }
  }

  async getErpProducts(args) {
    const viewToUse = args?.viewToUse ?? "vSTS_WebProductInfo_PEP_V2";
    const erpProducts = (await WMSBreezeAPI.selectFromView(viewToUse)).map(
      (productVariant) => {
        return {
          sku: productVariant.sku,
          stockQuantity: parseInt(productVariant.qtyavailable),
        };
      }
    );
    this.erpProducts = erpProducts;
  }
  auditLineItems() {
    const ordersPassingAudit = [];
    const ordersNotPassingAudit = [];

    loopingOrders: for (const order of this.shopifyUnfulfilledOrders) {
      const auditedOrder = { id: order.id, tags: order.tags ?? [] };
      for (const lineItem of order.lineItems) {
        const matchingErpProduct = _.find(this.erpProducts, {
          sku: lineItem.sku,
        });
        if (!matchingErpProduct) {
          Logger.error(
            `Order ${order.name} line item ${lineItem.sku} wasn't found in the D2C product feed from WMS.`
          );
          ordersNotPassingAudit.push(auditedOrder);
          continue loopingOrders;
        }
        if (matchingErpProduct.stockQuantity - lineItem.quantity < 0) {
          Logger.info(
            `Order ${order.name} line item ${lineItem.sku} doesn't have enough stock in Rivervale.`
          );
          ordersNotPassingAudit.push(auditedOrder);
          continue loopingOrders;
        }
      }
      ordersPassingAudit.push(auditedOrder);
    }
    return {
      ordersPassingAudit,
      ordersNotPassingAudit,
    };
  }
}

module.exports = ShopifyOrderStockAuditor;
