const { ShopifyAPI } = require("../../helpers/apitools");
const { ShopifyAPI: ShopifyGraphQLAPI } = require("../../helpers/ShopifyAPI");
const { ShopifyOrder } = require("../../../models");
const { Sequelize } = require("sequelize");
const Op = Sequelize.Op;
const { FTPTools } = require("../../helpers/ftpTools");
const { writeFile } = require("../../helpers/fileTools");
const { create } = require("xmlbuilder2");
const moment = require("moment");
const _ = require("lodash");
const { Logger } = require("../../helpers/Logger");
const { valueExistsAndIsNotEmpty } = require("../../helpers/stringTools");
require("dotenv").config();

class ShopifyOrderExport {
  constructor(storeConfig) {
    this.shopify = ShopifyAPI(storeConfig);
    this.shopifyGraphQL = new ShopifyGraphQLAPI(storeConfig);
    this.xmlSettings = storeConfig?.xmlSettings ?? false;
    this.channelId = storeConfig.id;
    this.channelName = storeConfig.name;
    this.cardfile = storeConfig.cardfile;
    this.suffix = storeConfig.suffix;
    this.datapelDTD = storeConfig.datapelDTD;
    this.fromdate = moment()
      .subtract(storeConfig.exportDays, "days")
      .toISOString();
    this.filePath = process.env.XML_SAVE_LOCATION;
    this.ftpPath = process.env.XML_FTP_LOCATION;
    this.ftpArchivePath = process.env.XML_ARCHIVE_FTP_LOCATION;
    this.ftpTools = new FTPTools({
      host: process.env.DATA_FTP_HOST,
      user: process.env.DATA_FTP_USER,
      password: process.env.DATA_FTP_PASSWORD,
    });
  }

  decorate(order) {
    const decoratedOrder = {
      salesChannelId: this.channelId,
      orderID: order.id,
      orderNumber: order?.name?.replace("#", ""),
      checkoutID: order.checkout_id,
      orderSnapshot: order,
      customerEmail: order.customer["email"],
    };

    console.info(`Decorate ${decoratedOrder.salesChannelId} order...`);
    return decoratedOrder;
  }

  async getBufferOrders(createdAt) {
    console.info(`Get Buffer Orders for ${this.channelId}...`);
    return ShopifyOrder.findAll({
      raw: true,
      where: {
        salesChannelId: this.channelId,
        xmlExported: true,
        createdAt: {
          [Op.gt]: createdAt,
        },
      },
    });
  }

  async getOneShopifyOrder(args) {
    const orderNumber = args?.orderNumber;
    if (!orderNumber) throw `No order number given`;
    const params = {
      name: `#${orderNumber}`,
      status: "any",
    };
    try {
      const foundOrdersArray = await this.shopify.order.list(params);
      return foundOrdersArray;
    } catch (error) {
      Logger.info(`Problem with getting order: ${error}`);
    }
  }

  async getShopifyOrders(args) {
    const includeArchived = args?.includeArchivedOrders;
    const orderExportFilter = args?.orderExportFilter;
    const dateFilter = args?.dateFilter ?? true;
    const allOrders = [];
    let params = {
      financial_status: "paid",
      limit: process.env.SHOPIFY_ORDER_EXPORT_LIMIT ?? 250,
    };

    if (dateFilter) params["created_at_min"] = this.fromdate;
    if (includeArchived) params["status"] = "any";
    if (orderExportFilter) params["query"] = `tag:${orderExportFilter}`;

    do {
      const orders = await this.shopify.order.list(params);
      allOrders.push(...orders);
      params = orders.nextPageParameters;
    } while (params !== undefined);

    return allOrders;
  }

  confirmShippingAddress(order) {
    if (!valueExistsAndIsNotEmpty(order.shipping_address)) {
      order.shipping_address = order.billing_address;
      Logger.info(
        `Order ${order.name} was a gift card only -order. Copied billing address as shipping address.`
      );
    }
    return order;
  }

  orderIsValid(order) {
    // TODO: basic criteria for an order
    // TODO: what do we do if an order is invalid? Notification?
    if (!valueExistsAndIsNotEmpty(order.billing_address)) return false;
    if (!valueExistsAndIsNotEmpty(order.name)) return false;
    return true;
  }

  async filterBufferedOrders(ordersFromShopify) {
    const bufferOrders = await this.getBufferOrders(this.fromdate);
    const filteredOrders = _.differenceWith(
      ordersFromShopify,
      bufferOrders,
      ({ id }, { orderID }) => _.eq(id, orderID)
    );
    return filteredOrders;
  }

  async exportOrdersToXML(args) {
    const ordersFromShopify = args?.ordersFromShopify;
    if (!valueExistsAndIsNotEmpty(ordersFromShopify)) throw `No orders given.`;
    const bypassBufferCheck = args?.bypassBufferCheck ?? false;
    let newOrders = ordersFromShopify;
    if (!bypassBufferCheck) {
      newOrders = await this.filterBufferedOrders(ordersFromShopify);
    }

    try {
      const validOrders = newOrders.filter((order) => {
        if (!this.orderIsValid(order)) {
          Logger.error(`Order ID ${order.id} is not valid`);
          return false;
        }
        return true;
      });

      const shippingConfirmedOrders = validOrders.map((order) => {
        return this.confirmShippingAddress(order);
      });

      const orderSavePromises = shippingConfirmedOrders.map((order) => {
        return this.createOrderXML(order);
      });

      console.log("... saving XML files");
      const savedOrders = await Promise.all(orderSavePromises).catch((e) =>
        console.log(`Error syncing this batch of orders: ${e}`)
      );

      return savedOrders;
    } catch (error) {
      console.error(`Error syncing orders: ${error}`);
    }
  }

  async saveOrderBufferEntry(order) {
    console.info(`Save Buffer Orders for ${this.channelId}...`);
    order.xmlExported = true;
    order.fulfilled = false;
    const oldOnesFound = await ShopifyOrder.findAll({
      where: {
        salesChannelId: this.channelId,
        orderID: order.orderID,
      },
    });

    if (oldOnesFound.length === 0) {
      try {
        ShopifyOrder.create(order);
      } catch (error) {
        console.error(
          `Error creating new buffer entry for order ID ${order.orderID}`
        );
      }
    } else {
      try {
        ShopifyOrder.update(order, {
          where: {
            orderID: order.orderID,
          },
        });
        console.info(
          `Order ${order.orderID} already exists in the buffer, updated`
        );
      } catch (error) {
        console.error(
          `Error updating new buffer entry for order ID ${order.orderID}, error: ${error}`
        );
      }
    }
    return true;
  }

  async saveXMLFile(formattedXML, order) {
    let filePath = `${this.filePath}${this.channelName}-${order.orderNumber}.xml`;
    let remotePath = `${this.ftpPath}${this.channelName}-${order.orderNumber}.xml`;
    let archivePath = `${this.ftpArchivePath}${this.channelName}-${order.orderNumber}.xml`;
    const savedFile = await writeFile(filePath, formattedXML);
    if (savedFile) {
      let files = [];
      console.info(`Wrote XML file for order ID ${order.orderNumber}`);
      await this.saveOrderBufferEntry(order);
      files.push({ source: filePath, remotePath: remotePath });
      files.push({ source: filePath, remotePath: archivePath });
      return files;
    } else {
      return false;
    }
  }

  async sendXMLFiles(xmlFiles) {
    try {
      let batchFiles = xmlFiles.reduce(function (a, b) {
        return a.concat(b);
      }, []);
      return await this.ftpTools.batchUpload(batchFiles);
    } catch (error) {
      console.error(`Error with FTP upload: ${error}`);
    }
  }

  async createOrderXML(orderObject) {
    const customerGroups = orderObject.customer.tags
      .split(",")
      .map((item) => item.trim());
    const customerGroupSettings =
      this.findCustomerGroupSettings(customerGroups);
    const identifier = customerGroupSettings?.identifier
      ? customerGroupSettings.identifier
      : "B2C";
    const shippingCode = orderObject.shipping_lines[0]?.code ?? "B2Cstandard";
    const uniqueCardfile = customerGroupSettings?.uniqueCardfile
      ? orderObject.customer.note
      : false;

    const shippingMethod = customerGroupSettings?.shippingCode
      ? _.get(customerGroupSettings.shippingCode, shippingCode)
      : "B2Cstandard";

    const shippingPriority = customerGroupSettings?.shippingPriority
      ? _.get(customerGroupSettings.shippingPriority, shippingCode)
      : "1 - Today";

    const orderHasAGiftCardRedemption = orderObject.payment_gateway_names
      ? orderObject.payment_gateway_names.includes("gift_card")
      : false;

    const matchingGiftCardInfo = orderHasAGiftCardRedemption
      ? await this.shopifyGraphQL.getGiftCardInformationForOrder({
          GraphQLOrderID: orderObject.admin_graphql_api_id,
        })
      : false;

    const giftCardTransactions = matchingGiftCardInfo?.giftCardTransactions;

    const formatter = new Intl.NumberFormat("en-AU", {
      minimumFractionDigits: 4,
      maximumFractionDigits: 4,
    });

    const purchaseOrderID = orderObject.name.replace("#", "");

    let shippingTotal = formatter.format(
      orderObject.total_shipping_price_set.shop_money.amount
    );

    if (this.channelName === "B2BDIST" && identifier === "B2C") {
      shippingTotal = formatter.format(
        orderObject.total_shipping_price_set.shop_money.amount * 1.1
      );
    }

    let paymentNote = "";
    let paymentMethod = orderObject.gateway
      ? orderObject.gateway.toLowerCase()
      : "Unknown Payment Method";

    if (paymentMethod == "manual") {
      paymentMethod = "checkmo";
    } else {
      if (this.channelName === "B2BDIST") {
        paymentNote = "PREPAID";
      }
    }

    if (paymentMethod == "paypal") {
      paymentMethod = "paypal_express";
    }
    if (paymentMethod == "afterpay") {
      paymentMethod = "afterpay_payment";
    }

    let documentRoot = create({ version: "1.0" }).ele("NewDataSet");

    // Create order headers
    documentRoot
      .ele("tREMOTETransHeader")
      .ele("Company_ID")
      .txt("DATAPEL")
      .up()
      .ele("DTD")
      .txt(this.datapelDTD ?? this.channelName)
      .up()
      .ele("Identifier")
      .txt(identifier)
      .up()
      .ele("StoreCode")
      .txt("WAREHOUSE")
      .up()
      .ele("PostingDate")
      .txt(orderObject.created_at)
      .up()
      .ele("TransID")
      .txt("")
      .up()
      .ele("TransDate")
      .txt(orderObject.created_at)
      .up()
      .ele("ClosedYN")
      .txt("")
      .up()
      .ele("ClosedBy")
      .txt("")
      .up()
      .ele("SaleType")
      .txt("S")
      .up()
      .ele("Cashier")
      .txt("")
      .up()
      .ele("Salesperson")
      .txt("")
      .up()
      .ele("CustomerID")
      .txt("")
      .up()
      .ele("CardIdentification")
      .txt("")
      .up()
      .ele("MYOBCardName")
      .txt(uniqueCardfile ? uniqueCardfile : this.cardfile)
      .up()
      .ele("Till")
      .txt("")
      .up()
      .ele("OriginalSaleType")
      .txt("")
      .up()
      .ele("TransactionTime")
      .txt("")
      .up()
      .ele("VoidYN")
      .txt("")
      .up()
      .ele("VoidedTransID")
      .att("xml:space", "preserve")
      .txt("")
      .up()
      .ele("DeletedTransaction")
      .txt("")
      .up()
      .ele("Period")
      .txt("")
      .up()
      .ele("WeekNo")
      .txt("")
      .up()
      .ele("WeekStartDate")
      .txt("")
      .up()
      .ele("OriginalTransID")
      .txt(purchaseOrderID)
      .up()
      .ele("Notes")
      .att("xml:space", "preserve")
      .txt(orderObject.contact_email)
      .up()
      .ele("ShipNote")
      .txt(orderObject.note ? orderObject.note : "" + " " + paymentNote)
      .up()
      .ele("Change")
      .txt("")
      .up()
      .ele("Sent2Host")
      .txt("")
      .up()
      .ele("Sent2HostDateTime")
      .txt("")
      .up()
      .ele("DueDate")
      .txt("")
      .up()
      .ele("Special1")
      .txt("5")
      .up()
      .ele("Special2")
      .att("xml:space", "preserve")
      .txt(purchaseOrderID)
      .up()
      .ele("Special3")
      .txt("")
      .up()
      .ele("UDF1")
      .txt("")
      .up()
      .ele("UDF2")
      .txt("")
      .up()
      .ele("UDF3")
      .txt("")
      .up()
      .ele("ShippingSKU")
      .txt("FR")
      .up()
      .ele("ShippingMethod")
      .txt(shippingMethod)
      .up()
      .ele("ShippingNote")
      .txt(orderObject.shipping_lines[0]?.code ?? "")
      .up()
      .ele("ShippingCost")
      .txt(shippingTotal)
      .up()
      .ele("Priority")
      .txt("NORMAL")
      .up()
      .ele("ReferralSource")
      .txt(shippingPriority)
      .up()
      .ele("DiscountSKU")
      .txt("DISCOUNT")
      .up()
      .ele("DiscountAmount")
      .txt("0.0000")
      .up()
      .ele("DiscountNote")
      .txt("")
      .up()
      .ele("SurchargeAmount")
      .txt("")
      .up()
      .ele("SurchargeNote")
      .txt("")
      .up()
      .up()
      .ele("tREMOTETransSaleTenders")
      .ele("Company_ID")
      .txt("")
      .up()
      .ele("StoreCode")
      .txt("WAREHOUSE")
      .up()
      .ele("PostingDate")
      .txt(orderObject.created_at)
      .up()
      .ele("TransID")
      .txt("")
      .up()
      .ele("TenderLine")
      .txt("")
      .up()
      .ele("TenderType")
      .txt(paymentMethod)
      .up()
      .ele("TenderAmount")
      .txt(formatter.format(orderObject.total_price))
      .up()
      .ele("TDets1")
      .att("xml:space", "preserve")
      .txt("")
      .up()
      .ele("TDets2")
      .txt("")
      .up()
      .ele("CurrencyCode")
      .txt("AUD")
      .up()
      .ele("ForeignTenderAmount")
      .txt("")
      .up()
      .ele("Search")
      .att("xml:space", "preserve")
      .txt("")
      .up()
      .ele("Sent2Host")
      .txt("")
      .up()
      .ele("Sent2HostDateTime")
      .txt("")
      .up()
      .up()
      .ele("tREMOTETransCustomer")
      .ele("Company_ID")
      .txt("")
      .up()
      .ele("StoreCode")
      .txt("WAREHOUSE")
      .up()
      .ele("PostingDate")
      .txt(orderObject.created_at)
      .up()
      .ele("TransID")
      .txt("")
      .up()
      .ele("Name")
      .txt("")
      .up()
      .ele("Address1")
      .txt("")
      .up()
      .ele("Address2")
      .att("xml:space", "preserve")
      .txt("")
      .up()
      .ele("Suburb")
      .txt("")
      .up()
      .ele("PostCode")
      .txt("")
      .up()
      .ele("FaxNo")
      .att("xml:space", "preserve")
      .txt("")
      .up()
      .ele("PhoneNo1")
      .att("xml:space", "preserve")
      .att("xml:space", "preserve")
      .txt(orderObject.billing_address.phone)
      .up()
      .ele("PhoneNo2")
      .att("xml:space", "preserve")
      .txt("")
      .up()
      .ele("EmailAddress")
      .att("xml:space", "preserve")
      .txt(orderObject.contact_email)
      .up()
      .ele("SalesPerson")
      .txt("")
      .up()
      .ele("ContactName")
      .att("xml:space", "preserve")
      .txt(orderObject.shipping_address.name)
      .up()
      .ele("DateTimeLastChanged")
      .txt("")
      .up()
      .ele("AccountCode")
      .txt("")
      .up()
      .ele("ShipToName")
      .txt(
        orderObject.shipping_address.company
          ? orderObject.shipping_address.company
          : "" +
              " " +
              orderObject.shipping_address.first_name +
              " " +
              orderObject.shipping_address.last_name
      )
      .up()
      .ele("ShipToAddress1")
      .att("xml:space", "preserve")
      .txt(
        customerGroupSettings?.noShipping
          ? ""
          : orderObject.shipping_address.address1
      )
      .up()
      .ele("ShipToAddress2")
      .att("xml:space", "preserve")
      .txt(
        customerGroupSettings?.noShipping
          ? ""
          : orderObject.shipping_address.address2
      )
      .up()
      .ele("ShipToSuburb")
      .att("xml:space", "preserve")
      .txt(
        customerGroupSettings?.noShipping
          ? ""
          : orderObject.shipping_address.city
      )
      .up()
      .ele("ShipToState")
      .att("xml:space", "preserve")
      .txt(
        customerGroupSettings?.noShipping
          ? ""
          : orderObject.shipping_address.province_code
      )
      .up()
      .ele("ShipToPostCode")
      .txt(
        customerGroupSettings?.noShipping
          ? ""
          : orderObject.shipping_address.zip
      )
      .up()
      .ele("Sent2Host")
      .txt("")
      .up()
      .ele("Sent2HostDateTime")
      .txt("")
      .up();

    // arrange order lines in SKU alphabetical ascending order
    const sortedOrderLines = _.orderBy(
      orderObject.line_items,
      ["sku"],
      ["asc"]
    );

    // Loop through order lines
    for (const [index, orderline] of orderObject.line_items.entries()) {
      let SaleLineNo = index + 1;
      const SaleUnitTaxType =
        customerGroupSettings?.saleUnitAmountTax ?? "SaleUnitAmountExcTax";
      const SaleTaxByHost = customerGroupSettings?.saleTaxByHost ?? "Y";
      const SalePriceByHost = customerGroupSettings?.salePriceByHost ?? "N";

      const roundedQuantity = formatter.format(orderline.quantity);
      const thisOrderLineIsAGiftCard =
        orderline.fulfillment_service === "gift_card";
      const verifiedSKU = thisOrderLineIsAGiftCard ? "GIFTCARD" : orderline.sku;
      let SKUDescription = thisOrderLineIsAGiftCard
        ? `NEW ${orderline.name}`
        : orderline.name;

      let roundedSaleUnitAmountEx = formatter.format(
        orderline.pre_tax_price / orderline.quantity
      );

      if (this.channelName !== "B2BDIST") {
        roundedSaleUnitAmountEx = formatter.format(orderline.price / 1.1);
      }

      if (this.channelName === "B2BDIST") {
        SKUDescription = "";
      }

      documentRoot
        .ele("tREMOTETransSaleLines")
        .ele("SaleLineNo")
        .txt(SaleLineNo)
        .up()
        .ele("SKU")
        .txt(this.suffix + verifiedSKU)
        .up()
        .ele("SaleQty")
        .txt(roundedQuantity)
        .up()
        .ele(SaleUnitTaxType)
        .txt(roundedSaleUnitAmountEx)
        .up()
        .ele("SKUDescription")
        .txt(SKUDescription)
        .up()
        .ele("SaleTaxByHost")
        .txt(SaleTaxByHost)
        .up()
        .ele("SalePriceByHost")
        .txt(SalePriceByHost)
        .up();
    }

    if (orderObject.total_discounts > 0 && this.channelName !== "B2BDIST") {
      const roundedDiscountAmountEx = formatter.format(
        orderObject.total_discounts / 1.1
      );
      documentRoot
        .ele("tREMOTETransSaleLines")
        .ele("SKU")
        .txt("DISCOUNT")
        .up()
        .ele("SaleQty")
        .txt("-1.0000")
        .up()
        .ele("SaleUnitAmountExcTax")
        .txt(roundedDiscountAmountEx)
        .up()
        .ele("SKUDescription")
        .txt("DISCOUNT")
        .up()
        .ele("SaleTaxByHost")
        .txt("Y")
        .up()
        .ele("SalePriceByHost")
        .txt("N")
        .up();
    }

    if (giftCardTransactions) {
      for (const giftCardTransaction of giftCardTransactions) {
        const giftCardID = giftCardTransaction.receiptJson?.gift_card_id;
        const giftCardAccountNumber = giftCardTransaction.accountNumber;
        const amount = giftCardTransaction.amountSet?.shopMoney?.amount;
        const lineItemDetails = {
          SaleQty: -1,
          SKU: "GIFTCARD",
          SaleUnitAmountExcTax: 0,
          SKUDescription: `REDEEM $${amount}, ID ${giftCardID}, Card ${giftCardAccountNumber}`,
          SaleTaxByHost: "Y",
          SalePriceByHost: "N",
        };
        documentRoot = this.addOrderExportLineItem(
          documentRoot,
          lineItemDetails
        );
      }
    }

    const xmlToBeSaved = documentRoot.end({
      prettyPrint: true,
      allowEmptyTags: true,
    });

    return await this.saveXMLFile(xmlToBeSaved, this.decorate(orderObject));
  }

  findCustomerGroupSettings(groups) {
    let group;
    for (const findGroup of groups) {
      group = _.find(this.xmlSettings, { tag: findGroup });
    }

    return group;
  }

  addOrderExportLineItem(documentRoot, args) {
    const SaleQty = args?.SaleQty ?? 0;
    const SKU = args?.SKU ?? null;
    if (!SKU) {
      Logger.error(`Line item has no SKU`);
      return documentRoot;
    }
    const SaleUnitAmountExcTax = args?.SaleUnitAmountExcTax ?? 0;
    const SKUDescription = args?.SKUDescription ?? 0;
    const SaleTaxByHost = args?.SaleTaxByHost ?? "Y";
    const SalePriceByHost = args?.SalePriceByHost ?? "Y";

    documentRoot
      .ele("tREMOTETransSaleLines")
      .ele("SKU")
      .txt(SKU)
      .up()
      .ele("SaleQty")
      .txt(SaleQty)
      .up()
      .ele("SaleUnitAmountExcTax")
      .txt(SaleUnitAmountExcTax)
      .up()
      .ele("SKUDescription")
      .txt(SKUDescription)
      .up()
      .ele("SaleTaxByHost")
      .txt(SaleTaxByHost)
      .up()
      .ele("SalePriceByHost")
      .txt(SalePriceByHost)
      .up();
    return documentRoot;
  }
}

module.exports = ShopifyOrderExport;
