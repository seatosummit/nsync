const fetch = require("node-fetch");
const { promises: fsp } = require("fs");
const fs = require("fs");
const sharp = require("sharp");
const pathTools = require("path");
const _ = require("lodash");
require("dotenv").config();
const { GoogleDriveAuth } = require("../../helpers/apitools");
const { GoogleDriveFile } = require("../../../models");
const RateLimiter = require("limiter").RateLimiter;
const limiter = new RateLimiter({ tokensPerInterval: 10, interval: 10000 });
const { prepareHeaders } = require("../../helpers/apitools");

// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

module.exports = {
  getProductImagesFromSalsifyWithPage: async (page = 0) => {
    const headers = await prepareHeaders();
    // console.log(`current page: ${page}`);
    let url;
    try {
      url = `https://app.salsify.com/api/v1/orgs/${process.env.SALSIFY_ORG_ID}/products?access_token=${process.env.SALSIFY_TOKEN}&page=${page}`;

      const response = await fetch(url, {
        headers: headers.SalsifyHeaders,
        timeout: 0,
      });
      if (response.status !== 200) {
        throw `response other than 200 - ${response.status}: ${response.statusText}`;
      }
      let responseJSON = await response.json();
      // console.log(responseJSON.meta);
      let imageURLArray = module.exports.parseArrayOfProductURLs(
        responseJSON.data
      );
      for (let index = 0; index < imageURLArray.length; index++) {
        const currentImage = imageURLArray[index];
        await module.exports.saveImageWithSKUFilename(currentImage);
      }
      const totalPages = responseJSON.meta.total_entries / 100;
      const currentPage = Number.parseInt(responseJSON.meta.current_page);
      const nextPage = currentPage + 1;
      // console.log(`next page: ${nextPage}`);
      if (nextPage < totalPages) {
        //await sleep(10000);
        module.exports.getProductImagesFromSalsifyWithPage(nextPage);
      } else {
        // console.log("last page");
      }
    } catch (error) {
      console.error(`Error fetching products from Salsify: ${error}`);
    }
  },
  parseArrayOfProductURLs: (productArray) => {
    let imageURLArray = [];
    productArray.forEach((product) => {
      if (product["salsify:digital_assets"]) {
        product["salsify:digital_assets"].forEach((productImage) => {
          const imageObject = {
            salsifyId: product["salsify:id"],
            sku: product["PRODUCT CODE"].replace(/\//g, "-"),
            URL: productImage["salsify:url"],
            filename: productImage["salsify:filename"],
            fileExtension: productImage["salsify:format"],
            updatedAt: productImage["salsify:updated_at"],
          };
          imageURLArray.push(imageObject);
        });
      } else {
        // console.log(`No images for sku ${product["PRODUCT CODE"]}`);
      }
    });
    return imageURLArray;
  },
  saveImageWithSKUFilename: async (image) => {
    if (["jpg", "png", "bmp", "tiff"].includes(image.fileExtension)) {
      const url = image.URL;
      let dir;
      let file;
      let path;
      dir = `${process.env.GOOGLE_DRIVE_IMAGES_LOCATION}/${image.sku}`;
      file = `/${image.filename}`;
      path = dir + file;

      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
      }
      if (fs.existsSync(path)) {
        //console.log(
        //   `Image ${pathTools.basename(path)} already exists, will update`
        // );
        return;
      }
      try {
        const response = await fetch(url);
        const buffer = await response.buffer();
        await sharp(buffer)
          .resize({ width: 1600, withoutEnlargement: true })
          .toFormat("jpg")
          .jpeg({ mozjpeg: true })
          .toFile(path);
        //console.log(
        //   `Saved image ${pathTools.basename(path)}, salsify id ${
        //     image.salsifyId
        //   }`
        // );
        return path;
      } catch (error) {
        console.error(
          `Error saving image for sku ${image.sku}, salsify id ${image.salsifyId}, error ${error}`
        );
      }
    } else {
      console.error(
        `Error: image ${image.sku}.${image.fileExtension} is in wrong format (${image.fileExtension}), salsify id ${image.salsifyId}`
      );
    }
  },
  saveImageToGoogleDrive: async (fileMetadata) => {
    const drive = await GoogleDriveAuth();
    await limiter.removeTokens(1);
    return new Promise((resolve, reject) => {
      if (
        !fs.existsSync(
          `${process.env.GOOGLE_DRIVE_IMAGES_LOCATION}/${fileMetadata.sku}/${fileMetadata.name}`
        )
      ) {
        // console.log("No file: " + fileMetadata.sku + " - " + fileMetadata.name);
        return resolve(fileMetadata);
      }
      let media = {
        mimeType: "image/jpeg",
        body: fs.createReadStream(
          `${process.env.GOOGLE_DRIVE_IMAGES_LOCATION}/${fileMetadata.sku}/${fileMetadata.name}`
        ),
      };
      drive.files
        .create({
          resource: fileMetadata,
          media: media,
          fields: "id",
        })
        .then(
          function (response) {
            // console.log("Upload " + fileMetadata.name);
            return resolve(response.data);
          },
          function (err) {
            console.error("File create error", err);
          }
        );
    });
  },
  uploadProductImages: async (productImages) => {
    productImages.map((currentImage) => {
      return module.exports.saveImageToGoogleDrive({
        sku: currentImage["sku"],
        name: currentImage["name"],
        parents: [currentImage["folder"]],
      });
    });
  },
  groupProductImages: async (
    products,
    parentProducts,
    folders,
    googleDriveBuffer
  ) => {
    let groupImages = [];
    let bufferList = [];
    let bufferFolder;
    let productFolder;
    for (const product of products) {
      let folderName = product.name.replace('"', "");
      if (product.parent_sku) {
        let currentParent = _.find(parentProducts, { sku: product.parent_sku });
        if (!currentParent) {
          return false;
        }
        folderName = currentParent.name.replace('"', "");
      }
      productFolder = _.find(folders, { name: folderName });
      bufferFolder = _.find(googleDriveBuffer, { name: folderName });

      if (bufferFolder) {
        if (!_.isNull(bufferFolder.list)) {
          bufferList = JSON.parse(bufferFolder.list);
        }
      }

      digital_assets = JSON.parse(product.digital_assets);
      digital_assets.map((currentImage) => {
        let existingFile = _.find(bufferList, {
          name: currentImage["salsify:name"],
        });
        if (
          !existingFile ||
          new Date(currentImage["salsify:created_at"]).getTime() >
            new Date().getTime() - process.env.GOOGLE_DRIVE_UPDATE_TIME
        ) {
          groupImages.push({
            sku: product.sku,
            name: currentImage["salsify:name"],
            folder: productFolder.id,
          });
          bufferList.push({ name: currentImage["salsify:name"] });
        }
      });
    }

    if (!_.isEmpty(bufferList)) {
      GoogleDriveFile.update(
        { list: bufferList },
        { where: { id: bufferFolder.id } }
      );
    }
    return module.exports.uploadProductImages(
      _.uniqBy(groupImages, "name"),
      bufferList
    );
  },
  getGoogleDriveBuffer: async () => {
    return await GoogleDriveFile.findAll({
      raw: true,
    });
  },
};
