require("dotenv").config();
const _ = require("lodash");
const { prepareHeaders } = require("../../helpers/apitools");
const { Sequelize } = require("sequelize");
const Op = Sequelize.Op;
const {
  getProductImagesFromSalsifyWithPage,
  groupProductImages,
  getGoogleDriveBuffer,
} = require("./productimages");
const {
  syncBrandFolders,
  syncProductFolders,
} = require("../folders/productfolders");
const {
  getParentProductPromises,
  getParentProductFromSalsify,
} = require("../../salsify/products/product");
const { SalsifyProduct } = require("../../../models");
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

module.exports = {
  syncUpdatedSalsifyProducts: async () => {
    const headers = await prepareHeaders();

    const allSalsifyBufferProducts = await SalsifyProduct.findAll({
      raw: true,
      where: {
        updatedAt: {
          [Op.gt]: new Date().getTime() - process.env.GOOGLE_DRIVE_UPDATE_TIME,
        },
        digital_assets: {
          [Op.ne]: "[]",
        },
        name: {
          [Op.ne]: null,
        },
      },
    });
    if (allSalsifyBufferProducts.length === 0) {
      process.exit();
    }

    const simpleProducts = _.filter(allSalsifyBufferProducts, [
      "parent_sku",
      null,
    ]);
    const configurableProducts = _.reject(allSalsifyBufferProducts, [
      "parent_sku",
      null,
    ]);
    const salsifyParents = _.reject(
      _.uniqBy(allSalsifyBufferProducts, "parent_sku"),
      ["parent_sku", null]
    );

    const parentProductsPromises = await salsifyParents.map((parentProduct) => {
      return getParentProductFromSalsify(parentProduct, headers);
    });

    const parentProducts = await getParentProductPromises(
      parentProductsPromises
    );
    const productBrands = await module.exports.getProductBrands(
      parentProducts,
      simpleProducts
    );

    const brandFolderPromises = productBrands.map((brand) => {
      return syncBrandFolders(brand, headers);
    });
    const setupFolders = await Promise.all(brandFolderPromises)
      .then(function (brandFolders) {
        const productFoldersPromises = simpleProducts
          .concat(parentProducts)
          .map((product) => {
            if (product.sku) {
              return syncProductFolders(product, brandFolders);
            }
          });

        return Promise.all(productFoldersPromises).then(function (
          productFolders
        ) {
          return productFolders;
        });
      })
      .catch((e) => console.error(`Error syncing brand folders: ${e}`));

    const groupSimpleProducts = _.values(_.groupBy(simpleProducts, "sku"));
    const groupConfigurableProducts = _.values(
      _.groupBy(configurableProducts, "parent_sku")
    );
    const allGroupProducts = groupSimpleProducts.concat(
      groupConfigurableProducts
    );

    const productFolders = await setupFolders;
    const googleDriveBuffer = await getGoogleDriveBuffer();

    const productImagePromises = allGroupProducts.map((product) => {
      return groupProductImages(
        product,
        parentProducts,
        productFolders,
        googleDriveBuffer
      );
    });

    await Promise.all(productImagePromises).catch((e) =>
      console.error(`Error syncing product images: ${e}`)
    );
    // console.log("complete");
  },
  getProductImagesFromSalsify: async () => {
    await getProductImagesFromSalsifyWithPage();
  },
  getProductBrands: async (parentProducts, simpleProducts) => {
    let simpleBrands = [];
    for (const product of simpleProducts) {
      let currentSnapshot = product["snapshot"];
      if (typeof currentSnapshot !== "object") {
        currentSnapshot = JSON.parse(product["snapshot"]);
      }
      simpleBrands.push({ brand: currentSnapshot["BRAND"] });
    }
    return _.filter(
      _.uniqBy(simpleBrands.concat(parentProducts), "brand"),
      _.size
    );
  },
};
