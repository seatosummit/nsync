require("dotenv").config();
const _ = require("lodash");
const fs = require("fs");
const { GoogleDriveAuth } = require("../../helpers/apitools");
const { GoogleDriveFile } = require("../../../models");
const RateLimiter = require("limiter").RateLimiter;
const limiter = new RateLimiter({ tokensPerInterval: 10, interval: 10000 });

// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

module.exports = {
  syncBrandFolders: async (productBrand) => {
    const parentFolderId = process.env.GOOGLE_DRIVE_ROOT;

    let currentBrand = await module.exports.searchGoogleDriveFolder(
      productBrand["brand"],
      parentFolderId,
      true
    );
    let imagesFolder = await module.exports.searchGoogleDriveFolder(
      "Images",
      currentBrand.files[0].id,
      true
    );
    let productImagesFolder = await module.exports.searchGoogleDriveFolder(
      "Product Images",
      imagesFolder.files[0].id,
      true
    );

    currentBrand.files[0]["imagesFolder"] = imagesFolder.files[0];
    currentBrand.files[0]["productImagesFolder"] = productImagesFolder.files[0];

    return currentBrand.files[0];
  },
  syncProductFolders: async (product, brandFolders) => {
    try {
      let currentSnapshot = product["snapshot"];
      if (typeof currentSnapshot !== "object") {
        currentSnapshot = JSON.parse(product["snapshot"]);
      }

      let currentBrand = await _.find(brandFolders, {
        name: currentSnapshot["BRAND"],
      });
      let folderName = product["name"].replace('"', "");
      let productFolder = await module.exports.searchGoogleDriveFolder(
        folderName,
        currentBrand["productImagesFolder"]["id"],
        true
      );

      return productFolder.files[0];
    } catch (error) {
      console.error(`Error Google Drive buffer: ${error}`);
    }
  },
  searchGoogleDriveFolder: async (name, parentId, create = true) => {
    const drive = await GoogleDriveAuth();
    await limiter.removeTokens(1);
    return new Promise((resolve, reject) => {
      drive.files.list(
        {
          q: `name = "${name}" and "${parentId}" in parents and trashed=false and mimeType = "application/vnd.google-apps.folder"`,
          fields: "files(id, name)",
        },
        function (err, { data }) {
          if (err) {
            // console.log(err);
            return reject(err);
          }
          if (!data.files.length && create) {
            return resolve(
              module.exports.createGoogleDriveFolder(name, parentId)
            );
          }

          GoogleDriveFile.findOrCreate({
            where: {
              name: name,
            },
            defaults: {
              drive_id: data.files[0].id,
              name: name,
              type: "application/vnd.google-apps.folder",
              parent: parentId,
            },
          });
          return resolve(data);
        }
      );
    });
  },
  getGoogleDriveFolderList: async (parentId) => {
    const drive = await GoogleDriveAuth();
    await limiter.removeTokens(1);
    return new Promise((resolve, reject) => {
      drive.files.list(
        {
          q: `"${parentId}" in parents and trashed=false and mimeType = "application/vnd.google-apps.folder"`,
          fields: "files(id, name)",
        },
        function (err, { data }) {
          if (err) {
            // console.log(err);
            return reject(err);
          }
          return resolve(data);
        }
      );
    });
  },
  createGoogleDriveFolder: async (
    name,
    parentFolderId = process.env.GOOGLE_DRIVE_ROOT
  ) => {
    const drive = await GoogleDriveAuth();
    await limiter.removeTokens(1);
    return new Promise((resolve, reject) => {
      let fileMetadata = {
        name: name,
        mimeType: "application/vnd.google-apps.folder",
        parents: [parentFolderId],
      };
      drive.files.create(
        {
          resource: fileMetadata,
          fields: "id, name",
        },
        function (err, file) {
          if (err) {
            // console.log(err);
            reject(err);
          } else {
            GoogleDriveFile.create({
              drive_id: file.data.id,
              name: name,
              type: "application/vnd.google-apps.folder",
              parent: parentFolderId,
            });
            return resolve({ files: [file.data] });
          }
        }
      );
    });
  },
  updateGoogleDriveBuffer: async (
    folder,
    parentId = process.env.GOOGLE_DRIVE_ROOT
  ) => {
    try {
      GoogleDriveFile.create({
        drive_id: folder.id,
        name: folder.name,
        type: "application/vnd.google-apps.folder",
        parent: folder.parent,
      });
    } catch (error) {
      console.error(`Error Google Drive buffer: ${error}`);
    }
  },
};
