class EmailConnectionError extends Error {
  constructor(payload) {
    const amendedMessage = `Problems connecting to STMP server: ${payload}`;
    super(amendedMessage);
    this.name = "SMTPConnectionError";
  }
}
class EmailSendError extends Error {
  constructor(payload) {
    const amendedMessage = `Can't send an email using SMTP server: ${payload}`;
    super(amendedMessage);
    this.name = "STMPEmailSendError";
  }
}

module.exports.EmailConnectionError = EmailConnectionError;
module.exports.EmailSendError = EmailSendError;
