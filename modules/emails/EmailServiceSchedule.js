const { getOrdersFilter } = require("../helpers/IntegrationRuleTools");
const { Logger } = require("../helpers/Logger");
const { ShopifyConfig } = require("../helpers/ShopifyConfig");
const ShopifyOrderExport = require("../shopify/orders/ShopifyOrderExport");
const EmailService = require("./EmailService");
require("dotenv").config();

const testEmails = async () => {
  const ShopifyStore = (
    await ShopifyConfig.getConfigsFromDatabase({
      machineNameOfSalesChannel: "BLACKDIAMOND",
    })
  )[0];

  let orderExporter = new ShopifyOrderExport(ShopifyStore.shopify_config);
  console.info(`Get ${ShopifyStore.shopify_config.name} Shopify Orders...`);
  const shopifyOrders = await orderExporter.getShopifyOrders({
    //orderExportFilter: getOrdersFilter({ ShopifyStore }),
    includeArchivedOrders: true,
    dateFilter: false,
  });

  const order = shopifyOrders.filter((order) => {
    return order.id == 4429432127651;
  })[0];

  try {
    const emailer = new EmailService();
    const emailResult = await emailer.sendOrderNotification({
      salesChannel: "Black Diamond",
      order,
    });
  } catch (error) {
    Logger.error(`Error sending email: ${error}`);
  }

  process.exit();
};

module.exports.testEmails = testEmails;
