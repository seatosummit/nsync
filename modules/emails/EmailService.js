const { Sequelize } = require("sequelize");
const Op = Sequelize.Op;
const _ = require("lodash");
const { Logger } = require("../helpers/Logger");
const { valueExistsAndIsNotEmpty } = require("../helpers/stringTools");
const { promises: fsp } = require("fs");
const fs = require("fs");
const sharp = require("sharp");
const pathTools = require("path");
const formData = require("form-data");
const Mailgun = require("mailgun.js");
const { DateTime } = require("luxon");
const { promisify } = require("util");
const sleep = promisify(setTimeout);

const {
  EmailConnectionError,
  EmailSendError,
} = require("./errors/EmailServiceErrors");
const { dateForOrderNotifications } = require("../helpers/dateTools");
const order = require("../pepperi/orders/order");
const { SequelizeOperations } = require("../sequelize/SequelizeOperations");
require("dotenv").config();

class EmailService {
  constructor() {
    this.apiKey = process.env.MAILGUN_API_KEY;
    this.domain = process.env.MAILGUN_DOMAIN;
    if (!this.apiKey || !this.domain)
      throw new EmailConnectionError("Need api key and domain");
    const mailgun = new Mailgun(formData);
    this.client = mailgun.client({
      username: "api",
      key: this.apiKey,
    });
  }

  async sendOrderNotification(args) {
    const order = args?.order;
    const ShopifyStore = args?.ShopifyStore;
    const sender = args?.emailRecipient;
    const emailRecipient = args?.emailRecipient;
    if (!ShopifyStore)
      throw new EmailSendError(`No Shopify store config given.`);
    if (!order) throw new EmailSendError(`No order information.`);
    if (!sender) throw new EmailSendError(`No recipient/sender information.`);
    const notificationData = this.decorateOrderNotification({
      order: order,
      ShopifyStore: ShopifyStore,
    });

    const email_title = `Order ${order.name} from ${ShopifyStore.name}`;
    const mailgunData = {
      "from": sender,
      "to": emailRecipient,
      "subject": email_title,
      "template": "shopify-order-notification",
      "h:X-Mailgun-Variables": JSON.stringify({
        ...notificationData,
      }),
      "h:Reply-To": sender,
    };
    try {
      await this.sendEmail({ mailgunData });
    } catch (error) {
      throw error;
    }
  }

  decorateOrderNotification(args) {
    const order = args?.order;
    const ShopifyStore = args?.ShopifyStore;
    const orderNotificationPayload = {
      order_status_url: `${ShopifyStore.shopify_config.shopUrl}admin/orders/${order.id}`,
      order_name: order.name,
      order_date: dateForOrderNotifications({
        shopifyDate: order.processed_at,
      }),
      payment_status: order.financial_status,
      line_items: order.line_items.map((lineItem) => {
        const price = lineItem.price;
        return {
          main_title: lineItem.title,
          variant_title: lineItem.variant_title,
          sku: lineItem.sku,
          price,
          quantity: lineItem.quantity,
          variant_total: lineItem.quantity * lineItem.price,
        };
      }),
      totals: {
        subtotal: order.total_line_items_price,
        discounts: order.discount_codes ?? [],
        shipping_name: order.shipping_lines[0].title,
        shipping_price: order.shipping_lines[0].price,
        tax: order.current_total_tax,
        total: order.total_price,
      },
      payment: {
        has_card_details: order.payment_details ? true : false,
        credit_card_company: order.payment_details?.credit_card_company,
        credit_card_number: order.payment_details?.credit_card_number,
        credit_card_bin: order.payment_details?.credit_card_bin,
        gateway: order.gateway,
        status: order.financial_status,
        reference: order.reference ?? `${order.name}`,
      },
      delivery: {
        method: order.shipping_lines[0].title,
        name: order.shipping_address.name,
        street1: order.shipping_address.address1,
        street2: order.shipping_address.address2,
        city: order.shipping_address.city,
        country: order.shipping_address.country,
        postcode: order.shipping_address.zip,
        state: order.shipping_address.province,
        phone: order.shipping_address.phone,
        email: order.email,
      },
    };
    return orderNotificationPayload;
  }

  async sendEmail(args) {
    const mailgunData = args?.mailgunData;
    if (!mailgunData)
      throw EmailSendError(`Don't have the data I need to send an email`);
    try {
      const emailSendResult = await this.client.messages.create(
        this.domain,
        mailgunData
      );
      Logger.info(
        `Sent email, with status ${emailSendResult.status}, id ${emailSendResult.id}`
      );
    } catch (error) {
      throw new EmailSendError(`Unable to send email: ${error}`);
    }
  }

  async sendNotificationForOrders(args) {
    const orders = args?.orders ?? [];
    const ShopifyStore = args?.ShopifyStore;
    const emailRecipient = args?.emailRecipient;
    if (!ShopifyStore)
      throw new EmailSendError(`No ShopifyStore provided for config.`);
    if (!emailRecipient)
      throw new EmailSendError(`No email recipient provided for config.`);
    for (const order of orders) {
      try {
        await sleep(2000);
        await this.sendOrderNotification({
          order,
          ShopifyStore,
          emailRecipient,
        });
      } catch (error) {
        Logger.error(error);
      }
    }
  }
}

module.exports = EmailService;
