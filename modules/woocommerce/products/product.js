const fetch = require("node-fetch");
const _ = require("lodash");
const WooCommerceRestApi = require("@woocommerce/woocommerce-rest-api").default;
require("dotenv").config();
const { promisify } = require("util");
const sleep = promisify(setTimeout);
const { prepareHeaders } = require("../../helpers/apitools");
const { syncProductsFromWooCommerceToBuffer } = require("./productbuffer");
const {
  SalsifyProduct,
  WMSProduct,
  SalsifyProductParent,
  HelinoxBufferProduct,
  STSBufferProduct,
} = require("../../../models");
const {
  woocommerceAndSalsifyProduct,
  decorateSTSBufferProductFromSaveResult,
} = require("./productdecorator");

const WooCommerce = new WooCommerceRestApi({
  url: process.env.STS_API_URL,
  consumerKey: process.env.STS_API_CONSUMERKEY,
  consumerSecret: process.env.STS_API_CONSUMERSECRET,
  version: "wc/v3",
});

// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

module.exports = {
  syncProductsToWoocommerceFromWMSUsingBuffer: async (filter = true) => {
    try {
      // refresh the ID buffer
      await syncProductsFromWooCommerceToBuffer();

      // get all Woo attributes

      const allWooAttributesResult = await WooCommerce.get(
        "products/attributes"
      );
      const allWooAttributes = allWooAttributesResult.data.map((attribute) => {
        const updatedAttribute = {
          id: attribute.id,
          name: attribute.name.toLowerCase(),
        };
        return updatedAttribute;
      });

      // get all Woo buffer products
      const allWooProductsFromBuffer = await STSBufferProduct.findAll();

      // Get all WMS Sea To Summit products
      const allWMSBufferProducts = await WMSProduct.findAll({
        where: {
          brand: "SEA TO SUMMIT",
        },
      });

      //get all salsify parents for Sea To Summit
      const allSTSSalsifyParentProducts = await SalsifyProductParent.findAll({
        where: {
          brand: "Sea To Summit",
        },
      });

      const allSTSSalsifyProducts = await SalsifyProduct.findAll({
        where: {
          brand: "Sea to Summit",
        },
      });

      const allSTSSalsifySimpleProducts = allSTSSalsifyProducts.filter(
        (product) => !product.parent_sku
      );
      const allSTSSalsifyVariantProducts = allSTSSalsifyProducts.filter(
        (product) => product.parent_sku
      );

      // Woocommerce has 3 different products:
      // 1. Variable products (Salsify product parents)
      // 2. Variations of a variable product (Salsify products that do have a parent_sku)
      // 3. Simple products (Salsify products that have no parent_sku)

      const decoratedVariableProducts = allSTSSalsifyParentProducts.map(
        function (productParent) {
          return woocommerceAndSalsifyProduct(this, productParent, "variable");
        },
        {
          allWooProductsFromBuffer,
          allSTSSalsifyVariantProducts,
          allWMSBufferProducts,
          allWooAttributes,
        }
      );

      const decoratedVariations = allSTSSalsifyVariantProducts.map(
        function (productVariant) {
          return woocommerceAndSalsifyProduct(this, productVariant);
        },
        {
          allWooProductsFromBuffer,
          allSTSSalsifyVariantProducts,
          allWMSBufferProducts,
          allWooAttributes,
        }
      );

      const decoratedSimpleProducts = allSTSSalsifySimpleProducts.map(
        function (simpleProduct) {
          return woocommerceAndSalsifyProduct(this, simpleProduct, "simple");
        },
        {
          allWooProductsFromBuffer,
          allSTSSalsifyVariantProducts,
          allWMSBufferProducts,
          allWooAttributes,
        }
      );

      // Woocommerce API accepts only chunks of 50-100, beyond that it times out
      const chunkedProductParents = _.chunk(decoratedVariableProducts, 10);

      for (const [index, batch] of chunkedProductParents.entries()) {
        try {
          const variableProductsUpdateResult =
            await module.exports.bulkSaveProducts(batch);
          if (!variableProductsUpdateResult) {
            console.log("No products to add in this batch");
          } else {
            console.log(
              `Sent ${variableProductsUpdateResult.data.create.length} product add requests in this batch`
            );
          }
        } catch (error) {
          console.log(
            `Batch ${index + 1} of ${
              chunkedProductParents.length
            } failed: ${error}`
          );
        }
      }

      // for (const createdProduct of variableProductsCreateDataSet) {
      //   if (createdProduct.error) {
      //     console.log("error creating product");
      //     console.log(createdProduct.error);
      //     continue;
      //   }
      //   const STSBufferProductPayload =
      //     decorateSTSBufferProductFromSaveResult(createdProduct);
      //   await STSBufferProduct.create(STSBufferProductPayload);
      // }
    } catch (error) {
      console.log(
        `Error syncing products from Salsify to Woocommerce, ${error}`
      );
      return;
    }
  },

  bulkSaveProducts: async (decoratedProductsArray) => {
    const productsToUpdate = decoratedProductsArray
      .map((product) => {
        if (product.id !== null) {
          delete product.parent_sku;
          return product;
        }
      })
      .filter((product) => product);

    const productsToCreate = decoratedProductsArray
      .map((product) => {
        if (product.id === null) {
          delete product.id;
          delete product.parent_sku;
          return product;
        }
      })
      .filter((product) => product);

    if (productsToCreate.length == 0) return;

    const payload = {
      // create: [...productsToCreate.slice(0, 5)], // TODO: this might not work, despite setting the limit higher.
      // update: [...productsToUpdate.slice(0, 5)],
      create: [...productsToCreate], // TODO: this might not work, despite setting the limit higher.
      //update: [...productsToUpdate],
    };

    const bulkProductSaveResult = await WooCommerce.post(
      "products/batch",
      payload
    );
    return bulkProductSaveResult;
  },

  allProductKidsAreDisabled: (WMSProductKids) => {
    const anyEnabledKids = WMSProductKids.filter((kid) => {
      return kid.productSnapshot.b2c_enabled == "1";
    });
    return anyEnabledKids.length > 0 ? false : true;
  },
  listAllMismatchingSTSProducts: async () => {
    const allWeirdProducts = await STSBufferProduct.findAll({
      where: {
        name: null,
      },
    });

    let mismatchingProductsWithDescriptions = [];

    for (const currentProduct of allWeirdProducts) {
      try {
        const matchingSTSProduct = await WooCommerce.get(
          `products/${currentProduct.woocommerce_product_id}`
        );
        const productToReturn = {
          sku: currentProduct.sku,
          name: matchingSTSProduct.data.name,
          type: matchingSTSProduct.data.type,
          woocommerce_id: currentProduct.woocommerce_product_id,
        };
        console.log(productToReturn.name);
        mismatchingProductsWithDescriptions.push(productToReturn);
      } catch (error) {
        console.log(
          `Product retrieval for sku ${currentProduct.sku} not working: ${error}`
        );
      }
    }

    const skusWithNames = mismatchingProductsWithDescriptions.filter(
      (product) => {
        return product.name !== null;
      }
    );

    console.log(skusWithNames.length);
  },
};
