const _ = require("lodash");
require("dotenv").config();
const { Sequelize, QueryTypes } = require("sequelize");
const dbConfig = require("../../../config/config.js")[process.env.NODE_ENV];
const dbConfigWoocommerce = require("../../../config/woocommerce-config.json")[
  process.env.NODE_ENV
];
const sequelize = new Sequelize(dbConfig);
const sequelizeWooCommerce = new Sequelize(dbConfigWoocommerce);
const {
  STSBufferProduct,
  SalsifyProductParent,
  SalsifyProduct,
} = require("../../../models");
const { decorateSTSBufferProduct } = require("./productdecorator");

module.exports = {
  syncProductsFromWooCommerceToBuffer: async () => {
    try {
      const db = sequelize.authenticate();
      const dbWoo = sequelizeWooCommerce.authenticate();
    } catch (error) {
      console.error(`Error authenticating DB's: ${error}`);
    }
    const query =
      "select * from s2s_wc_product_meta_lookup sswpml where sku is not null";
    const allSTSProductsFromSite = await sequelizeWooCommerce.query(query, {
      type: QueryTypes.SELECT,
    });
    // console.log(`Found ${allSTSProductsFromSite.length} products from sts.com`);

    const allHelinoxSalsifyParentProducts = await SalsifyProductParent.findAll({
      where: {
        brand: "Sea To Summit",
      },
      raw: true,
    });

    const allSalsifyBufferProducts = await SalsifyProduct.findAll({
      raw: true,
    });

    const allSalsifyBufferProductsForSTS = allSalsifyBufferProducts.filter(
      (product) => {
        const brand = JSON.parse(product.snapshot)["BRAND"];
        return brand == "Sea to Summit";
      }
    );

    const salsifyProducts = [
      ...allHelinoxSalsifyParentProducts,
      ...allSalsifyBufferProductsForSTS,
    ].map((product) => {
      return {
        sku: product.sku,
        name: product.name,
      };
    });

    // console.log(`Have ${salsifyProducts.length} products to save to buffer`);

    const orderedSalsifyProducts = _.orderBy(salsifyProducts, "sku", "asc");

    let bufferSavePromises = [];

    for (const product of allSTSProductsFromSite) {
      let matchingSalsifyProduct = _.find(orderedSalsifyProducts, {
        sku: product.sku.replace("TEST_", ""),
      });
      if (!matchingSalsifyProduct) {
        matchingSalsifyProduct = _.find(orderedSalsifyProducts, {
          sku: product.sku.replace(/\//g, "-"),
        });
      }
      const decoratedProduct = decorateSTSBufferProduct(
        product,
        matchingSalsifyProduct
      );

      bufferSavePromises.push(
        module.exports.stsBufferProductSavePromise(decoratedProduct)
      );
    }

    const resultsOfBufferSave = await Promise.all(bufferSavePromises);
    // console.log("... done saving buffer products.");
  },
  stsBufferProductSavePromise: async (decoratedProduct) => {
    try {
      const foundIt = await STSBufferProduct.findAll({
        where: {
          sku: decoratedProduct.sku,
        },
      });

      if (foundIt.length > 0) {
        return await foundIt[0].update(decoratedProduct);
      } else {
        return await STSBufferProduct.create(decoratedProduct);
      }
    } catch (error) {
      console.error(`Error creating buffer entry promise, ${error}`);
    }
  },
};
