const fetch = require("node-fetch");
const _ = require("lodash");
const moment = require("moment");
const product = require("./product");
require("dotenv").config();
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

module.exports = {
  woocommerceAndSalsifyProduct: (
    relevantDataContainer,
    SalsifyProduct,
    productType = "variable"
  ) => {
    const attributesForParent = module.exports.attributesForSalsifyParent(
      SalsifyProduct,
      relevantDataContainer.allSTSSalsifyVariantProducts
    );

    const matchingSTSProduct = _.find(
      relevantDataContainer.allWooProductsFromBuffer,
      { sku: SalsifyProduct.sku }
    );
    const salsifyDigitalAssets =
      SalsifyProduct.snapshot["salsify:digital_assets"];
    const productImages = salsifyDigitalAssets
      ? salsifyDigitalAssets.map((digitalAsset) => {
          return {
            src: digitalAsset["salsify:url"],
            name: digitalAsset["salsify:name"],
          };
        }, SalsifyProduct)
      : [];

    const productToReturn = {
      name: "TEST_" + SalsifyProduct.name,
      sku: SalsifyProduct.sku,
      type: productType,
      status: "publish",
      description: SalsifyProduct.snapshot["LONG PRODUCT DESCRIPTION"],
      short_description: SalsifyProduct.snapshot["SHORT DESCRIPTION"],
      parent_sku: SalsifyProduct.parent_sku,
      id: matchingSTSProduct ? matchingSTSProduct.woocommerce_product_id : null,
      images: [...productImages],
      attributes: [...attributesForParent],
    };
    return productToReturn;
  },
  decorateSTSBufferProduct: (wooCommerceProduct, matchingSalsifyProduct) => {
    const decoratedBufferProduct = {
      sku: wooCommerceProduct.sku,
      name: matchingSalsifyProduct ? matchingSalsifyProduct.name : null,
      woocommerce_product_id: wooCommerceProduct.product_id,
    };
    return decoratedBufferProduct;
  },
  decorateSTSBufferProductFromSaveResult: (productFromSaveResult) => {
    const decoratedBufferProduct = {
      sku: productFromSaveResult.sku,
      name: productFromSaveResult.name,
      woocommerce_product_id: productFromSaveResult.id,
    };
    return decoratedBufferProduct;
  },
  attributesForSalsifyParent: (SalsifyParent, Salsifykids) => {
    const relevantSalsifyVariants = Salsifykids.filter(
      (variant) => variant.parent_sku == SalsifyParent.sku
    );
    const variantSizeArray = relevantSalsifyVariants
      .map((variant) => variant.size)
      .filter((size) => size);
    const variantColourArray = relevantSalsifyVariants
      .map((variant) => variant.colour)
      .filter((colour) => colour);

    let variantAttributes = [];

    if (variantColourArray.length > 0) {
      const colourOptions = {
        id: 1,
        name: "Colour",
        visible: true,
        variation: true,
        options: [...variantColourArray],
      };
      variantAttributes.push(colourOptions);
    }

    if (variantSizeArray.length > 0) {
      const sizeOptions = {
        id: 2,
        name: "Size",
        visible: true,
        variation: true,
        options: [...variantSizeArray],
      };
      variantAttributes.push(sizeOptions);
    }
    return variantAttributes;
  },
};
