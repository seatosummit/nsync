const fetch = require("node-fetch");
const _ = require("lodash");
const WooCommerceRestApi = require("@woocommerce/woocommerce-rest-api").default;
require("dotenv").config();
const { promisify } = require("util");
const sleep = promisify(setTimeout);
const { prepareHeaders } = require("../../helpers/apitools");
const {
  SalsifyProduct,
  WMSProduct,
  SalsifyProductParent,
  HelinoxBufferProduct,
} = require("../../../models");
const {
  mergeSalsifyAndWMSProductsForShopify,
  shopifyBufferProductFromVariant,
  shopifyBufferParentProductFromSalsify,
} = require("./productdecorator");

const WooCommerce = new WooCommerceRestApi({
  url: process.env.STS_API_URL,
  consumerKey: process.env.STS_API_CONSUMERKEY,
  consumerSecret: process.env.STS_API_CONSUMERSECRET,
  version: "wc/v3",
});

// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

module.exports = {
  updateOrSaveProductToWooCommerce: async (
    arrayOfDecoratedProducts,
    wooCommerceProductIDLookup
  ) => {
    // TODO: check for presence of existing product ID
    // TODO: create data set like below

    const data = {
      create: [
        {
          name: "Woo Single #1",
          type: "variable",
          regular_price: "21.99",
          description:
            "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.",
          short_description:
            "Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.",
          categories: [
            {
              id: 11,
            },
            {
              id: 13,
            },
          ],
          images: [
            {
              src: "http://demo.woothemes.com/woocommerce/wp-content/uploads/sites/56/2013/06/cd_4_angle.jpg",
            },
          ],
        },
      ],
      update: [
        {
          id: 799,
          default_attributes: [
            {
              id: 6,
              name: "Color",
              option: "Green",
            },
            {
              id: 0,
              name: "Size",
              option: "M",
            },
          ],
        },
      ],
      delete: [
        //794
      ],
    };

    const dataset = {};

    try {
      const wooCommerceUpdateResult = await WooCommerce.post(
        "products/batch",
        {}
      );
    } catch (error) {
      console.error(`Error saving product to sts.com: ${error}`);
    }
  },
};
