class NumberTools {
  static priceToShopifyFormat(price) {
    const regex = /[$#@!,]*/gm;
    const charactersRemovedFromPrice = isNaN(price)
      ? price.replace(regex, "")
      : price;
    const formattedPrice = charactersRemovedFromPrice
      ? Number.parseFloat(charactersRemovedFromPrice)
      : null;
    return formattedPrice;
  }

  static twoDecimalsRounded(value) {
    return Math.round((value + Number.EPSILON) * 100) / 100;
  }

  static convertToInt(value) {
    const parsedInt = Number.isNaN(parseInt(value)) ? 0 : parseInt(value);
    return parsedInt;
  }
}

module.exports.NumberTools = NumberTools;
