const { valueExistsAndIsNotEmpty } = require("./stringTools");
const _ = require("lodash");

class SalsifyTools {
  static isSalsifyProductParent(args) {
    const salsifyEntry = args?.salsifyEntry;
    if (!salsifyEntry) throw `No Salsify entry given`;
    const hasNoParent =
      salsifyEntry.hasOwnProperty("salsify:parent_id") === false;
    return hasNoParent;
  }
  static isSalsifySimpleProduct(args) {
    const salsifyEntry = args?.salsifyEntry;
    if (!salsifyEntry) throw `No Salsify entry given`;
    const isParent = this.isSalsifyProductParent({ salsifyEntry });
    const isSetToSimpleProduct =
      salsifyEntry["Simple or Configurable SKU Type"] === "simple";
    return isParent && isSetToSimpleProduct;
  }
  static extractTechSpecs(args) {
    const salsifyEntry = args?.salsifyEntry;
    if (!salsifyEntry) throw `No Salsify entry given`;
    let setOfTechSpecs = new Set();
    // TODO: wait for STS tech specs to get cleared up to extend this.
    // It's potentially going to be like /Tech spec|BC_ML/ or whatever.
    // Or a lookup table of values. Task https://app.clickup.com/t/2fz541m
    const techSpecsRegex = /Tech spec/;
    const lengthOfTechSpecsSlice = 11;
    for (const key in salsifyEntry) {
      if (techSpecsRegex.test(key)) {
        const element = salsifyEntry[key];
        const keySliced = key.slice(lengthOfTechSpecsSlice);
        const keyWithFirstCap = keySliced[0].toUpperCase() + keySliced.slice(1);
        let newObject = {};
        newObject[keyWithFirstCap] = element;
        setOfTechSpecs.add(newObject);
      }
    }
    const arrayOfTechSpecs = Array.from(setOfTechSpecs);
    return arrayOfTechSpecs;
  }
  static extractUSPs(args) {
    const salsifyEntry = args?.salsifyEntry;
    if (!salsifyEntry) throw `No Salsify entry given`;
    const techSpecs = _.values(
      _.pickBy(salsifyEntry, function (value, key) {
        return /USP \d/.test(key);
      })
    );
    return techSpecs;
  }
}

module.exports.SalsifyTools = SalsifyTools;
