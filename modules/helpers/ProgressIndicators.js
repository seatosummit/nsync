const { startCase } = require("lodash");
const { Logger } = require("./Logger");
const { valueIsNullOrUndefined } = require("./stringTools");

class ProgressIndicators {
  static showProgress(stats, increase = 100, stdout = false) {
    if (valueIsNullOrUndefined(stats.total)) stats.total = 1;
    stats.current += stats.increase ? stats.increase : increase;
    if (
      Math.floor((stats.current / stats.total) * 100) >
      Math.floor(((stats.current - increase) / stats.total) * 100)
    ) {
      if (stdout) {
        process.stdout.write(
          `${Math.floor((stats.current / stats.total) * 100)}% done. `
        );
      } else {
        Logger.info(
          `${Math.floor((stats.current / stats.total) * 100)}% done.`
        );
      }
    }
  }
}

module.exports.ProgressIndicators = ProgressIndicators;
