// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");
const fs = require("fs");
const fsp = fs.promises;
const { writeFile, appendFile, open } = require("fs/promises");
const readline = require("readline");
const request = require("request");
const { CSVWriter } = require("./CSVWriter");
const path = require("path");
const csv = require("@fast-csv/parse");
const { Logger } = require("./Logger");

module.exports = {
  moveFile: async (fileName, oldPath, newPath) => {
    if (!fs.existsSync(newPath)) {
      fs.mkdirSync(newPath);
    }
    await fs.rename(
      oldPath + fileName,
      newPath + fileName,
      function (err, data) {
        if (err) console.error("error", err);
      }
    );
  },
  getFiles: async (filePath, filetype = false) => {
    let fileList = await fsp.readdir(filePath, function (err, files) {
      if (err) {
        return console.error("Unable to scan directory: " + err);
      }
    });
    if (filetype) {
      fileList = fileList.filter(
        (el) => path.extname(el).toLowerCase() === `.${filetype}`
      );
    }
    return fileList;
  },
  createDir: async (dirPath) => {
    if (!fs.existsSync(dirPath)) {
      await fsp.mkdir(dirPath, function (err, files) {
        if (err) {
          return console.error("Unable to scan directory: " + err);
        }
      });
    }
    return true;
  },
  writeFile: async (path, fileData) => {
    return new Promise(function (resolve, reject) {
      fs.writeFile(path, fileData, { flag: "w" }, (error) => {
        if (error) {
          console.error(`problem saving file: ${error}`);
          reject(error);
        }
        resolve(true);
      });
    });
  },

  writeJsonlFile: async (args) => {
    const path = args?.path;
    if (!path) throw `No file path provided`;
    const linesArray = args?.linesArray;
    if (!linesArray) throw `No lines to write`;
    try {
      const fileHandler = await open(path, "w+");
      for (const line of linesArray) {
        await appendFile(fileHandler, `${JSON.stringify(line)}\n`);
      }
      await fileHandler.close();
      return true;
    } catch (error) {
      throw `Problem writing to JSONL file: ${error}`;
    }
  },

  parseJSONLFileFromURL: async (args) => {
    const fileURL = args?.fileURL;
    return new Promise((resolve, reject) => {
      if (!fileURL) reject(`No URL defined?`);
      const lineBuffer = [];
      const readInterface = readline.createInterface({
        input: request.get(fileURL),
      });
      readInterface.on("line", function (line) {
        lineBuffer.push(JSON.parse(line));
      });
      readInterface.on("close", () => resolve(lineBuffer));
      readInterface.on("error", (err) => reject(err));
    });
  },

  deleteFile: (path) => {
    try {
      fs.unlinkSync(path);
      return true;
    } catch (error) {
      Logger.error(`Error deleting file at path ${path}`);
      return false;
    }
  },

  async createBadShopifyPayloadsCSV(args) {
    try {
      const arrayOfPayloads = args?.arrayOfPayloads ?? [];
      if (arrayOfPayloads.length === 0) return true;
      const shopName = args?.shopName ?? "b2b_dist";
      const CSVHeaders = [
        { id: "productEnhancementId", title: "Product Enhancement buffer ID" },
        { id: "productSKU", title: "Product SKU" },
        { id: "error", title: "Error message" },
      ];

      this.CSV = new CSVWriter({
        path: `./${shopName}_missingImages.csv`,
        header: CSVHeaders,
        records: arrayOfPayloads,
      });
      await this.CSV.create();
    } catch (error) {
      Logger.error(`Problem creating the Shopify bad payloads CSV: ${error}`);
    }
  },

  async createMissingImagesCSV(args) {
    const productsArray = args?.productsArray;
    const shopName = args?.shopName ?? "b2b_dist";
    const CSVHeaders = [
      { id: "type", title: "Type" },
      { id: "product_name", title: "Product Name" },
      { id: "variant_name", title: "Variant Name" },
      { id: "product_status", title: "Product Status" },
      { id: "sku", title: "Variant SKU" },
      { id: "vendor", title: "Vendor" },
      { id: "product_id", title: "Shopify Product ID" },
      { id: "variant_id", title: "Shopify Variant ID" },
    ];

    const missingImageEntries = [];
    for (const product of productsArray) {
      missingImageEntries.push({
        type: product.type,
        product_name: product.product_name,
        variant_name: product.variant_name,
        product_id: product.product_id,
        variant_id: product.variant_id,
        sku: product.sku,
        vendor: product.vendor,
        product_status: product.status,
      });
      for (const variant of product.variants) {
        missingImageEntries.push({
          type: variant.type,
          product_name: variant.product_name,
          variant_name: variant.variant_name,
          product_id: variant.product_id,
          variant_id: variant.variant_id,
          sku: variant.sku,
          vendor: variant.vendor,
          product_status: product.status,
        });
      }
    }

    this.CSV = new CSVWriter({
      path: `./${shopName}_missingImages.csv`,
      header: CSVHeaders,
      records: missingImageEntries,
    });
    await this.CSV.create();
  },

  async createSkuAndUrlList(args) {
    const arrayOfSKUs = args?.arrayOfSKUs;
    if (!arrayOfSKUs) throw `No data to create CSV. Check your parameters`;
    const CSVHeaders = [
      { id: "sku", title: "SKU" },
      { id: "url", title: "URL" },
    ];

    this.CSV = new CSVWriter({
      path: `./VariantImageURLLookup.csv`,
      header: CSVHeaders,
      records: arrayOfSKUs,
    });
    await this.CSV.create();
  },

  async createPriceDifferenceCSV(args) {
    const differentialsArray = args?.differentialsArray;
    const shopName = args?.shopName;
    if (!differentialsArray)
      throw `No data to create CSV. Check your parameters`;
    const CSVHeaders = [
      { id: "sales_channel", title: "Store name" },
      { id: "sku", title: "SKU" },
      { id: "shopify_price", title: "Shopify Price" },
      { id: "wms_price", title: "WMS Price" },
      { id: "shopify_compare_price", title: "Shopify Compare Price" },
      { id: "buffer_compare_price", title: "Buffer Compare Price" },
      { id: "price_to_be_updated", title: "Price to be updated to Shopify" },
      {
        id: "comparison_price_to_be_updated",
        title: "Comparison price to be updated to Shopify",
      },
      { id: "product_status", title: "Shopify Product status" },
      { id: "product_id", title: "Shopify Product ID" },
      { id: "stock", title: "Shopify Stock" },
      { id: "variant_id", title: "Shopify Variant ID" },
    ];
    const priceDifferenceEntries = differentialsArray.map((item) => {
      return {
        sales_channel: item.sales_channel,
        sku: item.sku,
        shopify_price: item.shopify_price,
        wms_price: item.wms_price,
        shopify_compare_price: item.shopify_compare_price,
        buffer_compare_price: item.buffer_compare_price,
        price_to_be_updated: item.price_to_be_updated,
        comparison_price_to_be_updated: item.comparison_price_to_be_updated,
        product_status: item.product_status,
        product_id: item.product_id,
        variant_id: item.variant_id,
        stock: item.stock,
      };
    });
    this.CSV = new CSVWriter({
      path: `./${shopName}_priceDifference.csv`,
      header: CSVHeaders,
      records: priceDifferenceEntries,
    });
    await this.CSV.create();
  },

  parseCSV: async (file) => {
    return new Promise(function (resolve, reject) {
      const results = [];
      csv
        .parseFile(file, { headers: true })
        .on("data", (row) => results.push(module.exports.sanitiseObject(row)))
        .on("end", function () {
          resolve(results);
        })
        .on("error", reject);
    });
  },
  sanitiseObject: (dataObject) => {
    let sanitisedData = [];
    for (const [key, value] of Object.entries(dataObject)) {
      let lineKey = key.replace(/\s+|'/g, "");
      let lineValue = value.replace(/'/g, "");
      sanitisedData[lineKey] = lineValue;
    }
    return sanitisedData;
  },
};
