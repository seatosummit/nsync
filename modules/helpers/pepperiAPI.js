const fetch = require("node-fetch");
require("dotenv").config();
const { prepareHeaders } = require("./apitools");
const { Logger } = require("./Logger");

module.exports = {
  deletePepperiItem: async (headers, sku) => {
    try {
      if (!headers) {
        headers = await prepareHeaders();
      }
      const response = await fetch(
        `https://api.pepperi.com/v1.0/items/externalid/${sku}`,
        {
          method: "DELETE",
          headers: headers.PepperiHeaders,
        }
      );
      if (response.status === 200) {
        Logger.info(`Removed SKU ${sku}`);
      } else {
        throw `Response ${response.status} when deleting ${sku}`;
      }
    } catch (error) {
      Logger.error(`Error removing SKU: ${error}`);
    }
  },
  getPepperiItemsWithPage: async (headers, page = 1) => {
    if (!headers) {
      headers = await prepareHeaders();
    }
    try {
      const response = await fetch(
        `https://api.pepperi.com/v1.0/items?page=${page}&fields=ExternalID,Name,Hidden`,
        {
          headers: headers.PepperiHeaders,
        }
      );
      if (response.status !== 200) {
        throw `Response other than 200 - ${response.status}: ${response.statusText}`;
      }
      const responseJSON = await response.json();
      return responseJSON;
    } catch (error) {
      Logger.error(`Error fetching pricing from WMS: ${error}`);
      return [];
    }
  },
};
