// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");
const _ = require("lodash");
const { DateTime, Interval } = require("luxon");
const { valueExistsAndIsNotEmpty } = require("./stringTools");
const { Logger } = require("./Logger");

module.exports = {
  todayIsBetween: (startDate, endDate, dateFormat) => {
    try {
      if (!valueExistsAndIsNotEmpty(startDate)) throw `Start date empty`;
      if (!valueExistsAndIsNotEmpty(endDate)) throw `End date empty`;
      const today = DateTime.now();
      const start = DateTime.fromISO(new Date(startDate).toISOString());
      const end = DateTime.fromISO(new Date(endDate).toISOString());
      if (!start.isValid) throw `Start date has a bad format`;
      if (!end.isValid) throw `End date has a bad format`;
      const interval = Interval.fromDateTimes(start, end);
      const isWithinInterval = interval.contains(today);
      return isWithinInterval;
    } catch (error) {
      Logger.error(`Error checking dates: ${error}`);
      return false;
    }
  },
  daysSince: (inputDate) => {
    try {
      if (!valueExistsAndIsNotEmpty(inputDate)) throw `Input date empty`;
      const today = DateTime.now();
      const sinceDate = DateTime.fromISO(new Date(inputDate).toISOString());
      if (!sinceDate.isValid) throw `Input date has a bad format`;
      const diffInDays = today.diff(sinceDate, "days").toObject();
      return diffInDays.days;
    } catch (error) {
      Logger.error(`Error checking dates: ${error}`);
      return false;
    }
  },

  stringToISODate(randomFormat) {
    const firstAttempt = new Date(randomFormat).toISOString();
    return firstAttempt;
  },

  parseDateWithFormat(args) {
    const expectedFormat = args?.expectedFormat ?? "yyyy-MM-dd HH:mm:ss";
    const dateString = args?.dateString;
    if (!dateString) throw `Need a date string to parse`;
    const parsedDate = DateTime.fromFormat(dateString, expectedFormat, {
      locale: "en-AU",
    });
    const formattedDateString = parsedDate.toISO({
      suppressMilliseconds: true,
    });
    return formattedDateString;
  },

  sqlDateWithinMonths(args) {
    const dateString = args?.dateString;
    const numberOfMonths = args?.months;
    if (!dateString) throw `Need a date string to parse`;
    const now = DateTime.now();
    const parsedDate = DateTime.fromSQL(dateString);
    const intervalToCompare = Interval.fromDateTimes(now, parsedDate);
    const months = intervalToCompare.length("months");
    return months < numberOfMonths;
  },

  dateForOrderNotifications(args) {
    const date = args?.shopifyDate;
    const dateForFormatting = DateTime.fromISO(date);
    return dateForFormatting.toLocaleString({
      weekday: "short",
      month: "short",
      day: "2-digit",
      hour: "2-digit",
      minute: "2-digit",
    });
  },
};
