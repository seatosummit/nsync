class DatabaseConnectionFailedError extends Error {
  constructor(error, environmentVariables) {
    const amendedMessage = `Unable to connect to database: ${error}, NODE_ENV: ${environmentVariables.NODE_ENV}`;
    super(amendedMessage);
    this.name = "DatabaseConnectionFailedError";
  }
}
class APILimitsExceededError extends Error {
  constructor(error) {
    const amendedMessage = `API Limits exceeded. Adjust your throttle.`;
    super(amendedMessage);
    this.name = "APILimitsExceededError";
  }
}
class NotFoundError extends Error {
  constructor(error) {
    const amendedMessage = `Item not found. Check your query.`;
    super(amendedMessage);
    this.name = "NotFoundError";
  }
}

class API500Error extends Error {
  constructor(error) {
    const amendedMessage = `API returned a 500 error`;
    super(amendedMessage);
    this.name = "API500Error";
  }
}
module.exports.DatabaseConnectionFailedError = DatabaseConnectionFailedError;
module.exports.APILimitsExceededError = APILimitsExceededError;
module.exports.NotFoundError = NotFoundError;
module.exports.API500Error = API500Error;
