const _ = require("lodash");
const fs = require("fs");
const path = require("path");
const fetch = require("node-fetch");
const FormData = require("form-data");
require("dotenv").config();

const {
  valueIsNullOrUndefined,
  valueExistsAndIsNotEmpty,
} = require("./stringTools");
const { Logger } = require("./Logger");
const {
  MyobConnectionError,
  MyobAPIError,
} = require("../myob/errors/MyobErrors");
class MyobAPI {
  constructor() {
    this.myobConfig = {
      grant_type: process.env.MYOB_GRANT_TYPE,
      client_id: process.env.MYOB_CLIENT_ID,
      client_secret: process.env.MYOB_CLIENT_SECRET,
      username: process.env.MYOB_USERNAME,
      password: process.env.MYOB_PASSWORD,
      scope: process.env.MYOB_SCOPE,
    };
    this.myobToken = {};
    this.myobHost = process.env.MYOB_HOST;
    this.myobBaseEndpoint = process.env.MYOB_BASE_ENDPOINT;
  }

  configIsValid(config) {
    if (!config.grant_type) return false;
    if (!config.client_id) return false;
    if (!config.client_secret) return false;
    if (!config.username) return false;
    if (!config.password) return false;
    if (!config.scope) return false;
    return true;
  }

  async connect(args) {
    try {
      const config = args?.config ? args.config : this.myobConfig;
      if (!this.configIsValid(config))
        throw new MyobConnectionError(`Config not valid. Can't connect.`);
      const params = new URLSearchParams();
      for (const key in config) {
        if (Object.hasOwnProperty.call(config, key)) {
          const connectionConfigValue = config[key];
          params.append(key, connectionConfigValue);
        }
      }
      const response = await fetch(`${this.myobHost}/identity/connect/token`, {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
        body: params,
      });
      this.myobToken = await response.json();
    } catch (error) {
      throw new MyobConnectionError(`Error connecting: ${error}`);
    }
  }

  async getSalesOrders() {
    try {
      return await this.MyobAPICall({
        entity: "SalesOrder",
        expand: "Details",
        select:
          "OrderNbr,OrderType,CustomerID,Details/InventoryID,Details/OrderQty,Details/UnitPrice",
      });
    } catch (error) {
      throw error;
    }
  }
  async getShipments() {
    try {
      return await this.MyobAPICall({
        entity: "SalesOrder",
        expand: "Shipments",
        select: "OrderNbr,OrderType,Shipments/InvoiceNbr,Shipments",
      });
    } catch (error) {
      throw error;
    }
  }

  async MyobAPICall(args) {
    try {
      if (!this.myobToken) this.myobToken = await this.connect();
      const myobBaseEndpoint = args?.myobBaseEndpoint ?? this.myobBaseEndpoint;
      const entity = args?.entity;
      if (!entity)
        throw new MyobAPIError(`No entity given. Please provide one.`);
      const method = args?.method ?? "GET";
      const body = args?.body ?? null;
      const select = args?.select;
      const expand = args?.expand;

      const settings = {
        method,
        headers: {
          "Authorization": `Bearer ${this.myobToken.access_token}`,
          "Content-length": 0,
          "Content-Type": "application/json",
        },
        timeout: 0,
        body: body ? JSON.stringify(body) : null,
      };
      const url = new URL(`${this.myobHost}/${myobBaseEndpoint}/${entity}`);
      if (select) url.searchParams.append("$select", select);
      if (expand) url.searchParams.append("$expand", expand);

      const response = await fetch(url.toString(), settings);

      switch (response.status) {
        case 204:
          //console.log(`Product updated successfully.`);
          return true;
        case 400:
          throw `Bad Request. Check what you're sending.`;
        case 401:
          throw `Unauthorized. Check your API credentials.`;
        case 403:
          throw `Forbidden. Check your API credentials.`;
        case 404:
          if (method == "put") {
            throw `Not found. Check your item ID`;
          } else {
            console.error(`Not found.`);
            return false;
          }
        case 408:
          throw `Timeout with Shopify API`;
        case 500:
          throw `500 error with Shopify API`;
        default:
          break;
      }
      const responseData = await response.json();
      return responseData;
    } catch (error) {
      throw new MyobAPIError(error);
    }
  }
}
module.exports.MyobAPI = MyobAPI;
