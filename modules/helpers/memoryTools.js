const { Logger } = require("./Logger");
require("dotenv").config();

module.exports = {
  currentMemoryUsage: (message) => {
    if (process.env.LOG_MEMORY_USAGE === "YES") {
      const currentMemory = Math.round(
        process.memoryUsage().heapUsed / 1024 / 1024
      );
      if (message) {
        Logger.info(`Memory usage ${message}: ${currentMemory} MB`);
      } else {
        Logger.info(`Memory usage: ${currentMemory} MB`);
      }
    }
  },
};
