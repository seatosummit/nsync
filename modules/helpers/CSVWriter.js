const createCsvWriter = require("csv-writer").createObjectCsvWriter;

class CSVWriter {
  constructor(args) {
    this.CSV = createCsvWriter({
      path: args?.path ?? "../output.csv",
      header: args?.header ?? [],
    });
    this.records = [...(args?.records ?? [])];
  }
  async create() {
    await this.CSV.writeRecords(this.records);
  }
}

module.exports.CSVWriter = CSVWriter;
