require("dotenv").config();

/**
 * An adaptor for any future loggers.
 */
// TODO: timestamps based on env var
// TODO: plug in a proper logging output.
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

// Logging levels: ALL/WARNINGS/ERRORS
class Logger {
  static info(message, responseObject) {
    if (process.env.LOG_INFO === "YES") {
      console.info(message);
      if (responseObject) console.info(responseObject);
    }
  }
  static error(message, errorObject) {
    if (process.env.LOG_ERRORS === "YES") {
      console.error(message);
      if (errorObject) console.info(errorObject);
    }
  }
  static warning(message, errorObject) {
    if (process.env.LOG_WARNINGS === "YES") {
      console.warn(message);
      if (errorObject) console.info(errorObject);
    }
  }
}

module.exports.Logger = Logger;
