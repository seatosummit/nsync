// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");
const _ = require("lodash");
const slugify = require("slugify");
const moment = require("moment");
const { DateTime } = require("luxon");
const { IntegrationRuleset } = require("./IntegrationRuleset");
const { Logger } = require("./Logger");
const { skuIsZCoded } = require("./stringTools");

module.exports = {
  checkForRule: (args) => {
    const ShopifyStore = args?.ShopifyStore;
    const rule = args?.rule;
    if (!ShopifyStore) return false;
    const rules = ShopifyStore.SalesChannelIntegrationRuleset
      ? ShopifyStore.SalesChannelIntegrationRuleset.toJSON()
      : {};
    const ruleResult = rules[rule];
    if (!ruleResult) {
      Logger.info(
        `Rule "${rule}" is ${
          ruleResult === undefined ? "not set" : "disabled"
        } for store ${ShopifyStore.name}`
      );
      return false;
    }
    return ruleResult;
  },
  checkForRuleInRuleset(args) {
    const rule = args?.rule;
    const ruleset = args?.ruleset;
    const storeName = args?.storeName;

    if (!rule || !ruleset) return false;

    const ruleResult = ruleset[rule];
    if (!ruleResult) {
      Logger.info(
        `Rule "${rule}" is ${
          ruleResult === undefined ? "not set" : "disabled"
        } for store ${storeName ?? ruleset.SalesChannelId}`
      );
    }
    return ruleResult;
  },
  getOrdersFilter: (args) => {
    const ShopifyStore = args?.ShopifyStore;
    if (!ShopifyStore) return false;
    const rules = ShopifyStore.SalesChannelIntegrationRuleset;
    return rules?.orderExportFilter ?? false;
  },
  getCustomOrderNotificationEmail: (args) => {
    const ShopifyStore = args?.ShopifyStore;
    if (!ShopifyStore) throw `No Shopify Store given`;
    const rules = ShopifyStore.SalesChannelIntegrationRuleset;
    const notificationEmail =
      rules.customOrderNotificationEmail ?? "ljalonen@seatosummit.com.au";
    return notificationEmail;
  },
};
