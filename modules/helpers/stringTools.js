// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");
const _ = require("lodash");
const slugify = require("slugify");
const moment = require("moment");
const { DateTime } = require("luxon");

module.exports = {
  capitaliseFirstLetters: (theString) => {
    if (theString !== "" && theString !== undefined) {
      const lowerCapsString = theString.toLowerCase();
      const words = lowerCapsString.split(" ");
      return words
        .map((word) => {
          if (word.length > 0) {
            return word[0].toUpperCase() + word.substring(1);
          }
        })
        .join(" ");
    } else {
      return null;
    }
  },
  createSlug: (string) => {
    if (string) {
      string = string.toLowerCase();
      string = string.replace(/['"`~!@#$%:;’+]/g, "");
      string = string.replace(/[_]/g, " ");
      string = string.replace(/[/.]/g, "-");
      string = slugify(string, "-", {
        lower: true,
        strict: true,
      });
    }
    return string;
  },

  sanitiseTechSpec(args) {
    const object = args?.object;
    const tag = args?.tag ?? "<br />";
    for (const key in object) {
      if (Object.hasOwnProperty.call(object, key)) {
        let value = object[key].toString();
        if (value === "true") value = "Yes";
        if (value === "false") value = "No";
        object[key] = value.replace(/[\n]/g, tag);
      }
    }
    return object;
  },
  addSpacesAndCapitalise(string, exceptions = [], replacementRules = []) {
    const foundException = _.find(exceptions, { original: string });
    if (foundException) {
      return foundException.converted;
    }

    let convertedString = string.replace(/[_|]/g, " ");
    //convertedString = string.replace(/[`~!@#$'";:,.\[\]\{\}\+\=\-]/g, "");
    convertedString = module.exports.capitaliseFirstLetters(convertedString);
    for (const rule of replacementRules) {
      convertedString = convertedString.replace(
        rule.original,
        rule.replacement
      );
    }
    return convertedString;
  },

  extractSKU(string) {
    const skuRegex =
      /^[a-zA-Z0-9.]+[_-](?!X-)[A-Z0-9]{1,9}[-_]|^[a-zA-Z0-9.]*[_-]/g;
    const skuSearchResults = skuRegex.exec(string);
    const sku = skuSearchResults ? skuSearchResults[0].slice(0, -1) : null;
    return sku;
  },

  extractEAN(WMSProductReference, sku, string) {
    const foundWMSProduct = _.find(WMSProductReference, ["sku", sku]);
    let ean = foundWMSProduct ? foundWMSProduct.ean : null;
    if (ean == null) {
      const eanEndRegex = /[_-](\d{13})[_-]*\.*[jp]*/g;
      const skuSearchResults = eanEndRegex.exec(string);
      ean = skuSearchResults ? skuSearchResults[1] : null;
    }
    return ean;
  },

  valueIsNullOrUndefined(string) {
    if (string === null) return true;
    if (string === undefined) return true;
  },
  valueExistsAndIsNotEmpty(value) {
    if (value === null) return false;
    if (typeof value === "string") {
      if (value === null) return false;
      if (value === undefined) return false;
      if (value === "") return false;
    }
    if (typeof value === "undefined") {
      return false;
    }
    if (Array.isArray(value)) {
      return value.length > 0;
    }
    if (
      Object.keys(value).length === 0 &&
      Object.getPrototypeOf(value) === Object.prototype
    ) {
      return false;
    }
    return true;
  },

  getAdditionalOrderFromFilename(filename) {
    const additionalCheck = /[-_](0[123456789])[_-]*\d*\.*[jp]*/g;
    const additionalCheckResults = additionalCheck.exec(filename);
    return additionalCheckResults ? additionalCheckResults[1] : false;
  },

  hasField(object, fieldname) {
    return fieldname in object;
  },

  hasMetadata(attachment) {
    if (attachment.attributes) {
      if (attachment.attributes.metadata) return true;
    }
    return false;
  },

  getLegacyId(graphQLIdentifier) {
    const idRegex = /gid:\/\/shopify\/\w*\/(\d+)/gm;
    const idSearchResults = idRegex.exec(graphQLIdentifier);
    if (module.exports.valueExistsAndIsNotEmpty(idSearchResults))
      return parseInt(idSearchResults[1]);
    return null;
  },

  getFormattedDate(dateString, outputFormat = "YYYY-MM-DD hh:mm:ss") {
    const parsedDateTime = DateTime.fromISO(dateString);
    if (parsedDateTime.isValid) {
      const formattedDate = parsedDateTime.toISO();
      return formattedDate;
    }
    return null;
  },

  skuIsZCoded(skuToEvaluate) {
    const zCodeRegex = /^[Zz]+/gm;
    const zCodeSearchResults = zCodeRegex.exec(skuToEvaluate);
    const isZCoded =
      module.exports.valueExistsAndIsNotEmpty(zCodeSearchResults);
    return isZCoded;
  },

  getAUImageStackFromFilename(filename) {
    const dimensionsRegex = /[_-][Dd]imensions/g;
    const dimensionsSearchResults = dimensionsRegex.exec(filename);
    const dimensions = dimensionsSearchResults ? true : false;

    const packagingRegex = /[_-][Pp]ackaging/g;
    const packagingSearchResults = packagingRegex.exec(filename);
    const packaging = !dimensions && packagingSearchResults ? true : false;

    const lifestyleRegex = /[_-][Ll]ifestyle/g;
    const lifestyleSearchResults = lifestyleRegex.exec(filename);
    const lifestyle = lifestyleSearchResults ? true : false;

    const additionalCheckResults =
      module.exports.getAdditionalOrderFromFilename(filename);
    const additional =
      !dimensions && !packaging && !lifestyle && additionalCheckResults
        ? true
        : false;

    const primaryRegex = /([_-]Primary[_-]*)\d*\.[jp]/g;
    const primarySearchResults = primaryRegex.exec(filename);
    const primary =
      !dimensions &&
      !packaging &&
      !additional &&
      !lifestyle &&
      primarySearchResults
        ? true
        : false;

    const AUImageStack = {
      AU_USP: "USP",
      AU_FAMILY: "Family",
      AU_PRIMARY: "Primary",
      AU_ADDITIONAL: "Additional",
      AU_PACKAGING: "Packaging",
      AU_DIMENSIONS: "Dimensions",
      AU_LIFESTYLE: "Lifestyle",
      AU_OTHER: "Other",
    };
    if (primary) return AUImageStack.AU_PRIMARY;
    if (dimensions) return AUImageStack.AU_DIMENSIONS;
    if (packaging) return AUImageStack.AU_PACKAGING;
    if (lifestyle) return AUImageStack.AU_LIFESTYLE;
    if (additional) return AUImageStack.AU_ADDITIONAL;
    return AUImageStack.AU_OTHER;
  },

  getNumberAsZeroFilledString(number) {
    if (Number.isNaN(parseInt(number))) return false;
    if (number < 10) {
      return `0${number}`;
    }
    return `${number}`;
  },
};
