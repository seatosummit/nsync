const { promises: fsp } = require("fs");
const fs = require("fs");
var path = require("path");
const sharp = require("sharp");
sharp.cache(false);
const _ = require("lodash");
require("dotenv").config();
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

module.exports = {
  traverseFoldersAndFormatImages: async (
    dir = process.env.IMAGE_RESIZER_LOCATION
  ) => {
    try {
      const folders = await fsp.readdir(dir);
      for (const folder of folders) {
        await module.exports.formatImagesInFolder(path.resolve(dir, folder));
      }
    } catch (error) {
      console.error(`Error saving images, error: ${error}`);
    }
  },

  formatImagesInFolder: async (dir) => {
    const imageFiles = await fsp.readdir(dir);
    const imageSavePromises = imageFiles.map((image) => {
      const file = path.resolve(dir, image);
      return module.exports.formatAndSaveImage(file);
    });

    await Promise.all(imageSavePromises).catch((e) =>
      console.error(`Error saving this batch of images: ${e}`)
    );
    // console.log(`Updated images in folder ${dir}`);
  },
  formatAndSaveImage: async (image) => {
    const fileExtension = path.extname(image);
    if ([".jpg", ".jpeg", ".png", ".bmp", ".tiff"].includes(fileExtension)) {
      try {
        let buffer = await sharp(image)
          .resize({ width: 1300, height: 1300, withoutEnlargement: true })
          .toBuffer();
        return sharp(buffer).toFile(image);
      } catch (error) {
        console.error(`Error saving image ${image}, error ${error}`);
      }
    } else {
      console.log(
        `Error: image ${image} is in wrong format (${fileExtension})`
      );
    }
  },
};
