const SalesChannels = {
  PEPPERI_APP: {
    description: "Pepperi order taking app",
    SQLView: "vSTS_WebProductInfo_PEP_V2",
    name: "PEPPERI_APP"
  },
  B2B_DIST: {
    description: "B2B Distribution site",
    SQLView: "vSTS_WebProductInfoV2",
    name: "B2B_DIST"
  },
  B2C_DIST: {
    description: "B2C Distribution site",
    SQLView: "vSTS_WebProductInfoV2",
    name: "B2C_DIST"
    
  },
  HELINOX: {
    description: "Helinox site",
    SQLView: "vSTS_WebProductInfo_HX",
    name: "HELINOX"
  },
  BLACK_DIAMOND: {
    description: "Black Diamond site",
    SQLView: "",
    name: "BLACK_DIAMOND"
  },
  SEA_TO_SUMMIT: {
    description: "Sea To Summit site",
    SQLView: "vSTS_WebProductInfo_AF",
    name: "SEA_TO_SUMMIT"
  },
};

module.exports = SalesChannels;
