const fetch = require("node-fetch");
const _ = require("lodash");
require("dotenv").config();
const moment = require("moment");
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

module.exports = {
  confirmSKUNameFromWMSBuffer: async (sku, WMSBufferProductsArray = []) => {
    try {
      const foundMatchingBufferProduct = _.find(WMSBufferProductsArray, [
        "sku",
        sku,
      ]);

      if (foundMatchingBufferProduct) {
        return foundMatchingBufferProduct.sku;
      } else {
        // this is a hack due to a few legacy SKUs in the system that contain "/" characters
        const replacedSKU = sku.replace(/\-/g, "/");
        const foundMatchingBufferProductWithReplacedSKU = _.find(
          WMSBufferProductsArray,
          ["sku", replacedSKU]
        );
        return foundMatchingBufferProductWithReplacedSKU.sku;
      }
    } catch (error) {
      console.error(
        `Error fetching product name for SKU ${sku} from buffer: ${error}`
      );
    }
  },
};
