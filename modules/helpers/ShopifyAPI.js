const { loadFile } = require("graphql-import-files");
const giftCardInformationForOrder = loadFile(
  "./graphQL/giftCardInformationForOrder.graphql"
);
const bulkInventoryAdjustment = loadFile(
  "./graphQL/inventoryBulkAdjustQuantityAtLocation.graphql"
);
const addTags = loadFile("./graphQL/addTags.graphql");
const createCollection = loadFile("./graphQL/collectionCreate.graphql");
const updateCollection = loadFile("./graphQL/collectionUpdate.graphql");
const removeTags = loadFile("./graphQL/removeTags.graphql");
const getOrders = loadFile("./graphQL/getOrders.graphql");
const createStagedUpload = loadFile("./graphQL/createStagedUpload.graphql");
const checkBulkOperationQuery = loadFile(
  "./graphQL/checkBulkOperationProgress.graphql"
);
const startBulkVariantExport = loadFile(
  "./graphQL/startBulkVariantExport.graphql"
);
const startBulkProductExport = loadFile(
  "./graphQL/startBulkProductExport.graphql"
);
const startBulkProductIDsExport = loadFile(
  "./graphQL/startBulkProductIDsExport.graphql"
);
const bulkProductImageDataExport = loadFile(
  "./graphQL/startBulkProductImageDataExport.graphql"
);
const startBulkInventoryExport = loadFile(
  "./graphQL/startBulkInventoryItemExport.graphql"
);
const productVariantUpdate = loadFile("./graphQL/productVariantUpdate.graphql");
const productCreate = loadFile("./graphQL/productCreate.graphql");
const productUpdate = loadFile("./graphQL/productUpdate.graphql");
const privateMetafieldUpsert = loadFile(
  "./graphQL/privateMetafieldUpsert.graphql"
);
const bulkOperationRunMutation = loadFile(
  "./graphQL/bulkOperationRunMutation.graphql"
);
const _ = require("lodash");
const fs = require("fs");
const path = require("path");
const fetch = require("node-fetch");
const FormData = require("form-data");
require("dotenv").config();
const throttle = require("fetch-throttle");
const fetchThrottle = throttle(fetch, 5, 1000);
const Shopify = require("shopify-api-node");
const { PromiseTools } = require("../helpers/promisetools");
const { ShopifyConfig } = require("./ShopifyConfig");
const {
  valueIsNullOrUndefined,
  valueExistsAndIsNotEmpty,
} = require("./stringTools");
const { Logger } = require("./Logger");
const {
  ShopifyConnectionError,
  ShopifyConfigError,
  ShopifyGraphQLError,
} = require("../shopify/errors/ProductErrors");
const { file } = require("googleapis/build/src/apis/file");
class ShopifyAPI {
  constructor(storeConfig) {
    if (valueIsNullOrUndefined(storeConfig.name))
      throw new ShopifyConfigError(
        "Can't set up Shopify API. Shopify channel name is missing."
      );
    this.salesChannelMachineName = storeConfig.name;
    this.storeConfig = storeConfig;
    this.shopifyConnection;
    this.shopifyConfigObject;
    this.salesChannelInfo;
  }

  async getSalesChannelInfo() {
    this.salesChannelInfo = await ShopifyConfig.getSalesChannelInfo(
      this.salesChannelMachineName
    );
    return this.salesChannelInfo;
  }

  configIsValid(config) {
    if (!config.shopName) return false;
    if (!config.apiKey) return false;
    if (!config.password) return false;
    if (!config.apiVersion) return false;
    if (!config.autoLimit) return false;
    return true;
  }

  async connect(
    salesChannelMachineName = this.salesChannelMachineName,
    existingConfig = {}
  ) {
    if (this.configIsValid(existingConfig)) {
      this.shopifyConfigObject = {
        ...existingConfig,
      };
    } else {
      try {
        this.shopifyConfigObject = await ShopifyConfig.getConfigForSalesChannel(
          salesChannelMachineName
        );
      } catch (error) {
        throw new ShopifyConfigError(error);
      }
    }
    try {
      this.shopifyConnection = new Shopify({
        shopName: this.shopifyConfigObject.shopName,
        apiKey: this.shopifyConfigObject.apiKey,
        password: this.shopifyConfigObject.password,
        apiVersion: "2022-07",
        autoLimit: this.shopifyConfigObject.autoLimit,
      });
      this.shopifyConnection.on("callLimits", function (limits) {
        if (limits.remaining < 3) {
          Logger.info(`Shopify API limits nearly reached: ${limits.remaining}`);
        }
      });
    } catch (error) {
      throw new ShopifyConnectionError(error);
    }
  }

  async bulkInventoryAdjustment(args) {
    const locationId = args?.locationId ?? false;
    if (!locationId) throw `no location ID provided.`;
    const adjustmentsArray = args?.adjustmentsArray ?? false;
    if (!adjustmentsArray) throw `no adjustmentsArray provided.`;
    const variables = {
      inventoryItemAdjustments: adjustmentsArray,
      locationId: `gid://shopify/Location/${locationId}`,
    };
    try {
      const bulkAdjustmentResult = await this.shopifyConnection.graphql(
        bulkInventoryAdjustment,
        variables
      );
      if (
        !bulkAdjustmentResult.inventoryBulkAdjustQuantityAtLocation
          ?.inventoryLevels
      )
        throw `Error with bulk inventory GraphQL request`;
      return bulkAdjustmentResult;
    } catch (error) {
      throw new ShopifyGraphQLError(error);
    }
  }

  async uploadBulkUpdateFile(args) {
    return new Promise((resolve, reject) => {
      const stagedUploadParams = args?.stagedUploadParams ?? false;
      if (!valueExistsAndIsNotEmpty(stagedUploadParams))
        throw `No staged upload info`;
      const fileURL = args?.fileURL ?? false;
      const uploadURL = "https://shopify.s3.amazonaws.com";
      const uploadForm = new FormData();
      for (const parameter of stagedUploadParams) {
        uploadForm.append(parameter.name, parameter.value);
      }
      const absoluteFilePath = path.resolve(fileURL);
      const fileReadStream = fs.createReadStream(absoluteFilePath);
      if (!fs.existsSync(absoluteFilePath))
        throw `File at path ${fileURL} doesn't exist. Check your path`;
      uploadForm.append("file", fileReadStream);
      uploadForm.submit(uploadURL, (error, response) => {
        if (error) {
          reject(`Issue with uploading to staging area: ${error}`);
        }
        fileReadStream.destroy();
        resolve(response);
      });
    });
  }

  async productVariantBulkMutation(args) {
    const stagedUploadPath = args?.stagedUploadPath;
    try {
      const variantBulkMutationResult = await this.bulkMutation({
        stagedUploadPath,
        mutation: productVariantUpdate,
      });
      return variantBulkMutationResult;
    } catch (error) {
      throw error;
    }
  }

  async productBufferIdBulkMutation(args) {
    const stagedUploadPath = args?.stagedUploadPath;
    try {
      const metafieldUpdateResult = await this.bulkMutation({
        stagedUploadPath,
        mutation: privateMetafieldUpsert,
      });
      return metafieldUpdateResult;
    } catch (error) {
      throw error;
    }
  }
  async productCreateBulkMutation(args) {
    const stagedUploadPath = args?.stagedUploadPath;
    try {
      const productBulkCreationResult = await this.bulkMutation({
        stagedUploadPath,
        mutation: productCreate,
      });
      return productBulkCreationResult;
    } catch (error) {
      throw error;
    }
  }

  async productUpdateBulkMutation(args) {
    const stagedUploadPath = args?.stagedUploadPath;
    try {
      const result = await this.bulkMutation({
        stagedUploadPath,
        mutation: productUpdate,
      });
      return result;
    } catch (error) {
      throw error;
    }
  }

  async productAppendImagesBulkMutation(args) {
    const stagedUploadPath = args?.stagedUploadPath;
    if (!stagedUploadPath)
      throw `stagedUploadPath is needed to run bulk mutations`;
    try {
      const productAppendImages = loadFile(
        "./graphQL/productAppendImages.graphql"
      );
      const productAppendImagesResult = await this.bulkMutation({
        stagedUploadPath,
        mutation: productAppendImages,
      });
      return productAppendImagesResult;
    } catch (error) {
      throw error;
    }
  }

  async bulkMutation(args) {
    const stagedUploadPath = args?.stagedUploadPath;
    const mutation = args?.mutation;
    if (!stagedUploadPath)
      throw `No staged upload path. This is needed to run the bulk mutation`;
    const variables = {
      stagedUploadPath,
      mutation,
    };
    try {
      const bulkMutationResult = await this.shopifyConnection.graphql(
        bulkOperationRunMutation,
        variables
      );
      if (bulkMutationResult.bulkOperationRunMutation.userErrors.length > 0)
        throw bulkMutationResult.bulkOperationRunMutation.userErrors;
      return bulkMutationResult;
    } catch (userErrors) {
      throw new ShopifyGraphQLError(userErrors);
    }
  }

  async bulkOperationQuery(args) {
    const query = args?.query;
    const variables = args?.variables;
    try {
      const bulkOperationResult = await this.shopifyConnection.graphql(
        query,
        variables
      );
      if (bulkOperationResult.bulkOperationRunQuery.userErrors.length > 0)
        throw bulkOperationResult.bulkOperationRunQuery.userErrors;

      return bulkOperationResult.bulkOperationRunQuery.bulkOperation ?? false;
    } catch (userErrors) {
      throw new ShopifyGraphQLError(userErrors);
    }
  }

  async createStagedUpload() {
    try {
      const stagedUploadResponse = await this.shopifyConnection.graphql(
        createStagedUpload
      );
      const anyErrors =
        stagedUploadResponse?.stagedUploadsCreate?.userErrors ?? [];
      if (anyErrors.length !== 0) throw anyErrors;
      const stagedTargets =
        stagedUploadResponse.stagedUploadsCreate?.stagedTargets;
      if (!valueExistsAndIsNotEmpty(stagedTargets))
        throw `Didn't get back staged upload info`;
      return stagedTargets[0].parameters;
    } catch (userErrors) {
      throw new ShopifyGraphQLError(userErrors);
    }
  }

  // TODO: These can all go into a generic adapter

  async startBulkVariantExport() {
    const bulkQueryResult = await this.bulkOperationQuery({
      query: startBulkVariantExport,
    });
    return bulkQueryResult;
  }

  async startBulkProductExport() {
    const bulkQueryResult = await this.bulkOperationQuery({
      query: startBulkProductExport,
    });
    return bulkQueryResult;
  }
  async startBulkProductIDsExport() {
    const bulkQueryResult = await this.bulkOperationQuery({
      query: startBulkProductIDsExport,
    });
    return bulkQueryResult;
  }

  async startBulkProductImageDataExport() {
    try {
      const bulkQueryResult = await this.bulkOperationQuery({
        query: bulkProductImageDataExport,
      });
      return bulkQueryResult;
    } catch (error) {
      throw error;
    }
  }

  async startBulkInventoryItemExport() {
    try {
      const bulkQueryResult = await this.bulkOperationQuery({
        query: startBulkInventoryExport,
      });
      return bulkQueryResult;
    } catch (error) {
      throw error;
    }
  }

  async getUnfulfilledOrders(args) {
    const queryParams = ["fulfillment_status:unshipped"];
    const noTag = args?.noTag;
    const fromDate = args?.fromDate;
    const financialStatus = args?.financialStatus;
    if (fromDate) queryParams.push(`created_at:>${fromDate}`);
    if (noTag) queryParams.push(`tag_not:${noTag}`);
    if (financialStatus)
      queryParams.push(`financial_status:${financialStatus}`);

    const query = queryParams.join(" AND ");
    const variables = {
      query,
    };
    try {
      const result = await this.shopifyConnection.graphql(getOrders, variables);
      const formattedOrders = result?.orders?.edges.map((item) => {
        return {
          id: item.node?.id,
          lineItems: item.node?.lineItems?.edges.map((item) => {
            return { sku: item.node?.sku, quantity: item.node?.quantity };
          }),
          financialStatus: item.node?.displayFinancialStatus,
          fulfillmentStatus: item.node?.displayFulfillmentStatus,
          name: item.node?.name,
          tags: item.node?.tags,
        };
      });
      return formattedOrders;
    } catch (error) {
      throw new ShopifyGraphQLError(error);
    }
  }

  async upsertCollection(args) {
    const collection = args?.collection;
    const operation = args?.operation;
    if (!collection)
      throw new ShopifyGraphQLError(`Need a collection to work with`);
    if (!operation)
      throw new ShopifyGraphQLError(`Need to know if updating or creating`);

    try {
      const variables = collection;
      if (operation === "UPDATE") {
        delete variables.input.publications;
        const collectionResult = await this.shopifyConnection.graphql(
          updateCollection,
          variables
        );
        return collectionResult.collectionUpdate;
      } else if (operation === "CREATE") {
        const collectionResult = await this.shopifyConnection.graphql(
          createCollection,
          variables
        );
        return collectionResult.collectionCreate;
      }
    } catch (error) {
      throw new ShopifyGraphQLError(error);
    }
  }

  async addTagsToOrder(args) {
    const GraphQLOrderID = args?.GraphQLOrderID;
    const tagsToAdd = args?.tagsToAdd ?? [];
    if (!GraphQLOrderID) throw `Need an order ID to add tags`;
    const variables = {
      id: GraphQLOrderID,
      tags: tagsToAdd,
    };
    try {
      const orderTagResult = await this.shopifyConnection.graphql(
        addTags,
        variables
      );
    } catch (error) {
      throw new ShopifyGraphQLError(error);
    }
  }

  async removeTagsFromOrder(args) {
    const GraphQLOrderID = args?.GraphQLOrderID;
    const tagsToRemove = args?.tagsToRemove ?? [];
    if (!GraphQLOrderID) throw `Need an order ID to remove tags`;
    const variables = {
      id: GraphQLOrderID,
      tags: tagsToRemove,
    };
    try {
      const orderTagResult = await this.shopifyConnection.graphql(
        removeTags,
        variables
      );
    } catch (error) {
      throw new ShopifyGraphQLError(error);
    }
  }

  async getGiftCardInformationForOrder(args) {
    const GraphQLOrderID = args?.GraphQLOrderID; //admin_graphql_api_id
    if (!GraphQLOrderID) throw `Need an order ID to get gift card info`;
    const variables = {
      orderid: GraphQLOrderID,
    };
    try {
      if (!this.shopifyConnection) await this.connect();
      const orderResult = await this.shopifyConnection.graphql(
        giftCardInformationForOrder,
        variables
      );
      if (!orderResult) throw `Error with getting order ${GraphQLOrderID}`;
      const giftCardTransactions = _.filter(orderResult.order.transactions, {
        gateway: "gift_card",
      }).map((transaction) => {
        return {
          ...transaction,
          receiptJson: JSON.parse(transaction.receiptJson),
        };
      });
      const giftCardInfo = {
        GraphQLOrderID,
        giftCardTransactions,
      };
      return giftCardInfo;
    } catch (error) {
      throw new ShopifyGraphQLError(error);
    }
  }

  async waitUntilBulkOperationComplete(args) {
    const bulkOperationType = args?.bulkOperationType;
    return new Promise(async (resolve, reject) => {
      try {
        Logger.info(`Checking if there's a bulk operation in progress...`);
        let bulkOperationStatus = await this.checkBulkOperationProgress({
          bulkOperationType,
        });
        if (_.isNull(bulkOperationStatus.currentBulkOperation))
          resolve(bulkOperationStatus.currentBulkOperation);
        while (
          !["COMPLETED", "EXPIRED"].includes(
            bulkOperationStatus?.currentBulkOperation?.status
          )
        ) {
          await PromiseTools.wait(2000);
          bulkOperationStatus = await this.checkBulkOperationProgress({
            bulkOperationType,
          });
          Logger.info(`Bulk operation in progress, waiting...`);
        }
        resolve(bulkOperationStatus.currentBulkOperation);
      } catch (error) {
        reject(error);
      }
    });
  }

  async checkBulkOperationProgress(args) {
    const bulkOperationType = args?.bulkOperationType;
    if (!bulkOperationType)
      throw `Need to know which bulk operation to check progress for`;
    const variables = {
      type: bulkOperationType,
    };
    try {
      const bulkOperationStatus = await this.shopifyConnection.graphql(
        checkBulkOperationQuery,
        variables
      );
      return bulkOperationStatus;
    } catch (error) {
      throw new ShopifyGraphQLError(error);
    }
  }

  async updateVariantPricing(variantEnhancement) {
    try {
      const variantUpdatePayload = {
        price: variantEnhancement.price,
      };
      if (variantEnhancement.compareToPrice) {
        variantUpdatePayload.compare_at_price =
          variantEnhancement.compareToPrice;
      }
      const priceUpdateResult =
        await this.shopifyConnection.productVariant.update(
          variantEnhancement.shopifyVariantId,
          variantUpdatePayload
        );
      return priceUpdateResult;
    } catch (error) {
      Logger.error(
        `Problem with variant SKU ${variantEnhancement.sku} update: ${error}`
      );
    }
  }

  async updateShopifyInventoryLevel(variantInventoryInfo) {
    try {
      if (!variantInventoryInfo) throw `No Inventory`;
      const inventoryUpdatePayload = {
        location_id: variantInventoryInfo.location_id,
        inventory_item_id: variantInventoryInfo.shopifyInventoryItemId,
        available: variantInventoryInfo.shopifyInventoryQuantity,
      };
      const inventoryUpdateResult =
        await this.shopifyConnection.inventoryLevel.set(inventoryUpdatePayload);
      return inventoryUpdateResult;
    } catch (error) {
      Logger.error(`Error updating inventory info: ${error}`);
    }
  }

  async ShopifyAPICall(url, method = "GET", body = null) {
    const settings = {
      method,
      headers: {
        "Authorization": `Bearer ${this.accessToken}`,
        "Content-length": 0,
        "Content-Type": "application/json",
      },
      timeout: 0,
      body: body ? JSON.stringify(body) : null,
    };
    const response = await fetchThrottle(url, settings);

    switch (response.status) {
      case 204:
        //console.log(`Product updated successfully.`);
        return true;
      case 400:
        throw `Bad Request. Check what you're sending.`;
      case 401:
        throw `Unauthorized. Check your API credentials.`;
      case 403:
        throw `Forbidden. Check your API credentials.`;
      case 404:
        if (method == "put") {
          throw `Not found. Check your item ID`;
        } else {
          console.error(`Not found.`);
          return false;
        }
      case 408:
        throw `Timeout with Shopify API`;
      case 500:
        throw `500 error with Shopify API`;
      default:
        break;
    }
    const responseData = await response.json();
    return responseData;
  }
}
module.exports.ShopifyAPI = ShopifyAPI;
