// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");
const _ = require("lodash");
const slugify = require("slugify");
const moment = require("moment");
const { DateTime } = require("luxon");
const { valueExistsAndIsNotEmpty } = require("./stringTools");
const { Logger } = require("./Logger");

module.exports = {
  stringToArray: (inputValue) => {
    if (!valueExistsAndIsNotEmpty(inputValue)) return [];
    const adjustedInputValue =
      inputValue.slice(-1) === "," ? inputValue.slice(0, -1) : inputValue;
    const arrayToReturn = adjustedInputValue
      .split(",")
      .filter((value) => value);
    return arrayToReturn;
  },
};
