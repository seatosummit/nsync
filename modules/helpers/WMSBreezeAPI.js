const fetch = require("node-fetch");
require("dotenv").config();
const moment = require("moment");
// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");
const { prepareHeaders } = require("./apitools");

module.exports = {
  getSpecialPricingFromERP: async () => {
    const headers = await prepareHeaders();
    try {
      const response = await fetch(
        `http://${process.env.WMS_API_LOCATION}/JSON/sql?filter~SELECT%20*%20FROM%20vw_AF_BD_SpecialPricing`,
        { headers: headers.WMSHeaders, timeout: 0 }
      );
      if (response.status !== 200) {
        throw `Response other than 200 - ${response.status}: ${response.statusText}`;
      }
      const responseJSON = await response.json();
      if (responseJSON.length === 0) {
        throw `No pricing found`;
      }
      return responseJSON;
    } catch (error) {
      console.error(`Error fetching pricing from WMS: ${error}`);
      return {
        status: 500,
        message: error,
      };
    }
  },
  productNameFromWMS: async (WMSHeaders, sku) => {
    try {
      const response = await fetch(
        `http://${process.env.WMS_API_LOCATION}/json/sql?filter~SELECT%20*%20FROM%20vGTH_Items_AF_Lookup%20where%20SKU%20=%20'${sku}'`,
        { headers: WMSHeaders, timeout: 0 }
      );
      if (response.status !== 200) {
        throw `Response other than 200 - ${response.status}: ${response.statusText}`;
      }
      const responseJSON = await response.json();
      if (responseJSON.length === 0) {
        throw `No product found`;
      }
      return responseJSON[0];
    } catch (error) {
      console.log(
        `Error fetching product name for SKU ${sku} from WMS: ${error}`
      );
      return {
        status: 500,
        message: error,
      };
    }
  },

  selectFromView: async (view = "vSTS_WebProductInfo_HX") => {
    const headers = await prepareHeaders();
    try {
      const response = await fetch(
        `http://${process.env.WMS_API_LOCATION}/JSON/sql?filter~SELECT%20*%20FROM%20${view}`,
        { headers: headers.WMSHeaders, timeout: 0 }
      );
      if (response.status !== 200) {
        throw `Response other than 200 - ${response.status}: ${response.statusText}`;
      }
      const responseJSON = await response.json();
      if (responseJSON.length === 0) {
        throw `No product found`;
      }
      return responseJSON;
    } catch (error) {
      console.log(`Error fetching products from WMS view: ${error}`);
      return {
        status: 500,
        message: error,
      };
    }
  },
};
