// require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");
const _ = require("lodash");
const slugify = require("slugify");
const moment = require("moment");
const { DateTime } = require("luxon");
const { valueExistsAndIsNotEmpty } = require("./stringTools");
const {
  BrandfolderBufferAsset,
  BrandfolderBufferAttachment,
  PepperiBufferImage,
} = require("../../models");
const { Logger } = require("./Logger");

module.exports = {
  getAssetDetailsToProcess(BrandfolderAsset, payloadIncludes) {
    //TODO: look for BrandfolderAsset custom field ID in payloadIncludes
    const customFieldValueID = BrandfolderAsset.relationships
      ?.custom_field_values?.data
      ? BrandfolderAsset.relationships?.custom_field_values?.data[0].id
      : null;
    const attachmentId = BrandfolderAsset.relationships?.attachments?.data
      ? BrandfolderAsset.relationships?.attachments?.data[0].id
      : null;

    if (!customFieldValueID) return false;
    if (!attachmentId) return false;

    const customFieldValue = "todo";

    // TODO: use custom field and attachment to get those things and return them
    // { brand, attachment}
  },
  getCompatibleAttachment(
    BrandfolderAttachmentsArray,
    fileExtensionsArray = ["jpg", "png", "jpeg"]
  ) {
    if (!valueExistsAndIsNotEmpty(BrandfolderAttachmentsArray)) return false;
    const matchingAttachment = _.find(BrandfolderAttachmentsArray, (item) => {
      return fileExtensionsArray.includes(item.extension);
    });
    return matchingAttachment ?? false;
  },

  async getAllRelevantProductImages({
    stack = ["Primary", "Additional"],
    maxAdditional = 3,
    brandfolderSectionIDs = [
      "ht7s95whgp3b7txkgf987nt",
      "q7cphz-8wbqqw-c4dg2u",
      "tb6mwcswp9ppx3thgqpk336",
    ],
  }) {
    try {
      const productImages = (
        await BrandfolderBufferAsset.findAll({
          include: BrandfolderBufferAttachment,
        })
      ).filter((asset) => {
        const isRelevantStack = stack.includes(asset.au_office_image_stack);
        if (!isRelevantStack) return false;
        if (asset.au_office_image_stack === "Additional") {
          return asset.au_office_image_ordering
            ? asset.au_office_image_ordering < maxAdditional
            : false;
        }
        if (brandfolderSectionIDs.includes(asset.section_id) === false) {
          return false;
        }
        return true;
      });
      return productImages;
    } catch (error) {
      Logger.error(
        `Problem with fetching relevant Brandfolder images: ${error}`
      );
      return false;
    }
  },
};
