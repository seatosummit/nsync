const {
  SalesChannel,
  SalesChannelIntegrationRuleset,
} = require("../../models");
const Sequelize = require("sequelize");
const { Logger } = require("./Logger");
const Op = Sequelize.Op;

class ShopifyConfig {
  static getConfig() {
    let BlackDiamondShopifyConfig;
    try {
      if (process.env.SHOPIFY_CONFIG) {
        BlackDiamondShopifyConfig = JSON.parse(process.env.SHOPIFY_CONFIG);
      } else {
        BlackDiamondShopifyConfig =
          require("../../../config/shopify/config.json")["stores"][
            "BLACKDIAMOND"
          ];
      }
      return BlackDiamondShopifyConfig;
    } catch (error) {
      console.log("Got an error fetching the Shopify config");
    }
  }

  static async getConfigsFromDatabase(args) {
    const query = {
      where: {
        shopify_config: { [Op.not]: null },
      },
      include: SalesChannelIntegrationRuleset,
    };
    const machineNameOfSalesChannel = args?.machineNameOfSalesChannel;
    if (machineNameOfSalesChannel)
      query.where.machineName = machineNameOfSalesChannel;
    const allStoreConfigs = await SalesChannel.findAll(query);
    return allStoreConfigs;
  }

  static async getSalesChannelInfo(machineNameOfSalesChannel) {
    try {
      const salesChannel = await SalesChannel.findOne({
        where: {
          machineName: machineNameOfSalesChannel,
        },
      });
      return salesChannel;
    } catch (error) {
      Logger.error(error);
    }
  }

  static async getConfigForSalesChannel(machineNameOfSalesChannel) {
    try {
      const singleStoreConfig = await SalesChannel.findOne({
        where: {
          machineName: machineNameOfSalesChannel,
        },
      });
      return singleStoreConfig.shopify_config;
    } catch (error) {
      Logger.error(error);
    }
  }
}

module.exports.ShopifyConfig = ShopifyConfig;
