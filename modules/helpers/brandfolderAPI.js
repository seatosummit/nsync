const fetch = require("node-fetch");
require("dotenv").config();
const { brandfolderHeaders } = require("./apitools");
const { Logger } = require("./Logger");
const JSONSchemaValidator = require("ajv");
const { APILimitsExceededError, NotFoundError } = require("./CustomErrors");

class BrandfolderAPI {
  constructor(brandfolderID) {
    this.apiRootURL = "https://brandfolder.com/api/v4";
    this.headers = brandfolderHeaders();
    this.JSONValidator = new JSONSchemaValidator();
    this.brandfolderID = brandfolderID ?? "q7cphz-8wbqqw-1dru9z";
  }

  async getAssetsWithPage(
    page = "1",
    per = 200,
    brandfolder = "q7cphz-8wbqqw-1dru9z"
  ) {
    const url = `/brandfolders/${brandfolder}/assets?page=${page}&include=custom_field_values,attachments,tags,collections,labels,section&fields=metadata&per=${per}`;
    try {
      const response = await this.BrandfolderAPICall(url, "GET");
      return response;
    } catch (error) {
      Logger.error(`Error getting assets from Brandfolder: ${error}`);
      return false;
    }
  }

  async getAssetsWithPage(
    page = "1",
    per = 200,
    brandfolder = "q7cphz-8wbqqw-1dru9z"
  ) {
    const url = `/brandfolders/${brandfolder}/assets?page=${page}&include=custom_field_values,attachments,tags,collections,labels,section&fields=metadata&per=${per}`;
    try {
      const response = await this.BrandfolderAPICall(url, "GET");
      return response;
    } catch (error) {
      Logger.error(`Error getting assets from Brandfolder: ${error}`);
      return false;
    }
  }

  async getLatestAssetsWithPage(
    page = 1,
    per = 200,
    daysBack = 30,
    brandfolder = "q7cphz-8wbqqw-1dru9z"
  ) {
    const url = `/brandfolders/${brandfolder}/assets?page=${page}&include=custom_field_values,attachments,tags,collections,labels,section&fields=metadata&per=${per}&search=updated_at%3A%3Enow-${daysBack}d`;
    try {
      const response = await this.BrandfolderAPICall(url, "GET");
      return response;
    } catch (error) {
      Logger.error(`Error getting assets from Brandfolder: ${error}`);
      return false;
    }
  }

  async getAssetsFromOrganisationWithPage(
    page = "1",
    per = 200,
    organisation = "q7cphz-8wbqqw-2bbekx"
  ) {
    const url = `/organizations/${organisation}/assets?page=${page}&include=custom_field_values,attachments,tags,collections,labels,section&fields=metadata&per=${per}`;
    try {
      const response = await this.BrandfolderAPICall(url, "GET");
      return response;
    } catch (error) {
      Logger.error(`Error getting assets from Brandfolder: ${error}`);
      return false;
    }
  }

  async getLabelsWithPage(page = "1", brandfolder_id = "q7cphz-8wbqqw-1dru9z") {
    const url = `/brandfolders/${brandfolder_id}/labels?per=1000&page=${page}`;
    try {
      const response = await this.BrandfolderAPICall(url, "GET");
      return response;
    } catch (error) {
      Logger.error(`Error getting assets from Brandfolder: ${error}`);
      return false;
    }
  }

  async getAttachmentsForAsset(assetID) {
    const url = `/assets/${assetID}/attachments?fields=metadata,asset&include=collections,section`;
    try {
      const response = await this.BrandfolderAPICall(url, "GET");
      return response;
    } catch (error) {
      Logger.error(`Error getting attachments from Brandfolder: ${error}`);
      return false;
    }
  }

  async getAttachmentsFromBrandfolderWithPage(
    page = "1",
    brandfolder = "q7cphz-8wbqqw-1dru9z"
  ) {
    const url = `/brandfolders/${brandfolder}/attachments?page=${page}&fields=metadata,asset&include=collections,section&per=100`;
    try {
      const response = await this.BrandfolderAPICall(url, "GET");
      return response;
    } catch (error) {
      Logger.error(`Error getting attachments from Brandfolder: ${error}`);
      return false;
    }
  }

  async getLatestAttachmentsFromBrandfolderWithPage(
    page = 1,
    daysBack = 30,
    brandfolder = "q7cphz-8wbqqw-1dru9z"
  ) {
    const url = `/brandfolders/${brandfolder}/attachments?page=${page}&fields=metadata,asset&include=collections,section&per=200&search=updated_at%3A%3Enow-${daysBack}d`;
    Logger.info(`Querying URL ${url}`);
    try {
      const response = await this.BrandfolderAPICall(url, "GET");
      return response;
    } catch (error) {
      Logger.error(`Error getting attachments from Brandfolder: ${error}`);
      return false;
    }
  }

  async getAttachmentWithId(attachmentId) {
    const url = `/attachments/${attachmentId}`;
    try {
      const response = await this.BrandfolderAPICall(url, "GET");
      return response;
    } catch (error) {
      Logger.error(
        `Error getting attachment ${attachmentId} from Brandfolder: ${error}`
      );
      return false;
    }
  }
  async getAssetWithId(assetId) {
    const url = `/assets/${assetId}`;
    try {
      const response = await this.BrandfolderAPICall(url, "GET");
      return response;
    } catch (error) {
      Logger.error(`Error getting asset ${assetId} from Brandfolder: ${error}`);
      return error;
    }
  }

  async getAssetsFromSectionWithPage(
    page = 1,
    per = 1,
    sectionId = "gxssj9rpsnn5gcbm39j7kn"
  ) {
    const url = `/sections/${sectionId}/assets?include=attachments,custom_fields&page=${page}&per=${per}`;
    try {
      const response = await this.BrandfolderAPICall(url, "GET");
      return response;
    } catch (error) {
      Logger.error(
        `Error getting assets to be processed from Brandfolder: ${error}`
      );
      return error;
    }
  }

  async getAttachmentsFromOrganisationWithPage(page = "1") {
    const url = `/organizations/q7cphz-8wbqqw-2bbekx/attachments?page=${page}&fields=metadata,asset&include=collections,section&per=100`;
    try {
      const response = await this.BrandfolderAPICall(url, "GET");
      return response;
    } catch (error) {
      Logger.error(`Error getting attachments from Brandfolder: ${error}`);
      return false;
    }
  }

  async createNewBrandfolderAssetInCollection(
    collectionID,
    payload,
    oldAttachment
  ) {
    const url = `/collections/${collectionID}/assets?include=attachments`;
    try {
      const response = await this.BrandfolderAPICall(url, "POST", payload);
      return { old: oldAttachment, new: response };
    } catch (error) {
      Logger.error(`Error creating new asset in Brandfolder: ${error}`);
      return false;
    }
  }
  async createNewBrandfolderAssetInBrandfolder(
    brandfolderID = "q7cphz-8wbqqw-1dru9z", // STS brandfolder
    payload,
    oldAttachment
  ) {
    const url = `/brandfolders/${brandfolderID}/assets?include=attachments`;
    try {
      const response = await this.BrandfolderAPICall(url, "POST", payload);
      return { old: oldAttachment, new: response };
    } catch (error) {
      Logger.error(`Error creating new asset in Brandfolder: ${error}`);
      return false;
    }
  }
  async getBrandfolderAssetInfo(assetID) {
    const url = `/assets/${assetID}?include=attachments,custom_fields`;
    try {
      const response = await this.BrandfolderAPICall(url, "GET");
      return response;
    } catch (error) {
      Logger.error(`Error getting Brandfolder assetID ${assetID}`);
      return false;
    }
  }

  async updateBrandfolderAsset(assetID, payload) {
    const url = `/assets/${assetID}`;
    try {
      const response = await this.BrandfolderAPICall(url, "PUT", payload);
      return response;
    } catch (error) {
      Logger.error(`Error with updating Brandfolder assetID ${assetID}`);
      return false;
    }
  }

  async updateBrandfolderAssetDetails(assetID, assetDetails) {
    const payload = {
      data: {
        attributes: assetDetails,
      },
    };
    const url = `/assets/${assetID}`;
    try {
      const response = await this.BrandfolderAPICall(url, "PUT", payload);
      return response;
    } catch (error) {
      Logger.error(
        `Error with updating details for Brandfolder assetID ${assetID}`
      );
      return false;
    }
  }

  async associateAssetsWithTag(assetIdArray = [], tagName) {
    if (!tagName) return false;
    const payload = {
      data: {
        asset_keys: [...assetIdArray],
        bulk_tag: {
          existing_tag_names: "",
          submitted_tag_names: `${tagName}`,
        },
      },
    };

    const tagPayloadSchema = {
      $schema: "http://json-schema.org/draft-07/schema#",
      title: "data",
      type: "object",
      required: ["data"],
      properties: {
        data: {
          type: "object",
          required: ["asset_keys", "bulk_tag"],
          additionalProperties: false,
          properties: {
            asset_keys: {
              type: "array",
              items: {
                type: "string",
                minLength: 10,
              },
            },
            bulk_tag: {
              required: ["existing_tag_names", "submitted_tag_names"],
              type: "object",
              properties: {
                existing_tag_names: {
                  type: "string",
                  maxLength: 0,
                },
                submitted_tag_names: {
                  type: "string",
                  minLength: 1,
                },
              },
            },
          },
        },
      },
    };

    const validate = this.JSONValidator.compile(tagPayloadSchema);
    const valid = validate(payload);
    if (!valid) {
      Logger.error(validate.errors);
      return false;
    }
    const url = `/bulk_actions/assets/tag?queue_priority=high`;
    try {
      const response = await this.BrandfolderAPICall(url, "PUT", payload);
      return response;
    } catch (error) {
      Logger.error(`Error associating assets with tag ${tagName}`);
      return false;
    }
  }

  async addTagsToOneAsset(assetID, tagsArray) {
    const url = `/assets/${assetID}/tags`;
    const payload = {
      data: {
        attributes: tagsArray.map((name) => {
          return { name };
        }),
      },
    };
    try {
      const response = await this.BrandfolderAPICall(url, "POST", payload);
      return response;
    } catch (error) {
      Logger.error(
        `Error adding tags to Brandfolder assetID ${assetID}: ${error}`
      );
      return false;
    }
  }

  /**
   * this is unusable if custom fields have multi-select
   * @param {*} brandfolderAssetID
   * @param {*} customFieldsArray
   * @returns response
   */
  async updateCustomFields(brandfolderAssetID, customFieldsArray) {
    const url = `/assets/${brandfolderAssetID}/custom_fields`;
    const payload = {
      data: {
        attributes: customFieldsArray,
      },
    };
    try {
      const response = await this.BrandfolderAPICall(url, "POST", payload);
      return response;
    } catch (error) {
      Logger.error(`Problem with BF custom fields API call: ${error}`);
    }
  }

  /**
    * If any custom fields have more than one field selectable,
    * all custom field values must be updated by custom field ID association (as per below) rather than by assignment
   * {
	"data": [
      {
        "attributes": {
          "value": "11111111113"
        },
        "relationships": {
          "asset": {
            "data": { "type": "assets", "id": "6k76wn3qkv47nn8f88h6p5pz" }
          }
        }
      }
    ]
  }
  */

  // https://brandfolder.com/api/v4/custom_field_keys/qdlecj-4sgrfk-b5oe3z/custom_field_values/

  async associateAssetsWithCustomField(customFieldKeyID, assetsValuesArray) {
    const url = `/custom_field_keys/${customFieldKeyID}/custom_field_values`;
    const payload = {
      data: assetsValuesArray,
    };
    try {
      const response = await this.BrandfolderAPICall(url, "POST", payload);
      return response;
    } catch (error) {
      Logger.error(error);
    }
  }

  async deleteAttachment(attachmentID) {
    const url = `/attachments/${attachmentID}`;
    try {
      const response = await this.BrandfolderAPICall(url, "DELETE");
      return response;
    } catch (error) {
      Logger.error(error);
    }
  }

  async associateAssetsWithCollections(
    asset_keys,
    collection_keys,
    brandfolder_key = "q7cphz-8wbqqw-1dru9z"
  ) {
    const payload = {
      data: {
        asset_keys,
        brandfolder_key,
        collection_keys,
      },
    };
    const url =
      "/bulk_actions/assets/collection_associations?queue_priority=high";
    try {
      const response = await this.BrandfolderAPICall(url, "POST", payload);
      return response;
    } catch (error) {
      Logger.error(error);
      return error;
    }
  }

  async associateAssetsWithTags(asset_keys, tag) {
    // https://brandfolder.com/api/v4/bulk_actions/assets/tag?queue_priority=high
    // example payload:
    // {
    //     "asset_keys": [
    //         "gv66g4ncpjmmghq6h3nnwkch",
    //         "65gv7cv6m8xnr5n4kjqvgxx"
    //     ],
    //     "bulk_tag": {
    //         "existing_tag_names": "",
    //         "submitted_tag_names": "test"
    //     }
    // }

    const payload = {
      data: {
        asset_keys: [].concat(asset_keys),
        bulk_tag: {
          existing_tag_names: "",
          submitted_tag_names: tag,
        },
      },
    };
    const url = "/bulk_actions/assets/tag?queue_priority=high";
    try {
      const response = await this.BrandfolderAPICall(url, "POST", payload);
      Logger.info(`Associated ${asset_keys.length} assets with tag ${tag}`);
      return response;
    } catch (error) {
      Logger.error(error);
      return error;
    }
  }

  async associateAssetsWithLabels(asset_keys, label_key) {
    const payload = {
      data: {
        asset_keys,
        label_key,
      },
      queue_priority: "high",
    };
    const url = "/bulk_actions/assets/add_to_label";
    try {
      const response = await this.BrandfolderAPICall(url, "POST", payload);
      return response;
    } catch (error) {
      Logger.error(error);
      return error;
    }
  }

  async bulkDestroyAssets(arrayOfAssetIds) {
    const url = `/bulk_actions/assets/destroy?queue_priority=high`;
    const payload = { data: { asset_keys: arrayOfAssetIds } };
    try {
      const response = await this.BrandfolderAPICall(url, "DELETE", payload);
      return response;
    } catch (error) {
      Logger.error(error);
      throw error;
    }
  }

  async BrandfolderAPICall(url, method = "GET", body = null) {
    const settings = {
      method,
      headers: this.headers,
      timeout: 0,
      body: body ? JSON.stringify(body) : null,
    };
    const response = await fetch(`${this.apiRootURL}${url}`, settings);

    switch (response.status) {
      case 204:
        Logger.info(`Updated successfully.`);
        return true;
      case 400:
        Logger.error(`Error with request.`);
        return false;
      //throw new Error(`Bad Request. Check what you're sending.`);
      case 401:
        throw new Error(`Unauthorized. Check your API credentials.`);
      case 403:
        throw new Error(`Forbidden. Check your API credentials.`);
      case 404:
        if (method == "put") {
          throw new NotFoundError(`Not found. Check your PUT request.`);
        } else {
          throw new NotFoundError(`Not found.`);
        }
      case 429:
        throw new APILimitsExceededError(
          `API limits exceeded. Adjust your throttle.`
        );
      case 408:
        throw new Error(`Timeout. Issue with Brandfolder API`);
      case 500:
        throw new Error(`500 error with API`);
      default:
        // this was a normal 200
        break;
    }
    const responseData = await response.json();
    return responseData;
  }
}

module.exports.BrandfolderAPI = BrandfolderAPI;
