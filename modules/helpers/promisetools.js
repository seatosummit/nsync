class PromiseTools {
  static async wait(ms) {
    return new Promise((resolve) => {
      setTimeout(resolve, ms);
    });
  }
}

module.exports.PromiseTools = PromiseTools;
