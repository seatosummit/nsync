const _ = require("lodash");
const fs = require("fs");
const path = require("path");
const fetch = require("node-fetch");
require("dotenv").config();
const {
  valueIsNullOrUndefined,
  valueExistsAndIsNotEmpty,
  skuIsZCoded,
} = require("./stringTools");
const { Logger } = require("./Logger");
const { sqlDateWithinMonths } = require("./dateTools");
const { SalesChannelIntegrationRuleset } = require("../../models");
const product = require("../salsify/products/product");

class IntegrationRuleset {
  constructor(args) {
    try {
      this.ruleset = args?.ruleset ? args.ruleset.toJSON() : {};
    } catch (error) {
      this.ruleset = args?.ruleset ?? {};
    }
    this.SalesChannelId = args?.SalesChannelId ?? this.ruleset.SalesChannelId;
    this.productEnabledRules = this.ruleset?.productEnabledRules ?? {};
    this.specialRules = this.productEnabledRules?.specialRules ?? [];
    this.statuses = this.productEnabledRules?.statuses ?? [];
    this.flags = this.productEnabledRules?.flags ?? [];
    for (const key in this.ruleset) {
      this[`${key}`] = this.ruleset[key];
    }
  }

  async getRulesForStore() {
    try {
      if (!this.SalesChannelId)
        throw `Need a sales channel ID to fetch ruleset`;
      const ruleset = await SalesChannelIntegrationRuleset.findAll({
        where: {
          SalesChannelId: this.SalesChannelId,
        },
      });

      if (!ruleset)
        throw `Ruleset not found for sales channel ID ${this.SalesChannelId}`;

      this.rules = ruleset;
      for (const key in this.ruleset) {
        this[`${key}`] = this.ruleset[key];
      }
      this.productEnabledRules = this.rules.productEnabledRules ?? {};
      this.specialRules = productEnabledRules.specialRules ?? [];
      this.statuses = productEnabledRules.statuses ?? [];
      this.flags = productEnabledRules.flags ?? [];
    } catch (error) {
      throw error;
    }
  }

  // TODO: fix promise date
  productPassesEnabledRules(args) {
    const productInstance = args?.productInstance;
    if (!productInstance) throw `No product instance provided`;

    for (const flag of this.flags) {
      // flags like b2c_enabled is false
      const missingFlag =
        productInstance[flag.name] !== "1" &&
        productInstance[flag.name] !== true;
      if (missingFlag)
        return {
          passed: false,
          disable_reason: `Missing required ${flag.name} flag`,
        };
    }

    // Z-coded
    if (skuIsZCoded(productInstance.sku))
      return { passed: false, disable_reason: `Z-coded SKU` };

    // Price is zero
    if (productInstance.retailPrice === 0)
      return { passed: false, disable_reason: `Price is zero` };

    const matchingStatus = _.find(this.statuses, {
      status: productInstance.status,
    });

    // Not one of the required statuses
    if (!matchingStatus)
      return {
        passed: false,
        disable_reason: `Status not one of the required ones`,
      };

    if (matchingStatus.stockRequired) {
      // There isn't any stock although that status requires there is
      if (productInstance.stockQuantity === 0) {
        if (matchingStatus.promiseDateRequired) {
          // There isn't any stock and there is no future incoming stock
          if (!productInstance.stockETA && !productInstance.promiseDate)
            return {
              passed: false,
              disable_reason: `EOL, zero stock, no promise date`,
            };
        } else {
          return {
            passed: false,
            disable_reason: `EOL, zero stock`,
          };
        }
      }
    }

    for (const specialRule of this.specialRules) {
      switch (specialRule.name) {
        case "moreStockWithinMonths":
          // There's an expectation of having stock soon
          // but there's no stock ETA set
          if (!productInstance.stockETA && !productInstance.promiseDate)
            return {
              passed: false,
              disable_reason: "No Promise Date",
            };
          if (productInstance.stockQuantity === 0) {
            const withinTimePeriod = sqlDateWithinMonths({
              dateString: productInstance.stockETA,
              months: specialRule.months,
            });
            // not within required stock ETA period
            if (!withinTimePeriod)
              return {
                passed: false,
                disable_reason: `Promise Date not within expected ${specialRule.months} months`,
              };
          }
          break;
      }
    }

    // no rules failed, this product passes
    return {
      passed: true,
    };
  }
}
module.exports.IntegrationRuleset = IntegrationRuleset;
