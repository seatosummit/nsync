const SftpClient = require("ssh2-sftp-client");

class FTPTools {
  constructor(ftpConfig = []) {
    this.client = new SftpClient();
    this.config = {
      host: ftpConfig.host,
      port: ftpConfig.port,
      username: ftpConfig.user,
      password: ftpConfig.password,
      debug: null,
    };
  }
  async connect() {
    try {
      return await this.client.connect(this.config);
    } catch (err) {
      console.error(`FTP connection problems: ${err}`);
    }
  }

  async uploadOne(fileInfo) {
    try {
      if (!fileInfo.sourcePath) throw `No local URL provided`;
      if (!fileInfo.remotePath) throw `No remote path provided`;
      await this.client.fastPut(fileInfo.sourcePath, fileInfo.remotePath);
    } catch (error) {
      throw `Error uploading file ${fileInfo.source}`;
    }
  }

  async batchUpload(batchData, needToConnect = true) {
    try {
      if (needToConnect) await this.connect();
      for (const batchItem of batchData) {
        console.info(
          `Uploading ${batchItem.source} to ${batchItem.remotePath}`
        );
        await this.client.fastPut(batchItem.source, batchItem.remotePath);
      }
      return true;
    } catch (err) {
      console.error(`FTP connection problems: ${err}`);
      return false;
    }
  }
  async getFiles(remotePath, localPath) {
    try {
      await this.connect();
      console.info(`Downloading directory... ${remotePath}`);
      return await this.client.downloadDir(remotePath, localPath);
    } catch (err) {
      console.error(`FTP connection problems: ${err}`);
    }
  }
  async remove(path) {
    try {
      await this.connect();
      console.info(`Removing file... ${path}`);
      return await this.client.delete(path);
    } catch (err) {
      console.error(`FTP connection problems: ${err}`);
    }
  }
}

module.exports.FTPTools = FTPTools;
