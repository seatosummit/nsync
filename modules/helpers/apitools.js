const fetch = require("node-fetch");
require("dotenv").config();
require("console-stamp")(console, "[yyyy-mm-dd HH:MM:ss]");

module.exports = {
  brandfolderHeaders: () => {
    return {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${process.env.BRANDFOLDER_API_KEY}`,
    };
  },
  prepareHeaders: async () => {
    const WMSHeaders = {
      "Authorization": Buffer.from(
        `${process.env.WMS_API_USER}:${process.env.WMS_API_PASSWORD}`
      ).toString("base64"),
      "Content-Type": "application/json",
      "Content-length": 0,
      "Auth_Token": await module.exports.WMSToken(),
    };
    const PepperiHeaders = {
      "Authorization":
        "Basic " +
        Buffer.from(
          `${process.env.PEPPERI_API_USERNAME}:${process.env.PEPPERI_API_PW}`
        ).toString("base64"),
      "X-Pepperi-ConsumerKey": process.env.PEPPERI_API_CONSUMERKEY,
      "Content-Type": "application/json",
    };
    const SalsifyHeaders = {
      "Content-length": 0,
      "Content-Type": "application/json",
    };
    const BrandfolderHeaders = {
      "Content-Type": "application/json",
      "Authorization": `Bearer ${process.env.BRANDFOLDER_API_KEY}`,
    };
    return { WMSHeaders, PepperiHeaders, SalsifyHeaders, BrandfolderHeaders };
  },
  WMSToken: async () => {
    const headers = {
      "Authorization": Buffer.from(
        `${process.env.WMS_API_USER}:${process.env.WMS_API_PASSWORD}`
      ).toString("base64"),
      "Content-Type": "application/json",
      "Content-length": 0,
    };
    try {
      const response = await fetch(
        `http://${process.env.WMS_API_LOCATION}/json/token`,
        { headers: headers, timeout: 0 }
      );
      const responseJSON = await response.json();
      return responseJSON[0].token;
    } catch (error) {
      throw "Error getting token from WMS";
    }
  },
  GoogleDriveAuth: async () => {
    const { google } = require("googleapis");
    const credentials = require("../../config/google-drive/credentials.json");
    const scopes = ["https://www.googleapis.com/auth/drive"];

    try {
      const auth = await new google.auth.JWT(
        credentials.client_email,
        null,
        credentials.private_key,
        scopes
      );

      return google.drive({ version: "v3", auth });
    } catch (error) {
      throw "Error authenticating";
    }
  },
  ShopifyAPI: (storeConfig) => {
    const Shopify = require("shopify-api-node");
    const shopify = new Shopify({
      shopName: storeConfig.shopName,
      apiKey: storeConfig.apiKey,
      password: storeConfig.password,
    });
    return shopify;
  },
};
