class SparkLayerPricingUpdateError extends Error {
  constructor(payload) {
    const amendedMessage = `Problems updating SparkLayer product pricing: ${payload}`;
    super(amendedMessage);
    this.name = "SparkLayerPricingUpdateError";
  }
}

class SparkLayerAPIError extends Error {
  constructor(payload) {
    const amendedMessage = `Problems with SparkLayer API call: ${payload}`;
    super(amendedMessage);
    this.name = "SparkLayerAPIError";
  }
}

module.exports.SparkLayerPricingUpdateError = SparkLayerPricingUpdateError;
module.exports.SparkLayerAPIError = SparkLayerAPIError;
