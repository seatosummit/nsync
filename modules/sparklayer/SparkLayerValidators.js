const JSONSchemaValidator = require("ajv");
class SparkLayerValidators {
  static validPricingUpdatePayload(args) {
    const itemToValidate = args?.itemToValidate;

    const JSONValidator = new JSONSchemaValidator({ allowUnionTypes: true });
    const sparkLayerPricingUpdateSchema = {
      type: "array",
      items: {
        type: "object",
        required: ["price_list_slug", "pricing"],
        additionalProperties: false,
        properties: {
          price_list_slug: {
            type: "string",
            minLength: 3,
          },
          pricing: {
            type: "array",
            minItems: 0,
            items: {
              type: "object",
              required: ["quantity", "price", "tax_type"],
              minProperties: 3,
              maxProperties: 3,
              properties: {
                quantity: {
                  type: "number",
                },
                price: {
                  type: "number",
                },
                tax_type: {
                  type: "string",
                  minLength: 2,
                },
              },
            },
          },
        },
      },
    };

    const validate = JSONValidator.compile(sparkLayerPricingUpdateSchema);
    let validationResult = validate(itemToValidate);

    if (!validationResult) {
      const errorsArray = validate.errors.map((error) => {
        return `${error.instancePath} ${error.message}`;
      });
      return { pass: validationResult, errors: errorsArray.join(", ") };
    }

    return { pass: validationResult, errors: null };
  }
}

module.exports.SparkLayerValidators = SparkLayerValidators;
