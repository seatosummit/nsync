const { Logger } = require("../helpers/Logger");
const { checkForRule } = require("../helpers/IntegrationRuleTools");
const SparkLayerUpdater = require("./SparkLayerUpdater");
const { ShopifyConfig } = require("../helpers/ShopifyConfig");

const updatePricingToSparkLayer = async ({ machineNameOfSalesChannel }) => {
  try {
    const ShopifyStores = await ShopifyConfig.getConfigsFromDatabase({
      machineNameOfSalesChannel,
    });
    for (const ShopifyStore of ShopifyStores) {
      let storeUpdater = new SparkLayerUpdater({
        salesChannel: ShopifyStore,
      });
      Logger.info(`Updating SparkLayer pricing...`);
      const shopifyPricingUpdates =
        await storeUpdater.updatePricingToSparkLayer();
      Logger.info(
        `Done with ${ShopifyStore.shopify_config.name} pricing updates.`
      );
    }
    process.exit();
  } catch (error) {
    Logger.error(`Error updating Shopify pricing: ${error}`);
  }
};

module.exports.updatePricingToSparkLayer = updatePricingToSparkLayer;
