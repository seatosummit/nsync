const { Sequelize } = require("sequelize");
const Op = Sequelize.Op;
const _ = require("lodash");
const { DateTime } = require("luxon");
const { Logger } = require("../helpers/Logger");
const { SparkLayerAPI } = require("./SparkLayerAPI");
const {
  PricingUpdate,
  SalesChannel,
  PricingEnhancement,
  ProductVariantEnhancement,
} = require("../../models");
const { SparkLayerValidators } = require("./SparkLayerValidators");
const { daysSince } = require("../helpers/dateTools");
const { valueExistsAndIsNotEmpty } = require("../helpers/stringTools");

class SparkLayerUpdater {
  constructor(args) {
    this.salesChannel = args?.salesChannel;
    if (!this.salesChannel) throw `No Sales Channel provided`;
    if (this.salesChannel instanceof SalesChannel === false)
      throw `Not a proper Sales Channel instance`;
    this.API = new SparkLayerAPI();
    this.pricingUpdatePromises = [];
  }

  async updatePricingToSparkLayer() {
    await this.API.authenticate();
    const productVariantEnhancements = await ProductVariantEnhancement.findAll({
      where: {
        SalesChannelId: this.salesChannel.id,
      },
      attributes: ["sku", "shopifyVariantId"],
    });
    const pricingUpdates = (
      await PricingUpdate.findAll({
        where: {
          SalesChannelId: this.salesChannel.id,
        },
      })
    ).map((update) => {
      const variantEnhancement = _.find(productVariantEnhancements, {
        sku: update.sku,
      });
      return {
        payload: update.payload,
        sku: update.sku,
        shopifyVariantId: variantEnhancement
          ? variantEnhancement.shopifyVariantId
          : null,
      };
    });

    for (const update of pricingUpdates) {
      this.pricingUpdatePromises = [];
      try {
        const validationResult = SparkLayerValidators.validPricingUpdatePayload(
          {
            itemToValidate: update.payload,
          }
        );
        if (!validationResult.pass)
          throw `Update payload for SKU ${update.sku} not valid: ${validationResult.errors}`;

        const regex = /[\/\*\\\&\#]/gm;
        let sku = update.sku;
        if (update.sku.match(regex)) {
          Logger.error(
            `SKU ${update.sku} has non-URL friendly characters like &#/ so looking for variant ID instead...`
          );
          if (!update.shopifyVariantId)
            throw `SKU ${update.sku} has a non-URL friendly SKU and doesn't have a Shopify variant ID`;
          Logger.info(
            `Using Shopify Variant ID ${update.shopifyVariantId} instead of SKU...`
          );
          sku = update.shopifyVariantId;
        }

        const updatePromise = this.API.updateProductPricing({
          sku,
          payload: update.payload,
        });
        this.pricingUpdatePromises.push(updatePromise);
      } catch (error) {
        Logger.error(
          `Problem with adding update request for SKU ${update.sku}: ${error}`
        );
      }
    }
    try {
      const results = await Promise.all(this.pricingUpdatePromises);
      const erroredOutArray = results.filter((result) => {
        return result.type === "API500Error";
      });
      if (erroredOutArray.length > 0) {
        Logger.error(`Problems with this batch of SKUs:`);
        for (const error of erroredOutArray) {
          Logger.error(error.sku);
        }
      }
    } catch (error) {
      Logger.error(`Problems updating pricing: ${error}`);
    }
  }
}

module.exports = SparkLayerUpdater;
