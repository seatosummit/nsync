require("dotenv").config();
const fetch = require("node-fetch");
const throttle = require("fetch-throttle");
const fetchThrottle = throttle(fetch, 5, 1000);
const {
  NotFoundError,
  APILimitsExceededError,
  API500Error,
} = require("../helpers/CustomErrors");
const { Logger } = require("../helpers/Logger");
const {
  SparkLayerPricingUpdateError,
  SparkLayerAPIError,
} = require("./SparkLayerErrors");

class SparkLayerAPI {
  constructor() {
    this.apiRootURL = process.env.SPARKLAYER_API_URL;
    this.headers;
  }

  async authenticate() {
    try {
      this.headers = await this.SparkLayerHeaders();
    } catch (error) {
      Logger.error(`Problem with SparkLayer API token fetch: ${error}`);
    }
  }

  async updateProductPricing(args) {
    const sku = args?.sku;
    const payload = args?.payload;
    if (!sku) {
      throw new SparkLayerPricingUpdateError(`No SKU provided`);
    }
    if (!payload)
      throw new SparkLayerPricingUpdateError(`No update payload provided`);
    try {
      const result = await this.SparkLayerAPICall({
        url: `/v1/variants/${sku}/pricing`,
        method: "PATCH",
        body: payload,
      });
      return {
        error: false,
        sku: sku,
        type: "PricingUpdate",
        message: `SKU ${sku} updated successfully`,
      };
    } catch (error) {
      return {
        error: true,
        sku: sku,
        type: error.name,
        message: `Error: ${error}`,
      };
    }
  }

  async SparkLayerHeaders() {
    const headers = {
      "site-id": "seatosummitdistribution",
      "Content-Type": "application/json",
      "Content-length": 0,
    };
    const payloadForAuth = {
      grant_type: "client_credentials",
      client_id: `${process.env.SPARKLAYER_CLIENT_ID}`,
      client_secret: `${process.env.SPARKLAYER_CLIENT_SECRET}`,
    };
    try {
      const response = await fetch(`${this.apiRootURL}/auth/token`, {
        method: "POST",
        headers: headers,
        body: JSON.stringify(payloadForAuth),
      });
      const responseJSON = await response.json();

      return {
        "Authorization": "Bearer " + responseJSON.access_token,
        "Site-Id": "seatosummitdistribution",
        "Content-Type": "application/json",
        "Content-length": 0,
      };
    } catch (error) {
      throw "Error getting token from SparkLayer";
    }
  }

  async SparkLayerAPICall(args) {
    const url = args?.url;
    const method = args?.method ?? "GET";
    const body = args?.body;
    if (!url) throw new SparkLayerAPIError(`No URL provided`);
    try {
      const headers = this.headers ?? (await this.authenticate());
      const bodyString = body ? JSON.stringify(body) : null;
      if (!headers) throw `Issue with SparkLayer headers`;
      const settings = {
        method,
        headers,
        body: bodyString,
        timeout: 0,
      };
      const completeURL = `${this.apiRootURL}${url}`;
      const response = await fetchThrottle(completeURL, settings);

      switch (response.status) {
        case 204:
          Logger.info(`Updated successfully.`);
          return true;
        case 200:
          if (method === "PUT" || method === "PATCH") {
            Logger.info(`PUT/PATCH Updated successfully.`);
          }
          return true;
        case 400:
          throw new Error(`Error with request.`);
        case 401:
          throw new Error(`Unauthorized. Check your API credentials.`);
        case 403:
          throw new Error(`Forbidden. Check your API credentials.`);
        case 404:
          if (method === "PUT" || method === "PATCH") {
            throw new NotFoundError(`Not found. Check your request.`);
          } else {
            throw new NotFoundError(`Not found.`);
          }
        case 429:
          throw new APILimitsExceededError(
            `API limits exceeded. Adjust your throttle.`
          );
        case 408:
          throw new Error(`Timeout. Issue with SparkLayer API`);
        case 500:
          throw new API500Error(`500 error with API`);
        default:
          // this was a normal 200
          break;
      }
      const responseData = await response.json();
      return responseData;
    } catch (error) {
      throw error;
    }
  }
}

module.exports.SparkLayerAPI = SparkLayerAPI;
