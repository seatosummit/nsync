# NSync

## What's this?

This Node.js app is a set of microservices related data synchronisation tasks between our internal and external systems.
These are mostly scheduled tasks.

## What it's touching at the moment

- Salsify
- Brandfolder
- Shopify
- Pepperi
- WMS
- Google Folder

## Before you can develop

- You need Node.js 14.x.
- Remember to run `npm install`.
- do `docker-compose up` to get a database. After that, do `npx sequelize db:migrate` to migrate the DB.
- Create a .env file, based on the example.

## Get a clone of production DB for your development

**WARNING!** **_The below action will remove everything you have on your localhost._**

There's a shell script `db-import-to-dev.sh` that you can run from the project root. You'll need to have installed the [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli) and authenticated with it first.

## Clone production DB to staging DB for testing

There's a shell script `db-clone-prod-to-staging.sh` that you can run from the project root. You'll need to have installed the [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli) and authenticated with it first.
