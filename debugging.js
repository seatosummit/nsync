// const { traverseFoldersAndFormatImages } = require('./modules/helpers/imageResizer');
const { syncCustomers } = require("./modules/pepperi/customers/customer");
const {
  syncProductsFromWMS,
  syncProductsFromWMSUsingBuffer,
  batchDeleteProductsFromPepperi,
  purgePepperiProductsUsingWMSBuffer,
} = require("./modules/pepperi/products/product");
const {
  updateProductStockLevels,
} = require("./modules/pepperi/products/productstock");
const { syncCategories } = require("./modules/pepperi/products/category");
const {
  updateCustomerBuffer,
} = require("./modules/pepperi/customers/customerbuffer");
const {
  updateProductBuffer,
} = require("./modules/pepperi/products/productbuffer");

const {
  syncProductsFromSalsify,
} = require("./modules/pepperi/products/product");
// const { syncProductsFromWooCommerceToBuffer } = require('./modules/woocommerce/products/productbuffer');
const {
  updateSalesChannelProductEnables,
} = require("./modules/wms/products/productenable");
const {
  updateWMSProductBuffer,
  updateWMSProductBufferForLoop,
  updateWMSProductBufferUsingChunks,
} = require("./modules/wms/products/productbuffer");
const {
  createWMSProductListCSV,
  createSalsifyOrphansCSV,
  reportOfDiscontinuedProducts,
} = require("./modules/wms/products/productcsv");
//const { updateCustomerBuffer } = require('./modules/wms/customers/customerbuffer');
const { exportOrdersToXML } = require("./modules/pepperi/orders/order");
const { updateOrderBufferFromWMS } = require("./modules/pepperi/orders/order");
const {
  getProductsWithNoBrand,
  convertSimpleProductsToParents,
  produceSimpleProductParentsCSV,
} = require("./modules/salsify/products/product");
const {
  getAllValuesForProperty,
} = require("./modules/salsify/properties/properties");
const {
  syncSalsifyProductParentsToDB,
  syncGlobalSalsifyBuffer,
  syncAustraliaSalsifyBuffer,
} = require("./modules/salsify/products/productbuffer");
const {
  syncSalsifyProductsToDB,
} = require("./modules/salsify/products/productbuffer");
const {
  createSalsifySimpleProductsCSV,
} = require("./modules/salsify/products/productcsv");
const {
  getSalsifyProductAttributes,
  extractColour,
  extractColourLayman,
  extractSize,
  addSingleColourFromParentToProducts,
} = require("./modules/salsify/products/productbuffer");
const {
  getProductImagesFromSalsifyWithPage,
  getProductImagesFromSalsifyWithCursor,
} = require("./modules/salsify/products/productimages");
const {
  updateDigitalAssetsBufferFromSalsify,
} = require("./modules/salsify/digitalassets/digitalassetbuffer");
const {
  syncRelevantSalsifyImages,
} = require("./modules/pepperi/products/productimages");
const {
  BDBufferImagesSync,
  BDShopifyImagesSync,
  BDProductBufferSync,
  BDShopifyProductSync,
  BDBufferCategoryDefinitions,
  BDBufferCollectionSync,
  BDShopifyCollectionSync,
  BDShopifyMetafieldSync,
  // BDShopifyProductSync,
  BDCleanUpProducts,
  BDInventorySync,
  BDShopifyPriceBufferSync,
  BDVariantBufferSync,
  BDShopifyPriceStoreSync,
  BDProductCacheSync,
} = require("./modules/blackdiamond/schedules/BDProductSchedules");
const {
  salsifyImageTransfer,
} = require("./modules/brandfolder/schedules/SalsifyImageTransfer");
const {
  exportShopifyOrders,
  exportOneShopifyOrder,
  tagOrdersWithNoStock,
} = require("./modules/shopify/orders/ShopifyOrderSchedule");
const {
  updateAttachmentsBuffer,
  updateSingleAttachmentInBuffer,
  updateAttachmentsBufferWithSections,
} = require("./modules/brandfolder/attachments/attachments");
const { updateLabelsBuffer } = require("./modules/brandfolder/labels/labels");
const { updateAssetsBuffer } = require("./modules/brandfolder/assets/assets");
// const { FirebaseTest } = require("./modules/firebase/FirebaseTest");
const {
  BrandfolderTransition,
  BrandfolderUpdateAttachmentDetails,
  BrandfolderBufferPrune,
  convertNewlyAddedAssets,
  cleanUpSTSDuplicates,
  addLabelsToAssets,
} = require("./modules/brandfolder/transition");
const {
  BrandfolderOutput,
} = require("./modules/brandfolder/output/BrandfolderOutput");
const {
  BrandfolderAttachmentsToDigitalAssets,
  BrandfolderAttachmentsToSalsifyProductImages,
  BrandfolderAssetsToDigitalAssets,
  BrandfolderAssetsSalsifyProductAssociations,
  BrandfolderSkuAndUrlList,
} = require("./modules/brandfolder/output");
const {
  ShopifyProductOperations,
} = require("./modules/shopify/products/ShopifyProductOperations");
const {
  GenericImport,
  CompareBufferToWMSView,
  ShopifyProductImportCommand,
} = require("./modules/central-buffer/ImportRunners");

const {
  CleanBufferProductVariants,
  CleanBufferProducts,
  CreateProductVariantRelationships,
} = require("./modules/central-buffer/BufferOperations");
const { BufferRefresher } = require("./modules/central-buffer/BufferRefresher");
const {
  refreshAllVariants,
  updateProductStatusBuffer,
  addNewProductsToBuffer,
  updatePricingEnhancementsBuffer,
  refreshAllCollections,
} = require("./modules/central-buffer/CentralBufferSchedule");
const {
  updatePricingAndInventoryToShopify,
  bulkUpdateInventoryToShopify,
  updatePricingToShopify,
  bulkUpdatePricingToShopify,
  bulkUpdateVariantsToShopify,
  bulkCreateNewProducts,
  reattemptBulkImportSync,
  bulkUpdatePrimaryImagesToShopify,
  updateCollectionsToShopify,
  getImagesFromSalesChannel,
  bulkUpdateExistingProducts,
  bulkUpdateExistingProductBufferIds,
} = require("./modules/shopify/store/ShopifyStoreSchedule");
const {
  updatePepperiImagesBuffer,
  updateImagesToPepperiFromBuffer,
} = require("./modules/pepperi/images/PepperiImageSchedule");
const { exportMyobOrders } = require("./modules/myob/orders/MyobOrderSchedule");
const {
  importShopifyShipments,
} = require("./modules/shopify/shipments/ShopifyShipmentSchedule");
const { testEmails } = require("./modules/emails/EmailServiceSchedule");
const {
  updatePricingToSparkLayer,
} = require("./modules/sparklayer/SparkLayerSchedule");

const runEverything = async () => {
  // UTILITY
  // await traverseFoldersAndFormatImages();
  /////////////////////
  // PEPPERI
  // await syncProductsFromWMS(false);
  // await syncProductsFromWMSUsingBuffer(false);
  // await batchDeleteProductsFromPepperi();
  // await exportOrdersToXML();
  // await updateProductStockLevels();
  // await syncCustomers();
  // await updateProductBuffer();
  // await purgePepperiProductsUsingWMSBuffer();
  // await updatePepperiImagesBuffer();
  // await updateImagesToPepperiFromBuffer();
  ///////////////////////
  // SALSIFY
  // await produceSimpleProductParentsCSV();
  // await convertSimpleProductsToParents("Black Diamond");
  // await syncSalsifyProductsToDB();
  // await syncSalsifyProductParentsToDB();
  // await getSalsifyProductAttributes(extractSize);
  // await getProductsWithNoBrand();
  // await createSalsifySimpleProductsCSV('Black Diamond', 'BLACKDIAMOND');
  // await getAllValuesForProperty("Black Diamond Shopify Category");
  // await updateDigitalAssetsBufferFromSalsify();
  // await addSingleColourFromParentToProducts();
  // await syncGlobalSalsifyBuffer();
  //
  //
  // USE THIS SYNC FOR Products and ProductVariants buffer sync
  // await syncAustraliaSalsifyBuffer();
  // await CreateProductVariantRelationships();
  ///////////////////////
  // BRANDFOLDER
  // await updateLabelsBuffer("q7cphz-8wbqqw-1dru9z"); // STS
  // await updateLabelsBuffer("jgs64snkkfgvxkvm454vsmrb"); // DIST
  // await updateSingleAttachmentInBuffer("b8ftqqcprxt9m5k97jxc6xv");
  // await updateAttachmentsBufferWithSections();
  // ASSETS HAVE ENOUGH INFO, DON'T NEED TO IMPORT ATTACHMENTS SEPARATELY
  // await updateAssetsBuffer(7, "q7cphz-8wbqqw-1dru9z"); // STS
  // await updateAssetsBuffer(7, "jgs64snkkfgvxkvm454vsmrb"); // DIST
  // CSV IMPORT
  // await BrandfolderSkuAndUrlList(); // CREATES A CSV IMPORT FOR MATRIXIFY
  //
  // await salsifyImageTransfer();
  // await BrandfolderUpdateAttachmentDetails("Black Diamond");
  // await BrandfolderTransition();
  // await convertNewlyAddedAssets();
  // await addLabelsToAssets();
  // await BrandfolderBufferPrune();
  // await BrandfolderAssetsToDigitalAssets();
  // await BrandfolderAssetsSalsifyProductAssociations();
  // await cleanUpSTSDuplicates();
  // await BrandfolderAttachmentsToSalsifyProductImages();
  ///////////////////////
  // FIREBASE
  // await FirebaseTest();
  ///////////////////////
  // WMS
  // await updateWMSProductBuffer({ filterOnlyActiveProducts: false });
  //
  //
  // Chunks is better
  // await updateWMSProductBufferUsingChunks({
  //   filterOnlyActiveProducts: false,
  //   chunkSize: 50,
  // });
  // await updateSalesChannelProductEnables();
  // await updateOrderBufferFromWMS();
  // await updateCustomerBuffer();
  // await createWMSProductListCSV();
  // await reportOfDiscontinuedProducts();
  ///////////////////////
  // BLACK DIAMOND
  // await BDBufferCategoryDefinitions();
  // await BDBufferCollectionSync();
  // await BDShopifyCollectionSync();
  // await BDShopifyPriceBufferSync();
  // await BDProductBufferSync();
  // await BDVariantBufferSync();
  // await BDProductCacheSync();
  // await BDBufferImagesSync();
  // await BDShopifyImagesSync();
  // await BDCleanUpProducts();
  // await BDInventorySync();
  // await BDShopifyPriceStoreSync();
  // await BDShopifyProductSync({newProductsOnly: false});
  // await BDShopifyMetafieldSync();
  ///////////////////////
  // SHOPIFY
  // await syncProductsFromShopifyToBuffer();
  // await syncProductsToShopifyFromWMSUsingBuffer();
  // await exportShopifyOrders({
  //   machineNameFilter: "CAMELBAK",
  //   uploadXML: false,
  // });
  // await exportShopifyOrders();
  // await tagOrdersWithNoStock();
  // await exportOneShopifyOrder({
  //   salesChannelMachineName: "SEA TO SUMMIT",
  //   orderNumber: 961007,
  // });
  // await ShopifyProductOperations.cleanOutRedundantBufferProducts();
  // await CleanBufferProductVariants();
  // await CleanBufferProducts();
  // await ShopifyProductImportCommand({
  //   storeFilterMachineName: "SEA TO SUMMIT",
  // });
  // await ShopifyProductImportCommand({
  //   storeFilterMachineName: "B2BDIST",
  // });
  // await GenericImport();
  // await addNewProductsToBuffer({
  //   machineNameOfSalesChannel: "B2BDIST",
  // });
  // await updatePricingToSparkLayer({
  //   machineNameOfSalesChannel: "B2BDIST",
  // });
  // await refreshAllVariants({
  //   machineNameOfSalesChannel: "B2BDIST",
  //   refreshVariants: true,
  //   refreshProductEnhancements: true,
  //   refreshPricingBuffer: true,
  //   updateOnlyEnabled: false,
  // });
  // await updatePricingEnhancementsBuffer({
  //   machineNameOfSalesChannel: "B2BDIST",
  // });
  // await bulkUpdateInventoryToShopify({
  //   machineNameOfSalesChannel: "B2BDIST",
  //   dryRun: false,
  // });
  await bulkUpdatePricingToShopify({
    machineNameOfSalesChannel: "B2BDIST",
    dryRun: false,
  });
  // await bulkUpdatePrimaryImagesToShopify({
  //   machineNameOfSalesChannel: "B2BDIST",
  // });
  // await getImagesFromSalesChannel({
  //   machineNameOfSalesChannel: "B2BDIST",
  // });
  // await bulkCreateNewProducts({
  //   machineNameOfSalesChannel: "B2BDIST",
  // });
  // await bulkUpdateExistingProducts({
  //   machineNameOfSalesChannel: "B2BDIST",
  //   processOnlyTheFirst: 10,
  // });
  // await bulkUpdateExistingProductBufferIds({
  //   machineNameOfSalesChannel: "SEA TO SUMMIT",
  //   //processOnlyTheFirst: 10,
  // });
  // await reattemptBulkImportSync({
  //   machineNameOfSalesChannel: "B2BDIST",
  // await refreshAllCollections({
  //   machineNameOfSalesChannel: "B2BDIST",
  // });
  // await updateCollectionsToShopify({
  //   machineNameOfSalesChannel: "B2BDIST",
  // });
  // await updateProductStatusBuffer();
  // await CompareBufferToWMSView();
  // await bulkUpdateVariantsToShopify({
  //   machineNameOfSalesChannel: "HELINOX",
  // });
  // await GenericImport();
  // await importShopifyShipments();
  // await addNewProductsToBuffer({
  //   machineNameOfSalesChannel: "B2BDIST",
  // });
  ///////////////////////
  // SEA TO SUMMIT
  // await listAllMismatchingSTSProducts();
  // await syncRelevantSalsifyImages();
  ///////////////////////
  // MYOB
  // await listAllMismatchingSTSProducts();
  // await exportMyobOrders();
  ///////////////////////
  // EMAILER
  // await testEmails();
  ///////////////////////
};

runEverything();
