require("dotenv").config();

module.exports = {
  development: {
    use_env_variable: "DATABASE_URL",
    dialect: "postgres",
    protocol: "postgres",
    dialectOptions: {
      connectTimeout: 24000,
      enableKeepAlive: true,
    },
    pool: {
      max: 50,
      min: 0,
      idle: 5000,
      acquire: 24000,
    },
    logQueryParameters: true,
    logging: false,
  },
  test: {
    database: "products_api_test",
    dialect: "postgres",
  },
  staging: {
    use_env_variable: "DATABASE_URL",
    ssl: true,
    dialect: "postgres",
    protocol: "postgres",
    dialectOptions: {
      connectTimeout: 120000,
      enableKeepAlive: true,
      ssl: {
        require: true,
        rejectUnauthorized: false,
      },
    },
    pool: {
      max: 5,
      min: 0,
      idle: 5000,
      acquire: 120000,
    },
    logQueryParameters: true,
    logging: false,
  },
  test: {
    database: "products_api_test",
    dialect: "postgres",
  },
  production: {
    use_env_variable: "DATABASE_URL",
    ssl: true,
    dialect: "postgres",
    protocol: "postgres",
    dialectOptions: {
      connectTimeout: 120000,
      enableKeepAlive: true,
      ssl: {
        require: true,
        rejectUnauthorized: false,
      },
    },
    pool: {
      max: 50,
      min: 0,
      idle: 5000,
      acquire: 120000,
    },
    logQueryParameters: true,
    logging: false,
  },
};
