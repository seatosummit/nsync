module.exports = {
  apps: [
    {
      script: "./modules/salsify/schedules/products/productbuffer.js",
      cron_restart: "0 */6 * * 1-5",
      name: "salsify-products-to-buffer",
      env_development: {
        NODE_ENV: "test",
      },
      env_production: {
        NODE_ENV: "production",
      },
    },
    {
      script: "./modules/salsify/schedules/products/productparentbuffer.js",
      cron_restart: "0 */6 * * 1-5",
      name: "salsify-product-parents-to-buffer",
      env_development: {
        NODE_ENV: "test",
      },
      env_production: {
        NODE_ENV: "production",
      },
    },
    {
      script: "./modules/pepperi/schedules/products/products.js",
      cron_restart: "*/30 5-17 * * 1-5",
      name: "pepperi-update-products",
      env_development: {
        NODE_ENV: "test",
      },
      env_production: {
        NODE_ENV: "production",
      },
    },
    {
      script: "./modules/pepperi/schedules/products/productimages.js",
      cron_restart: "0 21 * * 1-5",
      name: "pepperi-update-productimages",
      env_development: {
        NODE_ENV: "test",
      },
      env_production: {
        NODE_ENV: "production",
      },
    },
    {
      script: "./modules/pepperi/schedules/customers/customers.js",
      cron_restart: "5 */1 * * 1-5",
      name: "pepperi-update-customers",
      env_development: {
        NODE_ENV: "test",
      },
      env_production: {
        NODE_ENV: "production",
      },
    },
    {
      script: "./modules/pepperi/schedules/orders/orders.js",
      cron_restart: "*/5 5-17 * * 1-5",
      name: "pepperi-export-orders-xml",
      env_development: {
        NODE_ENV: "test",
      },
      env_production: {
        NODE_ENV: "production",
      },
    },
    {
      script: "./modules/pepperi/schedules/products/stocklevels.js",
      cron_restart: "*/10 5-17 * * 1-5",
      name: "pepperi-update-stocklevels",
      autorestart: false,
      env_development: {
        NODE_ENV: "test",
      },
      env_production: {
        NODE_ENV: "production",
      },
    },
    {
      script: "./modules/wms/schedules/products/productbuffer.js",
      cron_restart: "*/15 5-17 * * 1-5",
      name: "pepperi-update-wms-productbuffer",
      autorestart: false,
      env_development: {
        NODE_ENV: "test",
      },
      env_production: {
        NODE_ENV: "production",
      },
    },
    {
      script: "./schedules/BDProductBufferSync.js",
      cron_restart: "*/1 * * * *",
      name: "bd-product-buffer",
      autorestart: false,
      env_development: {
        NODE_ENV: "test",
      },
      env_production: {
        NODE_ENV: "production",
      },
    },
    {
      script: "./schedules/BDPricingBufferSync.js",
      cron_restart: "*/1 * * * *",
      name: "bd-pricing-shopify",
      autorestart: false,
      env_development: {
        NODE_ENV: "development",
      },
      env_production: {
        NODE_ENV: "production",
      },
    },
  ],
};
