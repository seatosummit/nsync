"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("Pricings", {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4,
      },
      variant_sku: {
        type: Sequelize.STRING,
      },
      variant_ean: {
        type: Sequelize.STRING,
      },
      price_retail: {
        type: Sequelize.DOUBLE,
      },
      price_warehouse: {
        type: Sequelize.DOUBLE,
      },
      price_special: {
        type: Sequelize.DOUBLE,
      },
      special_price_startdate: {
        type: Sequelize.DATE,
      },
      special_price_enddate: {
        type: Sequelize.DATE,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("Pricings");
  },
};
