"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("SalesChannelMinimumOrderQuantities", {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4,
      },
      SalesChannelId: {
        type: Sequelize.UUID,
        references: {
          model: "SalesChannels",
          key: "id",
          as: "SalesChannelId",
        },
      },
      sku: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      minimumOrderQuantity: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 1,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("SalesChannelIntegrationRulesets");
  },
};
