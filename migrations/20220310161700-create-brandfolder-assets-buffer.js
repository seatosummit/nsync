"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("BrandfolderBufferAssets", {
      id: {
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      bf_id: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      bf_asset_id: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      description: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      brand: {
        type: Sequelize.STRING,
      },
      type: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      collections: {
        type: Sequelize.JSON,
        allowNull: true,
      },
      custom_fields: {
        type: Sequelize.JSON,
        allowNull: true,
      },
      tags: {
        type: Sequelize.JSON,
        allowNull: true,
      },
      labels: {
        type: Sequelize.JSON,
        allowNull: true,
      },
      au_skus: {
        type: Sequelize.JSON,
        allowNull: true,
      },
      usa_skus: {
        type: Sequelize.JSON,
        allowNull: true,
      },
      parent_skus: {
        type: Sequelize.JSON,
        allowNull: true,
      },
      eans: {
        type: Sequelize.JSON,
        allowNull: true,
      },
      au_office_image_stack: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      au_office_image_ordering: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      usa_office_image_stack: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      ger_office_image_stack: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      china_office_image_stack: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      brand_internal_product_code: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      brand_internal_product_code: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      au_salsify_digital_asset_id: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      au_salsify_linked_to_product: {
        type: Sequelize.BOOLEAN,
        default: false,
      },
      global_salsify_digital_asset_id: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      global_salsify_linked_to_product: {
        type: Sequelize.BOOLEAN,
        default: false,
      },
      section_id: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      snapshot: {
        type: Sequelize.JSON,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("BrandfolderBufferAssets");
  },
};
