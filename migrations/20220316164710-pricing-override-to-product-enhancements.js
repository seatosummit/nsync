"use strict";

// TODO: implement this for pricing updates

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        "ProductVariantEnhancements", // table name
        "override_pricing", // new field name
        {
          type: Sequelize.BOOLEAN,
        }
      ),
    ]);
  },
  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn(
        "ProductVariantEnhancements",
        "override_pricing"
      ),
    ]);
  },
};
