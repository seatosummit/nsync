'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'SalsifyProducts', // table name
        'snapshot', // new field name
        {
          type: Sequelize.TEXT,
          allowNull: false,
          default: 0
        },
      )
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('SalsifyProducts', 'snapshot')
    ]);
  }
};