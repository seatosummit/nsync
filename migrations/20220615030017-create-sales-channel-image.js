"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("SalesChannelImages", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      ProductVariantEnhancementId: {
        type: Sequelize.UUID,
      },
      ProductEnhancementId: {
        type: Sequelize.UUID,
      },
      BrandFolderBufferAttachmentId: {
        type: Sequelize.UUID,
      },
      BrandfolderBufferAssetId: {
        type: Sequelize.UUID,
      },
      brandfolderURL: {
        type: Sequelize.STRING,
      },
      SalesChannelId: {
        type: Sequelize.UUID,
      },
      format: {
        type: Sequelize.STRING,
      },
      name: {
        type: Sequelize.STRING,
      },
      associatedSKUs: {
        type: Sequelize.JSON,
      },
      imageGraphQLId: {
        type: Sequelize.STRING,
      },
      imageShopifyURL: {
        type: Sequelize.STRING,
      },
      imageStack: {
        type: Sequelize.STRING,
      },
      orderNumber: {
        type: Sequelize.INTEGER,
      },
      isPrimary: {
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("SalesChannelImages");
  },
};
