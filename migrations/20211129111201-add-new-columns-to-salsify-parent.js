"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        "SalsifyProductParents", // table name
        "base_product_name", // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
          default: "",
        }
      ),
      queryInterface.addColumn(
        "SalsifyProductParents", // table name
        "primary_image", // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
          default: "",
        }
      ),
      queryInterface.addColumn(
        "SalsifyProductParents", // table name
        "additional_images", // new field name
        {
          type: Sequelize.TEXT,
          allowNull: true,
          default: "[]",
        }
      ),
      queryInterface.addColumn(
        "SalsifyProductParents", // table name
        "packaging_images", // new field name
        {
          type: Sequelize.TEXT,
          allowNull: true,
          default: "[]",
        }
      ),
      queryInterface.addColumn(
        "SalsifyProductParents", // table name
        "salsify_category_ids", // new field name
        {
          type: Sequelize.TEXT,
          allowNull: true,
          default: "[]",
        }
      ),
      queryInterface.addColumn(
        "SalsifyProductParents", // table name
        "colour_layman", // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
          default: "",
        }
      ),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn("SalsifyProductParents", "base_product_name"),
      queryInterface.removeColumn("SalsifyProductParents", "primary_image"),
      queryInterface.removeColumn("SalsifyProductParents", "additional_images"),
      queryInterface.removeColumn("SalsifyProductParents", "packaging_images"),
      queryInterface.removeColumn("SalsifyProductParents", "salsify_category_ids"),
      queryInterface.removeColumn("SalsifyProductParents", "colour_layman"),
    ]);
  },
};
