"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        "SalesChannelIntegrationRulesets", // table name
        "tagOrdersWithNoStock", // new field name
        {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: false,
        }
      ),
    ]);
  },
  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn(
        "SalesChannelIntegrationRulesets",
        "tagOrdersWithNoStock"
      ),
    ]);
  },
};
