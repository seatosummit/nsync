"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("BlackDiamondBufferProducts", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      sku: {
        type: Sequelize.STRING,
      },
      salsify_system_id: {
        type: Sequelize.STRING,
      },
      shopify_product_id: {
        type: Sequelize.BIGINT,
        allowNull: true,
      },
      title: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      body_html: {
        type: Sequelize.TEXT,
        allowNull: true,
      },
      vendor: {
        type: Sequelize.STRING,
        default: "Black Diamond",
      },
      gender: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      usps: {
        type: Sequelize.TEXT,
        default: "[]",
      },
      metafields: {
        type: Sequelize.TEXT,
        default: "[]",
      },
      tech_specs: {
        type: Sequelize.TEXT,
        default: "[]",
      },
      tags: {
        type: Sequelize.TEXT,
        default: "[]",
      },
      categories: {
        type: Sequelize.TEXT,
        default: "[]",
      },
      status: {
        type: Sequelize.STRING,
        default: "draft",
      },
      admin_graphql_api_id: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      images: {
        type: Sequelize.TEXT,
      },
      options: {
        type: Sequelize.TEXT,
      },
      variants: {
        type: Sequelize.TEXT,
      },
      snapshot: {
        type: Sequelize.TEXT,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("BlackDiamondBufferProducts");
  },
};
