"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("SalsifyGlobalProducts", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      salsify_id: {
        type: Sequelize.STRING,
      },
      salsify_created_at: {
        type: Sequelize.DATE,
      },
      salsify_updated_at: {
        type: Sequelize.DATE,
      },
      salsify_parent_id: {
        type: Sequelize.STRING,
      },
      salsify_system_id: {
        type: Sequelize.STRING,
      },
      ean: {
        type: Sequelize.STRING,
      },
      sku: {
        type: Sequelize.STRING,
      },
      us_sku: {
        type: Sequelize.STRING,
      },
      primary_name: {
        type: Sequelize.STRING,
      },
      us_product_name: {
        type: Sequelize.STRING,
      },
      size_name_longform: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      brand: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      category: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      sub_category: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      family: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      sub_family: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      colour_pantone: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      colour_family: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      gender: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      country_of_origin: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      detail1: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      detail2: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      detail3: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      detail4: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      detail5: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      packaged: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      digital_assets: {
        type: Sequelize.TEXT,
        default: "[]",
        allowNull: true,
      },
      snapshot: {
        type: Sequelize.TEXT,
        default: "{}",
        allowNull: true,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("SalsifyGlobalProducts");
  },
};
