"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.createTable(
        "PepperiOrderLineItems",
        {
          id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER,
          },
          WMS_SAL_ID: {
            type: Sequelize.INTEGER,
            allowNull: true,
          },
          itemID: {
            type: Sequelize.INTEGER,
            allowNull: false,
          },
          sku: {
            type: Sequelize.STRING,
            allowNull: false,
          },
          name: {
            type: Sequelize.STRING,
            allowNull: false,
          },
          quantity: {
            type: Sequelize.INTEGER,
          },
          snapshot: {
            type: Sequelize.TEXT,
            allowNull: false,
          },
          unitPriceExBeforeDiscount: {
            type: Sequelize.DOUBLE,
            allowNull: true,
          },
          unitPriceExAfterDiscount: {
            type: Sequelize.DOUBLE,
            allowNull: true,
          },
          lineTotalExBeforeDiscount: {
            type: Sequelize.DOUBLE,
            allowNull: true,
          },
          lineTotalExAfterDiscount: {
            type: Sequelize.DOUBLE,
            allowNull: true,
          },
          STSLineItemHasShipped: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            default: false,
          },
          STSTrackingCode: {
            type: Sequelize.STRING,
            allowNull: true,
          },
          createdAt: {
            allowNull: false,
            type: Sequelize.DATE,
          },
          updatedAt: {
            allowNull: false,
            type: Sequelize.DATE,
          },
        },
        { transaction }
      );
      await queryInterface.addIndex("PepperiOrderLineItems", ["WMS_SAL_ID"], {
        fields: "WMS_SAL_ID",
        unique: true,
        transaction,
      });
      await transaction.commit();
    } catch (error) {
      await transaction.rollback();
      throw err;
    }
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("PepperiOrderLineItems");
  },
};
