'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'SalsifyProducts', // table name
        'salsify_brand', // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
        },
      )
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('SalsifyProducts', 'salsify_brand')
    ]);
  }
};