"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("BlackDiamondBufferImages", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      salsify_digital_asset_id: {
        type: Sequelize.STRING,
      },
      salsify_filename: {
        type: Sequelize.STRING,
      },
      salsify_format: {
        type: Sequelize.STRING,
      },
      salsify_url: {
        type: Sequelize.STRING,
      },
      salsify_parent_skus: {
        type: Sequelize.TEXT,
      },
      salsify_variant_skus: {
        type: Sequelize.TEXT,
      },
      salsify_primary_image_ids: {
        type: Sequelize.TEXT,
      },
      salsify_additional_image_ids: {
        type: Sequelize.TEXT,
      },
      salsify_packaging_image_ids: {
        type: Sequelize.TEXT,
      },
      salsify_lifestyle_image_ids: {
        type: Sequelize.TEXT,
      },
      shopify_id: {
        type: Sequelize.BIGINT,
      },
      shopify_product_id: {
        type: Sequelize.BIGINT,
      },
      shopify_variant_ids: {
        type: Sequelize.TEXT,
      },
      shopify_src: {
        type: Sequelize.STRING,
      },
      main_image: {
        type: Sequelize.BOOLEAN,
      },
      position: {
        type: Sequelize.INTEGER,
      },
      encoded_version: {
        type: Sequelize.TEXT,
      },
      snapshot: {
        type: Sequelize.TEXT,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("BlackDiamondBufferImages");
  },
};
