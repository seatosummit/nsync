"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        "BlackDiamondProductCategoryDefinitions", // table name
        "custom_name", // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
        }
      ),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn(
        "BlackDiamondProductCategoryDefinitions",
        "custom_name"
      ),
    ]);
  },
};
