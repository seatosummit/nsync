"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addIndex("ShopifyBufferProductVariants", {
      name: "ShopifyBufferProductVariants_product_id_IDX",
      using: "BTREE",
      fields: [
        {
          name: "shopify_product_id",
          order: "ASC",
        },
      ],
    });
    await queryInterface.addIndex("ShopifyBufferProductVariants", {
      name: "ShopifyBufferProductVariants_shopify_id_IDX",
      using: "BTREE",
      fields: [
        {
          name: "shopify_id",
          order: "ASC",
        },
      ],
    });
    await queryInterface.addIndex("BlackDiamondBufferProducts", {
      name: "BlackDiamondBufferProducts_shopify_product_id_IDX",
      using: "BTREE",
      fields: [
        {
          name: "shopify_product_id",
          order: "ASC",
        },
      ],
    });
    await queryInterface.addIndex("BlackDiamondBufferProducts", {
      name: "BlackDiamondBufferProducts_vendor_IDX",
      using: "BTREE",
      fields: [
        {
          name: "vendor",
          order: "ASC",
        },
      ],
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex(
      "ShopifyBufferProductVariants",
      "ShopifyBufferProductVariants_product_id_IDX"
    );
    await queryInterface.removeIndex(
      "ShopifyBufferProductVariants",
      "ShopifyBufferProductVariants_shopify_id_IDX"
    );
    await queryInterface.removeIndex(
      "BlackDiamondBufferProducts",
      "BlackDiamondBufferProducts_shopify_product_id_IDX"
    );
    await queryInterface.removeIndex(
      "BlackDiamondBufferProducts",
      "BlackDiamondBufferProducts_vendor_IDX"
    );
  },
};
