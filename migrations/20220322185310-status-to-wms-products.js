"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        "WMSProducts", // table name
        "status", // new field name
        {
          type: Sequelize.STRING,
        }
      ),
    ]);
  },
  down: async (queryInterface, Sequelize) => {
    return Promise.all([queryInterface.removeColumn("WMSProducts", "status")]);
  },
};
