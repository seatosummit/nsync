"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        "BlackDiamondBufferCollections", // table name
        "salsify_property_value_id", // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
        }
      ),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn(
        "BlackDiamondBufferCollections",
        "salsify_property_value_id"
      ),
    ]);
  },
};
