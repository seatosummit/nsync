"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        "SalesChannels", // table name
        "shopify_config", // new field name
        {
          type: Sequelize.TEXT,
          default: "{}",
        }
      ),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn("SalesChannels", "shopify_config"),
    ]);
  },
};
