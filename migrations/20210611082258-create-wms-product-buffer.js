"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.createTable(
        "WMSProducts",
        {
          id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER,
          },
          WMSProductID: {
            allowNull: false,
            type: Sequelize.INTEGER,
          },
          sku: {
            allowNull: false,
            type: Sequelize.STRING,
          },
          name: {
            type: Sequelize.STRING,
          },
          unitPrice: {
            type: Sequelize.DOUBLE,
          },
          category: {
            allowNull: true,
            type: Sequelize.STRING,
          },
          brand: {
            allowNull: true,
            type: Sequelize.STRING,
          },
          barcode: {
            allowNull: true,
            type: Sequelize.STRING,
          },
          retailPrice: {
            allowNull: true,
            type: Sequelize.DOUBLE,
          },
          perUnitQuantity: {
            type: Sequelize.DOUBLE,
          },
          stockQuantity: {
            type: Sequelize.INTEGER,
          },
          unitOfMeasure: {
            type: Sequelize.STRING,
          },
          pepperiEnabled: {
            type: Sequelize.BOOLEAN,
          },
          stockETA: {
            allowNull: true,
            type: Sequelize.DATE,
          },
          productSnapshot: {
            type: Sequelize.TEXT,
          },
          createdAt: {
            allowNull: false,
            type: Sequelize.DATE,
          },
          updatedAt: {
            allowNull: false,
            type: Sequelize.DATE,
          },
        },
        { transaction }
      );
      await queryInterface.addIndex(
        "WMSProducts",
        ["sku"],
        {
          fields: "sku",
          unique: true,
          transaction,
        }
      );
      await transaction.commit();
    } catch (error) {
      await transaction.rollback();
      throw err;
    }
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("WMSProducts");
  },
};
