"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        "SalsifyDigitalAssets", // table name
        "salsify_primary_skus", // new field name
        {
          type: Sequelize.TEXT,
          default: "[]",
          allowNull: true,
        }
      ),
      queryInterface.addColumn(
        "SalsifyDigitalAssets", // table name
        "salsify_additional_skus", // new field name
        {
          type: Sequelize.TEXT,
          default: "[]",
          allowNull: true,
        }
      ),
      queryInterface.addColumn(
        "SalsifyDigitalAssets", // table name
        "salsify_packaging_skus", // new field name
        {
          type: Sequelize.TEXT,
          default: "[]",
          allowNull: true,
        }
      ),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn("SalsifyDigitalAssets", "primary_skus"),
      queryInterface.removeColumn("SalsifyDigitalAssets", "additional_skus"),
      queryInterface.removeColumn("SalsifyDigitalAssets", "packaging_skus"),
    ]);
  },
};
