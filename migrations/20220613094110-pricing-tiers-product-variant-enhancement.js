"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        "PricingEnhancements", // table name
        "ProductVariantEnhancementId", // new field name
        {
          type: Sequelize.UUID,
        }
      ),
    ]);
  },
  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn(
        "PricingEnhancements",
        "ProductVariantEnhancementId"
      ),
    ]);
  },
};
