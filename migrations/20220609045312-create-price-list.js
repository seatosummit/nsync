"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("PriceLists", {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4,
      },
      SalesChannelId: {
        type: Sequelize.UUID,
        references: {
          model: "SalesChannels",
          key: "id",
          as: "SalesChannelId",
        },
      },
      name: Sequelize.STRING,
      description: Sequelize.STRING,
      erpFieldName: Sequelize.STRING,
      timeSensitive: Sequelize.BOOLEAN,
      timeSensitiveStartField: Sequelize.STRING,
      timeSensitiveEndField: Sequelize.STRING,
      price_list_slug: Sequelize.STRING,
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("PriceLists");
  },
};
