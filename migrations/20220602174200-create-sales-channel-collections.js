"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("SalesChannelCollections", {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4,
      },
      SalesChannelId: {
        type: Sequelize.UUID,
        references: {
          model: "SalesChannels",
          key: "id",
          as: "SalesChannelId",
        },
      },
      shopifyCollectionId: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      title: {
        type: Sequelize.STRING,
      },
      handle: {
        type: Sequelize.STRING,
      },
      appliedDisjunctively: {
        type: Sequelize.BOOLEAN,
        default: false,
      },
      descriptionHtml: {
        type: Sequelize.TEXT,
        default: "",
      },
      sortOrder: {
        type: Sequelize.STRING,
        default: "BEST_SELLING",
      },
      templateSuffix: {
        type: Sequelize.STRING,
        default: null,
      },
      ruleSet: {
        type: Sequelize.JSON,
      },
      payload: {
        type: Sequelize.JSON,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("SalesChannelCollections");
  },
};
