"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        "SalsifyDigitalAssets", // table name
        "au_office_additional_ordering", // new field name
        {
          type: Sequelize.STRING,
        }
      ),
    ]);
  },
  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn(
        "SalsifyDigitalAssets",
        "au_office_additional_ordering"
      ),
    ]);
  },
};
