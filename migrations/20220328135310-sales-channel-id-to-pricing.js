"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        "Pricings", // table name
        "SalesChannelId", // new field name
        {
          type: Sequelize.UUID,
        }
      ),
    ]);
  },
  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn("Pricings", "SalesChannelId"),
    ]);
  },
};
