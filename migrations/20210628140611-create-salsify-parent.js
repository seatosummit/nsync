"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("SalsifyProductParents", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      sku: {
        type: Sequelize.STRING,
      },
      name: {
        type: Sequelize.STRING,
      },
      base_product_name: {
        type: Sequelize.STRING,
      },
      brand_category: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      brand_sub_category: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      erp_tier1_financial_category: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      erp_category: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      erp_sub_category: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      b2b_category: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      b2b_sub_category: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      child_skus: {
        type: Sequelize.TEXT,
        allowNull: true,
        default: "",
      },
      snapshot: {
        type: Sequelize.TEXT,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("SalsifyProductParents");
  },
};
