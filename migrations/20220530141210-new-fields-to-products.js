"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        "Products", // table name
        "tech_specs", // new field name
        {
          type: Sequelize.JSON,
          defaultValue: {},
        }
      ),
      queryInterface.addColumn(
        "Products", // table name
        "usps", // new field name
        {
          type: Sequelize.JSON,
          defaultValue: {},
        }
      ),
      queryInterface.addColumn(
        "Products", // table name
        "erp_category", // new field name
        {
          type: Sequelize.STRING,
        }
      ),
      queryInterface.addColumn(
        "Products", // table name
        "erp_sub_category", // new field name
        {
          type: Sequelize.STRING,
        }
      ),
    ]);
  },
  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn("Products", "tech_specs"),
      queryInterface.removeColumn("Products", "usps"),
      queryInterface.removeColumn("Products", "erp_category"),
      queryInterface.removeColumn("Products", "erp_sub_category"),
    ]);
  },
};
