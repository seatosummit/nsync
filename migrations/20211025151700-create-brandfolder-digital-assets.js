"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("BrandfolderDigitalAssets", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      salsify_digital_asset_id: {
        type: Sequelize.STRING,
      },
      salsify_brand: {
        type: Sequelize.STRING,
      },
      base_product_name: {
        type: Sequelize.STRING,
      },
      short_description: {
        type: Sequelize.TEXT,
        default: "No product description available",
      },
      brandfolder_asset_id: {
        type: Sequelize.STRING,
      },
      brandfolder_asset_url: {
        type: Sequelize.STRING,
      },
      brandfolder_collection_id: {
        type: Sequelize.STRING,
      },
      filename: {
        type: Sequelize.STRING,
      },
      format: {
        type: Sequelize.STRING,
      },
      salsify_url: {
        type: Sequelize.STRING,
      },
      salsify_parent_skus: {
        type: Sequelize.TEXT,
        default: "[]",
      },
      salsify_variant_skus: {
        type: Sequelize.TEXT,
        default: "[]",
      },
      salsify_parent_eans: {
        type: Sequelize.TEXT,
        default: "[]",
      },
      salsify_variant_eans: {
        type: Sequelize.TEXT,
        default: "[]",
      },
      au_office_image_stack: {
        type: Sequelize.STRING,
      },
      us_office_image_stack: {
        type: Sequelize.STRING,
      },
      ger_office_image_stack: {
        type: Sequelize.STRING,
      },
      gender: {
        type: Sequelize.STRING,
      },
      base_product_name: {
        type: Sequelize.STRING,
      },
      colour: {
        type: Sequelize.STRING,
      },
      layman_colour: {
        type: Sequelize.STRING,
      },
      file_size_bytes: {
        type: Sequelize.INTEGER,
      },
      original_width_px: {
        type: Sequelize.INTEGER,
      },
      original_height_px: {
        type: Sequelize.INTEGER,
      },
      tags: {
        type: Sequelize.TEXT,
      },
      salsify_digital_asset_snapshot: {
        type: Sequelize.TEXT,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("BrandfolderDigitalAssets");
  },
};
