"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("WMSCustomers", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      company_name: {
        type: Sequelize.STRING,
      },
      contact_name: {
        type: Sequelize.STRING,
      },
      contact_email: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      category: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      myob_card_id: {
        type: Sequelize.INTEGER
      },
      sts_territory_manager: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      street1: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      street2: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      street3: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      street4: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      suburb: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      state: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      postcode: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      phone: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      customerSnapshot: {
        type: Sequelize.TEXT,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("WMSCustomers");
  },
};
