"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        "SalsifyProducts", // table name
        "child_skus", // new field name
        {
          type: Sequelize.TEXT,
          allowNull: true,
          default: 0,
        }
      ),
      queryInterface.addColumn(
        "SalsifyProducts", // table name
        "parent_sku", // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
          default: 0,
        }
      ),
      queryInterface.addColumn(
        "SalsifyProducts", // table name
        "is_child", // new field name
        {
          type: Sequelize.BOOLEAN,
          allowNull: true,
          default: false,
        }
      ),
      queryInterface.addColumn(
        "SalsifyProducts", // table name
        "simple_or_configurable", // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
          default: 0,
        }
      ),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn("SalsifyProducts", "child_skus"),
      queryInterface.removeColumn("SalsifyProducts", "parent_sku"),
      queryInterface.removeColumn("SalsifyProducts", "simple_or_configurable"),
    ]);
  },
};
