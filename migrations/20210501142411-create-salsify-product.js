"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("SalsifyProducts", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      sku: {
        type: Sequelize.STRING,
      },
      name: {
        type: Sequelize.STRING,
      },
      size: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      colour: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      erp_tier1_financial_category: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      erp_category: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      erp_sub_category: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      b2b_category: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      b2b_sub_category: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("SalsifyProducts");
  },
};
