"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        "SalesChannelIntegrationRulesets", // table name
        "usePricingEnhancements", // new field name
        {
          type: Sequelize.BOOLEAN,
          defaultValue: false,
        }
      ),
    ]);
  },
  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn(
        "SalesChannelIntegrationRulesets",
        "usePricingEnhancements"
      ),
    ]);
  },
};
