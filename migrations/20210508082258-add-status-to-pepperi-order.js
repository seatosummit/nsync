'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'PepperiOrders', // table name
        'status', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: false,
          default: 0
        },
      ),
      queryInterface.addColumn(
        'PepperiOrders', // table name
        'statusName', // new field name
        {
          type: Sequelize.STRING,
          allowNull: false,
          default: "Not set"
        },
      ),
      queryInterface.addColumn(
        'PepperiOrders', // table name
        'WMSStatus', // new field name
        {
          type: Sequelize.STRING,
          allowNull: true,
          default: "Not set"
        },
      ),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('PepperiOrders', 'status'),
      queryInterface.removeColumn('PepperiOrders', 'statusName'),
      queryInterface.removeColumn('PepperiOrders', 'WMSStatus'),
    ]);
  }
};