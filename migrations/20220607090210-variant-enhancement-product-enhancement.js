"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        "ProductVariantEnhancements", // table name
        "ProductEnhancementId", // new field name
        {
          type: Sequelize.UUID,
        }
      ),
    ]);
  },
  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn(
        "ProductVariantEnhancements",
        "ProductEnhancementId"
      ),
    ]);
  },
};
