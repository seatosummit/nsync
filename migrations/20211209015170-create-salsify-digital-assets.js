"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("SalsifyDigitalAssets", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      salsify_organization_id: {
        type: Sequelize.STRING,
      },
      salsify_id: {
        type: Sequelize.STRING,
      },
      salsify_system_id: {
        type: Sequelize.STRING,
      },
      salsify_url: {
        type: Sequelize.STRING,
      },
      salsify_format: {
        type: Sequelize.STRING,
      },
      salsify_brand: {
        type: Sequelize.STRING,
      },
      salsify_name: {
        type: Sequelize.STRING,
      },
      salsify_filename: {
        type: Sequelize.STRING,
      },
      salsify_status: {
        type: Sequelize.STRING,
      },
      salsify_asset_height_pixels: {
        type: Sequelize.INTEGER,
      },
      salsify_asset_width_pixels: {
        type: Sequelize.INTEGER,
      },
      salsify_au_ids: {
        type: Sequelize.TEXT,
        default: "[]",
      },
      salsify_usa_ids: {
        type: Sequelize.TEXT,
        default: "[]",
      },
      snapshot: {
        type: Sequelize.TEXT,
        default: "{}",
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("SalsifyDigitalAssets");
  },
};
