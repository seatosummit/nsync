"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("BlackDiamondBufferCollections", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      shopify_collection_id: {
        type: Sequelize.BIGINT,
        allowNull: true,
      },
      title: {
        type: Sequelize.STRING,
      },
      handle: {
        type: Sequelize.STRING,
      },
      disjunctive: {
        type: Sequelize.BOOLEAN,
        default: false
      },
      body_html: {
        type: Sequelize.TEXT,
        default: ""
      },
      sort_order: {
        type: Sequelize.STRING,
        default: "best-selling"
      },
      template_suffix: {
        type: Sequelize.STRING,
        default: null
      },
      admin_graphql_api_id:{
        type: Sequelize.STRING,
      },
      rules: {
        type: Sequelize.TEXT
      },
      snapshot: {
        type: Sequelize.TEXT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("BlackDiamondBufferCollections");
  },
};
