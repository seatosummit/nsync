'use strict';

module.exports = {
    up: async (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn(
                'SalsifyProducts', // table name
                'brand', // new field name
                {
                    type: Sequelize.TEXT,
                    allowNull: true,
                    default: 'No Brand set'
                }
            )
        ]);
    },

    down: async (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.removeColumn('SalsifyProducts', 'brand'),
        ]);
    }
};