"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("PepperiBufferImages", {
      id: {
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      BrandfolderBufferAssetId: {
        type: Sequelize.UUID,
      },
      sku: {
        type: Sequelize.STRING,
      },
      filename: {
        type: Sequelize.STRING,
      },
      brand: {
        type: Sequelize.STRING,
      },
      original_url: {
        type: Sequelize.STRING,
      },
      uploadedToSFTP: {
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("PepperiBufferImages");
  },
};
