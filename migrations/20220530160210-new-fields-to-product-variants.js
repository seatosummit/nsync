"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        "ProductVariants", // table name
        "parent_sku", // new field name
        {
          type: Sequelize.STRING,
        }
      ),
    ]);
  },
  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn("ProductVariants", "parent_sku"),
    ]);
  },
};
