"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        "ProductEnhancements", // table name
        "GraphQLProductId", // new field name
        {
          type: Sequelize.STRING,
        }
      ),
      queryInterface.addColumn(
        "ProductVariantEnhancements", // table name
        "GraphQLVariantId", // new field name
        {
          type: Sequelize.STRING,
        }
      ),
      queryInterface.addColumn(
        "ProductVariantEnhancements", // table name
        "GraphQLInventoryItemId", // new field name
        {
          type: Sequelize.STRING,
        }
      ),
    ]);
  },
  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn("ProductEnhancements", "GraphQLProductId"),
      queryInterface.removeColumn(
        "ProductVariantEnhancements",
        "GraphQLVariantId"
      ),
      queryInterface.removeColumn(
        "ProductVariantEnhancements",
        "GraphQLInventoryItemId"
      ),
    ]);
  },
};
