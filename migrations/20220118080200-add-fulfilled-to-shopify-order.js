'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'ShopifyOrders', // table name
        'fulfilled', // new field name
        {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          default: 0,
        },
      )
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('ShopifyOrders', 'fulfilled')
    ]);
  }
};