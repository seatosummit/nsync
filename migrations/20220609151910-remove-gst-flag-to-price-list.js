"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        "PriceLists", // table name
        "removeGSTFromPrice", // new field name
        {
          type: Sequelize.BOOLEAN,
        }
      ),
    ]);
  },
  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn("PriceLists", "removeGSTFromPrice"),
    ]);
  },
};
