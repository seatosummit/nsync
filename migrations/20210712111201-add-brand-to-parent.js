'use strict';

module.exports = {
    up: async (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn(
                'SalsifyProductParents', // table name
                'brand', // new field name
                {
                    type: Sequelize.STRING,
                    allowNull: true,
                    default: 0
                }
            ),
            queryInterface.removeColumn('SalsifyProductParents', 'base_product_name')
        ]);
    },

    down: async (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.removeColumn('SalsifyProductParents', 'brand')
        ]);
    }
};