"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        "WMSProducts", // table name
        "moq", // new field name
        {
          type: Sequelize.INTEGER,
          default: 1,
        }
      ),
    ]);
  },
  down: async (queryInterface, Sequelize) => {
    return Promise.all([queryInterface.removeColumn("WMSProducts", "moq")]);
  },
};
