"use strict";

module.exports = {
    up: async (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.addColumn(
                "SalsifyProducts", // table name
                "base_product_name", // new field name
                {
                    type: Sequelize.STRING,
                    allowNull: true,
                }
            ),
            queryInterface.addColumn(
                "SalsifyProducts", // table name
                "primary_image", // new field name
                {
                    type: Sequelize.STRING,
                    allowNull: true,
                    default: "",
                }
            ),
            queryInterface.addColumn(
                "SalsifyProducts", // table name
                "additional_images", // new field name
                {
                    type: Sequelize.TEXT,
                    allowNull: true,
                    default: "[]",
                }
            ),
            queryInterface.addColumn(
                "SalsifyProducts", // table name
                "packaging_images", // new field name
                {
                    type: Sequelize.TEXT,
                    allowNull: true,
                    default: "[]",
                }
            ),
            queryInterface.addColumn(
                "SalsifyProducts", // table name
                "salsify_category_ids", // new field name
                {
                    type: Sequelize.TEXT,
                    allowNull: true,
                    default: "[]",
                }
            ),
            queryInterface.addColumn(
                "SalsifyProducts", // table name
                "colour_layman", // new field name
                {
                    type: Sequelize.STRING,
                    allowNull: true,
                }
            ),
            queryInterface.addColumn(
                "SalsifyProducts", // table name
                "barcode_ean", // new field name
                {
                    type: Sequelize.STRING,
                    allowNull: true,
                }
            ),
            queryInterface.addColumn(
                "SalsifyProducts", // table name
                "usa_sku", // new field name
                {
                    type: Sequelize.STRING,
                    allowNull: true,
                }
            ),
            queryInterface.addColumn(
                "SalsifyProducts", // table name
                "asin_amazon", // new field name
                {
                    type: Sequelize.STRING,
                    allowNull: true,
                }
            ),
        ]);
    },

    down: async (queryInterface, Sequelize) => {
        return Promise.all([
            queryInterface.removeColumn("SalsifyProducts", "base_product_name"),
            queryInterface.removeColumn("SalsifyProducts", "primary_image"),
            queryInterface.removeColumn("SalsifyProducts", "additional_images"),
            queryInterface.removeColumn("SalsifyProducts", "packaging_images"),
            queryInterface.removeColumn("SalsifyProducts", "salsify_category_ids"),
            queryInterface.removeColumn("SalsifyProducts", "barcode_ean"),
            queryInterface.removeColumn("SalsifyProducts", "usa_sku"),
            queryInterface.removeColumn("SalsifyProducts", "colour_layman"),
            queryInterface.removeColumn("SalsifyProducts", "asin_amazon"),
        ]);
    },
};
