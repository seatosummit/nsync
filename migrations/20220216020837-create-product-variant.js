"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("ProductVariants", {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4,
      },
      productId: {
        type: Sequelize.UUID,
      },
      name: {
        type: Sequelize.STRING,
      },
      sku: {
        type: Sequelize.STRING,
      },
      ean: {
        type: Sequelize.STRING,
      },
      asin: {
        type: Sequelize.STRING,
      },
      salsifyProductId: {
        type: Sequelize.STRING,
      },
      erpProductId: {
        type: Sequelize.STRING,
      },
      size: {
        type: Sequelize.STRING,
      },
      colour: {
        type: Sequelize.STRING,
      },
      colourLayman: {
        type: Sequelize.STRING,
      },
      description: {
        type: Sequelize.TEXT,
      },
      minOrderQty: {
        type: Sequelize.INTEGER,
      },
      erpSnapshot: {
        type: Sequelize.JSON,
      },
      stockRefillETA: {
        type: Sequelize.DATE,
      },
      brand: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("ProductVariants");
  },
};
