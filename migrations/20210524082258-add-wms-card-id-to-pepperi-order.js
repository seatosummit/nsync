'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        'PepperiOrders', // table name
        'wmsCardID', // new field name
        {
          type: Sequelize.INTEGER,
          allowNull: false,
          default: 0
        },
      ),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn('PepperiOrders', 'wmsCardID'),
    ]);
  }
};