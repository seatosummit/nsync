"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    const transaction = await queryInterface.sequelize.transaction();

    try {
      await queryInterface.createTable(
        "PepperiOrders",
        {
          id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER,
          },
          submittedAt: {
            type: Sequelize.DATE,
          },
          ownerEmail: {
            type: Sequelize.STRING,
          },
          salesPerson: {
            type: Sequelize.STRING,
          },
          pepperiCustomerID: {
            type: Sequelize.INTEGER,
          },
          pepperiCustomerName: {
            type: Sequelize.STRING,
          },
          pepperiOrderInternalID: {
            type: Sequelize.INTEGER,
          },
          orderLines: {
            type: Sequelize.TEXT,
          },
          shipTo: {
            type: Sequelize.TEXT,
          },
          orderSnapshot: {
            type: Sequelize.TEXT,
          },
          xmlExported: {
            type: Sequelize.BOOLEAN,
            allowNull: false,
            default: false,
          },
          customerEmail: {
            type: Sequelize.STRING,
            allowNull: true,
          },
          createdAt: {
            allowNull: false,
            type: Sequelize.DATE,
          },
          updatedAt: {
            allowNull: false,
            type: Sequelize.DATE,
          },
        },
        { transaction }
      );
      await queryInterface.addIndex(
        "PepperiOrders",
        ["pepperiOrderInternalID"],
        {
          fields: "pepperiOrderInternalID",
          unique: true,
          transaction,
        }
      );
      await transaction.commit();
    } catch (error) {
      await transaction.rollback();
      throw err;
    }
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("PepperiOrders");
  },
};
