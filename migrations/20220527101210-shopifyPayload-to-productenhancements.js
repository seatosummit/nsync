"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        "ProductEnhancements", // table name
        "shopifyPayload", // new field name
        {
          type: Sequelize.JSON,
          defaultValue: {},
        }
      ),
    ]);
  },
  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn("ProductEnhancements", "shopifyPayload"),
    ]);
  },
};
