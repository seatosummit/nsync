"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addIndex("BrandfolderBufferAttachments", {
      name: "BrandfolderBufferAttachments_brandfolderasset_id_IDX",
      using: "BTREE",
      fields: [
        {
          name: "BrandfolderBufferAssetId",
          order: "ASC",
        },
      ],
    });
    await queryInterface.addIndex("ProductVariantEnhancements", {
      name: "ProductVariantEnhancements_saleschannel_id_IDX",
      using: "BTREE",
      fields: [
        {
          name: "SalesChannelId",
          order: "ASC",
        },
      ],
    });
    await queryInterface.addIndex("ProductVariantEnhancements", {
      name: "ProductVariantEnhancements_productvariant_id_IDX",
      using: "BTREE",
      fields: [
        {
          name: "ProductVariantId",
          order: "ASC",
        },
      ],
    });
    await queryInterface.addIndex("ProductVariantEnhancements", {
      name: "ProductEnhancements_graphqlvariant_saleschannel_id_IDX",
      using: "BTREE",
      fields: [
        {
          name: "GraphQLVariantId",
          order: "ASC",
        },
        {
          name: "SalesChannelId",
          order: "ASC",
        },
      ],
    });
    await queryInterface.addIndex("ProductEnhancements", {
      name: "ProductEnhancements_saleschannel_id_IDX",
      using: "BTREE",
      fields: [
        {
          name: "SalesChannelId",
          order: "ASC",
        },
      ],
    });
    await queryInterface.addIndex("ProductEnhancements", {
      name: "ProductEnhancements_graphql_saleschannel_id_IDX",
      using: "BTREE",
      fields: [
        {
          name: "GraphQLProductId",
          order: "ASC",
        },
        {
          name: "SalesChannelId",
          order: "ASC",
        },
      ],
    });
    await queryInterface.addIndex("PricingEnhancements", {
      name: "PricingEnhancements_saleschannel_id_IDX",
      using: "BTREE",
      fields: [
        {
          name: "SalesChannelId",
          order: "ASC",
        },
      ],
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex(
      "BrandfolderBufferAttachments",
      "BrandfolderBufferAttachments_brandfolderasset_id_IDX"
    );
    await queryInterface.removeIndex(
      "ProductVariantEnhancements",
      "ProductVariantEnhancements_saleschannel_id_IDX"
    );
    await queryInterface.removeIndex(
      "ProductVariantEnhancements",
      "ProductVariantEnhancements_productvariant_id_IDX"
    );
    await queryInterface.removeIndex(
      "ProductVariantEnhancements",
      "ProductEnhancements_graphqlvariant_saleschannel_id_IDX"
    );
    await queryInterface.removeIndex(
      "ProductEnhancements",
      "ProductEnhancements_saleschannel_id_IDX"
    );
    await queryInterface.removeIndex(
      "ProductEnhancements",
      "ProductEnhancements_graphql_saleschannel_id_IDX"
    );
    await queryInterface.removeIndex(
      "PricingEnhancements",
      "PricingEnhancements_saleschannel_id_IDX"
    );
  },
};
