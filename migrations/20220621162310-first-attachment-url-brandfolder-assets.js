"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        "BrandfolderBufferAssets", // table name
        "latestAttachmentURL", // new field name
        {
          type: Sequelize.STRING,
          defaultValue: false,
        }
      ),
    ]);
  },
  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn(
        "BrandfolderBufferAssets",
        "latestAttachmentURL"
      ),
    ]);
  },
};
