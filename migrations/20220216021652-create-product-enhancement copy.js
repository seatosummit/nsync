"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("ProductEnhancements", {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4,
      },
      productId: {
        type: Sequelize.UUID,
      },
      salesChannelId: {
        type: Sequelize.UUID,
      },
      shopifyProductId: {
        type: Sequelize.BIGINT,
      },
      shopifyImages: {
        type: Sequelize.JSON,
      },
      shopifyOptions: {
        type: Sequelize.JSON,
      },
      shopifyVariants: {
        type: Sequelize.JSON,
      },
      metafields: {
        type: Sequelize.JSON,
      },
      status: {
        type: Sequelize.STRING,
      },
      updatedToShopify: {
        type: Sequelize.DATE,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("ProductEnhancements");
  },
};
