"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        "BrandfolderBufferAssets", // table name
        "bf_processed", // new field name
        {
          type: Sequelize.BOOLEAN,
          default: false,
        }
      ),
    ]);
  },
  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn("BrandfolderBufferAssets", "bf_processed"),
    ]);
  },
};
