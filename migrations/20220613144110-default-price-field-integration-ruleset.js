"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        "SalesChannelIntegrationRulesets", // table name
        "defaultPriceFieldName", // new field name
        {
          type: Sequelize.STRING,
        }
      ),
    ]);
  },
  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn(
        "SalesChannelIntegrationRulesets",
        "defaultPriceFieldName"
      ),
    ]);
  },
};
