"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        "ShopifyOrders", // table name
        "salesChannelId", // new field name
        {
          type: Sequelize.UUID,
          references: {
            model: "SalesChannels",
            key: "id",
            as: "salesChannelId",
          },
        }
      ),
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn("ShopifyOrders", "salesChannelId"),
    ]);
  },
};
