"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("ShopifyOrders", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      orderID: {
        type: Sequelize.BIGINT,
      },
      orderNumber: {
        type: Sequelize.STRING,
      },
      checkoutID: {
        type: Sequelize.STRING,
      },
      orderSnapshot: {
        type: Sequelize.TEXT,
      },
      xmlExported: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        default: false,
      },
      customerEmail: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("ShopifyOrders");
  },
};
