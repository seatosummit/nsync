"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("PricingEnhancements", {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4,
      },
      SalesChannelId: {
        type: Sequelize.UUID,
        references: {
          model: "SalesChannels",
          key: "id",
          as: "SalesChannelId",
        },
      },
      PricingId: {
        type: Sequelize.UUID,
        references: {
          model: "Pricings",
          key: "id",
          as: "PricingId",
        },
      },
      PriceListId: {
        type: Sequelize.UUID,
        references: {
          model: "PriceLists",
          key: "id",
          as: "PriceListId",
        },
      },
      PricingUpdateId: {
        type: Sequelize.UUID,
        references: {
          model: "PricingUpdates",
          key: "id",
          as: "PricingUpdateId",
        },
      },
      amount: Sequelize.DOUBLE,
      quantity: Sequelize.INTEGER,
      sku: Sequelize.STRING,
      price_list_slug: Sequelize.STRING,
      valid: Sequelize.BOOLEAN,
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("PricingEnhancements");
  },
};
