"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("ShopifyPricings", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      shopify_product_id: {
        type: Sequelize.STRING,
      },
      shopify_variant_id: {
        type: Sequelize.STRING,
      },
      parent_sku: {
        type: Sequelize.STRING,
      },
      variant_sku: {
        type: Sequelize.STRING,
      },
      variant_ean: {
        type: Sequelize.STRING,
      },
      price_retail: {
        type: Sequelize.DOUBLE,
      },
      price_warehouse: {
        type: Sequelize.DOUBLE,
      },
      price_special: {
        type: Sequelize.DOUBLE,
      },
      special_price_startdate: {
        type: Sequelize.DATE,
      },
      special_price_enddate: {
        type: Sequelize.DATE,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("ShopifyPricings");
  },
};
