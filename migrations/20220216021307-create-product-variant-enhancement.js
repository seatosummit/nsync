"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("ProductVariantEnhancements", {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4,
      },
      productId: {
        type: Sequelize.UUID,
      },
      productVariantId: {
        type: Sequelize.UUID,
      },
      salesChannelId: {
        type: Sequelize.UUID,
      },
      sku: {
        type: Sequelize.STRING,
      },
      shopifyProductId: {
        type: Sequelize.BIGINT,
      },
      shopifyVariantId: {
        type: Sequelize.BIGINT,
      },
      shopifyImageId: {
        type: Sequelize.BIGINT,
      },
      price: {
        type: Sequelize.DOUBLE,
      },
      compareToPrice: {
        type: Sequelize.DOUBLE,
      },
      warehousePrice: {
        type: Sequelize.DOUBLE,
      },
      size: {
        type: Sequelize.STRING,
      },
      colour: {
        type: Sequelize.STRING,
      },
      colourLayman: {
        type: Sequelize.STRING,
      },
      metafields: {
        type: Sequelize.JSON,
      },
      shopifySnapshot: {
        type: Sequelize.JSON,
      },
      shopifyInventoryItemId: {
        type: Sequelize.BIGINT,
      },
      shopifyInventoryQuantity: {
        type: Sequelize.INTEGER,
      },
      enabled: {
        type: Sequelize.BOOLEAN,
      },
      minimumOrderQuantity: {
        type: Sequelize.INTEGER,
      },
      updatedToShopify: {
        type: Sequelize.DATE,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("ProductVariantEnhancements");
  },
};
