"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        "WMSProducts", // table name
        "b2b_enabled", // new field name
        {
          type: Sequelize.BOOLEAN,
        }
      ),
      queryInterface.addColumn(
        "WMSProducts", // table name
        "b2c_enabled", // new field name
        {
          type: Sequelize.BOOLEAN,
        }
      ),
    ]);
  },
  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn("WMSProducts", "b2c_enabled"),
      queryInterface.removeColumn("WMSProducts", "b2b_enabled"),
    ]);
  },
};
