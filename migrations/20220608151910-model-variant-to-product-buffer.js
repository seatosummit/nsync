"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn(
        "Products", // table name
        "modelVariant", // new field name
        {
          type: Sequelize.STRING,
        }
      ),
      queryInterface.addColumn(
        "ProductVariants", // table name
        "modelVariant", // new field name
        {
          type: Sequelize.STRING,
        }
      ),
    ]);
  },
  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeColumn("Products", "modelVariant"),
      queryInterface.removeColumn("ProductVariants", "modelVariant"),
    ]);
  },
};
