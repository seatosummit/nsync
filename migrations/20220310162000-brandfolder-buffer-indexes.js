"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addIndex("BrandfolderBufferAssets", {
      name: "BrandfolderBufferAssets_section_id_IDX",
      using: "BTREE",
      fields: [
        {
          name: "section_id",
          order: "ASC",
        },
      ],
    });
    await queryInterface.addIndex("BrandfolderBufferAttachments", {
      name: "BrandfolderBufferAttachments_section_id_IDX",
      using: "BTREE",
      fields: [
        {
          name: "section_id",
          order: "ASC",
        },
      ],
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex(
      "BrandfolderBufferAssets",
      "BrandfolderBufferAssets_section_id_IDX"
    );
    await queryInterface.removeIndex(
      "BrandfolderBufferAttachments",
      "BrandfolderBufferAttachments_section_id_IDX"
    );
  },
};
