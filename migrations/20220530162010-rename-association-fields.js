"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.renameColumn(
        "ProductVariants", // table name
        "productId", // old field name
        "ProductId" // new field name
      ),
      queryInterface.renameColumn(
        "ProductVariantEnhancements", // table name
        "productId", // old field name
        "ProductId" // new field name
      ),
      queryInterface.renameColumn(
        "ProductVariantEnhancements", // table name
        "productVariantId", // old field name
        "ProductVariantId" // new field name
      ),
      queryInterface.renameColumn(
        "ProductVariantEnhancements", // table name
        "salesChannelId", // old field name
        "SalesChannelId" // new field name
      ),
      queryInterface.renameColumn(
        "ProductEnhancements", // table name
        "productId", // old field name
        "ProductId" // new field name
      ),
      queryInterface.renameColumn(
        "ProductEnhancements", // table name
        "salesChannelId", // old field name
        "SalesChannelId" // new field name
      ),
    ]);
  },
  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.renameColumn("ProductVariants", "ProductId", "productId"),
      queryInterface.renameColumn(
        "ProductVariantEnhancements",
        "ProductId",
        "productId"
      ),
      queryInterface.renameColumn(
        "ProductVariantEnhancements",
        "SalesChannelId",
        "salesChannelId"
      ),
      queryInterface.renameColumn(
        "ProductVariantEnhancements",
        "ProductVariantId",
        "productVariantId"
      ),
      queryInterface.renameColumn(
        "ProductEnhancements",
        "productId",
        "ProductId"
      ),
      queryInterface.renameColumn(
        "ProductEnhancements",
        "SalesChannelId",
        "salesChannelId"
      ),
    ]);
  },
};
