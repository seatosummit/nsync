"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("BrandfolderBufferAttachments", {
      id: {
        allowNull: false,
        autoIncrement: false,
        primaryKey: true,
        type: Sequelize.UUID,
      },
      BrandfolderBufferAssetId: {
        type: Sequelize.UUID,
        allowNull: false,
      },
      bf_id: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      bf_attachment_id: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      bf_asset_id: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      filename: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      extension: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      section_id: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      url: {
        type: Sequelize.STRING,
        allowNull: true,
      },
      bf_updated_date: {
        type: Sequelize.DATE,
      },
      metadata: {
        type: Sequelize.JSON,
      },
      snapshot: {
        type: Sequelize.JSON,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("BrandfolderBufferAttachments");
  },
};
