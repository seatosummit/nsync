"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("ShopifyBufferProductVariants", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      shopify_id: {
        type: Sequelize.BIGINT,
      },
      shopify_product_id: {
        type: Sequelize.BIGINT,
      },
      shopify_title: {
        type: Sequelize.STRING,
      },
      shopify_price: {
        type: Sequelize.DOUBLE,
      },
      shopify_compare_at_price: {
        type: Sequelize.DOUBLE,
      },
      shopify_sku: {
        type: Sequelize.STRING,
      },
      shopify_position: {
        type: Sequelize.INTEGER,
      },
      shopify_inventory_policy: {
        type: Sequelize.STRING,
        default: "deny",
      },
      shopify_fulfillment_service: {
        type: Sequelize.STRING,
        default: "manual",
      },
      shopify_inventory_management: {
        type: Sequelize.STRING,
        default: "shopify",
      },
      shopify_option1: {
        type: Sequelize.STRING,
      },
      shopify_option2: {
        type: Sequelize.STRING,
      },
      shopify_option3: {
        type: Sequelize.STRING,
      },
      shopify_taxable: {
        type: Sequelize.BOOLEAN,
        default: true,
      },
      shopify_barcode: {
        type: Sequelize.STRING,
      },
      shopify_grams: {
        type: Sequelize.DOUBLE,
      },
      shopify_weight: {
        type: Sequelize.DOUBLE,
      },
      shopify_weight_unit: {
        type: Sequelize.STRING,
      },
      shopify_image_id: {
        type: Sequelize.BIGINT,
      },
      shopify_inventory_item_id: {
        type: Sequelize.BIGINT,
      },
      shopify_inventory_quantity: {
        type: Sequelize.INTEGER,
      },
      shopify_old_inventory_quantity: {
        type: Sequelize.INTEGER,
      },
      shopify_requires_shipping: {
        type: Sequelize.BOOLEAN,
        default: true,
      },
      shopify_admin_graphql_api_id: {
        type: Sequelize.STRING,
      },
      salsify_system_id: {
        type: Sequelize.STRING,
      },
      salsify_parent_sku: {
        type: Sequelize.STRING,
      },
      salsify_size_variant: {
        type: Sequelize.STRING,
      },
      salsify_colour_variant: {
        type: Sequelize.STRING,
      },
      salsify_laymen_colour: {
        type: Sequelize.STRING,
      },
      erp_b2b_enabled: {
        type: Sequelize.BOOLEAN,
        default: false,
      },
      erp_b2c_enabled: {
        type: Sequelize.BOOLEAN,
        default: false,
      },
      shopify_snapshot: {
        type: Sequelize.TEXT,
      },
      salsify_snapshot: {
        type: Sequelize.TEXT,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("ShopifyBufferProductVariants");
  },
};
