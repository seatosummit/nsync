"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addConstraint(
        "ProductVariantEnhancements", // table name
        {
          fields: ["SalesChannelId"],
          type: "foreign key",
          name: "ProductVariantEnhancements_SalesChannelId_SalesChannels_fk",
          references: {
            table: "SalesChannels",
            field: "id",
          },
        }
      ),
      queryInterface.addConstraint(
        "ProductEnhancements", // table name
        {
          fields: ["SalesChannelId"],
          type: "foreign key",
          name: "ProductEnhancements_SalesChannelId_SalesChannels_fk",
          references: {
            table: "SalesChannels",
            field: "id",
          },
        }
      ),
      queryInterface.addConstraint(
        "SalesChannelIntegrationRulesets", // table name
        {
          fields: ["SalesChannelId"],
          type: "foreign key",
          name: "SalesChannelIntegrationRulesets_SalesChannelId_SalesChannels_fk",
          references: {
            table: "SalesChannels",
            field: "id",
          },
        }
      ),
    ]);
  },
  down: async (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.removeConstraint(
        "ProductVariantEnhancements",
        "ProductVariantEnhancements_SalesChannelId_SalesChannels_fk"
      ),
      queryInterface.removeConstraint(
        "ProductEnhancements",
        "ProductEnhancements_SalesChannelId_SalesChannels_fk"
      ),
      queryInterface.removeConstraint(
        "SalesChannelIntegrationRulesets",
        "SalesChannelIntegrationRulesets_SalesChannelId_SalesChannels_fk"
      ),
    ]);
  },
};
