#!/bin/bash
cd ..
docker-compose down && docker-compose up -d
docker-compose exec postgres bash -c 'sed -i -e "s/\r$//" ./docker-entrypoint-initdb.d/restore-localhost-dump.sh'
docker-compose exec postgres bash -c './docker-entrypoint-initdb.d/restore-localhost-dump.sh'
echo ".... DONE."
echo "Running any new migrations..."
npx sequelize db:migrate
echo ".... DONE."
exit