#!/bin/bash
echo "Getting latest production dump from Heroku..."
heroku pg:backups:capture -a nsync-production
heroku pg:backups:download -a nsync-production -o ../backup/production.dump
echo "Resetting database into the current dump file in /backup..."
cd ..
docker-compose down && docker-compose up -d
docker-compose exec postgres bash -c 'sed -i -e "s/\r$//" ./docker-entrypoint-initdb.d/restore-production-dump.sh'
docker-compose exec postgres bash -c './docker-entrypoint-initdb.d/restore-production-dump.sh'
echo ".... DONE."
echo "Running any new migrations..."
npx sequelize db:migrate
echo ".... DONE."
exit