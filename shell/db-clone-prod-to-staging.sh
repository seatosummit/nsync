#!/bin/bash
echo "Creating Heroku PROD DB backup..."
heroku pg:backups:capture --app nsync-production postgresql-slippery-13907
echo "Overwriting STAGING DB using dump from PROD DB. Please confirm"
heroku pg:backups restore --app nsync-staging $(heroku pg:backups public-url --app nsync-production) DATABASE_URL
