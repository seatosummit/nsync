#!/bin/bash
echo "Running all migrations"
heroku run --app nsync-production npx sequelize db:migrate
echo "Cleaning up enhancements..."
heroku run --app nsync-production npm run enhancement-cleanup
echo "Importing Salsify stuff..."
heroku run --app nsync-production npm run salsify-sync
echo "Importing products from sales channels..."
heroku run --app nsync-production npm run import-from-shopify "BLACKDIAMOND"
heroku run --app nsync-production npm run import-from-shopify "B2BDIST"
heroku run --app nsync-production npm run import-from-shopify "CAMELBAK"
heroku run --app nsync-production npm run import-from-shopify "HELINOX"
heroku run --app nsync-production npm run import-from-shopify "SEA TO SUMMIT"
echo "Refreshing all variant enhancements..."
heroku run --app nsync-production npm run refresh-variants "BLACKDIAMOND"
heroku run --app nsync-production npm run refresh-variants "B2BDIST"
heroku run --app nsync-production npm run refresh-variants "CAMELBAK"
heroku run --app nsync-production npm run refresh-variants "HELINOX"
heroku run --app nsync-production npm run refresh-variants "SEA TO SUMMIT"
echo "...DONE."
exit 0