const {
  IntegrationRuleset,
} = require("../../modules/helpers/IntegrationRuleset");

const b2brules = {
  statuses: [
    {
      status: "ACTIVE",
    },
    {
      status: "DISCONTINUED",
      stockRequired: true,
      promiseDateRequired: true,
    },
    {
      status: "END OF LINE",
      stockRequired: true,
      promiseDateRequired: true,
    },
    {
      status: "END OF LIFE",
      stockRequired: true,
      promiseDateRequired: true,
    },
    {
      status: "SPECIAL ORDER",
    },
    {
      status: "EOL",
      stockRequired: true,
      promiseDateRequired: true,
    },
    {
      status: "INACTIVE",
      stockRequired: true,
      promiseDateRequired: true,
    },
  ],
  flags: [
    {
      name: "b2b_enabled",
    },
  ],
  specialRules: [
    // {
    //   name: "moreStockWithinMonths",
    //   months: 3,
    // },
  ],
};
const b2crules = {
  statuses: [
    {
      status: "ACTIVE",
    },
    {
      status: "DISCONTINUED",
      stockRequired: true,
      promiseDateRequired: true,
    },
    {
      status: "END OF LINE",
      stockRequired: true,
      promiseDateRequired: true,
    },
    {
      status: "INACTIVE",
      stockRequired: true,
      promiseDateRequired: true,
    },
    {
      status: "END OF LIFE",
      stockRequired: true,
      promiseDateRequired: true,
    },
    {
      status: "DIRECT TO CONSUMER",
    },
    {
      status: "SPECIAL ORDER",
    },
    {
      status: "EOL",
      stockRequired: true,
    },
  ],
  flags: [
    {
      name: "b2c_enabled",
    },
  ],
  specialRules: [],
};

describe("SKU disable/enable according to rules", () => {
  beforeEach(() => {
    // jest.resetModules();
  });

  afterAll(async () => {
    // await WMSProduct.destroy({
    //   where: {},
    //   truncate: true,
    // });
    //await sequelize.close();
  });

  it("Product enabled for B2C", async () => {
    const productInstance = {
      retailPrice: 12.59,
      b2c_enabled: true,
      stockQuantity: 0,
      status: "SPECIAL ORDER",
    };

    const validator = new IntegrationRuleset({ ruleset: b2crules });
    const variantEvaluationResult = validator.productPassesEnabledRules({
      productInstance,
    });

    expect(variantEvaluationResult.disable_reason).toBeUndefined();
    expect(variantEvaluationResult.passed).toEqual(true);
  });

  it("Product enabled for for B2B", async () => {
    const productInstance = {
      retailPrice: 12.59,
      b2c_enabled: false,
      b2b_enabled: true,
      status: "SPECIAL ORDER",
      stockQuantity: 0,
    };

    const validator = new IntegrationRuleset({ ruleset: b2brules });
    const variantEvaluationResult = validator.productPassesEnabledRules({
      productInstance,
    });

    expect(variantEvaluationResult.disable_reason).toBeUndefined();
    expect(variantEvaluationResult.passed).toEqual(true);
  });
  it("Consumer-only product disabled for for B2B", async () => {
    const productInstance = {
      retailPrice: 12.59,
      b2c_enabled: true,
      b2b_enabled: true,
      status: "DIRECT TO CONSUMER",
      stockQuantity: 1,
    };

    const validator = new IntegrationRuleset({ ruleset: b2brules });
    const variantEvaluationResult = validator.productPassesEnabledRules({
      productInstance,
    });

    expect(variantEvaluationResult.disable_reason).toBeDefined();
    expect(variantEvaluationResult.passed).toEqual(false);
  });
  it("Discontinued Product enabled because of stock", async () => {
    const productInstance = {
      retailPrice: 12.59,
      b2c_enabled: true,
      b2b_enabled: true,
      status: "DISCONTINUED",
      stockQuantity: 1,
    };

    const validator = new IntegrationRuleset({ ruleset: b2crules });
    const variantEvaluationResult = validator.productPassesEnabledRules({
      productInstance,
    });

    expect(variantEvaluationResult.disable_reason).toBeUndefined();
    expect(variantEvaluationResult.passed).toEqual(true);
  });
  it("Discontinued Product enabled because of promise date", async () => {
    const productInstance = {
      retailPrice: 12.59,
      b2c_enabled: true,
      b2b_enabled: true,
      status: "DISCONTINUED",
      stockQuantity: 0,
      stockETA: "2022-10-01T00:00:00",
    };

    const validator = new IntegrationRuleset({ ruleset: b2crules });
    const variantEvaluationResult = validator.productPassesEnabledRules({
      productInstance,
    });

    expect(variantEvaluationResult.disable_reason).toBeUndefined();
    expect(variantEvaluationResult.passed).toEqual(true);
  });

  it("Discontinued Product disabled because of zero stock and no promise date", async () => {
    const productInstance = {
      retailPrice: 12.59,
      b2c_enabled: true,
      b2b_enabled: false,
      status: "INACTIVE",
      stockQuantity: 0,
      stockETA: "",
    };

    const validator = new IntegrationRuleset({ ruleset: b2crules });
    const variantEvaluationResult = validator.productPassesEnabledRules({
      productInstance,
    });

    expect(variantEvaluationResult.disable_reason).toBeDefined();
    expect(variantEvaluationResult.passed).toEqual(false);
  });
  it("Product active but stock is empty", async () => {
    const productInstance = {
      retailPrice: 12.59,
      b2c_enabled: true,
      b2b_enabled: false,
      status: "ACTIVE",
      stockQuantity: null,
      stockETA: "",
    };

    const validator = new IntegrationRuleset({ ruleset: b2crules });
    const variantEvaluationResult = validator.productPassesEnabledRules({
      productInstance,
    });

    expect(variantEvaluationResult.disable_reason).toBeUndefined();
    expect(variantEvaluationResult.passed).toEqual(true);
  });

  it("Product disabled due to zero price", async () => {
    const productInstance = {
      retailPrice: 0,
      b2c_enabled: true,
      status: "ACTIVE",
    };

    const validator = new IntegrationRuleset({ ruleset: b2crules });
    const variantEvaluationResult = validator.productPassesEnabledRules({
      productInstance,
    });

    expect(variantEvaluationResult.disable_reason).toBeDefined();
    expect(variantEvaluationResult.passed).toEqual(false);
  });
});
