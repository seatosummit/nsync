const {
  IntegrationRuleset,
} = require("../../modules/helpers/IntegrationRuleset");

const b2brules = {
  statuses: [
    {
      status: "ACTIVE",
    },
    {
      status: "DISCONTINUED",
      stockRequired: true,
      promiseDateRequired: true,
    },
    {
      status: "END OF LINE",
      stockRequired: true,
      promiseDateRequired: true,
    },
    {
      status: "END OF LIFE",
      stockRequired: true,
      promiseDateRequired: true,
    },
    {
      status: "SPECIAL ORDER",
    },
    {
      status: "EOL",
      stockRequired: true,
    },
    {
      status: "INACTIVE",
      stockRequired: true,
      promiseDateRequired: true,
    },
  ],
  flags: [
    {
      name: "b2b_enabled",
    },
  ],
  specialRules: [
    {
      name: "moreStockWithinMonths",
      months: 3,
    },
  ],
};
const b2crules = {
  statuses: [
    {
      status: "ACTIVE",
    },
    {
      status: "DISCONTINUED",
      stockRequired: true,
      promiseDateRequired: true,
    },
    {
      status: "END OF LINE",
      stockRequired: true,
      promiseDateRequired: true,
    },
    {
      status: "INACTIVE",
      stockRequired: true,
      promiseDateRequired: true,
    },
    {
      status: "END OF LIFE",
      stockRequired: true,
      promiseDateRequired: true,
    },
    {
      status: "DIRECT TO CONSUMER",
    },
    {
      status: "SPECIAL ORDER",
    },
    {
      status: "EOL",
      stockRequired: true,
    },
  ],
  flags: [
    {
      name: "b2c_enabled",
    },
  ],
  specialRules: [],
};

describe("Reasons for disabling SKUs", () => {
  beforeEach(() => {
    // jest.resetModules();
  });

  afterAll(async () => {
    // await WMSProduct.destroy({
    //   where: {},
    //   truncate: true,
    // });
    //await sequelize.close();
  });

  it("Zero price", async () => {
    const productInstance = {
      retailPrice: 0,
      b2c_enabled: "1",
      shopifyInventoryQuantity: 0,
      status: "SPECIAL ORDER",
    };

    const validator = new IntegrationRuleset({ ruleset: b2crules });
    const variantEvaluationResult = validator.productPassesEnabledRules({
      productInstance,
    });

    expect(variantEvaluationResult.passed).toEqual(false);
    expect(variantEvaluationResult.disable_reason).toEqual("Price is zero");
  });
  it("Z-coded sku", async () => {
    const productInstance = {
      retailPrice: 12.25,
      sku: "ZFGFDG",
      b2c_enabled: "1",
      stockQuantity: 0,
      status: "SPECIAL ORDER",
    };

    const validator = new IntegrationRuleset({ ruleset: b2crules });
    const variantEvaluationResult = validator.productPassesEnabledRules({
      productInstance,
    });

    expect(variantEvaluationResult.passed).toEqual(false);
    expect(variantEvaluationResult.disable_reason).toEqual("Z-coded SKU");
  });
  it("Status not matching", async () => {
    const productInstance = {
      retailPrice: 12.25,
      sku: "FGFDG",
      b2c_enabled: false,
      b2b_enabled: "1",
      stockQuantity: 0,
      status: "DIRECT TO CONSUMER",
    };

    const validator = new IntegrationRuleset({ ruleset: b2brules });
    const variantEvaluationResult = validator.productPassesEnabledRules({
      productInstance,
    });

    expect(variantEvaluationResult.passed).toEqual(false);
    expect(variantEvaluationResult.disable_reason).toEqual(
      "Status not one of the required ones"
    );
  });
  it("EOL, zero stock and no promise date", async () => {
    const productInstance = {
      retailPrice: 12.25,
      sku: "FGFDG",
      b2c_enabled: true,
      b2b_enabled: "1",
      stockQuantity: 0,
      stockETA: "",
      status: "END OF LIFE",
    };

    const validator = new IntegrationRuleset({ ruleset: b2crules });
    const variantEvaluationResult = validator.productPassesEnabledRules({
      productInstance,
    });

    expect(variantEvaluationResult.passed).toEqual(false);
    expect(variantEvaluationResult.disable_reason).toEqual(
      "EOL, zero stock, no promise date"
    );
  });
  it("EOL, zero stock, promise date option not needed for this status", async () => {
    const productInstance = {
      retailPrice: 12.25,
      sku: "FGFDG",
      b2c_enabled: true,
      b2b_enabled: "1",
      stockQuantity: 0,
      status: "EOL",
    };

    const validator = new IntegrationRuleset({ ruleset: b2crules });
    const variantEvaluationResult = validator.productPassesEnabledRules({
      productInstance,
    });

    expect(variantEvaluationResult.passed).toEqual(false);
    expect(variantEvaluationResult.disable_reason).toEqual("EOL, zero stock");
  });
  it("Missing b2c flag", async () => {
    const productInstance = {
      retailPrice: 12.25,
      sku: "FGFDG",
      b2c_enabled: false,
      b2b_enabled: true,
      stockQuantity: 0,
      status: "EOL",
    };

    const validator = new IntegrationRuleset({ ruleset: b2crules });
    const variantEvaluationResult = validator.productPassesEnabledRules({
      productInstance,
    });

    expect(variantEvaluationResult.passed).toEqual(false);
    expect(variantEvaluationResult.disable_reason).toEqual(
      "Missing required b2c_enabled flag"
    );
  });
  it("Missing b2b flag", async () => {
    const productInstance = {
      retailPrice: 12.25,
      sku: "FGFDG",
      b2c_enabled: true,
      b2b_enabled: false,
      stockQuantity: 0,
      status: "EOL",
    };

    const validator = new IntegrationRuleset({ ruleset: b2brules });
    const variantEvaluationResult = validator.productPassesEnabledRules({
      productInstance,
    });

    expect(variantEvaluationResult.passed).toEqual(false);
    expect(variantEvaluationResult.disable_reason).toEqual(
      "Missing required b2b_enabled flag"
    );
  });
});
