const IntegrationRuleTools = require("../../modules/helpers/IntegrationRuleTools");
const {
  SalesChannel,
  SalesChannelIntegrationRuleset,
} = require("../../models");

let channel;
let ruleset;
describe("Passing Sales Channel rulesets", () => {
  beforeEach(() => {
    // jest.resetModules();

    channel = SalesChannel.build({
      id: "5c4b33b7-4b99-46fb-81cc-ad2e3028bc9d",
      machineName: "TEST CHANNEL",
    });
    ruleset = SalesChannelIntegrationRuleset.build({
      id: "a61e7854-86d7-4eb9-a2a8-ce8a69219fa4",
      SalesChannelId: "5c4b33b7-4b99-46fb-81cc-ad2e3028bc9d",
      productAdd: false,
      productRemove: true,
      productUpdate: false,
      productCategorise: false,
      productEnabledRules: {
        statuses: [
          { status: "ACTIVE" },
          {
            status: "DISCONTINUED",
            stockRequired: true,
            promiseDateRequired: true,
          },
          {
            status: "END OF LINE",
            stockRequired: true,
            promiseDateRequired: true,
          },
          {
            status: "INACTIVE",
            stockRequired: true,
            promiseDateRequired: true,
          },
          {
            status: "END OF LIFE",
            stockRequired: true,
            promiseDateRequired: true,
          },
          { status: "DIRECT TO CONSUMER" },
          { status: "SPECIAL ORDER" },
          { status: "EOL", stockRequired: true, promiseDateRequired: true },
        ],
        flags: [{ name: "b2c_enabled" }],
        specialRules: [],
      },
      imagesAdd: false,
      imagesUpdate: false,
      ordersExport: true,
      ordersUpdateFulfilment: false,
      salsifyCategoriesSync: false,
      priceUpdate: true,
      stockUpdate: true,
      erpProductDataView: null,
      createdAt: "2022-05-03T16:00:00.000Z",
      updatedAt: "2022-05-03T16:00:00.000Z",
      orderExportFilter: null,
      customOrderNotification: false,
      customOrderNotificationEmail: null,
      tagOrdersWithNoStock: false,
    });
    channel.SalesChannelIntegrationRuleset = ruleset;
  });

  afterAll(async () => {
    // await WMSProduct.destroy({
    //   where: {},
    //   truncate: true,
    // });
    //await sequelize.close();
  });

  it("productRemove is allowed", async () => {
    const testRule = IntegrationRuleTools.checkForRule({
      ShopifyStore: channel,
      rule: "productRemove",
    });

    expect(testRule).toEqual(true);
  });
  it("productRemove is not allowed", async () => {
    channel.SalesChannelIntegrationRuleset.productRemove = false;
    const testRule = IntegrationRuleTools.checkForRule({
      ShopifyStore: channel,
      rule: "productRemove",
    });

    expect(testRule).toEqual(false);
  });
});
