const {
  optionStatus,
  variantOptionStatuses,
} = require("../../modules/shopify/products/productdecorator");

describe("Shopify colour and size options", () => {
  it("Both colour and size options", () => {
    const salsifyBufferChildProducts = [
      {
        name: "Binky",
        colour: "Blue",
        size: "Medium",
      },
      {
        name: "Binky",
        colour: "Blue",
        size: "Large",
      },
    ];

    expect(optionStatus(salsifyBufferChildProducts)).toEqual(
      variantOptionStatuses.COLOUR_AND_SIZE
    );
  });
  it("Colour only", () => {
    const salsifyBufferChildProducts = [
      {
        name: "Binky",
        colour: "Blue",
      },
      {
        name: "Binky",
        colour: "Blue",
      },
    ];
    expect(optionStatus(salsifyBufferChildProducts)).toEqual(
      variantOptionStatuses.COLOUR_ONLY
    );
  });

  it("Size only", () => {
    const salsifyBufferChildProducts = [
      {
        name: "Binky",
        size: "Large",
      },
      {
        name: "Binky",
        size: "Sumtin",
      },
    ];
    expect(optionStatus(salsifyBufferChildProducts)).toEqual(
      variantOptionStatuses.SIZE_ONLY
    );
  });

  it("Size only", () => {
    const salsifyBufferChildProducts = [
      {
        name: "Binky",
        size: "Large",
      },
      {
        name: "Binky",
        size: "Sumtin",
      },
    ];
    expect(optionStatus(salsifyBufferChildProducts)).toEqual(
      variantOptionStatuses.SIZE_ONLY
    );
  });
  it("Inconsistent options", () => {
    const salsifyBufferChildProducts = [
      {
        name: "Binky",
        colour: "Pink",
      },
      {
        name: "Binky",
        size: "Sumtin",
      },
    ];
    expect(optionStatus(salsifyBufferChildProducts)).toEqual(
      variantOptionStatuses.INCONSISTENT_OPTIONS
    );
  });
});
