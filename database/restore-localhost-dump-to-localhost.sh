#!/bin/bash
set -e
echo "Restoring from recent LOCALHOST DB dump........"
dropdb -h localhost -U $POSTGRES_USER -i --if-exists nsync
createdb -h localhost -U $POSTGRES_USER nsync
pg_restore --clean --no-acl --no-owner --if-exists -h localhost -U $POSTGRES_USER -d nsync /backup/localhost.dump
