const {
  refreshAllVariants,
} = require("../modules/central-buffer/CentralBufferSchedule");
const { Logger } = require("../modules/helpers/Logger");
const {
  bulkUpdatePricingToShopify,
} = require("../modules/shopify/store/ShopifyStoreSchedule");
const { salesChannelNameFromCommandLine } = require("./CommandTools");
(async () => {
  try {
    const salesChannelMachineName = await salesChannelNameFromCommandLine();
    Logger.info(
      `Bulk updating pricing to Shopify for ${salesChannelMachineName}....`
    );
    await bulkUpdatePricingToShopify({
      machineNameOfSalesChannel: salesChannelMachineName,
    });
  } catch (error) {
    Logger.error(`Issue with bulk updating pricing to Shopify: ${error}`);
  }
  process.exit();
})();
