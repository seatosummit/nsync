const {
  Product,
  ProductVariant,
  ProductEnhancement,
  ProductVariantEnhancement,
  PricingEnhancement,
  PricingUpdate,
} = require("../models");
const { Logger } = require("../modules/helpers/Logger");
(async () => {
  try {
    Logger.info(`Cleaning up ProductEnhancements...`);
    await ProductEnhancement.destroy({
      where: {},
      truncate: true,
    });
    Logger.info(`Cleaning up ProductVariantEnhancements...`);
    await ProductVariantEnhancement.destroy({
      where: {},
      truncate: true,
    });
    Logger.info(`Cleaning up ProductVariants...`);
    await ProductVariant.destroy({
      where: {},
      truncate: true,
    });
    Logger.info(`Cleaning up Products...`);
    await Product.destroy({
      where: {},
      truncate: true,
    });
    Logger.info(`Cleaning up PricingEnhancements...`);
    await PricingEnhancement.destroy({
      where: {},
      truncate: true,
    });
    Logger.info(`Cleaning up PricingUpdates...`);
    await PricingUpdate.destroy({
      where: {},
      truncate: true,
    });
    process.exit();
  } catch (error) {
    Logger.error(`Problem with cleaning up enhancements: ${error}`);
    process.exit(1);
  }
})();
