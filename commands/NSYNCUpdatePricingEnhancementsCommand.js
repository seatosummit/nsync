const {
  updatePricingEnhancementsBuffer,
} = require("../modules/central-buffer/CentralBufferSchedule");
const { Logger } = require("../modules/helpers/Logger");
const { salesChannelNameFromCommandLine } = require("./CommandTools");
(async () => {
  try {
    const salesChannelMachineName = await salesChannelNameFromCommandLine();
    Logger.info(
      `Refreshing PricingEnhancements and PricingUpdates for ${salesChannelMachineName}....`
    );
    await updatePricingEnhancementsBuffer({
      machineNameOfSalesChannel: salesChannelMachineName,
    });
  } catch (error) {
    Logger.error(`Problem with updating PricingEnhancements: ${error}`);
  }
  process.exit();
})();
