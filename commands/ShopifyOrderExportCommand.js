require("dotenv").config();
const { Logger } = require("../modules/helpers/Logger");
const prompt = require("prompt");
const colors = require("@colors/colors/safe");
const {
  exportOneShopifyOrder,
} = require("../modules/shopify/orders/ShopifyOrderSchedule");
const { ShopifyConfig } = require("../modules/helpers/ShopifyConfig");

(async () => {
  const ShopifyStores = await ShopifyConfig.getConfigsFromDatabase();

  const stringOfChannels = ShopifyStores.map(
    (store, index) => `${index}: ${store.machineName}`
  ).join("\n");

  var schema = {
    properties: {
      salesChannelIndex: {
        minimum: 0,
        maximum: ShopifyStores.length - 1,
        type: "integer",
        description: colors.brightGreen(" Sales channel # "),
        message: "You need to choose an available sales channel",
        required: true,
      },
      orderNumber: {
        description: colors.brightGreen(" Order # "),
        required: true,
      },
    },
  };

  prompt.message = colors.brightGreen(
    `MANUAL ORDER EXPORT\nThis will re-export the XML for an order and place it in the WMS orders queue.\nPlease choose a sales channel: \n${stringOfChannels}\n\n`
  );
  prompt.delimiter = colors.green(">");
  prompt.start();

  prompt.get(schema, async (err, result) => {
    if (err) {
      return onErr(err);
    }
    Logger.info("Exporting order :");
    Logger.info(
      "  machineName: " + ShopifyStores[result.salesChannelIndex].machineName
    );
    Logger.info("  orderNumber: " + result.orderNumber);

    try {
      await exportOneShopifyOrder({
        salesChannelMachineName:
          ShopifyStores[result.salesChannelIndex].machineName,
        orderNumber: result.orderNumber,
      });
      process.exit();
    } catch (error) {
      Logger.error(`Blip ${error}`);
      process.exit(1);
    }
  });

  function onErr(err) {
    Logger.info(err);
    return 1;
  }
})();
