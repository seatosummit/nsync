const {
  CreateProductVariantRelationships,
} = require("../modules/central-buffer/BufferOperations");
const {
  syncAustraliaSalsifyBuffer,
} = require("../modules/salsify/products/productbuffer");
(async () => {
  await syncAustraliaSalsifyBuffer();
  await CreateProductVariantRelationships();
  process.exit();
})();
