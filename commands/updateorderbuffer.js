const { updateOrderBufferAllProducts } = require('../modules/orders/order');

const allOrders = async() => {
    await updateOrderBufferAllProducts();
}

allOrders();
