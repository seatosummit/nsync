const {
  ShopifyProductImportCommand,
  GenericImport,
} = require("../modules/central-buffer/ImportRunners");
const { Logger } = require("../modules/helpers/Logger");
const { ShopifyConfig } = require("../modules/helpers/ShopifyConfig");

module.exports = {
  salesChannelNameFromCommandLine: async () => {
    const arguments = process.argv.slice(2);
    const salesChannelMachineName = arguments[0];
    if (!salesChannelMachineName) throw `Need a sales channel name`;
    const shopifyStoreMachineNames = (
      await ShopifyConfig.getConfigsFromDatabase()
    ).map((channel) => channel.machineName);

    if (!shopifyStoreMachineNames.includes(salesChannelMachineName))
      throw `The sales channel name ${salesChannelMachineName} doesn't match any sales channels in the database. \n\nPlease provide one of ${shopifyStoreMachineNames
        .map((name) => `"${name}"`)
        .join(
          ", "
        )} like this: \n\nnpm run [command] -- "[SALESCHANNELNAME]"\n\n`;
    return salesChannelMachineName;
  },
};
