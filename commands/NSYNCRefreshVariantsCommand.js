const {
  refreshAllVariants,
} = require("../modules/central-buffer/CentralBufferSchedule");
const { Logger } = require("../modules/helpers/Logger");
const { ShopifyConfig } = require("../modules/helpers/ShopifyConfig");
const { salesChannelNameFromCommandLine } = require("./CommandTools");
(async () => {
  try {
    const salesChannelMachineName = await salesChannelNameFromCommandLine();
    Logger.info(
      `Refreshing buffer for products and variants for ${salesChannelMachineName}....`
    );
    await refreshAllVariants({
      machineNameOfSalesChannel: salesChannelMachineName,
      refreshVariants: true,
      refreshProductEnhancements: true,
      refreshPricingBuffer: true,
      updateOnlyEnabled: false,
    });
    process.exit();
  } catch (error) {
    Logger.error(`Problem with buffer variants refresh: ${error}`);
    process.exit(1);
  }
})();
