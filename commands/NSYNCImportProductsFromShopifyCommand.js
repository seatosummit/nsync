const {
  ShopifyProductImportCommand,
  GenericImport,
} = require("../modules/central-buffer/ImportRunners");
const { Logger } = require("../modules/helpers/Logger");
const { ShopifyConfig } = require("../modules/helpers/ShopifyConfig");
const { salesChannelNameFromCommandLine } = require("./CommandTools");
(async () => {
  try {
    const salesChannelMachineName = await salesChannelNameFromCommandLine();
    Logger.info(
      `Importing product data from ${salesChannelMachineName} Shopify to NSYNC buffer`
    );
    await ShopifyProductImportCommand({
      storeFilterMachineName: salesChannelMachineName,
    });
    await GenericImport();
    process.exit(0);
  } catch (error) {
    Logger.error(`Problem with import: ${error}`);
    process.exit(1);
  }
})();
