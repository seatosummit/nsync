const { Logger } = require("../modules/helpers/Logger");
const {
  bulkUpdateExistingProductBufferIds,
} = require("../modules/shopify/store/ShopifyStoreSchedule");
const { salesChannelNameFromCommandLine } = require("./CommandTools");
(async () => {
  try {
    const salesChannelMachineName = await salesChannelNameFromCommandLine();
    await bulkUpdateExistingProductBufferIds({
      machineNameOfSalesChannel: salesChannelMachineName,
    });
    process.exit();
  } catch (error) {
    Logger.error(`Problem with bufferIds update: ${error}`);
  }
})();
