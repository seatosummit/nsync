const { Logger } = require("../modules/helpers/Logger");
const {
  updatePricingToSparkLayer,
} = require("../modules/sparklayer/SparkLayerSchedule");
const { salesChannelNameFromCommandLine } = require("./CommandTools");
(async () => {
  try {
    const salesChannelMachineName = await salesChannelNameFromCommandLine();
    await updatePricingToSparkLayer({
      machineNameOfSalesChannel: salesChannelMachineName,
    });
    process.exit();
  } catch (error) {
    Logger.error(`Problem with SparkLayer pricingh update: ${error}`);
  }
})();
