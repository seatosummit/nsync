const {
  refreshAllVariants,
} = require("../modules/central-buffer/CentralBufferSchedule");
const { Logger } = require("../modules/helpers/Logger");
const {
  bulkUpdateInventoryToShopify,
  bulkUpdatePricingToShopify,
} = require("../modules/shopify/store/ShopifyStoreSchedule");
const { salesChannelNameFromCommandLine } = require("./CommandTools");
(async () => {
  try {
    const salesChannelMachineName = await salesChannelNameFromCommandLine();
    Logger.info(
      `Bulk updating inventory on Shopify for ${salesChannelMachineName}....`
    );
    await bulkUpdateInventoryToShopify({
      machineNameOfSalesChannel: salesChannelMachineName,
      dryRun: false,
    });
  } catch (error) {
    Logger.error(`Issue with bulk updating inventory: ${error}`);
  }
  process.exit();
})();
