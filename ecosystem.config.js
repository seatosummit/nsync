module.exports = {
  apps: [
    // {
    //   script: "./modules/salsify/schedules/products/productbuffer.js",
    //   cron_restart: "0 */6 * * 1-5",
    //   name: "salsify-products-to-buffer",
    //   env_development: {
    //     NODE_ENV: "test",
    //   },
    //   env_production: {
    //     NODE_ENV: "production",
    //   },
    // },
    // {
    //   script: "./modules/salsify/schedules/products/productparentbuffer.js",
    //   cron_restart: "0 */6 * * 1-5",
    //   name: "salsify-product-parents-to-buffer",
    //   env_development: {
    //     NODE_ENV: "test",
    //   },
    //   env_production: {
    //     NODE_ENV: "production",
    //   },
    // },
    // {
    //   script: "./modules/wms/schedules/products/productbuffer.js",
    //   cron_restart: "*/15 5-17 * * 1-5",
    //   name: "update-wms-productbuffer",
    //   autorestart: false,
    //   env_development: {
    //     NODE_ENV: "test",
    //   },
    //   env_production: {
    //     NODE_ENV: "production",
    //   },
    // },
    // {
    //   script: "./schedules/BDProductBufferSync.js",
    //   cron_restart: "15 */3 * * 1-5",
    //   name: "bd-product-buffer",
    //   autorestart: false,
    //   env_development: {
    //     NODE_ENV: "development",
    //   },
    //   env_production: {
    //     NODE_ENV: "production",
    //   },
    // },
    // {
    //   script: "./schedules/BDVariantBufferSync.js",
    //   cron_restart: "15 */3 * * 1-5",
    //   name: "bd-variant-buffer",
    //   autorestart: false,
    //   env_development: {
    //     NODE_ENV: "test",
    //   },
    //   env_production: {
    //     NODE_ENV: "production",
    //   },
    // },
    {
      script: "./schedules/tempB2BInventoryUpdate.js",
      cron_restart: "*/30 5-17 * * 1-5",
      name: "b2b-temp-inventory",
      autorestart: false,
      env_production: {
        NODE_ENV: "production",
      },
    },
    // {
    //   script: "./schedules/ShopifyOrderSync.js",
    //   name: "bd-orders-export-from-shopify",
    //   autorestart: false,
    //   env_development: {
    //     NODE_ENV: "test",
    //   },
    //   env_production: {
    //     NODE_ENV: "production",
    //   },
    // },
    // {
    //   script: "./schedules/BDInventoryShopifySync.js",
    //   cron_restart: "0 */1 * * 1-5",
    //   name: "bd-inventory-shopify",
    //   autorestart: false,
    //   env_development: {
    //     NODE_ENV: "test",
    //   },
    //   env_production: {
    //     NODE_ENV: "production",
    //   },
    // },
    // {
    //   script: "./schedules/BDPricingShopifySync.js",
    //   cron_restart: "30 */1 * * 1-5",
  ],
};
