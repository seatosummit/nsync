# Changelog

## v3.2

20th July, 2022

- cleanp of NPM commands: consistency in naming, saleschannel variable
- SparkLayer pricing update routines

## v3.1

3rd June, 2022

- bulk create products https://app.clickup.com/t/1wep5v2
- B2B stock level updates and related enhancements https://app.clickup.com/t/2j36yb7
- B2B pricing tiers
- More GraphQL calls

## v3.0

19th May, 2022

- proper integration ruleset support for variant enhancement enable/disable
- stock zeroed out for disabled enhancements

## v2.9

17th May, 2022

- option to auto-tag orders with no-stock lineitems https://app.clickup.com/t/2cj8xtg
- more safeguards for stock level updates https://app.clickup.com/t/2a7f83m

## v2.8

11th May, 2022

- Cherry-pick various integration operations per sales channel: orders/images/inventory/pricing etc
- Auto-import tagged Camelbak orders into WMS
- Send custom notification about selected camelbak orders only

## v2.7

4th May, 2022

- Afterpay order export fix (rename method)
- Manual Camelbak import command

## v2.6

22nd April, 2022

- GraphQL queries into their own files
- GraphQL validation config to VS Code
- bulk pricing updates for variants
- generic mechanism for bulk updates, docs https://shopify.dev/api/usage/bulk-operations/imports
- small fixes to Brandfolder new asset processing
- fix to manual single-order export
- output a CSV of price differentials

## v2.5

14th April, 2022

- manual order import NPM command
- manual BD pricing update

## v2.4

13th April, 2022

- started moving GraphQL scripts into a separate folder
- gift card support for order export
- improvements to generic shopify updates

## v2.3

8th April, 2022

- (semi) bulk updates for inventory entries
- refactor of GraphQL calls: DRY updates, more bulk calls
- using variant export as reference to avoid 404 API calls when updating price and stock
- separate schedules for generic Shopify pricing and inventory updates

## v2.2

31st March, 2022

- Fixes to sync schedules
- Improvement to generic buffer import from Shopify

## v2.1

16th Feb, 2022

- Generic Product / Variant / Enhancement models, with relations to SalesChannel
- Brandfolder integration work
- Fixes to Shopify integration
- Salsify product transformation
- Salsify/BF integration

## v2.0

31st Jan, 2022

- Brandfolder transition https://app.clickup.com/t/1qm6amc
- Fix to barcode/EAN fetch from WMS
- Custom Error objects start
- Centralised helpers for Salsify / Brandfolder APIs and common operations
- Configuration update related to Heroku migration: config.json -> config.js, using `process.env` instead

## v1.3

24th Dec, 2021

- Centralised DB connection, cleanup of redundant DB connections
- paypal paymentprovider fix to order XML export

## v1.2

24th Dec, 2021

- Shopify buffer stats to PM2 monitoring dashboard

## v1.1

24th Dec, 2021

- WMS product sync caters for Z-coded SKUs. They now get cleaned up from the DB buffer
- Better Shopify product buffer sync: variants take pricing rules into account
- Better Shopify product store sync: product status updates properly, prices update on product sync

## v1.0.1

22nd Dec, 2021

- Fix to orders XML export

## v1.0

21st Dec, 2021

- Black Diamond Shopify sync
- Generic Shopify sync
- Brandfolder / Salsify initial sync
- Overall refactoring
